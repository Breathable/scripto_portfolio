//
//  MobileViewController.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 6/26/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Cocoa
import SwiftLoggly


class MobileViewController: NSViewController, NSTableViewDelegate, NSTableViewDataSource, BonjourServerDelegate {
    
    @IBOutlet weak var CloseMobileWindowButton: NSButton!
    @IBOutlet weak var ConnectionLabel: NSTextField!
    
    

    var bonjourServer: BonjourServer!
    
    @IBOutlet var tableView: NSTableView!
    @IBOutlet weak var receivedInformationTable: NSTableView!
    
    
    @IBOutlet weak var RefreshButton: NSButton!
    @IBOutlet weak var StartButtonAction: NSButton!
    @IBOutlet weak var StopButtonAction: NSButton!
    @IBOutlet weak var InstructionButton: NSButton!
    @IBOutlet weak var MobileAddButton: NSButton!
    @IBOutlet weak var MobileCloseButton: NSButton!
    
    
    
    
    
    var RecTableCount = Int32()
    
    
    var MobileListArray: [MobileCategoryInformation] = []
    
    @IBOutlet weak var receivedInformationTableDeleteButton: NSButton!
    
    @IBOutlet weak var addItemToLabel: NSTextField!
    
    
    
    var CurrentAppearanceMobileViewController = String()
    
    
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.bonjourServer = BonjourServer()
//        self.bonjourServer.delegate = self
//
//        self.tableView.dataSource = self
//        self.tableView.delegate = self
    }
    
    override func viewWillAppear() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(StopServiceButtonAction(_:)), name: NSNotification.Name(rawValue: "StopMobileService"), object: nil)
        
        ReceivedTableCount()

        ReceivedTableInformation()
        
        
        //print("MobileListArray Count: \(MobileListArray.count)")
        
        ///////////////////////////////////
        
        
        self.bonjourServer = BonjourServer()
        self.bonjourServer.delegate = self
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        
        self.receivedInformationTable.dataSource = self
        self.receivedInformationTable.delegate = self
        
        
        self.ConnectionLabel.stringValue = "Serivce Running"
        self.ConnectionLabel.textColor = NSColor.green
        

        //////////////////////////////////
        
 
        
        

        
        
        
        
        //////////////////////////////////
        
//        RefreshButton.isEnabled = true
//        StartButtonAction.isEnabled = false
//        StopButtonAction.isEnabled = true
        
        
        
//        StartButtonAction.state = NSControl.StateValue(rawValue: 1)
        
        //StartButtonAction.highlight(true)
        
        //(StartButtonAction.cell as! NSButtonCell).imageDimsWhenDisabled = false
        
        

        
        
        
        //////////////////////////////////
        
        
        ////print("MobileViewController viewWillAppear() RAN")
    
    
    }// end of viewWillAppear()
    
    
    override func viewDidLayout() {
        
        //        enum InterfaceStyle : String {
        //            case Dark, Light
        //
        //            init() {
        //                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
        //                self = InterfaceStyle(rawValue: type)!
        //            }
        //        }
        //
        //        let currentStyle = InterfaceStyle()
        //
        //        //print("currentStyle: \"\(currentStyle)\"  -  CurrentAppearance: \"\(CurrentAppearance)\" ")
        //
        //        if ("\(currentStyle)" != CurrentAppearanceNewUserViewController) {
        //            appearanceCheck()
        //        }
        
        appearanceCheck()
        
        super.viewDidLayout()
        
        print("MobileViewController - viewDidLayout finished")
        
        
    }// end of viewDidLayout()
    
    
    func appearanceCheck() {
        
        enum InterfaceStyle : String {
            case Dark, Light
            
            init() {
                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
                self = InterfaceStyle(rawValue: type)!
            }
        }
        
        let currentStyle = InterfaceStyle()
        
        if ("\(currentStyle)" != CurrentAppearanceMobileViewController) {
            
            if (currentStyle.rawValue == "Dark") {
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColorDarkMode
                
                RefreshButton.styleButtonText(button: RefreshButton, buttonName: "Refresh", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                StartButtonAction.styleButtonText(button: StartButtonAction, buttonName: "Start", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                StopButtonAction.styleButtonText(button: StopButtonAction, buttonName: "Stop", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                InstructionButton.styleButtonText(button: InstructionButton, buttonName: "Instructions", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                MobileAddButton.styleButtonText(button: MobileAddButton, buttonName: "Add", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                MobileCloseButton.styleButtonText(button: MobileCloseButton, buttonName: "Close", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                RefreshButton.wantsLayer = true
                RefreshButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                RefreshButton.layer?.cornerRadius = 7
                
                StartButtonAction.wantsLayer = true
                StartButtonAction.layer?.backgroundColor = NSColor.lightGray.cgColor
                StartButtonAction.layer?.cornerRadius = 7
                
                StopButtonAction.wantsLayer = true
                StopButtonAction.layer?.backgroundColor = NSColor.lightGray.cgColor
                StopButtonAction.layer?.cornerRadius = 7
                
                InstructionButton.wantsLayer = true
                InstructionButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                InstructionButton.layer?.cornerRadius = 7
                
                MobileAddButton.wantsLayer = true
                MobileAddButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                MobileAddButton.layer?.cornerRadius = 7
                
                MobileCloseButton.wantsLayer = true
                MobileCloseButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                MobileCloseButton.layer?.cornerRadius = 7
                
                addItemToLabel.font = NSFont(name:AppFontBold, size: 13.0)
                addItemToLabel.textColor = NSColor.white
                
                
                
            } else if (currentStyle.rawValue == "Light") {
                
 
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColor.cgColor
                
                RefreshButton.styleButtonText(button: RefreshButton, buttonName: "Refresh", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                StartButtonAction.styleButtonText(button: StartButtonAction, buttonName: "Start", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                StopButtonAction.styleButtonText(button: StopButtonAction, buttonName: "Stop", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                InstructionButton.styleButtonText(button: InstructionButton, buttonName: "Instructions", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                MobileAddButton.styleButtonText(button: MobileAddButton, buttonName: "Add", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                MobileCloseButton.styleButtonText(button: MobileCloseButton, buttonName: "Close", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                RefreshButton.wantsLayer = true
                RefreshButton.layer?.backgroundColor = NSColor.gray.cgColor
                RefreshButton.layer?.cornerRadius = 7
                
                StartButtonAction.wantsLayer = true
                StartButtonAction.layer?.backgroundColor = NSColor.gray.cgColor
                StartButtonAction.layer?.cornerRadius = 7
                
                StopButtonAction.wantsLayer = true
                StopButtonAction.layer?.backgroundColor = NSColor.gray.cgColor
                StopButtonAction.layer?.cornerRadius = 7
                
                InstructionButton.wantsLayer = true
                InstructionButton.layer?.backgroundColor = NSColor.gray.cgColor
                InstructionButton.layer?.cornerRadius = 7
                
                MobileAddButton.wantsLayer = true
                MobileAddButton.layer?.backgroundColor = NSColor.gray.cgColor
                MobileAddButton.layer?.cornerRadius = 7
                
                MobileCloseButton.wantsLayer = true
                MobileCloseButton.layer?.backgroundColor = NSColor.gray.cgColor
                MobileCloseButton.layer?.cornerRadius = 7
                
                addItemToLabel.font = NSFont(name:AppFontBold, size: 13.0)
                addItemToLabel.textColor = NSColor.white
                
            }
        }
        
        CurrentAppearanceMobileViewController = "\(currentStyle)"
        
        
    }//end of appearanceCheck()
    
    
    
    
    
    
    
    
    
    func RefreshReceivedTable() {
        
        
        ReceivedTableCount()
        
        ReceivedTableInformation()
        
        
    }



    
    
    func ReceivedTableCount() {
        
        db.open()
        
        
        let CountQuery = "select COUNT(ikey) as cnt from mobiletable where userikeyreference = \"\(CurrentUserIkey!)\""
        
        do {
            
            let COUNTQueryRun = try db.executeQuery(CountQuery, values:nil)
            
            while COUNTQueryRun.next() {
                RecTableCount = COUNTQueryRun.int(forColumn: "cnt")
                print("ReceivedTableCount RecTableCount Count: \(RecTableCount)")
                
            }
            
        } catch {
            
            ////print("ERROR: Unable to get count in the MobileTable\n\(CountQuery)\n\(db.lastErrorMessage())")
            
            let ErrorMessage = "MobileViewCongtroller:\(#function) - Unable to get count in the MobileTable\n\(CountQuery)\n\(db.lastErrorMessage())"
            
            //print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "MobileViewCongtroller", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Unable to get count in the MobileTable\n\(CountQuery)\n\nPlease Quit and reopen ScriptoPortfolio", DatabaseError: "\(db.lastErrorMessage())")
            
        }
        
    }
    
    
    func ReceivedTableInformation() {
        
        
        MobileListArray.removeAll()
        
        
        // `type` TEXT,
        
//        `characterfirstname` TEXT,
//        `charactermiddlename` TEXT,
//        `characterlastname` TEXT,
//        `characterbirthdate` TEXT,
//        `charactersummary` TEXT,
//
//        `storyname` TEXT,
//        `storytext` TEXT,
//
//        `locationname` TEXT,
//        `locationaddress` TEXT,
//        `locationtext` TEXT,
//
//        `storyideaname` TEXT,
//        `storyideatype` TEXT,
//        `storyideatext` TEXT
        
        
        
        var IkeyDB: String!
        var TypeDB: String!
        
        var CharacterFNameDB: String?
        var CharacterMNameDB: String?
        var CharacterMLameDB: String?
        //var CharacterBDateDB: String?
        //var CharacterSummaryDB: String?
        
        var StoryNameDB: String?
        //var StoryTextDB: String?
        
        var LocationNameDB: String?
        //var LocationAddressDB: String?
        //var LocationTextDB: String?
        
        var StoryIdeaNameDB: String?
        var StoryIdeaTypeDB: String?
        //var StoryIdeaTextDB: String?
        
        var ItemName: String?
//        var ItemComp: String?
//        var ItemHistory: String?
        
        
        
        
        
        let Query = "select * from mobiletable where userikeyreference = \(CurrentUserIkey!) order by type"
        

        
        do {
            
            let QueryRun = try db.executeQuery(Query, values:nil)
            
            while QueryRun.next() {
                
                TypeDB = QueryRun.string(forColumn: "type")
                
                
                if (TypeDB == "Character") {
                    
                    IkeyDB = QueryRun.string(forColumn: "ikey")
                    CharacterFNameDB = QueryRun.string(forColumn: "characterfirstname")
                    CharacterMNameDB = QueryRun.string(forColumn: "charactermiddlename")
                    CharacterMLameDB = QueryRun.string(forColumn: "characterlastname")
  
                    let FullCharacterName: String
                    
                    if (CharacterMNameDB?.count == 0) {
                        FullCharacterName = "\(CharacterFNameDB ?? "BLANK") \(CharacterMLameDB ?? "BLANK")"
                    } else {
                        FullCharacterName = "\(CharacterFNameDB ?? "BLANK") \(CharacterMNameDB ?? "BLANK") \(CharacterMLameDB ?? "BLANK")"
                    }
                    
                    let MobileInfoToAdd = MobileCategoryInformation.init(Ikey: IkeyDB!, Name: FullCharacterName, Category: TypeDB)
                    
                    MobileListArray.append(MobileInfoToAdd)

                } else if (TypeDB == "Story Idea") {
                    
                    IkeyDB = QueryRun.string(forColumn: "ikey")
                    StoryIdeaNameDB = QueryRun.string(forColumn: "storyideaname")
                    StoryIdeaTypeDB = QueryRun.string(forColumn: "storyideatype")
                    
                    let StoryInfoName = "\(StoryIdeaTypeDB ?? "BLANK")-\(StoryIdeaNameDB ?? "BLANK")"

                    let MobileInfoToAdd = MobileCategoryInformation.init(Ikey: IkeyDB!, Name: StoryInfoName, Category: TypeDB)
                    
                    MobileListArray.append(MobileInfoToAdd)
                    
                } else if (TypeDB == "Story") {
                    
                    IkeyDB = QueryRun.string(forColumn: "ikey")
                    StoryNameDB = QueryRun.string(forColumn: "storyname")
                    
                    ////print("Ikey \"\(IkeyDB!)\" StoryName: \"\(StoryNameDB!)\" ")
                
                    let MobileInfoToAdd = MobileCategoryInformation.init(Ikey: IkeyDB!, Name: StoryNameDB!, Category: TypeDB)
                    
                    MobileListArray.append(MobileInfoToAdd)
                    
                    
                } else if (TypeDB == "Location") {
                    
                    IkeyDB = QueryRun.string(forColumn: "ikey")
                    LocationNameDB = QueryRun.string(forColumn: "locationname")
                    
                    let MobileInfoToAdd = MobileCategoryInformation.init(Ikey: IkeyDB!, Name: LocationNameDB!, Category: TypeDB)
                    
                    MobileListArray.append(MobileInfoToAdd)
                    
                } else if (TypeDB == "Item") {
                    
                    IkeyDB = QueryRun.string(forColumn: "ikey")
                    ItemName = QueryRun.string(forColumn: "itemname")
 
                    
                    let MobileInfoToAdd = MobileCategoryInformation.init(Ikey: IkeyDB!, Name: ItemName!, Category: TypeDB)
                    
                    MobileListArray.append(MobileInfoToAdd)
                    
                } else {
                    
                    //print("ERROR: TypeDB")
                    loggly(LogType.Error, text: "ERROR: UNKNOWN OR INVALID - Information Type sent from Scripto Idea")
                    
                    DoAlert.DisplayAlert(Class: "MobileViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "ERROR: UNKNOWN OR INVALID - Information Type sent from Scripto Idea\n\n\(Query)", DatabaseError: "")
                    
                }
                
                
                
                

                /////////////////
                

                
                
            }
            
        } catch {
            
            //print("ERROR: MobileViewController: Failed to get MobileTableInformation for Table n\(Query)\n\(db.lastErrorMessage())")
            
            let ErrorMessage = "MobileViewController:\(#function) - Failed to get MobileTableInformation for Table\n\(db.lastErrorMessage())\n\(Query))"
            
            //print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "MobileViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to get MobileTableInformation for Tableview\n\n\(Query)", DatabaseError: "\(db.lastErrorMessage())")
            
        }
        
        
        
        receivedInformationTable.reloadData()
        
        
        
        
        
    }
    
    
    
    
    
    
    
    
    
    // MARK: Bonjour server delegates
    
    
    func writeToDB() {
        
        // From ScriptoIdea iPhone app
        // "Character"
        // "Story"
        // "Story Idea"
        // "Location"
        
        ConnectionTest.CheckConnection()
        
        print("dicFromData: \"\(dicFromData)\"")
        
        let Category: String = dicFromData["ideaTypeLabel"]!
        

       // //print("\n\n\n\n")
        
        
        if (Category == "Character") {
            ////print("Character Category FOUND")
            print("dicFromData: \"\(dicFromData)\"")
            
            ////////////////
            
            

                let QueryUpdate = String(format: "INSERT INTO mobiletable (ikey,userikeyreference,type,characterfirstname,charactermiddlename,characterlastname,charactersummary,characterbirthdate) values ((select MAX(ikey)+ 1 from mobiletable),\"\(CurrentUserIkey!)\", \"Character\", \"\(dicFromData["firstname"]!)\", \"\(dicFromData["middlename"]!)\", \"\(dicFromData["lastname"]!)\", \"\(dicFromData["charactersummary"] ??  "")\", \"\(dicFromData["birthdate"] ?? "")\" )")
                
                print("Character QueryUpdate: \n\"\(QueryUpdate)\" ")
            
            
                
                let Success = db.executeStatements(QueryUpdate)
                

                
                if (!Success) {
                    
                    //print("Error: \(db.lastErrorMessage())")
                    
                    
                    loggly(LogType.Error, text: "\(db.lastErrorMessage())" )
                    
                    DoAlert.DisplayAlert(Class: "MobileViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to Insert \"\(dicFromData["ideaTypeLabel"]!)\"\n\n ", DatabaseError: "\n\(QueryUpdate)\n\n\(db.lastErrorMessage())\n")
                    return
                    
                }
                
  
            
            //////////////////////////////

            // Below is testing to make sure it was written to the DB correctly

//            var typeDB: String?
//            var FNDB: String?
//            var MNDB: String?
//            var LNDB: String?
//            var BDDB: String?
//            var SUMDB: String?
//
//            let DBUserQuery = "SELECT * FROM mobiletable where ikey = (select MAX(ikey) from mobiletable)"
//
//            //print("DBUserQuery: \"\(DBUserQuery)\"")
//
//            do {
//                let UserQuery = try db.executeQuery(DBUserQuery, values:nil)
//
//                while UserQuery.next() {
//
//                    typeDB = UserQuery.string(forColumn: "type")
//                    FNDB = UserQuery.string(forColumn: "characterfirstname")
//                    MNDB = UserQuery.string(forColumn: "charactermiddlename")
//                    LNDB = UserQuery.string(forColumn: "characterlastname")
//                    BDDB = UserQuery.string(forColumn: "characterbirthdate")
//                    SUMDB = UserQuery.string(forColumn: "charactersummary")
//
//                    //print("typeDB: \"\(typeDB)\" Firstname: \"\(FNDB)\" Middlename: \"\(MNDB)\" Lastname: \"\(LNDB)\"  Birthdate: \"\(BDDB)\" CharacterSummary: \"\(SUMDB)\"")
//
//                }
//
//            } catch {
//                //print("ERROR: \(db.lastErrorMessage())")
//                loggly(LogType.Error, text: "ViewController-\(#function) Query Failed: \"\(DBUserQuery)\"")
//                DoAlert.DisplayAlert(Class: "ViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"\(DBUserQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
//            }

            //////////////////////////////
            
            
            
            
        } else if (Category == "Story") {
            ////print("Story Category FOUND")
            ////print("dicFromData: \"\(dicFromData)\"")
            
            ////////////////
            
            // SendDictionary = ["ideaTypeLabel":"Story","name":"\(PullStoryName)","storytext":"\(PullStoryText)"]
            
            
            
            let QueryUpdate = String(format: "INSERT INTO mobiletable (ikey,userikeyreference,type,storyname,storytext) values ((select MAX(ikey)+ 1 from mobiletable),\"\(CurrentUserIkey!)\", \"Story\", \"\(dicFromData["name"] ?? "Blank")\", \"\(dicFromData["storytext"] ?? "Blank")\" )")
            
            
            
            //print("dicFromData: \n\(dicFromData)")
            //print("Story QueryUpdate: \n\"\(QueryUpdate)\" ")
            
            
            
            let Success = db.executeStatements(QueryUpdate)
            
            
            
            if (!Success) {
                
                //print("Error: \(db.lastErrorMessage())")
                
                
                loggly(LogType.Error, text: "\(db.lastErrorMessage())" )
                
                DoAlert.DisplayAlert(Class: "MobileViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to Insert \"\(dicFromData["ideaTypeLabel"]!)\"\n\n ", DatabaseError: "\(db.lastErrorMessage())\n\n\(QueryUpdate)")
                return
                
            }
            
            
            
            //////////////////////////////
            
            
        } else if (Category == "Story Idea") {
            ////print("Story Idea Category FOUND")
            ////print("dicFromData: \"\(dicFromData)\"")
            
            ////////////////
            
            // SendDictionary = ["ideaTypeLabel":"Story Idea","name":"\(PullStoryIdeaName)","storyideatext":"\(PullStoryIdeaText)","storyideatype":"\(PullStoryIdeaType)"]
            
            let QueryUpdate = String(format: "INSERT INTO mobiletable (ikey,userikeyreference,type,storyideaname,storyideatype,storyideatext) values ((select MAX(ikey)+ 1 from mobiletable),\"\(CurrentUserIkey!)\", \"Story Idea\", \"\(dicFromData["name"] ?? "Blank")\", \"\(dicFromData["storyideatype"] ?? "Blank")\", \"\(dicFromData["storyideatext"] ?? "Blank")\" )")
            
            ////print("Story Idea QueryUpdate: \n\"\(QueryUpdate)\" ")
            
 
            
            let Success = db.executeStatements(QueryUpdate)
            
            
            
            if (!Success) {
                
                ////print("Error: \(db.lastErrorMessage())")
                
                
                loggly(LogType.Error, text: "\(db.lastErrorMessage())" )
                
                DoAlert.DisplayAlert(Class: "MobileViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to Insert \"\(dicFromData["ideaTypeLabel"]!)\"\n\n ", DatabaseError: "\(db.lastErrorMessage())\n\n\(QueryUpdate)")
                return
                
            }
            
            
            
            //////////////////////////////
            
            
        } else if (Category == "Location") {
            ////print("Location Category FOUND")
            ////print("dicFromData: \"\(dicFromData)\"")
            
            ////////////////
            
            // SendDictionary = ["ideaTypeLabel":"Location","name":"\(PullLocationName)","locationtext":"\(PullLocationText)","locationaddress":"\(PullLocationAddressText)"]
            
            let QueryUpdate = String(format: "INSERT INTO mobiletable (ikey,userikeyreference,type,locationname,locationaddress,locationtext) values ((select MAX(ikey)+ 1 from mobiletable),\"\(CurrentUserIkey!)\", \"Location\", \"\(dicFromData["name"] ?? "Blank")\", \"\(dicFromData["locationaddress"] ?? "Blank")\", \"\(dicFromData["locationtext"] ?? "Blank")\" )")
            
            ////print("Location QueryUpdate: \n\"\(QueryUpdate)\" ")
            
            
            
            let Success = db.executeStatements(QueryUpdate)
            
            
            
            if (!Success) {
                
                ////print("Error: \(db.lastErrorMessage())")
                
                
                loggly(LogType.Error, text: "\(db.lastErrorMessage())" )
                
                DoAlert.DisplayAlert(Class: "MobileViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to Insert \"\(dicFromData["ideaTypeLabel"]!)\"\n\n ", DatabaseError: "\(db.lastErrorMessage())\n\n\(QueryUpdate)")
                return
                
            }
            
            
            
            //////////////////////////////
            
            
        } else if (Category == "Item") {
            ////print("Location Category FOUND")
            ////print("dicFromData: \"\(dicFromData)\"")
            
            ////////////////
            
            // SendDictionary = ["ideaTypeLabel":"Location","name":"\(PullLocationName)","locationtext":"\(PullLocationText)","locationaddress":"\(PullLocationAddressText)"]
            
            let QueryUpdate = String(format: "INSERT INTO mobiletable (ikey,userikeyreference,type,itemname,itemcomp,itemhistory) values ((select MAX(ikey)+ 1 from mobiletable),\"\(CurrentUserIkey!)\", \"Item\", \"\(dicFromData["name"] ?? "Blank")\", \"\(dicFromData["itemcomptext"] ?? "Blank")\", \"\(dicFromData["itemhistorytext"] ?? "Blank")\" )")
            
            ////print("Location QueryUpdate: \n\"\(QueryUpdate)\" ")
            
            
            
            let Success = db.executeStatements(QueryUpdate)
            
            
            
            if (!Success) {
                
                ////print("Error: \(db.lastErrorMessage())")
                
                
                loggly(LogType.Error, text: "\(db.lastErrorMessage())" )
                
                DoAlert.DisplayAlert(Class: "MobileViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to Insert \"\(dicFromData["ideaTypeLabel"]!)\"\n\n ", DatabaseError: "\(db.lastErrorMessage())\n\n\(QueryUpdate)")
                return
                
            }
            
            
            
            //////////////////////////////
            
            
        } else {
            
            ////print("Category: \"\(Category)\"")
            
            
            loggly(LogType.Error, text: "MobileViewController:\(#function) - Unknown Or Invalid infromation received - Possible the information received was not a Dictionaryx. Please quit both apps and try to send it again.\n")
            
            DoAlert.DisplayAlert(Class: "MobileViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Unknown Or Invalid infromation found. Please quit both apps and try to send it again. ", DatabaseError: "Unknown Category: \"\(Category)\"")

            dicFromData.removeAll()
            
        }
        
        
        ////print("\n\n\n\n")
        
        
        dicFromData.removeAll()
        
        ////print("writeToDB() RAN")
        
        
        
        
        
        
        //////////////////////////
        
        
        
        
        
        
        
        
        
        
        
    }// end of Function
    
    
    
    func ButtonServiceStopped() {
        
        self.ConnectionLabel.stringValue = "Serivce Stopped"
        self.ConnectionLabel.textColor = NSColor.red
        
        //RefreshButton.isEnabled = false
        //StartButtonAction.isEnabled = true
        //StopButtonAction.isEnabled = false
        
        RefreshReceivedTable()
        //print("ButtonServiceStopped() RAN - RefreshReceivedTable() RAN ")

        
    }
    
    func ButtonServiceStarted() {
        
        self.ConnectionLabel.stringValue = "Serivce Running"
        self.ConnectionLabel.textColor = NSColor.green
        
        
    }
    
    
    func ServiceStartERROR(ErrorString: String, ErrorDict: String) {
        
        loggly(LogType.Error, text: "MobileViewController:\(#function) - \n\(ErrorString)\n\(ErrorDict)")
        
        DoAlert.DisplayAlert(Class: "MobileViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "\(ErrorString)", DatabaseError: "\(ErrorDict)")
        
    }
    
    
    func didChangeServices() {
        //print("didChangeServices() called to RELOAD TABLE DATA")
        self.tableView.reloadData()
        
//        RefreshButton.isEnabled = false
//        StartButtonAction.isEnabled = true
//        StopButtonAction.isEnabled = false
        
        
        
    }
    
    func connected() {
        
    }
    
    func disconnected() {
        //print("disconnected()")
        //bonjourServer.stopBrowsing()
        
        
    }
    
    func handleBody(_ body: NSString?) {
        //self.readLabel.stringValue = body! as String
        
    }
    
    
    ///////////////////////////////////
    // MARK: TableView Delegates
    
    func numberOfRows(in aTableView: NSTableView) -> Int {
        
        
        //if (aTableView == receivedInformationTable) {
            //ReceivedTableCount()
            ////print("RecTableCount count: \(RecTableCount)")
        //}
        
        ///////////////////////////////////////
        
        var count = Int()

        if (aTableView == tableView) {
            
            if (self.bonjourServer == nil) {
                count = 0
            } else {
                count = bonjourServer.devices.count
            }
            //print("tableView count: bonjourServer.devices.count: \(count)")
        }
        
        if (aTableView == receivedInformationTable) {
            ReceivedTableCount()
            count = Int(RecTableCount)
            print("receivedInformationTable count: \(count)")
            
            
        }
        
        
        return count
    }
    
    
    
    func tableView(_ CurrentTableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        
        guard let cell = CurrentTableView.makeView(withIdentifier: tableColumn!.identifier, owner: self) as? NSTableCellView else
        {
            //print("Table Returned Nil")
            return nil
        }
        
//        if (CurrentTableView == receivedInformationTable) {
//            //print("CurrentTableView = receivedInformationTable")
//        } else if (CurrentTableView == tableView) {
//            //print("CurrentTableView = tableView")
//        }
        
        
        
        
        
        if (CurrentTableView == receivedInformationTable) {
            
            
            
            if (tableColumn?.identifier)!.rawValue == "ikey" {
                cell.textField?.stringValue = MobileListArray[row].Ikey
            }
            if (tableColumn?.identifier)!.rawValue == "name" {
                cell.textField?.stringValue = MobileListArray[row].Name
            }
            if (tableColumn?.identifier)!.rawValue == "category" {
                cell.textField?.stringValue = MobileListArray[row].Category
            }

            
        }
        
        
        
        
        if (CurrentTableView == tableView) {
            let columnIdentifier = tableColumn!.identifier
            if columnIdentifier.rawValue == "bonjour-device" {
                let device = self.bonjourServer.devices[row]
                cell.textField?.stringValue = device.name
            }
        }

        
        
        return cell
        
    }
    
    
    
    
    
    

    
    func tableViewSelectionDidChange(_ notification: Notification) {
        
        
        let CurrentTable = notification.object as! NSTableView
        
        if (CurrentTable == tableView) {
            
            //print("notification: \(String(describing: notification.userInfo))")
            //print("bonjourServer.devices.count: \"\(bonjourServer.devices.count)\"")
            
            if self.bonjourServer.devices.count == 0 {
                
            } else {
                //print("self.tableView.selectedRow: \"\(self.tableView.selectedRow)\"")
                
                let service = self.bonjourServer.devices[self.tableView.selectedRow]
                self.bonjourServer.connectTo(service)
            }
            
        }
        
        if (CurrentTable == receivedInformationTable) {
            
            let SelectedRow = receivedInformationTable.selectedRow
            
            //print("SelectedRow: \(SelectedRow)")
            
            if (SelectedRow == -1) {
                addItemToLabel.stringValue = ""
                return
            }
            
//            let Name = MobileListArray[SelectedRow].Name
//            let Ikey = MobileListArray[SelectedRow].Ikey
//
//            //print("Table Selection - SelectedRow: \"\(SelectedRow)\" Ikey: \"\(Ikey)\" Name: \"\(Name)\"")
            
            let isIndexValid = MobileListArray.indices.contains(SelectedRow)
            
            if (isIndexValid) {
                addItemToLabel.stringValue = "Add \"\(MobileListArray[SelectedRow].Category) - \(MobileListArray[SelectedRow].Name)\" to \(MobileListArray[SelectedRow].Category)'s ?"
                receivedInformationTableDeleteButton.isEnabled = true
                
            } else {
                addItemToLabel.stringValue = ""
            }
            
        }
            
        
        
        
        
    } // end of tableViewSelectionDidChange()
    
    
    
    @IBAction func receivedInformationTableDeleteButtonAction(_ sender: Any) {
        
        let SelectedRow = receivedInformationTable.selectedRow
        
        if (SelectedRow == -1) {
            return
        }
        
        let Name = MobileListArray[SelectedRow].Name
        
        let alert = NSAlert()
        alert.messageText = "Delete \"\(Name)\" forever?"
        alert.informativeText = "Do you want to Delete it?"
        alert.alertStyle = .warning
        alert.window.titlebarAppearsTransparent = true
        
        if (CurrentAppearanceMobileViewController == "Dark") {
            alert.window.backgroundColor = AlertBackgroundColorDarkMode
        } else {
            alert.window.backgroundColor = WindowBackgroundColor
        }
        
        alert.addButton(withTitle: "Cancel")
        alert.addButton(withTitle: "Delete")
        
        
        let returnCode = alert.runModal()
        print("returnCode: \(returnCode)")
        
        if (returnCode.rawValue == 1001) {
            print("OK Pressed")
            
            
            let SelectedRow = receivedInformationTable.selectedRow
            
            //print("SelectedRow: \(SelectedRow)")
            
            if (SelectedRow == -1) {
                return
            }
            
            
            let Ikey = MobileListArray[SelectedRow].Ikey
            
            
            //print("Table Selection - SelectedRow: \"\(SelectedRow)\" Ikey: \"\(Ikey)\" Name: \"\(Name)\"")
            
            let isIndexValid = MobileListArray.indices.contains(SelectedRow)
            
            if (isIndexValid) {
                //print("IS GOING TO DELETE")
                
                var DeleteQuery = String()
                
                
                DeleteQuery = "DELETE FROM mobiletable WHERE ikey = \"\(Ikey)\""
                
                print("UpdateQuery: \(DeleteQuery)")
                
                do {
                    
                    try db.executeUpdate(DeleteQuery, values:nil)

                    
                } catch {
                    print("ERROR: \(db.lastErrorMessage())")
                    loggly(LogType.Error, text: "MobileViewController-\(#function) Query Failed: \"\(DeleteQuery)\"")
                    DoAlert.DisplayAlert(Class: "MobileViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"\(DeleteQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                    return
                }
                
                ReceivedTableInformation()
                addItemToLabel.stringValue = ""
                
            } else {
                //print("NOT GOING TO DELETE - INVALID")
            }
            
            
            receivedInformationTable.deselectRow(SelectedRow)
            
            receivedInformationTableDeleteButton.isEnabled = false
            
            
        }
        if (returnCode.rawValue == 1000) {
            
        }
        
        
        
        
    
        
    }// end of receivedInformationTableDeleteButtonAction()
    
    
    @IBAction func AddItemButtonAction(_ sender: Any) {
        
        // \"\(MobileListArray[SelectedRow].Category) - \(MobileListArray[SelectedRow].Name)\"
        
        let SelectedRow = receivedInformationTable.selectedRow
        
        if (SelectedRow == -1) {
            let alert = NSAlert()
            alert.messageText = "No Item Selected to Add"
            alert.informativeText = "Please Select an Items first"
            alert.alertStyle = .warning
            alert.window.titlebarAppearsTransparent = true
            
            if (CurrentAppearanceMobileViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.runModal()
            return
        }
        
        
        
        let Name = MobileListArray[SelectedRow].Name
        let Category = MobileListArray[SelectedRow].Category
        let Ikey = MobileListArray[SelectedRow].Ikey
        
        let alert = NSAlert()
        alert.messageText = "Do you want to add \"\(Name)\" to your \(Category)'s?"
        alert.informativeText = ""
        alert.alertStyle = .warning
        alert.window.titlebarAppearsTransparent = true
        
        if (CurrentAppearanceMobileViewController == "Dark") {
            alert.window.backgroundColor = AlertBackgroundColorDarkMode
        } else {
            alert.window.backgroundColor = WindowBackgroundColor
        }
        
        alert.addButton(withTitle: "Save")
        alert.addButton(withTitle: "Cancel")

        let returnCode = alert.runModal()
        //print("returnCode: \(returnCode)")

        if (returnCode.rawValue == 1000) {
            //print("Save Pressed")
            
            /////////////////////
            
            
            
            
            
            var IkeyDB: String!
            var TypeDB: String!
            
            var CharacterFNameDB: String?
            var CharacterMNameDB: String?
            var CharacterLNameDB: String?
            var CharacterBDateDB: String?
            var CharacterSummaryDB: String?
            
            var StoryNameDB: String?
            var StoryTextDB: String?
            
            var LocationNameDB: String?
            var LocationAddressDB: String?
            var LocationTextDB: String?
            
            var StoryIdeaNameDB: String?
            var StoryIdeaTypeDB: String?
            var StoryIdeaTextDB: String?
            
            var ItemNameDB: String?
            var ItemCompoDB: String?
            var ItemHistoryDB: String?
            
            
            //////////////////////////////////////////
            
            let Query = "select * from mobiletable where userikeyreference = \(CurrentUserIkey!) AND ikey = \(Ikey) "
            print("Query: \"\(Query)\" ")

            do {
                
                let QueryRun = try db.executeQuery(Query, values:nil)
                
                while QueryRun.next() {
                    
                    TypeDB = QueryRun.string(forColumn: "type")
                    
                    if (TypeDB == "Character") {
                        
                        IkeyDB = QueryRun.string(forColumn: "ikey")
                        CharacterFNameDB = QueryRun.string(forColumn: "characterfirstname")
                        CharacterMNameDB = QueryRun.string(forColumn: "charactermiddlename")
                        CharacterLNameDB = QueryRun.string(forColumn: "characterlastname")
                        CharacterBDateDB = QueryRun.string(forColumn: "characterbirthdate")
                        CharacterSummaryDB = QueryRun.string(forColumn: "charactersummary")

                    } else if (TypeDB == "Story Idea") {
                        
                        IkeyDB = QueryRun.string(forColumn: "ikey")
                        StoryIdeaNameDB = QueryRun.string(forColumn: "storyideaname")
                        StoryIdeaTypeDB = QueryRun.string(forColumn: "storyideatype")
                        StoryIdeaTextDB = QueryRun.string(forColumn: "storyideatext")
                        
                    } else if (TypeDB == "Story") {
                        
                        IkeyDB = QueryRun.string(forColumn: "ikey")
                        StoryNameDB = QueryRun.string(forColumn: "storyname")
                        StoryTextDB = QueryRun.string(forColumn: "storytext")
                        
                    } else if (TypeDB == "Location") {
                        
                        IkeyDB = QueryRun.string(forColumn: "ikey")
                        LocationNameDB = QueryRun.string(forColumn: "locationname")
                        LocationAddressDB = QueryRun.string(forColumn: "locationaddress")
                        LocationTextDB = QueryRun.string(forColumn: "locationtext")
                        
                    } else if (TypeDB == "Item") {
                        
                        IkeyDB = QueryRun.string(forColumn: "ikey")
                        ItemNameDB = QueryRun.string(forColumn: "itemname")
                        ItemCompoDB = QueryRun.string(forColumn: "itemcomp")
                        ItemHistoryDB = QueryRun.string(forColumn: "itemhistory")
                        
                    } else {
                        
                        //print("ERROR: TypeDB")
                        loggly(LogType.Error, text: "ERROR: UNKNOWN OR INVALID - Information Type sent from Scripto Idea")
                        
                        DoAlert.DisplayAlert(Class: "MobileViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "ERROR: UNKNOWN OR INVALID - Information Type sent from Scripto Idea\n\n\(Query)", DatabaseError: "")
                        
                    }
                    
                    
                    
                    
                    
                    /////////////////
                    
                    
                    
                    
                }
                
            } catch {
                
                //print("ERROR: MobileViewController: Failed to get MobileTableInformation for Table n\(Query)\n\(db.lastErrorMessage())")
                
                let ErrorMessage = "MobileViewController:\(#function) - Failed to get information from MobileTableInformation to Add Item\n\(db.lastErrorMessage())\n\(Query))"
                
                //print(ErrorMessage)
                
                loggly(LogType.Error, text: ErrorMessage)
                
                DoAlert.DisplayAlert(Class: "MobileViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to get information from MobileTableInformation to Add Item\n\n\(Query)", DatabaseError: "\(db.lastErrorMessage())")
                
                return
                
            }
            
            
            /////////////////////
            
            
            if (Category == "Character") {
                ////print("Character Category FOUND")
                ////print("dicFromData: \"\(dicFromData)\"")
                
                ////////////////
                
                
                let dateFormatterGet = DateFormatter()
                dateFormatterGet.dateFormat = "YYYY-MM-DD HH:MM:SS.SSS"
                
                let BirthdateDB = "2018-07-22 16:20:41 +0000"
                
                let Bdate: NSDate? = dateFormatterGet.date(from: BirthdateDB) as NSDate?
                
                

                ////////////////

                let NewCharaterQuery = String(format: "INSERT INTO charactertable (ikey,userikeyreference,firstname,middlename,lastname,birthdate,heightfeet,heightinches,weight,gender,ethnicity,birthplace,orientation,haircolor,facialcolor,skintone,charactersummary,haircut,hairpart,hairlength,facialhair,facialhairother) VALUES ((select MAX(ikey)+ 1 from charactertable),\"\(CurrentUserIkey!)\",\"\(CharacterFNameDB ?? "Blank")\",\"\(CharacterMNameDB ?? "Blank")\",\"\(CharacterLNameDB ?? "Blank")\",\"\(CharacterBDateDB ?? "Blank")\",\"0\",\"0\",\"0\",\"1\",\"Ethnicity\",\"\",\"\",\"\",\"\",\"\",\"\(CharacterSummaryDB ?? "Blank")\",\"\",\"\",\"\",\"\",\"\")")

                print("NewCharaterQuery: \(NewCharaterQuery)")

                let Success = db.executeStatements(NewCharaterQuery)
                
                if (!Success) {
                    
                    //print("Error: \(db.lastErrorMessage())")

                    loggly(LogType.Error, text: "\(db.lastErrorMessage())" )
                    
                    DoAlert.DisplayAlert(Class: "MobileViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to Insert \"Character\"\n\n ", DatabaseError: "\n\(NewCharaterQuery)\n\n\(db.lastErrorMessage())\n")
                   
                    return
                    
                }
                
                
            } else if (Category == "Story") {
                ////print("Story Category FOUND")
                ////print("dicFromData: \"\(dicFromData)\"")
                
                
                
                ////////////////////////////
                
                ConnectionTest.CheckConnection()
                
                ConnectionTest.OpenDB()
                
                
                let DupStoryCheck = "SELECT count(ikey) as count FROM storytable WHERE storyname = \"\(StoryNameDB!)\" and userikeyreference in (select ikey from users where user = \"\(CurrentUserName!)\")"
                
                print("DupStoryCheck: \(DupStoryCheck)")
                
                var NumCount:Int32 = 0
                
                do {
                    let result = try db.executeQuery(DupStoryCheck, values: nil)
                    
                    while result.next()
                    {
                        NumCount = result.int(forColumn: "count")
                        print("NumCount: \(NumCount)")
                    }
                    
                } catch let error as NSError {
                    
                    let ErrorMessage = "\(#function) - Failed to do query \(DupStoryCheck)\nError: \(error.localizedDescription)"
                    
                    print(ErrorMessage)
                    
                    
                    
                    DoAlert.DisplayAlert(Class: "ViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to check the Scripto Portfolio database for username.\nPlease quit then try again.", DatabaseError: "\(DupStoryCheck)\n\n\(db.lastErrorMessage())")
                }
                
                
                if (NumCount > 0) {
                    
                   // I cant have any stories with same name becuase of the way I pull them from a rtf file
                    
                    
                    let alert = NSAlert()
                    alert.messageText = "Warning: Another of your Stories has that name"
                    alert.informativeText = "Do you want to name this the same ?"
                    alert.alertStyle = .warning
                    alert.addButton(withTitle: "Cancel")
                    alert.addButton(withTitle: "Yes")
                    
                    if (CurrentAppearanceMobileViewController == "Dark") {
                        alert.window.backgroundColor = AlertBackgroundColorDarkMode
                    } else {
                        alert.window.backgroundColor = WindowBackgroundColor
                    }
                    
                    let resultCode: Float = Float(alert.runModal().rawValue)
                    
                    if (resultCode == 1000) {
                        //print("Cancel Pressed")
                        return
                    }
                    
                    if (resultCode == 1001) {
                        //print("Yes Pressed")
                        
                    }
                    
                }
                
                //////////////////////////////////
                
                ////////////////
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MM/dd/yyyy"
                
                let dateString = dateFormatter.string(from: Date())
                
                //////////////
                
                // StoriesLocation
                let PathToStory = "\(StoriesLocation)/\(StoryNameDB!).rtf"
                
                ////////////////

                let QueryUpdate = String(format: "INSERT INTO storytable (ikey,userikeyreference,storyname,dateoflastedit,createdate,pathtostory) VALUES ((select MAX(ikey)+ 1 from storytable),\(CurrentUserIkey!),\"\(StoryNameDB!)\",\"\(dateString)\",\"\(dateString)\",\"\(PathToStory)\")")
                
                print("Story QueryUpdate: \n\"\(QueryUpdate)\" ")
                
                
                
                let Success = db.executeStatements(QueryUpdate)
                
                
                
                if (!Success) {
                    
                    //print("Error: \(db.lastErrorMessage())")
                    
                    
                    loggly(LogType.Error, text: "\(db.lastErrorMessage())" )
                    
                    DoAlert.DisplayAlert(Class: "MobileViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to Insert \"\(dicFromData["ideaTypeLabel"]!)\"\n\n ", DatabaseError: "\(db.lastErrorMessage())\n\n\(QueryUpdate)")
                    return
                    
                }
                
                //////////////////////////////////
                

                do {
                    
                    print("PathToStory: \(PathToStory)")
                    
                    let exportedFileURL = URL(fileURLWithPath:"\(PathToStory)")
                    
                    
                    //let exportedFileURL = URL.init(string: "file://\(PathToStory)")
                    
                    print("exportedFileURL: \(exportedFileURL)")
                    
                        
                    let textDNA = NSAttributedString(string: StoryTextDB!)
                    
                    let r1 = StoryTextDB?.range(of: StoryTextDB!)
                    
                    // let range = NSRange(0..<1)
                    let range = NSRange(r1!, in: StoryTextDB!)
                    
                    let textTSave = try textDNA.fileWrapper(from: range, documentAttributes: [NSAttributedString.DocumentAttributeKey.documentType:NSAttributedString.DocumentType.rtf])
                    try textTSave.write(to: exportedFileURL as URL, options: FileWrapper.WritingOptions.atomic, originalContentsURL: nil)
                        
                    
                    
                } catch {
                    
                    loggly(LogType.Error, text: "ViewController:\(#function) - Failed to create \(StoryNameDB!).rtf at\n\(StoriesLocation)\n\(error.localizedDescription)")
                    
                    DoAlert.DisplayAlert(Class: "ViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to create \(StoryNameDB!).rtf.\nPlease quit then try again.", DatabaseError: "\n\n\(error)")
                    
                    return
                }
                
                
                
                
                
                
                
                
                //////////////////////////////
                
                
            } else if (Category == "Story Idea") {
                ////print("Story Idea Category FOUND")
                ////print("dicFromData: \"\(dicFromData)\"")
                
                ////////////////
                
                
//                StoryIdeaNameDB
//                StoryIdeaTypeDB
//                StoryIdeaTextDB
                
                
                // Choose an Option = 0
                // Arcs             = 1
                // Starts           = 2
                // Mids             = 3
                // Endings          = 4
                // Scenes           = 5
                
                var SelectedCategoryIndex = Int()
                
                if (StoryIdeaTypeDB == "Arcs") {
                    SelectedCategoryIndex = 1
                } else if (StoryIdeaTypeDB == "Starts") {
                    SelectedCategoryIndex = 2
                } else if (StoryIdeaTypeDB == "Mids") {
                    SelectedCategoryIndex = 3
                } else if (StoryIdeaTypeDB == "Endings") {
                    SelectedCategoryIndex = 4
                } else if (StoryIdeaTypeDB == "Scenes") {
                    SelectedCategoryIndex = 5
                } else {
                    SelectedCategoryIndex = 0
                }
                
                
                
                
                // SendDictionary = ["ideaTypeLabel":"Story Idea","name":"\(PullStoryIdeaName)","storyideatext":"\(PullStoryIdeaText)","storyideatype":"\(PullStoryIdeaType)"]
                
                let QueryUpdate = String(format: "INSERT INTO storyideastable (ikey,userikeyreference,categoryikey,name,storyvalue) VALUES ((select MAX(ikey)+ 1 from storyideastable),'\(CurrentUserIkey!)','\(SelectedCategoryIndex)','\(StoryIdeaNameDB!)\','\(StoryIdeaTextDB!)')")
                
                print("Story Idea QueryUpdate: \n\"\(QueryUpdate)\" ")
                
                
                
                let Success = db.executeStatements(QueryUpdate)
                
                
                
                if (!Success) {
                    
                    ////print("Error: \(db.lastErrorMessage())")
                    
                    
                    loggly(LogType.Error, text: "\(db.lastErrorMessage())" )
                    
                    DoAlert.DisplayAlert(Class: "MobileViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to Insert \"\(dicFromData["ideaTypeLabel"]!)\"\n\n ", DatabaseError: "\(db.lastErrorMessage())\n\n\(QueryUpdate)")
                    return
                    
                }
                
                
                
                //////////////////////////////
                
                
            } else if (Category == "Location") {
                ////print("Location Category FOUND")
                ////print("dicFromData: \"\(dicFromData)\"")
                
                ////////////////
                
                // SendDictionary = ["ideaTypeLabel":"Location","name":"\(PullLocationName)","locationtext":"\(PullLocationText)","locationaddress":"\(PullLocationAddressText)"]
                
                
//                LocationNameDB
//                LocationAddressDB
//                LocationTextDB
                
                let QueryUpdate = String(format: "INSERT INTO locationtable (ikey,name,location,notes,imagetableikeyreference,associatedstoryikey,userikeyreference) VALUES ((select MAX(ikey)+ 1 from locationtable),\"\(LocationNameDB!)\",\"\(LocationAddressDB!)\",\"\(LocationTextDB!)\", '' , '' ,\(CurrentUserIkey!))")
                
                ////print("Location QueryUpdate: \n\"\(QueryUpdate)\" ")
                
                
                
                let Success = db.executeStatements(QueryUpdate)
                
                
                
                if (!Success) {
                    
                    ////print("Error: \(db.lastErrorMessage())")
                    
                    
                    loggly(LogType.Error, text: "\(db.lastErrorMessage())" )
                    
                    DoAlert.DisplayAlert(Class: "MobileViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to Insert \"\(dicFromData["ideaTypeLabel"]!)\"\n\n ", DatabaseError: "\(db.lastErrorMessage())\n\n\(QueryUpdate)")
                    return
                    
                }
                
                
                
                //////////////////////////////
                
                
            } else if (Category == "Item") {
                ////print("Location Category FOUND")
                ////print("dicFromData: \"\(dicFromData)\"")
                
                ////////////////
                
                // SendDictionary = ["ideaTypeLabel":"Location","name":"\(PullLocationName)","locationtext":"\(PullLocationText)","locationaddress":"\(PullLocationAddressText)"]
                
                
                //                LocationNameDB
                //                LocationAddressDB
                //                LocationTextDB
                
                let QueryUpdate = String(format: "INSERT INTO itemtable (ikey,name,composition,history,userikeyreference) VALUES ((select MAX(ikey)+ 1 from itemtable),\"\(ItemNameDB!)\",\"\(ItemCompoDB!)\",\"\(ItemHistoryDB!)\",\(CurrentUserIkey!))")
                
                ////print("Location QueryUpdate: \n\"\(QueryUpdate)\" ")
                
                
                
                let Success = db.executeStatements(QueryUpdate)
                
                
                
                if (!Success) {
                    
                    ////print("Error: \(db.lastErrorMessage())")
                    
                    
                    loggly(LogType.Error, text: "\(db.lastErrorMessage())" )
                    
                    DoAlert.DisplayAlert(Class: "MobileViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to Insert \"\(dicFromData["ideaTypeLabel"]!)\"\n\n ", DatabaseError: "\(db.lastErrorMessage())\n\n\(QueryUpdate)")
                    return
                    
                }
                
                
                
                //////////////////////////////
                
                
            } else {
                
                ////print("Category: \"\(Category)\"")
                
                
                loggly(LogType.Error, text: "MobileViewController:\(#function) - Unknown Or Invalid infromation received - Possible the information received was not a Dictionaryx. Please quit both apps and try to send it again.\n")
                
                DoAlert.DisplayAlert(Class: "MobileViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Unknown Or Invalid infromation found. Please quit both apps and try to send it again. ", DatabaseError: "Unknown Category: \"\(Category)\"")
                
                dicFromData.removeAll()
                
            }
            
            
            
            
            
            
            
            
            
            
            
            
        }
        if (returnCode.rawValue == 1001) {
            //print("Cancel Pressed")
           return
        }
        
        
        ///////////////////////////////
        
        

        
        delayWithSeconds(0.5) {
            
            
            let alertPrompt = NSAlert()
            alertPrompt.messageText = "\"\(Name)\" moved to \(Category) area"
            alertPrompt.informativeText = "Do you want to remove \"\(Name)\" from the Mobile area ?"
            alertPrompt.alertStyle = .warning
            alertPrompt.window.titlebarAppearsTransparent = true
            
            if (self.CurrentAppearanceMobileViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alertPrompt.addButton(withTitle: "Cancel")
            alertPrompt.addButton(withTitle: "Remove")
            
            
            
            let resultCode1: Float = Float(alertPrompt.runModal().rawValue)
            
            if (resultCode1 == 1001) {
                print("Remove Pressed")
                
                let isIndexValid = self.MobileListArray.indices.contains(SelectedRow)
                
                if (isIndexValid) {
                    //print("IS GOING TO DELETE")
                    
                    var DeleteQuery = String()
                    
                    
                    DeleteQuery = "DELETE FROM mobiletable WHERE ikey = \"\(Ikey)\""
                    
                    print("UpdateQuery: \(DeleteQuery)")
                    
                    do {
                        
                        try db.executeUpdate(DeleteQuery, values:nil)
                        
                        
                    } catch {
                        print("ERROR: \(db.lastErrorMessage())")
                        loggly(LogType.Error, text: "MobileViewController-\(#function) Query Failed: \"\(DeleteQuery)\"")
                        DoAlert.DisplayAlert(Class: "MobileViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"\(DeleteQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                        return
                    }
                    
                    self.ReceivedTableInformation()
                    self.addItemToLabel.stringValue = ""
                    
                } else {
                    //print("NOT GOING TO DELETE - INVALID")
                }
                
            }
            if (resultCode1 == 1000) {
                print("Cancel Pressed")
                
            }
            
            
        }
        
       
        
        
        
        
        
        
        
        
        
    }// end of function
    
    
    
    
    
    

    ///////////////////////////////////
    
    
    
    @IBAction func CloseMobileWindowButtonAction(_ sender: Any) {
        
        //let count = self.tableView(self.tableView, numberOfRowsInSection: 0)
        
        
        //let count = self.bonjourServer.devices.count
        
        if (self.ConnectionLabel.stringValue != "Serivce Stopped") {
            self.bonjourServer.devices.removeAll(keepingCapacity: true)
            self.bonjourServer.stopBrowsing()
            self.bonjourServer.connectedService = nil
            self.bonjourServer.sockets.removeAll()
            self.bonjourServer.delegate = nil
            self.bonjourServer = nil
            
        }
    
        self.tableView.reloadData()
        
        
        
        self.view.window?.close()
        
        
        
        
    } // end of CloseMobileWindowButtonAction
    
    
    @IBAction func RefreshButtonAction(_ sender:Any) {  // _ sender: NSButton
        
        if (self.ConnectionLabel.stringValue == "Serivce Stopped") {
            
            //print("Refresh Button Pressed - Serivce is Stopped - Ignoring")
            
        } else {
        
        
            self.ConnectionLabel.stringValue = "Serivce Stopping"
            self.ConnectionLabel.textColor = NSColor.yellow
            
//            RefreshButton.isEnabled = false
//            StartButtonAction.isEnabled = false
//            StopButtonAction.isEnabled = false
            
            //self.bonjourServer.devices.removeAll(keepingCapacity: true)
            self.bonjourServer.devices.removeAll()
            
            self.tableView.reloadData()
            
            //print("Refresh Buton Pressed: Services and Table Refreshed")
            
            self.bonjourServer.stopBrowsing()
            
            delayWithSeconds(0.25) {
                self.ConnectionLabel.stringValue = "Serivce Stopped"
                self.ConnectionLabel.textColor = NSColor.red
            }
            
            delayWithSeconds(0.5) {
                self.ConnectionLabel.stringValue = "Serivce Starting"
                self.ConnectionLabel.textColor = NSColor.yellow
            }
            
            self.bonjourServer.delegate.didChangeServices()

            delayWithSeconds(1.0) {
                self.bonjourServer.startService()
//                self.RefreshButton.isEnabled = true
//                self.StopButtonAction.isEnabled = true
                self.ConnectionLabel.stringValue = "Serivce Running"
                self.ConnectionLabel.textColor = NSColor.green
            }
            
            //print("bonjourServer.devices.count = \(bonjourServer.devices)")
       
        }
        
    }
    
    
    @IBAction func StopServiceButtonAction(_ sender: Any) {
        
        
        if (self.ConnectionLabel.stringValue == "Serivce Stopped") {
            
            //print("Stop Button Pressed - Serivce is Stopped - Ignoring")
            
        } else {
        
            //RefreshButton.isEnabled = false
            
            self.bonjourServer.devices.removeAll(keepingCapacity: true)
            
            self.tableView.reloadData()
            
            self.bonjourServer.stopBrowsing()
            
            //self.bonjourServer.connectedService.stop()
            self.bonjourServer.connectedService = nil
            self.bonjourServer.sockets.removeAll()
            self.bonjourServer.delegate = nil
            self.bonjourServer = nil
            
            delayWithSeconds(0.5) {
                self.ConnectionLabel.stringValue = "Serivce Stopped"
                self.ConnectionLabel.textColor = NSColor.red
//                self.StartButtonAction.isEnabled = true
//                self.StopButtonAction.isEnabled = false
            }
        }
        
    }// end of StopServiceButtonAction
    
    
    @IBAction func StartServiceButtonAction(_ sender: Any) {
        
        
        if (self.ConnectionLabel.stringValue == "Serivce Running") {
            
            //print("Start Button Pressed - Serivce is Running - Ignoring")
            
        } else {
            self.ConnectionLabel.stringValue = "Serivce Starting"
            self.ConnectionLabel.textColor = NSColor.yellow
            
//            RefreshButton.isEnabled = true
//            StartButtonAction.isEnabled = false
//            StopButtonAction.isEnabled = true
            
            self.bonjourServer = BonjourServer()
            self.bonjourServer.delegate = self
            self.bonjourServer.startService()
            
            delayWithSeconds(0.5) {
                self.ConnectionLabel.stringValue = "Serivce Running"
                self.ConnectionLabel.textColor = NSColor.green
            }
        }
        
        
    }
    
    
    func MobileButtonStatus() {
        
        
        
//        RefreshButton.isEnabled = false
//        StartButtonAction.isEnabled = true
//        StopButtonAction.isEnabled = false
        
        
        
    }
    
    
    @IBAction func InstructionsButtonAction(_ sender: Any) {
        
        //MobileInstructionsPopover.showWindow(self)
        
        
        let popover = NSPopover()
        popover.contentViewController = MobileInstructionsPopover
        popover.behavior = .transient
        popover.show(relativeTo: InstructionButton.bounds, of: InstructionButton, preferredEdge: .maxX)
        
        
    }
    
    
    
    
    
    
    
}// end of MobileViewController
