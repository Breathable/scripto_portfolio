//
//  MobileCategoryInformation.swift
//  Scripto Portfolio
//
//  Created by Dorian Glanville on 7/17/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Foundation

class MobileCategoryInformation: NSObject {
    var Ikey: String
    var Name: String
    var Category: String
    
    
    init(Ikey: String, Name: String, Category: String) {
        self.Ikey = Ikey
        self.Name = Name
        self.Category = Category
    }
    
}
