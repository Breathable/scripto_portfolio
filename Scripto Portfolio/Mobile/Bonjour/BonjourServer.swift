//
//  BonjourServer.swift
//  bonjour-demo-mac
//
//  Created by James Zaghini on 14/05/2015.
//  Copyright (c) 2015 James Zaghini. All rights reserved.
//

import Cocoa
import SwiftLoggly

enum PacketTag: Int {
    case header = 1
    case body = 2
}

protocol BonjourServerDelegate {
    func connected()
    func disconnected()
    func handleBody(_ body: NSString?)
    func didChangeServices()
    func ButtonServiceStopped()
    func ButtonServiceStarted()
    func ServiceStartERROR(ErrorString: String, ErrorDict: String)
    func writeToDB()
}

class BonjourServer: NSObject, NetServiceBrowserDelegate, NetServiceDelegate, GCDAsyncSocketDelegate {

    var delegate: BonjourServerDelegate!
    
    var coServiceBrowser: NetServiceBrowser!
    
    var devices: Array<NetService>!
    
    var connectedService: NetService!
    
    var sockets: [String : GCDAsyncSocket]!
    
    override init() {
        super.init()
        self.devices = []
        self.sockets = [:]
        self.startService()
    }
    
    func parseHeader(_ data: Data) -> UInt {
        var out: UInt = 0
        (data as NSData).getBytes(&out, length: MemoryLayout<UInt>.size)
        return out
    }
    
    func handleResponseBody(_ data: Data) {
//        if let message = NSString(data: data, encoding: String.Encoding.utf8.rawValue) {
//            self.delegate.handleBody(message)
//        }
        
        
        
        
        
            
        
            
            do {
                dicFromData = ( try PropertyListSerialization.propertyList(from: data, options: [], format: nil) as! [String : String])// as! Dictionary<String, String>
                
                print("RECEIVED dicFromData: \n\(dicFromData)")
                
                if (dicFromData.count < 1) {
                    
                    loggly(LogType.Verbose, text: "BonjourServer:\(#function) - dicFromData.count = \"\(dicFromData.count)\"\nMore than likely due to it trying to run a second time. (More than likly ignore)  ")
                    
                    
                    
                    //print("dicFromData Count = \"\(dicFromData.count)\"")
                    //print("BonjourServer - dicFromData = EMPTY - STOPPED **********")
                    return
                }
                
                print("RECEIVED AFTER- dicFromData: \n\(dicFromData)")
                
                
                
                if ((dicFromData["stop"]) != nil) {
                    
                    //print("STOP WAS DETECTED")
                    
                    self.delegate.ButtonServiceStopped()
                    
                    delayWithSeconds(5.0) {
                        
                        self.devices.removeAll()
                        
                        self.stopBrowsing()
                        self.connectedService.stopMonitoring()
                        self.connectedService.stop()
                        //self.connectedService = nil
                        self.sockets.removeAll()
                        //self.delegate = nil
                        //print("SERVICES WERE STOP")
                        
                        //self.delegate.ButtonServiceStopped()
                        
                    }
                    return
                }
                
                var ScriptoIdeaVersion = ""
                
                if let val = dicFromData["ScriptoPortfolioVersion"] {
                    
                    print("ScriptoPortfolioVersion: \"\(val)\"  ")
                    
                    ScriptoIdeaVersion = val
                    
                    if val < "1.0.3" {
                        
                        delayWithSeconds(0.5) {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "StopMobileService"), object: nil)
                        }
                       
                        //print("ScriptoPortfolioVersion:  \"1.0.4\" Less Than \"\(val)\" ")
                        
                        loggly(LogType.Verbose, text: "BonjourServer:\(#function) - Warning the version ScriptoIdea is not up to date or the information from ScriptoIdea is Invalid. ScriptoIdea's version: \"\(val)\" \n\nWarned to check for ScriptoIdea updates on the mobile device.")
                        
                        DoAlert.DisplayAlert(Class: "BonjourServer", Level: 2, Function: "\(#function)", MessageText: "Warning", InformativeText: "Warning Scripto Idea is not up to date or the information from ScriptoIdea is Invalid.\nPlease check for ScriptoIdea updates on you mobile device.\n\nPlease Press Cancel in ScriptoIdea.", DatabaseError: "N/A")
                        
                        return
                        
                    }
                }// end of if let val = dicFromData["ScriptoPortfolioVersion"]

                //if (dicFromData["ideaTypeLabel"] == nil) {
                if let index = dicFromData.index(forKey: "ideaTypeLabel") {
                
                    
                   
                } else {
                    
                    print("index: \"\(String(describing: index))\" ")
                    
                    loggly(LogType.Verbose, text: "BonjourServer:\(#function) - Warning \"ideaTypeLabel\" information was missing from the dictionary that was sent from Scripto Idea.\n\"\(dicFromData)\"\nScriptoIdea's version: \"\(ScriptoIdeaVersion)\"")
                    
                    DoAlert.DisplayAlert(Class: "BonjourServer", Level: 2, Function: "\(#function)", MessageText: "Warning", InformativeText: "Please check for ScriptoIdea updates on you mobile device.ScriptoIdea must be higher than 1.0.3. ScriptoIdea's version: \"\(ScriptoIdeaVersion)\"\n\nPlease Press Cancel in ScriptoIdea.", DatabaseError: "\n\"ideaTypeLabel\" information was missing. ")
                    
                    delayWithSeconds(0.5) {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "StopMobileService"), object: nil)
                    }
                    
                    return
                    
                }
                
               
                
                
                
                
                

                
//                let Category = "Category: \"\(dicFromData["ideaTypeLabel"] ?? "BLANK CATEGORY")\""
//                
//                print("Category: \"\(Category)\"")
                
                self.delegate.writeToDB()
                
                
            } catch {
                print("ERROR: \(error)")
                
                loggly(LogType.Error, text: "BonjourServer:\(#function) - Unknown Or Invalid infromation received - Possible the information received was not a Dictionary. Please quit both apps and try to send it again. \n\"\(dicFromData)\"\n\n\(error)\n")
                
                DoAlert.DisplayAlert(Class: "BonjourServer", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Unknown Or Invalid infromation found. Please quit both apps and try to send it again. ", DatabaseError: "ERROR INFORMATION:\n\n\"\(dicFromData)\"\n\"\(error)\" ")
                
                
            }
            
            
//            if ((dicFromData["stop"]) != nil) {
//
//                print("STOP WAS DETECTED")
//
//
//                delayWithSeconds(5.0) {
//
//                    self.devices.removeAll()
//                    
//                    self.stopBrowsing()
//                    self.connectedService.stopMonitoring()
//                    self.connectedService.stop()
//                    //self.connectedService = nil
//                    self.sockets.removeAll()
//                    //self.delegate = nil
//                    print("SERVICES WERE STOP")
//
//                    self.delegate.ButtonServiceStopped()
//
//                }
//
//            }
            
        
        
        
            
        
            
            
       
        
        
        
        
    
        
    
    }
    
    func connectTo(_ service: NetService) {
        service.delegate = self
        service.resolve(withTimeout: 60)
    }
    
    // MARK: NSNetServiceBrowser helpers
    
    func stopBrowsing() {
        if self.coServiceBrowser != nil {
            self.coServiceBrowser.stop()
            self.coServiceBrowser.delegate = nil
            self.coServiceBrowser = nil
        }
    }
    
    func startService() {
        if (self.delegate != nil) {
            if self.devices != nil {
                self.devices.removeAll(keepingCapacity: true)
            }
            
            self.coServiceBrowser = NetServiceBrowser()
            self.coServiceBrowser.delegate = self
            self.coServiceBrowser.searchForServices(ofType: "_ScriptoPortfolio._tcp.", inDomain: "local.")
            
            delayWithSeconds(0.5){
                self.delegate.ButtonServiceStarted()
            }
        }
        
    }
    
    func send(_ data: Data) {
        print("send data")
        
        if let socket = self.getSelectedSocket() {
            var header = data.count
            let headerData = Data(bytes: &header, count: MemoryLayout<UInt>.size)
            socket.write(headerData, withTimeout: -1.0, tag: PacketTag.header.rawValue)
            socket.write(data, withTimeout: -1.0, tag: PacketTag.body.rawValue)
        }
    }
    
    func connectToServer(_ service: NetService) -> Bool {
        var connected = false
        
        let addresses: Array = service.addresses!
        var socket = self.sockets[service.name]
        
        if !(socket?.isConnected != nil) {
            socket = GCDAsyncSocket(delegate: self, delegateQueue: DispatchQueue.main)
            
            while !connected && !addresses.isEmpty {
                let address: Data = addresses[0] 
                do {
                    if (try socket?.connect(toAddress: address) != nil) {
                        self.sockets.updateValue(socket!, forKey: service.name)
                        self.connectedService = service
                        connected = true
                    }
                } catch {
                    print(error);
                }
            }
        }
        
        return true
    }
    
    // MARK: NSNetService Delegates
    
    func netServiceDidResolveAddress(_ sender: NetService) {
        //print("did resolve address \(sender.name)")
        if self.connectToServer(sender) {
            //print("connected to \(sender.name)")
            
            loggly(LogType.Error, text: "BonjourServer:\(#function) - Connected to \"\(sender.name)\" ")
            
            //DoAlert.DisplayAlert(Class: "BonjourServer", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Unknown Or Invalid infromation found. Please quit both apps and try to send it again. ", DatabaseError: "ERROR INFORMATION:\n\n\"\(dicFromData)\"\n\"\(error)\" ")
            
        }
    }
    
    func netService(_ sender: NetService, didNotResolve errorDict: [String : NSNumber]) {
        print("net service did not resolve. errorDict: \(errorDict)")
        
        loggly(LogType.Error, text: "BonjourServer:\(#function) - Net Service did not resolve. errorDict: \(errorDict) ")
        
        self.delegate.ServiceStartERROR(ErrorString: "Serice Didnt Resolve - Please Quit and ReOpen and try again.", ErrorDict: "\(errorDict)")
    }   
    
    // MARK: GCDAsyncSocket Delegates
    
    func socket(_ sock: GCDAsyncSocket!, didConnectToHost host: String!, port: UInt16) {
        print("connected to host \(host), on port \(port)")
        loggly(LogType.Error, text: "BonjourServer:\(#function) - Connected toHost \"\(host)\", on port \"\(port)\" ")
        sock.readData(toLength: UInt(MemoryLayout<UInt64>.size), withTimeout: -1.0, tag: 0)
    }
    
    func socketDidDisconnect(_ sock: GCDAsyncSocket!, withError err: Error!) {
        print("socket did disconnect \(sock), error: \(err._userInfo!)")
        loggly(LogType.Error, text: "BonjourServer:\(#function) - Socket Did Disconnect \'\(sock)\", error: \"\(err._userInfo!)\" ")
    }
    
    func socket(_ sock: GCDAsyncSocket!, didRead data: Data!, withTag tag: Int) {
        //print("socket did read data. tag: \(tag)")
        
        if self.getSelectedSocket() == sock {
            
            if data.count == MemoryLayout<UInt>.size {
                let bodyLength: UInt = self.parseHeader(data)
                sock.readData(toLength: bodyLength, withTimeout: -1, tag: PacketTag.body.rawValue)
                //print("TAG: \"\(tag)\"")
            } else {
                self.handleResponseBody(data)
                sock.readData(toLength: UInt(MemoryLayout<UInt>.size), withTimeout: -1, tag: PacketTag.header.rawValue)
                //print("TAG ELSE: \"\(tag)\"")
            }
        }
    }
    
    func socketDidCloseReadStream(_ sock: GCDAsyncSocket!) {
        print("socket did close read stream")
        loggly(LogType.Info, text: "BonjourServer:\(#function) - Socket Did Close Read Stream ")
    }    
    
    // MARK: NSNetServiceBrowser Delegates
    
    func netServiceBrowser(_ aNetServiceBrowser: NetServiceBrowser, didFind aNetService: NetService, moreComing: Bool) {
        self.devices.append(aNetService)
        if !moreComing {
            self.delegate.didChangeServices()
            //print("didFind aNetService moreComing - self.delegate.didChangeServices() ")
        }
    }
    
    func netServiceBrowser(_ aNetServiceBrowser: NetServiceBrowser, didRemove aNetService: NetService, moreComing: Bool) {
        self.devices.removeObject(aNetService)
        if !moreComing {
            self.delegate.didChangeServices()
            //print("didRemove aNetService moreComing - self.delegate.didChangeServices() ")
        }
    }
    
    func netServiceBrowserDidStopSearch(_ aNetServiceBrowser: NetServiceBrowser) {
        self.stopBrowsing()
    }
    
    func netServiceBrowser(_ aNetServiceBrowser: NetServiceBrowser, didNotSearch errorDict: [String : NSNumber]) {
        self.stopBrowsing()
    }
    
    // MARK: helpers
    
    func getSelectedSocket() -> GCDAsyncSocket? {
        var sock: GCDAsyncSocket?
        if self.connectedService != nil {
            sock = self.sockets[self.connectedService.name]!
        }
        return sock
    }
    
}
