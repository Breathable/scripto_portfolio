//
//  StoryHistoryViewController.swift
//  Scripto Portfolio
//
//  Created by Dorian Glanville on 5/13/19.
//  Copyright © 2019 DTGM. All rights reserved.
//

import Cocoa
import SwiftLoggly


class StoryHistoryViewController: NSViewController, NSTableViewDelegate, NSTableViewDataSource {
    
    
    
    
    @IBOutlet weak var StoryHistoryTabView: NSTabView!
    @IBOutlet weak var StoryHistoryTableView: NSTableView!
    
    @IBOutlet weak var StoryHistoryCloseButton: NSButton!
    @IBOutlet weak var StoryHistoryStoryNameLabel: NSTextField!
    @IBOutlet var StoryHistoryView: NSView!
    
    
    
    
    
    
    
    var StoryHistoryArray: [StoryHistoryList] = []
    
    var CurrentAppearanceStoryHistoryViewController = String()
    
    
    
    
    
    
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        
        
        StoryHistoryTableView.delegate = self
        StoryHistoryTableView.dataSource = self
        
        
    }
    
    
    override func viewDidLayout() {
        
        enum InterfaceStyle : String {
            case Dark, Light
            
            init() {
                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
                self = InterfaceStyle(rawValue: type)!
            }
        }
        
        let currentStyle = InterfaceStyle()
        
        //print("currentStyle: \"\(currentStyle)\"  -  CurrentAppearance: \"\(CurrentAppearance)\" ")
        
        if ("\(currentStyle)" != CurrentAppearanceStoryHistoryViewController) {
            appearanceCheck()
        }
        
        
        
        super.viewDidLayout()
        
        print("StoryListViewController - viewDidLayout finished")
        
        
        
        
    }// end of viewDidLayout()
    
    
    override func viewWillAppear() {
        LoadStoryHistoryTable()
        
        StoryHistoryStoryNameLabel.stringValue = StoryNameOpen
        //StoryNameOpen
        //StoryWindow.window!.title
        
    }//end of override func viewWillAppear()
    
    
    
    func appearanceCheck() {
        
        enum InterfaceStyle : String {
            case Dark, Light
            
            init() {
                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
                self = InterfaceStyle(rawValue: type)!
            }
        }
        
        let currentStyle = InterfaceStyle()
        
       
        if (currentStyle.rawValue == "Dark") {
            print("Dark Style Detected - STORYLISTVIEWCONTROLLER")
            
            StoryHistoryTableView.backgroundColor = TableBackgroundColorDarkMode
            
            view.wantsLayer = true
            view.layer?.backgroundColor = WindowBackgroundColorDarkMode
            
            StoryHistoryCloseButton.styleButtonText(button: StoryHistoryCloseButton, buttonName: "Close", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
            
            StoryHistoryCloseButton.wantsLayer = true
            StoryHistoryCloseButton.layer?.backgroundColor = NSColor.gray.cgColor
            StoryHistoryCloseButton.layer?.cornerRadius = 7
            
            StoryHistoryStoryNameLabel.font = NSFont(name:AppFontBold, size: 13.0)
            StoryHistoryStoryNameLabel.textColor = NSColor.lightGray
            
            
            
        } else if (currentStyle.rawValue == "Light") {
            print("Light Style Detected - STORYLISTVIEWCONTROLLER")
            
            view.wantsLayer = true
            view.layer?.backgroundColor = WindowBackgroundColor.cgColor
            
            StoryHistoryCloseButton.styleButtonText(button: StoryHistoryCloseButton, buttonName: "Close", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
            
            StoryHistoryCloseButton.wantsLayer = true
            StoryHistoryCloseButton.layer?.backgroundColor = NSColor.lightGray.cgColor
            StoryHistoryCloseButton.layer?.cornerRadius = 7
            
            StoryHistoryStoryNameLabel.font = NSFont(name:AppFontBold, size: 13.0)
            StoryHistoryStoryNameLabel.textColor = NSColor.white
            
        }
        
        
        
        CurrentAppearanceStoryHistoryViewController = "\(currentStyle)"
        
        
        LoadStoryHistoryTable()
        
    }//end of appearanceCheck()
    
    
    func LoadStoryHistoryTable() {
        
        
        
        StoryHistoryArray.removeAll()
        
        
        var StartDateDB: String
        var EndDateDB: String
        var StartWordCountDB: String
        var EndWordCountDB: String
        var UpdatedByDB: Int
        var OwnedByDB: Int
        
        var Query = String()
        
        db.open()
        
        do {
            
            
            
            
            
            Query = "select * from storyhistorytable where storyikeyreference = \(StoryIkeyOpen)"
            
            
            print("Query: \"\(Query)\"")
            
            let StoryQuery = try db.executeQuery(Query, values:nil)
            
            
            while StoryQuery.next() {
                
                StartDateDB = StoryQuery.string(forColumn: "startdate")!
                EndDateDB = StoryQuery.string(forColumn: "enddate")!
                StartWordCountDB = StoryQuery.string(forColumn: "startwordcount")!
                EndWordCountDB = StoryQuery.string(forColumn: "endwordcount")!
                UpdatedByDB = Int(StoryQuery.int(forColumn: "updatedbyuserikey"))
                OwnedByDB = Int(StoryQuery.int(forColumn: "ownedbyuserikey"))
                
                //////////////
                
//                let dateFormatter = DateFormatter()
//                //dateFormatter.dateFormat = "MMMM d, yyyy"
//                dateFormatter.dateFormat = "MMMM d, yyyy H:mm a"
//                //dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
//                dateFormatter.timeZone = TimeZone.current
//                dateFormatter.locale = Locale.current
//
//                let dateFormatter2 = DateFormatter()
//                //dateFormatter.dateFormat = "MMMM d, yyyy"
//                dateFormatter2.dateFormat = "MMMM d, yyyy h:mm a"
//                //dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
//                dateFormatter2.timeZone = TimeZone.current
//                dateFormatter2.locale = Locale.current
//
//                print("StartDateDB: \"\(StartDateDB)\" ")
//                let StartDate = dateFormatter.date(from:StartDateDB)
//                print("StartDate: \"\(StartDate!)\" ")
//                let StartDateString = dateFormatter2.string(from: StartDate!)
//                print("StartDateString: \"\(StartDateString)\" ")
//
//                print("EndDateDB: \"\(EndDateDB)\" ")
//                let EndDate = dateFormatter.date(from:EndDateDB)
//                 print("EndDate: \"\(EndDate!)\" ")
//                let EndDateString = dateFormatter2.string(from: EndDate!)
//                print("EndDateString: \"\(EndDateString)\" ")
                
                //////////////
                
                
                var userDict = Dictionary<Int,String>()
                
                var ikeyDB = Int()
                var NameDB = String()
                
                
                let UQuery = "select ikey,user from users"
                
                
                print("UQuery: \"\(UQuery)\"")
                
                let UserQuery = try db.executeQuery(UQuery, values:nil)

                while UserQuery.next() {
                    
                    ikeyDB = Int(UserQuery.int(forColumn: "ikey"))
                    NameDB = UserQuery.string(forColumn: "user")!
                    
                    userDict[ikeyDB] = "\(NameDB)"
                
                }
            
                //////////////
                
                let UpdatedBy = userDict[UpdatedByDB]
                print("UpdatedBy: \"\(UpdatedBy!)\" ")
                
                let OwnedBy = userDict[OwnedByDB]
                print("OwnedBy: \"\(OwnedBy!)\" ")
                
                
                //////////////
                
                let StoryToAdd = StoryHistoryList.init(StartDate: StartDateDB, EndDate: EndDateDB, StartWordCount: StartWordCountDB, EndWordCount: EndWordCountDB, UpdatedBy: UpdatedBy!, OwnedBy: OwnedBy! )
                
                StoryHistoryArray.append(StoryToAdd)
                
            }
            
        } catch {
            
            let ErrorString = "ERROR: StoryHistoryViewController Adding to Stories Array \(db.lastErrorMessage())"
            
            print("\(ErrorString)")
            
            loggly(LogType.Error, text: "ERROR: StoryHistoryViewController Adding to Stories Array \(db.lastErrorMessage())")
            
            DoAlert.DisplayAlert(Class: "StoryHistoryViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to Add to get history for \(StoryNameOpen)\n\n\(Query)", DatabaseError: "\(db.lastErrorMessage())")
        }
        
        //print("Stories: \(Stories)")
        
        StoryHistoryTableView.reloadData()
    
        
    }//end of func LoadStoryHistoryTable()
    
    
    
    
    func numberOfRows(in tableVieww: NSTableView) -> Int {
        print("Stories.count: \(StoryHistoryArray.count)")
        return StoryHistoryArray.count
        
    }
    
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        
        guard let cell = tableView.makeView(withIdentifier: tableColumn!.identifier, owner: self) as? NSTableCellView else
        {
            print("Table Returned Nil")
            return nil
        }
        
//        var StartDateDB: String
//        var EndDateDB: String
//        var StartWordCountDB: Int
//        var EndWordCountDB: Int
//        var UpdatedByDB: String
//        var OwnedByDB: String
        
        if (tableColumn?.identifier)!.rawValue == "startdate" {
            cell.textField?.stringValue = StoryHistoryArray[row].StartDate
        }
        if (tableColumn?.identifier)!.rawValue == "enddate" {
            cell.textField?.stringValue = StoryHistoryArray[row].EndDate
        }
        if (tableColumn?.identifier)!.rawValue == "startwordcount" {
            cell.textField?.stringValue = StoryHistoryArray[row].StartWordCount
        }
        if (tableColumn?.identifier)!.rawValue == "endwordcount" {
            cell.textField?.stringValue = StoryHistoryArray[row].EndWordCount
        }
        if (tableColumn?.identifier)!.rawValue == "updatedby" {
            cell.textField?.stringValue = StoryHistoryArray[row].UpdatedBy
        }
        if (tableColumn?.identifier)!.rawValue == "ownedby" {
            cell.textField?.stringValue = StoryHistoryArray[row].OwnedBy
        }
        
        
        return cell
        
    }//end of tableView
    
    
    
    
    func tableViewSelectionDidChange(_ notification: Notification) {
        
        
        print("Row Clicked")
        let SelectedRow = StoryHistoryTableView.selectedRow
        print("SelectedRow: \(SelectedRow)")
        
        
        let isIndexValid = StoryHistoryArray.indices.contains(SelectedRow)
        
        if isIndexValid {
            
            let StartDate: String = self.StoryHistoryArray[SelectedRow].StartDate
            let EndDate: String = self.StoryHistoryArray[SelectedRow].EndDate
            let StartWordCount: String = self.StoryHistoryArray[SelectedRow].StartWordCount
            let EndWordCount: String = self.StoryHistoryArray[SelectedRow].EndWordCount
            let UpdatedBy: String = self.StoryHistoryArray[SelectedRow].UpdatedBy
            let OwnedBy: String = self.StoryHistoryArray[SelectedRow].OwnedBy
        
            print("Selected Record: StartDate: \"\(StartDate)\" EndDate: \"\(EndDate)\" StartWordCount: \"\(StartWordCount)\" EndWordCount: \"\(EndWordCount)\" StartDate: \"\(UpdatedBy)\" StartDate: \"\(OwnedBy)\" ")
            

            
            
        } else {
            
        }
        
        
    }//end of func tableViewSelectionDidChange
    
    
    
    @IBAction func closeStoryHistoryWindowButton(_ sender: Any) {
        
        self.view.window?.close()
        
        StoryHistoryArray.removeAll()
        
        StoryHistoryTableView.reloadData()
        
        
    }// end of func closeStoryHistoryWindowButton
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}// end of class StoryHistoryViewController: NSViewController
