//
//  StoryHistoryList.swift
//  Scripto Portfolio
//
//  Created by Dorian Glanville on 5/13/19.
//  Copyright © 2019 DTGM. All rights reserved.
//

import Foundation


class StoryHistoryList: NSObject {
    var StartDate: String
    var EndDate: String
    var StartWordCount: String
    var EndWordCount: String
    var UpdatedBy: String
    var OwnedBy: String
    
    init(StartDate: String, EndDate: String, StartWordCount: String, EndWordCount: String, UpdatedBy: String, OwnedBy: String) {
        self.StartDate = StartDate
        self.EndDate = EndDate
        self.StartWordCount = StartWordCount
        self.EndWordCount = EndWordCount
        self.UpdatedBy = UpdatedBy
        self.OwnedBy = OwnedBy
    }
    
    
}
