//
//  StoryHistoryWindowController.swift
//  Scripto Portfolio
//
//  Created by Dorian Glanville on 5/13/19.
//  Copyright © 2019 DTGM. All rights reserved.
//

import Cocoa

class StoryHistoryWindowController: NSWindowController {
    
    
    
    
    
    
    
    
    

    override func windowDidLoad() {
        super.windowDidLoad()
    
        // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
        
        if (OSSYSTEMVERSION >= 10.13) {  // >=
            window?.titlebarAppearsTransparent = true
            print("StoryWindowController = window?.titlebarAppearsTransparent = true")
            
        }
        
        
        window?.titleVisibility = .hidden
 
    }
    

    

    
    
    
}// end of class StoryHistoryWindowController: NSWindowController
