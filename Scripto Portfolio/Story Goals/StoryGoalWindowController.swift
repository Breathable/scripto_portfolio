//
//  StoryGoalWindowController.swift
//  Scripto Portfolio
//
//  Created by Dorian Glanville on 2/11/19.
//  Copyright © 2019 DTGM. All rights reserved.
//

import Cocoa

class StoryGoalWindowController: NSWindowController {

    override func windowDidLoad() {
        super.windowDidLoad()
    
        // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
        
        window?.titlebarAppearsTransparent = true
        window?.titleVisibility = .hidden
        
    }

}
