//
//  StoryGoalViewController.swift
//  Scripto Portfolio
//
//  Created by Dorian Glanville on 2/11/19.
//  Copyright © 2019 DTGM. All rights reserved.
//

import Cocoa
import SwiftLoggly
import FMDB



class StoryGoalViewController: NSViewController, NSTableViewDataSource, NSTableViewDelegate, NSTextFieldDelegate {

    
    @IBOutlet weak var StoryGoalCloseButton: NSButton!
    @IBOutlet weak var StoryGoalAddGoalButton: NSButton!
    
    
    @IBOutlet weak var StoryGoalCatagoryPopupButton: NSPopUpButton!
    @IBOutlet weak var StoryGoalCategoryOptionSelectionPopupButton: NSPopUpButton!
    @IBOutlet weak var StoryGoalSubOptionPopupButton: NSPopUpButton!
    @IBOutlet weak var StoryGoalDailyWordCountTextField: NSTextField!
    @IBOutlet weak var StoryGoalTotalWordCountTextField: NSTextField!
    
    @IBOutlet weak var StoryStartGoalDatePicker: NSDatePicker!
    @IBOutlet weak var StoryNoStartGoalCheckBox: NSButton!
    
    @IBOutlet weak var StoryEndGoalDatePicker: NSDatePicker!

    
    @IBOutlet weak var StoryNoEndGoalCheckBox: NSButton!
    
    
    

    @IBOutlet weak var StoryGoalTableView: NSTableView!
    
    @IBOutlet weak var StoryGoalDeleteGoalButton: NSButton!
    @IBOutlet weak var StoryEditGoalButton: NSButton!
 
    @IBOutlet weak var StoryGoalDurationLabel: NSTextField!
    
    
    @IBOutlet weak var itemStatusCheckbox: NSButton!
    
    
    var CurrentAppearanceStoryGoalViewController = String()
    
    
   
    @IBOutlet weak var StoryGoalOnOffSwitchLabel: NSTextField!
    
    
   
    @IBOutlet weak var StoryGoalStoryFilterPopUpButton: NSPopUpButton!
    
  
    
    // Labels
    
    @IBOutlet weak var StoryGoalCatagoryLabel: NSTextField!
    @IBOutlet weak var StoryGoalDailyWordCountLabel: NSTextField!
    @IBOutlet weak var StoryGoalTotalWordCountLabel: NSTextField!
    @IBOutlet weak var StoryGoalStartDatePickerLabel: NSTextField!
    @IBOutlet weak var StoryGoalEndDatePickerLabel: NSTextField!
    @IBOutlet weak var StoryGoalSearchLabel: NSTextField!
    
    
    
    
    
    var CatagoryListArray: [String] = ["Stories","Characters","Locations","Items","Story Ideas"]
    var CategoryOptionArray: [String] = ["Select a Story"]
    
    var StoryListArray: [String] = []

    
    var StoryGoalsArray: [StoryGoalList] = []
    var StorySearchIkeyDB = 0
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        

        
        //LoadStoryGoalTableData()
        
    }
    
    
    
    override func viewWillAppear() {
        
        StoryGoalTableView.delegate = self
        StoryGoalTableView.dataSource = self
        
        StoryStartGoalDatePicker.dateValue = Date()
        StoryEndGoalDatePicker.dateValue = Date()
        
        
        
        StoryGoalCatagoryPopupButton.removeAllItems()
        StoryGoalCatagoryPopupButton.addItems(withTitles: CatagoryListArray)
        StoryGoalCatagoryPopupButton.selectItem(withTitle: "Stories")
        
        StoryGoalCategoryOptionSelectionPopupButton.removeAllItems()
        StoryGoalCategoryOptionSelectionPopupButton.addItems(withTitles: CategoryOptionArray)
        StoryGoalCategoryOptionSelectionPopupButton.selectItem(withTitle: "Select an Item")
        
        appearanceCheck()
        
    }// end of viewWillAppear()
    
    override func viewDidAppear() {
        
        //StoryGoalDatePicker.dateValue = Date()
        
        
        
        LoadStoryGoalTableData()
        LoadStoryListDropDown()
    }
    
    override func viewDidLayout() {
    
        
        appearanceCheck()
        
        super.viewDidLayout()
        
//        StoryGoalOnOffSwitch.setOn(on: true, animated: false)
//        StoryGoalOnOffSwitch.isEnabled = false
        
        // Hiding the Daily Word Count until later development.
        StoryGoalTableView.tableColumn(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "dailywordcount"))?.isHidden = true


        
        
    }// end of viewDidLayout()
    
    
    
    func appearanceCheck() {
        
        enum InterfaceStyle : String {
            case Dark, Light
            
            init() {
                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
                self = InterfaceStyle(rawValue: type)!
            }
        }
        
        let currentStyle = InterfaceStyle()
        
        
        
        if (currentStyle.rawValue == "Dark") {
            print("Dark Style Detected - STORYLISTVIEWCONTROLLER")
            
            
            
            view.wantsLayer = true
            view.layer?.backgroundColor = WindowBackgroundColorDarkMode
            
            
            StoryGoalCloseButton.styleButtonText(button: StoryGoalCloseButton, buttonName: "Close", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
            
            StoryGoalCloseButton.wantsLayer = true
            StoryGoalCloseButton.layer?.backgroundColor = NSColor.lightGray.cgColor
            StoryGoalCloseButton.layer?.cornerRadius = 7


            StoryGoalAddGoalButton.styleButtonText(button: StoryGoalAddGoalButton, buttonName: "Add Goal", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)

            StoryGoalAddGoalButton.wantsLayer = true
            StoryGoalAddGoalButton.layer?.backgroundColor = NSColor.lightGray.cgColor
            StoryGoalAddGoalButton.layer?.cornerRadius = 7

            StoryGoalCatagoryLabel.font = NSFont(name:AppFontBold, size: 13.0)
            StoryGoalCatagoryLabel.textColor = NSColor.lightGray
            
            StoryGoalDailyWordCountLabel.font = NSFont(name:AppFontBold, size: 13.0)
            StoryGoalDailyWordCountLabel.textColor = NSColor.lightGray
            
            StoryGoalTotalWordCountLabel.font = NSFont(name:AppFontBold, size: 13.0)
            StoryGoalTotalWordCountLabel.textColor = NSColor.lightGray
            
            StoryGoalStartDatePickerLabel.font = NSFont(name:AppFontBold, size: 13.0)
            StoryGoalStartDatePickerLabel.textColor = NSColor.lightGray
            
            StoryGoalEndDatePickerLabel.font = NSFont(name:AppFontBold, size: 13.0)
            StoryGoalEndDatePickerLabel.textColor = NSColor.lightGray
            
            StoryGoalSearchLabel.font = NSFont(name:AppFontBold, size: 13.0)
            StoryGoalSearchLabel.textColor = NSColor.lightGray


            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.alignment = .center
            
            StoryNoStartGoalCheckBox.attributedTitle = NSMutableAttributedString(string: "No Start Date", attributes: [NSAttributedStringKey.foregroundColor: NSColor.white, NSAttributedStringKey.paragraphStyle: paragraphStyle, NSAttributedStringKey.font: NSFont.systemFont(ofSize: 13)])
            
            StoryNoEndGoalCheckBox.attributedTitle = NSMutableAttributedString(string: "No End Date", attributes: [NSAttributedStringKey.foregroundColor: NSColor.white, NSAttributedStringKey.paragraphStyle: paragraphStyle, NSAttributedStringKey.font: NSFont.systemFont(ofSize: 13)])
            
            
            
        } else if (currentStyle.rawValue == "Light") {
            print("Light Style Detected - STORYLISTVIEWCONTROLLER")
            
           
            
            view.wantsLayer = true
            view.layer?.backgroundColor = WindowBackgroundColor.cgColor
            

            StoryGoalCloseButton.styleButtonText(button: StoryGoalCloseButton, buttonName: "Close", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
            StoryGoalAddGoalButton.styleButtonText(button: StoryGoalAddGoalButton, buttonName: "Add Goal", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
            
            
            StoryGoalCloseButton.wantsLayer = true
            StoryGoalCloseButton.layer?.backgroundColor = NSColor.gray.cgColor
            StoryGoalCloseButton.layer?.cornerRadius = 7

            StoryGoalAddGoalButton.wantsLayer = true
            StoryGoalAddGoalButton.layer?.backgroundColor = NSColor.gray.cgColor
            StoryGoalAddGoalButton.layer?.cornerRadius = 7



            StoryGoalCatagoryLabel.font = NSFont(name:AppFontBold, size: 13.0)
            StoryGoalCatagoryLabel.textColor = NSColor.white
            
            StoryGoalDailyWordCountLabel.font = NSFont(name:AppFontBold, size: 13.0)
            StoryGoalDailyWordCountLabel.textColor = NSColor.white
            
            StoryGoalTotalWordCountLabel.font = NSFont(name:AppFontBold, size: 13.0)
            StoryGoalTotalWordCountLabel.textColor = NSColor.white
            
            StoryGoalStartDatePickerLabel.font = NSFont(name:AppFontBold, size: 13.0)
            StoryGoalStartDatePickerLabel.textColor = NSColor.white
            
            StoryGoalEndDatePickerLabel.font = NSFont(name:AppFontBold, size: 13.0)
            StoryGoalEndDatePickerLabel.textColor = NSColor.white
            
            StoryGoalSearchLabel.font = NSFont(name:AppFontBold, size: 13.0)
            StoryGoalSearchLabel.textColor = NSColor.white

            
        }
        
        
        
        CurrentAppearanceStoryGoalViewController = "\(currentStyle)"
        
        
        
        
    }//end of appearanceCheck()
    
    
    
    
    
   
    
    @IBAction func StoryGoalFilterPopUpButtonAction(_ sender: Any) {
        
        db.open()
        
        if (StoryGoalStoryFilterPopUpButton.titleOfSelectedItem == "Select a Story") {
            
            print("StoryGoalStoryFilterPopUpButton.stringValue = \"\(StoryGoalStoryFilterPopUpButton.titleOfSelectedItem!)\" ")
            StorySearchIkeyDB = 0
            
            
            
        } else {
            
            
            print("StoryGoalStoryFilterPopUpButton.stringValue = \"\(StoryGoalStoryFilterPopUpButton.titleOfSelectedItem!)\" ")
            
            
            let StoryIkeyQuery = "select ikey from storytable where storyname = \"\(StoryGoalStoryFilterPopUpButton.titleOfSelectedItem!)\" "
            
            do {
                
                let StoryIkeyQueryRun = try db.executeQuery(StoryIkeyQuery, values:nil)
                
                while StoryIkeyQueryRun.next() {
                    
                    StorySearchIkeyDB = Int(StoryIkeyQueryRun.int(forColumn: "ikey"))
                    print("StorySearchIkeyDB: \(StorySearchIkeyDB)")
                    
                }
                
            } catch {
                
                print("ERROR: StoryGoalViewController:\(#function): Failed to get \"\(CurrentUserName!)\"'s Story ID \n\(StoryIkeyQuery)\n\(db.lastErrorMessage())")
                
                let ErrorMessage = "StoryGoalViewController:\(#function) - Failed to get \"\(CurrentUserName!)\"'s Story ID \n\(db.lastErrorMessage())\n\(StoryIkeyQuery))"
                
                print(ErrorMessage)
                
                loggly(LogType.Error, text: ErrorMessage)
                
                DoAlert.DisplayAlert(Class: "StoryGoalViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to Failed to get \"\(CurrentUserName!)\"'s Story Names\n\n\(StoryIkeyQuery)", DatabaseError: "\(db.lastErrorMessage())")
                
            }
            
            
        }
        
        LoadStoryGoalTableData()
        
    }// end of StoryGoalFilterPopUpButtonAction
    
    
    
    
    
    
    
    func LoadStoryGoalTableData() {
        
        ConnectionTest.OpenDB()
        
        
        if (StoryGoalsArray.count > 0) {
            StoryGoalsArray.removeAll()
        }
        
        
        var IkeyDB: String!
        var UserIkeyReferenceDB: String!
        var CategoryDB: String!
        var DailyWordCountDB: String?
        var TotalWordCountDB: String?
        var GoalStartDateDB: String?
        var GoalEndDateDB: String?
        var CreateDate: String?
        var StatusDB: String!
        var CompletedDB: Int
        
        var StoryGoalQuery = ""
        
       
        
        print("StoryGoalStoryFilterPopUpButton.titleOfSelectedItem: \"\(StoryGoalStoryFilterPopUpButton.titleOfSelectedItem!)\" ")
        
        
        
        if (StorySearchIkeyDB == 0)  {
            StoryGoalQuery = "select * from storygoaltable where userikeyreference = \(CurrentUserIkey!) or userikeyreference = 0 "
        } else {
            StoryGoalQuery = "select * from storygoaltable where storyikeyreference = \"\(StorySearchIkeyDB)\" and userikeyreference = \(CurrentUserIkey!) " //   or userikeyreference = 0
        }
        
        
        
        
        
        print("StoryGoalQuery: \(StoryGoalQuery)")

        
        
        
        
        do {
            
            let StoryGoalQueryRun = try db.executeQuery(StoryGoalQuery, values:nil)
            
            while StoryGoalQueryRun.next() {
                
                IkeyDB = StoryGoalQueryRun.string(forColumn: "ikey")
                UserIkeyReferenceDB = StoryGoalQueryRun.string(forColumn: "userikeyreference")
                
                CategoryDB = "Stories"
                DailyWordCountDB = StoryGoalQueryRun.string(forColumn: "dailywordcount")
                TotalWordCountDB = StoryGoalQueryRun.string(forColumn: "totalwordcount")
                GoalStartDateDB = StoryGoalQueryRun.string(forColumn: "startgoaldate")
                GoalEndDateDB = StoryGoalQueryRun.string(forColumn: "endgoaldate")
                CreateDate = StoryGoalQueryRun.string(forColumn: "createdate")
                StatusDB = StoryGoalQueryRun.string(forColumn: "status")
                CompletedDB = Int(StoryGoalQueryRun.long(forColumn: "completed"))
                
                let storyIkeyReferenceDB = Int(StoryGoalQueryRun.long(forColumn: "storyikeyreference"))
                
                
                var storyNameDB = ""
                
                
                if (storyIkeyReferenceDB == 0) {
                    storyNameDB = "Default"
                } else {
                
                    
                    
                    let StoryNameQuery = "select storyname from storytable where ikey = \(storyIkeyReferenceDB)"
                    do {
                        
                        let StoryNameQueryRun = try db.executeQuery(StoryNameQuery, values:nil)
                        
                        while StoryNameQueryRun.next() {
                            storyNameDB = StoryNameQueryRun.string(forColumn: "storyname")!
                        }
                        
                    } catch {
                        
                            print("ERROR: StoryGoalViewController:\(#function): Failed to get \"\(CurrentUserName!)\"'s Stories Name\n\(StoryGoalQueryRun)\n\(db.lastErrorMessage())")
                        
                            let ErrorMessage = "StoryGoalViewController:\(#function) - Failed to get \"\(CurrentUserName!)\"'s Stories Name\n\(db.lastErrorMessage())\n\(StoryGoalQueryRun))"
                        
                            print(ErrorMessage)
                        
                            loggly(LogType.Error, text: ErrorMessage)
                        
                            DoAlert.DisplayAlert(Class: "StoryGoalViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to Failed to get \"\(CurrentUserName!)\"'s Stories Name\n\n\(StoryGoalQueryRun)", DatabaseError: "\(db.lastErrorMessage())")
                        
                    }
                }
                
                
                var CompletedForTable = ""
                if (CompletedDB == 0) {
                    CompletedForTable = "Ongoing"
                } else {
                    CompletedForTable = "Completed"
                }
                
                
                
                
                
//                let dateFormatter = DateFormatter()
//                //dateFormatter.dateFormat = "MMMM d, yyyy"
//                dateFormatter.dateFormat = "MMMM d, yyyy hh:mm a"
//                //dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX

                
                
                if (DailyWordCountDB == "0" ) {
                    DailyWordCountDB = "None"
                }
                if (TotalWordCountDB == "0" ) {
                    TotalWordCountDB = "None"
                }
                if (GoalStartDateDB == "" ) {
                    GoalStartDateDB = "None"
                }
//                else {
//
//
//
//                    let StartDBDate = "\(GoalStartDateDB!)"
//
//                    let timeStamp = dateFormatter.date(from:StartDBDate)
//
//                    GoalStartDateDB = "\(timeStamp!)"
//                }
                if (GoalEndDateDB == "" ) {
                    GoalEndDateDB = "None"
                }
//                else {
//                    let EndDBDate = "\(GoalStartDateDB!)"
//                    let EndDateDate = dateFormatter.date(from:EndDBDate)!
//                    GoalEndDateDB = "\(EndDateDate)"
//                }
                if (CreateDate == "" ) {
                    CreateDate = "Long Ago"
                }
                
                

                ///////////////////////
                
                print("Ikey: \(IkeyDB!) UserIkeyReferenceDB: \(UserIkeyReferenceDB!) CategoryDB: \(CategoryDB!) NameDB: \(storyNameDB) DailyWordCountDB: \(DailyWordCountDB!) TotalWordCountDB: \(TotalWordCountDB!) GoalStartDateDB: \(String(describing: GoalStartDateDB))  GoalSEndDateDB: \(String(describing: GoalEndDateDB)) CreateDate: \(String(describing: CreateDate)) ")
                
                //////////////////////
                
                let StoryGoalToAdd = StoryGoalList(Ikey: IkeyDB, UserIkeyReference: UserIkeyReferenceDB, Category: CategoryDB, Name: storyNameDB, DailyWordCount: DailyWordCountDB ?? "", TotalWordCount: TotalWordCountDB ?? "", GoalStartDate: GoalStartDateDB ?? "" , CreateDate: CreateDate!, GoalEndDate: GoalEndDateDB ?? "" , Status: StatusDB, Completed: "\(CompletedForTable)")
            
                StoryGoalsArray.append(StoryGoalToAdd)
                
            }
            
        } catch {
            
            print("ERROR: StoryGoalViewController:\(#function): Failed to get \"\(CurrentUserName!)\"'s Story Goals\n\(StoryGoalQuery)\n\(db.lastErrorMessage())")
            
            let ErrorMessage = "StoryGoalViewController:\(#function) - Failed to get \"\(CurrentUserName!)\"'s Story Goals\n\(db.lastErrorMessage())\n\(StoryGoalQuery))"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "StoryGoalViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to Failed to get \"\(CurrentUserName!)\"'s Story Goals\n\n\(StoryGoalQuery)", DatabaseError: "\(db.lastErrorMessage())")
            
        }
        
        print("StoryGoalsArray: \(StoryGoalsArray)")
        
        StoryGoalTableView.reloadData()

    }// end of table load data
    
    func LoadStoryListDropDown() {
        
        StoryGoalStoryFilterPopUpButton.removeAllItems()
        
        var storyNameDB = ""
        
        let StoryNameQuery = "select storyname from storytable where userikeyreference = \(CurrentUserIkey!)"
        
        do {
            
            let StoryNameQueryRun = try db.executeQuery(StoryNameQuery, values:nil)
            
            while StoryNameQueryRun.next() {
                storyNameDB = StoryNameQueryRun.string(forColumn: "storyname")!
                
                StoryListArray.append(storyNameDB)
            }
            
        } catch {
            
            print("ERROR: StoryGoalViewController:\(#function): Failed to get \"\(CurrentUserName!)\"'s Story Names\n\(StoryNameQuery)\n\(db.lastErrorMessage())")
            
            let ErrorMessage = "StoryGoalViewController:\(#function) - Failed to get \"\(CurrentUserName!)\"'s Story Names\n\(db.lastErrorMessage())\n\(StoryNameQuery))"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "StoryGoalViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to Failed to get \"\(CurrentUserName!)\"'s Story Names\n\n\(StoryNameQuery)", DatabaseError: "\(db.lastErrorMessage())")
            
        }
        
        StoryGoalCategoryOptionSelectionPopupButton.addItems(withTitles: StoryListArray)
        StoryGoalStoryFilterPopUpButton.addItems(withTitles: StoryListArray)
        
        StoryGoalStoryFilterPopUpButton.insertItem(withTitle: "Select a Story", at: 0)
        //StoryGoalStoryFilterPopUpButton.addItem(withTitle: "Select a Story")

        StoryGoalStoryFilterPopUpButton.selectItem(withTitle: "Select a Story")
        
    }
        
        
        
        
    
    
    
    func numberOfRows(in tableVieww: NSTableView) -> Int {
        
    print("StoryGoalsArray Count: \(StoryGoalsArray.count)")
        
        
    return StoryGoalsArray.count
    
    }
    
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        
        
        
        guard let cell = tableView.makeView(withIdentifier: tableColumn!.identifier, owner: self) as? NSTableCellView else
        {
            print("Table Returned Nil")
            print("tableColumn?.identifier: \(String(describing: tableColumn?.identifier.rawValue))")
            return nil
        }
        
        
        
        
        
        if (tableColumn?.identifier)!.rawValue == "ikey" {
            cell.textField?.stringValue = StoryGoalsArray[row].Ikey
        }
        if (tableColumn?.identifier)!.rawValue == "userikeyreference" {
            cell.textField?.stringValue = StoryGoalsArray[row].UserIkeyReference
        }
        if (tableColumn?.identifier)!.rawValue == "category" {
            cell.textField?.stringValue = StoryGoalsArray[row].Category
        }
        if (tableColumn?.identifier)!.rawValue == "itemname" {
            cell.textField?.stringValue = StoryGoalsArray[row].Name
        }
        if (tableColumn?.identifier)!.rawValue == "dailywordcount" {
            cell.textField?.stringValue = StoryGoalsArray[row].DailyWordCount
        }
        if (tableColumn?.identifier)!.rawValue == "totalwordcount" {
            cell.textField?.stringValue = StoryGoalsArray[row].TotalWordCount
        }
        if (tableColumn?.identifier)!.rawValue == "startdate" {
            cell.textField?.stringValue = StoryGoalsArray[row].GoalStartDate
        }
        if (tableColumn?.identifier)!.rawValue == "enddate" {
            cell.textField?.stringValue = StoryGoalsArray[row].GoalEndDate
        }
        if (tableColumn?.identifier)!.rawValue == "createdate" {
            cell.textField?.stringValue = StoryGoalsArray[row].CreateDate
        }
        if (tableColumn?.identifier)!.rawValue == "status" {
            
            let imageNeeded = StoryGoalsArray[row].Status
            
            if (imageNeeded == "t") {
                
                cell.imageView?.image = NSImage(named: NSImage.Name(rawValue: "statusColorActive.png"))
                
//                if (CurrentAppearanceStoryGoalViewController == "Dark") {
//                    cell.imageView?.image = NSImage(named: NSImage.Name(rawValue: "statusDarkActive.png"))
//                } else if (CurrentAppearanceStoryGoalViewController == "Light") {
//                    cell.imageView?.image = NSImage(named: NSImage.Name(rawValue: "statusColorActive.png"))
//                }
            } else if (imageNeeded == "f") {
                
                cell.imageView?.image = NSImage(named: NSImage.Name(rawValue: "statusColorNotActive.png"))
                
//                if (CurrentAppearanceStoryGoalViewController == "Dark") {
//                    cell.imageView?.image = NSImage(named: NSImage.Name(rawValue: "statusDarkNotActive.png"))
//                } else if (CurrentAppearanceStoryGoalViewController == "Light") {
//                    cell.imageView?.image = NSImage(named: NSImage.Name(rawValue: "statusColorNotActive.png"))
//                }
            }
  
        }
        if (tableColumn?.identifier)!.rawValue == "completed" {
            cell.textField?.stringValue = StoryGoalsArray[row].Completed
        }

        return cell
        
    }
    
    func tableViewSelectionDidChange(_ notification: Notification) {
        
        let SelectedRow = StoryGoalTableView.selectedRow
        
        print("SelectedRow: \(SelectedRow)")
        
        if (SelectedRow == -1) {
            
            itemStatusCheckbox.isEnabled = false
            itemStatusCheckbox.state = NSControl.StateValue(rawValue: 0)
            
            StoryGoalOnOffSwitchLabel.stringValue = "Activate:"
            StoryGoalDeleteGoalButton.isEnabled = false
            itemStatusCheckbox.isEnabled = false
            itemStatusCheckbox.state = NSControl.StateValue(rawValue: 0)
            
//            StoryGoalOnOffSwitch.isEnabled = false
//            StoryGoalOnOffSwitch.setOn(on: false, animated: true)
            
            return
        }
        
        let Status = StoryGoalsArray[SelectedRow].Status
        let Completed = StoryGoalsArray[SelectedRow].Completed

        if (Completed == "Ongoing") {
            itemStatusCheckbox.isEnabled = true
        }
        
        if (Status == "t") {
            StoryGoalOnOffSwitchLabel.stringValue = "Deactivate:"
            itemStatusCheckbox.isEnabled = true
            itemStatusCheckbox.state = NSControl.StateValue(rawValue: 1)
//            StoryGoalOnOffSwitch.isEnabled = true
//            StoryGoalOnOffSwitch.setOn(on: true, animated: true)
        } else if (Status == "f") {
            StoryGoalOnOffSwitchLabel.stringValue = "Activate:"
            itemStatusCheckbox.state = NSControl.StateValue(rawValue: 0)
//            StoryGoalOnOffSwitch.isEnabled = false
//            StoryGoalOnOffSwitch.setOn(on: false, animated: true)
        }
        
        
        
//        let Name = StoryGoalsArray[SelectedRow].Name
//        print("Selected Story Name: \"\(Name)\" ")
        
        //let UserIkeyReference = StoryGoalsArray[SelectedRow].UserIkeyReference
        //let Ikey = StoryGoalsArray[SelectedRow].Ikey
        //let Category = StoryGoalsArray[SelectedRow].Category
        
        //print("Table Selection - Ikey: \"\(Ikey)\" Name: \"\(Name)\" Category: \"\(Category)\" ")
        
//        if (UserIkeyReference == "0") {
//
//            print("UserIkeyReference = 0")
//
//            let alert = NSAlert()
//            alert.messageText = "This is a Default Goal"
//            alert.informativeText = "You are unable to delete a Default Goal"
//            alert.alertStyle = .warning
//            alert.addButton(withTitle: "OK")
//
//            if (CurrentAppearanceStoryGoalViewController == "Dark") {
//                alert.window.backgroundColor = AlertBackgroundColorDarkMode
//            } else {
//                alert.window.backgroundColor = WindowBackgroundColor
//            }
//
//            alert.window.titlebarAppearsTransparent = true
//
//            alert.runModal()
//
//            return
//        }
        
        
        
        
        
        let isIndexValid = StoryGoalsArray.indices.contains(SelectedRow)
        
        if isIndexValid {
            
            //print("ITEM IS VALID\nTable Selection - Ikey: \"\(Ikey)\" Name: \"\(Name)\" Category: \"\(Category)\" ")
            StoryGoalDeleteGoalButton.isEnabled = true
            
        } else {
            
            //print("**** IS NOT VALID\nTable Selection - Ikey: \"\(Ikey)\" Name: \"\(Name)\" Category: \"\(Category)\" ")
            StoryGoalDeleteGoalButton.isEnabled = false
        }
        
        
    }
    
    @IBAction func itemStatusCheckboxAction(_ sender: Any) {
        
        
        let SelectedRow = StoryGoalTableView.selectedRow

        print("SelectedRow: \(SelectedRow)")

        if (SelectedRow == -1) {

            itemStatusCheckbox.isEnabled = false
            itemStatusCheckbox.state = NSControl.StateValue(rawValue: 0)
            //itemStatusCheckbox.setOn(on: false, animated: true)

            return
        }

        let Ikey = StoryGoalsArray[SelectedRow].Ikey
        let Status = StoryGoalsArray[SelectedRow].Status
        let Completed = StoryGoalsArray[SelectedRow].Completed





        if (Completed == "Completed") {

            let alert = NSAlert()
            alert.messageText = "Completed Goal Selected"
            alert.informativeText = "Completed Goals are not able to be Inactivated"
            alert.alertStyle = .warning
            alert.addButton(withTitle: "Ok")


            if (CurrentAppearanceStoryGoalViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }

            alert.window.titlebarAppearsTransparent = true

            alert.runModal()

            StoryGoalOnOffSwitchLabel.stringValue = "Deactivate:"
//            StoryGoalOnOffSwitch.isEnabled = true
//            StoryGoalOnOffSwitch.setOn(on: true, animated: true)
            
            itemStatusCheckbox.isEnabled = true
            itemStatusCheckbox.state = NSControl.StateValue(rawValue: 1)

            return
        }

        if (Status == "t") {

            let SelectedRow = StoryGoalTableView.selectedRow

            print("SelectedRow: \(SelectedRow)")

            dbQueue.inTransaction { _, rollback in

                let UpdateDBStatement = String(format:
                    "UPDATE storygoaltable SET status = 'f' WHERE ikey = \(Ikey)"
                )

                let Success = db.executeStatements(UpdateDBStatement)

                if (!Success) {

                    rollback.pointee = true

                    loggly(LogType.Warnings, text: "StoryGoalViewController-\(#function): \nUnable to set status = 'f' in storygoaltable\n\(UpdateDBStatement)\n\(db.lastErrorMessage())")

                    DoAlert.DisplayAlert(Class: "StoryGoalViewController", Level: 1, Function: "\(#function)", MessageText: "An Error has occurred", InformativeText: "Database changes were rolled back from updating.\nPlease Quit and try again.\nIf this continues please restart your computer.", DatabaseError: "\nSQL Query Failed:\n\n\(UpdateDBStatement)\n\nLastErrorMessage: \"\(db.lastErrorMessage())\"")

                    return

                }// end of !Success



            }// end of dbQueue.inTransaction

            StoryGoalOnOffSwitchLabel.stringValue = "Activate:"

        } else if (Status == "f") {

            let SelectedRow = StoryGoalTableView.selectedRow

            print("SelectedRow: \(SelectedRow)")

            dbQueue.inTransaction { _, rollback in

                let UpdateDBStatement = String(format:
                    "UPDATE storygoaltable SET status = 't' WHERE ikey = \(Ikey)"
                )

                let Success = db.executeStatements(UpdateDBStatement)

                if (!Success) {

                    rollback.pointee = true

                    loggly(LogType.Warnings, text: "StoryGoalViewController-\(#function): \nUnable to set status = 't' in storygoaltable\n\(UpdateDBStatement)\n\(db.lastErrorMessage())")

                    DoAlert.DisplayAlert(Class: "StoryGoalViewController", Level: 1, Function: "\(#function)", MessageText: "An Error has occurred", InformativeText: "Database changes were rolled back from updating.\nPlease Quit and try again.\nIf this continues please restart your computer.", DatabaseError: "\nSQL Query Failed:\n\n\(UpdateDBStatement)\n\nLastErrorMessage: \"\(db.lastErrorMessage())\"")

                    return

                }// end of !Success

            }// end of dbQueue.inTransaction

            StoryGoalOnOffSwitchLabel.stringValue = "Deactivate:"

        }


        LoadStoryGoalTableData()
        let indexset: IndexSet = [SelectedRow]
        StoryGoalTableView.reloadData()
        StoryGoalTableView.selectRowIndexes(indexset, byExtendingSelection: false)
        
        
    }// end of itemStatusCheckboxAction
    
    
//    @IBAction func StoryGoalOnOffSwitchAction(_ sender: NSSwitch) {
        
//        let SelectedRow = StoryGoalTableView.selectedRow
//
//        print("SelectedRow: \(SelectedRow)")
//
//        if (SelectedRow == -1) {
//
//            StoryGoalOnOffSwitch.isEnabled = false
//            StoryGoalOnOffSwitch.setOn(on: false, animated: true)
//
//            return
//        }
//
//        let Ikey = StoryGoalsArray[SelectedRow].Ikey
//        let Status = StoryGoalsArray[SelectedRow].Status
//        let Completed = StoryGoalsArray[SelectedRow].Completed
//
//
//
//
//
//        if (Completed == "Completed") {
//
//            let alert = NSAlert()
//            alert.messageText = "Completed Goal Selected"
//            alert.informativeText = "Completed Goals are not able to be Inactivated"
//            alert.alertStyle = .warning
//            alert.addButton(withTitle: "Ok")
//
//
//            if (CurrentAppearanceStoryGoalViewController == "Dark") {
//                alert.window.backgroundColor = AlertBackgroundColorDarkMode
//            } else {
//                alert.window.backgroundColor = WindowBackgroundColor
//            }
//
//            alert.window.titlebarAppearsTransparent = true
//
//            alert.runModal()
//
//            StoryGoalOnOffSwitchLabel.stringValue = "Deactivate:"
//            StoryGoalOnOffSwitch.isEnabled = true
//            StoryGoalOnOffSwitch.setOn(on: true, animated: true)
//
//            return
//        }
//
//        if (Status == "t") {
//
//            let SelectedRow = StoryGoalTableView.selectedRow
//
//            print("SelectedRow: \(SelectedRow)")
//
//            dbQueue.inTransaction { _, rollback in
//
//                let UpdateDBStatement = String(format:
//                    "UPDATE storygoaltable SET status = 'f' WHERE ikey = \(Ikey)"
//                )
//
//                let Success = db.executeStatements(UpdateDBStatement)
//
//                if (!Success) {
//
//                    rollback.pointee = true
//
//                    loggly(LogType.Warnings, text: "StoryGoalViewController-\(#function): \nUnable to set status = 'f' in storygoaltable\n\(UpdateDBStatement)\n\(db.lastErrorMessage())")
//
//                    DoAlert.DisplayAlert(Class: "StoryGoalViewController", Level: 1, Function: "\(#function)", MessageText: "An Error has occurred", InformativeText: "Database changes were rolled back from updating.\nPlease Quit and try again.\nIf this continues please restart your computer.", DatabaseError: "\nSQL Query Failed:\n\n\(UpdateDBStatement)\n\nLastErrorMessage: \"\(db.lastErrorMessage())\"")
//
//                    return
//
//                }// end of !Success
//
//
//
//            }// end of dbQueue.inTransaction
//
//            StoryGoalOnOffSwitchLabel.stringValue = "Activate:"
//
//        } else if (Status == "f") {
//
//            let SelectedRow = StoryGoalTableView.selectedRow
//
//            print("SelectedRow: \(SelectedRow)")
//
//            dbQueue.inTransaction { _, rollback in
//
//                let UpdateDBStatement = String(format:
//                    "UPDATE storygoaltable SET status = 't' WHERE ikey = \(Ikey)"
//                )
//
//                let Success = db.executeStatements(UpdateDBStatement)
//
//                if (!Success) {
//
//                    rollback.pointee = true
//
//                    loggly(LogType.Warnings, text: "StoryGoalViewController-\(#function): \nUnable to set status = 't' in storygoaltable\n\(UpdateDBStatement)\n\(db.lastErrorMessage())")
//
//                    DoAlert.DisplayAlert(Class: "StoryGoalViewController", Level: 1, Function: "\(#function)", MessageText: "An Error has occurred", InformativeText: "Database changes were rolled back from updating.\nPlease Quit and try again.\nIf this continues please restart your computer.", DatabaseError: "\nSQL Query Failed:\n\n\(UpdateDBStatement)\n\nLastErrorMessage: \"\(db.lastErrorMessage())\"")
//
//                    return
//
//                }// end of !Success
//
//            }// end of dbQueue.inTransaction
//
//            StoryGoalOnOffSwitchLabel.stringValue = "Deactivate:"
//
//        }
//
//
//        LoadStoryGoalTableData()
//        let indexset: IndexSet = [SelectedRow]
//        StoryGoalTableView.reloadData()
//        StoryGoalTableView.selectRowIndexes(indexset, byExtendingSelection: false)
        
        
        
        
        
        
        
//    }//end of StoryGoalOnOffSwitchAction
    
    
    
    
    
    
    
    
    
    
    
    @IBAction func StoryGoalAddGoalButton(_ sender: Any) {
        
//        var nowDate:Date = StoryStartGoalDatePicker.dateValue
//        var endDate:Date = StoryEndGoalDatePicker.dateValue
//
////        print("nowDate: \(nowDate)")
////        print("endDate: \(endDate)")
//
//        if StoryNoStartGoalCheckBox.state == .on {
//
//            nowDate = Date()
//
//        }
//
//        if StoryNoEndGoalCheckBox.state == .on {
//
//            endDate = Date()
//
//        }
        
        if (StoryGoalCategoryOptionSelectionPopupButton.titleOfSelectedItem == "Select a Story") {
            
            let alert = NSAlert()
            alert.messageText = "At this time any Goals need to be assigned to a story."
            alert.informativeText = "Please select a story. "
            alert.alertStyle = .warning
            alert.addButton(withTitle: "Ok")
            
            
            if (CurrentAppearanceStoryGoalViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.window.titlebarAppearsTransparent = true
            
            alert.runModal()
            
            return
            
        }
        
        
        
        if ("\(StoryStartGoalDatePicker.dateValue)" == "\(StoryEndGoalDatePicker.dateValue)" && StoryGoalDailyWordCountTextField.stringValue.count < 1 && StoryGoalTotalWordCountTextField.stringValue.count < 1) {
            let alert = NSAlert()
            alert.messageText = "Both Start and End Dates are the same"
            alert.informativeText = "Are you really that fast ?"
            alert.alertStyle = .warning
            alert.addButton(withTitle: "Yes but lets change it")
            alert.addButton(withTitle: "Yes Most Definitly")
            
            if (CurrentAppearanceStoryGoalViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.window.titlebarAppearsTransparent = true
            
            let returnCode = alert.runModal()
            print("returnCode: \(returnCode)")
            
            if (returnCode.rawValue == 1000) {
                //print("Yes but lets change it")
                return
            }
            if (returnCode.rawValue == 1001) {
                //print("Yes Most Definitly")
            }
            
            
            
        }
        
        
        print("StoryGoalCategoryOptionSelectionPopupButton.titleOfSelectedItem = \"\(StoryGoalCategoryOptionSelectionPopupButton.titleOfSelectedItem!)\" ")
        
        
        
        var storyIkeyDB = ""
        
        if (StoryGoalCategoryOptionSelectionPopupButton.titleOfSelectedItem != "Select a Story") {
            
            let StoryNameQuery = "select ikey from storytable where storyname = \"\(StoryGoalCategoryOptionSelectionPopupButton.titleOfSelectedItem!)\""
            print("StoryNameQuery: \"\(StoryNameQuery)\" ")
            
            do {
                
                let StoryNameQueryRun = try db.executeQuery(StoryNameQuery, values:nil)
                
                while StoryNameQueryRun.next() {
                    storyIkeyDB = StoryNameQueryRun.string(forColumn: "ikey")!
                }
                
            } catch {
                
                print("ERROR: StoryGoalViewController:\(#function): Failed to get \"\(CurrentUserName!)\"'s Stories Name 2 \n\(StoryNameQuery)\n\(db.lastErrorMessage())")
                
                let ErrorMessage = "StoryGoalViewController:\(#function) - Failed to get \"\(CurrentUserName!)\"'s Stories Name 2 \n\(db.lastErrorMessage())\n\(StoryNameQuery))"
                
                print(ErrorMessage)
                
                loggly(LogType.Error, text: ErrorMessage)
                
                DoAlert.DisplayAlert(Class: "StoryGoalViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to Failed to get \"\(CurrentUserName!)\"'s Stories Name 2 \n\n\(StoryNameQuery)", DatabaseError: "\(db.lastErrorMessage())")
                
            }
        } else {
            
            storyIkeyDB = "-1"
            
        }// end of if (StoryGoalCategoryOptionSelectionPopupButton.stringValue != "Select a Story")
        
        
        
        
        
        //let TodayDate = NSDate()

        let dateFormatter = DateFormatter()
        //dateFormatter.dateFormat = "MMMM d, yyyy"
        dateFormatter.dateFormat = "MMMM d, yyyy H:mm a"
        //dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        
        
        let timeStamp = dateFormatter.string(from:Date())
        print("timeStamp: \"\(timeStamp)\" ")
        
        
        
        //var StoryStartGoal = Date()
        //var StoryEndGoal = Date()
        
        let calendar = Calendar.current
        let c = NSDateComponents()
        
        var StoryStartGoalString = ""
        var StoryEndGoalString = ""
        
        
        if (StoryNoStartGoalCheckBox.state == .on) {
            
        } else {
            //StoryStartGoal = dateFormatter.date(from: "\(StoryStartGoalDatePicker.dateValue)")!
            

            
            print("StoryStartGoalDatePicker.dateValue =  \"\(StoryStartGoalDatePicker.dateValue)\" ")
            
            c.year = calendar.component(.year, from: StoryStartGoalDatePicker.dateValue)
            c.month = calendar.component(.month, from: StoryStartGoalDatePicker.dateValue)
            c.day = calendar.component(.day, from: StoryStartGoalDatePicker.dateValue)
            

            let Startdate = NSCalendar(identifier: NSCalendar.Identifier.gregorian)?.date(from: c as DateComponents)
            
            print("Startdate: \(Startdate!)")
            
            StoryStartGoalString = dateFormatter.string(from:Startdate!)
            
        }
      
        if (StoryNoEndGoalCheckBox.state == .on) {
            
        } else {
            
            print("StoryEndGoalDatePicker.dateValue =  \"\(StoryEndGoalDatePicker.dateValue)\" ")
            
            c.year = calendar.component(.year, from: StoryEndGoalDatePicker.dateValue)
            c.month = calendar.component(.month, from: StoryEndGoalDatePicker.dateValue)
            c.day = calendar.component(.day, from: StoryEndGoalDatePicker.dateValue)
            c.hour = calendar.component(.hour, from: StoryEndGoalDatePicker.dateValue)
            c.minute = calendar.component(.minute, from: StoryEndGoalDatePicker.dateValue)
            
            let Enddate = NSCalendar(identifier: NSCalendar.Identifier.gregorian)?.date(from: c as DateComponents)
            
            print("Enddate: \(Enddate!)")
            
            StoryEndGoalString = dateFormatter.string(from:Enddate!)
            
            
        }
        
        
        
        
        
        ///////////////////////////
        
        var UpdateStoryGoalSqlquery = String()
        
        do {
            UpdateStoryGoalSqlquery = String(format: "INSERT INTO storygoaltable(ikey, userikeyreference, storyikeyreference, dailywordcount, totalwordcount, startgoaldate, endgoaldate, createdate, status, completed) VALUES ((select MAX(ikey) + 1 from storygoaltable),\(CurrentUserIkey!),\(storyIkeyDB),\(StoryGoalDailyWordCountTextField.integerValue),\(StoryGoalTotalWordCountTextField.integerValue),'\(StoryStartGoalString)','\(StoryEndGoalString)','\(timeStamp)','t','0');")
            
            print("UpdateStoryGoalSqlquery: \(UpdateStoryGoalSqlquery)")
            
            
            
            try db.executeUpdate(UpdateStoryGoalSqlquery, values: nil)
            
        } catch let error as NSError {
            
            print("ERROR: \(error)\n\n\(db.lastErrorMessage())")
            loggly(LogType.Error, text: "StoryGoalViewController-\(#function) Query Failed:\n \"\(UpdateStoryGoalSqlquery)\"")
            DoAlert.DisplayAlert(Class: "StoryGoalViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(UpdateStoryGoalSqlquery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
            return
        }
        
        
        
        
        
        LoadStoryGoalTableData()
        StoryGoalTableView.reloadData()
        
        
        
        
    }
    
    
    @IBAction func deleteGoalButton(_ sender: NSButton) {
        
        let SelectedRow = StoryGoalTableView.selectedRow
        
        //print("SelectedRow: \(SelectedRow)")
        
        if (SelectedRow == -1) {
            StoryGoalDeleteGoalButton.isEnabled = false
            return
        }
        
        let Name = StoryGoalsArray[SelectedRow].Name
        let UserIkeyReference = StoryGoalsArray[SelectedRow].UserIkeyReference
        let Ikey = StoryGoalsArray[SelectedRow].Ikey
        //let Category = StoryGoalsArray[SelectedRow].Category
        
        //print("Table Selection - Ikey: \"\(Ikey)\" Name: \"\(Name)\" Category: \"\(Category)\" ")
        
        if (UserIkeyReference == "0") {

            let alert = NSAlert()
            alert.messageText = "This is a Default Goal"
            alert.informativeText = "You are unable to delete Default Goals"
            alert.alertStyle = .warning
            alert.addButton(withTitle: "OK")

            if (CurrentAppearanceStoryGoalViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }

            alert.window.titlebarAppearsTransparent = true
            alert.runModal()

            return
        }
        
        let alert = NSAlert()
        alert.messageText = "Delete Selected Goal ?"
        alert.informativeText = "Are you sure you want to delete the selected Goal \"\(Name)\" ?"
        alert.alertStyle = .warning
        alert.addButton(withTitle: "Cancel")
        alert.addButton(withTitle: "Yes")
        
        if (CurrentAppearanceStoryGoalViewController == "Dark") {
            alert.window.backgroundColor = AlertBackgroundColorDarkMode
        } else {
            alert.window.backgroundColor = WindowBackgroundColor
        }
        
        alert.window.titlebarAppearsTransparent = true

        let returnCode = alert.runModal()
        print("returnCode: \(returnCode)")
        
        if (returnCode.rawValue == 1001) {
            print("Yes Pressed")
            
            var DeleteStoryGoalSqlQuery = String()
            
            do {
                
                DeleteStoryGoalSqlQuery = String(format: "DELETE FROM storygoaltable WHERE ikey = \(Ikey) ")
                
                print("DeleteStoryGoalSqlQuery: \(DeleteStoryGoalSqlQuery)")
                
                try db.executeUpdate(DeleteStoryGoalSqlQuery, values: nil)
                
            } catch let error as NSError {
                
                print("ERROR: \(error)\n\n\(db.lastErrorMessage())")
                loggly(LogType.Error, text: "StoryGoalViewController-\(#function) Query Failed:\n \"\(DeleteStoryGoalSqlQuery)\"")
                DoAlert.DisplayAlert(Class: "StoryGoalViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(DeleteStoryGoalSqlQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }

        }
        
        if returnCode.rawValue == 1000 {
            print("Cancel Pressed")
            
        }
        
        
        
        LoadStoryGoalTableData()
        StoryGoalTableView.reloadData()
        
        
        
    }// end of deleteGoalButton
    
    @IBAction func editGoalButton(_ sender: NSButton) {
        
//        db.open()
//
//
//        var storyIkeyDB = ""
//
//        let StoryNameQuery = "select ikey from storytable where ikey = \(StoryGoalCategoryOptionSelectionPopupButton.stringValue)"
//        do {
//
//            let StoryNameQueryRun = try db.executeQuery(StoryNameQuery, values:nil)
//
//            while StoryNameQueryRun.next() {
//                storyIkeyDB = StoryNameQueryRun.string(forColumn: "ikey")!
//            }
//
//        } catch {
//
//            print("ERROR: StoryGoalViewController:\(#function): Failed to get \"\(CurrentUserName!)\"'s Stories Name 2 \n\(StoryNameQuery)\n\(db.lastErrorMessage())")
//
//            let ErrorMessage = "StoryGoalViewController:\(#function) - Failed to get \"\(CurrentUserName!)\"'s Stories Name 2 \n\(db.lastErrorMessage())\n\(StoryNameQuery))"
//
//            print(ErrorMessage)
//
//            loggly(LogType.Error, text: ErrorMessage)
//
//            DoAlert.DisplayAlert(Class: "StoryGoalViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to Failed to get \"\(CurrentUserName!)\"'s Stories Name 2 \n\n\(StoryNameQuery)", DatabaseError: "\(db.lastErrorMessage())")
//
//        }
        
        

        
        //////////////////////////////////
        
        // (ikey,name,createdate,composition,createdby,usedfor,history,itemcategory,itemsubcategory,userikeyreference)
        
//        let today = NSData()
//
//        let InsertNewItemQuery = "INSERT INTO storygoaltable (ikey,userikeyreference,storyikeyreference,dailywordcount,totalwordcount,goaldate,createdate) VALUES ((select MAX(ikey) + 1 from storygoaltable),\"\(CurrentUserIkey!)\",\(storyIkeyDB),\(StoryGoalDailyWordCountTextField.stringValue),\(StoryGoalTotalWordCountTextField.stringValue),\(StoryGoalDatePicker.dateValue),\(today)"
//
//        print("InsertNewItemQuery: \n\(InsertNewItemQuery)")
//
//
//        let Success = db.executeStatements(InsertNewItemQuery)
//
//        if (!Success) {
//
//            let ErrorMessage = "Failed to INSERT New Story Goal\n\n\(InsertNewItemQuery)\n\n\(db.lastErrorMessage())"
//
//            loggly(LogType.Error, text: ErrorMessage)
//
//            DoAlert.DisplayAlert(Class: "StoryGoalViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed INSERT New Story Goal\n\n\(InsertNewItemQuery)", DatabaseError: "\(db.lastErrorMessage())")
//            return
//
//        } else {
//
//            print("SUCCESSFULLY INSERTED NEW Story Goal ")
//
//        }
        
        
    }//end of editGoalButton
    
    
    
    
    override func controlTextDidChange(_ obj: Notification) {
        
        
        print("controlTextDidChange RAN ")
        
//        print("self.StoryGoalTimeTextField.stringValue: \"\(self.StoryGoalTimeTextField.stringValue)\" ")
//
//        if (self.StoryGoalTimeTextField.stringValue.count > 1) {
//            print("StoryGoalTimeTextField CHANGED")
//        } else {
//            return
//        }
        
        
        
    }
    
    
    @IBAction func StoryNoStartDateCheckBoxAction(_ sender: NSButton) {
        
        
        if StoryNoStartGoalCheckBox.state == .on {
            
            
//            // 1000-01-01 00:00:00 +0000
//            let dateFormatter = DateFormatter()
//            //dateFormatter.dateFormat = "MMMM d, yyyy"
//            //teFormatter.dateFormat = "yyyy-MM-dd HH:mm:ssZ"
//            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//            //dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
//            dateFormatter.timeZone = TimeZone.current
//            dateFormatter.locale = Locale.current
//
//            let timeStamp = dateFormatter.date(from: "1000-01-01 00:00:01")
//            print("timeStamp: \"\(timeStamp ?? Date())\" ")
            
            
            
            //StoryStartGoalDatePicker.dateValue = timeStamp ?? Date()
            
            
            
            
           
            StoryGoalStartDateDatePcker(self)
            
            
            StoryStartGoalDatePicker.isEnabled = false
            
        } else {
            StoryStartGoalDatePicker.isEnabled = true
            StoryStartGoalDatePicker.dateValue = Date()
            StoryGoalStartDateDatePcker(self)
        }
        
        
    }
    
    
    @IBAction func StoryNoEndDateCheckBoxAction(_ sender: NSButton) {
        
        if StoryNoEndGoalCheckBox.state == .on {
            StoryEndGoalDatePicker.isEnabled = false
            
        } else {
            StoryEndGoalDatePicker.isEnabled = true
            
        }
        
    }
    
    
    
    
    @IBAction func StoryGoalStartDateDatePcker(_ sender: Any) {
        
        
        var nowDate = StoryStartGoalDatePicker.dateValue
        var endDate = StoryEndGoalDatePicker.dateValue
        
        if StoryNoStartGoalCheckBox.state == .on {
            
            nowDate = Date()
            StoryGoalDurationLabel.stringValue = "\(StoryGoalDurationLabel.stringValue) from now"
            
        }
        
        if StoryNoEndGoalCheckBox.state == .on {
            
            endDate = Date()
            
        }
        
        
        var timeDuration = ""
        
        if (nowDate == Date() && endDate == Date() ) {
            timeDuration = "No Time"
            return
        }
        
        if endDate.timeIntervalSince(nowDate).sign == FloatingPointSign.minus {
            // endDate is in past
            let timeDuration = "TIME TRAVELER !!"
            StoryGoalDurationLabel.stringValue = timeDuration
            return
        }
        
        
        
        
        let components = Set<Calendar.Component>([.second, .minute, .hour, .day, .month, .year, .weekOfYear])
        let differenceOfDate = Calendar.current.dateComponents(components, from: nowDate, to: endDate)
        
        print (differenceOfDate)
        
        
        let years = differenceOfDate.year
        let months = differenceOfDate.month
        let weeks = differenceOfDate.weekOfYear
        let days = differenceOfDate.day
        let hours = differenceOfDate.hour
        let minutes = differenceOfDate.minute
        
        
        print("TimeFrame: Year: \(years ?? 0) Months: \(months ?? 0) Weeks: \(weeks ?? 0) Days: \(days ?? 0) Hours: \(hours ?? 0) Minutes: \(minutes ?? 0) ")
        
        //var timeDuration = "Y:\(years ?? 0) M:\(months ?? 0) W:\(weeks ?? 0) D:\(days ?? 0) H:\(hours ?? 0) M:\(minutes ?? 0)"
        
        
        
        if (years != 0 ) {
            if (weeks == 0) {
                timeDuration = "Y:\(years ?? 0) M:\(months ?? 0) D:\(days ?? 0) H:\(hours ?? 0) M:\(minutes ?? 0)"
            } else {
                timeDuration = "Y:\(years ?? 0) M:\(months ?? 0) W:\(weeks ?? 0) D:\(days ?? 0) H:\(hours ?? 0) M:\(minutes ?? 0)"
            }
        } else if (years == 0 && months != 0) {
            if (weeks == 0) {
                timeDuration = "M:\(months ?? 0) D:\(days ?? 0) H:\(hours ?? 0) M:\(minutes ?? 0)"
            } else {
                timeDuration = "M:\(months ?? 0) W:\(weeks ?? 0) D:\(days ?? 0) H:\(hours ?? 0) M:\(minutes ?? 0)"
            }
        } else if (months == 0 && weeks != 0) {
            timeDuration = "W:\(weeks ?? 0) D:\(days ?? 0) H:\(hours ?? 0) M:\(minutes ?? 0)"
        } else if (weeks == 0 && days  != 0) {
            timeDuration = "D:\(days ?? 0) H:\(hours ?? 0) M:\(minutes ?? 0)"
        } else if (days  == 0 && hours != 0) {
            timeDuration = "H:\(hours ?? 0) M:\(minutes ?? 0)"
        } else if (hours == 0 && minutes != 0) {
            timeDuration = "M:\(minutes ?? 0)"
        } else if (minutes == 0) {
            timeDuration = "No Time Flat !"
        }
        
       
        StoryGoalDurationLabel.stringValue = timeDuration
        
        if StoryNoStartGoalCheckBox.state == .on {
            
            StoryGoalDurationLabel.stringValue = "\(StoryGoalDurationLabel.stringValue) from now"
            
        }
        

        //StoryGoalDurationLabel.string
        
    }// end of StoryGoalStartDateDatePcker
    
    @IBAction func StoryGoalEndTimeDatePicker(_ sender: Any) {
        
        
        var nowDate = StoryStartGoalDatePicker.dateValue
        var endDate = StoryEndGoalDatePicker.dateValue
        
        if StoryNoStartGoalCheckBox.state == .on {
            
            nowDate = Date()
            StoryGoalDurationLabel.stringValue = "\(StoryGoalDurationLabel.stringValue) from now"
            
        }
        
        if StoryNoEndGoalCheckBox.state == .on {
            
            endDate = Date()
            
        }
        
        
        var timeDuration = ""
        
        if (nowDate == Date() && endDate == Date() ) {
            timeDuration = "No Time"
            return
        }
        
        if endDate.timeIntervalSince(nowDate).sign == FloatingPointSign.minus {
            // endDate is in past
            let timeDuration = "TIME TRAVELER !!"
            StoryGoalDurationLabel.stringValue = timeDuration
            return
        }
        
        
        
        
        let components = Set<Calendar.Component>([.second, .minute, .hour, .day, .month, .year, .weekOfYear])
        let differenceOfDate = Calendar.current.dateComponents(components, from: nowDate, to: endDate)
        
        print (differenceOfDate)
        
        
        let years = differenceOfDate.year
        let months = differenceOfDate.month
        let weeks = differenceOfDate.weekOfYear
        let days = differenceOfDate.day
        let hours = differenceOfDate.hour
        let minutes = differenceOfDate.minute
        
        
        print("TimeFrame: Year: \(years ?? 0) Months: \(months ?? 0) Weeks: \(weeks ?? 0) Days: \(days ?? 0) Hours: \(hours ?? 0) Minutes: \(minutes ?? 0) ")
        
        //var timeDuration = "Y:\(years ?? 0) M:\(months ?? 0) W:\(weeks ?? 0) D:\(days ?? 0) H:\(hours ?? 0) M:\(minutes ?? 0)"
        
        
        
        if (years != 0 ) {
            if (weeks == 0) {
                timeDuration = "Y:\(years ?? 0) M:\(months ?? 0) D:\(days ?? 0) H:\(hours ?? 0) M:\(minutes ?? 0)"
            } else {
                timeDuration = "Y:\(years ?? 0) M:\(months ?? 0) W:\(weeks ?? 0) D:\(days ?? 0) H:\(hours ?? 0) M:\(minutes ?? 0)"
            }
        } else if (years == 0 && months != 0) {
            if (weeks == 0) {
                timeDuration = "M:\(months ?? 0) D:\(days ?? 0) H:\(hours ?? 0) M:\(minutes ?? 0)"
            } else {
                timeDuration = "M:\(months ?? 0) W:\(weeks ?? 0) D:\(days ?? 0) H:\(hours ?? 0) M:\(minutes ?? 0)"
            }
        } else if (months == 0 && weeks != 0) {
            timeDuration = "W:\(weeks ?? 0) D:\(days ?? 0) H:\(hours ?? 0) M:\(minutes ?? 0)"
        } else if (weeks == 0 && days  != 0) {
            timeDuration = "D:\(days ?? 0) H:\(hours ?? 0) M:\(minutes ?? 0)"
        } else if (days  == 0 && hours != 0) {
            timeDuration = "H:\(hours ?? 0) M:\(minutes ?? 0)"
        } else if (hours == 0 && minutes != 0) {
            timeDuration = "M:\(minutes ?? 0)"
        } else if (minutes == 0) {
            timeDuration = "No Time Flat !"
        }
        
        
        StoryGoalDurationLabel.stringValue = timeDuration
        
        if StoryNoStartGoalCheckBox.state == .on {
            
            StoryGoalDurationLabel.stringValue = "\(StoryGoalDurationLabel.stringValue) from now"
            
        }
        
        
    }// end of StoryGoalStartTimeDatePicker
    
    
    
    
    
    
    @IBAction func StoryGoalTableShowCreateDateColumnAction(_ sender: Any) {
        
        if (StoryGoalTableView.tableColumn(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "createdate"))?.isHidden == true) {
            StoryGoalTableView.tableColumn(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "createdate"))?.isHidden = false
        } else {
            StoryGoalTableView.tableColumn(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "createdate"))?.isHidden = true
        }
        
        
    }
    
    @IBAction func StoryGoalTableShowIkeyColumnAction(_ sender: Any) {
        
        
        if (StoryGoalTableView.tableColumn(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "ikey"))?.isHidden == true) {
            StoryGoalTableView.tableColumn(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "ikey"))?.isHidden = false
        } else {
            StoryGoalTableView.tableColumn(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "ikey"))?.isHidden = true
        }
        
    }
    
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    @IBAction func storyGoalCloseButton(_ sender: NSButton) {
        
        self.view.window?.close()
        
    }// end of storyGoalCloseButton
    
    
    
    
    
    
    
}// end of StoryGoalViewController




extension Date {
    
    func interval(ofComponent comp: Calendar.Component, fromDate date: Date) -> Int {
        
        let currentCalendar = Calendar.current
        
        guard let start = currentCalendar.ordinality(of: comp, in: .era, for: date) else { return 0 }
        guard let end = currentCalendar.ordinality(of: comp, in: .era, for: self) else { return 0 }
        
        return end - start
    }
}
