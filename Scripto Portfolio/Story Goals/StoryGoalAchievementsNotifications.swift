//
//  StoryGoalAchievementsNotifications.swift
//  Scripto Portfolio
//
//  Created by Dorian Glanville on 3/23/19.
//  Copyright © 2019 DTGM. All rights reserved.
//

import Foundation
import SwiftLoggly
import UserNotifications


@available(OSX 10.14, *)
class StoryGoalAchievementsNotifications: NSObject, UNUserNotificationCenterDelegate {
    
//    init(workaround _: Void = ()) {
//    // Call a designated initializer
//        super.init()
//        let center = UNUserNotificationCenter.current()
//        center.delegate = self
//    }
    
//    let center = UNUserNotificationCenter.current()
//    center.delegate = self

    
    
    
    
    private func requestAuthorization(completionHandler: @escaping (_ success: Bool) -> ()) {
        // Request Authorization
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (success, error) in
            if let error = error {
                print("Request Authorization Failed (\(error), \(error.localizedDescription))")
            }
            
            completionHandler(success)
        }
    }
    

    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        //If you don't want to show notification when app is open, do something here else and make a return here.
        //Even you you don't implement this delegate method, you will not see the notification on the specified controller. So, you have to implement this delegate and make sure the below line execute. i.e. completionHandler.
        
        completionHandler([.alert, .badge, .sound])
    }
    
    
    
    
    
    func NotificationAuth() {
        
        
        // Request Notification Settings
        center.getNotificationSettings { (notificationSettings) in
            switch notificationSettings.authorizationStatus {
            case .notDetermined:
                // Request Authorization
                print("Requested Authorization")
                
                self.requestAuthorization(completionHandler: { (success) in
                    guard success else {
                        return
                    }
                    
                    // Schedule Local Notification
                    
                    self.scheduleNotification()
                    
                })
                
            case .authorized:
                // Schedule Local Notification
                print("Local Notification Authorized")
                self.scheduleNotification()
            case .denied:
                print("Application Not Allowed to Display Notifications")
            case .provisional:
                print("ERROR")
            }
        }
        
        
        
    }// end of func ShowNotification()
    
    
    
    
    
    
    func scheduleNotification() {
        
        
        let content = UNMutableNotificationContent()
        content.title = "Late wake up call"
        content.body = "The early bird catches the worm, but the second mouse gets the cheese."
        content.categoryIdentifier = "alarm"
        content.userInfo = ["customData": "fizzbuzz"]
        content.sound = UNNotificationSound.default()
        
        var dateComponents = DateComponents()
        dateComponents.hour = 11
        dateComponents.minute = 21
        //let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5, repeats: false)
        
        let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
        
        center.add(request) { (error) in
            if error != nil {
                // Handle any errors.
                print("Schedule ERROR: \(error!)")
                return
            } else {
                print("Scheduled")
            }
        }
        
        
        
        
        
    }//end of func scheduleNotification()
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}// end of class StoryGoalAchievementsNotifications:
