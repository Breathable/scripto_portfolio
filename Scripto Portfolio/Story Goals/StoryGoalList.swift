//
//  StoryGoalList.swift
//  Scripto Portfolio
//
//  Created by Dorian Glanville on 3/3/19.
//  Copyright © 2019 DTGM. All rights reserved.
//

import Foundation


class StoryGoalList: NSObject {
    
    var Ikey: String
    var UserIkeyReference: String
    var Category: String
    var Name: String
    var DailyWordCount: String
    var TotalWordCount: String
    var GoalStartDate: String
    var GoalEndDate: String
    var CreateDate: String
    var Status: String
    var Completed: String
    
    init(Ikey: String, UserIkeyReference: String ,Category: String, Name: String, DailyWordCount: String, TotalWordCount: String, GoalStartDate: String, CreateDate: String, GoalEndDate: String, Status: String, Completed: String) {
        self.Ikey = Ikey
        self.UserIkeyReference = UserIkeyReference
        self.Category = Category
        self.Name = Name
        self.DailyWordCount = DailyWordCount
        self.TotalWordCount = TotalWordCount
        self.GoalStartDate = GoalStartDate
        self.GoalEndDate = GoalEndDate
        self.CreateDate = CreateDate
        self.Status = Status
        self.Completed = Completed
    }

}
