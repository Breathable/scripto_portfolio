//
//  LockWindowViewConroller.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 2/13/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Cocoa
import SwiftLoggly

class LockWindowViewConroller: NSViewController {

    
    @IBOutlet weak var LockUserNameTextField: NSTextField!
    @IBOutlet weak var LockPasswordTextField: NSSecureTextField!
    @IBOutlet weak var LockLoginButton: NSButton!
    @IBOutlet weak var LockChangeUserButton: NSButton!
    
    var CurrentAppearanceLockWindowViewController = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        

        
        
        
        
    }
    
    
    override func viewWillAppear() {
        
        
        NSApp.mainMenu?.item(at: 0)?.submenu?.removeItem(at: 4) // App Themes
        NSApp.mainMenu?.item(at: 0)?.submenu?.removeItem(at: 3) // Wanted Features
        NSApp.mainMenu?.item(at: 0)?.submenu?.removeItem(at: 2) // Preferences
        NSApp.mainMenu?.removeItem(at: 5) // UserMenu
        
        
        LockUserNameTextField.stringValue = CurrentUserName!
        
    }
    
    
    override func viewDidLayout() {
        
        //        enum InterfaceStyle : String {
        //            case Dark, Light
        //
        //            init() {
        //                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
        //                self = InterfaceStyle(rawValue: type)!
        //            }
        //        }
        //
        //        let currentStyle = InterfaceStyle()
        //
        //        //print("currentStyle: \"\(currentStyle)\"  -  CurrentAppearance: \"\(CurrentAppearance)\" ")
        //
        //        if ("\(currentStyle)" != CurrentAppearanceNewUserViewController) {
        //            appearanceCheck()
        //        }
        
        
       
        
        
        appearanceCheck()
        
        super.viewDidLayout()
        
        
        
        
    }// end of viewDidLayout()
    
    
    func appearanceCheck() {
        
        enum InterfaceStyle : String {
            case Dark, Light
            
            init() {
                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
                self = InterfaceStyle(rawValue: type)!
            }
        }
        
        let currentStyle = InterfaceStyle()
        
        if ("\(currentStyle)" != CurrentAppearanceLockWindowViewController) {
            
            if (currentStyle.rawValue == "Dark") {
                
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColorDarkMode
                
                LockLoginButton.styleButtonText(button: LockLoginButton, buttonName: "Login", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                LockLoginButton.wantsLayer = true
                LockLoginButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                LockLoginButton.layer?.cornerRadius = 7
                
                LockChangeUserButton.styleButtonText(button: LockChangeUserButton, buttonName: "Change User", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                LockChangeUserButton.wantsLayer = true
                LockChangeUserButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                LockChangeUserButton.layer?.cornerRadius = 7
                
                
                
            } else if (currentStyle.rawValue == "Light") {
                
                
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColor.cgColor
                
                LockLoginButton.styleButtonText(button: LockLoginButton, buttonName: "Login", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                LockLoginButton.wantsLayer = true
                LockLoginButton.layer?.backgroundColor = NSColor.gray.cgColor
                LockLoginButton.layer?.cornerRadius = 7
                
                LockChangeUserButton.styleButtonText(button: LockChangeUserButton, buttonName: "Change User", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                LockChangeUserButton.wantsLayer = true
                LockChangeUserButton.layer?.backgroundColor = NSColor.gray.cgColor
                LockChangeUserButton.layer?.cornerRadius = 7
                
            }
        }
        
        CurrentAppearanceLockWindowViewController = "\(currentStyle)"
        
        
    }//end of appearanceCheck()
    
    
    
    
    
    
    @IBAction func LockLoginButtonAction(_ sender: Any) {
        
        
        let userName = CurrentUserName  //self.LockUserNameTextField.stringValue
        let password = self.LockPasswordTextField.stringValue
        
        let userNameBlankCheck = userName?.trimmingCharacters(in: .whitespacesAndNewlines)
        let passwordBlankCheck = password.trimmingCharacters(in: .whitespacesAndNewlines)
        
        print("userNameBlankCheck: \"\(userNameBlankCheck!)\" ")
        
        
        
        if ((userNameBlankCheck?.count == 0) || (passwordBlankCheck.count == 0)) {
            
            
            
            loggly(LogType.Info, text: "ViewController-\(#function): Username or Password is blank")
            //DoAlert.DisplayAlert(Class: "ViewController", Level: 3, Function: "\(#function)", MessageText: "Username or Password is blank", InformativeText: "Please try again", DatabaseError: "None") // \(db.lastExtendedErrorCode())
            
            
            let alert = NSAlert()
            alert.messageText = "Username or Password is blank"
            alert.informativeText = "Please try again"
            alert.alertStyle = .warning
            alert.addButton(withTitle: "OK")
        
            if (CurrentAppearanceLockWindowViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
        
            alert.window.titlebarAppearsTransparent = true

            if alert.runModal() == .alertFirstButtonReturn {
                print("Ok Pressed")
                //userTextField.stringValue = ""
                LockPasswordTextField.stringValue = ""
            }
            
            return
        }
        
        
        
        ////////////////
        
        
        let EncodedPW = passwordBlankCheck.base64Encoded()
        
        
        db.open()
        
        var userCount: Int32!
        
        do {
            //let UserCountQuery = try db.executeQuery("SELECT count(*) as cnt FROM users WHERE user = ? and password = ? ", values: ["\(userName!)","\(password)"])
            
            let UserCountQuery = try db.executeQuery("SELECT count(*) as cnt FROM users WHERE user = ? and password = ? ", values: ["\(userName!)","\(EncodedPW!)"])
            
            while UserCountQuery.next() {
                userCount = UserCountQuery.int(forColumn: "cnt")
            }
            
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "LockWindowViewConroller-\(#function): Query: SELECT count(*) as cnt FROM users WHERE user = ? and password = ? ")
            DoAlert.DisplayAlert(Class: "LockWindowViewConroller", Level: 1, Function: "\(#function)", MessageText: "Query Failed", InformativeText: "Query: SELECT count(*) as cnt FROM users WHERE user = ? and password = ? ", DatabaseError: "\(db.lastErrorMessage())")
            return
        }
        
        if (userCount == 0) {
            
            loggly(LogType.Info, text: "LockWindowViewConroller-\(#function): Username or Password is incorrect")
            
            //DoAlert.DisplayAlert(Class: "ViewController", Level: 3, Function: "\(#function)", MessageText: "Username or Password is incorrect", InformativeText: "Please try again", DatabaseError: "None") // \(db.lastExtendedErrorCode())
            
            
            
            let alert = NSAlert()
            alert.messageText = "Username or Password is blank"
            alert.informativeText = "Please try again"
            alert.alertStyle = .warning
            alert.addButton(withTitle: "OK")
            
            if (CurrentAppearanceLockWindowViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.window.titlebarAppearsTransparent = true
            
            if alert.runModal() == .alertFirstButtonReturn {
                print("Ok Pressed")
                //userTextField.stringValue = ""
                LockPasswordTextField.stringValue = ""
            }
            
            return
        }
        
        
        /////////////////
        
        
        var DBUserName: String!
        var DBPassword: String!
        var DBIkey: Int32!
        
        do {
            let UserQuery = try db.executeQuery("SELECT * FROM users WHERE user = ? and password = ? ", values: ["\(userName!)","\(EncodedPW!)"])
            
            while UserQuery.next() {
                DBUserName = UserQuery.string(forColumn: "user")
                DBPassword = UserQuery.string(forColumn: "password")
                DBIkey = UserQuery.int(forColumn: "ikey")
            }
            
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "LockWindowViewConroller-\(#function): Query Failed: SELECT * FROM users WHERE user = ? and password = ?")
            DoAlert.DisplayAlert(Class: "LockWindowViewConroller", Level: 1, Function: "\(#function)", MessageText: "Query Failed", InformativeText: "Query Failed: SELECT * FROM users WHERE user = ? and password = ?", DatabaseError: "\(db.lastErrorMessage())")
        }
        
        //        print("DBUserName: \(DBUserName)")
        //        print("DBPassword: \(DBPassword)")
        //        print("DBIkey: \(DBIkey)")
        
        
        
        if userName == DBUserName {
            //if password == DBPassword {
            if EncodedPW == DBPassword {
                print("Success logging In")
                
                LockUserNameTextField.stringValue = ""
                LockPasswordTextField.stringValue = ""
                
                ////////////
                CurrentUserIkey = DBIkey
                CurrentUserName = DBUserName
                
                
                print("Success in logging into Script Porfolio")
                
                
                ////////////////////////
                
                LoginVC.AddMenuItems()
                
                
                
                
                ////////////////////////
                
                
                
               
                
                
                //self.view.window?.close()
                
                //  https://stackoverflow.com/questions/45119813/how-do-i-open-another-window-in-macos-in-swift-with-cocoa
                //toryListWindow.showWindow(self)
                
                loggly(LogType.Info, text: "\(CurrentUserName!) Logged in")
                
                
                
            } else {
                
                loggly(LogType.Info, text: "LockWindowViewConroller-\(#function): Username or Password is incorrect")
                
                //DoAlert.DisplayAlert(Class: "ViewController", Level: 3, Function: "\(#function)", MessageText: "Username or Password is incorrect", InformativeText: "Please try again", DatabaseError: "None") // \(db.lastExtendedErrorCode())
                
                
                let alert = NSAlert()
                alert.messageText = "Username or Password is blank"
                alert.informativeText = "Please try again"
                alert.alertStyle = .warning
                alert.addButton(withTitle: "OK")
                
                if (CurrentAppearanceLockWindowViewController == "Dark") {
                    alert.window.backgroundColor = AlertBackgroundColorDarkMode
                } else {
                    alert.window.backgroundColor = WindowBackgroundColor
                }
                
                alert.window.titlebarAppearsTransparent = true
                
                if alert.runModal() == .alertFirstButtonReturn {
                    print("Ok Pressed")
                    //userTextField.stringValue = ""
                    LockPasswordTextField.stringValue = ""
                }
                
                return
            }
            
        } else {
            
            loggly(LogType.Info, text: "LockWindowViewConroller-\(#function): Username or Password is incorrect")
            
            //DoAlert.DisplayAlert(Class: "ViewController", Level: 3, Function: "\(#function)", MessageText: "Username or Password is incorrect", InformativeText: "Please try again", DatabaseError: "None") // \(db.lastExtendedErrorCode())
            
            let alert = NSAlert()
            alert.messageText = "Username or Password is blank"
            alert.informativeText = "Please try again"
            alert.alertStyle = .warning
            alert.addButton(withTitle: "OK")
            
            if (CurrentAppearanceLockWindowViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.window.titlebarAppearsTransparent = true
            
            if alert.runModal() == .alertFirstButtonReturn {
                print("Ok Pressed")
                //userTextField.stringValue = ""
                LockPasswordTextField.stringValue = ""
            }
            
            return
        }
        
        
        
        
        
        
        
        
        
        //////////////////////////
        
        
        
        
        
        OpenWindows.LockOpenWindows()
        self.view.window?.close()
        
        
        
    }
    
    
    @IBAction func LockChangeUserButtonAction(_ sender: Any) {
        
        
        self.view.window!.close()
        
        LoginWindow.showWindow(self)
        
        CurrentUserIkey = 0
        CurrentUserName = ""
        
        ///////////////////
        
//        NSApp.mainMenu?.item(at: 0)?.submenu?.removeItem(at: 3) // Wanted Features
//        NSApp.mainMenu?.removeItem(at: 5) // UserMenu
        
        OpenWindows.CheckForOpenWindows()

    }
    
    
    
    
    
    
    
    
    
    
    
    
} // End of LockWindowViewConroller
