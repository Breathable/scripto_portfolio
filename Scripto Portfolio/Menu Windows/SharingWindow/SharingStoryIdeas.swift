//
//  SharingStoryIdeas.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 4/20/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Foundation

class SharingStoryIdeas: NSObject {
    
    let Ikey: String
    let StoryIdeaName: String
    let StoryIdeaShare: String
    let StoryIdeaEdit: String
    
    init(Ikey: String,StoryIdeaName: String,StoryIdeaShare: String,StoryIdeaEdit: String) {
        self.Ikey = Ikey
        self.StoryIdeaName = StoryIdeaName
        self.StoryIdeaShare = StoryIdeaShare
        self.StoryIdeaEdit = StoryIdeaEdit
    }
    
}
