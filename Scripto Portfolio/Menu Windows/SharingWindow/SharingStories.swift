//
//  SharingStories.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 4/18/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Foundation

class SharingStories: NSObject {
    
    let Ikey: String
    let StoryName: String
    let StoryShare: String
    let StoryCo: String
    
    init(Ikey: String,StoryName: String,StoryShare: String,StoryCo: String) {
        self.Ikey = Ikey
        self.StoryName = StoryName
        self.StoryShare = StoryShare
        self.StoryCo = StoryCo
    }
    
}
