//
//  SharingCharacters.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 4/20/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Foundation

class SharingCharacters: NSObject {
    
    let Ikey: String
    let CharacterName: String
    let CharacterShare: String
    let CharacterEdit: String
    
    init(Ikey: String,CharacterName: String,CharacterShare: String,CharacterEdit: String) {
        self.Ikey = Ikey
        self.CharacterName = CharacterName
        self.CharacterShare = CharacterShare
        self.CharacterEdit = CharacterEdit
    }
    
}
