//
//  SharingLocations.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 4/20/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Foundation

class SharingLocations: NSObject {
    
    let Ikey: String
    let LocationName: String
    let LocationShare: String
    let LocationEdit: String
    
    init(Ikey: String,LocationName: String,LocationShare: String,LocationEdit: String) {
        self.Ikey = Ikey
        self.LocationName = LocationName
        self.LocationShare = LocationShare
        self.LocationEdit = LocationEdit
    }
    
}
