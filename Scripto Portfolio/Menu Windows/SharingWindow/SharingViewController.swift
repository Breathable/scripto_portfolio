//
//  SharingViewController.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 2/14/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Cocoa
import SwiftLoggly


class SharingViewController: NSViewController, NSTableViewDelegate, NSTableViewDataSource {
    

    
    @IBOutlet weak var SharingUserDropDown: NSPopUpButton!
    
    // Stories Tab
    @IBOutlet weak var SharingStoryTable: NSTableView!
    @IBOutlet weak var SharingStoryYesShareButton: NSButton!
    @IBOutlet weak var SharingStoryNoShareButton: NSButton!
    @IBOutlet weak var SharingStoryYesCoButton: NSButton!
    @IBOutlet weak var SharingStoryNoCoButton: NSButton!
    
    // Characters Tab
    
    @IBOutlet weak var SharingCharactersTable: NSTableView!
    @IBOutlet weak var SharingCharactersYesShareButton: NSButton!
    @IBOutlet weak var SharingCharactersNoShareButton: NSButton!
    @IBOutlet weak var SharingCharactersYesEditButton: NSButton!
    @IBOutlet weak var SharingCharactersNoEditButton: NSButton!
    
    // Story Ideas Tab
    
    @IBOutlet weak var SharingStoryIdeasTable: NSTableView!
    @IBOutlet weak var SharingStoryIdeasYesShareButton: NSButton!
    @IBOutlet weak var SharingStoryIdeasNoShareButton: NSButton!
    @IBOutlet weak var SharingStoryIdeasYesEditButton: NSButton!
    @IBOutlet weak var SharingStoryIdeasNoEditButton: NSButton!
    
    
    // Locations Tab
    
    @IBOutlet weak var SharingLocationsTable: NSTableView!
    @IBOutlet weak var SharingLocationsYesShareButton: NSButton!
    @IBOutlet weak var SharingLocationsNoShareButton: NSButton!
    @IBOutlet weak var SharingLocationsYesEditButton: NSButton!
    @IBOutlet weak var SharingLocationsNoEditButton: NSButton!
    
    
    
    @IBOutlet weak var SharingCloseButton: NSButton!
    
    var SharingStoriesArray: [SharingStories] = []
    
    var SharingCharactersArray: [SharingCharacters] = []
    
    var SharingStoryIdeasArray: [SharingStoryIdeas] = []

    var SharingLocationsArray: [SharingLocations] = []

    var SharingEmptyArray: [String] = []
    
    
    var UserArray: [String] = []
    
    var UserDropDownSelection = String()
    

    
    var CurrentAppearanceSharingViewController = String()
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        
    }
    
    override func viewWillAppear() {
        
        
        SharingStoryTable.delegate = self
        SharingStoryTable.dataSource = self
        
        SharingCharactersTable.delegate = self
        SharingCharactersTable.dataSource = self

        SharingStoryIdeasTable.delegate = self
        SharingStoryIdeasTable.dataSource = self

        SharingLocationsTable.delegate = self
        SharingLocationsTable.dataSource = self
        
        
        
        

       
        PopulateUserDropDown()
    }
    
    override func viewDidLayout() {
        
        //        enum InterfaceStyle : String {
        //            case Dark, Light
        //
        //            init() {
        //                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
        //                self = InterfaceStyle(rawValue: type)!
        //            }
        //        }
        //
        //        let currentStyle = InterfaceStyle()
        //
        //        //print("currentStyle: \"\(currentStyle)\"  -  CurrentAppearance: \"\(CurrentAppearance)\" ")
        //
        //        if ("\(currentStyle)" != CurrentAppearanceNewUserViewController) {
        //            appearanceCheck()
        //        }
        
        appearanceCheck()
        
        super.viewDidLayout()
        
        
        
        
    }// end of viewDidLayout()
    
    
    func appearanceCheck() {
        
        enum InterfaceStyle : String {
            case Dark, Light
            
            init() {
                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
                self = InterfaceStyle(rawValue: type)!
            }
        }
        
        let currentStyle = InterfaceStyle()
        
        //rint("currentStyle: \"\(currentStyle)\" CurrentAppearanceUserPreferencesViewController: \"\(CurrentAppearanceSharingViewController)\" ")
        
        if ("\(currentStyle)" != CurrentAppearanceSharingViewController) {
            
            if (currentStyle.rawValue == "Dark") {
                
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColorDarkMode
                
                SharingCloseButton.styleButtonText(button: SharingCloseButton, buttonName: "Close", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                SharingCloseButton.wantsLayer = true
                SharingCloseButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                SharingCloseButton.layer?.cornerRadius = 7
                
                SharingStoryYesShareButton.styleButtonText(button: SharingStoryYesShareButton, buttonName: "Yes Share", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                SharingStoryYesShareButton.wantsLayer = true
                SharingStoryYesShareButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                SharingStoryYesShareButton.layer?.cornerRadius = 7
                
                SharingStoryNoShareButton.styleButtonText(button: SharingStoryNoShareButton, buttonName: "No Share", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                SharingStoryNoShareButton.wantsLayer = true
                SharingStoryNoShareButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                SharingStoryNoShareButton.layer?.cornerRadius = 7
                
                SharingStoryYesCoButton.styleButtonText(button: SharingStoryYesCoButton, buttonName: "Yes CO", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                SharingStoryYesCoButton.wantsLayer = true
                SharingStoryYesCoButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                SharingStoryYesCoButton.layer?.cornerRadius = 7
                
                SharingStoryNoCoButton.styleButtonText(button: SharingStoryNoCoButton, buttonName: "No CO", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                SharingStoryNoCoButton.wantsLayer = true
                SharingStoryNoCoButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                SharingStoryNoCoButton.layer?.cornerRadius = 7
                
                /////
                
                SharingCharactersYesShareButton.styleButtonText(button: SharingCharactersYesShareButton, buttonName: "Yes Share", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                SharingCharactersYesShareButton.wantsLayer = true
                SharingCharactersYesShareButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                SharingCharactersYesShareButton.layer?.cornerRadius = 7
                
                SharingCharactersNoShareButton.styleButtonText(button: SharingCharactersNoShareButton, buttonName: "No Share", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                SharingCharactersNoShareButton.wantsLayer = true
                SharingCharactersNoShareButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                SharingCharactersNoShareButton.layer?.cornerRadius = 7
                
                SharingCharactersYesEditButton.styleButtonText(button: SharingCharactersYesEditButton, buttonName: "Yes Edit", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                SharingCharactersYesEditButton.wantsLayer = true
                SharingCharactersYesEditButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                SharingCharactersYesEditButton.layer?.cornerRadius = 7
                
                SharingCharactersNoEditButton.styleButtonText(button: SharingCharactersNoEditButton, buttonName: "No Edit", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                SharingCharactersNoEditButton.wantsLayer = true
                SharingCharactersNoEditButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                SharingCharactersNoEditButton.layer?.cornerRadius = 7
                
                /////
                
                SharingStoryIdeasYesShareButton.styleButtonText(button: SharingStoryIdeasYesShareButton, buttonName: "Yes Share", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                SharingStoryIdeasYesShareButton.wantsLayer = true
                SharingStoryIdeasYesShareButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                SharingStoryIdeasYesShareButton.layer?.cornerRadius = 7
                
                SharingStoryIdeasNoShareButton.styleButtonText(button: SharingStoryIdeasNoShareButton, buttonName: "No Share", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                SharingStoryIdeasNoShareButton.wantsLayer = true
                SharingStoryIdeasNoShareButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                SharingStoryIdeasNoShareButton.layer?.cornerRadius = 7
                
                SharingStoryIdeasYesEditButton.styleButtonText(button: SharingStoryIdeasYesEditButton, buttonName: "Yes Edit", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                SharingStoryIdeasYesEditButton.wantsLayer = true
                SharingStoryIdeasYesEditButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                SharingStoryIdeasYesEditButton.layer?.cornerRadius = 7
                
                SharingStoryIdeasNoEditButton.styleButtonText(button: SharingStoryIdeasNoEditButton, buttonName: "No Edit", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                SharingStoryIdeasNoEditButton.wantsLayer = true
                SharingStoryIdeasNoEditButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                SharingStoryIdeasNoEditButton.layer?.cornerRadius = 7
                
                /////
                
                SharingLocationsYesShareButton.styleButtonText(button: SharingLocationsYesShareButton, buttonName: "Yes Share", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                SharingLocationsYesShareButton.wantsLayer = true
                SharingLocationsYesShareButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                SharingLocationsYesShareButton.layer?.cornerRadius = 7
                
                SharingLocationsNoShareButton.styleButtonText(button: SharingLocationsNoShareButton, buttonName: "No Share", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                SharingLocationsNoShareButton.wantsLayer = true
                SharingLocationsNoShareButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                SharingLocationsNoShareButton.layer?.cornerRadius = 7
                
                SharingLocationsYesEditButton.styleButtonText(button: SharingLocationsYesEditButton, buttonName: "Yes Edit", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                SharingLocationsYesEditButton.wantsLayer = true
                SharingLocationsYesEditButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                SharingLocationsYesEditButton.layer?.cornerRadius = 7
                
                SharingLocationsNoEditButton.styleButtonText(button: SharingLocationsNoEditButton, buttonName: "No Edit", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                SharingLocationsNoEditButton.wantsLayer = true
                SharingLocationsNoEditButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                SharingLocationsNoEditButton.layer?.cornerRadius = 7
                
                
                
            } else if (currentStyle.rawValue == "Light") {
                print("Light Style Detected - NewUserViewController")
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColor.cgColor
                
                SharingCloseButton.styleButtonText(button: SharingCloseButton, buttonName: "Close", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                SharingCloseButton.wantsLayer = true
                SharingCloseButton.layer?.backgroundColor = NSColor.gray.cgColor
                SharingCloseButton.layer?.cornerRadius = 7
                
                SharingStoryYesShareButton.styleButtonText(button: SharingStoryYesShareButton, buttonName: "Yes Share", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                SharingStoryYesShareButton.wantsLayer = true
                SharingStoryYesShareButton.layer?.backgroundColor = NSColor.gray.cgColor
                SharingStoryYesShareButton.layer?.cornerRadius = 7
                
                SharingStoryNoShareButton.styleButtonText(button: SharingStoryNoShareButton, buttonName: "No Share", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                SharingStoryNoShareButton.wantsLayer = true
                SharingStoryNoShareButton.layer?.backgroundColor = NSColor.gray.cgColor
                SharingStoryNoShareButton.layer?.cornerRadius = 7
                
                SharingStoryYesCoButton.styleButtonText(button: SharingStoryYesCoButton, buttonName: "Yes CO", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                SharingStoryYesCoButton.wantsLayer = true
                SharingStoryYesCoButton.layer?.backgroundColor = NSColor.gray.cgColor
                SharingStoryYesCoButton.layer?.cornerRadius = 7
                
                SharingStoryNoCoButton.styleButtonText(button: SharingStoryNoCoButton, buttonName: "No CO", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                SharingStoryNoCoButton.wantsLayer = true
                SharingStoryNoCoButton.layer?.backgroundColor = NSColor.gray.cgColor
                SharingStoryNoCoButton.layer?.cornerRadius = 7
                
                /////
                
                SharingCharactersYesShareButton.styleButtonText(button: SharingCharactersYesShareButton, buttonName: "Yes Share", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                SharingCharactersYesShareButton.wantsLayer = true
                SharingCharactersYesShareButton.layer?.backgroundColor = NSColor.gray.cgColor
                SharingCharactersYesShareButton.layer?.cornerRadius = 7
                
                SharingCharactersNoShareButton.styleButtonText(button: SharingCharactersNoShareButton, buttonName: "No Share", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                SharingCharactersNoShareButton.wantsLayer = true
                SharingCharactersNoShareButton.layer?.backgroundColor = NSColor.gray.cgColor
                SharingCharactersNoShareButton.layer?.cornerRadius = 7
                
                SharingCharactersYesEditButton.styleButtonText(button: SharingCharactersYesEditButton, buttonName: "Yes Edit", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                SharingCharactersYesEditButton.wantsLayer = true
                SharingCharactersYesEditButton.layer?.backgroundColor = NSColor.gray.cgColor
                SharingCharactersYesEditButton.layer?.cornerRadius = 7
                
                SharingCharactersNoEditButton.styleButtonText(button: SharingCharactersNoEditButton, buttonName: "No Edit", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                SharingCharactersNoEditButton.wantsLayer = true
                SharingCharactersNoEditButton.layer?.backgroundColor = NSColor.gray.cgColor
                SharingCharactersNoEditButton.layer?.cornerRadius = 7
                
                /////
                
                SharingStoryIdeasYesShareButton.styleButtonText(button: SharingStoryIdeasYesShareButton, buttonName: "Yes Share", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                SharingStoryIdeasYesShareButton.wantsLayer = true
                SharingStoryIdeasYesShareButton.layer?.backgroundColor = NSColor.gray.cgColor
                SharingStoryIdeasYesShareButton.layer?.cornerRadius = 7
                
                SharingStoryIdeasNoShareButton.styleButtonText(button: SharingStoryIdeasNoShareButton, buttonName: "No Share", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                SharingStoryIdeasNoShareButton.wantsLayer = true
                SharingStoryIdeasNoShareButton.layer?.backgroundColor = NSColor.gray.cgColor
                SharingStoryIdeasNoShareButton.layer?.cornerRadius = 7
                
                SharingStoryIdeasYesEditButton.styleButtonText(button: SharingStoryIdeasYesEditButton, buttonName: "Yes Edit", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                SharingStoryIdeasYesEditButton.wantsLayer = true
                SharingStoryIdeasYesEditButton.layer?.backgroundColor = NSColor.gray.cgColor
                SharingStoryIdeasYesEditButton.layer?.cornerRadius = 7
                
                SharingStoryIdeasNoEditButton.styleButtonText(button: SharingStoryIdeasNoEditButton, buttonName: "No Edit", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                SharingStoryIdeasNoEditButton.wantsLayer = true
                SharingStoryIdeasNoEditButton.layer?.backgroundColor = NSColor.gray.cgColor
                SharingStoryIdeasNoEditButton.layer?.cornerRadius = 7
                
                /////
                
                SharingLocationsYesShareButton.styleButtonText(button: SharingLocationsYesShareButton, buttonName: "Yes Share", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                SharingLocationsYesShareButton.wantsLayer = true
                SharingLocationsYesShareButton.layer?.backgroundColor = NSColor.gray.cgColor
                SharingLocationsYesShareButton.layer?.cornerRadius = 7
                
                SharingLocationsNoShareButton.styleButtonText(button: SharingLocationsNoShareButton, buttonName: "No Share", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                SharingLocationsNoShareButton.wantsLayer = true
                SharingLocationsNoShareButton.layer?.backgroundColor = NSColor.gray.cgColor
                SharingLocationsNoShareButton.layer?.cornerRadius = 7
                
                SharingLocationsYesEditButton.styleButtonText(button: SharingLocationsYesEditButton, buttonName: "Yes Edit", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                SharingLocationsYesEditButton.wantsLayer = true
                SharingLocationsYesEditButton.layer?.backgroundColor = NSColor.gray.cgColor
                SharingLocationsYesEditButton.layer?.cornerRadius = 7
                
                SharingLocationsNoEditButton.styleButtonText(button: SharingLocationsNoEditButton, buttonName: "No Edit", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                SharingLocationsNoEditButton.wantsLayer = true
                SharingLocationsNoEditButton.layer?.backgroundColor = NSColor.gray.cgColor
                SharingLocationsNoEditButton.layer?.cornerRadius = 7
                
            }
        }
        
        CurrentAppearanceSharingViewController = "\(currentStyle)"
        
        
    }//end of appearanceCheck()
    
    
    
    
    
    func PopulateUserDropDown() {
        
        var DBUser: String!
        
        UserArray.removeAll()
        
        do {
            let UserQuery = try db.executeQuery("SELECT user FROM users", values:nil)
            
            while UserQuery.next() {
                DBUser = UserQuery.string(forColumn: "user")
                
                if (DBUser == "AppSDemo") {
                    
                } else {
                
                    UserArray.append(DBUser)
                    
                }
            }
            
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed: \"SELECT user FROM users\"")
            DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"SELECT user FROM users\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
            return
        }
        
        
        
        SharingUserDropDown.removeAllItems()
        SharingUserDropDown.addItems(withTitles: UserArray)
        SharingUserDropDown.removeItem(withTitle: CurrentUserName!)
        SharingUserDropDown.insertItem(withTitle: "Choose a User", at: 0)
        SharingUserDropDown.selectItem(at: 0)
    }
    
    
    
    
    @IBAction func SharingUserDropDownAction(_ sender: Any) {
        
        let SelectedUser = SharingUserDropDown.titleOfSelectedItem
        UserDropDownSelection = SelectedUser!
        print("SelectedUser: \"\(SelectedUser!)\"")
        
        if (SelectedUser == "Choose a User") {
            print("Choose a User: was selected and stopped")
            EmptyTables()
            return
        }
        
        

        
        print("StoryTableData Started")
        StorieTableData()
        print("StoryTableData Finished")
        
        
        delayWithSeconds(0.25) {
            print("CharacterTableData Started")
            self.CharacterTableData()
            print("CharacterTableData Finished")
        }
        
        delayWithSeconds(0.50) {
            print("StoryIdeasTableData Started")
            self.StoryIdeasTableData()
            print("StoryIdeasTableData Finished")
        }
        
        delayWithSeconds(0.75) {
            print("LocationsTableData Started")
            self.LocationsTableData()
            print("LocationsTableData Finished")
        }
        

    }
    
    
    ////////////////////////////////
    
    
    
    func StorieTableData() {

        SharingStoriesArray.removeAll()
        SharingStoryTable.reloadData()
        
        /////////////////////////////////
        
        // select count(ikey) as cnt from storytable where userikeyreference = \"%ld\" ",CurrentUserIkey
        
        
        let StorySharingCountQuery = String(format: "select count(ikey) as cnt from storytable where userikeyreference = \"\(CurrentUserIkey!)\"")
        
        print("StorySharingCountQuery:\n\(StorySharingCountQuery)")
        
        var CountDB = String()
        
        do {
            let StorySharingCountQueryRun = try db.executeQuery(StorySharingCountQuery, values:nil)
            
            while StorySharingCountQueryRun.next() {
                CountDB = StorySharingCountQueryRun.string(forColumn: "cnt")!

                print("COUNT: \"\(CountDB)\"")
                
            }
            
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(StorySharingCountQuery)\"")
            DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(StorySharingCountQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
        }
        
        
        
        var loop = 1
        let countLoop = Int(CountDB)
        
        
        //////////////////////////////////
        
        let StoryInfoQuery = String(format: "select * from storytable where userikeyreference = \(CurrentUserIkey!)")
        
        print("StoryInfoQuery:\n\(StoryInfoQuery)")
        
        
        //var StoryTableIkeyDB: String!
        //var StoryTableNameDB: String!
        

        
        
        do {
            
            let StoryInfoQueryRun = try db.executeQuery(StoryInfoQuery, values:nil)
            
            while StoryInfoQueryRun.next() {
                var StoryTableIkeyDB = StoryInfoQueryRun.int(forColumn: "ikey")
                
                
                //print("StoryTableIkeyDB: \(StoryTableIkeyDB) StoryTableNameDB: \"\(StoryTableNameDB!)\"")

                /////////////////////////////////

                while (loop != (countLoop! + 1)) {
                
                var StoryIkeyDB: String!
                var StoryShareDB: String!
                var StoryCoDB: String!
                
                    let StorySharingQuery = String(format: "SELECT * FROM storysharingtable WHERE userikeyreference = \(CurrentUserIkey!) AND sharewithuserikeyreference = (select ikey from users where user = \"\(UserDropDownSelection)\") AND storyikey = \"\(StoryTableIkeyDB)\" ")
                
                
                //let StorySharingQuery = String(format: "SELECT * FROM storysharingtable WHERE userikeyreference = \(CurrentUserIkey!) AND storyikey = \"\(StoryTableIkeyDB!)\" ")
                
                
                print("StorySharingQuery:\n\(StorySharingQuery)")

                do {
                    let StorySharingQueryRun = try db.executeQuery(StorySharingQuery, values:nil)

                    while StorySharingQueryRun.next() {
                        StoryIkeyDB = StorySharingQueryRun.string(forColumn: "ikey")
                        StoryShareDB = StorySharingQueryRun.string(forColumn: "storyshare")
                        StoryCoDB = StorySharingQueryRun.string(forColumn: "storyedit")
                    }
                    
                        
                        if ((StoryShareDB == "") && (StoryCoDB == "") || (StoryShareDB == nil) && (StoryCoDB == nil)) {
                            StoryShareDB = "No"
                            StoryCoDB = "No"
                        }
                        
                        let StoryTableNameDB = StoryInfoQueryRun.string(forColumn: "storyname")

                        let StoryToAdd = SharingStories.init(Ikey: "\(StoryTableIkeyDB)", StoryName: StoryTableNameDB!, StoryShare: StoryShareDB, StoryCo: StoryCoDB)
                        
                        SharingStoriesArray.append(StoryToAdd)
                        
                        //print("ADDED: \(StoryIkeyDB) \(StoryTableNameDB!) \(StoryShareDB) \(StoryCoDB)")
                        
                        

                    } catch {
                        print("ERROR: \(db.lastErrorMessage())")
                        loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(StorySharingQuery)\"")
                        DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(StorySharingQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                    }

                    
                    
                    loop = loop + 1
                    StoryTableIkeyDB = StoryTableIkeyDB + 1
                    
                    if (loop == (countLoop! + 1)) {
                        break
                    } else {
                        StoryInfoQueryRun.next()
                    }
                    
                    
                    
                }// end of while (loop != countLoop)
                    
                }// end of StoryInfoQueryRun.next()
            
                
                
            } catch {
                print("ERROR: \(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(StoryInfoQuery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(StoryInfoQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
            }
       
        
        SharingStoryTable.reloadData()

    }
    
    
    func CharacterTableData() {
        
        SharingCharactersArray.removeAll()
        SharingStoryTable.reloadData()
        
        /////////////////////////////////
        let CharacterSharingCountQuery = String(format: "select count(ikey) as cnt from charactertable where userikeyreference = \"\(CurrentUserIkey!)\"")
        
        print("CharacterSharingCountQuery:\n\(CharacterSharingCountQuery)")
        
        var CountDB = String()
        
        do {
            let CharacterSharingCountQueryRun = try db.executeQuery(CharacterSharingCountQuery, values:nil)
            
            while CharacterSharingCountQueryRun.next() {
                CountDB = CharacterSharingCountQueryRun.string(forColumn: "cnt")!
                
                print("COUNT: \"\(CountDB)\"")
                
            }
            
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(CharacterSharingCountQuery)\"")
            DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(CharacterSharingCountQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
        }
        
        
        
        var loop = 1
        let countLoop = Int(CountDB)
        
        
        //////////////////////////////////
        
        let CharacterInfoQuery = String(format: "select * from charactertable where userikeyreference = \(CurrentUserIkey!)")
        
        print("CharacterInfoQuery:\n\(CharacterInfoQuery)")
        
        
        //var StoryTableIkeyDB: String!
        //var StoryTableNameDB: String!
        
        do {
            
            let CharacterInfoQueryRun = try db.executeQuery(CharacterInfoQuery, values:nil)
            
            while CharacterInfoQueryRun.next() {
                var CharacterTableIkeyDB = CharacterInfoQueryRun.int(forColumn: "ikey")
                
                /////////////////////////////////
                
                while (loop != (countLoop! + 1)) {

                var CharacterIkeyDB: String!
                var CharacterShareDB: String!
                var CharacterEditDB: String!

                let CharacterSharingQuery = String(format: "SELECT * FROM charactersharingtable WHERE userikeyreference = \(CurrentUserIkey!) AND characterikey = \"\(CharacterTableIkeyDB)\" ")
                
                print("CharacterSharingQuery:\n\(CharacterSharingQuery)")
                
                do {
                    let CharacterSharingQueryRun = try db.executeQuery(CharacterSharingQuery, values:nil)
                    
                    while CharacterSharingQueryRun.next() {
                        CharacterIkeyDB = CharacterSharingQueryRun.string(forColumn: "ikey")
                        CharacterShareDB = CharacterSharingQueryRun.string(forColumn: "charactershare")
                        CharacterEditDB = CharacterSharingQueryRun.string(forColumn: "characteredit")
                    }
                    
                    if ((CharacterShareDB == "") && (CharacterEditDB == "") || (CharacterShareDB == nil) && (CharacterEditDB == nil)) {
                        CharacterShareDB = "No"
                        CharacterEditDB = "No"
                    }
                    
                    let CharacterTableFNameDB = CharacterInfoQueryRun.string(forColumn: "firstname")
                    let CharacterTableLNameDB = CharacterInfoQueryRun.string(forColumn: "lastname")
                    
                    let FullCharacterName = "\(CharacterTableFNameDB!) \(CharacterTableLNameDB!)"
                    
                    let CharacterToAdd = SharingCharacters.init(Ikey: "\(CharacterTableIkeyDB)", CharacterName: FullCharacterName, CharacterShare: CharacterShareDB, CharacterEdit: CharacterEditDB)
                    
                    SharingCharactersArray.append(CharacterToAdd)
                    
                   // print("ADDED: \(CharacterIkeyDB) \(FullCharacterName) \(CharacterShareDB) \(CharacterEditDB)")

                } catch {
                    print("ERROR: \(db.lastErrorMessage())")
                    loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(CharacterSharingQuery)\"")
                    DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(CharacterSharingQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                }

                loop = loop + 1
                CharacterTableIkeyDB = CharacterTableIkeyDB + 1
                
                if (loop == (countLoop! + 1)) {
                    break
                } else {
                    CharacterInfoQueryRun.next()
                }
                    
                }// end of loop
                    
            }// end of StoryInfoQueryRun.next()
            
                
                
                
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(CharacterInfoQuery)\"")
            DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(CharacterInfoQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
        }
        
        SharingCharactersTable.reloadData()
        
    }
    
    
    func StoryIdeasTableData() {
        
        SharingStoryIdeasArray.removeAll()
        SharingStoryTable.reloadData()
        
        
        print("SharingStoryIdeasArray EMPTIED")
        print("SharingStoryTable RELOADED BLANK")
        
        /////////////////////////////////
        let StoryideasSharingCountQuery = String(format: "select count(ikey) as cnt from storyideastable where userikeyreference = \"\(CurrentUserIkey!)\"")
        
        print("StoryideasSharingCountQuery:\n\(StoryideasSharingCountQuery)")
        
        var CountDB = String()
        
        do {
            let StoryideasSharingCountQueryRun = try db.executeQuery(StoryideasSharingCountQuery, values:nil)
            
            while StoryideasSharingCountQueryRun.next() {
                CountDB = StoryideasSharingCountQueryRun.string(forColumn: "cnt")!
                
                print("COUNT: \"\(CountDB)\"")
                
            }
            
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(StoryideasSharingCountQuery)\"")
            DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(StoryideasSharingCountQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
        }
        
        
        
        var loop = 1
        let countLoop = Int(CountDB)
        
        
        //////////////////////////////////
        
        
        
        
        
        let StoryIdeasInfoQuery = String(format: "select * from storyideastable where userikeyreference = \(CurrentUserIkey!)")
        
        print("StoryIdeasInfoQuery:\n\(StoryIdeasInfoQuery)")
        
        
        //var StoryTableIkeyDB: String!
        //var StoryTableNameDB: String!
        
        do {
            
            let StoryIdeasInfoQueryRun = try db.executeQuery(StoryIdeasInfoQuery, values:nil)
            
            while StoryIdeasInfoQueryRun.next() {
                
                var StoryIdeaTableIkeyDB = StoryIdeasInfoQueryRun.int(forColumn: "ikey")
                
            
                //print("StoryIdeaTableIkeyDB: \(StoryIdeaTableIkeyDB) StoryIdeaTableNameDB: \"\(StoryIdeaTableNameDB!)\"")
                
                /////////////////////////////////
                
                while (loop != (countLoop! + 1)) {
                
                var StoryIdeaIkeyDB: String!
                var StoryIdeaShareDB: String!
                var StoryIdeaEditDB: String!

                let StoryIdeaSharingQuery = String(format: "SELECT * FROM storyideasharingtable WHERE userikeyreference = \(CurrentUserIkey!) AND storyideaikey = \"\(StoryIdeaTableIkeyDB)\" ")
                
                print("StoryIdeaSharingQuery:\n\(StoryIdeaSharingQuery)")
                
                do {
                    let StoryIdeaSharingQueryRun = try db.executeQuery(StoryIdeaSharingQuery, values:nil)

                    while StoryIdeaSharingQueryRun.next() {
                        StoryIdeaIkeyDB = StoryIdeaSharingQueryRun.string(forColumn: "ikey")
                        StoryIdeaShareDB = StoryIdeaSharingQueryRun.string(forColumn: "storyideashare")
                        StoryIdeaEditDB = StoryIdeaSharingQueryRun.string(forColumn: "storyideaedit")
                    }
                    
                    if ((StoryIdeaShareDB == "") && (StoryIdeaEditDB == "") || (StoryIdeaShareDB == nil) && (StoryIdeaEditDB == nil)) {
                        StoryIdeaShareDB = "No"
                        StoryIdeaEditDB = "No"
                    }
                    
                    let StoryIdeaTableNameDB = StoryIdeasInfoQueryRun.string(forColumn: "name")
                    
                    
                    //print("BEFORE: \"\(StoryIdeaTableIkeyDB)\" \"\(StoryIdeaTableNameDB!)\" \"\(StoryIdeaShareDB)\" \"\(StoryIdeaEditDB)\"")
                    
                    
                    
                    let StoryIdeaToAdd = SharingStoryIdeas.init(Ikey: "\(StoryIdeaTableIkeyDB)", StoryIdeaName: StoryIdeaTableNameDB!, StoryIdeaShare: StoryIdeaShareDB, StoryIdeaEdit: StoryIdeaEditDB)
                    
                    SharingStoryIdeasArray.append(StoryIdeaToAdd)
                    
                    //print("ADDED: \"\(StoryIdeaTableIkeyDB)\" \"\(StoryIdeaTableNameDB!)\" \"\(StoryIdeaShareDB)\" \"\(StoryIdeaEditDB)\"")
                        
                    
                    
                } catch {
                    print("ERROR: \(db.lastErrorMessage())")
                    loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(StoryIdeaSharingQuery)\"")
                    DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(StoryIdeaSharingQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                }
                
                    loop = loop + 1
                    StoryIdeaTableIkeyDB = StoryIdeaTableIkeyDB + 1
                    
                    if (loop == (countLoop! + 1)) {
                        break
                    } else {
                        StoryIdeasInfoQueryRun.next()
                    }
                    
                }// end of loop
                    
            }// end of StoryInfoQueryRun.next()
            
            SharingStoryIdeasTable.reloadData()
            print("SharingStoryTable RELOADED 1")
            
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(StoryIdeasInfoQuery)\"")
            DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(StoryIdeasInfoQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
        }
        
        SharingStoryIdeasTable.reloadData()
        print("SharingStoryTable RELOADED 2")
        
    }
    
    
    func LocationsTableData() {
        
        SharingLocationsArray.removeAll()
        SharingStoryTable.reloadData()
        
        /////////////////////////////////
        
        /////////////////////////////////
        let LocationSharingCountQuery = String(format: "select count(ikey) as cnt from locationtable where userikeyreference = \"\(CurrentUserIkey!)\"")
        
        print("LocationSharingCountQuery:\n\(LocationSharingCountQuery)")
        
        var CountDB = String()
        
        do {
            let LocationSharingCountQueryRun = try db.executeQuery(LocationSharingCountQuery, values:nil)
            
            while LocationSharingCountQueryRun.next() {
                CountDB = LocationSharingCountQueryRun.string(forColumn: "cnt")!
                
                print("COUNT: \"\(CountDB)\"")
                
            }
            
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(LocationSharingCountQuery)\"")
            DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(LocationSharingCountQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
        }
        
        
        
        var loop = 1
        let countLoop = Int(CountDB)
        
        
        //////////////////////////////////
        
        let LocationsinfoQuery = String(format: "select * from locationtable where userikeyreference = \(CurrentUserIkey!)")
        
        print("LocationsinfoQuery:\n\(LocationsinfoQuery)")
        
        
  
        
        do {
            
            let LocationsinfoQueryRun = try db.executeQuery(LocationsinfoQuery, values:nil)
            
            while LocationsinfoQueryRun.next() {
                var LocationsTableIkeyDB = LocationsinfoQueryRun.int(forColumn: "ikey")
                
                
                //print("LocationsTableIkeyDB: \(LocationsTableIkeyDB) LocationsTableNameDB: \"\(LocationsTableNameDB!)\"")
                
                
                while (loop != (countLoop! + 1)) {
                
                
                /////////////////////////////////
                
                var LocationsIkeyDB: String!
                var LocationsShareDB: String!
                var LocationsEditDB: String!
                
                let LocationsSharingQuery = String(format: "SELECT * FROM locationsharingtable WHERE userikeyreference = \(CurrentUserIkey!) AND sharewithuserikeyreference = (select ikey from users where user = \"\(UserDropDownSelection)\") AND locationikey = \"\(LocationsTableIkeyDB)\" ")

                //let LocationsSharingQuery = String(format: "SELECT * FROM locationsharingtable WHERE userikeyreference = \(CurrentUserIkey!) AND locationikey = \"\(LocationsTableIkeyDB)\" ")
                
                    
                    //  AND sharewithuserikeyreference = (select ikey from users where user = \"\(UserDropDownSelection)\")
                    
                    
                    
                print("LocationsSharingQuery:\n\(LocationsSharingQuery)")
                
                do {
                    let LocationsSharingQueryRun = try db.executeQuery(LocationsSharingQuery, values:nil)
                    
                    while LocationsSharingQueryRun.next() {
                        LocationsIkeyDB = LocationsSharingQueryRun.string(forColumn: "ikey")
                        LocationsShareDB = LocationsSharingQueryRun.string(forColumn: "locationshare")
                        LocationsEditDB = LocationsSharingQueryRun.string(forColumn: "locationedit")

                    }
                    
                    if ((LocationsShareDB == "") && (LocationsEditDB == "") || (LocationsShareDB == nil) && (LocationsEditDB == nil)) {
                        LocationsShareDB = "No"
                        LocationsEditDB = "No"
                        
                    }
                    
                    let LocationsTableNameDB = LocationsinfoQueryRun.string(forColumn: "name")
                    
                    print("IKEY: \"\(LocationsTableIkeyDB)\" LocationName: \"\(LocationsTableNameDB!)\" LocationShare: \"\(LocationsShareDB)\", LocationEdit: \"\(LocationsEditDB)\" ")
                    
                    
                    
                    
                    
                    let LocationToAdd = SharingLocations.init(Ikey: "\(LocationsTableIkeyDB)", LocationName: LocationsTableNameDB!, LocationShare: LocationsShareDB, LocationEdit: LocationsEditDB)
                    
                    SharingLocationsArray.append(LocationToAdd)
                    
                    print("ADDED: \(LocationsTableIkeyDB) \(LocationsTableNameDB!) \(LocationsShareDB) \(LocationsEditDB)")
                    
                    
                    
                } catch {
                    print("ERROR: \(db.lastErrorMessage())")
                    loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(LocationsSharingQuery)\"")
                    DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(LocationsSharingQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                }
                    
                    
                    
                    loop = loop + 1
                    LocationsTableIkeyDB = LocationsTableIkeyDB + 1
                    
                    if (loop == (countLoop! + 1)) {
                        break
                    } else {
                        LocationsinfoQueryRun.next()
                    }
                    
                    
                }// end of loop
                
            }// end of StoryInfoQueryRun.next()
            
            
            
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(LocationsinfoQuery)\"")
            DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(LocationsinfoQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
        }
        
        SharingLocationsTable.reloadData()
        
    }
    
    
    

    /////////////////////////////////

    func numberOfRows(in tableView: NSTableView) -> Int {
        
        //print("numberOfRows RAN")
        
        var RowsCount = Int()
        
        if (tableView == SharingStoryTable) {
            print("numberOfRows = SharingStoryTable")
            RowsCount = SharingStoriesArray.count
        }
        if (tableView == SharingCharactersTable) {
            print("numberOfRows = SharingCharactersTable")
            RowsCount = SharingCharactersArray.count
        }
        if (tableView == SharingStoryIdeasTable) {
            print("numberOfRows = SharingStoryIdeasTable")
            RowsCount = SharingStoryIdeasArray.count
        }
        if (tableView == SharingLocationsTable) {
            print("numberOfRows = SharingLocationsTable")
            RowsCount = SharingLocationsArray.count
        }
        
        
        return RowsCount

    }


    
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        
        guard let cell = tableView.makeView(withIdentifier: tableColumn!.identifier, owner: self) as? NSTableCellView else
        {
            print("Table Returned Nil")
            return nil
        }
        //print("tableView RAN")
        
        
        
        if (tableView == SharingStoryTable) {
        //if (StoryTableIsDoneLoading == false) {
            //print("tableColumn?.identifier)!.rawValue: \"\((tableColumn?.identifier)!.rawValue)\"")
            
            if (tableColumn?.identifier)!.rawValue == "ikey" {
                cell.textField?.stringValue = SharingStoriesArray[row].Ikey
                //print("IKEY =  \"\(SharingStoriesArray[row].Ikey)\"")
            }
            if (tableColumn?.identifier)!.rawValue == "storyname" {
                cell.textField?.stringValue = SharingStoriesArray[row].StoryName
                //print("STORYNAME =  \"\(SharingStoriesArray[row].StoryName)\"")
            }
            if (tableColumn?.identifier)!.rawValue == "share" {
                cell.textField?.stringValue = SharingStoriesArray[row].StoryShare
                //print("SHARE =  \"\(SharingStoriesArray[row].StoryShare)\"")
            }
            if (tableColumn?.identifier)!.rawValue == "co" {
                cell.textField?.stringValue = SharingStoriesArray[row].StoryCo
                //print("CO =  \"\(SharingStoriesArray[row].StoryCo)\"")
            }
            
        }

        if (tableView == SharingCharactersTable) {
        //if (CharactersTableIsDoneLoading == false) {
            //print("tableColumn?.identifier)!.rawValue: \"\((tableColumn?.identifier)!.rawValue)\"")
            
            if (tableColumn?.identifier)!.rawValue == "ikey" {
                cell.textField?.stringValue = SharingCharactersArray[row].Ikey
                //print("IKEY =  \"\(SharingStoriesArray[row].Ikey)\"")
            }
            if (tableColumn?.identifier)!.rawValue == "charactername" {
                cell.textField?.stringValue = SharingCharactersArray[row].CharacterName
                //print("STORYNAME =  \"\(SharingStoriesArray[row].StoryName)\"")
            }
            if (tableColumn?.identifier)!.rawValue == "share" {
                cell.textField?.stringValue = SharingCharactersArray[row].CharacterShare
                //print("SHARE =  \"\(SharingStoriesArray[row].StoryShare)\"")
            }
            if (tableColumn?.identifier)!.rawValue == "edit" {
                cell.textField?.stringValue = SharingCharactersArray[row].CharacterEdit
                //print("CO =  \"\(SharingStoriesArray[row].StoryCo)\"")
            }
            
        }
        
        if (tableView == SharingStoryIdeasTable) {
            //if (CharactersTableIsDoneLoading == false) {
            //print("tableColumn?.identifier)!.rawValue: \"\((tableColumn?.identifier)!.rawValue)\"")
            
            if (tableColumn?.identifier)!.rawValue == "ikey" {
                cell.textField?.stringValue = SharingStoryIdeasArray[row].Ikey
                //print("IKEY =  \"\(SharingStoriesArray[row].Ikey)\"")
            }
            if (tableColumn?.identifier)!.rawValue == "storyideaname" {
                cell.textField?.stringValue = SharingStoryIdeasArray[row].StoryIdeaName
                //print("STORYNAME =  \"\(SharingStoriesArray[row].StoryName)\"")
            }
            if (tableColumn?.identifier)!.rawValue == "share" {
                cell.textField?.stringValue = SharingStoryIdeasArray[row].StoryIdeaShare
                //print("SHARE =  \"\(SharingStoriesArray[row].StoryShare)\"")
            }
            if (tableColumn?.identifier)!.rawValue == "edit" {
                cell.textField?.stringValue = SharingStoryIdeasArray[row].StoryIdeaEdit
                //print("CO =  \"\(SharingStoriesArray[row].StoryCo)\"")
            }
            
        }
        
        
        if (tableView == SharingLocationsTable) {
            //if (CharactersTableIsDoneLoading == false) {
            //print("tableColumn?.identifier)!.rawValue: \"\((tableColumn?.identifier)!.rawValue)\"")
            
            if (tableColumn?.identifier)!.rawValue == "ikey" {
                cell.textField?.stringValue = SharingLocationsArray[row].Ikey
                //print("IKEY =  \"\(SharingStoriesArray[row].Ikey)\"")
            }
            if (tableColumn?.identifier)!.rawValue == "locationname" {
                cell.textField?.stringValue = SharingLocationsArray[row].LocationName
                //print("STORYNAME =  \"\(SharingStoriesArray[row].StoryName)\"")
            }
            if (tableColumn?.identifier)!.rawValue == "share" {
                cell.textField?.stringValue = SharingLocationsArray[row].LocationShare
                //print("SHARE =  \"\(SharingStoriesArray[row].StoryShare)\"")
            }
            if (tableColumn?.identifier)!.rawValue == "edit" {
                cell.textField?.stringValue = SharingLocationsArray[row].LocationEdit
                //print("CO =  \"\(SharingStoriesArray[row].StoryCo)\"")
            }
            
        }


        return cell

    }



    func tableViewSelectionDidChange(_ notification: Notification) {


        let SelectedTable = (notification.object as! NSTableView)

        if (SelectedTable == SharingStoryTable) {

            let SelectedRow = SharingStoryTable.selectedRow
            //print("SelectedRow: \(SelectedRow)")
            
            let isIndexValid = SharingStoriesArray.indices.contains(SelectedRow)
            
            if isIndexValid {
                let StoryIkey: String = SharingStoriesArray[SelectedRow].Ikey
                let StoryName: String = SharingStoriesArray[SelectedRow].StoryName
                let StoryShare: String = SharingStoriesArray[SelectedRow].StoryShare
                let StoryCO: String = SharingStoriesArray[SelectedRow].StoryCo

                print("StoryIkey: \"\(StoryIkey)\"\nStoryName: \(StoryName)\nStoryShare: \(StoryShare)\nStoryCO: \(StoryCO)")

                SharingStoryYesShareButton.isEnabled = true
                SharingStoryNoShareButton.isEnabled = true
                SharingStoryYesCoButton.isEnabled = true
                SharingStoryNoCoButton.isEnabled = true
                
            } else {
                
                SharingStoryYesShareButton.isEnabled = false
                SharingStoryNoShareButton.isEnabled = false
                SharingStoryYesCoButton.isEnabled = false
                SharingStoryNoCoButton.isEnabled = false
                
            }

        }
        
        
        if (SelectedTable == SharingCharactersTable) {
            
            let SelectedRow = SharingCharactersTable.selectedRow
            //print("SelectedRow: \(SelectedRow)")
            
            let isIndexValid = SharingCharactersArray.indices.contains(SelectedRow)
            
            if isIndexValid {
                let CharacterIkey: String = SharingCharactersArray[SelectedRow].Ikey
                let CharacterName: String = SharingCharactersArray[SelectedRow].CharacterName
                let CharacterShare: String = SharingCharactersArray[SelectedRow].CharacterShare
                let CharacterEdit: String = SharingCharactersArray[SelectedRow].CharacterEdit
                
                print("CharacterIkey: \"\(CharacterIkey)\"\nCharacterName: \(CharacterName)\nCharacterShare: \(CharacterShare)\nCharacterEdit: \(CharacterEdit)")
                
                SharingCharactersYesShareButton.isEnabled = true
                SharingCharactersNoShareButton.isEnabled = true
                SharingCharactersYesEditButton.isEnabled = true
                SharingCharactersNoEditButton.isEnabled = true
                
            } else {
                
                SharingCharactersYesShareButton.isEnabled = false
                SharingCharactersNoShareButton.isEnabled = false
                SharingCharactersYesEditButton.isEnabled = false
                SharingCharactersNoEditButton.isEnabled = false
                
            }
            
        }
        
        
        if (SelectedTable == SharingStoryIdeasTable) {
            
            let SelectedRow = SharingStoryIdeasTable.selectedRow
            //print("SelectedRow: \(SelectedRow)")
            
            let isIndexValid = SharingStoryIdeasArray.indices.contains(SelectedRow)
            
            if isIndexValid {
                let StoryIdeasIkey: String = SharingStoryIdeasArray[SelectedRow].Ikey
                let StoryIdeasName: String = SharingStoryIdeasArray[SelectedRow].StoryIdeaName
                let StoryIdeasShare: String = SharingStoryIdeasArray[SelectedRow].StoryIdeaShare
                let StoryIdeasEdit: String = SharingStoryIdeasArray[SelectedRow].StoryIdeaEdit
                
                print("StoryIdeasIkey: \"\(StoryIdeasIkey)\"\nStoryIdeasName: \(StoryIdeasName)\nStoryIdeasShare: \(StoryIdeasShare)\nStoryIdeasEdit: \(StoryIdeasEdit)")
                
                SharingStoryIdeasYesShareButton.isEnabled = true
                SharingStoryIdeasNoShareButton.isEnabled = true
                SharingStoryIdeasYesEditButton.isEnabled = true
                SharingStoryIdeasNoEditButton.isEnabled = true
                
            } else {
                
                SharingStoryIdeasYesShareButton.isEnabled = false
                SharingStoryIdeasNoShareButton.isEnabled = false
                SharingStoryIdeasYesEditButton.isEnabled = false
                SharingStoryIdeasNoEditButton.isEnabled = false
                
            }
            
        }
        
        
        if (SelectedTable == SharingLocationsTable) {
            
            let SelectedRow = SharingLocationsTable.selectedRow
            //print("SelectedRow: \(SelectedRow)")
            
            let isIndexValid = SharingLocationsArray.indices.contains(SelectedRow)
            
            if isIndexValid {
                let LocationsIkey: String = SharingLocationsArray[SelectedRow].Ikey
                let LocationsName: String = SharingLocationsArray[SelectedRow].LocationName
                let LocationsShare: String = SharingLocationsArray[SelectedRow].LocationShare
                let LocationsEdit: String = SharingLocationsArray[SelectedRow].LocationEdit
                
                print("LocationsIkey: \"\(LocationsIkey)\"\nLocationsName: \(LocationsName)\nLocationsShare: \(LocationsShare)\nLocationsEdit: \(LocationsEdit)")
                
                SharingLocationsYesShareButton.isEnabled = true
                SharingLocationsNoShareButton.isEnabled = true
                SharingLocationsYesEditButton.isEnabled = true
                SharingLocationsNoEditButton.isEnabled = true
                
            } else {
                
                SharingLocationsYesShareButton.isEnabled = false
                SharingLocationsNoShareButton.isEnabled = false
                SharingLocationsYesEditButton.isEnabled = false
                SharingLocationsNoEditButton.isEnabled = false
                
            }
            
        }
        
        
        

    } // end of tableViewSelectionDidChange
    
    
    /////////////////////////////////
    
    // Story Sharing Buttons
    @IBAction func StorySharingYesShareButtonAction(_ sender: Any) {
        
        let SelectedRow = SharingStoryTable.selectedRow
        //print("SelectedRow: \(SelectedRow)")
        
        let isIndexValid = SharingStoriesArray.indices.contains(SelectedRow)
        
        var StoryIkey = String()
        
        if isIndexValid {
            StoryIkey = SharingStoriesArray[SelectedRow].Ikey
            //let StoryName: String = SharingStoriesArray[SelectedRow].StoryName
            //let StoryShare: String = SharingStoriesArray[SelectedRow].StoryShare
            //let StoryCO: String = SharingStoriesArray[SelectedRow].StoryCo
        } else {
            let alert = NSAlert()
            alert.messageText = "No Story Selected"
            alert.informativeText = "Please Select a Story to Share"
            alert.alertStyle = .warning
            alert.addButton(withTitle: "OK")

            
            if (CurrentAppearanceSharingViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.window.titlebarAppearsTransparent = true
            
            let returnCode = alert.runModal()
            print("returnCode: \(returnCode)")
            
            if (returnCode.rawValue == 1000) {
                print("OK Pressed")
                return
            }
            
        }
        
        
        let StoryCountQuery = String(format: "SELECT count(ikey) as cnt from storysharingtable WHERE userikeyreference = \(CurrentUserIkey!) AND sharewithuserikeyreference = (select ikey from users where user = \"\(UserDropDownSelection)\") AND storyikey = \"\(StoryIkey)\" ")
        
        print("StoryCountQuery:\n\(StoryCountQuery)")
        
        var StoryCount = Int32()
        
        do {
            let StoryCountQueryRun = try db.executeQuery(StoryCountQuery, values:nil)
            
            while StoryCountQueryRun.next() {
                StoryCount = StoryCountQueryRun.int(forColumn: "cnt")
            }
            
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(StoryCountQuery)\"")
            DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(StoryCountQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
            return
        }
        
        ////////////////////////////
        
        if (StoryCount == 0) {
            
            var UpdateStoryShareSqlquery = String()
            
            do {
                UpdateStoryShareSqlquery = String(format: "INSERT INTO storysharingtable (ikey,userikeyreference,sharewithuserikeyreference,storyikey,storyshare) VALUES ((select MAX(ikey)+ 1 from storysharingtable),\"\(CurrentUserIkey!)\",(select ikey from users where user = \"\(UserDropDownSelection)\"),\(StoryIkey),\"Yes\")")
                
                print("UpdateStoryShareSqlquery: \(UpdateStoryShareSqlquery)")
                
                try db.executeUpdate(UpdateStoryShareSqlquery, values: nil)
                
            } catch let error as NSError {
                
                print("ERROR: \(error)\n\n\(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(UpdateStoryShareSqlquery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(UpdateStoryShareSqlquery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            let indexset: IndexSet = [SelectedRow]
            StorieTableData()
            SharingStoryTable.selectRowIndexes(indexset, byExtendingSelection: false)
            
            
        } else if (StoryCount != 0) {
            
            let StoryQuery = String(format: "SELECT * from storysharingtable WHERE userikeyreference = \(CurrentUserIkey!) AND sharewithuserikeyreference = (select ikey from users where user = \"\(UserDropDownSelection)\") AND storyikey = \"\(StoryIkey)\" ")
            
            print("StoryQuery:\n\(StoryQuery)")
            
            var StoryIkey = String()
            
            do {
                let StoryQueryRun = try db.executeQuery(StoryQuery, values:nil)
                
                while StoryQueryRun.next() {
                    StoryIkey = StoryQueryRun.string(forColumn: "ikey")!
                }
                
            } catch {
                print("ERROR: \(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(StoryQuery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(StoryQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            /////////////////////////////
            
            var UpdateStoryShareSqlquery = String()
            
            do {
                UpdateStoryShareSqlquery = String(format: "UPDATE storysharingtable SET storyshare = \"Yes\" WHERE ikey = \"\(StoryIkey)\" ")
                
                print("UpdateStoryShareSqlquery: \(UpdateStoryShareSqlquery)")
                
                try db.executeUpdate(UpdateStoryShareSqlquery, values: nil)
                
            } catch let error as NSError {
                
                print("ERROR: \(error)\n\n\(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(UpdateStoryShareSqlquery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(UpdateStoryShareSqlquery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            let indexset: IndexSet = [SelectedRow]
            StorieTableData()
            SharingStoryTable.selectRowIndexes(indexset, byExtendingSelection: false)

        }

    }  // end of StorySharingYesShareButtonAction
    @IBAction func StorySharingNoShareButtonAction(_ sender: Any) {
        
        
        let SelectedRow = SharingStoryTable.selectedRow
        //print("SelectedRow: \(SelectedRow)")
        
        let isIndexValid = SharingStoriesArray.indices.contains(SelectedRow)
        
        var StoryIkey = String()
        
        if isIndexValid {
            StoryIkey = SharingStoriesArray[SelectedRow].Ikey
            //let StoryName: String = SharingStoriesArray[SelectedRow].StoryName
            //let StoryShare: String = SharingStoriesArray[SelectedRow].StoryShare
            //let StoryCO: String = SharingStoriesArray[SelectedRow].StoryCo
        } else {
            let alert = NSAlert()
            alert.messageText = "No Story Selected"
            alert.informativeText = "Please Select a Story to Share"
            alert.alertStyle = .warning
            alert.addButton(withTitle: "OK")
            
            
            if (CurrentAppearanceSharingViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.window.titlebarAppearsTransparent = true
            
            let returnCode = alert.runModal()
            print("returnCode: \(returnCode)")
            
            if (returnCode.rawValue == 1000) {
                print("OK Pressed")
                return
            }
            
        }
        
        
        let StoryCountQuery = String(format: "SELECT count(ikey) as cnt from storysharingtable WHERE userikeyreference = \(CurrentUserIkey!) AND sharewithuserikeyreference = (select ikey from users where user = \"\(UserDropDownSelection)\") AND storyikey = \"\(StoryIkey)\" ")
        
        print("StoryCountQuery:\n\(StoryCountQuery)")
        
        var StoryCount = Int32()
        
        do {
            let StoryCountQueryRun = try db.executeQuery(StoryCountQuery, values:nil)
            
            while StoryCountQueryRun.next() {
                StoryCount = StoryCountQueryRun.int(forColumn: "cnt")
            }
            
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(StoryCountQuery)\"")
            DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(StoryCountQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
            return
        }
        
        ////////////////////////////
        
        if (StoryCount == 0) {
            
            var UpdateStoryShareSqlquery = String()
            
            do {
                UpdateStoryShareSqlquery = String(format: "INSERT INTO storysharingtable (ikey,userikeyreference,sharewithuserikeyreference,storyikey,storyshare,storyedit) VALUES ((select MAX(ikey)+ 1 from storysharingtable),\"\(CurrentUserIkey!)\",(select ikey from users where user = \"\(UserDropDownSelection)\"),\(StoryIkey),\"No\",\"No\")")
                
                print("UpdateStoryShareSqlquery: \(UpdateStoryShareSqlquery)")
                
                try db.executeUpdate(UpdateStoryShareSqlquery, values: nil)
                
            } catch let error as NSError {
                
                print("ERROR: \(error)\n\n\(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(UpdateStoryShareSqlquery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(UpdateStoryShareSqlquery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            let indexset: IndexSet = [SelectedRow]
            StorieTableData()
            SharingStoryTable.selectRowIndexes(indexset, byExtendingSelection: false)
            
            
        } else if (StoryCount != 0) {
            
            let StoryQuery = String(format: "SELECT * from storysharingtable WHERE userikeyreference = \(CurrentUserIkey!) AND sharewithuserikeyreference = (select ikey from users where user = \"\(UserDropDownSelection)\") AND storyikey = \"\(StoryIkey)\" ")
            
            print("StoryQuery:\n\(StoryQuery)")
            
            var StoryIkey = String()
            
            do {
                let StoryQueryRun = try db.executeQuery(StoryQuery, values:nil)
                
                while StoryQueryRun.next() {
                    StoryIkey = StoryQueryRun.string(forColumn: "ikey")!
                }
                
            } catch {
                print("ERROR: \(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(StoryQuery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(StoryQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            /////////////////////////////
            
            var UpdateStoryShareSqlquery = String()
            
            do {
                UpdateStoryShareSqlquery = String(format: "UPDATE storysharingtable SET storyshare = \"No\", storyedit = \"No\" WHERE ikey = \"\(StoryIkey)\" ")
                
                print("UpdateStoryShareSqlquery: \(UpdateStoryShareSqlquery)")
                
                try db.executeUpdate(UpdateStoryShareSqlquery, values: nil)
                
            } catch let error as NSError {
                
                print("ERROR: \(error)\n\n\(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(UpdateStoryShareSqlquery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(UpdateStoryShareSqlquery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            
            let indexset: IndexSet = [SelectedRow]
            StorieTableData()
            SharingStoryTable.selectRowIndexes(indexset, byExtendingSelection: false)
            
            
        }
        
        
    } // end of StorySharingNoShareButtonAction
    @IBAction func StorySharingYesCOButtonAction(_ sender: Any) {
        
        
        let SelectedRow = SharingStoryTable.selectedRow
        //print("SelectedRow: \(SelectedRow)")
        
        let isIndexValid = SharingStoriesArray.indices.contains(SelectedRow)
        
        var StoryIkey = String()
        
        if isIndexValid {
            StoryIkey = SharingStoriesArray[SelectedRow].Ikey
            //let StoryName: String = SharingStoriesArray[SelectedRow].StoryName
            //let StoryShare: String = SharingStoriesArray[SelectedRow].StoryShare
            //let StoryCO: String = SharingStoriesArray[SelectedRow].StoryCo
        } else {
            let alert = NSAlert()
            alert.messageText = "No Story Selected"
            alert.informativeText = "Please Select a Story to Share"
            alert.alertStyle = .warning
            alert.addButton(withTitle: "OK")
            
            
            if (CurrentAppearanceSharingViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.window.titlebarAppearsTransparent = true
            
            let returnCode = alert.runModal()
            print("returnCode: \(returnCode)")
            
            if (returnCode.rawValue == 1000) {
                print("OK Pressed")
                return
            }
            
        }
        
        
        let StoryCountQuery = String(format: "SELECT count(ikey) as cnt from storysharingtable WHERE userikeyreference = \(CurrentUserIkey!) AND sharewithuserikeyreference = (select ikey from users where user = \"\(UserDropDownSelection)\") AND storyikey = \"\(StoryIkey)\" ")
        
        print("StoryCountQuery:\n\(StoryCountQuery)")
        
        var StoryCount = Int32()
        
        do {
            let StoryCountQueryRun = try db.executeQuery(StoryCountQuery, values:nil)
            
            while StoryCountQueryRun.next() {
                StoryCount = StoryCountQueryRun.int(forColumn: "cnt")
            }
            
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(StoryCountQuery)\"")
            DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(StoryCountQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
            return
        }
        
        ////////////////////////////
        
        if (StoryCount == 0) {
            
            var UpdateStoryShareSqlquery = String()
            
            do {
                UpdateStoryShareSqlquery = String(format: "INSERT INTO storysharingtable (ikey,userikeyreference,sharewithuserikeyreference,storyikey,storyedit,storyshare) VALUES ((select MAX(ikey)+ 1 from storysharingtable),\"\(CurrentUserIkey!)\",(select ikey from users where user = \"\(UserDropDownSelection)\"),\(StoryIkey),\"Yes\",\"Yes\")")
                
                print("UpdateStoryShareSqlquery: \(UpdateStoryShareSqlquery)")
                
                try db.executeUpdate(UpdateStoryShareSqlquery, values: nil)
                
            } catch let error as NSError {
                
                print("ERROR: \(error)\n\n\(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(UpdateStoryShareSqlquery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(UpdateStoryShareSqlquery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            let indexset: IndexSet = [SelectedRow]
            StorieTableData()
            SharingStoryTable.selectRowIndexes(indexset, byExtendingSelection: false)
            
            
        } else if (StoryCount != 0) {
            
            let StoryQuery = String(format: "SELECT * from storysharingtable WHERE userikeyreference = \(CurrentUserIkey!) AND sharewithuserikeyreference = (select ikey from users where user = \"\(UserDropDownSelection)\") AND storyikey = \"\(StoryIkey)\" ")
            
            print("StoryQuery:\n\(StoryQuery)")
            
            var StoryIkey = String()
            
            do {
                let StoryQueryRun = try db.executeQuery(StoryQuery, values:nil)
                
                while StoryQueryRun.next() {
                    StoryIkey = StoryQueryRun.string(forColumn: "ikey")!
                }
                
            } catch {
                print("ERROR: \(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(StoryQuery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(StoryQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            /////////////////////////////
            
            var UpdateStoryShareSqlquery = String()
            
            do {
                UpdateStoryShareSqlquery = String(format: "UPDATE storysharingtable SET storyedit = \"Yes\", storyshare = \"Yes\" WHERE ikey = \"\(StoryIkey)\" ")
                
                print("UpdateStoryShareSqlquery: \(UpdateStoryShareSqlquery)")
                
                try db.executeUpdate(UpdateStoryShareSqlquery, values: nil)
                
            } catch let error as NSError {
                
                print("ERROR: \(error)\n\n\(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(UpdateStoryShareSqlquery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(UpdateStoryShareSqlquery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            
            let indexset: IndexSet = [SelectedRow]
            StorieTableData()
            SharingStoryTable.selectRowIndexes(indexset, byExtendingSelection: false)
            
            
        }
        
        
    }//  end of StorySharingYesCOButtonAction
    @IBAction func StorySharingNoCOButtonAction(_ sender: Any) {
        
        
        let SelectedRow = SharingStoryTable.selectedRow
        //print("SelectedRow: \(SelectedRow)")
        
        let isIndexValid = SharingStoriesArray.indices.contains(SelectedRow)
        
        var StoryIkey = String()
        
        if isIndexValid {
            StoryIkey = SharingStoriesArray[SelectedRow].Ikey
            //let StoryName: String = SharingStoriesArray[SelectedRow].StoryName
            //let StoryShare: String = SharingStoriesArray[SelectedRow].StoryShare
            //let StoryCO: String = SharingStoriesArray[SelectedRow].StoryCo
        } else {
            let alert = NSAlert()
            alert.messageText = "No Story Selected"
            alert.informativeText = "Please Select a Story to Share"
            alert.alertStyle = .warning
            alert.addButton(withTitle: "OK")
            
            
            if (CurrentAppearanceSharingViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.window.titlebarAppearsTransparent = true
            
            let returnCode = alert.runModal()
            print("returnCode: \(returnCode)")
            
            if (returnCode.rawValue == 1000) {
                print("OK Pressed")
                return
            }
            
        }
        
        
        let StoryCountQuery = String(format: "SELECT count(ikey) as cnt from storysharingtable WHERE userikeyreference = \(CurrentUserIkey!) AND sharewithuserikeyreference = (select ikey from users where user = \"\(UserDropDownSelection)\") AND storyikey = \"\(StoryIkey)\" ")
        
        print("StoryCountQuery:\n\(StoryCountQuery)")
        
        var StoryCount = Int32()
        
        do {
            let StoryCountQueryRun = try db.executeQuery(StoryCountQuery, values:nil)
            
            while StoryCountQueryRun.next() {
                StoryCount = StoryCountQueryRun.int(forColumn: "cnt")
            }
            
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(StoryCountQuery)\"")
            DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(StoryCountQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
            return
        }
        
        ////////////////////////////
        
        if (StoryCount == 0) {
            
            var UpdateStoryShareSqlquery = String()
            
            do {
                UpdateStoryShareSqlquery = String(format: "INSERT INTO storysharingtable (ikey,userikeyreference,sharewithuserikeyreference,storyikey,storyshare) VALUES ((select MAX(ikey)+ 1 from storysharingtable),\"\(CurrentUserIkey!)\",(select ikey from users where user = \"\(UserDropDownSelection)\"),\(StoryIkey),\"No\")")
                
                print("UpdateStoryShareSqlquery: \(UpdateStoryShareSqlquery)")
                
                try db.executeUpdate(UpdateStoryShareSqlquery, values: nil)
                
            } catch let error as NSError {
                
                print("ERROR: \(error)\n\n\(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(UpdateStoryShareSqlquery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(UpdateStoryShareSqlquery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            let indexset: IndexSet = [SelectedRow]
            StorieTableData()
            SharingStoryTable.selectRowIndexes(indexset, byExtendingSelection: false)
            
            
        } else if (StoryCount != 0) {
            
            let StoryQuery = String(format: "SELECT * from storysharingtable WHERE userikeyreference = \(CurrentUserIkey!) AND sharewithuserikeyreference = (select ikey from users where user = \"\(UserDropDownSelection)\") AND storyikey = \"\(StoryIkey)\" ")
            
            print("StoryQuery:\n\(StoryQuery)")
            
            var StoryIkey = String()
            
            do {
                let StoryQueryRun = try db.executeQuery(StoryQuery, values:nil)
                
                while StoryQueryRun.next() {
                    StoryIkey = StoryQueryRun.string(forColumn: "ikey")!
                }
                
            } catch {
                print("ERROR: \(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(StoryQuery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(StoryQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            /////////////////////////////
            
            var UpdateStoryShareSqlquery = String()
            
            do {
                UpdateStoryShareSqlquery = String(format: "UPDATE storysharingtable SET storyedit = \"No\" WHERE ikey = \"\(StoryIkey)\" ")
                
                print("UpdateStoryShareSqlquery: \(UpdateStoryShareSqlquery)")
                
                try db.executeUpdate(UpdateStoryShareSqlquery, values: nil)
                
            } catch let error as NSError {
                
                print("ERROR: \(error)\n\n\(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(UpdateStoryShareSqlquery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(UpdateStoryShareSqlquery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            
            let indexset: IndexSet = [SelectedRow]
            StorieTableData()
            SharingStoryTable.selectRowIndexes(indexset, byExtendingSelection: false)
            
            
        }
        
        
    }// end of StorySharingNoCOButtonAction
    
    // Character Sharing Buttons
    @IBAction func CharacterSharingYesShareButtonAction(_ sender: Any) {
        
        
        let SelectedRow = SharingCharactersTable.selectedRow
        //print("SelectedRow: \(SelectedRow)")
        
        let isIndexValid = SharingCharactersArray.indices.contains(SelectedRow)
        
        var CharacterIkey = String()
        
        if isIndexValid {
            CharacterIkey = SharingCharactersArray[SelectedRow].Ikey
            //let StoryName: String = SharingStoriesArray[SelectedRow].StoryName
            //let StoryShare: String = SharingStoriesArray[SelectedRow].StoryShare
            //let StoryCO: String = SharingStoriesArray[SelectedRow].StoryCo
        } else {
            let alert = NSAlert()
            alert.messageText = "No Character Selected"
            alert.informativeText = "Please Select a Character to Share"
            alert.alertStyle = .warning
            alert.addButton(withTitle: "OK")
            
            
            if (CurrentAppearanceSharingViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.window.titlebarAppearsTransparent = true
            
            let returnCode = alert.runModal()
            print("returnCode: \(returnCode)")
            
            if (returnCode.rawValue == 1000) {
                print("OK Pressed")
                return
            }
            
        }
        
        
        let CharacterCountQuery = String(format: "SELECT count(ikey) as cnt from charactersharingtable WHERE userikeyreference = \(CurrentUserIkey!) AND sharewithuserikeyreference = (select ikey from users where user = \"\(UserDropDownSelection)\") AND characterikey = \"\(CharacterIkey)\" ")
        
        print("CharacterCountQuery:\n\(CharacterCountQuery)")
        
        var CharacterCount = Int32()
        
        do {
            let CharacterCountQueryRun = try db.executeQuery(CharacterCountQuery, values:nil)
            
            while CharacterCountQueryRun.next() {
                CharacterCount = CharacterCountQueryRun.int(forColumn: "cnt")
            }
            
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(CharacterCountQuery)\"")
            DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(CharacterCountQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
            return
        }
        
        ////////////////////////////
        
        if (CharacterCount == 0) {
            
            var UpdateCharacterShareSqlquery = String()
            
            // INSERT INTO charactersharingtable (ikey,userikeyreference,sharewithuserikeyreference,characterikey,charactershare) VALUES (%@,%ld,%@,%d,%@)",@"(select MAX(ikey)+ 1 from charactersharingtable)",CurrentUserIkey,query2,CharacterIkeyint,@"\"Yes\""
            
            
            
            do {
                UpdateCharacterShareSqlquery = String(format: "INSERT INTO charactersharingtable (ikey,userikeyreference,sharewithuserikeyreference,characterikey,charactershare) VALUES ((select MAX(ikey)+ 1 from charactersharingtable),\"\(CurrentUserIkey!)\",(select ikey from users where user = \"\(UserDropDownSelection)\"),\(CharacterIkey),\"Yes\")")
                
                print("UpdateCharacterShareSqlquery: \(UpdateCharacterShareSqlquery)")
                
                try db.executeUpdate(UpdateCharacterShareSqlquery, values: nil)
                
            } catch let error as NSError {
                
                print("ERROR: \(error)\n\n\(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(UpdateCharacterShareSqlquery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(UpdateCharacterShareSqlquery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            let indexset: IndexSet = [SelectedRow]
            CharacterTableData()
            SharingCharactersTable.selectRowIndexes(indexset, byExtendingSelection: false)
            
            
        } else if (CharacterCount != 0) {
            
            let CharacterQuery = String(format: "SELECT * from charactersharingtable WHERE userikeyreference = \(CurrentUserIkey!) AND sharewithuserikeyreference = (select ikey from users where user = \"\(UserDropDownSelection)\") AND characterikey = \"\(CharacterIkey)\" ")
            
            print("CharacterQuery:\n\(CharacterQuery)")
            
            var CharacterIkey = String()
            
            do {
                let CharacterQueryRun = try db.executeQuery(CharacterQuery, values:nil)
                
                while CharacterQueryRun.next() {
                    CharacterIkey = CharacterQueryRun.string(forColumn: "ikey")!
                }
                
            } catch {
                print("ERROR: \(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(CharacterQuery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(CharacterQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            /////////////////////////////
            
            var UpdateCharacterShareSqlquery = String()
            
            do {
                UpdateCharacterShareSqlquery = String(format: "UPDATE charactersharingtable SET charactershare = \"Yes\" WHERE ikey = \"\(CharacterIkey)\" ")
                
                print("UpdateCharacterShareSqlquery: \(UpdateCharacterShareSqlquery)")
                
                try db.executeUpdate(UpdateCharacterShareSqlquery, values: nil)
                
            } catch let error as NSError {
                
                print("ERROR: \(error)\n\n\(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(UpdateCharacterShareSqlquery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(UpdateCharacterShareSqlquery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            let indexset: IndexSet = [SelectedRow]
            CharacterTableData()
            SharingCharactersTable.selectRowIndexes(indexset, byExtendingSelection: false)
            
        }
        
        
    }
    @IBAction func CharacterSharingNoShareButtonAction(_ sender: Any) {
        
        
        let SelectedRow = SharingCharactersTable.selectedRow
        //print("SelectedRow: \(SelectedRow)")
        
        let isIndexValid = SharingCharactersArray.indices.contains(SelectedRow)
        
        var CharacterIkey = String()
        
        if isIndexValid {
            CharacterIkey = SharingCharactersArray[SelectedRow].Ikey
            //let StoryName: String = SharingStoriesArray[SelectedRow].StoryName
            //let StoryShare: String = SharingStoriesArray[SelectedRow].StoryShare
            //let StoryCO: String = SharingStoriesArray[SelectedRow].StoryCo
        } else {
            let alert = NSAlert()
            alert.messageText = "No Character Selected"
            alert.informativeText = "Please Select a Character to Share"
            alert.alertStyle = .warning
            alert.addButton(withTitle: "OK")
            
            
            if (CurrentAppearanceSharingViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.window.titlebarAppearsTransparent = true
            
            let returnCode = alert.runModal()
            print("returnCode: \(returnCode)")
            
            if (returnCode.rawValue == 1000) {
                print("OK Pressed")
                return
            }
            
        }
        
        
        let CharacterCountQuery = String(format: "SELECT count(ikey) as cnt from charactersharingtable WHERE userikeyreference = \(CurrentUserIkey!) AND sharewithuserikeyreference = (select ikey from users where user = \"\(UserDropDownSelection)\") AND characterikey = \"\(CharacterIkey)\" ")
        
        print("CharacterCountQuery:\n\(CharacterCountQuery)")
        
        var CharacterCount = Int32()
        
        do {
            let CharacterCountQueryRun = try db.executeQuery(CharacterCountQuery, values:nil)
            
            while CharacterCountQueryRun.next() {
                CharacterCount = CharacterCountQueryRun.int(forColumn: "cnt")
            }
            
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(CharacterCountQuery)\"")
            DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(CharacterCountQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
            return
        }
        
        ////////////////////////////
        
        if (CharacterCount == 0) {
            
            var UpdateCharacterShareSqlquery = String()
            

            do {
                UpdateCharacterShareSqlquery = String(format: "INSERT INTO charactersharingtable (ikey,userikeyreference,sharewithuserikeyreference,characterikey,charactershare,characteredit) VALUES ((select MAX(ikey)+ 1 from charactersharingtable),\"\(CurrentUserIkey!)\",(select ikey from users where user = \"\(UserDropDownSelection)\"),\(CharacterIkey),\"No\",\"No\")")
                
                print("UpdateCharacterShareSqlquery: \(UpdateCharacterShareSqlquery)")
                
                try db.executeUpdate(UpdateCharacterShareSqlquery, values: nil)
                
            } catch let error as NSError {
                
                print("ERROR: \(error)\n\n\(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(UpdateCharacterShareSqlquery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(UpdateCharacterShareSqlquery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            let indexset: IndexSet = [SelectedRow]
            CharacterTableData()
            SharingCharactersTable.selectRowIndexes(indexset, byExtendingSelection: false)
            
            
        } else if (CharacterCount != 0) {
            
            let CharacterQuery = String(format: "SELECT * from charactersharingtable WHERE userikeyreference = \(CurrentUserIkey!) AND sharewithuserikeyreference = (select ikey from users where user = \"\(UserDropDownSelection)\") AND characterikey = \"\(CharacterIkey)\" ")
            
            print("CharacterQuery:\n\(CharacterQuery)")
            
            var CharacterIkey = String()
            
            do {
                let CharacterQueryRun = try db.executeQuery(CharacterQuery, values:nil)
                
                while CharacterQueryRun.next() {
                    CharacterIkey = CharacterQueryRun.string(forColumn: "ikey")!
                }
                
            } catch {
                print("ERROR: \(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(CharacterQuery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(CharacterQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            /////////////////////////////
            
            var UpdateCharacterShareSqlquery = String()
            
            do {
                UpdateCharacterShareSqlquery = String(format: "UPDATE charactersharingtable SET charactershare = \"No\",characteredit = \"No\" WHERE ikey = \"\(CharacterIkey)\" ")
                
                print("UpdateCharacterShareSqlquery: \(UpdateCharacterShareSqlquery)")
                
                try db.executeUpdate(UpdateCharacterShareSqlquery, values: nil)
                
            } catch let error as NSError {
                
                print("ERROR: \(error)\n\n\(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(UpdateCharacterShareSqlquery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(UpdateCharacterShareSqlquery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            let indexset: IndexSet = [SelectedRow]
            CharacterTableData()
            SharingCharactersTable.selectRowIndexes(indexset, byExtendingSelection: false)
            
        }
        
        
    }
    @IBAction func CharacterSharingYesEditButtonAction(_ sender: Any) {
        
        let SelectedRow = SharingCharactersTable.selectedRow
        //print("SelectedRow: \(SelectedRow)")
        
        let isIndexValid = SharingCharactersArray.indices.contains(SelectedRow)
        
        var CharacterIkey = String()
        
        if isIndexValid {
            CharacterIkey = SharingCharactersArray[SelectedRow].Ikey
            //let StoryName: String = SharingStoriesArray[SelectedRow].StoryName
            //let StoryShare: String = SharingStoriesArray[SelectedRow].StoryShare
            //let StoryCO: String = SharingStoriesArray[SelectedRow].StoryCo
        } else {
            let alert = NSAlert()
            alert.messageText = "No Character Selected"
            alert.informativeText = "Please Select a Character to Share"
            alert.alertStyle = .warning
            alert.addButton(withTitle: "OK")
            
            
            if (CurrentAppearanceSharingViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.window.titlebarAppearsTransparent = true
            
            let returnCode = alert.runModal()
            print("returnCode: \(returnCode)")
            
            if (returnCode.rawValue == 1000) {
                print("OK Pressed")
                return
            }
            
        }
        
        
        let CharacterCountQuery = String(format: "SELECT count(ikey) as cnt from charactersharingtable WHERE userikeyreference = \(CurrentUserIkey!) AND sharewithuserikeyreference = (select ikey from users where user = \"\(UserDropDownSelection)\") AND characterikey = \"\(CharacterIkey)\" ")
        
        print("CharacterCountQuery:\n\(CharacterCountQuery)")
        
        var CharacterCount = Int32()
        
        do {
            let CharacterCountQueryRun = try db.executeQuery(CharacterCountQuery, values:nil)
            
            while CharacterCountQueryRun.next() {
                CharacterCount = CharacterCountQueryRun.int(forColumn: "cnt")
            }
            
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(CharacterCountQuery)\"")
            DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(CharacterCountQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
            return
        }
        
        ////////////////////////////
        
        if (CharacterCount == 0) {
            
            var UpdateCharacterShareSqlquery = String()
            

            do {
                UpdateCharacterShareSqlquery = String(format: "INSERT INTO charactersharingtable (ikey,userikeyreference,sharewithuserikeyreference,characterikey,charactershare,characteredit) VALUES ((select MAX(ikey)+ 1 from charactersharingtable),\"\(CurrentUserIkey!)\",(select ikey from users where user = \"\(UserDropDownSelection)\"),\(CharacterIkey),\"Yes\",\"Yes\")")
                
                print("UpdateCharacterShareSqlquery: \(UpdateCharacterShareSqlquery)")
                
                try db.executeUpdate(UpdateCharacterShareSqlquery, values: nil)
                
            } catch let error as NSError {
                
                print("ERROR: \(error)\n\n\(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(UpdateCharacterShareSqlquery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(UpdateCharacterShareSqlquery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            let indexset: IndexSet = [SelectedRow]
            CharacterTableData()
            SharingCharactersTable.selectRowIndexes(indexset, byExtendingSelection: false)
            
            
        } else if (CharacterCount != 0) {
            
            let CharacterQuery = String(format: "SELECT * from charactersharingtable WHERE userikeyreference = \(CurrentUserIkey!) AND sharewithuserikeyreference = (select ikey from users where user = \"\(UserDropDownSelection)\") AND characterikey = \"\(CharacterIkey)\" ")
            
            print("CharacterQuery:\n\(CharacterQuery)")
            
            var CharacterIkey = String()
            
            do {
                let CharacterQueryRun = try db.executeQuery(CharacterQuery, values:nil)
                
                while CharacterQueryRun.next() {
                    CharacterIkey = CharacterQueryRun.string(forColumn: "ikey")!
                }
                
            } catch {
                print("ERROR: \(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(CharacterQuery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(CharacterQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            /////////////////////////////
            
            var UpdateCharacterShareSqlquery = String()
            
            do {
                UpdateCharacterShareSqlquery = String(format: "UPDATE charactersharingtable SET charactershare = \"Yes\", characteredit = \"Yes\" WHERE ikey = \"\(CharacterIkey)\" ")
                
                print("UpdateCharacterShareSqlquery: \(UpdateCharacterShareSqlquery)")
                
                try db.executeUpdate(UpdateCharacterShareSqlquery, values: nil)
                
            } catch let error as NSError {
                
                print("ERROR: \(error)\n\n\(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(UpdateCharacterShareSqlquery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(UpdateCharacterShareSqlquery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            let indexset: IndexSet = [SelectedRow]
            CharacterTableData()
            SharingCharactersTable.selectRowIndexes(indexset, byExtendingSelection: false)
            
        }
        
    }
    @IBAction func CharacterSharingNoEditButtonAction(_ sender: Any) {
        
        let SelectedRow = SharingCharactersTable.selectedRow
        //print("SelectedRow: \(SelectedRow)")
        
        let isIndexValid = SharingCharactersArray.indices.contains(SelectedRow)
        
        var CharacterIkey = String()
        
        if isIndexValid {
            CharacterIkey = SharingCharactersArray[SelectedRow].Ikey
            //let StoryName: String = SharingStoriesArray[SelectedRow].StoryName
            //let StoryShare: String = SharingStoriesArray[SelectedRow].StoryShare
            //let StoryCO: String = SharingStoriesArray[SelectedRow].StoryCo
        } else {
            let alert = NSAlert()
            alert.messageText = "No Character Selected"
            alert.informativeText = "Please Select a Character to Share"
            alert.alertStyle = .warning
            alert.addButton(withTitle: "OK")
            
            
            if (CurrentAppearanceSharingViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.window.titlebarAppearsTransparent = true
            
            let returnCode = alert.runModal()
            print("returnCode: \(returnCode)")
            
            if (returnCode.rawValue == 1000) {
                print("OK Pressed")
                return
            }
            
        }
        
        
        let CharacterCountQuery = String(format: "SELECT count(ikey) as cnt from charactersharingtable WHERE userikeyreference = \(CurrentUserIkey!) AND sharewithuserikeyreference = (select ikey from users where user = \"\(UserDropDownSelection)\") AND characterikey = \"\(CharacterIkey)\" ")
        
        print("CharacterCountQuery:\n\(CharacterCountQuery)")
        
        var CharacterCount = Int32()
        
        do {
            let CharacterCountQueryRun = try db.executeQuery(CharacterCountQuery, values:nil)
            
            while CharacterCountQueryRun.next() {
                CharacterCount = CharacterCountQueryRun.int(forColumn: "cnt")
            }
            
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(CharacterCountQuery)\"")
            DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(CharacterCountQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
            return
        }
        
        ////////////////////////////
        
        if (CharacterCount == 0) {
            
            var UpdateCharacterShareSqlquery = String()
            
            
            do {
                UpdateCharacterShareSqlquery = String(format: "INSERT INTO charactersharingtable (ikey,userikeyreference,sharewithuserikeyreference,characterikey,characteredit) VALUES ((select MAX(ikey)+ 1 from charactersharingtable),\"\(CurrentUserIkey!)\",(select ikey from users where user = \"\(UserDropDownSelection)\"),\(CharacterIkey),\"No\")")
                
                print("UpdateCharacterShareSqlquery: \(UpdateCharacterShareSqlquery)")
                
                try db.executeUpdate(UpdateCharacterShareSqlquery, values: nil)
                
            } catch let error as NSError {
                
                print("ERROR: \(error)\n\n\(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(UpdateCharacterShareSqlquery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(UpdateCharacterShareSqlquery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            let indexset: IndexSet = [SelectedRow]
            CharacterTableData()
            SharingCharactersTable.selectRowIndexes(indexset, byExtendingSelection: false)
            
            
        } else if (CharacterCount != 0) {
            
            let CharacterQuery = String(format: "SELECT * from charactersharingtable WHERE userikeyreference = \(CurrentUserIkey!) AND sharewithuserikeyreference = (select ikey from users where user = \"\(UserDropDownSelection)\") AND characterikey = \"\(CharacterIkey)\" ")
            
            print("CharacterQuery:\n\(CharacterQuery)")
            
            var CharacterIkey = String()
            
            do {
                let CharacterQueryRun = try db.executeQuery(CharacterQuery, values:nil)
                
                while CharacterQueryRun.next() {
                    CharacterIkey = CharacterQueryRun.string(forColumn: "ikey")!
                }
                
            } catch {
                print("ERROR: \(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(CharacterQuery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(CharacterQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            /////////////////////////////
            
            var UpdateCharacterShareSqlquery = String()
            
            do {
                UpdateCharacterShareSqlquery = String(format: "UPDATE charactersharingtable SET characteredit = \"No\" WHERE ikey = \"\(CharacterIkey)\" ")
                
                print("UpdateCharacterShareSqlquery: \(UpdateCharacterShareSqlquery)")
                
                try db.executeUpdate(UpdateCharacterShareSqlquery, values: nil)
                
            } catch let error as NSError {
                
                print("ERROR: \(error)\n\n\(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(UpdateCharacterShareSqlquery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(UpdateCharacterShareSqlquery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            let indexset: IndexSet = [SelectedRow]
            CharacterTableData()
            SharingCharactersTable.selectRowIndexes(indexset, byExtendingSelection: false)
            
        }
        
    }
    
    // Story Ideas Sharing Buttons
    @IBAction func StoryIdeaSharingYesShareButtonAction(_ sender: Any) {
        
        let SelectedRow = SharingStoryIdeasTable.selectedRow
        //print("SelectedRow: \(SelectedRow)")
        
        let isIndexValid = SharingStoryIdeasArray.indices.contains(SelectedRow)
        
        var StoryIdeaIkey = String()
        
        if isIndexValid {
            StoryIdeaIkey = SharingStoryIdeasArray[SelectedRow].Ikey
            //let StoryName: String = SharingStoriesArray[SelectedRow].StoryName
            //let StoryShare: String = SharingStoriesArray[SelectedRow].StoryShare
            //let StoryCO: String = SharingStoriesArray[SelectedRow].StoryCo
        } else {
            let alert = NSAlert()
            alert.messageText = "No StoryIdea Selected"
            alert.informativeText = "Please Select a StoryIdea to Share"
            alert.alertStyle = .warning
            alert.addButton(withTitle: "OK")
            
            
            if (CurrentAppearanceSharingViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.window.titlebarAppearsTransparent = true
            
            let returnCode = alert.runModal()
            print("returnCode: \(returnCode)")
            
            if (returnCode.rawValue == 1000) {
                print("OK Pressed")
                return
            }
            
        }
        
        
        let StoryIdeaCountQuery = String(format: "SELECT count(ikey) as cnt from storyideasharingtable WHERE userikeyreference = \(CurrentUserIkey!) AND sharewithuserikeyreference = (select ikey from users where user = \"\(UserDropDownSelection)\") AND storyideaikey = \"\(StoryIdeaIkey)\" ")
        
        print("StoryIdeaCountQuery:\n\(StoryIdeaCountQuery)")
        
        var StoryIdeaCount = Int32()
        
        do {
            let StoryIdeaCountQueryRun = try db.executeQuery(StoryIdeaCountQuery, values:nil)
            
            while StoryIdeaCountQueryRun.next() {
                StoryIdeaCount = StoryIdeaCountQueryRun.int(forColumn: "cnt")
            }
            
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(StoryIdeaCountQuery)\"")
            DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(StoryIdeaCountQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
            return
        }
        
        ////////////////////////////
        
        if (StoryIdeaCount == 0) {
            
            var UpdateCharacterShareSqlquery = String()
            
            // INSERT INTO storyideasharingtable (ikey,userikeyreference,sharewithuserikeyreference,storyideaikey,storyideashare) VALUES (%@,%ld,%@,%d,%@)",@"(select MAX(ikey)+ 1 from storyideasharingtable)",CurrentUserIkey,query2,StoryIdeaIkeyint,@"\"Yes\""
            
            
            
            do {
                UpdateCharacterShareSqlquery = String(format: "INSERT INTO storyideasharingtable (ikey,userikeyreference,sharewithuserikeyreference,storyideaikey,storyideashare) VALUES ((select MAX(ikey)+ 1 from storyideasharingtable),\"\(CurrentUserIkey!)\",(select ikey from users where user = \"\(UserDropDownSelection)\"),\(StoryIdeaIkey),\"Yes\")")
                
                print("UpdateCharacterShareSqlquery: \(UpdateCharacterShareSqlquery)")
                
                try db.executeUpdate(UpdateCharacterShareSqlquery, values: nil)
                
            } catch let error as NSError {
                
                print("ERROR: \(error)\n\n\(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(UpdateCharacterShareSqlquery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(UpdateCharacterShareSqlquery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            let indexset: IndexSet = [SelectedRow]
            StoryIdeasTableData()
            SharingStoryIdeasTable.selectRowIndexes(indexset, byExtendingSelection: false)
            
            
        } else if (StoryIdeaCount != 0) {
            

            
            let StoryIdeaIkeyQuery = String(format: "SELECT * from storyideasharingtable WHERE userikeyreference = \(CurrentUserIkey!) AND sharewithuserikeyreference = (select ikey from users where user = \"\(UserDropDownSelection)\") AND storyideaikey = \"\(StoryIdeaIkey)\" ")
            
            
            print("StoryIdeaIkeyQuery:\n\(StoryIdeaIkeyQuery)")
            
            var StoryIdeaIkeyDB = String()
            //var StoryIdeaStoryIkeyDB = String()
            var StoryIdeasharedwithuserikeyreferenceDB = String()
            
            do {
                let StoryIdeaIkeyQueryRun = try db.executeQuery(StoryIdeaIkeyQuery, values:nil)
                
                while StoryIdeaIkeyQueryRun.next() {
                    StoryIdeaIkeyDB = StoryIdeaIkeyQueryRun.string(forColumn: "ikey")!
                    //StoryIdeaStoryIkeyDB = StoryIdeaIkeyQueryRun.string(forColumn: "storyideaikey")!
                    StoryIdeasharedwithuserikeyreferenceDB = StoryIdeaIkeyQueryRun.string(forColumn: "sharewithuserikeyreference")!
                }
                
            } catch {
                print("ERROR: \(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(StoryIdeaIkeyQuery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(StoryIdeaIkeyQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            /////////////////////////////
            
            var UpdateStoryIdeaShareSqlquery = String()
            
            
            
            
            
            do {
                UpdateStoryIdeaShareSqlquery = String(format: "UPDATE storyideasharingtable SET storyideashare = \"Yes\" WHERE storyideaikey = \"\(StoryIdeaIkey)\" AND ikey = \"\(StoryIdeaIkeyDB)\" AND sharewithuserikeyreference =  \"\(StoryIdeasharedwithuserikeyreferenceDB)\"")

                
                print("UpdateStoryIdeaShareSqlquery: \(UpdateStoryIdeaShareSqlquery)")
                
                try db.executeUpdate(UpdateStoryIdeaShareSqlquery, values: nil)
                
            } catch let error as NSError {
                
                print("ERROR: \(error)\n\n\(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(UpdateStoryIdeaShareSqlquery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(UpdateStoryIdeaShareSqlquery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            let indexset: IndexSet = [SelectedRow]
            StoryIdeasTableData()
            SharingStoryIdeasTable.selectRowIndexes(indexset, byExtendingSelection: false)
            
        }
        
        
        
    }
    @IBAction func StoryIdeaSharingNoShareButtonAction(_ sender: Any) {
        
        let SelectedRow = SharingStoryIdeasTable.selectedRow
        //print("SelectedRow: \(SelectedRow)")
        
        let isIndexValid = SharingStoryIdeasArray.indices.contains(SelectedRow)
        
        var StoryIdeaIkey = String()
        
        if isIndexValid {
            StoryIdeaIkey = SharingStoryIdeasArray[SelectedRow].Ikey
            //let StoryName: String = SharingStoriesArray[SelectedRow].StoryName
            //let StoryShare: String = SharingStoriesArray[SelectedRow].StoryShare
            //let StoryCO: String = SharingStoriesArray[SelectedRow].StoryCo
        } else {
            let alert = NSAlert()
            alert.messageText = "No StoryIdea Selected"
            alert.informativeText = "Please Select a StoryIdea to Share"
            alert.alertStyle = .warning
            alert.addButton(withTitle: "OK")
            
            
            if (CurrentAppearanceSharingViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.window.titlebarAppearsTransparent = true
            
            let returnCode = alert.runModal()
            print("returnCode: \(returnCode)")
            
            if (returnCode.rawValue == 1000) {
                print("OK Pressed")
                return
            }
            
        }
        
       
        
        let StoryIdeaCountQuery = String(format: "SELECT count(ikey) as cnt from storyideasharingtable WHERE userikeyreference = \(CurrentUserIkey!) AND sharewithuserikeyreference = (select ikey from users where user = \"\(UserDropDownSelection)\") AND storyideaikey = \"\(StoryIdeaIkey)\" ")
        
        print("StoryIdeaCountQuery:\n\(StoryIdeaCountQuery)")
        
        var StoryIdeaCount = Int32()
        
        do {
            let StoryIdeaCountQueryRun = try db.executeQuery(StoryIdeaCountQuery, values:nil)
            
            while StoryIdeaCountQueryRun.next() {
                StoryIdeaCount = StoryIdeaCountQueryRun.int(forColumn: "cnt")
            }
            
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(StoryIdeaCountQuery)\"")
            DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(StoryIdeaCountQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
            return
        }
        
        ////////////////////////////
        
        if (StoryIdeaCount == 0) {
            
            var UpdateCharacterShareSqlquery = String()
            
            // INSERT INTO storyideasharingtable (ikey,userikeyreference,sharewithuserikeyreference,storyideaikey,storyideashare) VALUES (%@,%ld,%@,%d,%@)",@"(select MAX(ikey)+ 1 from storyideasharingtable)",CurrentUserIkey,query2,StoryIdeaIkeyint,@"\"Yes\""
            
            
            
            do {
                UpdateCharacterShareSqlquery = String(format: "INSERT INTO storyideasharingtable (ikey,userikeyreference,sharewithuserikeyreference,storyideaikey,storyideashare,storyideaedit) VALUES ((select MAX(ikey)+ 1 from storyideasharingtable),\"\(CurrentUserIkey!)\",(select ikey from users where user = \"\(UserDropDownSelection)\"),\(StoryIdeaIkey),\"No\",\"No\")")
                
                print("UpdateCharacterShareSqlquery: \(UpdateCharacterShareSqlquery)")
                
                try db.executeUpdate(UpdateCharacterShareSqlquery, values: nil)
                
            } catch let error as NSError {
                
                print("ERROR: \(error)\n\n\(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(UpdateCharacterShareSqlquery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(UpdateCharacterShareSqlquery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            let indexset: IndexSet = [SelectedRow]
            StoryIdeasTableData()
            SharingStoryIdeasTable.selectRowIndexes(indexset, byExtendingSelection: false)
            
            
        } else if (StoryIdeaCount != 0) {
            
            let StoryIdeaIkeyQuery = String(format: "SELECT * from storyideasharingtable WHERE userikeyreference = \(CurrentUserIkey!) AND sharewithuserikeyreference = (select ikey from users where user = \"\(UserDropDownSelection)\") AND storyideaikey = \"\(StoryIdeaIkey)\" ")
            
            print("StoryIdeaIkeyQuery:\n\(StoryIdeaIkeyQuery)")
            
            var StoryIdeaIkeyDB = String()
            //var StoryIdeaStoryIkeyDB = String()
            var StoryIdeasharedwithuserikeyreferenceDB = String()
            
            do {
                let StoryIdeaIkeyQueryRun = try db.executeQuery(StoryIdeaIkeyQuery, values:nil)
                
                while StoryIdeaIkeyQueryRun.next() {
                    StoryIdeaIkeyDB = StoryIdeaIkeyQueryRun.string(forColumn: "ikey")!
                    //StoryIdeaStoryIkeyDB = StoryIdeaIkeyQueryRun.string(forColumn: "storyideaikey")!
                    StoryIdeasharedwithuserikeyreferenceDB = StoryIdeaIkeyQueryRun.string(forColumn: "sharewithuserikeyreference")!
                }
                
            } catch {
                print("ERROR: \(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(StoryIdeaIkeyQuery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(StoryIdeaIkeyQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            /////////////////////////////
            
            var UpdateStoryIdeaShareSqlquery = String()
            
            do {
                UpdateStoryIdeaShareSqlquery = String(format: "UPDATE storyideasharingtable SET storyideashare = \"No\", storyideaedit = \"No\" WHERE storyideaikey = \"\(StoryIdeaIkey)\" AND ikey = \"\(StoryIdeaIkeyDB)\" AND sharewithuserikeyreference =  \"\(StoryIdeasharedwithuserikeyreferenceDB)\"")
                
                print("UpdateStoryIdeaShareSqlquery: \(UpdateStoryIdeaShareSqlquery)")
                
                try db.executeUpdate(UpdateStoryIdeaShareSqlquery, values: nil)
                
            } catch let error as NSError {
                
                print("ERROR: \(error)\n\n\(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(UpdateStoryIdeaShareSqlquery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(UpdateStoryIdeaShareSqlquery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            let indexset: IndexSet = [SelectedRow]
            StoryIdeasTableData()
            SharingStoryIdeasTable.selectRowIndexes(indexset, byExtendingSelection: false)
            
        }
        
    }
    @IBAction func StoryIdeaSharingYesEditButtonAction(_ sender: Any) {
        
        let SelectedRow = SharingStoryIdeasTable.selectedRow
        //print("SelectedRow: \(SelectedRow)")
        
        let isIndexValid = SharingStoryIdeasArray.indices.contains(SelectedRow)
        
        var StoryIdeaIkey = String()
        
        if isIndexValid {
            StoryIdeaIkey = SharingStoryIdeasArray[SelectedRow].Ikey
            //let StoryName: String = SharingStoriesArray[SelectedRow].StoryName
            //let StoryShare: String = SharingStoriesArray[SelectedRow].StoryShare
            //let StoryCO: String = SharingStoriesArray[SelectedRow].StoryCo
        } else {
            let alert = NSAlert()
            alert.messageText = "No StoryIdea Selected"
            alert.informativeText = "Please Select a StoryIdea to Share"
            alert.alertStyle = .warning
            alert.addButton(withTitle: "OK")
            
            
            if (CurrentAppearanceSharingViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.window.titlebarAppearsTransparent = true
            
            let returnCode = alert.runModal()
            print("returnCode: \(returnCode)")
            
            if (returnCode.rawValue == 1000) {
                print("OK Pressed")
                return
            }
            
        }
        
        
        let StoryIdeaCountQuery = String(format: "SELECT count(ikey) as cnt from storyideasharingtable WHERE userikeyreference = \(CurrentUserIkey!) AND sharewithuserikeyreference = (select ikey from users where user = \"\(UserDropDownSelection)\") AND storyideaikey = \"\(StoryIdeaIkey)\" ")
        
        print("StoryIdeaCountQuery:\n\(StoryIdeaCountQuery)")
        
        var StoryIdeaCount = Int32()
        
        do {
            let StoryIdeaCountQueryRun = try db.executeQuery(StoryIdeaCountQuery, values:nil)
            
            while StoryIdeaCountQueryRun.next() {
                StoryIdeaCount = StoryIdeaCountQueryRun.int(forColumn: "cnt")
            }
            
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(StoryIdeaCountQuery)\"")
            DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(StoryIdeaCountQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
            return
        }
        
        ////////////////////////////
        
        if (StoryIdeaCount == 0) {
            
            var UpdateCharacterShareSqlquery = String()
            
            // INSERT INTO storyideasharingtable (ikey,userikeyreference,sharewithuserikeyreference,storyideaikey,storyideashare) VALUES (%@,%ld,%@,%d,%@)",@"(select MAX(ikey)+ 1 from storyideasharingtable)",CurrentUserIkey,query2,StoryIdeaIkeyint,@"\"Yes\""
            
            
            
            do {
                UpdateCharacterShareSqlquery = String(format: "INSERT INTO storyideasharingtable (ikey,userikeyreference,sharewithuserikeyreference,storyideaikey,storyideashare,storyideaedit) VALUES ((select MAX(ikey)+ 1 from storyideasharingtable),\"\(CurrentUserIkey!)\",(select ikey from users where user = \"\(UserDropDownSelection)\"),\(StoryIdeaIkey),\"Yes\",\"Yes\")")
                
                print("UpdateCharacterShareSqlquery: \(UpdateCharacterShareSqlquery)")
                
                try db.executeUpdate(UpdateCharacterShareSqlquery, values: nil)
                
            } catch let error as NSError {
                
                print("ERROR: \(error)\n\n\(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(UpdateCharacterShareSqlquery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(UpdateCharacterShareSqlquery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            let indexset: IndexSet = [SelectedRow]
            StoryIdeasTableData()
            SharingStoryIdeasTable.selectRowIndexes(indexset, byExtendingSelection: false)
            
            
        } else if (StoryIdeaCount != 0) {
            
            let StoryIdeaIkeyQuery = String(format: "SELECT * from storyideasharingtable WHERE userikeyreference = \(CurrentUserIkey!) AND sharewithuserikeyreference = (select ikey from users where user = \"\(UserDropDownSelection)\") AND storyideaikey = \"\(StoryIdeaIkey)\" ")
            
            print("StoryIdeaIkeyQuery:\n\(StoryIdeaIkeyQuery)")
            
            var StoryIdeaIkeyDB = String()
            //var StoryIdeaStoryIkeyDB = String()
            var StoryIdeasharedwithuserikeyreferenceDB = String()
            
            do {
                let StoryIdeaIkeyQueryRun = try db.executeQuery(StoryIdeaIkeyQuery, values:nil)
                
                while StoryIdeaIkeyQueryRun.next() {
                    StoryIdeaIkeyDB = StoryIdeaIkeyQueryRun.string(forColumn: "ikey")!
                    //StoryIdeaStoryIkeyDB = StoryIdeaIkeyQueryRun.string(forColumn: "storyideaikey")!
                    StoryIdeasharedwithuserikeyreferenceDB = StoryIdeaIkeyQueryRun.string(forColumn: "sharewithuserikeyreference")!
                }
                
            } catch {
                print("ERROR: \(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(StoryIdeaIkeyQuery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(StoryIdeaIkeyQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            /////////////////////////////
            
            var UpdateStoryIdeaShareSqlquery = String()
            
            do {
                UpdateStoryIdeaShareSqlquery = String(format: "UPDATE storyideasharingtable SET storyideashare = \"Yes\", storyideaedit = \"Yes\" WHERE storyideaikey = \"\(StoryIdeaIkey)\" AND ikey = \"\(StoryIdeaIkeyDB)\" AND sharewithuserikeyreference =  \"\(StoryIdeasharedwithuserikeyreferenceDB)\"")
                
                print("UpdateStoryIdeaShareSqlquery: \(UpdateStoryIdeaShareSqlquery)")
                
                try db.executeUpdate(UpdateStoryIdeaShareSqlquery, values: nil)
                
            } catch let error as NSError {
                
                print("ERROR: \(error)\n\n\(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(UpdateStoryIdeaShareSqlquery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(UpdateStoryIdeaShareSqlquery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            let indexset: IndexSet = [SelectedRow]
            StoryIdeasTableData()
            SharingStoryIdeasTable.selectRowIndexes(indexset, byExtendingSelection: false)
            
        }
        
    }
    @IBAction func StoryIdeaSharingNoEditButtonAction(_ sender: Any) {
        
        
        let SelectedRow = SharingStoryIdeasTable.selectedRow
        //print("SelectedRow: \(SelectedRow)")
        
        let isIndexValid = SharingStoryIdeasArray.indices.contains(SelectedRow)
        
        var StoryIdeaIkey = String()
        
        if isIndexValid {
            StoryIdeaIkey = SharingStoryIdeasArray[SelectedRow].Ikey
            //let StoryName: String = SharingStoriesArray[SelectedRow].StoryName
            //let StoryShare: String = SharingStoriesArray[SelectedRow].StoryShare
            //let StoryCO: String = SharingStoriesArray[SelectedRow].StoryCo
        } else {
            let alert = NSAlert()
            alert.messageText = "No StoryIdea Selected"
            alert.informativeText = "Please Select a StoryIdea to Share"
            alert.alertStyle = .warning
            alert.addButton(withTitle: "OK")
            
            
            if (CurrentAppearanceSharingViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.window.titlebarAppearsTransparent = true
            
            let returnCode = alert.runModal()
            print("returnCode: \(returnCode)")
            
            if (returnCode.rawValue == 1000) {
                print("OK Pressed")
                return
            }
            
        }
        
        
        let StoryIdeaCountQuery = String(format: "SELECT count(ikey) as cnt from storyideasharingtable WHERE userikeyreference = \(CurrentUserIkey!) AND sharewithuserikeyreference = (select ikey from users where user = \"\(UserDropDownSelection)\") AND storyideaikey = \"\(StoryIdeaIkey)\" ")
        
        print("StoryIdeaCountQuery:\n\(StoryIdeaCountQuery)")
        
        var StoryIdeaCount = Int32()
        
        do {
            let StoryIdeaCountQueryRun = try db.executeQuery(StoryIdeaCountQuery, values:nil)
            
            while StoryIdeaCountQueryRun.next() {
                StoryIdeaCount = StoryIdeaCountQueryRun.int(forColumn: "cnt")
            }
            
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(StoryIdeaCountQuery)\"")
            DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(StoryIdeaCountQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
            return
        }
        
        ////////////////////////////
        
        if (StoryIdeaCount == 0) {
            
            var UpdateCharacterShareSqlquery = String()
            
            // INSERT INTO storyideasharingtable (ikey,userikeyreference,sharewithuserikeyreference,storyideaikey,storyideashare) VALUES (%@,%ld,%@,%d,%@)",@"(select MAX(ikey)+ 1 from storyideasharingtable)",CurrentUserIkey,query2,StoryIdeaIkeyint,@"\"Yes\""
            
            
            
            do {
                UpdateCharacterShareSqlquery = String(format: "INSERT INTO storyideasharingtable (ikey,userikeyreference,sharewithuserikeyreference,storyideaikey,storyideaedit) VALUES ((select MAX(ikey)+ 1 from storyideasharingtable),\"\(CurrentUserIkey!)\",(select ikey from users where user = \"\(UserDropDownSelection)\"),\(StoryIdeaIkey),\"No\")")
                
                print("UpdateCharacterShareSqlquery: \(UpdateCharacterShareSqlquery)")
                
                try db.executeUpdate(UpdateCharacterShareSqlquery, values: nil)
                
            } catch let error as NSError {
                
                print("ERROR: \(error)\n\n\(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(UpdateCharacterShareSqlquery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(UpdateCharacterShareSqlquery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            let indexset: IndexSet = [SelectedRow]
            StoryIdeasTableData()
            SharingStoryIdeasTable.selectRowIndexes(indexset, byExtendingSelection: false)
            
            
        } else if (StoryIdeaCount != 0) {
            
            let StoryIdeaIkeyQuery = String(format: "SELECT * from storyideasharingtable WHERE userikeyreference = \(CurrentUserIkey!) AND sharewithuserikeyreference = (select ikey from users where user = \"\(UserDropDownSelection)\") AND storyideaikey = \"\(StoryIdeaIkey)\" ")
            
            print("StoryIdeaIkeyQuery:\n\(StoryIdeaIkeyQuery)")
            
            var StoryIdeaIkeyDB = String()
            //var StoryIdeaStoryIkeyDB = String()
            var StoryIdeasharedwithuserikeyreferenceDB = String()
            
            do {
                let StoryIdeaIkeyQueryRun = try db.executeQuery(StoryIdeaIkeyQuery, values:nil)
                
                while StoryIdeaIkeyQueryRun.next() {
                    StoryIdeaIkeyDB = StoryIdeaIkeyQueryRun.string(forColumn: "ikey")!
                    //StoryIdeaStoryIkeyDB = StoryIdeaIkeyQueryRun.string(forColumn: "storyideaikey")!
                    StoryIdeasharedwithuserikeyreferenceDB = StoryIdeaIkeyQueryRun.string(forColumn: "sharewithuserikeyreference")!
                }
                
            } catch {
                print("ERROR: \(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(StoryIdeaIkeyQuery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(StoryIdeaIkeyQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            /////////////////////////////
            
            var UpdateStoryIdeaShareSqlquery = String()
            
            do {
                UpdateStoryIdeaShareSqlquery = String(format: "UPDATE storyideasharingtable SET storyideaedit = \"No\" WHERE storyideaikey = \"\(StoryIdeaIkey)\" AND ikey = \"\(StoryIdeaIkeyDB)\" AND sharewithuserikeyreference =  \"\(StoryIdeasharedwithuserikeyreferenceDB)\"")
                
                print("UpdateStoryIdeaShareSqlquery: \(UpdateStoryIdeaShareSqlquery)")
                
                try db.executeUpdate(UpdateStoryIdeaShareSqlquery, values: nil)
                
            } catch let error as NSError {
                
                print("ERROR: \(error)\n\n\(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(UpdateStoryIdeaShareSqlquery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(UpdateStoryIdeaShareSqlquery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            let indexset: IndexSet = [SelectedRow]
            StoryIdeasTableData()
            SharingStoryIdeasTable.selectRowIndexes(indexset, byExtendingSelection: false)
            
        }
        
        
        
        
    }
    
    // Locations Sharing Buttons
    @IBAction func LocationSharingYesShareButtonAction(_ sender: Any) {
        
        let SelectedRow = SharingLocationsTable.selectedRow
        //print("SelectedRow: \(SelectedRow)")
        
        let isIndexValid = SharingLocationsArray.indices.contains(SelectedRow)
        
        var CurrentLocationIkey = String()
        
        if isIndexValid {
            CurrentLocationIkey = SharingLocationsArray[SelectedRow].Ikey
            //let StoryName: String = SharingStoriesArray[SelectedRow].StoryName
            //let StoryShare: String = SharingStoriesArray[SelectedRow].StoryShare
            //let StoryCO: String = SharingStoriesArray[SelectedRow].StoryCo
        } else {
            let alert = NSAlert()
            alert.messageText = "No Location Selected"
            alert.informativeText = "Please Select a Location to Share"
            alert.alertStyle = .warning
            alert.addButton(withTitle: "OK")
            
            
            if (CurrentAppearanceSharingViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.window.titlebarAppearsTransparent = true
            
            let returnCode = alert.runModal()
            print("returnCode: \(returnCode)")
            
            if (returnCode.rawValue == 1000) {
                print("OK Pressed")
                return
            }
            
        }
        
        
        let LocationCountQuery = String(format: "SELECT count(ikey) as cnt from locationsharingtable WHERE userikeyreference = \(CurrentUserIkey!) AND sharewithuserikeyreference = (select ikey from users where user = \"\(UserDropDownSelection)\") AND locationikey = \"\(CurrentLocationIkey)\" ")
        
        print("LocationCountQuery:\n\(LocationCountQuery)")

        var LocationCount = Int32()
        
        do {
            let LocationCountQueryRun = try db.executeQuery(LocationCountQuery, values:nil)
            
            while LocationCountQueryRun.next() {
                LocationCount = LocationCountQueryRun.int(forColumn: "cnt")
            }
            
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(LocationCountQuery)\"")
            DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(LocationCountQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
            return
        }
        
        ////////////////////////////
        
        if (LocationCount == 0) {
            
            var UpdateLocationShareSqlquery = String()
            
            // INSERT INTO locationsharingtable (ikey,userikeyreference,sharewithuserikeyreference,locationikey,locationshare) VALUES (%@,%ld,%@,%d,%@)",@"(select MAX(ikey)+ 1 from locationsharingtable)",CurrentUserIkey,query2,LocationIkeyint,@"\"Yes\""
            
            
            
            do {
                UpdateLocationShareSqlquery = String(format: "INSERT INTO locationsharingtable (ikey,userikeyreference,sharewithuserikeyreference,locationikey,locationshare) VALUES ((select MAX(ikey)+ 1 from locationsharingtable),\"\(CurrentUserIkey!)\",(select ikey from users where user = \"\(UserDropDownSelection)\"),\(CurrentLocationIkey),\"Yes\")")
                
                print("UpdateLocationShareSqlquery: \(UpdateLocationShareSqlquery)")
                
                try db.executeUpdate(UpdateLocationShareSqlquery, values: nil)
                
            } catch let error as NSError {
                
                print("ERROR: \(error)\n\n\(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(UpdateLocationShareSqlquery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(UpdateLocationShareSqlquery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            let indexset: IndexSet = [SelectedRow]
            LocationsTableData()
            SharingLocationsTable.selectRowIndexes(indexset, byExtendingSelection: false)
            
            
        } else if (LocationCount != 0) {
            
            let LocationIkeyQuery = String(format: "SELECT * from locationsharingtable WHERE userikeyreference = \(CurrentUserIkey!) AND sharewithuserikeyreference = (select ikey from users where user = \"\(UserDropDownSelection)\") AND locationikey = \"\(CurrentLocationIkey)\" ")
            
            print("LocationIkeyQuery:\n\(LocationIkeyQuery)")
            
            var CurrentLocationIkey2 = String()
            
            do {
                let LocationIkeyQueryRun = try db.executeQuery(LocationIkeyQuery, values:nil)
                
                while LocationIkeyQueryRun.next() {
                    CurrentLocationIkey2 = LocationIkeyQueryRun.string(forColumn: "ikey")!
                }
                
            } catch {
                print("ERROR: \(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(LocationIkeyQuery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(LocationIkeyQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            /////////////////////////////
            
            var UpdateLocationShareSqlquery = String()
            
            do {
                UpdateLocationShareSqlquery = String(format: "UPDATE locationsharingtable SET locationshare = \"Yes\" WHERE ikey = \"\(CurrentLocationIkey2)\" ")
                
                print("UpdateStoryIdeaShareSqlquery: \(UpdateLocationShareSqlquery)")
                
                try db.executeUpdate(UpdateLocationShareSqlquery, values: nil)
                
            } catch let error as NSError {
                
                print("ERROR: \(error)\n\n\(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(UpdateLocationShareSqlquery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(UpdateLocationShareSqlquery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            let indexset: IndexSet = [SelectedRow]
            LocationsTableData()
            SharingLocationsTable.selectRowIndexes(indexset, byExtendingSelection: false)
            
        }
        
        
        
        
        
    }// end of LocationSharingYesShareButtonAction
    
    @IBAction func LocationSharingNoShareButtonAction(_ sender: Any) {
        
        
        let SelectedRow = SharingLocationsTable.selectedRow
        //print("SelectedRow: \(SelectedRow)")
        
        let isIndexValid = SharingLocationsArray.indices.contains(SelectedRow)
        
        var CurrentLocationIkey = String()
        
        if isIndexValid {
            CurrentLocationIkey = SharingLocationsArray[SelectedRow].Ikey
            //let StoryName: String = SharingStoriesArray[SelectedRow].StoryName
            //let StoryShare: String = SharingStoriesArray[SelectedRow].StoryShare
            //let StoryCO: String = SharingStoriesArray[SelectedRow].StoryCo
        } else {
            let alert = NSAlert()
            alert.messageText = "No Location Selected"
            alert.informativeText = "Please Select a Location to Share"
            alert.alertStyle = .warning
            alert.addButton(withTitle: "OK")
            
            
            if (CurrentAppearanceSharingViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.window.titlebarAppearsTransparent = true
            
            let returnCode = alert.runModal()
            print("returnCode: \(returnCode)")
            
            if (returnCode.rawValue == 1000) {
                print("OK Pressed")
                return
            }
            
        }
        
        
        let LocationCountQuery = String(format: "SELECT count(ikey) as cnt from locationsharingtable WHERE userikeyreference = \(CurrentUserIkey!) AND sharewithuserikeyreference = (select ikey from users where user = \"\(UserDropDownSelection)\") AND locationikey = \"\(CurrentLocationIkey)\" ")
        
        print("LocationCountQuery:\n\(LocationCountQuery)")
        
        var LocationCount = Int32()
        
        do {
            let LocationCountQueryRun = try db.executeQuery(LocationCountQuery, values:nil)
            
            while LocationCountQueryRun.next() {
                LocationCount = LocationCountQueryRun.int(forColumn: "cnt")
            }
            
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(LocationCountQuery)\"")
            DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(LocationCountQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
            return
        }
        
        ////////////////////////////
        
        if (LocationCount == 0) {
            
            var UpdateLocationShareSqlquery = String()
            
            // INSERT INTO locationsharingtable (ikey,userikeyreference,sharewithuserikeyreference,locationikey,locationshare) VALUES (%@,%ld,%@,%d,%@)",@"(select MAX(ikey)+ 1 from locationsharingtable)",CurrentUserIkey,query2,LocationIkeyint,@"\"Yes\""
            
            
            
            do {
                UpdateLocationShareSqlquery = String(format: "INSERT INTO locationsharingtable (ikey,userikeyreference,sharewithuserikeyreference,locationikey,locationshare,locationedit) VALUES ((select MAX(ikey)+ 1 from locationsharingtable),\"\(CurrentUserIkey!)\",(select ikey from users where user = \"\(UserDropDownSelection)\"),\(CurrentLocationIkey),\"No\",\"No\")")
                
                print("UpdateLocationShareSqlquery: \(UpdateLocationShareSqlquery)")
                
                try db.executeUpdate(UpdateLocationShareSqlquery, values: nil)
                
            } catch let error as NSError {
                
                print("ERROR: \(error)\n\n\(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(UpdateLocationShareSqlquery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(UpdateLocationShareSqlquery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            let indexset: IndexSet = [SelectedRow]
            LocationsTableData()
            SharingLocationsTable.selectRowIndexes(indexset, byExtendingSelection: false)
            
            
        } else if (LocationCount != 0) {
            
            let LocationIkeyQuery = String(format: "SELECT * from locationsharingtable WHERE userikeyreference = \(CurrentUserIkey!) AND sharewithuserikeyreference = (select ikey from users where user = \"\(UserDropDownSelection)\") AND locationikey = \"\(CurrentLocationIkey)\" ")
            
            print("LocationIkeyQuery:\n\(LocationIkeyQuery)")
            
            var CurrentLocationIkey2 = String()
            
            do {
                let LocationIkeyQueryRun = try db.executeQuery(LocationIkeyQuery, values:nil)
                
                while LocationIkeyQueryRun.next() {
                    CurrentLocationIkey2 = LocationIkeyQueryRun.string(forColumn: "ikey")!
                }
                
            } catch {
                print("ERROR: \(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(LocationIkeyQuery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(LocationIkeyQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            /////////////////////////////
            
            var UpdateLocationShareSqlquery = String()
            
            do {
                UpdateLocationShareSqlquery = String(format: "UPDATE locationsharingtable SET locationshare = \"No\",locationedit = \"No\" WHERE ikey = \"\(CurrentLocationIkey2)\" ")
                
                print("UpdateStoryIdeaShareSqlquery: \(UpdateLocationShareSqlquery)")
                
                try db.executeUpdate(UpdateLocationShareSqlquery, values: nil)
                
            } catch let error as NSError {
                
                print("ERROR: \(error)\n\n\(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(UpdateLocationShareSqlquery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(UpdateLocationShareSqlquery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            let indexset: IndexSet = [SelectedRow]
            LocationsTableData()
            SharingLocationsTable.selectRowIndexes(indexset, byExtendingSelection: false)
            
        }
        
        
    }
    @IBAction func LocationSharingYesEditButtonAction(_ sender: Any) {
        
        let SelectedRow = SharingLocationsTable.selectedRow
        print("LocationSharingYesEditButtonAction SelectedRow: \(SelectedRow)")
        
        let isIndexValid = SharingLocationsArray.indices.contains(SelectedRow)
        
        var CurrentLocationIkey = String()
        
        if isIndexValid {
            CurrentLocationIkey = SharingLocationsArray[SelectedRow].Ikey
            //let StoryName: String = SharingStoriesArray[SelectedRow].StoryName
            //let StoryShare: String = SharingStoriesArray[SelectedRow].StoryShare
            //let StoryCO: String = SharingStoriesArray[SelectedRow].StoryCo
        } else {
            let alert = NSAlert()
            alert.messageText = "No Location Selected"
            alert.informativeText = "Please Select a Location to Share"
            alert.alertStyle = .warning
            alert.addButton(withTitle: "OK")
            
            
            if (CurrentAppearanceSharingViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.window.titlebarAppearsTransparent = true
            
            let returnCode = alert.runModal()
            print("returnCode: \(returnCode)")
            
            if (returnCode.rawValue == 1000) {
                print("OK Pressed")
                return
            }
            
        }
        
        
        let LocationCountQuery = String(format: "SELECT count(ikey) as cnt from locationsharingtable WHERE userikeyreference = \(CurrentUserIkey!) AND sharewithuserikeyreference = (select ikey from users where user = \"\(UserDropDownSelection)\") AND locationikey = \"\(CurrentLocationIkey)\" ")
        
        print("LocationCountQuery:\n\(LocationCountQuery)")
        
        var LocationCount = Int32()
        
        do {
            let LocationCountQueryRun = try db.executeQuery(LocationCountQuery, values:nil)
            
            while LocationCountQueryRun.next() {
                LocationCount = LocationCountQueryRun.int(forColumn: "cnt")
            }
            
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(LocationCountQuery)\"")
            DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(LocationCountQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
            return
        }
        
        ////////////////////////////
        
        if (LocationCount == 0) {
            
            var UpdateLocationShareSqlquery = String()
            
  
            do {
                UpdateLocationShareSqlquery = String(format: "INSERT INTO locationsharingtable (ikey,userikeyreference,sharewithuserikeyreference,locationikey,locationshare,locationedit) VALUES ((select MAX(ikey)+ 1 from locationsharingtable),\"\(CurrentUserIkey!)\",(select ikey from users where user = \"\(UserDropDownSelection)\"),\(CurrentLocationIkey),\"Yes\",\"Yes\")")
                
                print("UpdateLocationShareSqlquery: \(UpdateLocationShareSqlquery)")
                
                try db.executeUpdate(UpdateLocationShareSqlquery, values: nil)
                
            } catch let error as NSError {
                
                print("ERROR: \(error)\n\n\(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(UpdateLocationShareSqlquery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(UpdateLocationShareSqlquery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            let indexset: IndexSet = [SelectedRow]
            LocationsTableData()
            SharingStoryIdeasTable.selectRowIndexes(indexset, byExtendingSelection: false)
            
            
        } else if (LocationCount != 0) {
            
            let LocationIkeyQuery = String(format: "SELECT * from locationsharingtable WHERE userikeyreference = \(CurrentUserIkey!) AND sharewithuserikeyreference = (select ikey from users where user = \"\(UserDropDownSelection)\") AND locationikey = \"\(CurrentLocationIkey)\" ")
            
            print("LocationIkeyQuery:\n\(LocationIkeyQuery)")
            
            var CurrentLocationIkey2 = String()
            
            do {
                let LocationIkeyQueryRun = try db.executeQuery(LocationIkeyQuery, values:nil)
                
                while LocationIkeyQueryRun.next() {
                    CurrentLocationIkey2 = LocationIkeyQueryRun.string(forColumn: "ikey")!
                }
                
            } catch {
                print("ERROR: \(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(LocationIkeyQuery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(LocationIkeyQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            /////////////////////////////
            
            var UpdateLocationShareSqlquery = String()
            
            do {
                UpdateLocationShareSqlquery = String(format: "UPDATE locationsharingtable SET locationshare = \"Yes\",locationedit = \"Yes\" WHERE ikey = \"\(CurrentLocationIkey2)\" ")
                
                print("UpdateStoryIdeaShareSqlquery: \(UpdateLocationShareSqlquery)")
                
                try db.executeUpdate(UpdateLocationShareSqlquery, values: nil)
                
            } catch let error as NSError {
                
                print("ERROR: \(error)\n\n\(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(UpdateLocationShareSqlquery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(UpdateLocationShareSqlquery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            let indexset: IndexSet = [SelectedRow]
            LocationsTableData()
            SharingLocationsTable.selectRowIndexes(indexset, byExtendingSelection: false)
            
        }
        
        
        
    }
    @IBAction func LocationSharingNoEditButtonAction(_ sender: Any) {
        
        
        let SelectedRow = SharingLocationsTable.selectedRow
        print("LocationSharingNoEditButtonAction SelectedRow: \(SelectedRow)")
        
        let isIndexValid = SharingLocationsArray.indices.contains(SelectedRow)
        
        var CurrentLocationIkey = String()
        
        if isIndexValid {
            CurrentLocationIkey = SharingLocationsArray[SelectedRow].Ikey
            //let StoryName: String = SharingStoriesArray[SelectedRow].StoryName
            //let StoryShare: String = SharingStoriesArray[SelectedRow].StoryShare
            //let StoryCO: String = SharingStoriesArray[SelectedRow].StoryCo
        } else {
            let alert = NSAlert()
            alert.messageText = "No Location Selected"
            alert.informativeText = "Please Select a Location to Share"
            alert.alertStyle = .warning
            alert.addButton(withTitle: "OK")
            
            
            if (CurrentAppearanceSharingViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.window.titlebarAppearsTransparent = true
            
            let returnCode = alert.runModal()
            print("returnCode: \(returnCode)")
            
            if (returnCode.rawValue == 1000) {
                print("OK Pressed")
                return
            }
            
        }
        
        
        let LocationCountQuery = String(format: "SELECT count(ikey) as cnt from locationsharingtable WHERE userikeyreference = \(CurrentUserIkey!) AND sharewithuserikeyreference = (select ikey from users where user = \"\(UserDropDownSelection)\") AND locationikey = \"\(CurrentLocationIkey)\" ")
        
        print("LocationCountQuery:\n\(LocationCountQuery)")
        
        var LocationCount = Int32()
        
        do {
            let LocationCountQueryRun = try db.executeQuery(LocationCountQuery, values:nil)
            
            while LocationCountQueryRun.next() {
                LocationCount = LocationCountQueryRun.int(forColumn: "cnt")
            }
            
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(LocationCountQuery)\"")
            DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(LocationCountQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
            return
        }
        
        ////////////////////////////
        
        if (LocationCount == 0) {
            
            var UpdateLocationShareSqlquery = String()
            
            // INSERT INTO locationsharingtable (ikey,userikeyreference,sharewithuserikeyreference,locationikey,locationshare) VALUES (%@,%ld,%@,%d,%@)",@"(select MAX(ikey)+ 1 from locationsharingtable)",CurrentUserIkey,query2,LocationIkeyint,@"\"Yes\""
            
            
            
            do {
                UpdateLocationShareSqlquery = String(format: "INSERT INTO locationsharingtable (ikey,userikeyreference,sharewithuserikeyreference,locationikey,locationshare) VALUES ((select MAX(ikey)+ 1 from locationsharingtable),\"\(CurrentUserIkey!)\",(select ikey from users where user = \"\(UserDropDownSelection)\"),\(CurrentLocationIkey),\"No\")")
                
                print("UpdateLocationShareSqlquery: \(UpdateLocationShareSqlquery)")
                
                try db.executeUpdate(UpdateLocationShareSqlquery, values: nil)
                
            } catch let error as NSError {
                
                print("ERROR: \(error)\n\n\(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(UpdateLocationShareSqlquery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(UpdateLocationShareSqlquery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            let indexset: IndexSet = [SelectedRow]
            LocationsTableData()
            SharingStoryIdeasTable.selectRowIndexes(indexset, byExtendingSelection: false)
            
            
        } else if (LocationCount != 0) {
            
            let LocationIkeyQuery = String(format: "SELECT * from locationsharingtable WHERE userikeyreference = \(CurrentUserIkey!) AND sharewithuserikeyreference = (select ikey from users where user = \"\(UserDropDownSelection)\") AND locationikey = \"\(CurrentLocationIkey)\" ")
            
            print("LocationIkeyQuery:\n\(LocationIkeyQuery)")
            
            var CurrentLocationIkey2 = String()
            
            do {
                let LocationIkeyQueryRun = try db.executeQuery(LocationIkeyQuery, values:nil)
                
                while LocationIkeyQueryRun.next() {
                    CurrentLocationIkey2 = LocationIkeyQueryRun.string(forColumn: "ikey")!
                }
                
            } catch {
                print("ERROR: \(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(LocationIkeyQuery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(LocationIkeyQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            /////////////////////////////
            
            var UpdateLocationShareSqlquery = String()
            
            do {
                UpdateLocationShareSqlquery = String(format: "UPDATE locationsharingtable SET locationedit = \"No\" WHERE ikey = \"\(CurrentLocationIkey2)\" ")
                
                print("UpdateStoryIdeaShareSqlquery: \(UpdateLocationShareSqlquery)")
                
                try db.executeUpdate(UpdateLocationShareSqlquery, values: nil)
                
            } catch let error as NSError {
                
                print("ERROR: \(error)\n\n\(db.lastErrorMessage())")
                loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(UpdateLocationShareSqlquery)\"")
                DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(UpdateLocationShareSqlquery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
            
            let indexset: IndexSet = [SelectedRow]
            LocationsTableData()
            SharingLocationsTable.selectRowIndexes(indexset, byExtendingSelection: false)
            
        }
        

        
        
    }// end of location no edit
    
    
    
    
    func EmptyTables() {
        
        SharingStoriesArray.removeAll()
        
        SharingCharactersArray.removeAll()
        
        SharingStoryIdeasArray.removeAll()
        
        SharingLocationsArray.removeAll()
        
        
        SharingStoryTable.reloadData()
        
        SharingCharactersTable.reloadData()
        
        SharingStoryIdeasTable.reloadData()
        
        SharingLocationsTable.reloadData()
        
    }
    
    
    
    
    
    /////////////////////////////////
    
    
    @IBAction func SharingCloseButtonAction(_ sender: Any) {
        
        EmptyTables()
        
        
        self.view.window?.close()
        
    }
    
    
    
}// end of SharingViewController




