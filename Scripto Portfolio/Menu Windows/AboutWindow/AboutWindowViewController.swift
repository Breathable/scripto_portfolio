//
//  AboutWindowViewController.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 2/14/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Cocoa

class AboutWindowViewController: NSViewController {
    
    @IBOutlet weak var AboutContactSupportButton: NSButton!
    @IBOutlet weak var AboutCloseButton: NSButton!
    
    @IBOutlet weak var AboutScriptoPortfolioLabel: NSTextField!
    @IBOutlet weak var AboutVersionLabel: NSTextField!
    @IBOutlet weak var AboutEngineLabel: NSTextField!
    @IBOutlet weak var AboutFMDBLabel: NSTextField!
    @IBOutlet weak var AboutFMDBVersionLabel: NSTextField!
    @IBOutlet weak var AboutQuestionIssueSuggestionLabel: NSTextField!
    
    @IBOutlet weak var AboutFMDBLinkButton: NSButton!
    

    private var CurrentAppearanceHeAboutViewController = String()
    
    @IBOutlet weak var privacyPolicyButton: NSButton!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        

        
        AboutVersionLabel.stringValue = "\(AppVersion)"
    }
    
    override func viewWillAppear() {
        
        
        
        
    }
    
    
    override func viewDidLayout() {
        
        
        if let FMDBVersion = Bundle(identifier: "org.cocoapods.FMDB")?.infoDictionary?["CFBundleShortVersionString"] as? String {
            //print("\(FMDBVersion)")
            
            AboutFMDBVersionLabel.stringValue = FMDBVersion
            
        }
        
        
        appearanceCheck()
        
        super.viewDidLayout()
        
        print("NewUserViewController - viewDidLayout finished")
        
        
    }// end of viewDidLayout()
    
    
    func appearanceCheck() {
        
        enum InterfaceStyle : String {
            case Dark, Light
            
            init() {
                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
                self = InterfaceStyle(rawValue: type)!
            }
        }
        
        let currentStyle = InterfaceStyle()
        
        if ("\(currentStyle)" != CurrentAppearanceHeAboutViewController) {
            
            if (currentStyle.rawValue == "Dark") {
                print("Dark Style Detected - NewUserViewController")
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColorDarkMode
                
                AboutContactSupportButton.styleButtonText(button: AboutContactSupportButton, buttonName: "Contact Support", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                AboutContactSupportButton.wantsLayer = true
                AboutContactSupportButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                AboutContactSupportButton.layer?.cornerRadius = 7
                
                AboutCloseButton.styleButtonText(button: AboutCloseButton, buttonName: "Close", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                AboutCloseButton.wantsLayer = true
                AboutCloseButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                AboutCloseButton.layer?.cornerRadius = 7
                
                AboutFMDBLinkButton.styleButtonText(button: AboutFMDBLinkButton, buttonName: "https://github.com/ccgus/fmdb", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                AboutFMDBLinkButton.wantsLayer = true
                AboutFMDBLinkButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                AboutFMDBLinkButton.layer?.cornerRadius = 7
                
                AboutScriptoPortfolioLabel.font = NSFont(name:AppFontBold, size: 13.0)
                AboutScriptoPortfolioLabel.textColor = NSColor.white
                
                AboutVersionLabel.font = NSFont(name:AppFontBold, size: 13.0)
                AboutVersionLabel.textColor = NSColor.white
                
                AboutEngineLabel.font = NSFont(name:AppFontBold, size: 13.0)
                AboutEngineLabel.textColor = NSColor.white
                
                AboutFMDBLabel.font = NSFont(name:AppFontBold, size: 13.0)
                AboutFMDBLabel.textColor = NSColor.white
                
                AboutFMDBVersionLabel.font = NSFont(name:AppFontBold, size: 13.0)
                AboutFMDBVersionLabel.textColor = NSColor.white
                
                AboutQuestionIssueSuggestionLabel.font = NSFont(name:AppFontBold, size: 13.0)
                AboutQuestionIssueSuggestionLabel.textColor = NSColor.white
                
                
                
            } else if (currentStyle.rawValue == "Light") {
                print("Light Style Detected - NewUserViewController")
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColor.cgColor
                
                AboutContactSupportButton.styleButtonText(button: AboutContactSupportButton, buttonName: "Contact Support", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                AboutContactSupportButton.wantsLayer = true
                AboutContactSupportButton.layer?.backgroundColor = NSColor.gray.cgColor
                AboutContactSupportButton.layer?.cornerRadius = 7
                
                AboutCloseButton.styleButtonText(button: AboutCloseButton, buttonName: "Close", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                AboutCloseButton.wantsLayer = true
                AboutCloseButton.layer?.backgroundColor = NSColor.gray.cgColor
                AboutCloseButton.layer?.cornerRadius = 7
                
                AboutFMDBLinkButton.styleButtonText(button: AboutFMDBLinkButton, buttonName: "https://github.com/ccgus/fmdb", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                AboutFMDBLinkButton.wantsLayer = true
                AboutFMDBLinkButton.layer?.backgroundColor = NSColor.gray.cgColor
                AboutFMDBLinkButton.layer?.cornerRadius = 7

                AboutScriptoPortfolioLabel.font = NSFont(name:AppFontBold, size: 13.0)
                AboutScriptoPortfolioLabel.textColor = NSColor.white
                
                AboutVersionLabel.font = NSFont(name:AppFontBold, size: 13.0)
                AboutVersionLabel.textColor = NSColor.white
                
                AboutEngineLabel.font = NSFont(name:AppFontBold, size: 13.0)
                AboutEngineLabel.textColor = NSColor.white
                
                AboutFMDBLabel.font = NSFont(name:AppFontBold, size: 13.0)
                AboutFMDBLabel.textColor = NSColor.white
                
                AboutFMDBVersionLabel.font = NSFont(name:AppFontBold, size: 13.0)
                AboutFMDBVersionLabel.textColor = NSColor.white
                
                AboutQuestionIssueSuggestionLabel.font = NSFont(name:AppFontBold, size: 13.0)
                AboutQuestionIssueSuggestionLabel.textColor = NSColor.white
                
            }
        }
        
        CurrentAppearanceHeAboutViewController = "\(currentStyle)"
        
        
    }//end of appearanceCheck()
    
    
    
    
    
    @IBAction func AboutContactSupportButtonAction(_ sender: Any) {
        
        NSWorkspace.shared.open(NSURL(string: "http://www.dangthatsgoodmedia.biz/support/")! as URL)
        
    }
    @IBAction func AboutFMDBWebPageButton(_ sender: Any) {
        
        NSWorkspace.shared.open(NSURL(string: "https://github.com/ccgus/fmdb")! as URL)
        
    }
    
    @IBAction func AboutCloseButtonAction(_ sender: Any) {
        
        self.view.window?.close()
        
    }
    
    
    
    
    @IBAction func privacyPolicyButtonAction(_ sender: Any) {
        
        NSWorkspace.shared.open(NSURL(string: "http://www.dangthatsgoodmedia.biz/Privacy-Policy.pdf")! as URL)
        
    }
    
    
    
    
    
    
    
    
}
