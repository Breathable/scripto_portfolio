//
//  ThemesViewController.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 2/14/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Cocoa

class ThemesViewController: NSViewController {

    
    @IBOutlet weak var ThemesOriginalBlueButton: NSButton!
    @IBOutlet weak var ThemesOriginalGreyButton: NSButton!
    @IBOutlet weak var ThemesDarkGreyButton: NSButton!
    @IBOutlet weak var ThemesCustomColorButton: NSButton!
    
    @IBOutlet weak var OriginalBlueColorWell: NSColorWell!
    @IBOutlet weak var CustomColorColorWell: NSColorWell!
    @IBOutlet weak var OriginalGreyColorWell: NSColorWell!
    @IBOutlet weak var DarkGreyColorWell: NSColorWell!
    

    var CurrentAppearanceThemesViewController = String()
    
    
    var currentStyle = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        
        
        
    }
    
    override func viewWillAppear() {
        
    }
    
    
    override func viewDidLayout() {
        
        //        enum InterfaceStyle : String {
        //            case Dark, Light
        //
        //            init() {
        //                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
        //                self = InterfaceStyle(rawValue: type)!
        //            }
        //        }
        //
        //        let currentStyle = InterfaceStyle()
        //
        //        //print("currentStyle: \"\(currentStyle)\"  -  CurrentAppearance: \"\(CurrentAppearance)\" ")
        //
        //        if ("\(currentStyle)" != CurrentAppearanceNewUserViewController) {
        //            appearanceCheck()
        //        }
        
        appearanceCheck()
        
        super.viewDidLayout()
        
        
        
        
    }// end of viewDidLayout()
    
    
    func appearanceCheck() {
        
        enum InterfaceStyle : String {
            case Dark, Light
            
            init() {
                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
                self = InterfaceStyle(rawValue: type)!
            }
        }
        
        currentStyle = InterfaceStyle().rawValue
        
        if ("\(currentStyle)" != CurrentAppearanceThemesViewController) {
            
            if (currentStyle == "Dark") {
            
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColorDarkMode
                
            } else if (currentStyle == "Light") {
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColor.cgColor
                
            }
        }
        
        CurrentAppearanceThemesViewController = "\(currentStyle)"
        
        
    }//end of appearanceCheck()
    
    
    
    

    
    
    
    
    
    override func viewDidAppear() {
        
        
        if (currentStyle == "Dark") {
            
            let alert = NSAlert()
            alert.messageText = "Darkmode has been enabled"
            alert.informativeText = "With Darkmode enabled you will not be able to change the Theme colors at this time.\n\nSorry for the inconvenience."
            alert.alertStyle = .warning
            alert.addButton(withTitle: "OK")
            
            if (CurrentAppearanceThemesViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.window.titlebarAppearsTransparent = true
            
           alert.runModal()
            
        }
        
        
    }
    
    
    func UpdateUserPrefBackgroundColor() {
        
        
        let ColorWellColor:NSColor = WindowBackgroundColor
        
        let colorspace = ColorWellColor.colorSpaceName
        print("colorspace: \"\(colorspace)\" ")
        
        let Hex = getHexFromColor(color: ColorWellColor)

        //let Hex = getHexFromColor(color: WindowBackgroundColor)
        
        print("Hex: \"\(Hex)\" ")
        
        UP.UpdateUserBackgroundColor(ColorString: "\(Hex)")
        
        
    }
    
    
    
    
    @IBAction func ThemesOriginalBlueButtonAction(_ sender: Any) {
        
        ThemesOriginalBlueButton.state = .off
        ThemesDarkGreyButton.state = .off
        ThemesCustomColorButton.state = .off
        CustomColorColorWell.isEnabled = false
        
        
        let alert = NSAlert()
        alert.messageText = "Changing the background color will close all windows so they can be opened under the new color."
        alert.informativeText = "Are you sure you want to do this?"
        alert.alertStyle = .warning
        alert.addButton(withTitle: "Cancel")
        alert.addButton(withTitle: "OK")
        
        if (CurrentAppearanceThemesViewController == "Dark") {
            alert.window.backgroundColor = AlertBackgroundColorDarkMode
        } else {
            alert.window.backgroundColor = WindowBackgroundColor
        }
        
        alert.window.titlebarAppearsTransparent = true
        
        let returnCode = alert.runModal()
        print("returnCode: \(returnCode)")
        
        if (returnCode.rawValue == 1001) {
            
            WindowBackgroundColor = OriginalBlueColorWell.color
            
            UpdateUserPrefBackgroundColor()
            
            SetWindowColor()
            
        }
        
        if (returnCode.rawValue == 1000) {
            return
        }
        
        
    }
    
    
    
    
    
    
    
    
    
    @IBAction func ThemesOriginalGreyButtonAction(_ sender: Any) {
        ThemesDarkGreyButton.state = .off
        ThemesCustomColorButton.state = .off
        CustomColorColorWell.isEnabled = false
        
        let alert = NSAlert()
        alert.messageText = "Changing the background color will close all windows so they can be opened under the new color."
        alert.informativeText = "Are you sure you want to do this?"
        alert.alertStyle = .warning
        alert.addButton(withTitle: "Cancel")
        alert.addButton(withTitle: "OK")
        
        if (CurrentAppearanceThemesViewController == "Dark") {
            alert.window.backgroundColor = AlertBackgroundColorDarkMode
        } else {
            alert.window.backgroundColor = WindowBackgroundColor
        }
        
        alert.window.titlebarAppearsTransparent = true
        
        let returnCode = alert.runModal()
        print("returnCode: \(returnCode)")
        
        if (returnCode.rawValue == 1001) {
            
            WindowBackgroundColor = OriginalGreyColorWell.color
            
            UpdateUserPrefBackgroundColor()
            
            SetWindowColor()
            
        }
        
        if (returnCode.rawValue == 1000) {
            return
        }
        
        
        
        
    }
    
    @IBAction func ThemesDarkGreyButtonAction(_ sender: Any) {
        ThemesOriginalGreyButton.state = .off
        ThemesCustomColorButton.state = .off
        CustomColorColorWell.isEnabled = false
        
        
        let alert = NSAlert()
        alert.messageText = "Changing the background color will close all windows so they can be opened under the new color."
        alert.informativeText = "Are you sure you want to do this?"
        alert.alertStyle = .warning
        alert.addButton(withTitle: "Cancel")
        alert.addButton(withTitle: "OK")
        
        if (CurrentAppearanceThemesViewController == "Dark") {
            alert.window.backgroundColor = AlertBackgroundColorDarkMode
        } else {
            alert.window.backgroundColor = WindowBackgroundColor
        }
        
        alert.window.titlebarAppearsTransparent = true
        
        let returnCode = alert.runModal()
        print("returnCode: \(returnCode)")
        
        if (returnCode.rawValue == 1001) {
            
            WindowBackgroundColor = DarkGreyColorWell.color
            
            UpdateUserPrefBackgroundColor()
            
            SetWindowColor()
            
        }
        
        if (returnCode.rawValue == 1000) {
            return
        }
        
        
    }
    
    @IBAction func CustomColorButtonAction(_ sender: Any) {
        ThemesDarkGreyButton.state = .off
        ThemesOriginalGreyButton.state = .off
        CustomColorColorWell.isEnabled = true
    }
    
    
    
    @IBAction func CustomColorColorWellAction(_ sender: Any) {
        print("CustomColorColorWellAction RAN")
        
        let alert = NSAlert()
        alert.messageText = "Changing the background color will close all windows so they can be opened under the new color."
        alert.informativeText = "Are you sure you want to do this?"
        alert.alertStyle = .warning
        alert.addButton(withTitle: "Cancel")
        alert.addButton(withTitle: "OK")
        
        if (CurrentAppearanceThemesViewController == "Dark") {
            alert.window.backgroundColor = AlertBackgroundColorDarkMode
        } else {
            alert.window.backgroundColor = WindowBackgroundColor
        }
        
        alert.window.titlebarAppearsTransparent = true
        
        let returnCode = alert.runModal()
        print("returnCode: \(returnCode)")
        
        if (returnCode.rawValue == 1001) {
            
            WindowBackgroundColor = CustomColorColorWell.color
            
            UpdateUserPrefBackgroundColor()
            
            SetWindowColor()
  
        }
        
         if (returnCode.rawValue == 1000) {
            return
        }

    }
    
    func SetWindowColor() {
        print("SetWindowColor RAN")

        OpenWindows.CloseAllWindows()
        StoryListWindow.showWindow(self)
        AppThemesWindow.showWindow(self)

    }
    
    

    
    
    
    
    
    func getHexFromColor(color: NSColor) -> NSString {
        
        
        
        
        // Get the red, green, and blue components of the color
//        var r: CGFloat = 0
//        var g: CGFloat = 0
//        var b: CGFloat = 0
//        var a: CGFloat = 0
        
        var rInt, gInt, bInt: Int
        var rHex, gHex, bHex: NSString
        
        var hexColor: NSString
        
        
        //let colorSpace = color.colorSpace
        
        //print("colorSpace: \"\(colorSpace)\" ")
        
        
        let ciColor:CIColor = CIColor(color: color)!
//        print(ciColor.red * 255.99999)//1.0
//        print(ciColor.green * 255.99999)//0.0
//        print(ciColor.blue * 255.99999)//0.0
//        print(ciColor.alpha * 255.99999)//1.0 /*or use nsColor.alphaComponent*/
        
        
       
        
        //color.getRed(&r, green: &g, blue: &b, alpha: &a)
        
        print("R: \(ciColor.red) G: \(ciColor.green) B:\(ciColor.blue) A:\(ciColor.alpha)")
        
        // Convert the components to numbers (unsigned decimal integer) between 0 and 255
        rInt = Int((ciColor.red * 255.99999))
        gInt = Int((ciColor.green * 255.99999))
        bInt = Int((ciColor.blue * 255.99999))
        
//        rInt = Int((r * 255.99999))
//        gInt = Int((g * 255.99999))
//        bInt = Int((b * 255.99999))
        
        
        // Convert the numbers to hex strings
        rHex = NSString(format:"%2X", rInt)
        gHex = NSString(format:"%2X", gInt)
        bHex = NSString(format:"%2X", bInt)
        
        if (rHex == " 0") {
            rHex = "00"
        }
        if (gHex == " 0") {
            gHex = "00"
        }
        if (bHex == " 0") {
            bHex = "00"
        }
        
        
        hexColor = "\(rHex)\(gHex)\(bHex)" as NSString
        //print("HEX: \"\(rHex)\" \"\(gHex)\" \"\(bHex)\"")
        
        return hexColor
    }
    
    
    
    
    
    
    
    
    
    
    
}// end of ThemesViewController






