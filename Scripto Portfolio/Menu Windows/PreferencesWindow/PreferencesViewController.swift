//
//  PreferencesViewController.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 2/14/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Cocoa
import SwiftLoggly

class PreferencesViewController: NSViewController, NSTableViewDelegate, NSTableViewDataSource {

    @IBOutlet weak var PreferencesBackupTable: NSTableView!
    @IBOutlet weak var PreferencesDeleteButton: NSButton!
    @IBOutlet weak var PreferencesBackupButton: NSButton!
    @IBOutlet weak var PreferencesCloseButton: NSButton!
    @IBOutlet weak var PreferencesBackupsLabel: NSTextField!
    @IBOutlet weak var PreferencesBackupCountLabel: NSTextField!
    
   
    @IBOutlet weak var sendCrashLogsRadioButton: NSButton!
    @IBOutlet weak var sendNOCrashLogsRadioButton: NSButton!
    
    @IBOutlet weak var sendUpdatedLable: NSTextField!
    
    
    
    
    
    
    var BackupFolderLcoation = String()
    
    
    var BackupDatesListArray: [BackupDatesList] = []
    
    
    var CurrentAppearancePreferencesViewController = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        
        PreferencesBackupTable.dataSource = self
        PreferencesBackupTable.delegate = self
        
    }
    
    override func viewWillAppear() {
        
        
        BackupTableData()
    }
    
    
    override func viewDidLayout() {
        
        //        enum InterfaceStyle : String {
        //            case Dark, Light
        //
        //            init() {
        //                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
        //                self = InterfaceStyle(rawValue: type)!
        //            }
        //        }
        //
        //        let currentStyle = InterfaceStyle()
        //
        //        //print("currentStyle: \"\(currentStyle)\"  -  CurrentAppearance: \"\(CurrentAppearance)\" ")
        //
        //        if ("\(currentStyle)" != CurrentAppearanceNewUserViewController) {
        //            appearanceCheck()
        //        }
        
        appearanceCheck()
        
        super.viewDidLayout()
        
        print("NewUserViewController - viewDidLayout finished")
        
        
    }// end of viewDidLayout()
    
    
    func appearanceCheck() {
        
        enum InterfaceStyle : String {
            case Dark, Light
            
            init() {
                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
                self = InterfaceStyle(rawValue: type)!
            }
        }
        
        let currentStyle = InterfaceStyle()
        
        if ("\(currentStyle)" != CurrentAppearancePreferencesViewController) {
            
            if (currentStyle.rawValue == "Dark") {
                print("Dark Style Detected - NewUserViewController")
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColorDarkMode
                
                
                PreferencesDeleteButton.styleButtonText(button: PreferencesDeleteButton, buttonName: "Delete", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                PreferencesDeleteButton.wantsLayer = true
                PreferencesDeleteButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                PreferencesDeleteButton.layer?.cornerRadius = 7
                
                PreferencesBackupButton.styleButtonText(button: PreferencesBackupButton, buttonName: "Backup", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                PreferencesBackupButton.wantsLayer = true
                PreferencesBackupButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                PreferencesBackupButton.layer?.cornerRadius = 7
                
                PreferencesCloseButton.styleButtonText(button: PreferencesCloseButton, buttonName: "Close", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                PreferencesCloseButton.wantsLayer = true
                PreferencesCloseButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                PreferencesCloseButton.layer?.cornerRadius = 7
                
                PreferencesBackupsLabel.font = NSFont(name:AppFontBold, size: 13.0)
                PreferencesBackupsLabel.textColor = NSColor.lightGray
                
                PreferencesBackupCountLabel.font = NSFont(name:AppFontBold, size: 13.0)
                PreferencesBackupCountLabel.textColor = NSColor.lightGray
                
                
                
            } else if (currentStyle.rawValue == "Light") {
                print("Light Style Detected - NewUserViewController")
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColor.cgColor
                
                
                PreferencesDeleteButton.styleButtonText(button: PreferencesDeleteButton, buttonName: "Delete", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                PreferencesDeleteButton.wantsLayer = true
                PreferencesDeleteButton.layer?.backgroundColor = NSColor.gray.cgColor
                PreferencesDeleteButton.layer?.cornerRadius = 7
                
                PreferencesBackupButton.styleButtonText(button: PreferencesBackupButton, buttonName: "Backup", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                PreferencesBackupButton.wantsLayer = true
                PreferencesBackupButton.layer?.backgroundColor = NSColor.gray.cgColor
                PreferencesBackupButton.layer?.cornerRadius = 7
                
                PreferencesCloseButton.styleButtonText(button: PreferencesCloseButton, buttonName: "Close", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                PreferencesCloseButton.wantsLayer = true
                PreferencesCloseButton.layer?.backgroundColor = NSColor.gray.cgColor
                PreferencesCloseButton.layer?.cornerRadius = 7
                
                PreferencesBackupsLabel.font = NSFont(name:AppFontBold, size: 13.0)
                PreferencesBackupsLabel.textColor = NSColor.white
                
                PreferencesBackupCountLabel.font = NSFont(name:AppFontBold, size: 13.0)
                PreferencesBackupCountLabel.textColor = NSColor.white
                
            }
        }
        
        CurrentAppearancePreferencesViewController = "\(currentStyle)"
        
        
    }//end of appearanceCheck()
    
    
    
    
    
    
    @IBAction func PreferencesBackupButtonAction(_ sender: Any) {
        
        PreferencesBackupButton.isEnabled = false
        
        /////////////////////////
        
        // Create Backups directory for all backup zips
        
        if !FM.fileExists(atPath: BackupsLocation) {
            do {
                try FM.createDirectory(atPath: BackupsLocation, withIntermediateDirectories: true, attributes: nil)
            } catch {
                print(error.localizedDescription)
                
                loggly(LogType.Error, text: "PreferencesViewController-\(#function): Unable to Create BackupsLocation directory: \(BackupsLocation)\n\(error.localizedDescription)")
                
                DoAlert.DisplayAlert(Class: "PreferencesViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to create BackupsLocation directory at\n\(BackupsLocation)\nPlease quit and try agian.", DatabaseError: "\(error.localizedDescription)")
                
                return
                
            }
        }
        
        ////////////////
        
        // MM-dd-yyyy.HH.mm.ssa
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "MM-dd-yyyy.hh.mm.ssa"  // MM-dd-yyyy.HH.mm.ssa
        
        let BackupFolderName = dateFormatterGet.string(from: Date())
        //print("BackupFolderName: \(BackupFolderName)")
        
        BackupFolderLcoation = String(format: "\(BackupsLocation)/\(BackupFolderName)")
        //print("BackupFolderLcoation: \(BackupFolderLcoation)")
        
        
        ///////////////
        
        //print(String("\(BackupFolderLcoation).zip"))
        
        // Create backup folder
        
//        if (!FM.fileExists(atPath: BackupFolderLcoation)) {
        if (!FM.fileExists(atPath: BackupFolderLcoation) && (!FM.fileExists(atPath: String("\(BackupFolderLcoation).zip")))) {
            do {
                try FM.createDirectory(atPath: BackupFolderLcoation, withIntermediateDirectories: true, attributes: nil)
            } catch {
                print(error.localizedDescription)

                loggly(LogType.Error, text: "PreferencesViewController-\(#function): Unable to Create\n\(BackupFolderLcoation)\n\(error.localizedDescription)")

                DoAlert.DisplayAlert(Class: "PreferencesViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to create \n\(BackupFolderLcoation)\nPlease quit and try agian.", DatabaseError: "\(error.localizedDescription)")

            }
        } else {
            
            let alert = NSAlert()
            alert.messageText = "A Backup Exists with this name."
            alert.informativeText = "Please wait a moment before creating another backup."
            alert.alertStyle = .warning
            alert.addButton(withTitle: "OK")
            
            if (CurrentAppearancePreferencesViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.window.titlebarAppearsTransparent = true
            
            
            alert.runModal()
            
//            let returnCode = alert.runModal()
//            rint("returnCode: \(returnCode)")
//
//            if (returnCode.rawValue == 1001) {
//
//                return
//
//            }
            
            
            delayWithSeconds(1.0) {
                self.PreferencesBackupButton.isEnabled = true
            }
            
            
            return
        }
        
    
        
        /////////////////////////
        
        // Copy DB to newly created dated Backup folder
        
        let SaveToPath = String(format: "\(BackupFolderLcoation)/ScriptoPortfolio.db")
        
        //print("SaveToPath: \(SaveToPath)")

        
        do {
            try FM.copyItem(atPath: DatabaseLocation, toPath: SaveToPath)
        } catch {
            
            removeBackupFolder()
            
            print(error.localizedDescription)
            
            loggly(LogType.Error, text: "PreferencesViewController-\(#function): Error copying DB to Path\n\(BackupFolderLcoation)\n\(error.localizedDescription)")
            
            DoAlert.DisplayAlert(Class: "PreferencesViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Error copying DB to Path \n\(BackupFolderLcoation)\nPlease quit and try agian.", DatabaseError: "\(error.localizedDescription)")
            
            return
            
        }
        
        /////////////////////////
        
        // Copy Images folder to the backups folder
        
        let SaveImagesToPath = String(format: "\(BackupFolderLcoation)/Images")
        
        //print("SaveImagesToPath: \(SaveImagesToPath)")
        
        do {
            try FM.copyItem(atPath: ImagesLocation, toPath: SaveImagesToPath)
        } catch {
            
            removeBackupFolder()
            
            print(error.localizedDescription)
            
            loggly(LogType.Error, text: "PreferencesViewController-\(#function): Error copying Images to Path\n\(SaveImagesToPath)\n\(error.localizedDescription)")
            
            DoAlert.DisplayAlert(Class: "PreferencesViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Error copying Images to Path \n\(SaveImagesToPath)\nPlease quit and try agian.", DatabaseError: "\(error.localizedDescription)")
            
            return
        }
        
        /////////////////////////
        
        // Copy the Stories folder to the backups folder
        
        let SaveStoriesToPath = String(format: "\(BackupFolderLcoation)/Stories")
        
        //print("SaveStoriesToPath: \(SaveStoriesToPath)")
        
        do {
            try FM.copyItem(atPath: StoriesLocation, toPath: SaveStoriesToPath)
        } catch {
            
            removeBackupFolder()
            
            print(error.localizedDescription)
            
            loggly(LogType.Error, text: "PreferencesViewController-\(#function): Error copying Stories to Path\n\(SaveStoriesToPath)\n\(error.localizedDescription)")
            
            DoAlert.DisplayAlert(Class: "PreferencesViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Error copying Stories to Path \n\(SaveStoriesToPath)\nPlease quit and try agian.", DatabaseError: "\(error.localizedDescription)")
            
            return
            
        }
        
        /////////////////////////
        
        // Copy the Stories folder to the backups folder
        
        let SaveLogsToPath = String(format: "\(BackupFolderLcoation)/ScriptoPortfolioLogs")
        
        //print("SaveLogsToPath: \(SaveLogsToPath)")
        
        do {
            try FM.copyItem(atPath: LogsLocation, toPath: SaveLogsToPath)
        } catch {
            
            removeBackupFolder()
            
            print(error.localizedDescription)
            
            loggly(LogType.Error, text: "PreferencesViewController-\(#function): Error copying Logs to Path\n\(SaveLogsToPath)\n\(error.localizedDescription)")
            
            DoAlert.DisplayAlert(Class: "PreferencesViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Error copying Logs to Path \n\(SaveLogsToPath)\nPlease quit and try agian.", DatabaseError: "\(error.localizedDescription)")
            
            return
            
        }
        
        /////////////////////////
        
        ZipBackupFile()
        
    }
    
    
    //  https://github.com/ZipArchive/ZipArchive
    
    
    
    
    
    
    
    
    
    func ZipBackupFile() {
        
        
        let ZipPath = "zip -r \(BackupFolderLcoation).zip \(BackupFolderLcoation)"
        
        print("ZipPath: \"\(ZipPath)\"")
        
        // https://gist.github.com/Seasons7/836d3676884a40c8c98a
        
        // Create a Task instance (was NSTask on swift pre 3.0)
        let task = Process()
        
        // Set the task parameters
        task.launchPath = "/bin/bash"
        task.arguments = ["-c" , ZipPath]
        
        // Create a Pipe and make the task
        // put all the output there
        let pipe = Pipe()
        task.standardOutput = pipe
        task.standardError = pipe
        
        // Launch the task
        task.launch()
        
        // Get the data
        let data = pipe.fileHandleForReading.readDataToEndOfFile()
        let output = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
        
        //print(output!)
        
        task.waitUntilExit()
        let status = task.terminationStatus
        
        if (status == 0) {
            
            //print("Zip Task succeeded.")
            
            
            
            ////////////////////////
            // write backup to DB
            
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "MM-dd-yyyy.hh.mm.ssa"  // MM-dd-yyyy.HH.mm.ssa
            
            let BackupFolderNameDate = dateFormatterGet.string(from: Date())
            
            let NewBackup = String(format: "INSERT INTO backupTable (ikey,path,createdate) VALUES ((select MAX(ikey) + 1 from backupTable),\"\(BackupFolderLcoation).zip\",\"\(BackupFolderNameDate)\")")
            
            print("NewBackup: \(NewBackup)")
            
            do {
                
                
                try db.executeUpdate(NewBackup, values: nil)
                
            } catch let error as NSError {
                
                removeBackupFolder()
                
                let ErrorMessage = "CharacterInfoViewConroller -\(#function) - Failed to Insert Backup Information into DB.\nError: \(error.localizedDescription)"
                
                print(ErrorMessage)
                
                DoAlert.DisplayAlert(Class: "CharacterInfoViewConroller", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to Insert Backup Information into DB.\n", DatabaseError: "\(error.localizedDescription)\n\n\(db.lastErrorMessage())")
                
                return
            }
            

            ////////////////////////
            
            removeBackupFolder()
            
            
            PreferencesBackupButton.isEnabled = true
        } else {
            
            removeBackupFolder()
            print("Task failed.")
            print("Zip Task Failed\n\(output!)")
            
            loggly(LogType.Error, text: "PreferencesViewController-\(#function): Zip Task Failed\n\(output!)")
            
            DoAlert.DisplayAlert(Class: "PreferencesViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Zip Task Failed", DatabaseError: "\(output!)")
        
            return
        
        }
        
        
        BackupTableData()
        self.PreferencesBackupButton.isEnabled = true
        
        
    }
    
    
    func removeBackupFolder() {
        
        print("BackupFolderLcoation: \"\(BackupFolderLcoation)\"")
        
        
        do {
            try FM.removeItem(atPath: BackupFolderLcoation)
        } catch {
            
            
            
            print(error.localizedDescription)
            
            loggly(LogType.Error, text: "PreferencesViewController-\(#function): Error removing folder at Path\n\(BackupFolderLcoation)\n\(error.localizedDescription)")
            
            DoAlert.DisplayAlert(Class: "PreferencesViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Error removing folder at Path\n\(BackupFolderLcoation)", DatabaseError: "\(error.localizedDescription)")
            
            self.PreferencesBackupButton.isEnabled = true
            
        }
        
    }
    
    
    
    
    @IBAction func PreferencesDeleteButtonAction(_ sender: Any) {
        
        self.PreferencesDeleteButton.isEnabled = false
        
        let alert = NSAlert()
        alert.messageText = "Delete a backup ?"
        alert.informativeText = "Are you sure you want to delete a backup ?"
        alert.alertStyle = .warning
        alert.addButton(withTitle: "Cancel")
        alert.addButton(withTitle: "Yes Delete!")
        
        if (CurrentAppearancePreferencesViewController == "Dark") {
            alert.window.backgroundColor = AlertBackgroundColorDarkMode
        } else {
            alert.window.backgroundColor = WindowBackgroundColor
        }
        
        alert.window.titlebarAppearsTransparent = true
        

        let returnCode = alert.runModal()
        //print("returnCode: \(returnCode)")
        if (returnCode.rawValue == 1000) {
            self.PreferencesDeleteButton.isEnabled = true
            return
        }

        if (returnCode.rawValue == 1001) {

            let SelectedRow = PreferencesBackupTable.selectedRow
            let isIndexValid = BackupDatesListArray.indices.contains(SelectedRow)
            
            var BackupDate = String()
            var Ikey = String()
            
            
            if isIndexValid {
                BackupDate = self.BackupDatesListArray[SelectedRow].BackupName
                Ikey = self.BackupDatesListArray[SelectedRow].Ikey
                print("Ikey: \"\(Ikey)\" - BackupDate: \"\(BackupDate)\"")
            }
            
            
            
            
            // "DELETE FROM backupTable where createdate = '%@'"
            
            
            let DeleteBackupQuery = String(format: "DELETE FROM backupTable WHERE ikey = \"\(Ikey)\" AND createdate = \"\(BackupDate)\"")
            
            print("DeleteBackupQuery: \(DeleteBackupQuery)")
            
            dbQueue.inTransaction() {(_, rollback) in
                
                if !db.executeStatements(DeleteBackupQuery) {
                    
                    print("PreferencesViewController-\(#function): Create Database Failure: \(db.lastErrorMessage())")
                    
                    DoAlert.DisplayAlert(Class: "PreferencesViewController", Level: 1, Function: "\(#function)", MessageText: "Unable to delete \"\(BackupDate)\" from Database", InformativeText: "Database changes were rolled back.\nPlease Quit and try again. If this continues please restart your computer.", DatabaseError: "SQL Create Database Query Failed: \(DeleteBackupQuery)\n\n\(db.lastErrorMessage())")
                    
                    rollback.pointee = true
                    
                    
                    
                    return
                }
                
            }
            
            /////////////////////////////////////////////////////////
            
            BackupTableData()
            
            /////////////////////////////////////////////////////////
            
            let DeleteFilePath = String(format: "\(BackupsLocation)/\(BackupDate).zip")
            
            print("DeleteFilePath: \"\(DeleteFilePath)\"")
            
            do {
                try FM.removeItem(atPath: DeleteFilePath)
            } catch {
                
                print("PreferencesViewController-\(#function): Error removing folder at Path\n\"\(DeleteFilePath)\"\n\(error.localizedDescription)")
                
                loggly(LogType.Error, text: "PreferencesViewController-\(#function): Error removing folder at Path\n\"\(DeleteFilePath)\"\n\(error.localizedDescription)")
                
                DoAlert.DisplayAlert(Class: "PreferencesViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Error removing folder at Path\n\"\(DeleteFilePath)\"", DatabaseError: "\(error.localizedDescription)")
                
                
                
            }
            
            
            

        }
        
        
        
        
        
        
        
    }// end of PreferencesDeleteButtonAction
    
    
    
    
    @IBAction func PreferencesCloseButtonAction(_ sender: Any) {
        NSColorPanel.shared.orderOut(nil)
        self.view.window?.close()
    }
    
    
    /////////////////////////////////////////////////
    /////////////////////////////////////////////////
    /////////////////////////////////////////////////
    /////////////////////////////////////////////////
    
    
    
    
    
    
    
    
    func BackupTableData() {

        db.open()
        
        BackupDatesListArray.removeAll()

        var BackupDB: String!
        var IkeyDB: String!

        do {
            let UserQuery = try db.executeQuery("SELECT * FROM backupTable", values:nil)

            while UserQuery.next() {
                BackupDB = UserQuery.string(forColumn: "createdate")
                IkeyDB = UserQuery.string(forColumn: "ikey")
                let DBDateToAdd = BackupDatesList.init(BackupName:BackupDB, Ikey: IkeyDB)
                BackupDatesListArray.append(DBDateToAdd)
                //print("DBDateToAdd: \"\(DBDateToAdd)\"")
            }

        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "PreferencesViewController-\(#function) Query Failed: \"SELECT * FROM backupTable\"")
            DoAlert.DisplayAlert(Class: "PreferencesViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"SELECT * FROM backupTable\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
        }

        PreferencesBackupCountLabel.stringValue = "\(BackupDatesListArray.count)"
        PreferencesBackupTable.reloadData()

    }

    func numberOfRows(in tableVieww: NSTableView) -> Int {

        return BackupDatesListArray.count

    }

    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {

        guard let cell = tableView.makeView(withIdentifier: tableColumn!.identifier, owner: self) as? NSTableCellView else { return nil }


        if (tableColumn?.identifier)!.rawValue == "createdate" {
            cell.textField?.stringValue = BackupDatesListArray[row].BackupName
        }
        if (tableColumn?.identifier)!.rawValue == "ikey" {
            cell.textField?.stringValue = BackupDatesListArray[row].Ikey
        }

        return cell

    }



    func tableViewSelectionDidChange(_ notification: Notification) {

        let SelectedRow = PreferencesBackupTable.selectedRow
        //print("SelectedRow: \(SelectedRow)")
        //print("Users: \(BackupDatesListArray.count)")

        let isIndexValid = BackupDatesListArray.indices.contains(SelectedRow)

        if isIndexValid {
            let BackupDate: String = self.BackupDatesListArray[SelectedRow].BackupName
            let Ikey: String = self.BackupDatesListArray[SelectedRow].Ikey
            print("Ikey: \"\(Ikey)\" - BackupDate: \"\(BackupDate)\"")
            PreferencesDeleteButton.isEnabled = true
        } else {
            PreferencesDeleteButton.isEnabled = false
        }

    }// end of tableViewSelectionDidChange
    
    
    
    /////////////////////////////////////////
    
    // Crash Logs Tab
    
    
    
    @IBAction func SendLogsRadioAction(_ sender: Any) {
        
        sendNOCrashLogsRadioButton.state = .off
        
        dbQueue.inTransaction { _, rollback in
            
            let UpdateDBStatement = String(format:
                "UPDATE dbasetable SET alwayssendlogs = true;" +
                "UPDATE dbasetable SET appcentersendlogsprompt = true;"
            )
            
            let Success = db.executeStatements(UpdateDBStatement)
            
            if (!Success) {
                
                rollback.pointee = true
                
                loggly(LogType.Warnings, text: "PreferencesWindow-\(#function): Unable to create to update dbasetable\n\(UpdateDBStatement)\n\(db.lastErrorMessage())")
                
                DoAlert.DisplayAlert(Class: "PreferencesWindow", Level: 1, Function: "\(#function)", MessageText: "An Error has occurred", InformativeText: "Database changes were rolled back from updating.\nPlease Quit and try again.\nIf this continues please restart your computer.", DatabaseError: "\nSQL Query Failed:\n\n\(UpdateDBStatement)\n\nLastErrorMessage: \"\(db.lastErrorMessage())\"")
                
                return
                
            }// end of !Success
            
        }// end of dbQueue.inTransaction

        sendUpdatedLable.isHidden = false
        delayWithSeconds(0.25) {
            self.sendUpdatedLable.isHidden = true
            delayWithSeconds(0.25) {
                self.sendUpdatedLable.isHidden = false
                delayWithSeconds(0.75) {
                    self.sendUpdatedLable.isHidden = true
                }
            }
        }
        
    }// end of SendLogsRadioAction
    
    
    
    @IBAction func SendNoLogsRadioAction(_ sender: Any) {
        
        sendCrashLogsRadioButton.state = .off
        
        dbQueue.inTransaction { _, rollback in
            
            let UpdateDBStatement = String(format:
                "UPDATE dbasetable SET alwayssendlogs = false;" +
                "UPDATE dbasetable SET appcentersendlogsprompt = true;"
            )
            
            let Success = db.executeStatements(UpdateDBStatement)
            
            if (!Success) {
                
                rollback.pointee = true
                
                loggly(LogType.Warnings, text: "PreferencesWindow-\(#function): Unable to create to update dbasetable\n\(UpdateDBStatement)\n\(db.lastErrorMessage())")
                
                DoAlert.DisplayAlert(Class: "PreferencesWindow", Level: 1, Function: "\(#function)", MessageText: "An Error has occurred", InformativeText: "Database changes were rolled back from updating.\nPlease Quit and try again.\nIf this continues please restart your computer.", DatabaseError: "\nSQL Query Failed:\n\n\(UpdateDBStatement)\n\nLastErrorMessage: \"\(db.lastErrorMessage())\"")
                
                return
                
            }// end of !Success
            
        }// end of dbQueue.inTransaction

        sendUpdatedLable.isHidden = false
        delayWithSeconds(0.25) {
            self.sendUpdatedLable.isHidden = true
            delayWithSeconds(0.50) {
                self.sendUpdatedLable.isHidden = false
                delayWithSeconds(1.0) {
                    self.sendUpdatedLable.isHidden = true
                }
            }
        }
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}// end of PreferencesViewController
