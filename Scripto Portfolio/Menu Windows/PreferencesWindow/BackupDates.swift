//
//  BackupDates.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 4/17/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Foundation

class BackupDatesList: NSObject {
    let BackupName: String
    let Ikey: String
    
    init(BackupName: String,Ikey: String) {
        self.BackupName = BackupName
        self.Ikey = Ikey
    }
    
}
