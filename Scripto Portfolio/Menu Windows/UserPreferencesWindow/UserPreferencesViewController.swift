//
//  UserPreferencesViewController.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 2/14/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Cocoa
import SwiftLoggly

class UserPreferencesViewController: NSViewController,NSTextFieldDelegate {

    
    @IBOutlet weak var UPCurrentUserLabel: NSTextField!
    @IBOutlet weak var UPUserLabel: NSTextField!
    @IBOutlet weak var UPBirthdateLabel: NSTextField!
    @IBOutlet weak var UPCurrentPasswordLabel: NSTextField!
    @IBOutlet weak var UPNewPasswordLabel: NSTextField!
    @IBOutlet weak var UPConfirmPasswordLabel: NSTextField!
    @IBOutlet weak var UPUserColorLabel: NSTextField!
    
    
    @IBOutlet weak var UPCurrentUserTextField: NSTextField!
    @IBOutlet weak var UPUserTextField: NSTextField!
    
    @IBOutlet weak var UPBirthdateNSDatePicker: NSDatePicker!
    
    @IBOutlet weak var UPCurrentPasswordTextField: NSSecureTextField!
    @IBOutlet weak var UPNewPasswordTextField: NSSecureTextField!
    @IBOutlet weak var UPConfirmPasswordTextField: NSTextField!
    
    @IBOutlet weak var UPUserColorColorWell: NSColorWell!
    
    
    @IBOutlet weak var UPUpdateUserNameButton: NSButton!
    @IBOutlet weak var UPUpdateBirthdateButton: NSButton!
    @IBOutlet weak var UPUpdatePasswordButton: NSButton!
    @IBOutlet weak var UPUserColorInfoButton: NSButton!
    @IBOutlet weak var UPCloseButton: NSButton!
    
    
    @IBOutlet weak var UPUpdatedLabel: NSTextField!
    
    
    var CurrentAppearanceUserPreferencesViewController = String()
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        

    }
    
    override func viewWillAppear() {
    
        UPUserTextField.delegate = self
        UPCurrentUserTextField.stringValue = CurrentUserName!
        GetUserInfo()
        
        
        
    }
    
    override func viewDidLayout() {
        
        //        enum InterfaceStyle : String {
        //            case Dark, Light
        //
        //            init() {
        //                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
        //                self = InterfaceStyle(rawValue: type)!
        //            }
        //        }
        //
        //        let currentStyle = InterfaceStyle()
        //
        //        //print("currentStyle: \"\(currentStyle)\"  -  CurrentAppearance: \"\(CurrentAppearance)\" ")
        //
        //        if ("\(currentStyle)" != CurrentAppearanceNewUserViewController) {
        //            appearanceCheck()
        //        }
        
        appearanceCheck()
        
        super.viewDidLayout()
        
        
        
        
    }// end of viewDidLayout()
    
    func appearanceCheck() {
        
        enum InterfaceStyle : String {
            case Dark, Light
            
            init() {
                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
                self = InterfaceStyle(rawValue: type)!
            }
        }
        
        let currentStyle = InterfaceStyle()
        
        print("currentStyle: \"\(currentStyle)\" CurrentAppearanceUserPreferencesViewController: \"\(CurrentAppearanceUserPreferencesViewController)\" ")
        
        if ("\(currentStyle)" != CurrentAppearanceUserPreferencesViewController) {
            
            if (currentStyle.rawValue == "Dark") {
                print("Dark Style Detected - NewUserViewController")
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColorDarkMode
                
                UPUpdateUserNameButton.styleButtonText(button: UPUpdateUserNameButton, buttonName: "Update Username", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                UPUpdateUserNameButton.wantsLayer = true
                UPUpdateUserNameButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                UPUpdateUserNameButton.layer?.cornerRadius = 7
                
                UPUpdateBirthdateButton.styleButtonText(button: UPUpdateBirthdateButton, buttonName: "Update Birthdate", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                UPUpdateBirthdateButton.wantsLayer = true
                UPUpdateBirthdateButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                UPUpdateBirthdateButton.layer?.cornerRadius = 7
                
                UPUpdatePasswordButton.styleButtonText(button: UPUpdatePasswordButton, buttonName: "Update Password", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                UPUpdatePasswordButton.wantsLayer = true
                UPUpdatePasswordButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                UPUpdatePasswordButton.layer?.cornerRadius = 7
                
                UPCloseButton.styleButtonText(button: UPCloseButton, buttonName: "Close", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                UPCloseButton.wantsLayer = true
                UPCloseButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                UPCloseButton.layer?.cornerRadius = 7
                
                
                
            } else if (currentStyle.rawValue == "Light") {
                print("Light Style Detected - NewUserViewController")
                
                
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColor.cgColor
                
                UPUpdateUserNameButton.styleButtonText(button: UPUpdateUserNameButton, buttonName: "Update Username", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                UPUpdateUserNameButton.wantsLayer = true
                UPUpdateUserNameButton.layer?.backgroundColor = NSColor.gray.cgColor
                UPUpdateUserNameButton.layer?.cornerRadius = 7
                
                UPUpdateBirthdateButton.styleButtonText(button: UPUpdateBirthdateButton, buttonName: "Update Birthdate", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                UPUpdateBirthdateButton.wantsLayer = true
                UPUpdateBirthdateButton.layer?.backgroundColor = NSColor.gray.cgColor
                UPUpdateBirthdateButton.layer?.cornerRadius = 7
                
                UPUpdatePasswordButton.styleButtonText(button: UPUpdatePasswordButton, buttonName: "Update Password", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                UPUpdatePasswordButton.wantsLayer = true
                UPUpdatePasswordButton.layer?.backgroundColor = NSColor.gray.cgColor
                UPUpdatePasswordButton.layer?.cornerRadius = 7
                
                UPCloseButton.styleButtonText(button: UPCloseButton, buttonName: "Close", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                UPCloseButton.wantsLayer = true
                UPCloseButton.layer?.backgroundColor = NSColor.gray.cgColor
                UPCloseButton.layer?.cornerRadius = 7
                
            }
        }
        
        CurrentAppearanceUserPreferencesViewController = "\(currentStyle)"
        
        
    }//end of appearanceCheck()
    
    
    
    
    
    func GetUserInfo() {
        
        db.open()
        
        var BirthdateDB: String!
        var ColorDB: String!
        
        let UserQueryString = "SELECT * FROM users WHERE user = \"\(CurrentUserName!)\" AND ikey = \(CurrentUserIkey!)"
        print("UserQueryString: \(UserQueryString)")
        
        do {
            let UserQuery = try db.executeQuery("\(UserQueryString)", values:nil)
            
            while UserQuery.next() {
                BirthdateDB = UserQuery.string(forColumn: "birthdate")
                ColorDB = UserQuery.string(forColumn: "usercolor")
            }
            
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "UserPreferencesViewController-\(#function) Query Failed: \"SELECT * FROM users WHERE user = \"\(CurrentUserName!)\" AND ikey = \(CurrentUserIkey!)\"")
            DoAlert.DisplayAlert(Class: "UserPreferencesViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"SELECT * FROM users WHERE user = \"\(CurrentUserName!)\" AND ikey = \(CurrentUserIkey!)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
        }
        
        //////////////////////////////////
        
        // setting the Birthdate
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "MM/dd/yyyy"
        //dateFormatterGet.dateFormat = "MM-DD-YYYY"
        
        //let Bdate = dateFormatterGet.date(from: BirthdateDB)
        let Bdate: Date = dateFormatterGet.date(from: BirthdateDB)!
        
        UPBirthdateNSDatePicker.dateValue = Bdate
        
        //////////////////////////////////
        
        // setting the user color
        
        let color = NSColor().HexToColor(hexString: ColorDB, alpha: 1.0)
        
        UPUserColorColorWell.color = color

    }// end of GetUserInfo
    
    
    @IBAction func UPUpdateUsernameAction(_ sender: Any) {
        
        if (UPUserTextField.stringValue == "") {
            __NSBeep()
            return
        }
        
        let alert = NSAlert()
        alert.messageText = "Update Username"
        alert.informativeText = "Are you sure you want to update your Username?"
        alert.alertStyle = .warning
        alert.addButton(withTitle: "Cancel")
        alert.addButton(withTitle: "Yes change it")
        
        if (CurrentAppearanceUserPreferencesViewController == "Dark") {
            alert.window.backgroundColor = AlertBackgroundColorDarkMode
        } else {
            alert.window.backgroundColor = WindowBackgroundColor
        }
        
        alert.window.titlebarAppearsTransparent = true
        
        
        
        let returnCode = alert.runModal()
        print("returnCode: \(returnCode)")
        
        
        
        
        if (returnCode.rawValue == 1000) {
           
            return

        }// end of first button
        
        if (returnCode.rawValue == 1001) {
            var UpdateCharacterUserNameSqlquery = String()
            
            do {
                
                UpdateCharacterUserNameSqlquery = String(format: "UPDATE users set user = \"\(UPUserTextField.stringValue)\" where ikey = \(CurrentUserIkey!) and user = \"\(CurrentUserName!)\"")
                
                print("UpdateCharacterUserNameSqlquery: \(UpdateCharacterUserNameSqlquery)")
                
                
                
                try db.executeUpdate(UpdateCharacterUserNameSqlquery, values: nil)
                
            } catch let error as NSError {
                
                let ErrorMessage = "\(#function) - Failed to do query \(UpdateCharacterUserNameSqlquery)\nError: \(error.localizedDescription)"
                
                print(ErrorMessage)
                
                loggly(LogType.Error, text: ErrorMessage)
                
                DoAlert.DisplayAlert(Class: "UserPreferencesViewController", Level: 3, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to update Username\nPlease try it again.\n\n\(UpdateCharacterUserNameSqlquery)", DatabaseError: "\(db.lastErrorMessage())")
                
                
            }
            
            //UPCurrentUserTextField.stringValue = CurrentUserName!
            
            UPCurrentUserTextField.stringValue = UPUserTextField.stringValue
            CurrentUserName = UPUserTextField.stringValue
            
            NewMenu.title = "\(UPUserTextField.stringValue)"
            
            StoryListWindow.close()
            
            //delayWithSeconds(0.1) {
                StoryListWindow.showWindow(self)
            
            //}
            
            
            UPUserTextField.stringValue = ""
            
            UpdatedLabel(LabelText: "Updated UserName")
            
            delayWithSeconds(0.25) {
                UserPreferencesWindow.window?.makeKey()
            }
            
        }
        
        
        
        
        
        
    }
    
    override func controlTextDidChange(_ obj: Notification) {
        
        //print("controlTextDidChange Ran")
        
        if (self.UPUserTextField.stringValue.count < 1) {
            
        } else {
            
            let lastCharacter = String(describing: self.UPUserTextField.stringValue.last!)
            
            //print("lastCharacter: \(lastCharacter)")
            
            let SymbolTest = CharacterSet.symbols.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
            
            let punctuationTest = CharacterSet.punctuationCharacters.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
            
            //let letterTest = CharacterSet.letters.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
            
            let SpaceNewLineTest = CharacterSet.whitespacesAndNewlines.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
            
            if (SymbolTest || punctuationTest || SpaceNewLineTest) {
                
                //                print("SymbolTest: \(SymbolTest)")
                //                print("punctuationTest: \(punctuationTest)")
                //                print("letterTest: \(letterTest)")
                //                print("SpaceNewLineTest: \(SpaceNewLineTest)")
                
                __NSBeep()
                
                self.UPUserTextField.stringValue = String(self.UPUserTextField.stringValue.dropLast())
                
                return
            }

        }
        
    }
    
    ///////////////////////////////////////////
    
    
    @IBAction func UPBirthdateButtonAction(_ sender: Any) {
        
        
        let alert = NSAlert()
        alert.messageText = "Update Birthdate"
        alert.informativeText = "Are you sure you want to update your Birthdate?"
        alert.alertStyle = .warning
        alert.addButton(withTitle: "Cancel")
        alert.addButton(withTitle: "Yes change it")
        
        if (CurrentAppearanceUserPreferencesViewController == "Dark") {
            alert.window.backgroundColor = AlertBackgroundColorDarkMode
        } else {
            alert.window.backgroundColor = WindowBackgroundColor
        }
        
        alert.window.titlebarAppearsTransparent = true
        
        
        
        let returnCode = alert.runModal()
        print("returnCode: \(returnCode)")
        
        
        
        
        if (returnCode.rawValue == 1000) {
            
            return
            
        }// end of first button
        
        if (returnCode.rawValue == 1001) {
            var UpdateUserBirthdateSqlquery = String()
            
            do {
            
                
                let dateFormatterGet = DateFormatter()
                dateFormatterGet.dateFormat = "MM/dd/yyyy"
                //dateFormatterGet.dateFormat = "MM-DD-YYYY"
                
                //let Bdate = dateFormatterGet.date(from: BirthdateDB)
                let Bdate: String = dateFormatterGet.string(from: UPBirthdateNSDatePicker.dateValue)
                
                print("Bdate: \"\(Bdate)\"")
                
                UpdateUserBirthdateSqlquery = String(format: "UPDATE users set birthdate = \"\(Bdate)\" where ikey = \(CurrentUserIkey!) and user = \"\(CurrentUserName!)\"")
                
                print("UpdateUserBirthdateSqlquery: \(UpdateUserBirthdateSqlquery)")
                
               
                
                try db.executeUpdate(UpdateUserBirthdateSqlquery, values: nil)
                
            } catch let error as NSError {
                
                let ErrorMessage = "\(#function) - Failed to do query \(UpdateUserBirthdateSqlquery)\nError: \(error.localizedDescription)"
                
                print(ErrorMessage)
                
                loggly(LogType.Error, text: ErrorMessage)
                
                DoAlert.DisplayAlert(Class: "UserPreferencesViewController", Level: 3, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to update User Birthdate\nPlease try it again.\n\n\(UpdateUserBirthdateSqlquery)", DatabaseError: "\(db.lastErrorMessage())")
                
                
            }
            
            UpdatedLabel(LabelText: "Updated Birthdate")
            
            
            
        }
        
        
        
        
        
    }// end of UPBirthdateButtonAction
    
    
    @IBAction func UPUpdatePasswordAction(_ sender: Any) {
        
        let CPass = RemoveWhiteSpace(aString: UPCurrentPasswordTextField.stringValue)
        let NPass = RemoveWhiteSpace(aString: UPNewPasswordTextField.stringValue)
        let ConPass = RemoveWhiteSpace(aString: UPConfirmPasswordTextField.stringValue)
        
        
//        let decodedCP = CPass.base64Encoded()
//        let decodedNP = CPass.base64Encoded()
//        let decodedCoP = CPass.base64Encoded()
        
        
        if (NPass == "" || ConPass == "") {
            
            let alert = NSAlert()
            alert.messageText = "New Password OR Confirm Password fields are blank"
            alert.informativeText = "Please try again. "
            alert.alertStyle = .warning
            alert.addButton(withTitle: "OK")
            
            if (CurrentAppearanceUserPreferencesViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.window.titlebarAppearsTransparent = true
            
            let returnCode = alert.runModal()
            print("returnCode: \(returnCode)")
            
            if (returnCode.rawValue == 1000) {
                return
            }
        }
        
        
        ///////////////////////////////////////
        
        
        var PassDB: String!

        let Query = "select password from users where ikey = \"\(CurrentUserIkey!)\" and user = \"\(CurrentUserName!)\""
        
        //print("Query 1: \(Query)")

        do {
            
            let CharacterQueryRun = try db.executeQuery(Query, values:nil)
            
            while CharacterQueryRun.next() {
                PassDB = CharacterQueryRun.string(forColumn: "password")
            }
            
        } catch {
            
            let ErrorMessage = "\(#function) - Failed to do query \(Query)\nError: \(error.localizedDescription)"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "UserPreferencesViewController", Level: 3, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to verify current User information.\nPlease try it again.\n\n", DatabaseError: "\(db.lastErrorMessage())")
            
        }
        
        ///////////////////////////////////////
        
        
        
        //let encryptedPassword = passwordLabelWhiteSpace.base64Encoded()
        
        //print("encryptedPassword: \"\(encryptedPassword!)\" ")
        
        //print("decryptedPassword: \"\(encryptedPassword?.base64Decoded()!)\" ")
        
        let decodedPW = PassDB.base64Decoded()
        
        
        //print("CPass: \"\(CPass)\"  decodedPW: \"\(decodedPW)\" ")
        
        
        
        ///////////////////////////////////////
        
        
        if (CPass != decodedPW) {
            
            let alert = NSAlert()
            alert.messageText = "Current Password Does Not Match"
            alert.informativeText = "Please retype and try again. "
            alert.alertStyle = .warning
            alert.addButton(withTitle: "OK")
            
            if (CurrentAppearanceUserPreferencesViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.window.titlebarAppearsTransparent = true
            
            let returnCode = alert.runModal()
            print("returnCode: \(returnCode)")
            
            if (returnCode.rawValue == 1000) {
                return
            }
            
        }
        
        ///////////////////////////////////////
        
        if (NPass != ConPass) {
            
            let alert = NSAlert()
            alert.messageText = "New Password and Confirm Password do not match"
            alert.informativeText = "Please retype them and try again. "
            alert.alertStyle = .warning
            alert.addButton(withTitle: "OK")
            
            if (CurrentAppearanceUserPreferencesViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.window.titlebarAppearsTransparent = true
            
            let returnCode = alert.runModal()
            print("returnCode: \(returnCode)")
            
            if (returnCode.rawValue == 1000) {
                return
            }
            
        }
        
        ///////////////////////////////////////
        
        
        let alert = NSAlert()
        alert.messageText = "Update Password?"
        alert.informativeText = "Are you sure you want to update your password"
        alert.alertStyle = .warning
        alert.addButton(withTitle: "Cancel")
        alert.addButton(withTitle: "OK")
        
        if (CurrentAppearanceUserPreferencesViewController == "Dark") {
            alert.window.backgroundColor = AlertBackgroundColorDarkMode
        } else {
            alert.window.backgroundColor = WindowBackgroundColor
        }
        
        alert.window.titlebarAppearsTransparent = true
        
        let returnCode = alert.runModal()
        print("returnCode: \(returnCode)")
        
        if (returnCode.rawValue == 1000) {
            
            return
            
        }
        
        if (returnCode.rawValue == 1001) {
            
            var UpdateCharacterPassQuery = String()
            let encryptedPassword = ConPass.base64Encoded()!
            
            
            do {
                
                UpdateCharacterPassQuery = String(format: "UPDATE users set password = \"\(encryptedPassword)\" where ikey = \(CurrentUserIkey!) and user = \"\(CurrentUserName!)\"")
                
                print("UpdateCharacterPassQuery: \(UpdateCharacterPassQuery)")
                
                
                
                try db.executeUpdate(UpdateCharacterPassQuery, values: nil)
                
            } catch let error as NSError {
                
                let ErrorMessage = "\(#function) - Failed to do query.\nError: \(error.localizedDescription)"
                
                print(ErrorMessage)
                
                loggly(LogType.Error, text: ErrorMessage)
                
                DoAlert.DisplayAlert(Class: "UserPreferencesViewController", Level: 3, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to update User Information\nPlease try again.\n\n", DatabaseError: "\(db.lastErrorMessage())")
                
            }
            
            UpdatedLabel(LabelText: "Updated Password")
        
            UPCurrentPasswordTextField.stringValue = ""
            UPNewPasswordTextField.stringValue = ""
            UPConfirmPasswordTextField.stringValue = ""
            
        }
    
    }//  end of UPUpdatePasswordAction
    
    
    
    
    @IBAction func UPUpdateUserColorAction(_ sender: Any) {
        
        let ColorWellColor = UPUserColorColorWell.color
        
        let Hex = getHexFromColor(color: ColorWellColor)
        
        print("Hex: \(Hex)")
        
        let ColorHexString = Hex as String
        
        /////////
        
        var UpdateCharacterColorQuery = String()
        
        do {
            
            UpdateCharacterColorQuery = String(format: "UPDATE users set usercolor = \"\(ColorHexString)\" where ikey = \(CurrentUserIkey!) and user = \"\(CurrentUserName!)\"")
            
            print("UpdateCharacterColorQuery: \(UpdateCharacterColorQuery)")
            
            
            
            try db.executeUpdate(UpdateCharacterColorQuery, values: nil)
            
        } catch let error as NSError {
            
            let ErrorMessage = "\(#function) - Failed to do Update \(UpdateCharacterColorQuery)\nError: \(error.localizedDescription)"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "UserPreferencesViewController", Level: 3, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to update User Color\nPlease try it again.\n\n\(UpdateCharacterColorQuery)", DatabaseError: "\(db.lastErrorMessage())")
            
            
        }

    }
    
    
    
    @IBAction func UPColorInfoButtonAction(_ sender: Any) {

        let ColorInfoButtonPopover = NSPopover()
        ColorInfoButtonPopover.contentViewController = NSStoryboard(name: NSStoryboard.Name(rawValue: "UserPreferencesWindow"), bundle: nil).instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "UserFontColorWC")) as? NSViewController
        ColorInfoButtonPopover.animates = true
        ColorInfoButtonPopover.behavior = NSPopover.Behavior.transient
        ColorInfoButtonPopover.show(relativeTo: UPUserColorInfoButton.visibleRect, of: UPUserColorInfoButton, preferredEdge: NSRectEdge.minY)
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    @IBAction func UPCloseButton(_ sender: Any) {
        NSColorPanel.shared.orderOut(nil)
        self.view.window?.close()
        
    }
    
    func RemoveWhiteSpace(aString: String) -> String {
        let replaced = aString.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        return replaced
    }
    
    func getHexFromColor(color: NSColor) -> NSString {
        // Get the red, green, and blue components of the color
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        
        var rInt, gInt, bInt: Int
        var rHex, gHex, bHex: NSString
        
        var hexColor: NSString
        
        color.getRed(&r, green: &g, blue: &b, alpha: &a)
        
        //print("R: \(r) G: \(g) B:\(b) A:\(a)")
        
        // Convert the components to numbers (unsigned decimal integer) between 0 and 255
        rInt = Int((r * 255.99999))
        gInt = Int((g * 255.99999))
        bInt = Int((b * 255.99999))
        

        // Convert the numbers to hex strings
        rHex = NSString(format:"%2X", rInt)
        gHex = NSString(format:"%2X", gInt)
        bHex = NSString(format:"%2X", bInt)
        
        if (rHex == " 0") {
            rHex = "00"
        }
        if (gHex == " 0") {
            gHex = "00"
        }
        if (bHex == " 0") {
            bHex = "00"
        }
        
        
        hexColor = "\(rHex)\(gHex)\(bHex)" as NSString
        //print("HEX: \"\(rHex)\" \"\(gHex)\" \"\(bHex)\"")
        
        return hexColor
    }

    
    
    
    
    
    func UpdatedLabel(LabelText: String) {
        
        UPUpdatedLabel.stringValue = LabelText
        UPUpdatedLabel.font = NSFont(name:AppFontBold, size: 13.0)
        UPUpdatedLabel.textColor = NSColor.systemGreen
        
        UPUpdatedLabel.isHidden = false
        
        delayWithSeconds(1.0) {
            self.UPUpdatedLabel.isHidden = true
            delayWithSeconds(0.2) {
                self.UPUpdatedLabel.isHidden = false
                delayWithSeconds(0.5) {
                    self.UPUpdatedLabel.isHidden = true
                }
            }
        }
        
        
        
    }
    
    
    
    
    
    
} // end of UserPreferencesViewController













