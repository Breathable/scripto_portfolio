//
//  SharedWindowViewController.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 2/14/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Cocoa
import SwiftLoggly



class SharedWindowViewController: NSViewController, NSTableViewDataSource, NSTableViewDelegate {
    
    
    @IBOutlet weak var SharedCategoryPopupButton: NSPopUpButton!
    @IBOutlet weak var SharedCategoryTableView: NSTableView!
    @IBOutlet weak var ShareWindowCloseButton: NSButton!
    @IBOutlet weak var SharedWindowOpenButton: NSButton!
    
    var InformationListArray: [SharedStoryInformation] = []
    
    var CurrentAppearanceSharedWindowViewController = String()
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
         
    }
    
    override func viewWillAppear() {
        
        
        
        PopulateCategoryPopupButton()
        
        
        let DoubleSelect: Selector = #selector(SharedWindowViewController.SharedWindowOpenButtonAction(_:))
        SharedCategoryTableView.doubleAction = DoubleSelect
        
        SharedCategoryTableView.delegate = self
        SharedCategoryTableView.dataSource = self
        
        
    }
    
    override func viewDidLayout() {
        
        //        enum InterfaceStyle : String {
        //            case Dark, Light
        //
        //            init() {
        //                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
        //                self = InterfaceStyle(rawValue: type)!
        //            }
        //        }
        //
        //        let currentStyle = InterfaceStyle()
        //
        //        //print("currentStyle: \"\(currentStyle)\"  -  CurrentAppearance: \"\(CurrentAppearance)\" ")
        //
        //        if ("\(currentStyle)" != CurrentAppearanceNewUserViewController) {
        //            appearanceCheck()
        //        }
        
        appearanceCheck()
        
        super.viewDidLayout()
        
        
        
        
    }// end of viewDidLayout()
    
    func appearanceCheck() {
        
        enum InterfaceStyle : String {
            case Dark, Light
            
            init() {
                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
                self = InterfaceStyle(rawValue: type)!
            }
        }
        
        let currentStyle = InterfaceStyle()
        
        //rint("currentStyle: \"\(currentStyle)\" CurrentAppearanceUserPreferencesViewController: \"\(CurrentAppearanceSharedWindowViewController)\" ")
        
        if ("\(currentStyle)" != CurrentAppearanceSharedWindowViewController) {
            
            if (currentStyle.rawValue == "Dark") {
                print("Dark Style Detected - NewUserViewController")
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColorDarkMode
                
                ShareWindowCloseButton.styleButtonText(button: ShareWindowCloseButton, buttonName: "Close", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                ShareWindowCloseButton.wantsLayer = true
                ShareWindowCloseButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                ShareWindowCloseButton.layer?.cornerRadius = 7
                
                SharedWindowOpenButton.styleButtonText(button: SharedWindowOpenButton, buttonName: "Open", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                SharedWindowOpenButton.wantsLayer = true
                SharedWindowOpenButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                SharedWindowOpenButton.layer?.cornerRadius = 7
                
                
                
            } else if (currentStyle.rawValue == "Light") {
                print("Light Style Detected - NewUserViewController")
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColor.cgColor
                
                ShareWindowCloseButton.styleButtonText(button: ShareWindowCloseButton, buttonName: "Close", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                ShareWindowCloseButton.wantsLayer = true
                ShareWindowCloseButton.layer?.backgroundColor = NSColor.gray.cgColor
                ShareWindowCloseButton.layer?.cornerRadius = 7
                
                SharedWindowOpenButton.styleButtonText(button: SharedWindowOpenButton, buttonName: "Open", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                SharedWindowOpenButton.wantsLayer = true
                SharedWindowOpenButton.layer?.backgroundColor = NSColor.gray.cgColor
                SharedWindowOpenButton.layer?.cornerRadius = 7
                
            }
        }
        
        CurrentAppearanceSharedWindowViewController = "\(currentStyle)"
        
        
    }//end of appearanceCheck()
    
    
    
    func PopulateCategoryPopupButton() {
        
        SharedCategoryPopupButton.removeAllItems()
        let PopupOptions: [String] = ["Select a Category","Stories","Characters","Story Ideas","Locations"]
        SharedCategoryPopupButton.addItems(withTitles: PopupOptions)
        SharedCategoryPopupButton.selectItem(withTitle: "\(PopupOptions[0])") // "Select a Category"
        
    }
    
    func UpdateColumnHeaderCO() {
        
        let columnInt = SharedCategoryTableView.column(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "coedit"))
        let columns = SharedCategoryTableView.tableColumns
        
        let columnNeeded = columns[columnInt]
        
        //print("columnNeeded: \"\(columnNeeded)\"")
        
        columnNeeded.headerCell.title = "CO"
        
        SharedCategoryTableView.headerView?.display()
        
    }
    
    func UpdateColumnHeaderEdit() {
        
        let columnInt = SharedCategoryTableView.column(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "coedit"))
        let columns = SharedCategoryTableView.tableColumns
        
        let columnNeeded = columns[columnInt]
        
        //print("columnNeeded: \"\(columnNeeded)\"")
        
        columnNeeded.headerCell.title = "Edit"
        
        SharedCategoryTableView.headerView?.display()
        
    }
    
    func HideCategoryColumn() {
        
        let columnInt = SharedCategoryTableView.column(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "category"))
        let columns = SharedCategoryTableView.tableColumns
        
        let columnNeeded = columns[columnInt]
        
        //print("columnNeeded: \"\(columnNeeded)\"")
        
        columnNeeded.isHidden = true
        
        SharedCategoryTableView.headerView?.display()
        
    }
    
    func ShowCategoryColumn() {
        
        let columnInt = SharedCategoryTableView.column(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "category"))
        let columns = SharedCategoryTableView.tableColumns
        
        let columnNeeded = columns[columnInt]
        
        //print("columnNeeded: \"\(columnNeeded)\"")
        
        columnNeeded.isHidden = false
        
        SharedCategoryTableView.headerView?.display()
        
        
    }
    
    
    @IBAction func SharedCategoryPopupButtonAction(_ sender: Any) {
        
        InformationListArray.removeAll()
        SharedCategoryTableView.reloadData()
        
        let CategoryPopupButtonSelection = SharedCategoryPopupButton.titleOfSelectedItem
        
        
        UpdateColumnHeaderEdit()
        HideCategoryColumn()
        
        if (CategoryPopupButtonSelection == "Select a Category") {
            print("CategoryPopupButtonSelection: Select a Category - was selected")
            return
            
        } else if (CategoryPopupButtonSelection == "Stories") {
            print("CategoryPopupButtonSelection: Stories - was selected")
            
            UpdateColumnHeaderCO()
            PopulateStories()
            
        } else if (CategoryPopupButtonSelection == "Characters") {
            print("CategoryPopupButtonSelection: Characters - was selected")
            
            PopulateCharacters()
            
        } else if (CategoryPopupButtonSelection == "Story Ideas") {
            print("CategoryPopupButtonSelection: Story Ideas - was selected")
            ShowCategoryColumn()
            PopulateStoryIdeas()
            
        } else if (CategoryPopupButtonSelection == "Locations") {
            print("CategoryPopupButtonSelection: Locations - was selected")
            
            PopulateLocations()
            
        } else {
            print("CategoryPopupButtonSelection: UNKNOWN: \"\(CategoryPopupButtonSelection!)\" - was selected")
            
            let alert = NSAlert()
            alert.messageText = "An UNKNOWN option was selected"
            alert.informativeText = "\"\(CategoryPopupButtonSelection!)\"\n\nPlease close the Shared window and try again. "
            alert.alertStyle = .warning
            alert.addButton(withTitle: "OK")
            
            
            if (CurrentAppearanceSharedWindowViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.window.titlebarAppearsTransparent = true
            
            let returnCode = alert.runModal()
            print("returnCode: \(returnCode)")
            
            if (returnCode.rawValue == 1000) {
                print("OK Pressed")
                
            }
            
            
        }
        
        
    }
    
    func PopulateStories() {
        
        
        
        
        
        let SelectStorySharingQuery = "select * from Storysharingtable where sharewithuserikeyreference = (select ikey from users where user = \"\(CurrentUserName!)\")"
        
        print("SelectStorySharingQuery: \(SelectStorySharingQuery)")
        
        var ikeyDB = String()
        var storyNameDB = String()
        var whosharedDB = String()
        var whosharedDBint = String()
        var shareDB = String()
        var editDB = String()
        
        
        do {
            
            let SelectStorySharingQueryRun = try db.executeQuery(SelectStorySharingQuery, values:nil)
            
            while SelectStorySharingQueryRun.next() {
                ikeyDB = SelectStorySharingQueryRun.string(forColumn: "ikey")!
                whosharedDBint = SelectStorySharingQueryRun.string(forColumn: "userikeyreference")!
                shareDB = SelectStorySharingQueryRun.string(forColumn: "storyshare")!
                editDB = SelectStorySharingQueryRun.string(forColumn: "storyedit")!
                
                print("SelectStorySharingQuery - ikeyDB: \"\(ikeyDB)\" whosharedDBint: \"\(whosharedDBint)\" shareDB: \"\(shareDB)\" editDB: \"\(editDB)\"")
                
                
                /////////////////////////////
                
                let StorySharedByWhomQuery = "select user from users where ikey = \"\(whosharedDBint)\""
                
                print("StorySharedByWhomQuery: \(StorySharedByWhomQuery)")
                
                do {
                    
                    let StorySharedByWhomQueryRun = try db.executeQuery(StorySharedByWhomQuery, values:nil)
                    
                    while StorySharedByWhomQueryRun.next() {
                        whosharedDB = StorySharedByWhomQueryRun.string(forColumn: "user")!
                    }
                    
                    /////////////////////////////
                    
                    let StoryNameSharedQuery = "select storyname from storytable where ikey = \"\(ikeyDB)\""
                    
                    print("StoryNameSharedQuery: \(StoryNameSharedQuery)")
                    
                    do {
                        
                        let StoryNameSharedQueryRun = try db.executeQuery(StoryNameSharedQuery, values:nil)
                        
                        while StoryNameSharedQueryRun.next() {
                            storyNameDB = StoryNameSharedQueryRun.string(forColumn: "storyname")!
                        }
                        
                        let InfoToAdd = SharedStoryInformation.init(Ikey: ikeyDB, Name: storyNameDB, Whoshared: whosharedDB, Category: "", Share: shareDB, Edit: editDB)
                        
                        InformationListArray.append(InfoToAdd)
                        
                    } catch {
                        
                        let ErrorMessage = "SharedWindowViewController:\(#function) - Failed to get Shared Story - Story Name Information\n\(db.lastErrorMessage())\n\(StoryNameSharedQuery))"
                        
                        print(ErrorMessage)
                        
                        loggly(LogType.Error, text: ErrorMessage)
                        
                        DoAlert.DisplayAlert(Class: "CharacterInfoViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to get Shared Story - Story Name Information\n\(db.lastErrorMessage())\n\n\(StoryNameSharedQuery)", DatabaseError: "\(db.lastErrorMessage())")
                        
                    }
                    
                    
                    
                } catch {
                    
                    let ErrorMessage = "SharedWindowViewController:\(#function) - Failed to get Shared Story - Shared by Whom Information\n\(db.lastErrorMessage())\n\(StorySharedByWhomQuery))"
                    
                    print(ErrorMessage)
                    
                    loggly(LogType.Error, text: ErrorMessage)
                    
                    DoAlert.DisplayAlert(Class: "CharacterInfoViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to get Shared Story - Shared by Whom Information\n\(db.lastErrorMessage())\n\n\(StorySharedByWhomQuery)", DatabaseError: "\(db.lastErrorMessage())")
                    
                }
                
                
                
                
                
            }
            
            
            

        } catch {
            
            let ErrorMessage = "SharedWindowViewController:\(#function) - Failed to get Shared Story Information\n\(db.lastErrorMessage())\n\(SelectStorySharingQuery))"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "CharacterInfoViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to get Shared Story Information\n\(db.lastErrorMessage())\n\n\(SelectStorySharingQuery)", DatabaseError: "\(db.lastErrorMessage())")
            
        }
        
        
        
        
        print("InformationListArray.count = \"\(InformationListArray.count)\"")
        
        
        
        SharedCategoryTableView.reloadData()
        
        
        
    } // end of PopulateStories()
    
    func PopulateCharacters() {
        
        let SelectCharacterSharingQuery = "select * from charactersharingtable where sharewithuserikeyreference = (select ikey from users where user = \"\(CurrentUserName!)\")"
        
        print("SelectCharacterSharingQuery: \(SelectCharacterSharingQuery)")
        
        var ikeyDB = String()
        var characterIkeyDB = String()
        var characterFirstNameDB = String()
        var characterLastNameDB = String()
        var whosharedDB = String()
        var whosharedDBint = String()
        var shareDB = String()
        var editDB = String()
        
        
        do {
            
            let SelectCharacterSharingQueryRun = try db.executeQuery(SelectCharacterSharingQuery, values:nil)
            
            while SelectCharacterSharingQueryRun.next() {
                
                ikeyDB = SelectCharacterSharingQueryRun.string(forColumn: "ikey")!
                characterIkeyDB = SelectCharacterSharingQueryRun.string(forColumn: "characterikey")!
                whosharedDBint = SelectCharacterSharingQueryRun.string(forColumn: "userikeyreference")!
                shareDB = SelectCharacterSharingQueryRun.string(forColumn: "charactershare")!
                editDB = SelectCharacterSharingQueryRun.string(forColumn: "characteredit")!

                
                let CharacterSharedByWhomQuery = "select user from users where ikey = \"\(whosharedDBint)\""
                
                print("CharacterSharedByWhomQuery: \(CharacterSharedByWhomQuery)")
                
                do {
                    
                    let CharacterSharedByWhomQueryRun = try db.executeQuery(CharacterSharedByWhomQuery, values:nil)
                    
                    while CharacterSharedByWhomQueryRun.next() {
                        whosharedDB = CharacterSharedByWhomQueryRun.string(forColumn: "user")!
                    }
                    
                    let CharacterNameQuery = "select firstname, lastname from charactertable where ikey = \"\(characterIkeyDB)\""
                    
                    print("CharacterNameQuery: \(CharacterNameQuery)")
                    
                    do {
                        
                        let CharacterNameQueryRun = try db.executeQuery(CharacterNameQuery, values:nil)
                        
                        
                        
                        while CharacterNameQueryRun.next() {
                            characterFirstNameDB = CharacterNameQueryRun.string(forColumn: "firstname")!
                            characterLastNameDB = CharacterNameQueryRun.string(forColumn: "lastname")!
                        }
                        
                        let fullName = String("\(characterFirstNameDB) \(characterLastNameDB)")
                        
                        
                        let InfoToAdd = SharedStoryInformation.init(Ikey: ikeyDB, Name: fullName, Whoshared: whosharedDB, Category: "", Share: shareDB, Edit: editDB)
                        
                        InformationListArray.append(InfoToAdd)
                        
                        
                        
                    } catch {
                        
                        let ErrorMessage = "SharedWindowViewController:\(#function) - Failed to get Shared Character Name - Shared by Whom Information\n\(db.lastErrorMessage())\n\(CharacterNameQuery))"
                        
                        print(ErrorMessage)
                        
                        loggly(LogType.Error, text: ErrorMessage)
                        
                        DoAlert.DisplayAlert(Class: "CharacterInfoViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to get Shared Character Name  - Shared by Whom Information\n\(db.lastErrorMessage())\n\n\(CharacterNameQuery)", DatabaseError: "\(db.lastErrorMessage())")
                        
                    }
                    
                    
                } catch {
                    
                    let ErrorMessage = "SharedWindowViewController:\(#function) - Failed to get Shared Character Name - Shared by Whom Information\n\(db.lastErrorMessage())\n\(CharacterSharedByWhomQuery))"
                    
                    print(ErrorMessage)
                    
                    loggly(LogType.Error, text: ErrorMessage)
                    
                    DoAlert.DisplayAlert(Class: "CharacterInfoViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to get Shared Character Name  - Shared by Whom Information\n\(db.lastErrorMessage())\n\n\(CharacterSharedByWhomQuery)", DatabaseError: "\(db.lastErrorMessage())")
                    
                }
                
                
            }
            
            

            
        } catch {
            
            let ErrorMessage = "SharedWindowViewController:\(#function) - Failed to get Shared Character Information\n\(db.lastErrorMessage())\n\(SelectCharacterSharingQuery))"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "CharacterInfoViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to get Shared Character Information\n\(db.lastErrorMessage())\n\n\(SelectCharacterSharingQuery)", DatabaseError: "\(db.lastErrorMessage())")
            
        }
        
        
        
        SharedCategoryTableView.reloadData()
        
            
            
        
    }// end of PopulateCharacters()
    
    func PopulateStoryIdeas() {
        
        
        let StoryIdeasQuery = "select * from storyideasharingtable where sharewithuserikeyreference = (select ikey from users where user = \"\(CurrentUserName!)\")"
        
        print("StoryIdeasQuery: \(StoryIdeasQuery)")
        
        var ikeyDB = ""
        var storyideaIkeyDB = ""
        var storyIdeacategoryDBint = ""
        var storyIdeacategoryDB = ""
        var storyIdeaNameDB = ""
        var whosharedDB = ""
        var whosharedDBint = ""
        var shareDB = ""
        var editDB = ""
        
        
        
        do {
            
            let StoryIdeasQueryRun = try db.executeQuery(StoryIdeasQuery, values:nil)

            while StoryIdeasQueryRun.next() {
                ikeyDB = StoryIdeasQueryRun.string(forColumn: "ikey")!
                storyideaIkeyDB = StoryIdeasQueryRun.string(forColumn: "storyideaikey")!
                whosharedDBint = StoryIdeasQueryRun.string(forColumn: "userikeyreference")!
                shareDB = StoryIdeasQueryRun.string(forColumn: "storyideashare")!
                editDB = StoryIdeasQueryRun.string(forColumn: "storyideaedit")!
            
           
            let StoryIdeasSharedByWhomQuery = "select user from users where ikey = \"\(whosharedDBint)\""
            
            print("StoryIdeasSharedByWhomQuery: \(StoryIdeasSharedByWhomQuery)")
            
            do {
                
                let StoryIdeasSharedByWhomQueryRun = try db.executeQuery(StoryIdeasSharedByWhomQuery, values:nil)
                
                while StoryIdeasSharedByWhomQueryRun.next() {
                    whosharedDB = StoryIdeasSharedByWhomQueryRun.string(forColumn: "user")!
                }
                
              
                
                let StoryIdeasSharedNameCategoryQuery = "select name,categoryikey from storyideastable where ikey = \"\(storyideaIkeyDB)\" "
                
                print("StoryIdeasSharedNameCategoryQuery: \(StoryIdeasSharedNameCategoryQuery)")
                
                do {
                    
                    let StoryIdeasSharedNameCategoryQueryRun = try db.executeQuery(StoryIdeasSharedNameCategoryQuery, values:nil)
                    
                    while StoryIdeasSharedNameCategoryQueryRun.next() {
                        storyIdeaNameDB = StoryIdeasSharedNameCategoryQueryRun.string(forColumn: "name")!
                        storyIdeacategoryDBint = StoryIdeasSharedNameCategoryQueryRun.string(forColumn: "categoryikey")!
                   
                    }
                    
                    if (storyIdeacategoryDBint == "1") {
                        storyIdeacategoryDB = "Arcs"
                    } else if (storyIdeacategoryDBint == "2") {
                        storyIdeacategoryDB = "Starts"
                    } else if (storyIdeacategoryDBint == "3") {
                        storyIdeacategoryDB = "Mids"
                    } else if (storyIdeacategoryDBint == "4") {
                        storyIdeacategoryDB = "Endings"
                    } else if (storyIdeacategoryDBint == "5") {
                        storyIdeacategoryDB = "Scenes"
                    } else {
                        storyIdeacategoryDB = "Uknown"
                    }
                    

                    
                    let InfoToAdd = SharedStoryInformation.init(Ikey: storyideaIkeyDB, Name: storyIdeaNameDB, Whoshared: whosharedDB, Category: storyIdeacategoryDB, Share: shareDB, Edit: editDB)
                    
                    InformationListArray.append(InfoToAdd)
                    
                    
                    
                } catch {
                    
                    let ErrorMessage = "SharedWindowViewController:\(#function) - Failed to get Shared StoryIdeas - Name & Category Information\n\(db.lastErrorMessage())\n\(StoryIdeasSharedByWhomQuery))"
                    
                    print(ErrorMessage)
                    
                    loggly(LogType.Error, text: ErrorMessage)
                    
                    DoAlert.DisplayAlert(Class: "CharacterInfoViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to get Shared StoryIdeas - Name & Category Information\n\(db.lastErrorMessage())\n\n\(StoryIdeasSharedByWhomQuery)", DatabaseError: "\(db.lastErrorMessage())")
                    
                }
                
            
                
            } catch {
                
                let ErrorMessage = "SharedWindowViewController:\(#function) - Failed to get Shared StoryIdeas - Shared by Whom Information\n\(db.lastErrorMessage())\n\(StoryIdeasSharedByWhomQuery))"
                
                print(ErrorMessage)
                
                loggly(LogType.Error, text: ErrorMessage)
                
                DoAlert.DisplayAlert(Class: "CharacterInfoViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to get Shared StoryIdeas Information\n\(db.lastErrorMessage())\n\n\(StoryIdeasSharedByWhomQuery)", DatabaseError: "\(db.lastErrorMessage())")
                
            }
                
            } // end of next
            
            
        } catch {
            
            let ErrorMessage = "SharedWindowViewController:\(#function) - Failed to get Shared StoryIdeas Information\n\(db.lastErrorMessage())\n\(StoryIdeasQuery))"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "CharacterInfoViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to get Shared StoryIdeas Information\n\(db.lastErrorMessage())\n\n\(StoryIdeasQuery)", DatabaseError: "\(db.lastErrorMessage())")
            
        }
        
        
        SharedCategoryTableView.reloadData()
        
    }  // end of PopulateStoryIdeas()
    
    func PopulateLocations() {
        
      
        
        let locationtableQuery = "select * from locationsharingtable where sharewithuserikeyreference = (select ikey from users where user = \"\(CurrentUserName!)\")"
        
        print("locationtableQuery: \(locationtableQuery)")
        
        var ikeyDB = String()
        var locationNameDB = String()
        var locationIkeyDB = String()
        var whosharedDB = String()
        var whosharedDBint = String()
        var shareDB = String()
        var editDB = String()
        
        
        
        do {
            
            let SlocationtableQueryRun = try db.executeQuery(locationtableQuery, values:nil)
            
            while SlocationtableQueryRun.next() {
                
                ikeyDB = SlocationtableQueryRun.string(forColumn: "ikey")!
                locationIkeyDB = SlocationtableQueryRun.string(forColumn: "locationikey")!
                whosharedDBint = SlocationtableQueryRun.string(forColumn: "userikeyreference")!
                shareDB = SlocationtableQueryRun.string(forColumn: "locationshare")!
                editDB = SlocationtableQueryRun.string(forColumn: "locationedit")!
 
                
                ///////////////////
                
                let LocationWhoSharedQuery = "select user from users where ikey = \"\(whosharedDBint)\""
                
                print("LocationWhoSharedQuery: \(LocationWhoSharedQuery)")
                
                
                do {
                    
                    let LocationWhoSharedQueryRun = try db.executeQuery(LocationWhoSharedQuery, values:nil)
                    
                    while LocationWhoSharedQueryRun.next() {
                        
                        whosharedDB = LocationWhoSharedQueryRun.string(forColumn: "user")!
                        
                        let LocationNameSharedQuery = "select * from locationtable where ikey = \"\(locationIkeyDB)\""
                        
                        print("LocationNameSharedQuery: \(LocationNameSharedQuery)")
                        
                        do {
                            
                            let LocationNameSharedQueryRun = try db.executeQuery(LocationNameSharedQuery, values:nil)
                            
                            while LocationNameSharedQueryRun.next() {
                                
                                locationNameDB = LocationNameSharedQueryRun.string(forColumn: "name")!
                                
                            }
                            
  
                            print("Ikey: \"\(locationIkeyDB)\" Name: \"\(locationNameDB)\" WhoShared: \"\(whosharedDB)\" Share: \"\(shareDB)\" Edit: \"\(editDB)\"")
                            
                            
                            let InfoToAdd = SharedStoryInformation.init(Ikey: locationIkeyDB, Name: locationNameDB, Whoshared: whosharedDB, Category: "", Share: shareDB, Edit: editDB)
                            
                            InformationListArray.append(InfoToAdd)
                                
                                
                            
                        } catch {
                            
                            let ErrorMessage = "SharedWindowViewController:\(#function) - Failed to get Location Information\n\(db.lastErrorMessage())\n\(LocationNameSharedQuery))"
                            
                            print(ErrorMessage)
                            
                            loggly(LogType.Error, text: ErrorMessage)
                            
                            DoAlert.DisplayAlert(Class: "CharacterInfoViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to get Location Information\n\(LocationNameSharedQuery)", DatabaseError: "\(db.lastErrorMessage())")
                            
                        }
                        
                        
                    }
                
                } catch {
                    
                    let ErrorMessage = "SharedWindowViewController:\(#function) - Failed to get Location Information\n\(db.lastErrorMessage())\n\(LocationWhoSharedQuery))"
                    
                    print(ErrorMessage)
                    
                    loggly(LogType.Error, text: ErrorMessage)
                    
                    DoAlert.DisplayAlert(Class: "CharacterInfoViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to get Location Information\n\(LocationWhoSharedQuery)", DatabaseError: "\(db.lastErrorMessage())")
                    
                }
                
                
            }

 
            
        } catch {
            
            let ErrorMessage = "SharedWindowViewController:\(#function) - Failed to get Location Information\n\(db.lastErrorMessage())\n\(locationtableQuery))"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "CharacterInfoViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to get Location Information\n\(locationtableQuery)", DatabaseError: "\(db.lastErrorMessage())")
            
        }
        
        
        
        
        SharedCategoryTableView.reloadData()
        
        
        
        
    } // end of PopulateLocations()
    
    
    
    //////////////////////////////////////////////////////////
    
    
    func numberOfRows(in tableVieww: NSTableView) -> Int {
        
        print("InformationListArray.count: \(InformationListArray.count)")
        
        return InformationListArray.count
        
    }
    
//    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {

    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
      
    
        guard let cell = tableView.makeView(withIdentifier: tableColumn!.identifier, owner: self) as? NSTableCellView else
        {
            print("Table Returned Nil")
            
            return nil
        }
        
        
        if (tableColumn?.identifier)!.rawValue == "ikey" {
            cell.textField?.stringValue = InformationListArray[row].Ikey
        }
        if (tableColumn?.identifier)!.rawValue == "whoshared" {
            cell.textField?.stringValue = InformationListArray[row].Whoshared
        }
        if (tableColumn?.identifier)!.rawValue == "category" {
            cell.textField?.stringValue = InformationListArray[row].Category
        }
        if (tableColumn?.identifier)!.rawValue == "name" {
            cell.textField?.stringValue = InformationListArray[row].Name
        }
        if (tableColumn?.identifier)!.rawValue == "shared" {
            cell.textField?.stringValue = InformationListArray[row].Share
        }
        if (tableColumn?.identifier)!.rawValue == "coedit" {
            cell.textField?.stringValue = InformationListArray[row].Edit
        }
        
        
        return cell
        
    }
    
    func tableViewSelectionDidChange(_ notification: Notification) {

        let SelectedRow = SharedCategoryTableView.selectedRow

        print("SelectedRow: \(SelectedRow)")

        if (SelectedRow == -1) {
            SharedWindowOpenButton.isEnabled = false
            return
        }

        
        let Ikey = InformationListArray[SelectedRow].Ikey
        let Name = InformationListArray[SelectedRow].Name

        

        let isIndexValid = InformationListArray.indices.contains(SelectedRow)

        if isIndexValid {
            print("Table Selection - Ikey: \"\(Ikey)\" Name: \"\(Name)\" ")
            SharedWindowOpenButton.isEnabled = true
        } else {
            SharedWindowOpenButton.isEnabled = false
        }


    }
    
    
    
    
    
    ///////////////////////////////////////////////////////////
    
    @IBAction func SharedWindowOpenButtonAction(_ sender: Any) {
    
        print("SharedWindowOpenButtonAction Pressed")
        
//        var SharedItemIkey = String()
//        var SharedItemName = String()
//        var SharedItemCategory = String()
//        var SharedItemOwner = String()
        
        let SelectedRow = SharedCategoryTableView.selectedRow

        let SelectedCategory = SharedCategoryPopupButton.titleOfSelectedItem
        
        if (SelectedCategory == "Select a Category") {
            return
        } else if (SelectedCategory == "Stories") {
            print("Open Stories")
            
            let Ikey = InformationListArray[SelectedRow].Ikey
            let Name = InformationListArray[SelectedRow].Name
            let Owner = InformationListArray[SelectedRow].Whoshared
            let CO = InformationListArray[SelectedRow].Edit
            let Share = InformationListArray[SelectedRow].Share
            
            
            //////////////////////
            
            let UserInfoQuery = "select ikey from users where user = \"\(Owner)\" "
            
            print("UserInfoQuery:\n\(UserInfoQuery)")
            
            var UserIkeyDB = String()
            
            do {
                
                let UserInfoQueryRun = try db.executeQuery(UserInfoQuery, values:nil)
                
                while UserInfoQueryRun.next() {
                    UserIkeyDB = UserInfoQueryRun.string(forColumn: "ikey")!
                }
            } catch {
                
                print("ERROR: SharedWindowViewController: Failed to get User Info \n \"\(UserInfoQuery)\"\n\"\(db.lastErrorMessage())\"")
                
                let ErrorMessage = "ERROR: SharedWindowViewController: Failed to get User Info \n \"\(UserInfoQuery)\"\n\"\(db.lastErrorMessage())\""
                
                print(ErrorMessage)
                
                loggly(LogType.Error, text: ErrorMessage)
                
                DoAlert.DisplayAlert(Class: "SharedWindowViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "SharedWindowViewController: Failed to get User Info \n \"\(UserInfoQuery)\"", DatabaseError: "\(db.lastErrorMessage())")
                
                return
            }
            
            ///////////////////////
            
            if (Share == "No") {
                // Alert - Unable to open Character due to it not shared
                
                
                let alert = NSAlert()
                
                alert.messageText = "Warning you do not have permissions to open \"\(Name)\" "
                alert.informativeText = "Please select another Shared item"
                alert.alertStyle = .warning
                alert.addButton(withTitle: "OK")
                
                
                if (CurrentAppearanceSharedWindowViewController == "Dark") {
                    alert.window.backgroundColor = AlertBackgroundColorDarkMode
                } else {
                    alert.window.backgroundColor = WindowBackgroundColor
                }
                
                alert.window.titlebarAppearsTransparent = true
                
                let returnCode = alert.runModal()
                print("returnCode: \(returnCode)")
                
                if (returnCode.rawValue == 1000) {
                    print("OK Pressed")
                    return
                }
                
                
                
            } else if (Share == "Yes" && CO == "No") {
                SharedItemLock = true
            } else if (CO == "Yes") {
                SharedItemLock = false
            } else {
                
                // alert here for unknow options found
                print("Unknown  options!!")
                
                let alert = NSAlert()
                alert.messageText = "Warning"
                alert.informativeText = "An unknown option was detected, please reselect a Shared item and try again. "
                alert.alertStyle = .warning
                alert.addButton(withTitle: "OK")
                
                
                if (CurrentAppearanceSharedWindowViewController == "Dark") {
                    alert.window.backgroundColor = AlertBackgroundColorDarkMode
                } else {
                    alert.window.backgroundColor = WindowBackgroundColor
                }
                
                alert.window.titlebarAppearsTransparent = true
                
                let returnCode = alert.runModal()
                print("returnCode: \(returnCode)")
                
                if (returnCode.rawValue == 1000) {
                    print("OK Pressed")
                    
                }
                
                return
            }
            
            
            ////////////////////
            
            StoryNameOpen = Name
            StoryIkeyOpen = Ikey
            
            SharedItemOwner = UserIkeyDB
            
            StoryWindow.showWindow(self)
            
            
            
            
            
            
            
            
            
        } else if (SelectedCategory == "Characters") {
            print("Open Characters")
            
            let Ikey = InformationListArray[SelectedRow].Ikey
            let Name = InformationListArray[SelectedRow].Name
            let Owner = InformationListArray[SelectedRow].Whoshared
            let Edit = InformationListArray[SelectedRow].Edit
            let Share = InformationListArray[SelectedRow].Share
            
            let NameArray = Name.components(separatedBy: " ")
            
            //////////////////////

            let UserInfoQuery = "select ikey from users where user = \"\(Owner)\" "
            
            print("UserInfoQuery:\n\(UserInfoQuery)")
            
            var UserIkeyDB = String()

            do {
                
                let UserInfoQueryRun = try db.executeQuery(UserInfoQuery, values:nil)
                
                while UserInfoQueryRun.next() {
                    UserIkeyDB = UserInfoQueryRun.string(forColumn: "ikey")!
                }
            } catch {
                
                print("ERROR: SharedWindowViewController: Failed to get User Info \n \"\(UserInfoQuery)\"\n\"\(db.lastErrorMessage())\"")
                
                let ErrorMessage = "ERROR: SharedWindowViewController: Failed to get User Info \n \"\(UserInfoQuery)\"\n\"\(db.lastErrorMessage())\""
                
                print(ErrorMessage)
                
                loggly(LogType.Error, text: ErrorMessage)
                
                DoAlert.DisplayAlert(Class: "SharedWindowViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "SharedWindowViewController: Failed to get User Info \n \"\(UserInfoQuery)\"", DatabaseError: "\(db.lastErrorMessage())")
                
                return
            }
            
            ///////////////////////
            
            print("Ikey: \"\(Ikey)\" Name: \"\(NameArray[0])\" Owner: \"\(Owner)\" OwnerIkey: \"\(UserIkeyDB)\"")
            
            CVCIkey = Ikey
            CVCName = NameArray[0]
            
            SharedItemIkey = Ikey
            SharedItemName = NameArray[0]
            SharedItemOwner = UserIkeyDB
            
            ///////
            
            if (Share == "No") {
                // Alert - Unable to open Character due to it not shared
                

                let alert = NSAlert()

                alert.messageText = "Warning you do not have permissions to open \"\(Name)\" "
                alert.informativeText = "Please select another Shared item"
                alert.alertStyle = .warning
                alert.addButton(withTitle: "OK")
                
                
                if (CurrentAppearanceSharedWindowViewController == "Dark") {
                    alert.window.backgroundColor = AlertBackgroundColorDarkMode
                } else {
                    alert.window.backgroundColor = WindowBackgroundColor
                }
                
                alert.window.titlebarAppearsTransparent = true
                
                let returnCode = alert.runModal()
                print("returnCode: \(returnCode)")
                
                if (returnCode.rawValue == 1000) {
                    print("OK Pressed")
                    return
                }
                
                
                
            } else if (Share == "Yes" && Edit == "No") {
                SharedItemLock = true
            } else if (Edit == "Yes") {
                SharedItemLock = false
            } else {
                
                // alert here for unknow options found
                print("Unknow options!!")
                
                let alert = NSAlert()
                alert.messageText = "Warning"
                alert.informativeText = "An unknown option was detected, please reselect a Shared item and try again. "
                alert.alertStyle = .warning
                alert.addButton(withTitle: "OK")
                
               
                if (CurrentAppearanceSharedWindowViewController == "Dark") {
                    alert.window.backgroundColor = AlertBackgroundColorDarkMode
                } else {
                    alert.window.backgroundColor = WindowBackgroundColor
                }
                
                alert.window.titlebarAppearsTransparent = true
                
                let returnCode = alert.runModal()
                print("returnCode: \(returnCode)")
                
                if (returnCode.rawValue == 1000) {
                    print("OK Pressed")
                    
                }
                
                return
            }
            
            
            ///////
            
            
            CharacterInfoWindow.showWindow(self)
            
        } else if (SelectedCategory == "Story Ideas") {
            print("Open Story Ideas")
            
            let Ikey = InformationListArray[SelectedRow].Ikey
            let Name = InformationListArray[SelectedRow].Name
            let Category = InformationListArray[SelectedRow].Category
            let Owner = InformationListArray[SelectedRow].Whoshared
            let Edit = InformationListArray[SelectedRow].Edit
            let Share = InformationListArray[SelectedRow].Share
        
            
            //////////////////////
            
            let UserInfoQuery = "select ikey from users where user = \"\(Owner)\" "
            
            print("UserInfoQuery:\n\(UserInfoQuery)")
            
            var UserIkeyDB = String()
            
            do {
                
                let UserInfoQueryRun = try db.executeQuery(UserInfoQuery, values:nil)
                
                while UserInfoQueryRun.next() {
                    UserIkeyDB = UserInfoQueryRun.string(forColumn: "ikey")!
                }
            } catch {
                
                print("ERROR: SharedWindowViewController: Failed to get User Info \n \"\(UserInfoQuery)\"\n\"\(db.lastErrorMessage())\"")
                
                let ErrorMessage = "ERROR: SharedWindowViewController: Failed to get User Info \n \"\(UserInfoQuery)\"\n\"\(db.lastErrorMessage())\""
                
                print(ErrorMessage)
                
                loggly(LogType.Error, text: ErrorMessage)
                
                DoAlert.DisplayAlert(Class: "SharedWindowViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "SharedWindowViewController: Failed to get User Info \n \"\(UserInfoQuery)\"", DatabaseError: "\(db.lastErrorMessage())")
                
                return
            }
            
            ///////////////////////
            
            if (Share == "No") {
                // Alert - Unable to open Character due to it not shared
                
                
                let alert = NSAlert()
                
                alert.messageText = "Warning you do not have permissions to open \"\(Name)\" "
                alert.informativeText = "Please select another Shared item"
                alert.alertStyle = .warning
                alert.addButton(withTitle: "OK")
                
                
                if (CurrentAppearanceSharedWindowViewController == "Dark") {
                    alert.window.backgroundColor = AlertBackgroundColorDarkMode
                } else {
                    alert.window.backgroundColor = WindowBackgroundColor
                }
                
                alert.window.titlebarAppearsTransparent = true
                
                let returnCode = alert.runModal()
                print("returnCode: \(returnCode)")
                
                if (returnCode.rawValue == 1000) {
                    print("OK Pressed")
                    return
                }
                
                
                
            } else if (Share == "Yes" && Edit == "No") {
                SharedItemLock = true
            } else if (Edit == "Yes") {
                SharedItemLock = false
            } else {
                
                // alert here for unknow options found
                print("Unknow options!!")
                
                let alert = NSAlert()
                alert.messageText = "Warning"
                alert.informativeText = "An unknown option was detected, please reselect a Shared item and try again. "
                alert.alertStyle = .warning
                alert.addButton(withTitle: "OK")
                
                
                if (CurrentAppearanceSharedWindowViewController == "Dark") {
                    alert.window.backgroundColor = AlertBackgroundColorDarkMode
                } else {
                    alert.window.backgroundColor = WindowBackgroundColor
                }
                
                alert.window.titlebarAppearsTransparent = true
                
                let returnCode = alert.runModal()
                print("returnCode: \(returnCode)")
                
                if (returnCode.rawValue == 1000) {
                    print("OK Pressed")
                    
                }
                
                return
            }
            
            
            ////////////////////
            
            StoryIdeaIkey = Ikey
            StoryIdeaName = Name
            StoryIdeaCategory = Category
            SharedItemOwner = UserIkeyDB
            
            StoryIdeasWindow.showWindow(self)
            

        } else if (SelectedCategory == "Locations") {
            print("Open Locations")
            
            let Ikey = InformationListArray[SelectedRow].Ikey
            let Name = InformationListArray[SelectedRow].Name
            let Owner = InformationListArray[SelectedRow].Whoshared
            let Edit = InformationListArray[SelectedRow].Edit
            let Share = InformationListArray[SelectedRow].Share
            
            //////////////////////
            
            let UserInfoQuery = "select ikey from users where user = \"\(Owner)\" "
            
            print("UserInfoQuery:\n\(UserInfoQuery)")
            
            var UserIkeyDB = String()
            
            do {
                
                let UserInfoQueryRun = try db.executeQuery(UserInfoQuery, values:nil)
                
                while UserInfoQueryRun.next() {
                    UserIkeyDB = UserInfoQueryRun.string(forColumn: "ikey")!
                }
            } catch {
                
                print("ERROR: SharedWindowViewController: Failed to get User Info \n \"\(UserInfoQuery)\"\n\"\(db.lastErrorMessage())\"")
                
                let ErrorMessage = "ERROR: SharedWindowViewController: Failed to get User Info \n \"\(UserInfoQuery)\"\n\"\(db.lastErrorMessage())\""
                
                print(ErrorMessage)
                
                loggly(LogType.Error, text: ErrorMessage)
                
                DoAlert.DisplayAlert(Class: "SharedWindowViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "SharedWindowViewController: Failed to get User Info \n \"\(UserInfoQuery)\"", DatabaseError: "\(db.lastErrorMessage())")
                
                return
            }
            
            ///////////////////////
            
            if (Share == "No") {
                // Alert - Unable to open Character due to it not shared
                
                
                let alert = NSAlert()
                
                alert.messageText = "Warning you do not have permissions to open \"\(Name)\" "
                alert.informativeText = "Please select another Shared item"
                alert.alertStyle = .warning
                alert.addButton(withTitle: "OK")
                
               
                if (CurrentAppearanceSharedWindowViewController == "Dark") {
                    alert.window.backgroundColor = AlertBackgroundColorDarkMode
                } else {
                    alert.window.backgroundColor = WindowBackgroundColor
                }
                
                alert.window.titlebarAppearsTransparent = true
                
                let returnCode = alert.runModal()
                print("returnCode: \(returnCode)")
                
                if (returnCode.rawValue == 1000) {
                    print("OK Pressed")
                    return
                }
                
                
                
            } else if (Share == "Yes" && Edit == "No") {
                SharedItemLock = true
            } else if (Edit == "Yes") {
                SharedItemLock = false
            } else {
                
                // alert here for unknow options found
                print("Unknow options!!")
                
                let alert = NSAlert()
                alert.messageText = "Warning"
                alert.informativeText = "An unknown option was detected, please reselect a Shared item and try again. "
                alert.alertStyle = .warning
                alert.addButton(withTitle: "OK")
                
                
                if (CurrentAppearanceSharedWindowViewController == "Dark") {
                    alert.window.backgroundColor = AlertBackgroundColorDarkMode
                } else {
                    alert.window.backgroundColor = WindowBackgroundColor
                }
                
                alert.window.titlebarAppearsTransparent = true
                
                let returnCode = alert.runModal()
                print("returnCode: \(returnCode)")
                
                if (returnCode.rawValue == 1000) {
                    print("OK Pressed")
                    
                }
                
                return
            }
            
            
            ////////////////////
            
            LocationIkey = Ikey
            LocationName = Name
            SharedItemOwner = UserIkeyDB
            
            print("LocationIkey: \"\(LocationIkey)\" Name: \"\(Name)\" SharedItemOwner: \"\(SharedItemOwner)\"")
            
            LocationInfoWindow.showWindow(self)
            
            
        } else {
            print("UNKNOWN OPTION DETECTED")
        }
        
        
        
       
        
        
        
        
        
        
        
        
        
        
        
    }
    

    
    @IBAction func ShareWindowCloseButtonAction(_ sender: Any) {
        print("ShareWindowCloseButtonAction Pressed")
        InformationListArray.removeAll()
        SharedCategoryTableView.reloadData()
        self.view.window?.close()
    }
    
    
    
    
}  // end of SharedWindowViewController
