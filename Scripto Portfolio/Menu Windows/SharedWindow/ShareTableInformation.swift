//
//  ShareTableInformation.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 4/24/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Foundation

class SharedStoryInformation: NSObject {
    
    var Ikey = String()
    var Name = String()
    var Whoshared = String()
    var Category = String()
    var Share = String()
    var Edit = String()
    
    init(Ikey: String, Name: String, Whoshared: String,Category: String, Share: String, Edit: String) {
        
        self.Ikey = Ikey
        self.Name = Name
        self.Whoshared = Whoshared
        self.Category = Category
        self.Share = Share
        self.Edit = Edit
        
        
    }
    
}
