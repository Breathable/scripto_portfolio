final class NodeFactory {
    func nodes() -> [Node] {
        return [
            
            .init(value: "4.0.3 (Build 16)", children: [
                .init(value: "NEW FEATURE - Verbosity in the Story Window, you can lookup words and thier definitions. More to come."),
                .init(value: "REDESIGN - You shouldn't get as many 'Theres already a story open' warnings. "),
                .init(value: "REDESIGN - Wanted Features window has a new look"),
                .init(value: "FIXED - Story History button not working nor being the correct color"),
                .init(value: "FIXED - Out of memory error when using the drop down in the Story Ideas window."),
                .init(value: "FIXED - Hid some columns in the Story Ideas Association window that are for debugging purposes."),
                .init(value: "FIXED - After closing and reopening the Wanted Feature window it would not be the correct color."),
                .init(value: "FIXED - CRASH when selecting 'Select a Subcategory' with the Subcategory dropdown in the Item info window."),
            ]),
            .init(value: "4.0.1 (Build 14)", children: [
                .init(value: "Took out a Switch in the Story Goals window per compatibility issues."),
            ]),
            .init(value: "3.7.1 (Build 12)", children: [
                .init(value: "ADDED - Story Ideas now can be deleted"),
                .init(value: "FIXED - Chracter Image name button location and image/icon."),
                .init(value: "FIXED - Unable to log back into App from Lock window"),
                .init(value: "FIXED - Other Character information would not save under specific steps."),
                .init(value: "FIXED - Printing while not in the Story Window."),
            ]),
            .init(value: "3.7.0 (Build 11)", children: [
                .init(value: "NEW FEATURE - Simple Story History"),
                .init(value: "NEW FEATURE - Simple Story Goals"),
            ]),
            .init(value: "3.6.0 (Build 10)", children: [
                .init(value: "IMPROVEMENT - Password security"),
                .init(value: "IMPROVEMENT - AppCenter Integration for Crash Logs and app improvements"),
                .init(value: "NEW FEATURE - New words in current writing session."),
            ]),
            .init(value: "3.4.6 (Build 7)", children: [
                .init(value: "NEW FEATURE - User Preference window background color now saved per user."),
                .init(value: "FIXED - New Item windows didn't change over to the new color until app restart."),
                .init(value: "FIXED - Another scenario where the font color was not correct for a shared story."),
                .init(value: "FIXED - Story List Table columns were editable"),
                .init(value: "FIXED - Story Associations - adding Characters made Scripto Portfolio crash!!  Oops."),
                .init(value: "ADDED - Added in Search for stories list window"),
                .init(value: "NEW FEATURE - Story Window now has Line Numbering but not shown by default"),
                .init(value: "FIXED - Changed \"Select a Story\" button titles to \"Close\" for consistency in Locations and Characters."),
                .init(value: "FIXED - Story Associations window not reflecting the correct color when changing users."),
                .init(value: "FIXED - Printing while not in the Story Window."),
                .init(value: "FIXED - Story Associations window not reflecting the correct color when changing users."),
                .init(value: "NEW FEATURE - Added Full Screen to the Story Window."),
                .init(value: "NEW FEATURE - macOS 10.14 - Dark Mode Compatible"),
                .init(value: "FIXED - Due to macOS 10.14 Scripto Idea would no longer connect to Scripto Portfolio. Had to tweak the networking options to get this to work. This was changed on the Scripto Portfolio side."),
                .init(value: "FIXED - When Locking Scripto Portfolio menu items under Scripto Portfolio were not being removed correctly so after logging back in one was being duplicated and the order was incorrect."),
                .init(value: "FIXED - If you Lock Scripto Portfolio a second time then the Username field would be blank."),
                .init(value: "FIXED - Location Images image count didn't update after adding or removing images."),
                
                .init(value: "FIXED - If a Location was open and you tried to open another it would just bring the open one to the front with no message or explanation."),
                .init(value: "LIMITATION - When Darkmode is enabled you will not be able to change the App Themes at this time. "),
                .init(value: "FIXED - After Deleting a Location that has Images and you create a new Location the new Location will have the Deleted Locations Images."),
            ]),
            .init(value: "3.0.0 (Build 6)", children: [
                .init(value: "FEATURE UPDATE - Location Images"),
                .init(value: "Your now able to have multiple images for each Location so you don't have to stress over which image fits the Location best. "),
                .init(value: "FIXED - User > Preferences window didn't update background color after changing the Theme color."),
                .init(value: "ADDED - Version label on the Login window"),
                .init(value: "NEW FEATURE - You can now create Items/Objects and create Categories and Subcategories and associate them with the items/Objects."),
            ]),
            .init(value: "2.2.2 (Build 3)", children: [
                .init(value: "NEW FEATURE - ScriptoIdea Released "),
                .init(value: "Scripto Idea is a mobile app that your able to create Story, Location, Character and Story Ideas and quickly save them in one place."),
                .init(value: "Scripto Idea can connect to Scripto Portfolio and send any saved idea to Scripto Portfolio so it can be expanded on or used in existing ideas.  "),
            ]),
            .init(value: "2.1.2 (Build 2)", children: [
                .init(value: "FIXED - Story Window title bar not the correct color in macOS 10.13", children: []),
            ]),
            
            
            
            
            
            
            
            
        ]
    }
}
