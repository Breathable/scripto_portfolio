//
//  WantedFeaturesViewController.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 2/12/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Cocoa

class WantedFeaturesViewController: NSViewController, NSOutlineViewDelegate {
    
    
    @IBOutlet weak var WFTopLabel: NSTextField!
    @IBOutlet weak var WFVersionHistoryLabel: NSTextField!
    @IBOutlet weak var WFCloseButton: NSButton!
    
    
    var CurrentAppearanceWantedFeaturesViewController = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        outlineView.delegate = self
        
        treeController.objectClass = Node.self
        treeController.childrenKeyPath = "children"
        treeController.countKeyPath = "count"
        treeController.leafKeyPath = "isLeaf"
        
        outlineView.gridStyleMask = .solidHorizontalGridLineMask
        outlineView.autosaveExpandedItems = true
        
        treeController.bind(NSBindingName(rawValue: "contentArray"),
                            to: self,
                            withKeyPath: "content",
                            options: nil)
        
        
        outlineView.bind(NSBindingName(rawValue: "content"),
                         to: treeController,
                         withKeyPath: "arrangedObjects",
                         options: nil)
        
        content.append(contentsOf: NodeFactory().nodes())
        
        
    }
    
    override func viewWillAppear() {
        
        appearanceCheck()
        
        super.viewDidLayout()
        
    }
    
    
    override func viewDidLayout() {
        
        //        enum InterfaceStyle : String {
        //            case Dark, Light
        //
        //            init() {
        //                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
        //                self = InterfaceStyle(rawValue: type)!
        //            }
        //        }
        //
        //        let currentStyle = InterfaceStyle()
        //
        //        //print("currentStyle: \"\(currentStyle)\"  -  CurrentAppearance: \"\(CurrentAppearance)\" ")
        //
        //        if ("\(currentStyle)" != CurrentAppearanceNewUserViewController) {
        //            appearanceCheck()
        //        }
        
        appearanceCheck()
        
        super.viewDidLayout()
        
        
        
        
    }// end of viewDidLayout()
    
    
    func appearanceCheck() {
        
        enum InterfaceStyle : String {
            case Dark, Light
            
            init() {
                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
                self = InterfaceStyle(rawValue: type)!
            }
        }
        
        let currentStyle = InterfaceStyle()
        
        print("currentStyle: '\(currentStyle)'")
        
        if ("\(currentStyle)" != CurrentAppearanceWantedFeaturesViewController) {
            
            if (currentStyle.rawValue == "Dark") {
                
                print("Dark Color Enabled")
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColorDarkMode
                

                
                
                WFCloseButton.styleButtonText(button: WFCloseButton, buttonName: "Close", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                WFCloseButton.wantsLayer = true
                WFCloseButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                WFCloseButton.layer?.cornerRadius = 7
                
                WFTopLabel.font = NSFont(name:AppFontBold, size: 13.0)
                WFTopLabel.textColor = NSColor.white
                
                WFVersionHistoryLabel.font = NSFont(name:AppFontBold, size: 13.0)
                WFVersionHistoryLabel.textColor = NSColor.white
                

                
                
                
            } else if (currentStyle.rawValue == "Light") {
                
                print("Light Color Enabled")
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColor.cgColor
                

                
                WFCloseButton.styleButtonText(button: WFCloseButton, buttonName: "Close", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                WFCloseButton.wantsLayer = true
                WFCloseButton.layer?.backgroundColor = NSColor.gray.cgColor
                WFCloseButton.layer?.cornerRadius = 7
                
                WFTopLabel.font = NSFont(name:AppFontBold, size: 13.0)
                WFTopLabel.textColor = NSColor.white
                
                WFVersionHistoryLabel.font = NSFont(name:AppFontBold, size: 13.0)
                WFVersionHistoryLabel.textColor = NSColor.white

                
            }
        }
        
        CurrentAppearanceWantedFeaturesViewController = "\(currentStyle)"
        
        
    }//end of appearanceCheck()
    
    
    @IBAction func WFCloseButtonAction(_ sender: Any) {
        
        self.view.window?.close()
        
    }
    
    @IBOutlet weak var outlineView: NSOutlineView!
    private let treeController = NSTreeController()
    @objc dynamic var content = [Node]()
    
    public func outlineView(_ outlineView: NSOutlineView,
                            viewFor tableColumn: NSTableColumn?,
                            item: Any) -> NSView? {
        var cellView: NSTableCellView?
        
        guard let identifier = tableColumn?.identifier else { return cellView }
        
        switch identifier {
        case .init("node"):
            if let view = outlineView.makeView(withIdentifier: identifier,
                                               owner: outlineView.delegate) as? NSTableCellView {
                view.textField?.bind(.value,
                                     to: view,
                                     withKeyPath: "objectValue.value",
                                     options: nil)
                cellView = view
            }
        case .init("count"):
            if let view = outlineView.makeView(withIdentifier: identifier,
                                               owner: outlineView.delegate) as? NSTableCellView {
                view.textField?.bind(.value,
                                     to: view,
                                     withKeyPath: "objectValue.childrenCount",
                                     options: nil)
                cellView = view
            }
        default:
            return cellView
        }
        return cellView
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
}



