//
//  ItemList.swift
//  Scripto Portfolio
//
//  Created by Dorian Glanville on 8/28/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Foundation

// @objc


//@objcMembers
class ItemList: NSObject {
    
    @objc var Ikey: String
    @objc var Name: String
    @objc var Category: String
    @objc var Subcategory: String
    
     init(Ikey: String, Name: String, Category: String, Subcategory: String) {
        self.Ikey = Ikey
        self.Name = Name
        self.Category = Category
        self.Subcategory = Subcategory
    }
    
    
}
