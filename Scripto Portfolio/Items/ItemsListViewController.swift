//
//  ItemsViewController.swift
//  Scripto Portfolio
//
//  Created by Dorian Glanville on 8/14/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Cocoa
import SwiftLoggly
import FMDB


class ItemsViewController: NSViewController, NSTableViewDataSource, NSTableViewDelegate {
    
    
    //var queue = FMDatabaseQueue()
    
    
    
    
    @IBOutlet weak var OpenItemButton: NSButton!
    @IBOutlet weak var ItemsCloseButton: NSButton!
    @IBOutlet weak var ItemsEditItemCategories: NSButton!
    @IBOutlet weak var ItemRemoveButton: NSButton!
    @IBOutlet weak var ItemAddButton: NSButton!
    @IBOutlet weak var ItemsTableView: NSTableView!
    @IBOutlet weak var ItemSearchBar: NSSearchField!
    

    
    var ItemArray: [ItemList] = []
    
    var SearchBool = false
    
    @IBOutlet weak var itemSearchField: NSSearchField!
    
    
    
    @IBOutlet weak var ItemCountLabel: NSTextField!
    
    var CurrentAppearanceItemsListViewController = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        


        
        //////////////////////

        
        
        
        
    }// end of viewDidLoad()
    
    override func viewWillAppear() {
        
        
        ItemsTableView.delegate = self
        ItemsTableView.dataSource = self
        
        ItemsTableView.tableColumn(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "ikey"))?.isHidden = true
        
        let DoubleSelect: Selector = #selector(itemListOpenButtonAction(_:))
        ItemsTableView.doubleAction = DoubleSelect
        
        //LoadListTableData()
        
    }
    
    override func viewDidAppear() {
        
        LoadListTableData()
    }
    
    
    
    
    override func viewDidLayout() {
        
        //        enum InterfaceStyle : String {
        //            case Dark, Light
        //
        //            init() {
        //                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
        //                self = InterfaceStyle(rawValue: type)!
        //            }
        //        }
        //
        //        let currentStyle = InterfaceStyle()
        //
        //        //print("currentStyle: \"\(currentStyle)\"  -  CurrentAppearance: \"\(CurrentAppearance)\" ")
        //
        //        if ("\(currentStyle)" != CurrentAppearanceNewUserViewController) {
        //            appearanceCheck()
        //        }
        
        appearanceCheck()
        
        super.viewDidLayout()
        
        
        
        
    }// end of viewDidLayout()
    
    
    func appearanceCheck() {
        
        enum InterfaceStyle : String {
            case Dark, Light
            
            init() {
                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
                self = InterfaceStyle(rawValue: type)!
            }
        }
        
        let currentStyle = InterfaceStyle()
        
        if ("\(currentStyle)" != CurrentAppearanceItemsListViewController) {
            
           
            
            if (currentStyle.rawValue == "Dark") {
                
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColorDarkMode
                
                
                
                OpenItemButton.styleButtonText(button: OpenItemButton, buttonName: "Open", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                OpenItemButton.wantsLayer = true
                OpenItemButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                OpenItemButton.layer?.cornerRadius = 7
                
                ItemsCloseButton.styleButtonText(button: ItemsCloseButton, buttonName: "Close", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                ItemsCloseButton.wantsLayer = true
                ItemsCloseButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                ItemsCloseButton.layer?.cornerRadius = 7
                
                ItemsEditItemCategories.styleButtonText(button: ItemsEditItemCategories, buttonName: "Edit Item Categories", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                ItemsEditItemCategories.wantsLayer = true
                ItemsEditItemCategories.layer?.backgroundColor = NSColor.lightGray.cgColor
                ItemsEditItemCategories.layer?.cornerRadius = 7
                
                ItemCountLabel.font = NSFont(name:AppFontBold, size: 13.0)
                ItemCountLabel.textColor = NSColor.lightGray
                
                
                
            } else if (currentStyle.rawValue == "Light") {
                
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColor.cgColor
                
                OpenItemButton.styleButtonText(button: OpenItemButton, buttonName: "Open", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                OpenItemButton.wantsLayer = true
                OpenItemButton.layer?.backgroundColor = NSColor.gray.cgColor
                OpenItemButton.layer?.cornerRadius = 7
                
                ItemsCloseButton.styleButtonText(button: ItemsCloseButton, buttonName: "Close", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                ItemsCloseButton.wantsLayer = true
                ItemsCloseButton.layer?.backgroundColor = NSColor.gray.cgColor
                ItemsCloseButton.layer?.cornerRadius = 7
                
                ItemsEditItemCategories.styleButtonText(button: ItemsEditItemCategories, buttonName: "Edit Item Categories", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                ItemsEditItemCategories.wantsLayer = true
                ItemsEditItemCategories.layer?.backgroundColor = NSColor.gray.cgColor
                ItemsEditItemCategories.layer?.cornerRadius = 7
                
                ItemCountLabel.font = NSFont(name:AppFontBold, size: 13.0)
                ItemCountLabel.textColor = NSColor.white
                
            }
        }
        
        CurrentAppearanceItemsListViewController = "\(currentStyle)"
        
        
    }//end of appearanceCheck()
    
    
    
    
    
    
    
    func LoadListTableData() {
        
        db.open()
        
        ItemArray.removeAll()
        
        var Ikey: String!
        var Name: String!
        var CategoryNUM: Int
        
        var SubcategoryNUM: Int
        
        
        var Query = "select * from itemtable where userikeyreference = \(CurrentUserIkey!) "
        
        print("Query: \(Query)")
        
        if (SearchBool == true) {
            Query = "select * from itemtable where userikeyreference = \"\(CurrentUserIkey!)\" and name LIKE '%\(itemSearchField.stringValue)%' OR itemcategory LIKE '%\(itemSearchField.stringValue)%' OR itemsubcategory LIKE '%\(itemSearchField.stringValue)%'"
            
            //print("Query 2: \(Query)")
            
        }
        
        do {
            
            let ItemQuery = try db.executeQuery(Query, values:nil)
            
            
            
            while ItemQuery.next() {
                Ikey = ItemQuery.string(forColumn: "ikey")
                Name = ItemQuery.string(forColumn: "name")
                CategoryNUM = ItemQuery.long(forColumn: "itemcategory")
                SubcategoryNUM = ItemQuery.long(forColumn: "itemsubcategory")
                
                print("Ikey: \(Ikey) Name: \(Name) CategoryNUM: \(CategoryNUM) SubcategoryNUM: \(SubcategoryNUM)")

                var Category: String = ""
                var CategoryS: String = ""
                
                // String(CategoryNUM).count > 0
                if (String(CategoryNUM).count > 0 && CategoryNUM != 0) {
                    
                    let CategoryQuery = try db.executeQuery("SELECT categoryname FROM itemcategorytable WHERE ikey = \(CategoryNUM)", values:nil)
                    
                    while CategoryQuery.next() {
                        Category = CategoryQuery.string(forColumn: "categoryname")!
                    }
                    
                } else {
                    
                    CategoryS = "None"
                }
                
                var Subcategory: String = ""
                var SubcategoryS: String = ""
                
                if (SubcategoryNUM != 0) {
                    
                    let SubcategoryQuery = try db.executeQuery("SELECT subcategoryname FROM itemsubcategorytable WHERE ikey = \(SubcategoryNUM)", values:nil)
                    
                    while SubcategoryQuery.next() {
                        Subcategory = SubcategoryQuery.string(forColumn: "subcategoryname")!
                    }
                } else {
                    SubcategoryS = "None"
                }
                
                var Cate: String = ""
                if (CategoryS == "None") {
                    Cate = "\(CategoryS)"
                } else {
                    Cate = "\(Category)"
                }
                
                var Sub: String = ""
                if (SubcategoryS == "None") {
                    Sub = "\(SubcategoryS)"
                } else {
                    Sub = "\(Subcategory)"
                }
                
                
                print("Ikey: \(Ikey!) - Name: \(Name!) - Category: \(Cate) Subcategory: \(Sub)")
                
                //////////////////////
               
                let ItemToAdd = ItemList(Ikey: Ikey, Name: Name, Category: Cate, Subcategory: Sub)
                
                
                ItemArray.append(ItemToAdd)
                
            }
            
        } catch {
            
            print("ERROR: ItemsListViewController: Failed to get \"\(CurrentUserName!)\"'s Locations\n\(Query)\n\(db.lastErrorMessage())")
            
            let ErrorMessage = "ItemsListViewController:\(#function) - Failed to get \"\(CurrentUserName!)\"'s Locations\n\(db.lastErrorMessage())\n\(Query))"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "ItemsListViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to Failed to get \"\(CurrentUserName!)\"'s Items\n\n\(Query)", DatabaseError: "\(db.lastErrorMessage())")
            
        }
        
        print("ItemArray: \(ItemArray)")
        
        
        
        
        ItemsTableView.reloadData()
        
        ItemCountLabel.stringValue = "Items: \(ItemArray.count)"
        
    }// end of table load data
    
    
    
    
    
    
    
    
    
    func numberOfRows(in tableVieww: NSTableView) -> Int {
        print("ItemArray Count: \(ItemArray.count)")
        return ItemArray.count
        
    }
    
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        
        guard let cell = tableView.makeView(withIdentifier: tableColumn!.identifier, owner: self) as? NSTableCellView else
        {
            print("Table Returned Nil")
            return nil
        }
        
        
        if (tableColumn?.identifier)!.rawValue == "ikey" {
            cell.textField?.stringValue = ItemArray[row].Ikey
        }
        if (tableColumn?.identifier)!.rawValue == "name" {
            cell.textField?.stringValue = ItemArray[row].Name
        }
        if (tableColumn?.identifier)!.rawValue == "category" {
            cell.textField?.stringValue = ItemArray[row].Category
        }
        if (tableColumn?.identifier)!.rawValue == "subcategory" {
            cell.textField?.stringValue = ItemArray[row].Subcategory
        }
        
        
        
        return cell
        
    }
    
    
    @objc func tableView(_ tableView: NSTableView, sortDescriptorsDidChange oldDescriptors: [NSSortDescriptor]) {
        let itemsAsMutableArray = NSMutableArray(array: ItemArray)
        itemsAsMutableArray.sort(using: tableView.sortDescriptors)
        ItemArray = itemsAsMutableArray as! [ItemList]
        tableView.reloadData()
    }
    
//    func tableView(_ tableView: NSTableView, sortDescriptorsDidChange oldDescriptors: [NSSortDescriptor]) {
//        let personsAsMutableArray = NSMutableArray(array: persons)
//        personsAsMutableArray.sort(using: tableView.sortDescriptors)
//        persons = personsAsMutableArray as! [Person]
//        tableView.reloadData()
//    }
    
    
    
    func tableViewSelectionDidChange(_ notification: Notification) {
        
        let SelectedRow = ItemsTableView.selectedRow
        
        print("SelectedRow: \(SelectedRow)")
        
        if (SelectedRow == -1) {
            
            ItemRemoveButton.isEnabled = false
            
            return
        }
        
        let Name = ItemArray[SelectedRow].Name
        let Ikey = ItemArray[SelectedRow].Ikey
        let Category = ItemArray[SelectedRow].Category
        let Subcateogry = ItemArray[SelectedRow].Subcategory
        
        print("Table Selection - Ikey: \"\(Ikey)\" Name: \"\(Name)\" Category: \"\(Category)\" Subcateogry: \"\(Subcateogry)\"")
        
        let isIndexValid = ItemArray.indices.contains(SelectedRow)
        
        if isIndexValid {
            print("ITEM IS VALID\nTable Selection - Ikey: \"\(Ikey)\" Name: \"\(Name)\" Category: \"\(Category)\" Subcateogry: \"\(Subcateogry)\"")
            
            ItemRemoveButton.isEnabled = true
            
//            itemIkeyToLoad = Int(Ikey)!
//            itemBool = true
            
        } else {
            print("**** IS NOT VALID\nTable Selection - Ikey: \"\(Ikey)\" Name: \"\(Name)\" Category: \"\(Category)\" Subcateogry: \"\(Subcateogry)\"")
            ItemRemoveButton.isEnabled = false
        }
        
        
    }
    
    
    
    
    ////////////////////////////////////////////////
    
    

    @IBAction func itemListOpenButtonAction(_ sender: Any) {
        
        let SelectedRow = ItemsTableView.selectedRow
        
        print("SelectedRow: \(SelectedRow)")
        
        if (SelectedRow == -1) {
            return
        }
        
        let Name = ItemArray[SelectedRow].Name
        let Ikey = ItemArray[SelectedRow].Ikey
        let Category = ItemArray[SelectedRow].Category
        let Subcateogry = ItemArray[SelectedRow].Subcategory
        
        print("Table Selection - Ikey: \"\(Ikey)\" Name: \"\(Name)\" Category: \"\(Category)\" Subcateogry: \"\(Subcateogry)\"")
        
        let isIndexValid = ItemArray.indices.contains(SelectedRow)
        
        if isIndexValid {
            //print("IS VALID")
            
            itemIkeyToLoad = Int(Ikey)!
            itemBool = true
            
            ItemInfoWindow.showWindow(nil)
            
        } else {
            //print("IS NOT VALID")
        }
        
        
        self.view.window?.close()
        
    }// end of itemListOpenButtonAction()
    
    
    @IBAction func itemSearchFieldAction(_ sender: Any) {
        
        if (itemSearchField.stringValue.count < 1 || SearchBool == true) {
            SearchBool = false
            LoadListTableData()
        } else {
            SearchBool = true
            LoadListTableData()
        }
        
    }
    
    
    
    
    
    
    
    
    
    
    
    @IBAction func itemListWindowCloseButtonAction(_ sender: Any) {
        
        
        
        
        self.view.window?.close()
        
    }
    
    
    
    
    
    @IBAction func itemEditCategoryButtonAction(_ sender: Any) {
        
        ItemCSCWindow.showWindow(self)
        
    }
    
    
    @IBAction func itemDeleteButtonAction(_ sender: Any) {
        
        
        let SelectedRow = ItemsTableView.selectedRow
        
        print("SelectedRow: \(SelectedRow)")
        
        if (SelectedRow == -1) {
            return
        }
        
        let Name = ItemArray[SelectedRow].Name
        let Ikey = ItemArray[SelectedRow].Ikey
    
        
        print("Table Selection - Ikey: \"\(Ikey)\" Name: \"\(Name)\"")
        
        let isIndexValid = ItemArray.indices.contains(SelectedRow)
        
        if isIndexValid {
     
            let alert = NSAlert()
            alert.messageText = "Are you sure you want to Delete Item \"\(Name)\""
            alert.informativeText = "This action cannot be undone."
            alert.alertStyle = .warning
            alert.addButton(withTitle: "Canel")
            alert.addButton(withTitle: "Yes Delete")
            
            
            
            if (CurrentAppearanceItemsListViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.window.titlebarAppearsTransparent = true
            
            let returnCode = alert.runModal()
            print("returnCode: \(returnCode)")
            
            if (returnCode.rawValue == 1000) {
                print("Canel Pressed")
            }
            if (returnCode.rawValue == 1001) {
                
                print("Yes Delete Pressed")
                
                let DeleteLocationQuery = "DELETE FROM itemtable WHERE ikey = \"\(Ikey)\" and name = \"\(Name)\" "
                
                do {
                    
                    try db.executeUpdate(DeleteLocationQuery, values:nil)
                    
                } catch {
                    
                    let ErrorMessage = "ItemsListViewController:\(#function) - Failed to Delete Item \"\(Name)\" \n\(DeleteLocationQuery)\n\(db.lastErrorMessage())\n\(DeleteLocationQuery))"
                    
                    print(ErrorMessage)
                    
                    loggly(LogType.Error, text: ErrorMessage)
                    
                    DoAlert.DisplayAlert(Class: "ItemsListViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to delete Item \(Name)\n\n\(DeleteLocationQuery)", DatabaseError: "\(db.lastErrorMessage())")
                    
                }
                
                LoadListTableData()
                
                
            } // end of 1000

            
        } else {
            
        }
        
        
        
        
        
        
        
    }
    
    @IBAction func itemAddButtonAction(_ sender: Any) {
        
        print("DB PATH: \(db.databasePath)")
        
        ConnectionTest.CheckConnection()
        
        db.open()
        
        // (ikey,name,createdate,composition,createdby,usedfor,history,itemcategory,itemsubcategory,userikeyreference)
        
        let InsertNewItemQuery = "INSERT INTO itemtable (ikey,name,userikeyreference) VALUES ((select MAX(ikey) + 1 from itemtable),\"Item Name\",\"\(CurrentUserIkey!)\")"

        print("InsertNewItemQuery: \n\(InsertNewItemQuery)")


        let Success = db.executeStatements(InsertNewItemQuery)

        if (!Success) {

            let ErrorMessage = "Failed to INSERT New Item\n\n\(InsertNewItemQuery)\n\n\(db.lastErrorMessage())"

            loggly(LogType.Error, text: ErrorMessage)

            DoAlert.DisplayAlert(Class: "ItemsListViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed INSERT New Item\n\n\(InsertNewItemQuery)", DatabaseError: "\(db.lastErrorMessage())")
            return

        } else {
            
            print("SUCCESSFULLY INSERTED NEW ITEMTABLE ROW")
            
        }
        
        ///////////////////////////////////////////
        
        let MaxIkeyQuery = "SELECT MAX(ikey) as MaxI FROM itemtable WHERE userikeyreference = \(CurrentUserIkey!)"
        
        print("MaxIkeyQuery: \(MaxIkeyQuery)")
 
        var Ikey: Int!
        
            do {
               
                let MaxIkeyQueryRun = try db.executeQuery(MaxIkeyQuery, values:nil)

                while MaxIkeyQueryRun.next() {

                    Ikey = Int(MaxIkeyQueryRun.int(forColumn: "MaxI"))
                    itemIkeyToLoad = Ikey!
                    itemBool = false
                    print("Ikey: \"\(Ikey)\"")

                }

            } catch {

                let ErrorMessage = "ItemsListViewController:\(#function) - Failed to get \"\(CurrentUserName!)\"'s Locations\n\(db.lastErrorMessage())\n\(MaxIkeyQuery))"

                print(ErrorMessage)

                loggly(LogType.Error, text: ErrorMessage)

                DoAlert.DisplayAlert(Class: "ItemsListViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to get max ikey from itemtable \n\n\(MaxIkeyQuery)", DatabaseError: "\(db.lastErrorMessage())")
                
                return
                
            }

        ///////////////////////////////////////////
        
        
        ItemInfoWindow.showWindow(self)
        
        LoadListTableData()
        
    }
    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}// end of ItemsViewController: NSViewController
