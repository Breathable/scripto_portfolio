//
//  ItemCategorySubcategoryList.swift
//  Scripto Portfolio
//
//  Created by Dorian Glanville on 9/14/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Foundation


class ItemCategorySubcategoryList: NSObject {
    var Ikey: String
    var Name: String
    var Count: Int
    
    
    init(Ikey: String, Name: String, Count: Int) {
        self.Ikey = Ikey
        self.Name = Name
        self.Count = Count
        
    }
    
    
}
