//
//  ItemCategorySubcategoryViewController.swift
//  Scripto Portfolio
//
//  Created by Dorian Glanville on 9/11/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Cocoa
import SwiftLoggly

class ItemCategorySubcategoryViewController: NSViewController, NSTextFieldDelegate, NSTextViewDelegate, NSTableViewDataSource, NSTableViewDelegate{

    
    @IBOutlet weak var ItemCategoryAddTextField: NSTextField!
    @IBOutlet weak var ItemCategoryAddButton: NSButton!
    @IBOutlet weak var ItemCategoryListTableView: NSTableView!
    @IBOutlet weak var ItemCategoryDeleteCategoryButton: NSButton!
    @IBOutlet var ItemCategoryNoteTextView: NSTextView!
    @IBOutlet weak var ItemCategoryNoteLabel: NSTextField!
    @IBOutlet weak var catSearchField: NSSearchField!
    
    
    
    @IBOutlet weak var ItemSubcategoryAddTextField: NSTextField!
    @IBOutlet weak var ItemSubcategoryAddButton: NSButton!
    @IBOutlet weak var ItemSubategoryListTableView: NSTableView!
    @IBOutlet weak var ItemSubcategoryDeleteCategoryButton: NSButton!
    @IBOutlet var ItemSubcategoryNoteTextView: NSTextView!
    @IBOutlet weak var subSearchField: NSSearchField!
    
    @IBOutlet weak var ItemListCloseButton: NSButton!
    
    
    var CatSearchBool = false
    var SubSearchBool = false
    
    
    
    var CategoryArray: [ItemCategorySubcategoryList] = []
    var SubcategoryArray: [ItemCategorySubcategoryList] = []
    
    
    var CurrentAppearanceItemCategoryViewViewController = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        
        
        //print("ItemCategoryViewViewController viewDidLoad() RAN")
        
        
        ItemCategoryListTableView.delegate = self
        ItemCategoryListTableView.dataSource = self

        ItemSubategoryListTableView.delegate = self
        ItemSubategoryListTableView.dataSource = self
        
        
        ItemCategoryAddTextField.delegate = self
        ItemSubcategoryAddTextField.delegate = self
        
        ItemCategoryNoteTextView.delegate = self
        ItemSubcategoryNoteTextView.delegate = self
    
        
        
        ItemCategoryListTableView.tableColumn(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "ikey"))?.isHidden = true
        
        ItemSubategoryListTableView.tableColumn(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "ikey"))?.isHidden = true
        
        
        
        
    }
    
    override func viewWillAppear() {
        
        print("ItemCategoryViewViewController viewWillAppear() RAN")
        
        
        
        
        LoadCategoryTable()
        
    }
    
    
    override func viewDidLayout() {
        
        //print("ItemCategoryViewViewController viewDidLayout() RAN")
        
        appearanceCheck()
        
        super.viewDidLayout()

        
    }
    
    
//    override func viewDidLayout() {
//
//        //        enum InterfaceStyle : String {
//        //            case Dark, Light
//        //
//        //            init() {
//        //                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
//        //                self = InterfaceStyle(rawValue: type)!
//        //            }
//        //        }
//        //
//        //        let currentStyle = InterfaceStyle()
//        //
//        //        //print("currentStyle: \"\(currentStyle)\"  -  CurrentAppearance: \"\(CurrentAppearance)\" ")
//        //
//        //        if ("\(currentStyle)" != CurrentAppearanceNewUserViewController) {
//        //            appearanceCheck()
//        //        }
//
//        appearanceCheck()
//
//        super.viewDidLayout()
//
//
//
//
//    }// end of viewDidLayout()
    
    
    func appearanceCheck() {
        
        enum InterfaceStyle : String {
            case Dark, Light
            
            init() {
                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
                self = InterfaceStyle(rawValue: type)!
            }
        }
        
        let currentStyle = InterfaceStyle()
        
        
        //print("ItemCategorySubcategoryViewController currentStyle: \"\(currentStyle)\" ")
        
        if ("\(currentStyle)" != CurrentAppearanceItemCategoryViewViewController) {
            
            if (currentStyle.rawValue == "Dark") {
                
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColorDarkMode
                
                ItemCategoryNoteTextView.wantsLayer = true
                ItemCategoryNoteTextView.backgroundColor = NSColor.lightGray
                
                ItemSubcategoryNoteTextView.wantsLayer = true
                ItemSubcategoryNoteTextView.backgroundColor = NSColor.lightGray
                
                ItemCategoryAddButton.styleButtonText(button: ItemCategoryAddButton, buttonName: "Add Category", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                ItemCategoryAddButton.wantsLayer = true
                ItemCategoryAddButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                ItemCategoryAddButton.layer?.cornerRadius = 7
                
                ItemCategoryDeleteCategoryButton.styleButtonText(button: ItemCategoryDeleteCategoryButton, buttonName: "Delete Category", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                ItemCategoryDeleteCategoryButton.wantsLayer = true
                ItemCategoryDeleteCategoryButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                ItemCategoryDeleteCategoryButton.layer?.cornerRadius = 7
                
                
                ItemSubcategoryAddButton.styleButtonText(button: ItemSubcategoryAddButton, buttonName: "Add Subcategory", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                ItemSubcategoryAddButton.wantsLayer = true
                ItemSubcategoryAddButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                ItemSubcategoryAddButton.layer?.cornerRadius = 7
                
                ItemSubcategoryDeleteCategoryButton.styleButtonText(button: ItemSubcategoryDeleteCategoryButton, buttonName: "Delete Subcategory", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                ItemSubcategoryDeleteCategoryButton.wantsLayer = true
                ItemSubcategoryDeleteCategoryButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                ItemSubcategoryDeleteCategoryButton.layer?.cornerRadius = 7
                
                ItemListCloseButton.styleButtonText(button: ItemListCloseButton, buttonName: "Close", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                ItemListCloseButton.wantsLayer = true
                ItemListCloseButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                ItemListCloseButton.layer?.cornerRadius = 7
                
                
                
            } else if (currentStyle.rawValue == "Light") {
                
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColor.cgColor
                
                ItemCategoryNoteTextView.wantsLayer = true
                ItemCategoryNoteTextView.backgroundColor = NSColor.white
                
                ItemSubcategoryNoteTextView.wantsLayer = true
                ItemSubcategoryNoteTextView.backgroundColor = NSColor.white
                
                ItemCategoryAddButton.styleButtonText(button: ItemCategoryAddButton, buttonName: "Add Category", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                ItemCategoryAddButton.wantsLayer = true
                ItemCategoryAddButton.layer?.backgroundColor = NSColor.gray.cgColor
                ItemCategoryAddButton.layer?.cornerRadius = 7
                
                ItemCategoryDeleteCategoryButton.styleButtonText(button: ItemCategoryDeleteCategoryButton, buttonName: "Delete Category", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                ItemCategoryDeleteCategoryButton.wantsLayer = true
                ItemCategoryDeleteCategoryButton.layer?.backgroundColor = NSColor.gray.cgColor
                ItemCategoryDeleteCategoryButton.layer?.cornerRadius = 7
                
                
                ItemSubcategoryAddButton.styleButtonText(button: ItemSubcategoryAddButton, buttonName: "Add Subcategory", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                ItemSubcategoryAddButton.wantsLayer = true
                ItemSubcategoryAddButton.layer?.backgroundColor = NSColor.gray.cgColor
                ItemSubcategoryAddButton.layer?.cornerRadius = 7
                
                ItemSubcategoryDeleteCategoryButton.styleButtonText(button: ItemSubcategoryDeleteCategoryButton, buttonName: "Delete Subcategory", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                ItemSubcategoryDeleteCategoryButton.wantsLayer = true
                ItemSubcategoryDeleteCategoryButton.layer?.backgroundColor = NSColor.gray.cgColor
                ItemSubcategoryDeleteCategoryButton.layer?.cornerRadius = 7
                
                ItemListCloseButton.styleButtonText(button: ItemListCloseButton, buttonName: "Close", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                ItemListCloseButton.wantsLayer = true
                ItemListCloseButton.layer?.backgroundColor = NSColor.gray.cgColor
                ItemListCloseButton.layer?.cornerRadius = 7
                
            }
        }
        
        CurrentAppearanceItemCategoryViewViewController = "\(currentStyle)"
        
        
    }//end of appearanceCheck()
    
    
    
    
    
    
    
    
    
    
    func textDidChange(_ notification: Notification) {
    //func textViewDidChangeSelection(_ notification: Notification) {
        // TextViews
        
        print("textDidChange RAN")
        
        let SelectedTextView = (notification.object as! NSTextView)
        
        if (SelectedTextView.string.count == 0) {
            return
        }
        
        let lastCharacter = String(describing: SelectedTextView.string.last!)
        
        print("textViewDidChangeSelection lastCharacter: \(lastCharacter)")
        
        let SymbolTest = CharacterSet.symbols.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
        
        let punctuationTest = CharacterSet.punctuationCharacters.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
        
        //let letterTest = CharacterSet.letters.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
        
        //let SpaceNewLineTest = CharacterSet.whitespacesAndNewlines.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
        //let NewLineTest = CharacterSet.newlines.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
        
        if (SymbolTest || punctuationTest) {
            
            //                print("SymbolTest: \(SymbolTest)")
            //                print("punctuationTest: \(punctuationTest)")
            //                print("letterTest: \(letterTest)")
            //                print("SpaceNewLineTest: \(SpaceNewLineTest)")
            
            __NSBeep()
            
            SelectedTextView.string = String(SelectedTextView.string.dropLast())
            
            return
        }
        
        //////////////////////////////
        
        
        
        
        //////////////////////////////

        
        if (SelectedTextView == ItemCategoryNoteTextView) {
            print("ItemCategoryNoteTextView Was the Select TextView")
            
            let SelectedRow = ItemCategoryListTableView.selectedRow
            
            print("SelectedRow: \(SelectedRow)")
            
            if (SelectedRow == -1) {
                SubcategoryArray.removeAll()
                ItemSubategoryListTableView.reloadData()
                
                let alert = NSAlert()
                alert.messageText = "No Category Selected"
                alert.informativeText = "Please Select a Category."
                alert.alertStyle = .warning
                
                if (CurrentAppearanceItemCategoryViewViewController == "Dark") {
                    alert.window.backgroundColor = AlertBackgroundColorDarkMode
                } else {
                    alert.window.backgroundColor = WindowBackgroundColor
                }
                
                alert.window.titlebarAppearsTransparent = true
                alert.addButton(withTitle: "OK")
                
                if alert.runModal() == .alertFirstButtonReturn {
                    print("Ok Pressed")
                    ItemCategoryNoteTextView.string = ""
                }
                
                return
            }
            
            let Name = CategoryArray[SelectedRow].Name
            let Ikey = CategoryArray[SelectedRow].Ikey
            
            
            let UpdateCategoryNoteQuery = String(format: "UPDATE itemcategorytable SET categorynote = \"\(SelectedTextView.string)\" WHERE ikey = \"\(Ikey)\" AND categoryname = \"\(Name)\"  ")
            
            print("UpdateCategoryNoteQuery: \(UpdateCategoryNoteQuery)")
            
            do {
                
                try db.executeUpdate(UpdateCategoryNoteQuery, values: nil)
                
            } catch let error as NSError {
                
                let ErrorMessage = "ItemCategorySubcategoryViewController: \(#function) - Failed to do Update Category Note\n \"\(UpdateCategoryNoteQuery)\" \nError: \(db.lastErrorMessage())\n\n\(error)"
                
                print(ErrorMessage)
                
                DoAlert.DisplayAlert(Class: "ItemCategorySubcategoryViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to do Update Category Note\n\nPlease quit then try again.\n\n\"\(UpdateCategoryNoteQuery)\"\n\n\"\(error)\" ", DatabaseError: "\(db.lastErrorMessage())")
                
                return
                
            }
            
            
        }
        
        if (SelectedTextView == ItemSubcategoryNoteTextView) {
            print("ItemSubcategoryNoteTextView Was the Select TextView")
            
            let SelectedRow = ItemSubategoryListTableView.selectedRow
            
            print("SelectedRow: \(SelectedRow)")
            
            if (SelectedRow == -1) {
                //SubcategoryArray.removeAll()
                //ItemSubategoryListTableView.reloadData()
                
                let alert = NSAlert()
                alert.messageText = "No Subcategory Selected"
                alert.informativeText = "Please Select a Subcategory."
                alert.alertStyle = .warning
                
                if (CurrentAppearanceItemCategoryViewViewController == "Dark") {
                    alert.window.backgroundColor = AlertBackgroundColorDarkMode
                } else {
                    alert.window.backgroundColor = WindowBackgroundColor
                }
                
                alert.window.titlebarAppearsTransparent = true
                alert.addButton(withTitle: "OK")
                
                if alert.runModal() == .alertFirstButtonReturn {
                    print("Ok Pressed")
                    ItemSubcategoryNoteTextView.string = ""
                }
                
                
                return
            }
            
            let Name = SubcategoryArray[SelectedRow].Name
            let Ikey = SubcategoryArray[SelectedRow].Ikey
            
            // if I have the ikey from the subcategory table of the item selected then I dont need the ikey from the category table for itemcategoryreferenceikey
            
            let UpdateSubCategoryNoteQuery = String(format: "UPDATE itemsubcategorytable SET subcategorynote = \"\(SelectedTextView.string)\" WHERE ikey = \"\(Ikey)\" AND subcategoryname = \"\(Name)\"  ")
            
            print("UpdateSubCategoryNoteQuery: \(UpdateSubCategoryNoteQuery)")
            
            do {
                
                try db.executeUpdate(UpdateSubCategoryNoteQuery, values: nil)
                
            } catch let error as NSError {
                
                let ErrorMessage = "ItemCategorySubcategoryViewController: \(#function) - Failed to do Update Subcategory Note\n \"\(UpdateSubCategoryNoteQuery)\" \nError: \(db.lastErrorMessage())\n\n\(error)"
                
                print(ErrorMessage)
                
                DoAlert.DisplayAlert(Class: "ItemCategorySubcategoryViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to do Update Subcategory Note\n\nPlease quit then try again.\n\n\"\(UpdateSubCategoryNoteQuery)\"\n\n\"\(error)\" ", DatabaseError: "\(db.lastErrorMessage())")
                
                return
                
            }
            
            
        }
        
        
        
        
        
        
    }// end of textDidChange
    
    

        
    
    
    
    
 
    
    
    
    
    override func controlTextDidChange(_ obj: Notification) {
        // TextFields
        
        let SelectedTextField = (obj.object as! NSTextField)
        
        if (SelectedTextField.stringValue.count == 0) {
            return
        }

        let lastCharacter = String(describing: SelectedTextField.stringValue.last!)
        
        //print("lastCharacter: \(lastCharacter)")
        
        let SymbolTest = CharacterSet.symbols.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
        
        let punctuationTest = CharacterSet.punctuationCharacters.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
        
        //let letterTest = CharacterSet.letters.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
        
        //let SpaceNewLineTest = CharacterSet.whitespacesAndNewlines.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
        let NewLineTest = CharacterSet.newlines.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
        
        if (SymbolTest || punctuationTest || NewLineTest) {
            
            //                print("SymbolTest: \(SymbolTest)")
            //                print("punctuationTest: \(punctuationTest)")
            //                print("letterTest: \(letterTest)")
            //                print("SpaceNewLineTest: \(SpaceNewLineTest)")
            
            __NSBeep()
            
            SelectedTextField.stringValue = String(SelectedTextField.stringValue.dropLast())
            
            return
        }

        
    }// end of controlTextDidChange
    
    ////////////////////////////////////////////////////////////////////////////////////////////////
    
    func LoadCategoryTable() {
        
        
        CategoryArray.removeAll()
        
        var Ikey: String!
        var Name: String!
        var Count: String?
        
        var Query = "select ikey,categoryname from itemcategorytable"
        
        print("Query: \(Query)")
        
        if (CatSearchBool == true) {

            Query = "select ikey,categoryname from itemcategorytable where categoryname LIKE '%\(catSearchField.stringValue)%'"

            //print("Query 2: \(Query)")

        }
        
        
        do {
            
            let CategoryQuery = try db.executeQuery(Query, values:nil)
            
            
            
            while CategoryQuery.next() {
                Ikey = CategoryQuery.string(forColumn: "ikey")
                Name = CategoryQuery.string(forColumn: "categoryname")
                

                print("Ikey: \(Ikey!) - Name: \(Name!)")
                
                
                ////////////////////////////////
                
                let CategoryNoteQuery = "SELECT count(ikey) as cnt FROM itemsubcategorytable WHERE itemcategoryreferenceikey = \(Ikey!)"
                
                print("CategoryNoteQuery: \(CategoryNoteQuery)")

                do {
                    
                    let CategoryNoteQueryRun = try db.executeQuery(CategoryNoteQuery, values:nil)

                    while CategoryNoteQueryRun.next() {
                        Count = CategoryNoteQueryRun.string(forColumn: "cnt")
                        
                        print("Ikey: \(Ikey!) - Count: \(Count!)")
                        
                    }
                    
                    
                    
                    
                } catch {
                    
                    
                    
                    let ErrorMessage = "LocationsViewController:\(#function) - Failed to get \"\(Name ?? "No Name")\" Subcategory count\n\(db.lastErrorMessage())\n\(Query))"
                    
                    print(ErrorMessage)
                    
                    loggly(LogType.Error, text: ErrorMessage)
                    
                    DoAlert.DisplayAlert(Class: "ItemCategorySubcategoryViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to get \"\(Name ?? "No Name")\" Subcategory count\n\n\(Query)", DatabaseError: "\(db.lastErrorMessage())")
                    
                    return
                    
                }
                
                /////////////////////////////////

                let CategoriesToAdd = ItemCategorySubcategoryList.init(Ikey: Ikey, Name: Name, Count: Int(Count!)! )
                
                
                CategoryArray.append(CategoriesToAdd)
                
            }
            
        } catch {
            
            print("ERROR: ItemCategorySubcategoryViewController: Failed to get Category List from database.\n\(Query)\n\(db.lastErrorMessage())")
            
            let ErrorMessage = "ItemCategorySubcategoryViewController:\(#function) - Failed to get Category List from database.\n\(db.lastErrorMessage())\n\(Query))"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "LocationsViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to get Category List from database.\n\n\(Query)", DatabaseError: "\(db.lastErrorMessage())")
            
            return
            
        }
        
        
        
        ItemCategoryListTableView.reloadData()
        
        
        
    }
    
    @IBAction func cateSearchAction(_ sender: Any) {
        
        if (catSearchField.stringValue.count > 0) {
            CatSearchBool = true
            LoadCategoryTable()
            RefreshSubCategoryTable()
        } else  {
            CatSearchBool = false
            LoadCategoryTable()
        }
        
    }
    
    @IBAction func subSearchAction(_ sender: Any) {
        
        if (subSearchField.stringValue.count > 0) {
            SubSearchBool = true
            RefreshSubCategoryTable()
        } else  {
            SubSearchBool = false
            RefreshSubCategoryTable()
        }
        
    }
    
    
    
    
    func numberOfRows(in tableVieww: NSTableView) -> Int {
        
        let CurrentTable = tableVieww as NSTableView
        
        var returnCount = Int()
        
        if (CurrentTable == ItemCategoryListTableView) {
            returnCount = CategoryArray.count
            print("LocationsArray Count: \(CategoryArray.count)")
        } else if (CurrentTable == ItemSubategoryListTableView) {
            returnCount = SubcategoryArray.count
            print("SubcategoryArray Count: \(SubcategoryArray.count)")
        }
        
        
        return returnCount
        
    }
    
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        
        guard let cell = tableView.makeView(withIdentifier: tableColumn!.identifier, owner: self) as? NSTableCellView else
        {
            print("Table Returned Nil")
            return nil
        }
        
        let CurrentTable = tableView as NSTableView
        
        
        if (CurrentTable == ItemCategoryListTableView) {
            
            if (tableColumn?.identifier)!.rawValue == "ikey" {
                cell.textField?.stringValue = CategoryArray[row].Ikey
            }
            if (tableColumn?.identifier)!.rawValue == "name" {
                cell.textField?.stringValue = CategoryArray[row].Name
            }
            if (tableColumn?.identifier)!.rawValue == "count" {
                cell.textField?.stringValue = String(CategoryArray[row].Count)
            }
            
        } else if (CurrentTable == ItemSubategoryListTableView) {
            
            if (tableColumn?.identifier)!.rawValue == "ikey" {
                cell.textField?.stringValue = SubcategoryArray[row].Ikey
            }
            if (tableColumn?.identifier)!.rawValue == "name" {
                cell.textField?.stringValue = SubcategoryArray[row].Name
            }
            
        }
        
        
        
       
        
        
        
        return cell
        
    }
    
    func tableViewSelectionDidChange(_ notification: Notification) {
        
        let CurrentTable = notification.object as? NSTableView
        
        if (CurrentTable == ItemCategoryListTableView) {
            
            let SelectedRow = ItemCategoryListTableView.selectedRow
            
            print("SelectedRow: \(SelectedRow)")
            
            if (SelectedRow == -1) {
                ItemCategoryNoteTextView.string = ""
                ItemSubcategoryNoteTextView.string = ""
                subSearchField.stringValue = ""
                SubcategoryArray.removeAll()
                ItemSubategoryListTableView.reloadData()
                return
            }
            
            
            
            let Name = CategoryArray[SelectedRow].Name
            let Ikey = CategoryArray[SelectedRow].Ikey
            
            
            print("Table Selection - Ikey: \"\(Ikey)\" Name: \"\(Name)\" ")
            
            let isIndexValid = CategoryArray.indices.contains(SelectedRow)
            
            if isIndexValid {
                
                RefreshTables()

            } else {
                
            }
            
        } else if (CurrentTable == ItemSubategoryListTableView) {
            
            let SelectedRow = ItemSubategoryListTableView.selectedRow
            
            print("SelectedRow: \(SelectedRow)")
            
            if (SelectedRow == -1) {
                ItemSubcategoryNoteTextView.string = ""
                return
            }
            
            //let Name = SubcategoryArray[SelectedRow].Name
            let Ikey = SubcategoryArray[SelectedRow].Ikey
            
            print("Table Selection - Ikey: \"\(Ikey)\"")
            
            let isIndexValid = SubcategoryArray.indices.contains(SelectedRow)
            
            if isIndexValid {
                
                
                
                let SubategoryNoteQuery = "SELECT subcategorynote FROM itemsubcategorytable WHERE ikey = \(Ikey)"
                
                print("SubategoryNoteQuery: \(SubategoryNoteQuery)")
                
                var NoteDB: String = ""
                
                do {
                    
                    let SubCategoryNoteQueryRun = try db.executeQuery(SubategoryNoteQuery, values:nil)
                    
                    while SubCategoryNoteQueryRun.next() {
                        NoteDB = SubCategoryNoteQueryRun.string(forColumn: "subcategorynote")!
                        
                        print("Ikey: \"\(Ikey)\"  NoteDB: \"\(NoteDB)\"  ")
                        
                    }
                    
                } catch {
                    
                    let ErrorMessage = "ItemCategorySubcategoryViewController:\(#function) - Failed to get Subcategory Note\n\(db.lastErrorMessage())\n\(SubategoryNoteQuery))"
                    
                    print(ErrorMessage)
                    
                    loggly(LogType.Error, text: ErrorMessage)
                    
                    DoAlert.DisplayAlert(Class: "ItemCategorySubcategoryViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to get Subcategory Note\n\n\(SubategoryNoteQuery)", DatabaseError: "\(db.lastErrorMessage())")
                    
                    return
                    
                }
                
                ItemSubcategoryNoteTextView.string = NoteDB
                
                
            } else {
                
            }
                
            
            
        }
        
        
        
        
        
        
        
    }// end of tableViewSelectionDidChange
    
    
    
    
    func RefreshTables() {
        
        // load category note
        
        let SelectedRow = ItemCategoryListTableView.selectedRow
        
        print("SelectedRow: \(SelectedRow)")
        
        if (SelectedRow == -1) {
            SubcategoryArray.removeAll()
            ItemSubategoryListTableView.reloadData()
            return
        }
        
        let Name = CategoryArray[SelectedRow].Name
        let Ikey = CategoryArray[SelectedRow].Ikey
        
        let CategoryNoteQuery = "SELECT categorynote FROM itemcategorytable WHERE ikey = \(Ikey)"
        
        print("CategoryNoteQuery: \(CategoryNoteQuery)")
        
        var NoteDB: String = ""
        
        do {
            
            let CategoryNoteQueryRun = try db.executeQuery(CategoryNoteQuery, values:nil)
            
            while CategoryNoteQueryRun.next() {
                NoteDB = CategoryNoteQueryRun.string(forColumn: "categorynote")!
                
                print("Ikey: \"\(Ikey)\"  NoteDB: \"\(NoteDB)\"  ")
                
            }
            
        } catch {
            
            let ErrorMessage = "ItemCategorySubcategoryViewController:\(#function) - Failed to get Category Note\n\(db.lastErrorMessage())\n\(CategoryNoteQuery))"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "ItemCategorySubcategoryViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to get Category Note\n\n\(CategoryNoteQuery)", DatabaseError: "\(db.lastErrorMessage())")
            
            return
            
        }
        
        ItemCategoryNoteTextView.string = NoteDB
        
        //////////////////////////////////////////////
        
        // pull the subcategories and load that table
        ItemSubcategoryNoteTextView.string = ""
        
        SubcategoryArray.removeAll()
        
        let SubCategoryNoteQuery = "SELECT * FROM itemsubcategorytable WHERE itemcategoryreferenceikey = \(Ikey)"
        
        print("SubCategoryNoteQuery: \(SubCategoryNoteQuery)")
        
        var SubName = String()
        var SubIkey = String()
        
        do {
            
            let SubCategoryNoteQueryRun = try db.executeQuery(SubCategoryNoteQuery, values:nil)
            
            while SubCategoryNoteQueryRun.next() {
                SubName = SubCategoryNoteQueryRun.string(forColumn: "subcategoryname")!
                SubIkey = SubCategoryNoteQueryRun.string(forColumn: "ikey")!
                
                print("SubName: \"\(SubName)\"  SubIkey: \"\(SubIkey)\"")
                
                let SubCategoriesToAdd = ItemCategorySubcategoryList.init(Ikey: SubIkey, Name: SubName, Count: 0 )
                
                SubcategoryArray.append(SubCategoriesToAdd)
                
            }
            
        } catch {
            
            let ErrorMessage = "ItemCategorySubcategoryViewController:\(#function) - Failed to get \"\(Name)\" Subcategories\n\(db.lastErrorMessage())\n\(SubCategoryNoteQuery))"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "ItemCategorySubcategoryViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to get \"\(Name)\" Subcategories \n\n\(SubCategoryNoteQuery)", DatabaseError: "\(db.lastErrorMessage())")
            
            return
            
        }
        
        ItemSubategoryListTableView.reloadData()
        
    }
    
    
    func RefreshSubCategoryTable() {
        
        let SelectedRow = ItemCategoryListTableView.selectedRow
        
        print("SelectedRow: \(SelectedRow)")
        
        if (SelectedRow == -1) {
            SubcategoryArray.removeAll()
            ItemSubategoryListTableView.reloadData()
            return
        }
        
        let Ikey = CategoryArray[SelectedRow].Ikey
        let Name = CategoryArray[SelectedRow].Name
        
        ///////////////////////////////////////////////
        
        SubcategoryArray.removeAll()
        
        var SubCategoryNoteQuery = "SELECT * FROM itemsubcategorytable WHERE itemcategoryreferenceikey = \(Ikey)"
        
        print("SubCategoryNoteQuery: \(SubCategoryNoteQuery)")
        
        if (SubSearchBool == true) {
            
            SubCategoryNoteQuery = "select ikey,subcategoryname from itemsubcategorytable where itemcategoryreferenceikey = \(Ikey) and subcategoryname LIKE '%\(subSearchField.stringValue)%'"
            
            print("SubCategoryNoteQuery Search: \(SubCategoryNoteQuery)")
            
        }
        
        
        var SubName = String()
        var SubIkey = String()
        
        do {
            
            let SubCategoryNoteQueryRun = try db.executeQuery(SubCategoryNoteQuery, values:nil)
            
            while SubCategoryNoteQueryRun.next() {
                SubName = SubCategoryNoteQueryRun.string(forColumn: "subcategoryname")!
                SubIkey = SubCategoryNoteQueryRun.string(forColumn: "ikey")!
                
                print("SubName: \"\(SubName)\"  SubIkey: \"\(SubIkey)\"")
                
                let SubCategoriesToAdd = ItemCategorySubcategoryList.init(Ikey: SubIkey, Name: SubName, Count: 0 )
                
                SubcategoryArray.append(SubCategoriesToAdd)
                
            }
            
        } catch {
            
            let ErrorMessage = "ItemCategorySubcategoryViewController:\(#function) - Failed to get \"\(Name)\" Subcategories\n\(db.lastErrorMessage())\n\(SubCategoryNoteQuery))"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "ItemCategorySubcategoryViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to get \"\(Name)\" Subcategories \n\n\(SubCategoryNoteQuery)", DatabaseError: "\(db.lastErrorMessage())")
            
            return
            
        }
        
        ItemSubcategoryNoteTextView.string = ""
        ItemSubategoryListTableView.reloadData()
        
        
    }
    
    
    
    
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    @IBAction func ItemCategoryAddCategoryButtonAction(_ sender: Any) {
        
        let CategoryNameLabelWhiteSpace = RemoveWhiteSpace(aString: ItemCategoryAddTextField.stringValue)
        
        if (CategoryNameLabelWhiteSpace == "") {
            
            let alert = NSAlert()
            alert.messageText = "Category name is empty"
            alert.informativeText = "Please enter a name for the Category"
            alert.alertStyle = .warning
            
            if (CurrentAppearanceItemCategoryViewViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.window.titlebarAppearsTransparent = true
            alert.addButton(withTitle: "OK")
            
            if alert.runModal() == .alertFirstButtonReturn {
                print("Ok Pressed")
                return
            }

        }// end of if
        
        
        
        
        let InsertNewCategorySqlquery = String(format: "INSERT INTO itemcategorytable (ikey,categoryname,categorynote) VALUES ((select MAX(ikey)+ 1 from itemcategorytable),\"\(CategoryNameLabelWhiteSpace)\",\"\") ")
        
        print("InsertNewCategorySqlquery: \(InsertNewCategorySqlquery)")

        do {

            try db.executeUpdate(InsertNewCategorySqlquery, values: nil)
            
        } catch let error as NSError {
            
            let ErrorMessage = "ItemCategorySubcategoryViewController: \(#function) - Failed to do insert new Category\n \"\(InsertNewCategorySqlquery)\" \nError: \(db.lastErrorMessage())\n\n\(error)"
            
            print(ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "ItemCategorySubcategoryViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to insert new Category.\nPlease quit then try again.\n\n\"\(InsertNewCategorySqlquery)\"\n\n\"\(error)\" ", DatabaseError: "\(db.lastErrorMessage())")

            return
            
        }
        
        ItemCategoryAddTextField.stringValue = ""
        
        LoadCategoryTable()
        
        
    }// end of ItemCategoryAddCategoryButtonAction
    
    
    
    
    @IBAction func ItemSubCategoryAddButtonAction(_ sender: Any) {
        
        
        let SubCategoryNameLabelWhiteSpace = RemoveWhiteSpace(aString: ItemSubcategoryAddTextField.stringValue)
        
        if (SubCategoryNameLabelWhiteSpace == "") {
            
            let alert = NSAlert()
            alert.messageText = "Subcategory name is empty"
            alert.informativeText = "Please enter a name for the Subcategory"
            alert.alertStyle = .warning
            
            if (CurrentAppearanceItemCategoryViewViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.window.titlebarAppearsTransparent = true
            alert.addButton(withTitle: "OK")
            
            if alert.runModal() == .alertFirstButtonReturn {
                print("Ok Pressed")
                return
            }
            
            return
            
        }// end of if
        
        ///////////////////////
        
        let SelectedRow = ItemCategoryListTableView.selectedRow
        
        print("SelectedRow: \(SelectedRow)")
        
        if (SelectedRow == -1) {
            
            let alert = NSAlert()
            alert.messageText = "No Category is Selected"
            alert.informativeText = "Please select a Category"
            alert.alertStyle = .warning
            
            if (CurrentAppearanceItemCategoryViewViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.window.titlebarAppearsTransparent = true
            alert.addButton(withTitle: "OK")
            
            if alert.runModal() == .alertFirstButtonReturn {
                print("Ok Pressed")
                return
            }
            
            return
        }
        
        let CategoryIkey = CategoryArray[SelectedRow].Ikey
        
        //////////////////////
        
        
        let InsertNewSubCategorySqlquery = String(format: "INSERT INTO itemsubcategorytable (ikey,itemcategoryreferenceikey,subcategoryname,subcategorynote) VALUES ((select MAX(ikey)+ 1 from itemsubcategorytable),\"\(CategoryIkey)\",\"\(SubCategoryNameLabelWhiteSpace)\",\"\") ")
        
        print("InsertNewSubCategorySqlquery: \(InsertNewSubCategorySqlquery)")
        
        
        
        
        do {
            
            try db.executeUpdate(InsertNewSubCategorySqlquery, values: nil)
            
        } catch let error as NSError {
            
            let ErrorMessage = "ItemCategorySubcategoryViewController: \(#function) - Failed to do insert new Subcategory\n \"\(InsertNewSubCategorySqlquery)\" \nError: \(db.lastErrorMessage())\n\n\(error)"
            
            print(ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "ItemCategorySubcategoryViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to insert new Subcategory.\nPlease quit then try again.\n\n\"\(InsertNewSubCategorySqlquery)\"\n\n\"\(error)\" ", DatabaseError: "\(db.lastErrorMessage())")
            
            return
            
        }
        
        
        let SelectedRowCategory = ItemCategoryListTableView.selectedRow
        
        
        
        LoadCategoryTable()
        RefreshSubCategoryTable()
        ItemSubcategoryAddTextField.stringValue = ""
        
        ItemCategoryListTableView.selectRowIndexes(NSIndexSet(index: SelectedRowCategory) as IndexSet, byExtendingSelection: false)
        ItemSubategoryListTableView.selectRowIndexes(NSIndexSet(index: SelectedRow) as IndexSet, byExtendingSelection: false)
        
        
        
    }//end of ItemSubCategoryAddButtonAction
    
    
    
    @IBAction func deleteCategoryButtonAction(_ sender: Any) {
        
        
        print("deleteCategoryButtonAction Pressed")
        
        
        let SelectedRow = ItemCategoryListTableView.selectedRow
        
        if (SelectedRow == -1) {
            return
        }
        
        let Name = CategoryArray[SelectedRow].Name
        let Ikey = CategoryArray[SelectedRow].Ikey
        let Count = CategoryArray[SelectedRow].Count
        
        if (Count > 0) {
            
            let alert = NSAlert()
            alert.messageText = "The Category \"\(Name)\" has \(Count) Subcategories associated with it."
            alert.informativeText = "You will need to remove the Subcategories before you can remove the Category."
            alert.alertStyle = .warning
            alert.addButton(withTitle: "Ok")

            
            if (CurrentAppearanceItemCategoryViewViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.window.titlebarAppearsTransparent = true
            
            let returnCode = alert.runModal()
            print("returnCode: \(returnCode)")
            
            if (returnCode.rawValue == 1000) {
                print("OK Pressed")
            }
            
            
            
        } else {
            
            let alert = NSAlert()
            alert.messageText = "Are you sure you want to Delete  the Category \"\(Name)\" "
            alert.informativeText = "This action cannot be undone."
            alert.alertStyle = .warning
            alert.addButton(withTitle: "Canel")
            alert.addButton(withTitle: "Yes Delete")
            
            
            
            if (CurrentAppearanceItemCategoryViewViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.window.titlebarAppearsTransparent = true
            
            let returnCode = alert.runModal()
            print("returnCode: \(returnCode)")
            
            if (returnCode.rawValue == 1000) {
                print("Canel Pressed")
            }
            if (returnCode.rawValue == 1001) {
                
                print("Yes Delete Pressed")
                
                let DeleteCategoryQuery = "DELETE FROM itemcategorytable WHERE ikey = \"\(Ikey)\" and categoryname = \"\(Name)\" "
                
                do {
                    
                    try db.executeUpdate(DeleteCategoryQuery, values:nil)
                    
                } catch {
                    
                    let ErrorMessage = "LocationsVieController:\(#function) - Failed to Delete Category \"\(Name)\" \n\(DeleteCategoryQuery)\n\(db.lastErrorMessage())\n\(DeleteCategoryQuery))"
                    
                    print(ErrorMessage)
                    
                    loggly(LogType.Error, text: ErrorMessage)
                    
                    DoAlert.DisplayAlert(Class: "LocationsViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to delete Cateorry \"\(Name)\"\n\n\(DeleteCategoryQuery)", DatabaseError: "\(db.lastErrorMessage())")
                    
                }
                
                LoadCategoryTable()
                
                
            } // end of 1000
            
        }// end of else
        
        
        
        
        
        
        
        
        
        
    }// end of deleteCategoryButtonAction
    
    
    @IBAction func deleteSubcategoryButtonAction(_ sender: Any) {

        print("deleteSubcategoryButtonAction Pressed")
        
        let SelectedRow = ItemSubategoryListTableView.selectedRow
        
        if (SelectedRow == -1) {
            return
        }
        
        let Name = SubcategoryArray[SelectedRow].Name
        let Ikey = SubcategoryArray[SelectedRow].Ikey
        
        let alert = NSAlert()
        alert.messageText = "Are you sure you want to Delete Subcategory \"\(Name)\" "
        alert.informativeText = "This action cannot be undone."
        alert.alertStyle = .warning
        alert.addButton(withTitle: "Canel")
        alert.addButton(withTitle: "Yes Delete")
        
        
        if (CurrentAppearanceItemCategoryViewViewController == "Dark") {
            alert.window.backgroundColor = AlertBackgroundColorDarkMode
        } else {
            alert.window.backgroundColor = WindowBackgroundColor
        }
        
        alert.window.titlebarAppearsTransparent = true
        
        let returnCode = alert.runModal()
        print("returnCode: \(returnCode)")
        
        if (returnCode.rawValue == 1000) {
            print("Canel Pressed")
        }
        if (returnCode.rawValue == 1001) {
            
            print("Yes Delete Pressed")
            
            let DeleteSubcategoryQuery = "DELETE FROM itemsubcategorytable WHERE ikey = \"\(Ikey)\" and subcategoryname = \"\(Name)\" "
            
            do {
                
                try db.executeUpdate(DeleteSubcategoryQuery, values:nil)
                
            } catch {
                
                let ErrorMessage = "LocationsViewController:\(#function) - Failed to Delete Subcategory \"\(Name)\"\n\(DeleteSubcategoryQuery)\n\(db.lastErrorMessage())\n\(DeleteSubcategoryQuery))"
                
                print(ErrorMessage)
                
                loggly(LogType.Error, text: ErrorMessage)
                
                DoAlert.DisplayAlert(Class: "LocationsViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to delete Subcategory \"\(Name)\"\n\n\(DeleteSubcategoryQuery)", DatabaseError: "\(db.lastErrorMessage())")
                
            }
            
            let SelectedRowCategoryTable = ItemCategoryListTableView.selectedRow
            
            LoadCategoryTable()
            //RefreshTables()
            RefreshSubCategoryTable()

            ItemCategoryListTableView.selectRowIndexes(NSIndexSet(index: SelectedRowCategoryTable) as IndexSet, byExtendingSelection: false)

        } // end of 1000
        
    }// end of deleteSubcategoryButtonAction
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    @IBAction func ItemWindowCloseButtonAction(_ sender: Any) {
        
        
        print("ItemWindowCloseButtonAction RUN")
        
        self.view.window?.close()
        
        // LoadListTableData
        
       ItemsWindow.showWindow(self)
        
    }// end of ItemWindowCloseButtonAction
    
    
    func RemoveWhiteSpace(aString: String) -> String {
        let replaced = aString.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        return replaced
    }
    
    
    
} // end of class ItemCategorySubcategoryViewController: NSViewController
