//
//  ItemViewController.swift
//  Scripto Portfolio
//
//  Created by Dorian Glanville on 8/16/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Cocoa
import SwiftLoggly

class ItemViewController: NSViewController,NSTextViewDelegate,NSTextFieldDelegate {
    
    
    @IBOutlet weak var itemNameTextfield: NSTextField!
    @IBOutlet weak var itemCategoryPopupButton: NSPopUpButton!
    @IBOutlet weak var itemSubcategoryPopupButton: NSPopUpButton!
    @IBOutlet weak var itemCreateDateDatePicker: NSDatePicker!
    @IBOutlet weak var itemCreatedByTextField: NSTextField!
    @IBOutlet weak var itemAgeTextField: NSTextField!
    @IBOutlet weak var itemUsedForTextField: NSTextField!
    @IBOutlet weak var itemPictureButton: NSButton!
    @IBOutlet var itemComposistionTextView: NSTextView!
    @IBOutlet var itemHistoryTextView: NSTextView!
    
    @IBOutlet weak var itemInfoWindowCloseButton: NSButton!
    
    @IBOutlet weak var itemNameLabel: NSTextField!
    @IBOutlet weak var itemCategoryLabel: NSTextField!
    @IBOutlet weak var itemSubcategoryLabel: NSTextField!
    @IBOutlet weak var itemCreateDateLabel: NSTextField!
    @IBOutlet weak var itemCreatedByLabel: NSTextField!
    @IBOutlet weak var itemUsedFoerLabel: NSTextField!
    @IBOutlet weak var itemComposistionLabel: NSTextField!
    @IBOutlet weak var itemHistoryLabel: NSTextField!
    @IBOutlet weak var itemAgeLabel: NSTextField!
    

    /////////////////////////////
    
    var CategoryListArray = ["Select a Category"]
    var SubCategoryListArray = ["Select a Subcategory"]
    
    var CurrentAppearanceItemsViewViewController = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        

        
        
        
        
        itemComposistionTextView.delegate = self
        itemHistoryTextView.delegate = self
        
        itemNameTextfield.delegate = self
        itemCreatedByTextField.delegate = self
        itemUsedForTextField.delegate = self
        
        
        
        
        
    }// end of viewDidLoad()
    
    override func  viewWillAppear() {
        
        
        itemCategoryPopupButton.removeAllItems()
        itemSubcategoryPopupButton.removeAllItems()
        
        LoadCategorySubcategoryDropDowns()
        
        LoadItem(itemikey: itemIkeyToLoad, editable: itemBool)
        
    }// end of dviewWillAppear()
    
    
    override func viewDidLayout() {
        
        //        enum InterfaceStyle : String {
        //            case Dark, Light
        //
        //            init() {
        //                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
        //                self = InterfaceStyle(rawValue: type)!
        //            }
        //        }
        //
        //        let currentStyle = InterfaceStyle()
        //
        //        //print("currentStyle: \"\(currentStyle)\"  -  CurrentAppearance: \"\(CurrentAppearance)\" ")
        //
        //        if ("\(currentStyle)" != CurrentAppearanceNewUserViewController) {
        //            appearanceCheck()
        //        }
        
        appearanceCheck()
        
        super.viewDidLayout()
        
        
        
        
    }// end of viewDidLayout()
    
    
    func appearanceCheck() {
        
        enum InterfaceStyle : String {
            case Dark, Light
            
            init() {
                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
                self = InterfaceStyle(rawValue: type)!
            }
        }
        
        let currentStyle = InterfaceStyle()
        
        //print("ItemsViewViewController currentStyle \"\(currentStyle)\" CurrentAppearanceItemsViewViewController: \"\(CurrentAppearanceItemsViewViewController)\"")
        
        if ("\(currentStyle)" != CurrentAppearanceItemsViewViewController) {
            
            //print("ItemsViewViewController currentStyle \"\(currentStyle)\" CurrentAppearanceItemsViewViewController: \"\(CurrentAppearanceItemsViewViewController)\"")
            
            if (currentStyle.rawValue == "Dark") {
                
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColorDarkMode
                
//                itemComposistionTextView.wantsLayer = true
//                itemComposistionTextView.backgroundColor = NSColor.gray
//                
//                itemHistoryTextView.wantsLayer = true
//                itemHistoryTextView.backgroundColor = NSColor.gray
                
            
                itemInfoWindowCloseButton.styleButtonText(button: itemInfoWindowCloseButton, buttonName: "Close", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                itemInfoWindowCloseButton.wantsLayer = true
                itemInfoWindowCloseButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                itemInfoWindowCloseButton.layer?.cornerRadius = 7
                
                itemNameLabel.font = NSFont(name:AppFontBold, size: 13.0)
                itemNameLabel.textColor = NSColor.gray
                
                itemCategoryLabel.font = NSFont(name:AppFontBold, size: 13.0)
                itemCategoryLabel.textColor = NSColor.gray
                
                itemSubcategoryLabel.font = NSFont(name:AppFontBold, size: 13.0)
                itemSubcategoryLabel.textColor = NSColor.gray
                
                itemCreateDateLabel.font = NSFont(name:AppFontBold, size: 13.0)
                itemCreateDateLabel.textColor = NSColor.gray
                
                itemCreatedByLabel.font = NSFont(name:AppFontBold, size: 13.0)
                itemCreatedByLabel.textColor = NSColor.gray
                
                itemAgeLabel.font = NSFont(name:AppFontBold, size: 13.0)
                itemAgeLabel.textColor = NSColor.gray
                
                itemUsedFoerLabel.font = NSFont(name:AppFontBold, size: 13.0)
                itemUsedFoerLabel.textColor = NSColor.gray
                
                itemComposistionLabel.font = NSFont(name:AppFontBold, size: 13.0)
                itemComposistionLabel.textColor = NSColor.gray
                
                itemHistoryLabel.font = NSFont(name:AppFontBold, size: 13.0)
                itemHistoryLabel.textColor = NSColor.gray
                
                
                
            } else if ("\(currentStyle.rawValue)" == "Light") {
                
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColor.cgColor
                
                itemComposistionTextView.wantsLayer = true
                itemComposistionTextView.backgroundColor = NSColor.white
                
                itemHistoryTextView.wantsLayer = true
                itemHistoryTextView.backgroundColor = NSColor.white
                
                itemInfoWindowCloseButton.styleButtonText(button: itemInfoWindowCloseButton, buttonName: "Close", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                itemInfoWindowCloseButton.wantsLayer = true
                itemInfoWindowCloseButton.layer?.backgroundColor = NSColor.gray.cgColor
                itemInfoWindowCloseButton.layer?.cornerRadius = 7
                
                itemNameLabel.font = NSFont(name:AppFontBold, size: 13.0)
                itemNameLabel.textColor = NSColor.white
                
                itemCategoryLabel.font = NSFont(name:AppFontBold, size: 13.0)
                itemCategoryLabel.textColor = NSColor.white
                
                itemSubcategoryLabel.font = NSFont(name:AppFontBold, size: 13.0)
                itemSubcategoryLabel.textColor = NSColor.white
                
                itemCreateDateLabel.font = NSFont(name:AppFontBold, size: 13.0)
                itemCreateDateLabel.textColor = NSColor.white
                
                itemCreatedByLabel.font = NSFont(name:AppFontBold, size: 13.0)
                itemCreatedByLabel.textColor = NSColor.white
                
                itemAgeLabel.font = NSFont(name:AppFontBold, size: 13.0)
                itemAgeLabel.textColor = NSColor.white
                
                itemUsedFoerLabel.font = NSFont(name:AppFontBold, size: 13.0)
                itemUsedFoerLabel.textColor = NSColor.white
                
                itemComposistionLabel.font = NSFont(name:AppFontBold, size: 13.0)
                itemComposistionLabel.textColor = NSColor.white
                
                itemHistoryLabel.font = NSFont(name:AppFontBold, size: 13.0)
                itemHistoryLabel.textColor = NSColor.white
                
            }
        }
        
        CurrentAppearanceItemsViewViewController = "\(currentStyle)"
        
        
    }//end of appearanceCheck()
    
    
    
    
    func doubleClickOnPictureButton() {
        
        
        
        
        
        
    }
    
    
    
    func LoadCategorySubcategoryDropDowns() {
        
        let CategoryQuery = "select categoryname from itemcategorytable "
        
        var CategoryNameDB: String!
        
        do {
            
            let ItemCategoryQuery = try db.executeQuery(CategoryQuery, values:nil)
            
            
            while ItemCategoryQuery.next() {
                CategoryNameDB = ItemCategoryQuery.string(forColumn: "categoryname")
                
                CategoryListArray.append(CategoryNameDB)
                
            }
            
            
            
        } catch {
            
            
            
            let ErrorMessage = "ItemsListViewController:\(#function) - Failed to get Category list\n\n\(db.lastErrorMessage())\n\(CategoryQuery))"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "ItemsListViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to get Category list\n\n\(CategoryQuery)", DatabaseError: "\(db.lastErrorMessage())")
            
        }
        
        for item in CategoryListArray {
            print("CategoryListArray: \(item)")
        }
        
        itemCategoryPopupButton.addItems(withTitles: CategoryListArray)
        
        
        ///////////////////////////////////////////////////////////////////////
        
        
//        let SubCategoryQuery = "select subcategoryname from itemsubcategorytable "
//
//        var SubCategoryNameDB: String!
//
//        do {
//
//            let ItemSubCategoryQuery = try db.executeQuery(SubCategoryQuery, values:nil)
//
//
//            while ItemSubCategoryQuery.next() {
//                SubCategoryNameDB = ItemSubCategoryQuery.string(forColumn: "subcategoryname")
//
//                SubCategoryListArray.append(SubCategoryNameDB)
//
//            }
//            
//
//            
//        } catch {
//            
//            
//
//            let ErrorMessage = "ItemsListViewController:\(#function) - Failed to get SubCategory list\n\n\(db.lastErrorMessage())\n\(SubCategoryQuery))"
//
//            print(ErrorMessage)
//
//            loggly(LogType.Error, text: ErrorMessage)
//
//            DoAlert.DisplayAlert(Class: "ItemsListViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to get SubCategory list\n\n\(SubCategoryQuery)", DatabaseError: "\(db.lastErrorMessage())")
//
//        }
//
//        for item in SubCategoryListArray {
//            print("SubCategoryListArray: \(item)")
//        }
//
//        itemSubcategoryPopupButton.addItems(withTitles: SubCategoryListArray)
        
        
        
        
        
        
        
    }// end of LoadCategorySubcategoryDropDowns()
    
    
    
    func LoadItem(itemikey: Int, editable: Bool) {
        

        
       
        var itemName: String?
        var itemCategory: String?
        var itemSubcategory: String?
        var itemCreateDate: String?
        var itemCreatedBy: String?
        var itemUsedBy: String?
        var itemComposision: String?
        var itemHistory: String?
        
        
        let ItemQuery = "select * from itemtable where ikey = \(itemikey) and userikeyreference = \(CurrentUserIkey!) "
        
        print("ItemQuery: \(ItemQuery)")

        do {
            
            let ItemQueryRun = try db.executeQuery(ItemQuery, values:nil)

            while ItemQueryRun.next() {
    
                itemName = ItemQueryRun.string(forColumn: "name")
                itemCategory = ItemQueryRun.string(forColumn: "itemcategory")
                itemSubcategory = ItemQueryRun.string(forColumn: "itemsubcategory")
                itemCreateDate = ItemQueryRun.string(forColumn: "createdate")
                itemCreatedBy = ItemQueryRun.string(forColumn: "createdby")
                itemUsedBy = ItemQueryRun.string(forColumn: "usedfor")
                itemComposision = ItemQueryRun.string(forColumn: "composition")
                itemHistory = ItemQueryRun.string(forColumn: "history")
                
                print("itemName: \(itemName ?? "BLANK") - itemCategory: \(itemCategory ?? "BLANK") - itemSubcategory: \(itemSubcategory ?? "BLANK") itemCreateDate: \(itemCreateDate ?? "BLANK") - itemCreatedBy: \(itemCreatedBy ?? "BLANK") - itemUsedBy: \(itemUsedBy ?? "BLANK") itemComposision: \(itemComposision ?? "BLANK") - itemHistory: \(itemHistory ?? "BLANK") ")
               
            }
            
        } catch {
            
            print("ERROR: ItemViewController: Failed to get \"\(CurrentUserName!)\"'s Item from the Database\n\(ItemQuery)\n\(db.lastErrorMessage())")
            
            let ErrorMessage = "ItemViewController:\(#function) - Failed to get \"\(CurrentUserName!)\"'s Item from the Database\n\(db.lastErrorMessage())\n\(ItemQuery))"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "ItemViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to get \"\(CurrentUserName!)\"'s Item from the Database\n\n\(ItemQuery)", DatabaseError: "\(db.lastErrorMessage())")
            
            return
            
        }
        
        
        ///////////////////////////////////////////////////////
        
        
        
        if (itemCategory != "" && itemCategory != nil) {
            
            let ItemCategoryQuery = "select categoryname from itemcategorytable where ikey = \(itemCategory!)"
            
            var itemCategoryName: String?
            
            
            do {
                
                let ItemCategoryQueryRun = try db.executeQuery(ItemCategoryQuery, values:nil)
                
                while ItemCategoryQueryRun.next() {
                    
                    itemCategoryName = ItemCategoryQueryRun.string(forColumn: "categoryname")
                    
                }
                
            } catch {
                
                print("ERROR: ItemViewController: Failed to get \"\(CurrentUserName!)\"'s Item Category from the Database\n\(ItemCategoryQuery)\n\(db.lastErrorMessage())")
                
                let ErrorMessage = "ItemViewController:\(#function) - Failed to get \"\(CurrentUserName!)\"'s Item from the Database\n\(db.lastErrorMessage())\n\(ItemCategoryQuery))"
                
                print(ErrorMessage)
                
                loggly(LogType.Error, text: ErrorMessage)
                
                DoAlert.DisplayAlert(Class: "ItemViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to get \"\(CurrentUserName!)\"'s Item Category from the Database\n\n\(ItemCategoryQuery)", DatabaseError: "\(db.lastErrorMessage())")
                
                return
                
            }
            
            itemCategoryPopupButton.setTitle("\(itemCategoryName!)")
            
            ////////////////////////////////////////////////////////
            
            let SubCategoryQuery = "select subcategoryname from itemsubcategorytable where itemcategoryreferenceikey = \(itemCategory!)"
            
            print("SubCategoryQuery: \(SubCategoryQuery)")
            
            var SubCategoryNameDB: String!
            
            do {
                
                let ItemSubCategoryQuery = try db.executeQuery(SubCategoryQuery, values:nil)
                
                while ItemSubCategoryQuery.next() {
                    SubCategoryNameDB = ItemSubCategoryQuery.string(forColumn: "subcategoryname")
                    
                    SubCategoryListArray.append(SubCategoryNameDB)
                    
                }
                
            } catch {
                
                let ErrorMessage = "ItemsListViewController:\(#function) - Failed to get SubCategory list\n\n\(db.lastErrorMessage())\n\(SubCategoryQuery))"
                
                print(ErrorMessage)
                
                loggly(LogType.Error, text: ErrorMessage)
                
                DoAlert.DisplayAlert(Class: "ItemsListViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to get SubCategory list\n\n\(SubCategoryQuery)", DatabaseError: "\(db.lastErrorMessage())")
                
            }
            
            for item in SubCategoryListArray {
                print("SubCategoryListArray: \(item)")
            }
            
            itemSubcategoryPopupButton.addItems(withTitles: SubCategoryListArray)
            
        } else {
            itemCategoryPopupButton.setTitle("Select a Category")
            itemSubcategoryPopupButton.addItems(withTitles: SubCategoryListArray)
            itemSubcategoryPopupButton.setTitle("Select a Subcategory")
        }
        
        
        
        
        if (itemSubcategory != "" && itemSubcategory != nil) {
            
            let ItemSubCategoryQuery = "select subcategoryname from itemsubcategorytable where ikey = \(itemSubcategory!)"
            
            print("ItemSubCategoryQuery: \(ItemSubCategoryQuery)")
            
            var itemSubCategoryName: String?
            
            
            do {
                
                let ItemSubCategoryQueryRun = try db.executeQuery(ItemSubCategoryQuery, values:nil)
                
                while ItemSubCategoryQueryRun.next() {
                    
                    itemSubCategoryName = ItemSubCategoryQueryRun.string(forColumn: "subcategoryname")
                    
                }
                
            } catch {
                
                
                
                let ErrorMessage = "ItemViewController:\(#function) - Failed to get \"\(CurrentUserName!)\"'s Item SubCategory from the Database\n\(db.lastErrorMessage())\n\(ItemSubCategoryQuery))"
                
                print(ErrorMessage)
                
                loggly(LogType.Error, text: ErrorMessage)
                
                DoAlert.DisplayAlert(Class: "ItemViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to get \"\(CurrentUserName!)\"'s Item SubCategory from the Database\n\n\(ItemSubCategoryQuery)", DatabaseError: "\(db.lastErrorMessage())")
                
                return
                
            }
            
            itemSubcategoryPopupButton.setTitle("\(itemSubCategoryName!)")
        } else {
            
            itemSubcategoryPopupButton.setTitle("Select a Subcategory")
        }
        
        
        
        ////////////////////////////
        
        itemNameTextfield.stringValue = "\(itemName ?? "BLANK")"
        
        
        let NowDate = Date()
        
        if (itemCreateDate == "" || itemCreateDate == nil) {
            itemCreateDateDatePicker.dateValue = NowDate
            itemAgeTextField.stringValue = "0"
        } else {
        
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ssZ"
            let dateDB = dateFormatter.date(from: itemCreateDate!)
            itemCreateDateDatePicker.dateValue = dateDB!
            
            let calendar = NSCalendar.current
            let date1 = calendar.startOfDay(for: dateDB!)
            let date2 = calendar.startOfDay(for: NowDate)
            let components = calendar.dateComponents([.year], from: date1, to: date2)
            itemAgeTextField.stringValue = "\(components.year!)"
        
        }
        
        if (itemCreatedBy != nil && itemCreatedBy!.count > 0) {
            itemCreatedByTextField.stringValue = "\(itemCreatedBy!)"
        }
        
        if (itemUsedBy != nil && itemUsedBy!.count > 0) {
            itemUsedForTextField.stringValue = "\(itemUsedBy!)"
        }
        
        if (itemComposision != nil && itemComposision!.count > 0) {
            itemComposistionTextView.string = "\(itemComposision!)"
        }
        
        if (itemHistory != nil && itemHistory!.count > 0) {
            itemHistoryTextView.string = "\(itemHistory!)"
        }
        
        //////////////////////////////////////////////////////////////////
        
        
        
        let ItemImagePathQuery = "SELECT pathtoimage FROM imagetable WHERE itemreference = \"\(itemIkeyToLoad)\" "
        
        print("ItemImagePathQuery: \"\(ItemImagePathQuery)\"")
        
        var ItemImagePathDB: String?
        
        do {
            
            let ItemImagePathQueryRun = try db.executeQuery(ItemImagePathQuery, values:nil)
            
            while ItemImagePathQueryRun.next() {
                ItemImagePathDB = ItemImagePathQueryRun.string(forColumn: "pathtoimage")
            }

        } catch {
            
            let ErrorMessage = "ItemViewController:\(#function) - Failed to get Item pathtoimage Image Reference\n\n\(db.lastErrorMessage())\n\(ItemImagePathQuery))"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "ItemViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to get Item pathtoimage Image Reference\n\n\(ItemImagePathQuery)", DatabaseError: "\(db.lastErrorMessage())")
            return
        }
        
        print("ItemImagePathDB: \"\(ItemImagePathDB)\" ")
        
        if (ItemImagePathDB == nil || ItemImagePathDB == "") {
            itemPictureButton.image = NSImage(named: NSImage.Name(rawValue: "ChooseAnImage.png"))
        } else {
            itemPictureButton.image = NSImage(contentsOfFile: NSImage.Name(rawValue: ItemImagePathDB!).rawValue)
        }
        
        
        ///////////////////////////////////////////////////////////
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
    }// end of LoadItem()
    
    
    func textDidChange(_ notification: Notification) {
        
        print("textDidChange")
        
        guard let TextV = notification.object as? NSTextView else { return }
        
        if ((TextV.string.count) < 1) {
            return
        }
        
        let lastCharacter = String(describing: TextV.string.last!)
        
        let SymbolTest = CharacterSet.symbols.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
        
        let punctuationTest = CharacterSet.punctuationCharacters.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
        
        //            let SpaceNewLineTest = CharacterSet.whitespacesAndNewlines.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
        
        //let NewLineTest = CharacterSet.newlines.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
        
        //if (SymbolTest || punctuationTest || NewLineTest) {
        if (SymbolTest || punctuationTest) {
            
            __NSBeep()
            
            TextV.string = String(TextV.string.dropLast())
            
            return
        }
        
        
        if (TextV == itemComposistionTextView) {
            SaveItemComposistion()
            print("itemComposistionTextView CHANGED & SAVED")
        }
        
        if (TextV == itemHistoryTextView) {
            SaveItemHistory()
            print("itemHistoryTextView CHANGED & SAVED")
        }
        
        
        
        
//        guard let textView = notification.object as? NSTextView else { return }
//        switch textView {
//        case mainTextView:
//            print("mainTextView changed")
//        case findPanelFindTextView:
//            print("findPanelFindTextView changed")
//        default:
//            break
//        }
    }
    
    
    override func controlTextDidChange(_ obj: Notification) {
        
        guard let textF = obj.object as? NSTextField else { return }
        
        if ((textF.stringValue.count) < 1) {
            return
        }
        
        let lastCharacter = String(describing: textF.stringValue.last!)
        
        let SymbolTest = CharacterSet.symbols.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
        
        let punctuationTest = CharacterSet.punctuationCharacters.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
        
        //            let SpaceNewLineTest = CharacterSet.whitespacesAndNewlines.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
        
        let NewLineTest = CharacterSet.newlines.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
        
        if (SymbolTest || punctuationTest || NewLineTest) {
            
            __NSBeep()
            
            textF.stringValue = String(textF.stringValue.dropLast())
            
            return
        }
        
        
    }

    
    
    @IBAction func itemNameTextFieldAction(_ sender: Any) {
        
        if (itemNameTextfield.stringValue.count < 1) {
            
            let alert = NSAlert()
            alert.messageText = "This Item Needs A Name"
            alert.informativeText = "Please Choose A Name For This Item"
            alert.alertStyle = .warning
            alert.addButton(withTitle: "Ok")

            
            
            
            
            alert.window.titlebarAppearsTransparent = true
            
            alert.runModal()
            
//            let returnCode = alert.runModal()
//
//            if (returnCode.rawValue == 1000) {
//            }
            return
            
        }
        
        
        
        let SaveItemNameQuery = "UPDATE itemtable SET name = \"\(itemNameTextfield.stringValue)\" WHERE ikey = \"\(itemIkeyToLoad)\""
        
        do {
            
            try db.executeUpdate(SaveItemNameQuery, values:nil)
            
        } catch {
            
            let ErrorMessage = "ItemViewController:\(#function) - Failed to Save \"\(itemNameTextfield.stringValue)\" to the Database\n\(db.lastErrorMessage())\n\(SaveItemNameQuery))"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "ItemViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to Save \"\(itemNameTextfield.stringValue)\" to the Database\n\n\(SaveItemNameQuery)", DatabaseError: "\(db.lastErrorMessage())")
            
            return
            
        }

        
        print("Name Button Action Triggered & SAVED")
        
    }
    
    @IBAction func itemCategoryPopupButtonAction(_ sender: Any) {
        print("itemCategoryPopupButtonAction Action Triggered")
        
        let PopupButtonSelection = itemCategoryPopupButton.titleOfSelectedItem!
        
        if (PopupButtonSelection == "Select a Category") {
            itemSubcategoryPopupButton.removeAllItems()
            itemSubcategoryPopupButton.addItem(withTitle: "Select a Subcategory")
            itemSubcategoryPopupButton.selectItem(withTitle: "Select a Subcategory")
            return
        }
        
        let CategoryQueryIkey = "SELECT ikey FROM itemcategorytable WHERE categoryname = \"\(PopupButtonSelection)\" "
        
        print("CategoryQueryIkey: \"\(CategoryQueryIkey)\"")
        
        var CategoryikeyDB: String!
        
        do {
            
            let CategoryQueryIkeyRun = try db.executeQuery(CategoryQueryIkey, values:nil)
            
            while CategoryQueryIkeyRun.next() {
                CategoryikeyDB = CategoryQueryIkeyRun.string(forColumn: "ikey")
            }

        } catch {

            let ErrorMessage = "ItemsListViewController:\(#function) - Failed to get Ikey for current Item\n\n\(db.lastErrorMessage())\n\(CategoryQueryIkey))"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "ItemsListViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to get CIkey for current Item\n\n\(CategoryQueryIkey)", DatabaseError: "\(db.lastErrorMessage())")
            return
        }
        
        
        
        ///////////////////////////
        
        let SaveItemCategoryQuery = "UPDATE itemtable SET itemcategory = \(Int(CategoryikeyDB)!) WHERE ikey = \(itemIkeyToLoad)"
        
        print("SaveItemCategoryQuery: \"\(SaveItemCategoryQuery)\"")
        
        do {
        
            try db.executeUpdate(SaveItemCategoryQuery, values:nil)
            
        } catch {
            
            let ErrorMessage = "ItemViewController:\(#function) - Failed to Save Items Category to the Database\n\(db.lastErrorMessage())\n\(SaveItemCategoryQuery))"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "ItemViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to Save Items Category to the Database\n\n\(SaveItemCategoryQuery)", DatabaseError: "\(db.lastErrorMessage())")
            
            return
            
        }

        
        ////////////////////////////////////////////////////////
        
        SubCategoryListArray = ["Select a Subcategory"]
        itemSubcategoryPopupButton.removeAllItems()
        
        let SubCategoryQuery = "select subcategoryname from itemsubcategorytable where itemcategoryreferenceikey = \(Int(CategoryikeyDB)!)"
        
        print("SubCategoryQuery: \(SubCategoryQuery)")
        
        var SubCategoryNameDB: String!
        
        do {
            
            let ItemSubCategoryQuery = try db.executeQuery(SubCategoryQuery, values:nil)
            
            while ItemSubCategoryQuery.next() {
                SubCategoryNameDB = ItemSubCategoryQuery.string(forColumn: "subcategoryname")
                
                SubCategoryListArray.append(SubCategoryNameDB)
                
            }
            
        } catch {
            
            let ErrorMessage = "ItemsListViewController:\(#function) - Failed to get SubCategory list\n\n\(db.lastErrorMessage())\n\(SubCategoryQuery))"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "ItemsListViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to get SubCategory list\n\n\(SubCategoryQuery)", DatabaseError: "\(db.lastErrorMessage())")
            
        }
        
//        for item in SubCategoryListArray {
//            print("SubCategoryListArray: \(item)")
//        }
        
        itemSubcategoryPopupButton.addItems(withTitles: SubCategoryListArray)
        
        
        
        
        
    }// end of itemCategoryPopupButtonAction
    
    @IBAction func itemSubcategoryPopupButtonAction(_ sender: Any) {
        
        let CategoryPopupButtonSelection = itemCategoryPopupButton.titleOfSelectedItem!
        let subcategoryPopupButtonSelection = itemSubcategoryPopupButton.titleOfSelectedItem!
        
        if (CategoryPopupButtonSelection == "Select a Category") {
            itemSubcategoryPopupButton.selectItem(withTitle: "Select a Subcategory")
            return;
        }
        
        if (subcategoryPopupButtonSelection == "Select a Subcategory") {
            return;
        }
        
        
        let SubCategoryPopupButtonSelection = itemSubcategoryPopupButton.titleOfSelectedItem!
        
        
        /////////////////////////////////
        
        let SubCategoryQueryIkey = "SELECT ikey FROM itemsubcategorytable WHERE subcategoryname = \"\(SubCategoryPopupButtonSelection)\" "

        print("SubCategoryQueryIkey: \"\(SubCategoryQueryIkey)\"")

        var SubCategoryikeyDB: String!

        do {

            let SubCategoryQueryIkeyRun = try db.executeQuery(SubCategoryQueryIkey, values:nil)

            while SubCategoryQueryIkeyRun.next() {
                SubCategoryikeyDB = SubCategoryQueryIkeyRun.string(forColumn: "ikey")
            }

        } catch {

            let ErrorMessage = "ItemsListViewController:\(#function) - Failed to get Ikey for current Item\n\n\(db.lastErrorMessage())\n\(SubCategoryikeyDB))"

            print(ErrorMessage)

            loggly(LogType.Error, text: ErrorMessage)

            DoAlert.DisplayAlert(Class: "ItemsListViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to get CIkey for current Item\n\n\(SubCategoryikeyDB)", DatabaseError: "\(db.lastErrorMessage())")
            return
        }
        
        ///////////////////////////
        
        let SaveItemSubCategoryQuery = "UPDATE itemtable SET itemsubcategory = \(Int(SubCategoryikeyDB)!) WHERE ikey = \(itemIkeyToLoad)"
        
        print("SaveItemSubCategoryQuery: \"\(SaveItemSubCategoryQuery)\"")
        
        do {
            
            try db.executeUpdate(SaveItemSubCategoryQuery, values:nil)
            
        } catch {
            
            let ErrorMessage = "ItemViewController:\(#function) - Failed to Save Items Subcategory to the Database\n\(db.lastErrorMessage())\n\(SaveItemSubCategoryQuery))"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "ItemViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to Save Items Subcategory to the Database\n\n\(SaveItemSubCategoryQuery)", DatabaseError: "\(db.lastErrorMessage())")
            
            return
            
        }
        
        
        ////////////////////////////////////////////////////////
        
        
        
        
        print("itemSubcategoryPopupButtonAction Action Triggered")
        
    }//end of itemSubcategoryPopupButtonAction
    
    @IBAction func itemCreateDateDatePickerAction(_ sender: Any) {
        
        let DateSelectedDatePicker = "\(itemCreateDateDatePicker.dateValue)"
        
        let SaveItemCreateDateQuery = "UPDATE itemtable SET createdate = \"\(DateSelectedDatePicker)\" WHERE ikey = \"\(itemIkeyToLoad)\""
        
        do {
            
            try db.executeUpdate(SaveItemCreateDateQuery, values:nil)
            
        } catch {
            
            let ErrorMessage = "ItemViewController:\(#function) - Failed to Save \"\(itemNameTextfield.stringValue)\" Create Date to the Database\n\(db.lastErrorMessage())\n\(SaveItemCreateDateQuery))"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "ItemViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to Save \"\(itemNameTextfield.stringValue)\" Create Date to the Database\n\n\(SaveItemCreateDateQuery)", DatabaseError: "\(db.lastErrorMessage())")
            
            return
            
        }
        
        ////////////////////////////////
        
        let SelecrtedDate = itemCreateDateDatePicker.dateValue
        let NowDate = Date()
        
        let calendar = NSCalendar.current
        let date1 = calendar.startOfDay(for: SelecrtedDate)
        let date2 = calendar.startOfDay(for: NowDate)
        let components = calendar.dateComponents([.year], from: date1, to: date2)
        
        itemAgeTextField.stringValue = "\(components.year!)"
        
        print("itemCreateDateDatePickerAction Action Triggered")
        
    }// end of itemCreateDateDatePickerAction
    
    @IBAction func itemCreatedByAction(_ sender: Any) {
        
        if (itemCreatedByTextField.stringValue.count < 1) {

            return
            
        }
        
        
        
        let SaveItemCreatedByQuery = "UPDATE itemtable SET createdby = \"\(itemCreatedByTextField.stringValue)\" WHERE ikey = \"\(itemIkeyToLoad)\""
        
        do {
            
            try db.executeUpdate(SaveItemCreatedByQuery, values:nil)
            
        } catch {
            
            let ErrorMessage = "ItemViewController:\(#function) - Failed to Save \"\(itemCreatedByTextField.stringValue)\" to the Database\n\(db.lastErrorMessage())\n\(SaveItemCreatedByQuery))"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "ItemViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to Save \"\(itemCreatedByTextField.stringValue)\" to the Database\n\n\(SaveItemCreatedByQuery)", DatabaseError: "\(db.lastErrorMessage())")
            
            return
            
        }
        
        print("itemCreatedByAction Action Triggered & SAVED")
        
    }// end of itemCreatedByAction
    
    @IBAction func itemUsedForAction(_ sender: Any) {
        
        if (itemUsedForTextField.stringValue.count < 1) {

            return
            
        }
        
        
        
        let SaveItemUsedForQuery = "UPDATE itemtable SET usedfor = \"\(itemUsedForTextField.stringValue)\" WHERE ikey = \"\(itemIkeyToLoad)\""
        
        do {
            
            try db.executeUpdate(SaveItemUsedForQuery, values:nil)
            
        } catch {
            
            let ErrorMessage = "ItemViewController:\(#function) - Failed to Save \"\(itemUsedForTextField.stringValue)\" to the Database\n\(db.lastErrorMessage())\n\(SaveItemUsedForQuery))"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "ItemViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to Save \"\(itemUsedForTextField.stringValue)\" to the Database\n\n\(SaveItemUsedForQuery)", DatabaseError: "\(db.lastErrorMessage())")
            
            return
            
        }
        
        print("itemUsedForAction Action Triggered")
        
    }// end of itemUsedForAction
    
    @IBAction func itemPictureButtonAction(_ sender: Any) {

        print("itemPictureButtonAction Action Triggered")
        
        if let url = NSOpenPanel().selectUrl {
            self.itemPictureButton.image = NSImage(contentsOf: url)
            print("file selected:", url.path)
            
            let ImagePath = url.path
            
            let ImageName = "\((ImagePath as NSString).lastPathComponent)"
            let FullPathImageCheck = "\(ImagesLocation)/\(ImageName)"
            
            ////////////////////////////////////////
            
            let ItemImageIkeyQuery = "SELECT ikey FROM imagetable WHERE itemreference = \"\(itemIkeyToLoad)\" "
            
            print("ItemImageIkeyQuery: \"\(ItemImageIkeyQuery)\"")
            
            var ItemImageIkeyDB: String?
            
            do {
                
                let ItemImageIkeyQueryRun = try db.executeQuery(ItemImageIkeyQuery, values:nil)
                
                
                while ItemImageIkeyQueryRun.next() {
                    ItemImageIkeyDB = ItemImageIkeyQueryRun.string(forColumn: "ikey")
                    
                    if (ItemImageIkeyDB == nil || ItemImageIkeyDB == "") {
                        ItemImageIkeyDB = "0"
                        
                    }
                    
                }
                
                
                
            } catch {
                
                let ErrorMessage = "ItemViewController:\(#function) - Failed to get Item Image Reference\n\n\(db.lastErrorMessage())\n\(ItemImageIkeyQuery))"
                
                print(ErrorMessage)
                
                loggly(LogType.Error, text: ErrorMessage)
                
                DoAlert.DisplayAlert(Class: "ItemViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to get Item Image Reference\n\n\(ItemImageIkeyQuery)", DatabaseError: "\(db.lastErrorMessage())")
                
            }
        
            
            
            
            ////////////////////////////////////////
            
            
             var ItemImageReference = String()
            
            if (ItemImageIkeyDB == nil || ItemImageIkeyDB == "0") {
                ItemImageReference = "INSERT INTO imagetable(ikey,userikeyreference ,itemreference, pathtoimage) VALUES((SELECT MAX(ikey) + 1 from imagetable),\(CurrentUserIkey!),\"\(itemIkeyToLoad)\",\"\(FullPathImageCheck)\")"
            } else {
                ItemImageReference = "UPDATE imagetable SET itemreference = \"\(FullPathImageCheck)\" WHERE ikey = \"\(itemIkeyToLoad)\" "
            }
            
            print("ItemImageReference: \"\(ItemImageReference)\" ")
           
            
            ConnectionTest.OpenDB()
            
            dbQueue.inTransaction { _, rollback in
                
                let Success = db.executeStatements(ItemImageReference)
                
                if (!Success) {
                    
                    rollback.pointee = true
                    
                    let ErrorMessage = "Failed - Item Image reference for Item: \"\(itemNameTextfield.stringValue)\" \n\n\(ItemImageReference)\n\n\(db.lastErrorMessage())"
                    
                    loggly(LogType.Error, text: ErrorMessage)
                    
                    DoAlert.DisplayAlert(Class: "LocationsInfoViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed - Item Image reference for Item: \"\(itemNameTextfield.stringValue)\" \n\n\(ItemImageReference)", DatabaseError: "\(db.lastErrorMessage())")
                    return
                }
                
                
            }
        
        ///////////////////
        // Copy image to Images folder
        

        
        if FM.fileExists(atPath: FullPathImageCheck) {
            print("File exists")
        } else {
            print("File not found")
            do {
                
                try FM.copyItem(atPath: ImagePath, toPath: FullPathImageCheck)
                
                
                
            }
            catch let error as NSError {
                
                let ErrorMessage = "Failed to copy image \((ImagePath as NSString).lastPathComponent) to Images folder\n\n\(error)"
                
                loggly(LogType.Error, text: ErrorMessage)
                
                DoAlert.DisplayAlert(Class: "LocationsInfoViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to copy image \((ImagePath as NSString).lastPathComponent) to Images folder \(FullPathImageCheck)", DatabaseError: "\(error)")
            }
        }
        
        
        
        
        //////////// end of if
        
    } else {
            print("file selection was canceled")
    }
        
        
        

        
    }// end of itemPictureButtonAction
    
    
    
    
    func SaveItemComposistion() {
        
        
        if (itemComposistionTextView.string.count < 1) {
            
            return
            
        }
        
        
        
        let SaveItemCompositionQuery = "UPDATE itemtable SET composition = \"\(itemComposistionTextView.string)\" WHERE ikey = \"\(itemIkeyToLoad)\""
        
        do {
            
            try db.executeUpdate(SaveItemCompositionQuery, values:nil)
            
        } catch {
            
            let ErrorMessage = "ItemViewController:\(#function) - Failed to Save Item Composition to the Database\n\(db.lastErrorMessage())\n\(SaveItemCompositionQuery))"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "ItemViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to Save Item Composition to the Database\n\n\(SaveItemCompositionQuery)", DatabaseError: "\(db.lastErrorMessage())")
            
            return
            
        }
        
        
    }//end of SaveItemComposistion()
    
    func SaveItemHistory() {
        
        
        if (itemHistoryTextView.string.count < 1) {
            
            return
            
        }
        
        let SaveItemHistoryQuery = "UPDATE itemtable SET history = \"\(itemHistoryTextView.string)\" WHERE ikey = \"\(itemIkeyToLoad)\""
        print("SaveItemHistoryQuery: \"\(SaveItemHistoryQuery)\" ")
        
        do {
            
            try db.executeUpdate(SaveItemHistoryQuery, values:nil)
            
        } catch {
            
            let ErrorMessage = "ItemViewController:\(#function) - Failed to Save Item History to the Database\n\(db.lastErrorMessage())\n\(SaveItemHistoryQuery))"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "ItemViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to Save Item History to the Database\n\n\(SaveItemHistoryQuery)", DatabaseError: "\(db.lastErrorMessage())")
            
            return
            
        }
        
        
    }//end of SaveItemHistory()
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    func CleanUp() {
        
        itemNameTextfield.stringValue = ""
        itemCategoryPopupButton.selectItem(withTitle: "Select a Cateogry")
        itemSubcategoryPopupButton.selectItem(withTitle: "Select a Subcateogry")
        
        let NowDate = Date()
        itemCreateDateDatePicker.dateValue = NowDate
        
        itemAgeTextField.stringValue = "0"
        itemCreatedByTextField.stringValue = ""
        itemUsedForTextField.stringValue = ""
        itemComposistionTextView.string = ""
        itemHistoryTextView.string = ""
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    @IBAction func itemInfoWindowCloseButtonAction(_ sender: Any) {

        self.view.window?.close()
        
        CleanUp()
        
        itemIkeyToLoad = 0
        itemBool = false
        
        //ItemListVC.LoadListTableData()
        ItemsWindow.showWindow(self)
    }
    
    
    
    
    
    
    
    
    
}// end of class ItemViewController: NSViewController


