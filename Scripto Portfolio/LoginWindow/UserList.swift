//
//  UserList.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 11/3/17.
//  Copyright © 2017 DTGM. All rights reserved.
//

import Foundation

class UserList: NSObject {
    let firstName: String
    
    init(firstName: String) {
        self.firstName = firstName
    }
    
}
