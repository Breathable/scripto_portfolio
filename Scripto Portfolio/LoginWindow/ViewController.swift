//
//  ViewController.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 1/19/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Cocoa
import FMDB
import AppKit
import Foundation
import SwiftyUserDefaults
import SwiftLoggly

import AppCenter
import AppCenterAnalytics
import AppCenterCrashes


// ViewController

class LoginViewController: NSViewController, NSTableViewDelegate, NSTableViewDataSource, MSCrashesDelegate {
    
    
    
    
    
    @IBOutlet weak var userTextField: NSTextField!
    @IBOutlet weak var passwordSecureTextField: NSSecureTextField!
    @IBOutlet weak var userTableView: NSTableView!
    
    
    @IBOutlet weak var loginButton: NSButton!
    @IBOutlet weak var newUserButton: NSButton!
    @IBOutlet weak var forgotPasswordButton: NSButton!
    
    @IBOutlet weak var LoginWindowVersionWindowLabel: NSTextField!
    
    
    var CurrentAppearance = String()
    
    var Users: [UserList] = []
    
    
    
    
    
    
    
    var MSErrorReportSTRING = String()
    var SendCrashes = false
    
    
    
    
    
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()        // Do any additional setup after loading the view.

        
        
        
        
        
        
        ///////////////////////////////////////////
        WriteToLog.CreateLoggingLogFile()
       
        DBCheck()
        
        
        
        let os = ProcessInfo().operatingSystemVersion
        
        print("OS VERSION: \(os.majorVersion).\(os.minorVersion).\(os.patchVersion)")
        
        loggly(LogType.Info, text: "Scripto Porfolio Started")
        loggly(LogType.Info, text: "Computer OS: \(os.majorVersion).\(os.minorVersion).\(os.patchVersion)")
        
        OSSYSTEMVERSION = Double("\(os.majorVersion).\(os.minorVersion)")!
        
        
        
        
        
        

        //        NSEvent.addLocalMonitorForEvents(matching: .flagsChanged) {
        //            self.flagsChanged(with: $0)
        //            return $0
        //        }
        NSEvent.addLocalMonitorForEvents(matching: .keyDown) {
            self.keyDown(with: $0)
            return $0
        }
        

        
        
            
            
            
            

            



            
            
            ////////////////////////
            

            //      https://github.com/radex/SwiftyUserDefaults

            
            ////////////////////////
            
            self.DirectoryLocationsCheck()
            

            self.userTableView.delegate = self
            self.userTableView.dataSource = self
            
            ///////////////////////////////
            
    
        
            self.LoginWindowVersionWindowLabel.stringValue = "\(AppVersion)"
            
            print("ViewController - viewDidLoad finished")
            
        
        
        
        
        if #available(OSX 10.15, *) {
            
            // if 10.15 do something else due to teh
            
        } else {
         
            
        
        }
        
        
        
        
        
        
       
       
        
    }// end of view did load


    
    
    /////////////////////
    
    // https://stackoverflow.com/questions/32446978/swift-capture-keydown-from-nsviewcontroller
    override func keyDown(with event: NSEvent) {
        

        switch event.modifierFlags.intersection(.deviceIndependentFlagsMask) {
        case [.command] where event.keyCode == 12:
            print("Command key & Q was pressed")
            loggly(LogType.Info, text: "Quit Called")
        default:
            print("")
        }
     
        
        
    }

    
    /////////////////////

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }
    
    override func viewWillAppear() {
        
        fontCheck()
        UserTableData()
        
        // Dark Mode Check
        //appearanceCheck()
        
        DBChangesTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(isDoneUpdatingCheck), userInfo: nil, repeats: true)
        

        repeat {

            //DoneUpdating = true
            if (DoneUpdating == true) {
                
                break
            }
        } while (DoneUpdating == false)
        
        
        
        if (DoneUpdating == true) {
            DBChangesTimer.invalidate()
        }
        
        
        
        
        
            
            if (AlwaysSendLogsAppCenter) {
                
                MSAppCenter.start("13704ccd-621a-4174-8dee-ebe2238b1372", withServices:[
                    MSAnalytics.self,
                    MSCrashes.self
                    ])
                
                // Added to the info.plist directly
                // AppKit catches exceptions thrown on the main thread, preventing the application from crashing on macOS, so the SDK cannot catch these crashes. To mimic iOS behavior, set NSApplicationCrashOnExceptions flag before SDK initialization, this will allow the application to crash on uncaught exceptions and the SDK can report them.
                //UserDefaults.standard.register(defaults: ["NSApplicationCrashOnExceptions": true])
                
                MSCrashes.setDelegate(self)
                
                MSAppCenter.setLogLevel(.verbose)
            }
            
            
            ///////////////////////////////////////////
        
        
        
       
        print("DoneUpdating: \"\(DoneUpdating)\" ")
        
        
        
        //delayWithSeconds(5.0) {
        
            if (!AppCenterSendLogsPrompt) {
                
                loggly(LogType.Info, text: "AppCenterSendLogsPrompt Prmpt Shown")
                
                let alert: NSAlert = NSAlert()
                alert.messageText = "Scripto Portfolion now has a Crash Reporting feature!"
                alert.informativeText = "In the event of an app crash by default Scripto Portfolio will collect and send ANONYMOUS information about the crash to the developer.\n\nThis new feature can be turned OFF in the app Preferences after you log into Scripto Portfolio."
                
                alert.addButton(withTitle: "OK")
                
                //alert.runModal()
                
                if alert.runModal() == .alertFirstButtonReturn {

                    print("Ok Pressed")
                    
                    loggly(LogType.Info, text: "AppCenterSendLogsPrompt Prompt: OK Presed")

                    dbQueue.inTransaction { _, rollback in

                        let UpdateDBStatement = String(format:
                            "UPDATE dbasetable SET appcentersendlogsprompt = 1 WHERE ikey = 1"
                        )

                        let Success = db.executeStatements(UpdateDBStatement)

                        if (!Success) {

                            rollback.pointee = true

                            loggly(LogType.Warnings, text: "ViewController-\(#function): \nAppCenterSendLogsPrompt Prompt\nUnable to create to update dbasetable\n\(UpdateDBStatement)\n\(db.lastErrorMessage())")

                            DoAlert.DisplayAlert(Class: "ViewController", Level: 1, Function: "\(#function)", MessageText: "An Error has occurred", InformativeText: "Database changes were rolled back from updating.\nPlease Quit and try again.\nIf this continues please restart your computer.", DatabaseError: "\nSQL Query Failed:\n\n\(UpdateDBStatement)\n\nLastErrorMessage: \"\(db.lastErrorMessage())\"")

                            return

                        }// end of !Success

                        loggly(LogType.Info, text: "appcentersendlogsprompt Prmpt Seen and Closed")

                    }// end of dbQueue.inTransaction

                }// end of if alert.runModal() == .alertFirstButtonReturn
                
                
            }
        
        //} // end of delay
        
        
        
        
        

        print("ViewController - viewWillAppear finished")
        
    }// end of viewWillAppear()
    
    
    override func viewDidAppear() {
        
        //OpenWindows.CheckForOpenWindows()
        
        ////////////////////////////////////
        
        view.wantsLayer = true

        //UserTableData()
        
//        DatabaseFunction.appVersionCheck()
        
        LoginWindowVersionWindowLabel.stringValue = "\(AppVersion)"
        
        print("ViewController - viewDidAppear finished")
        
        
        // Testing the Dark mode with the Alert class
        
        //DoAlert.DisplayAlert(Class: "ViewController", Level: 3, Function: "\(#function)", MessageText: "Warning", InformativeText: "This is a test of the Alert Class\nPlease dont be alarmed.", DatabaseError: "\(db.lastErrorMessage())")
        
       appearanceCheck()
        
    }
    
    func delayWithSeconds(_ seconds: Double, completion: @escaping () -> ()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            completion()
        }
    }
    
    //////////////////////////
    
    

    override func viewDidLayout() {
        
//        appearanceCheck()
        
        print("ViewController - viewDidLayout finished")

    }
    
    func appearanceCheck() {
        
        //        func isDarkMode(view: NSView?) -> Bool {
        //            if #available(OSX 10.14, *) {
        //                let appearance = view?.effectiveAppearance ?? NSAppearance.current!
        //                return (appearance.name == .darkAqua)
        //            }
        //            return false
        //        }
        
        
        
        enum InterfaceStyle : String {
            case Dark, Light
            
            init() {
                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
                self = InterfaceStyle(rawValue: type)!
            }
        }
        
        
        let currentStyle = InterfaceStyle()
        
        if (currentStyle.rawValue == "Dark") {
            print("Dark Style Detected - ViewController")
        
            userTableView.backgroundColor = TableBackgroundColorDarkMode

            view.layer?.backgroundColor = WindowBackgroundColorDarkMode
            
            userTextField.wantsLayer = true
            userTextField.layer?.backgroundColor = NSColor.gray.cgColor
            userTextField.layer?.cornerRadius = 4
            
            passwordSecureTextField.wantsLayer = true
            passwordSecureTextField.layer?.backgroundColor = NSColor.gray.cgColor
            passwordSecureTextField.layer?.cornerRadius = 4
        
            LoginWindowVersionWindowLabel.font = NSFont(name:AppFontBold, size: 13.0)
            LoginWindowVersionWindowLabel.textColor = .lightGray
            
            loginButton.styleButtonText(button: loginButton, buttonName: "Login", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
            forgotPasswordButton.styleButtonText(button: forgotPasswordButton, buttonName: "Forgot Password", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
            newUserButton.styleButtonText(button: newUserButton, buttonName: "Add User", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
            
            loginButton.wantsLayer = true
            loginButton.layer?.backgroundColor = NSColor.lightGray.cgColor
            loginButton.layer?.cornerRadius = 7
            
            forgotPasswordButton.wantsLayer = true
            forgotPasswordButton.layer?.backgroundColor = NSColor.lightGray.cgColor
            forgotPasswordButton.layer?.cornerRadius = 7
            
            newUserButton.wantsLayer = true
            newUserButton.layer?.backgroundColor = NSColor.lightGray.cgColor
            newUserButton.layer?.cornerRadius = 7
            
        } else if (currentStyle.rawValue == "Light") {
            print("Light Style Detected - ViewController")
            
            let loginWindowColor = NSColor(calibratedRed: 0.00, green: 0.46, blue: 0.80, alpha: 1.00)
            
            if view.layer?.backgroundColor != loginWindowColor.cgColor {
                //print("Background Color was not set,, Reset Background color")
                //print("LoginBackgroundColorBlue: \(LoginBackgroundColorBlue)")
                
                view.layer?.backgroundColor = loginWindowColor.cgColor
            } else {
                //print("Background Color was set")
            }
            
            
            loginButton.styleButtonText(button: loginButton, buttonName: "Login", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
            forgotPasswordButton.styleButtonText(button: forgotPasswordButton, buttonName: "Forgot Password", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
            newUserButton.styleButtonText(button: newUserButton, buttonName: "Add User", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
            
            loginButton.wantsLayer = true
            loginButton.layer?.backgroundColor = NSColor.gray.cgColor
            loginButton.layer?.cornerRadius = 7
            
            forgotPasswordButton.wantsLayer = true
            forgotPasswordButton.layer?.backgroundColor = NSColor.gray.cgColor
            forgotPasswordButton.layer?.cornerRadius = 7
            
            newUserButton.wantsLayer = true
            newUserButton.layer?.backgroundColor = NSColor.gray.cgColor
            newUserButton.layer?.cornerRadius = 7
            
            LoginWindowVersionWindowLabel.font = NSFont(name:AppFontBold, size: 13.0)
            LoginWindowVersionWindowLabel.textColor = NSColor.white

        }// end of else if (currentStyle.rawValue == "Light")
        
        CurrentAppearance = "\(currentStyle)"
        
        
        
        UserTableData()
        
    }// end of appearanceCheck()
    
    
    
    
    
    
    
    func crashes(_ crashes: MSCrashes!, shouldProcessErrorReport errorReport: MSErrorReport!) -> Bool {
        
        
        MSErrorReportSTRING = "\(MSErrorReport.self)"
        
        
        
        return SendCrashes
        
        // return true; // return true if the crash report should be processed, otherwise false.
    }
    
    
    // The following callback will be invoked before the SDK sends a crash log
    
    func crashes(_ crashes: MSCrashes!, willSend errorReport: MSErrorReport!) {
        // Your code, e.g. to present a custom UI.
        
        loggly(LogType.Info, text: "Scripto Portfolio Crashed\n\nERROR REPORT: \(MSErrorReport.self)\n\n")
        
        print("Scripto Portfolio Crashed\n\nERROR REPORT: \(MSErrorReport.self)\n\n")
        
        //let reportError = "MSErrorReport: \(MSErrorReport.self)"
        
        
    }
    
    
    
    // The following callback will be invoked after the SDK sent a crash log successfully
    
    func crashes(_ crashes: MSCrashes!, didSucceedSending errorReport: MSErrorReport!) {
        // Your code goes here.
        
        
        
        
        print("Scripto Portfolio Crash Log was Successfully Uploaded")
        
        loggly(LogType.Info, text: "Crash Report Uploaded Successfully")
        
        //DoAlert.DisplayAlert(Class: "Crash Report Upload", Level: 2, Function: "None", MessageText: "Warning", InformativeText: "Scripto Portfolio Crash Successfully Uploaded", DatabaseError: "No Action is Required\n\nERROR REPORT: \(MSErrorReport.self)")

        
        
        
    }
    
    // The following callback will be invoked if the SDK failed to send a crash log
    
    func crashes(_ crashes: MSCrashes!, didFailSending errorReport: MSErrorReport!, withError error: Error!) {
        // Your code goes here.
        
        print("Scripto Portfolio Crash Log FAILED TP UPLOAD")
        
        loggly(LogType.Error, text: "Scripto Portfolio Crash Log FAILED TO UPLOAD - No Action is Required\nERROR REPORT: \(MSErrorReport.self)\nERROR: \(Error.self)")
        
        //DoAlert.DisplayAlert(Class: "Crash Report Upload", Level: 2, Function: "None", MessageText: "Warning", InformativeText: "Scripto Portfolio Crash Log FAILED TP UPLOAD - Please send the email to the developer", DatabaseError: "No Action is Required\n\nERROR REPORT: \(MSErrorReport.self)\n\nERROR: \(Error.self)")
        
    }
    
    
    
    
    
    
    
    @objc func isDoneUpdatingCheck() {
        
        
        
        
        print("Checking... DoneUpdating = \"\(DoneUpdating)\" ")
        
        
        
    }
    
    
    
    
    
    
   
    
    

    
    func fontCheck() {
        

        //let FontArray: Array = NSFontManager.availableFontNames(self)
        
        let FontList: Array = NSFontManager.shared.availableFontFamilies
        
        //print("FontList: \(FontList)")
        
       
        if FontList.contains("Chalkboard") {
            print("NSFont Chalkboard Set")
            
            AppFont = "Chalkboard"
            AppFontBold = "Chalkboard-Bold"
            
            loggly(LogType.Info, text: "NSFont Chalkboard set as Scripto Portfolio Font")
            
        } else if FontList.contains("Cambria") {
            print("NSFont Cambria Set")
            
            AppFont = "Cambria"
            AppFontBold = "Cambria-Bold"
            
            loggly(LogType.Info, text: "NSFont Cambria set as Scripto Portfolio Font")
            
        } else {
            
            AppFont = "Avenir"
            AppFontBold = "Avenir-Heavy"
            
            loggly(LogType.Info, text: "NSFont Avenir set as Scripto Portfolio Font")
            
        }
        
        
    }
    
    
    
    
    func DBCheck() {

        //print("DatabaseLocation: \(DatabaseLocation)")

        loggly(LogType.Info, text: "DatabaseLocation: \(DatabaseLocation)")

        // Check if DB exists

        if FM.fileExists(atPath: DatabaseLocation) {
            loggly(LogType.Info, text: "Database Found at Database Location")

            db = FMDatabase(path: DatabaseLocation)

            //delayWithSeconds(0.50) {

                //DatabaseFunction.databaseVersionCheck()
                //DatabaseFunction.appVersionCheck()
                DatabaseFunction.VersionChecks()
            //}

        } else {


            loggly(LogType.Info, text: "Database NOT Found at Database Location")
            db = FMDatabase(path: DatabaseLocation)
            loggly(LogType.Info, text: "Database Created at Database Location")

            //delayWithSeconds(0.10) {
                UpdateDatabase.CreateDatabase()
                //DatabaseFunction.VersionChecks()
            //}

        }



        // Open Database

        //db.open()

        // Check to see we can connect to it

        //ConnectionCheck()



    }
    
    
    func DirectoryLocationsCheck() {
        
        // StoriesLocation
        
        if !FM.fileExists(atPath: StoriesLocation) {
            do {
                try FM.createDirectory(atPath: StoriesLocation, withIntermediateDirectories: true, attributes: nil)
            } catch {
                print(error.localizedDescription)
                
                loggly(LogType.Warnings, text: "ViewConroller-\(#function): Unable to Create StoriesLocation directory: \(StoriesLocation)\n\(error.localizedDescription)")
                
                DoAlert.DisplayAlert(Class: "ViewController", Level: 2, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to create StoriesLocation directory at\n\(StoriesLocation)\nPlease quit and try agian.", DatabaseError: "\(error.localizedDescription)\n\n\(Thread.callStackSymbols.forEach{print($0)})")
                
            }
        }
        
        // ImagesLocation
        
        if !FM.fileExists(atPath: ImagesLocation) {
            do {
                try FM.createDirectory(atPath: ImagesLocation, withIntermediateDirectories: true, attributes: nil)
            } catch {
                print(error.localizedDescription)
                
                loggly(LogType.Warnings, text: "ViewConroller-\(#function): Unable to Create ImagesLocation directory: \(ImagesLocation)\n\(error.localizedDescription)")
                
                DoAlert.DisplayAlert(Class: "ViewController", Level: 2, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to create ImagesLocation directory at\n\(StoriesLocation)\nPlease quit and try agian.", DatabaseError: "\(error.localizedDescription)")
                
            }
        }
        
        
        
        
        
        
        
    }// end of DirectoryCheck()
    
    
    
    
    
    
    func ConnectionCheck() {
        
        let ConnectionCheck = db.goodConnection
        
        print("ConnectionCheck: \(ConnectionCheck)")
        
        if ConnectionCheck {
            // Connected
            //print("Connection Check SUCCESS")
            loggly(LogType.Info, text: "Connection Check SUCCESS")
        } else {
            // Not Connected
            //let answer = dialogOKCancel(question: "Unable to Connect to the Database", text: "Please quit and try again.")
            
            loggly(LogType.Error, text: "Connection Check FAILED: Unable to Connect to the Database")
            DoAlert.DisplayAlert(Class: "ViewController", Level: 1, Function: "\(#function)", MessageText: "Connection Check FAILED: Unable to Connect to the Database", InformativeText: "Its HIGHLY recommended to quit and try again", DatabaseError: "\(db.lastErrorMessage())")

            
        }
        
    }
    
    //////////////////////////
    
    // Login Table info
    
    func UserTableData() {
        
        db.close()
        
        delayWithSeconds(0.5) {
            
            db.open()
            //ConnectionCheck()
            
            self.Users.removeAll()
            
            var DBUser: String!
            
            let DBUserQuery = "SELECT user FROM users"
            
            do {
                let UserQuery = try db.executeQuery(DBUserQuery, values:nil)
                
                while UserQuery.next() {
                    DBUser = UserQuery.string(forColumn: "user")
                    //print("DBUser: \"\(DBUser)\"")
                    
                    if (DBUser == "AppSDemo") {
                        
                    } else {
                        let userToAdd = UserList.init(firstName:DBUser)
                        print("userToAdd: \"\(userToAdd)\"")
                        self.Users.append(userToAdd)
                    }
                    
                }
                
            } catch {
                print("ERROR: \(db.lastErrorMessage())")
                loggly(LogType.Error, text: "ViewController-\(#function) Query Failed: \"\(DBUserQuery)\"")
                DoAlert.DisplayAlert(Class: "ViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"\(DBUserQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
            }
            
            self.userTableView.reloadData()
            
        }
        
        
        
        
    }// end of UserTableData()
    
    
    
    func numberOfRows(in tableVieww: NSTableView) -> Int {
        
        return Users.count
        
    }
    
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        
        guard let cell = tableView.makeView(withIdentifier: tableColumn!.identifier, owner: self) as? NSTableCellView else
        {
            return nil
        }
        
        //cell.textField?.font = NSFont(name: AppFontBold, size: 13)
        
        if (tableColumn?.identifier)!.rawValue == "username" {
            
            cell.textField?.stringValue = Users[row].firstName
            
        }

        return cell
        
    }
    
    
    
    func tableViewSelectionDidChange(_ notification: Notification) {
        
        let SelectedRow = userTableView.selectedRow
        print("SelectedRow: \(SelectedRow)")
        print("Users: \(Users.count)")
        
        let isIndexValid = Users.indices.contains(SelectedRow)
        
        if isIndexValid {
            let UserString: String = self.Users[SelectedRow].firstName
            print("String: \(UserString)")
            userTextField.stringValue = UserString
            passwordSecureTextField.becomeFirstResponder()
        }
        
    }
    
    
    
    // Table Double Click Action
    
    @objc func TableDoubleClick() {
        
        print("DOUBLE CLICK")
        
        let SelectedRow = userTableView.selectedRow
        print("SelectedRow: \(SelectedRow)")
        print("Users: \(Users.count)")
        
        let isIndexValid = Users.indices.contains(SelectedRow)
        
        if isIndexValid {
            let UserString: String = self.Users[SelectedRow].firstName
            print("String: \(UserString)")
            userTextField.stringValue = UserString
            passwordSecureTextField.becomeFirstResponder()
        }
        
        
    }
    
    
    func RemoveWhiteSpace(aString: String) -> String {
        let replaced = aString.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        return replaced
    }
    
    
    
    
    
    //////////////////////////
    //////////////////////////
    //////////////////////////
    //////////////////////////
    
    

@IBAction func loginButton(_ sender: Any) {
    
    
//    DoAlert.DisplayAlert(Class: "ViewController", Level: 2, Function: "\(#function)", MessageText: "An Error has occurred", InformativeText: "My Informative Text which is my Error in detail", DatabaseError: "\(db.lastExtendedErrorCode())")

    
    
        let userName = self.userTextField.stringValue
        let password = self.passwordSecureTextField.stringValue
    
        let userNameBlankCheck = userName.trimmingCharacters(in: .whitespacesAndNewlines)
        let passwordBlankCheck = password.trimmingCharacters(in: .whitespacesAndNewlines)
    
        print("userNameBlankCheck: \"\(userNameBlankCheck)\" ")
    
        let UserNCount = userNameBlankCheck.count
        let PassNCount = passwordBlankCheck.count
    
    print("UserNCount: \"\(UserNCount)\"  PassNCount: \"\(PassNCount)\"  ")
    
    
        if ((userNameBlankCheck.count == 0) || (passwordBlankCheck.count == 0)) {
        
            
            
            loggly(LogType.Info, text: "ViewController-\(#function): Username or Password is blank")
            //DoAlert.DisplayAlert(Class: "ViewController", Level: 3, Function: "\(#function)", MessageText: "Username or Password is blank", InformativeText: "Please try again", DatabaseError: "None") // \(db.lastExtendedErrorCode())
            
            let alert = NSAlert()
            
            alert.messageText = "Username or Password is blank"
            alert.informativeText = "Please try again"
            alert.alertStyle = .warning
            alert.addButton(withTitle: "OK")
            
            if (CurrentAppearance == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.window.titlebarAppearsTransparent = true
            
            if alert.runModal() == .alertFirstButtonReturn {
                print("Ok Pressed")
                //userTextField.stringValue = ""
                //LockPasswordTextField.stringValue = ""
            }
            
        return
        }
    
    
        let EncodedPW = password.base64Encoded()
    
    

    
    
    
    
        ////////////////
    
        var userCount: Int32!
    
        do {
            let UserCountQuery = try db.executeQuery("SELECT count(*) as cnt FROM users WHERE user = ? and password = ? ", values: ["\(userName)","\(EncodedPW!)"])
        
            while UserCountQuery.next() {
                userCount = UserCountQuery.int(forColumn: "cnt")
            }
        
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "ViewController-\(#function): Query: SELECT count(*) as cnt FROM users WHERE user = ? and password = ? ")
            DoAlert.DisplayAlert(Class: "ViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed", InformativeText: "Query: SELECT count(*) as cnt FROM users WHERE user = ? and password = ? ", DatabaseError: "\(db.lastErrorMessage())")
            return
        }
    
        if (userCount == 0) {
            
            loggly(LogType.Info, text: "ViewController-\(#function): Username or Password is incorrect")
            
            //DoAlert.DisplayAlert(Class: "ViewController", Level: 3, Function: "\(#function)", MessageText: "Username or Password is incorrect", InformativeText: "Please try again", DatabaseError: "None") // \(db.lastExtendedErrorCode())
            
            let alert = NSAlert()
            
            alert.messageText = "Username or Password is Incorrect"
            alert.informativeText = "Please try again"
            alert.alertStyle = .warning
            alert.addButton(withTitle: "OK")
            alert.window.titlebarAppearsTransparent = true
            
            if (CurrentAppearance == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            if alert.runModal() == .alertFirstButtonReturn {
                print("Ok Pressed")
                //userTextField.stringValue = ""
                //LockPasswordTextField.stringValue = ""
                
        
                
                
            }
            return
        }
    
    
        /////////////////
    
    
        var DBUserName: String!
        var DBPassword: String!
        var DBIkey: Int32!
    
        do {
            let UserQuery = try db.executeQuery("SELECT * FROM users WHERE user = ? and password = ? ", values: ["\(userName)","\(EncodedPW!)"])
            
            while UserQuery.next() {
                DBUserName = UserQuery.string(forColumn: "user")
                DBPassword = UserQuery.string(forColumn: "password")
                DBIkey = UserQuery.int(forColumn: "ikey")
            }
            
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "ViewController-\(#function): Query Failed: SELECT * FROM users WHERE user = ? and password = ?")
            DoAlert.DisplayAlert(Class: "ViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed", InformativeText: "Query Failed: SELECT * FROM users WHERE user = ? and password = ?", DatabaseError: "\(db.lastErrorMessage())")
        }
    
        //        print("DBUserName: \(DBUserName)")
        //        print("DBPassword: \(DBPassword)")
        //        print("DBIkey: \(DBIkey)")
    
    
        let DecodedPassword = DBPassword?.base64Decoded()
        print("DBPassword: \"\(DBPassword!)\" ")
        print("DecodedPassword: \"\(DecodedPassword!)\" ")

    
    
    
    
        if userName == DBUserName {
            if password == DecodedPassword {
                print("Success logging In")
                
                userTextField.stringValue = ""
                passwordSecureTextField.stringValue = ""
                
                ////////////
                CurrentUserIkey = DBIkey
                CurrentUserName = DBUserName
                
                UP.UserPrefCheck()
                

                print("Success in logging into Script Porfolio")
                
                
                ////////////////////////
                
                
                AddMenuItems()
                

                 ////////////////////////
                
                db.close()
                
                self.view.window?.close()
                
                //  https://stackoverflow.com/questions/45119813/how-do-i-open-another-window-in-macos-in-swift-with-cocoa
                StoryListWindow.showWindow(self)
                
                loggly(LogType.Info, text: "User: \"\(CurrentUserName!)\" Logged in")

                
               // MSCrashes.generateTestCrash()
                
                ///////////////////////////////
                
//                MSCrashes.setUserConfirmationHandler({ (errorReports: [MSErrorReport]) in
//
//                    // Your code to present your UI to the user, e.g. an NSAlert.
//                    let alert: NSAlert = NSAlert()
//                    alert.messageText = "Sorry about that!"
//                    alert.informativeText = "Do you want to send an anonymous crash report so we can fix the issue?"
//                    alert.addButton(withTitle: "Always send")
//                    alert.addButton(withTitle: "Send")
//                    alert.addButton(withTitle: "Don't send")
//                    alert.alertStyle = .warning
//
//                    switch (alert.runModal()) {
//                    case .alertFirstButtonReturn:
//                        //MSCrashes.notify(with: .always)
//                        print("MSCrashes.notify(with: .always)")
//                        break;
//                    case .alertSecondButtonReturn:
//                        //MSCrashes.notify(with: .send)
//                        print("MSCrashes.notify(with: .send)")
//                        break;
//                    case .alertThirdButtonReturn:
//                        //MSCrashes.notify(with: .dontSend)
//                        print("MSCrashes.notify(with: .dontSend)")
//                        break;
//                    default:
//                        break;
//                    }
//
//                    return true // Return true if the SDK should await user confirmation, otherwise return false.
//                })
                

                ///////////////////////////////
                
               
                
                
            } else {
              
            loggly(LogType.Info, text: "ViewController-\(#function): Username or Password is incorrect")
                
                
                let alert = NSAlert()
                
                alert.messageText = "Username or Password is blank"
                alert.informativeText = "Please try again"
                alert.alertStyle = .warning
                alert.addButton(withTitle: "OK")
                alert.window.titlebarAppearsTransparent = true
                
                if (CurrentAppearance == "Dark") {
                    alert.window.backgroundColor = AlertBackgroundColorDarkMode
                } else {
                    alert.window.backgroundColor = WindowBackgroundColor
                }
                
                if alert.runModal() == .alertFirstButtonReturn {
                    print("Ok Pressed")
                    //userTextField.stringValue = ""
                    //LockPasswordTextField.stringValue = ""
                }

            }
        
        } else {
            
            loggly(LogType.Info, text: "ViewController-\(#function): Username or Password is incorrect")
            
            DoAlert.DisplayAlert(Class: "ViewController", Level: 3, Function: "\(#function)", MessageText: "Username or Password is incorrect", InformativeText: "Please try again", DatabaseError: "None") // \(db.lastExtendedErrorCode())
            

            return
        }
    
    
                //////////////////////////////////
    
    
    db.close()
    
    
    
    
    
    
    
    
    // testing the alert view and its informaiton and email and copy info buttons
    
//                let Query = String("SELECT user FROM users0")
//
//                do {
//                    let UserQuery = try db.executeQuery(Query, values:nil)
//
//                    while UserQuery.next() {
//                        let DBUser = UserQuery.string(forColumn: "user")
//
//                    }
//
//                } catch {
//
//
//                    var ErrorStack = String()
//                    Thread.callStackSymbols.forEach{
//                        print($0)
//                        ErrorStack = "\(ErrorStack)\n" + $0
//                    }
//
//                    print("ERROR: \(db.lastErrorMessage())")
//                    loggly(LogType.Error, text: "ViewController-\(#function) Query Failed: \"\(Query)\"\n\(ErrorStack)")
//                    DoAlert.DisplayAlert(Class: "ViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"\(Query)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())\n\nErrorStack written to the LOG")
//                }
//
    
    
    }// end of login button
    
    
    
    @IBAction func newUserButton(_ sender: Any) {
        
        self.view.window?.close()
        
        if (NewUserWindow.window?.isKeyWindow)! {
            NewUserWindow.window?.makeKeyAndOrderFront(self)
            //NewUserWindow.showWindow(self)
        } else {
            NewUserWindow.showWindow(self)
        }
        
        
        
        
    }
    
    
    @IBAction func forgotPasswordButton(_ sender: Any) {

        if (ForgotUserWindow.window?.isKeyWindow)! {
            ForgotUserWindow.window?.makeKeyAndOrderFront(self)
            //NewUserWindow.showWindow(self)
        } else {
            ForgotUserWindow.showWindow(self)
        }
        
    }
    
    
    
    @objc func MenuLock() {
        print("Lock menu was clicked")
        OpenWindows.CheckForOpenWindows()
        OpenWindows.CloseAllWindows()
        LockWindow.showWindow(self)
    }
    
    @objc func CurrentUserPreferencesWindow() {
        
        print("User Preferences menu was clicked")
        UserPreferencesWindow.showWindow(self)
    }
    
    @objc func SharingWindowOpen() {
        print("Sharing menu was clicked")
        SharingWindow.showWindow(self)
    }
    
    @objc func SharedWindowOpen() {
        print("Shared menu was clicked")
        SharedWindow.showWindow(self)
        
    }
    
    @objc func MobileDataOpen() {
        print("Mobile menu was clicked")
        MobileWindow.showWindow(self)
        
    }

    
    @objc func WantedFeatureShowWindow() {
        
        //print("TESTING 1 2 3 !!")
        WantedFeatureWindow.showWindow(self)
  
    }
    
    @objc func AppPreferences() {
        print("AppPreferences RAN!!")
        
        AppPreferencesWindow.showWindow(self)
    }
    
    @objc func AppThemes() {
        print("AppThemes RAN!!")
        
        AppThemesWindow.showWindow(self)
    }
    
    @objc func StoryGoalOpen() {
        
        
//        let currentHost = Host.current().localizedName
//
//        if (currentHost == "Dorian’s MacBook Pro") {
//            StoryGoalsWindow.showWindow(self)
//            return
//        }
        
        
//        if (AppVersion < "4.0.0") {
//            let alert: NSAlert = NSAlert()
//            alert.messageText = "Story Goals"
//            alert.informativeText = "The Story Goals area is still under construction.\nSorry for taking so long."
//            alert.addButton(withTitle: "OK")
//            alert.runModal()
//        }
        
        
        StoryGoalsWindow.showWindow(self)
        
    }// end of @objc func StoryGoalOpen()
    

    
    func AddMenuItems() {
        
        // About Scripto Portfolio
        //                NSApp.mainMenu?.item(at: 0)?.submenu?.insertItem(withTitle: "About Scripto Portfolio", action: #selector(WantedFeatureShowWindow), keyEquivalent: "", at: 0)
        //                NSApp.mainMenu?.item(at: 0)?.submenu?.item(at: 0)?.target = self
        //                NSApp.mainMenu?.item(at: 0)?.submenu?.item(at: 0)?.isEnabled = true
        
        
//        let key = String(utf16CodeUnits: [unichar(NSBackspaceCharacter)], count: 1) as String
//        item.keyEquivalentModifierMask = Int(NSEventModifierFlags.CommandKeyMask.rawValue | NSEventModifierFlags.ControlKeyMask.rawValue)
        
        // ⌘
        
//        let item = NewMenu.addItem(
//            withTitle: title,
//            action: #selector(AppPreferences),
//            keyEquivalent: ",")
//
//        item.keyEquivalentModifierMask = [.command]
        
      
        
        //App Preferences
        NSApp.mainMenu?.item(at: 0)?.submenu?.insertItem(withTitle: "Preferences", action: #selector(AppPreferences), keyEquivalent: "", at: 2)
        NSApp.mainMenu?.item(at: 0)?.submenu?.item(at: 2)?.target = self
        NSApp.mainMenu?.item(at: 0)?.submenu?.item(at: 2)?.isEnabled = true
        
        // Wanted Feature Menu Item
        NSApp.mainMenu?.item(at: 0)?.submenu?.insertItem(withTitle: "Wanted Features", action: #selector(WantedFeatureShowWindow), keyEquivalent: "", at: 3)
        NSApp.mainMenu?.item(at: 0)?.submenu?.item(at: 3)?.target = self
        NSApp.mainMenu?.item(at: 0)?.submenu?.item(at: 3)?.isEnabled = true
        
        // App Themes
        
        NSApp.mainMenu?.item(at: 0)?.submenu?.insertItem(withTitle: "App Themes", action: #selector(AppThemes), keyEquivalent: "", at: 4)
        NSApp.mainMenu?.item(at: 0)?.submenu?.item(at: 4)?.target = self
        NSApp.mainMenu?.item(at: 0)?.submenu?.item(at: 4)?.isEnabled = true
        
        
        
        
        
        
        
        /////
        
        // User Menu Item
        
        //let NewMenu : NSMenu = NSMenu(title: "\(CurrentUserName!.capitalized)")
        NewMenu = NSMenu(title: "\(CurrentUserName!.capitalized)")
        
        
        let newItem = NSMenuItem()
        newItem.target = self
        newItem.submenu = NewMenu
        
        let newItem0 = NSMenuItem(title:"Lock", action: #selector(MenuLock), keyEquivalent: "")
        newItem0.target = self
        
        let newItem1 = NSMenuItem(title:"Preferences", action: #selector(CurrentUserPreferencesWindow), keyEquivalent: "")
        newItem1.target = self
        
        let newItem2 = NSMenuItem(title:"Sharing", action: #selector(SharingWindowOpen), keyEquivalent: "")
        newItem2.target = self
        
        let newItem3 = NSMenuItem(title:"Shared", action: #selector(SharedWindowOpen), keyEquivalent: "")
        newItem3.target = self
        
        let newItem4 = NSMenuItem(title:"Mobile", action: #selector(MobileDataOpen), keyEquivalent: "")
        newItem4.target = self
        
        let newItem5 = NSMenuItem(title:"Story Goals", action: #selector(StoryGoalOpen), keyEquivalent: "")
        newItem5.target = self

        ///////////////////////////////////
        
        // Lock
        let letterL: Unicode.Scalar = "l"
        let CharacterL: Character = Character(letterL)
        newItem0.keyEquivalent = String(CharacterL)
        newItem0.keyEquivalentModifierMask = [NSEvent.ModifierFlags.function]
        
        // Preferences
        let letterP: Unicode.Scalar = "p"
        let CharacterP: Character = Character(letterP)
        newItem1.keyEquivalent = String(CharacterP)
        newItem1.keyEquivalentModifierMask = [NSEvent.ModifierFlags.function]
        
        // Sharing
        let letterS: Unicode.Scalar = "s"
        let CharacterS: Character = Character(letterS)
        newItem2.keyEquivalent = String(CharacterS)
        newItem2.keyEquivalentModifierMask = [NSEvent.ModifierFlags.function,NSEvent.ModifierFlags.shift]
        
        //Shared
        newItem3.keyEquivalent = String(CharacterS)
        newItem3.keyEquivalentModifierMask = [NSEvent.ModifierFlags.function]
        
        // Mobile
        let letterM: Unicode.Scalar = "m"
        let CharacterM: Character = Character(letterM)
        newItem4.keyEquivalent = String(CharacterM)
        newItem4.keyEquivalentModifierMask = [NSEvent.ModifierFlags.function]
        
        // Mobile
        let letterG: Unicode.Scalar = "g"
        let CharacterG: Character = Character(letterG)
        newItem5.keyEquivalent = String(CharacterG)
        newItem5.keyEquivalentModifierMask = [NSEvent.ModifierFlags.function]
        
        ///////////////////////////////////

        
        NewMenu.addItem(newItem0)
        NewMenu.addItem(newItem1)
        NewMenu.addItem(newItem2)
        NewMenu.addItem(newItem3)
        NewMenu.addItem(newItem4)
        NewMenu.addItem(newItem5)
        
        NSApp.mainMenu?.insertItem(newItem, at: 5)
    }
    
    

    
    
//    @IBAction func ContactSupportBurtton(_ sender: Any) {
//
//        NSWorkspace.shared.open(NSURL(string: "http://www.dangthatsgoodmedia.biz/support/")! as URL)
//
//    }
//
//
//    @IBAction func CloseHelpButton(_ sender: Any) {
//
//        self.view.window?.close()
//
//    }
    
    
    
    
    
    
    
    
    
    
    
    @IBAction func CrashButton(_ sender: Any) {
        
        
//        MSAppCenter.start("13704ccd-621a-4174-8dee-ebe2238b1372", withServices:[
//            //MSAnalytics.self,
//            MSCrashes.self
//            ])
//
//        // Added to the info.plist directly
//        // AppKit catches exceptions thrown on the main thread, preventing the application from crashing on macOS, so the SDK cannot catch these crashes. To mimic iOS behavior, set NSApplicationCrashOnExceptions flag before SDK initialization, this will allow the application to crash on uncaught exceptions and the SDK can report them.
//        //UserDefaults.standard.register(defaults: ["NSApplicationCrashOnExceptions": true])
//
//        MSCrashes.setDelegate(self)
//
//        MSAppCenter.setLogLevel(.verbose)
//
//        MSCrashes.generateTestCrash()
        
        
        fatalError()
        
        
        
        
    }
    
    
    
    
    
    
}













