//
//  HelpViewController.swift
//  Scripto Portfolio
//
//  Created by Dorian Glanville on 10/5/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Cocoa

class HelpViewController: NSViewController {
    
    
    @IBOutlet weak var helpCloseButton: NSButton!
    @IBOutlet weak var helpContactSupportButton: NSButton!
    @IBOutlet weak var helpEmailSupportButton: NSButton!
    @IBOutlet weak var helpLabel: NSTextField!
    
    
    var CurrentAppearanceHelpViewController = String()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.

        
    }
    
    override func viewDidLayout() {
        
        //        enum InterfaceStyle : String {
        //            case Dark, Light
        //
        //            init() {
        //                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
        //                self = InterfaceStyle(rawValue: type)!
        //            }
        //        }
        //
        //        let currentStyle = InterfaceStyle()
        //
        //        //print("currentStyle: \"\(currentStyle)\"  -  CurrentAppearance: \"\(CurrentAppearance)\" ")
        //
        //        if ("\(currentStyle)" != CurrentAppearanceNewUserViewController) {
        //            appearanceCheck()
        //        }
        
        appearanceCheck()
        
        super.viewDidLayout()
        
        print("NewUserViewController - viewDidLayout finished")
        
        
    }// end of viewDidLayout()
    
    
    func appearanceCheck() {
        
        enum InterfaceStyle : String {
            case Dark, Light
            
            init() {
                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
                self = InterfaceStyle(rawValue: type)!
            }
        }
        
        let currentStyle = InterfaceStyle()
        
        if ("\(currentStyle)" != CurrentAppearanceHelpViewController) {
            
            if (currentStyle.rawValue == "Dark") {
                print("Dark Style Detected - NewUserViewController")
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColorDarkMode
                
                
                
                helpCloseButton.styleButtonText(button: helpCloseButton, buttonName: "Close", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                helpCloseButton.wantsLayer = true
                helpCloseButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                helpCloseButton.layer?.cornerRadius = 7
                
                helpContactSupportButton.styleButtonText(button: helpContactSupportButton, buttonName: "Supports Web site", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                helpContactSupportButton.wantsLayer = true
                helpContactSupportButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                helpContactSupportButton.layer?.cornerRadius = 7
                
                helpEmailSupportButton.styleButtonText(button: helpEmailSupportButton, buttonName: "Email Support", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                helpEmailSupportButton.wantsLayer = true
                helpEmailSupportButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                helpEmailSupportButton.layer?.cornerRadius = 7
                
                
                helpLabel.font = NSFont(name:AppFontBold, size: 13.0)
                helpLabel.textColor = NSColor.white
                
                
                
            } else if (currentStyle.rawValue == "Light") {
                print("Light Style Detected - NewUserViewController")
                
                
                
                //            storyListTableView.backgroundColor = .white
                //
                //            storyListTableView.selectionHighlightStyle = .regular
                //            storyListTableView.usesAlternatingRowBackgroundColors = true
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColor.cgColor
                
                
                
                helpCloseButton.styleButtonText(button: helpCloseButton, buttonName: "Close", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                helpCloseButton.wantsLayer = true
                helpCloseButton.layer?.backgroundColor = NSColor.gray.cgColor
                helpCloseButton.layer?.cornerRadius = 7
                
                helpContactSupportButton.styleButtonText(button: helpContactSupportButton, buttonName: "Supports Web site", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                helpContactSupportButton.wantsLayer = true
                helpContactSupportButton.layer?.backgroundColor = NSColor.gray.cgColor
                helpContactSupportButton.layer?.cornerRadius = 7
                
                helpEmailSupportButton.styleButtonText(button: helpEmailSupportButton, buttonName: "Email Support", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                helpEmailSupportButton.wantsLayer = true
                helpEmailSupportButton.layer?.backgroundColor = NSColor.gray.cgColor
                helpEmailSupportButton.layer?.cornerRadius = 7
                
                
                helpLabel.font = NSFont(name:AppFontBold, size: 13.0)
                helpLabel.textColor = NSColor.white
                
            }
        }
        
        CurrentAppearanceHelpViewController = "\(currentStyle)"
        
        
    }//end of appearanceCheck()
    
    @IBAction func ContactSupportBurtton(_ sender: Any) {
        
        NSWorkspace.shared.open(NSURL(string: "http://www.dangthatsgoodmedia.biz/support/")! as URL)
        
    }
    
    
    @IBAction func emailSupporButton(_ sender: Any) {
        
        let service = NSSharingService(named: NSSharingService.Name.composeEmail)
        service?.recipients = ["Support@dangthatsgoodmedia.biz"]
        service?.subject = "Scripto Portfolio Question?"
        service?.perform(withItems: [""])
        
    }
    
    
    
    @IBAction func CloseHelpButton(_ sender: Any) {
        
        self.view.window?.close()
        
    }
    
    
    
}
