//
//  UserPreferences.swift
//  Scripto Portfolio
//
//  Created by Dorian Glanville on 9/28/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Foundation
import SwiftyUserDefaults
import SwiftLoggly


class UserPreferences: NSObject {
    
    
    
    var UPPlistArrayOfDict: [[String:String]] = []
    
    
    
    
    func UserPrefCheck() {
        
        PlistCheck()
        
        //UpdateUserBackgroundColor()
        
        
    } // end of UserPrefCheck()
    
    func PlistCheck() {
        
        
            let fileManager = FileManager.default
        
            //let LogsLocation = NSSearchPathForDirectoriesInDomains(.documentDirectory , .userDomainMask, true)[0].appending("/ScriptoPortfolioLogs")
//            let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
//            let path = documentDirectory.appending("/UserPreferences.plist")
        
            if (!fileManager.fileExists(atPath: UserPrefPlist)) {
                
                let ArrayOfDict: [[String:String]] = []
//
//                let dicContent1:[String: String] = ["Alabama": "Montgomery", "Alaska":"Juneau","Arizona":"Phoenix"]
//                let dicContent2:[String: String] = ["Alabama": "Montgomery", "Alaska":"Juneau","Arizona":"Phoenix"]
//
//                ArrayOfDict.append(dicContent1)
//                ArrayOfDict.append(dicContent2)
//
//                //let plistContent = NSDictionary(dictionary: dicContent)
                let plistContent = NSArray(array: ArrayOfDict)
                let success:Bool = plistContent.write(toFile: UserPrefPlist, atomically: true)
                
                
                if success {
                    print("file has been created!")
                }else{
                    print("unable to create the file")
                }
                

            } else {
                print("file already exist")
                
              
                
                let ArrayOfDict: [[String:String]] = NSArray(contentsOfFile: UserPrefPlist) as! [[String : String]]
                
                //print("ArrayOfDict: \(ArrayOfDict)")
               
                //UPPlistArrayOfDict =
                
                let count = ArrayOfDict.count
                
                if (count == 0) {
                    
                } else {
                    
                    if let value = ArrayOfDict[0]["\(CurrentUserIkey!)"] {
                        
                        print("value: \(value)")
                        
                        let color = NSColor().HexToColor(hexString: value, alpha: 1.0)
                        
                        WindowBackgroundColor = color
                        
                    } else {
                        WindowBackgroundColor = NSColor(calibratedRed: 0.00, green: 0.46, blue: 0.80, alpha: 1.00)
                    }
                    
                }

            }
 
    }
    
    

    func UpdateUserBackgroundColor(ColorString: String) {
        
        
       
        var ArrayOfDict: [[String:String]] = NSArray(contentsOfFile: UserPrefPlist) as! [[String : String]]
        
        
      
        let dicContent1:[String: String] = ["\(CurrentUserIkey!)":"\(ColorString)"]
        
        let count = ArrayOfDict.count
        
        if (count == 0) {
            ArrayOfDict.append(dicContent1)
        } else {
            
            var UserDict = ArrayOfDict[0]
            
            UserDict["\(CurrentUserIkey!)"] = "\(ColorString)"
            
            ArrayOfDict[0] = UserDict
        }
        
        let plistContent = NSArray(array: ArrayOfDict)
        let success:Bool = plistContent.write(toFile: UserPrefPlist, atomically: true)
        
        
        if success {
            print("file has been created!")
        }else{
            print("unable to create the file")
        }
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
            
            
//            var ColorDB: String!
//
//            let Query = "SELECT * FROM users WHERE user = \"\(CurrentUserName!)\" AND ikey = \(CurrentUserIkey!)"
//
//            do {
//                let UserQuery = try db.executeQuery(Query, values:nil)
//
//                while UserQuery.next() {
//                    ColorDB = UserQuery.string(forColumn: "usercolor")
//                }
//
//            } catch {
//
//                loggly(LogType.Error, text: "UserPreferences-\(#function) Query Failed: \"\(Query)\"  ")
//
//                DoAlert.DisplayAlert(Class: "UserPreferences", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"\(Query)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
//
//                return
//
//            }
//
//
//            //let CurrentUserDict: [Int: String] = [Int(CurrentUserIkey!):"\(ColorDB!)"]
//
//
//           userDefaults.set(Int(CurrentUserIkey!), forKey:"\(ColorDB!)")
            
        
        
        
        
        
    }// end of UserPreferenceDictionaryCheck()
    
    
    
    
    
    
    
    

    
    
    
    
    
    
}// end of UserPreferences


