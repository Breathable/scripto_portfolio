//
//  ForgotUserList.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 2/4/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Foundation

class ForgotUserList: NSObject {
    let firstName: String
    
    init(firstName: String) {
        self.firstName = firstName
    }
    
}
