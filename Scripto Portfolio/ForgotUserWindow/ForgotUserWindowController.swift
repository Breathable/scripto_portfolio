//
//  ForgotWindowController.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 2/4/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Cocoa

class ForgotUserWindowController: NSWindowController {

    override func windowDidLoad() {
        super.windowDidLoad()
    
        // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
        
        window?.titlebarAppearsTransparent = true
        window?.titleVisibility = .hidden
        
        
    }

}
