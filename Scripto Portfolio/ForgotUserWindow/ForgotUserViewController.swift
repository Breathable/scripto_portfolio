//
//  ForgotUserViewController.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 2/4/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Cocoa
import SwiftLoggly


class ForgotUserViewController: NSViewController, NSTableViewDelegate, NSTableViewDataSource {

    @IBOutlet weak var forgotUserTableView: NSTableView!
    
    @IBOutlet weak var usernameLabel: NSTextField!
    @IBOutlet weak var usernameTextField: NSTextField!
    
    @IBOutlet weak var newPasswordLabel: NSTextField!
    @IBOutlet weak var newPasswordSecureTextField: NSSecureTextField!
    

    @IBOutlet weak var birthdateLabel: NSTextField!
    @IBOutlet weak var birthdateDatePicker: NSDatePicker!

    @IBOutlet weak var confirmPasswordLabel: NSTextField!
    @IBOutlet weak var confirmPasswordSecureTextField: NSSecureTextField!
    
    
    
    @IBOutlet weak var changePasswordButton: NSButton!
    
    
    var CurrentAppearanceForgotUserViewController = String()
    
    
    var Users: [ForgotUserList] = []
    
    // ForgotUserList
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        
        
        
        
        
    }
    
    override func viewWillAppear() {
        
       
        
        forgotUserTableView.delegate = self
        forgotUserTableView.dataSource = self
        
        
        
        
        Users.removeAll()
        forgotUserTableView.reloadData()
        
        ForgotUserTableData()
        
    }
    
    
    override func viewDidLayout() {
        
        //        enum InterfaceStyle : String {
        //            case Dark, Light
        //
        //            init() {
        //                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
        //                self = InterfaceStyle(rawValue: type)!
        //            }
        //        }
        //
        //        let currentStyle = InterfaceStyle()
        //
        //        //print("currentStyle: \"\(currentStyle)\"  -  CurrentAppearance: \"\(CurrentAppearance)\" ")
        //
        //        if ("\(currentStyle)" != CurrentAppearanceNewUserViewController) {
        //            appearanceCheck()
        //        }
        
        appearanceCheck()
        
        super.viewDidLayout()
        
        print("NewUserViewController - viewDidLayout finished")
        
        
        let SelectedRow = forgotUserTableView.selectedRow
        if (SelectedRow == -1) {

        } else {
            forgotUserTableView.deselectRow(SelectedRow)
            usernameTextField.stringValue = ""
        }
        //forgotUserTableView.deselectAll(self)
        
    }// end of viewDidLayout()
    
    
    func appearanceCheck() {
        
        enum InterfaceStyle : String {
            case Dark, Light
            
            init() {
                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
                self = InterfaceStyle(rawValue: type)!
            }
        }
        
        let currentStyle = InterfaceStyle()
        
        if ("\(currentStyle)" != CurrentAppearanceForgotUserViewController) {
            
            if (currentStyle.rawValue == "Dark") {
                print("Dark Style Detected - NewUserViewController")
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColorDarkMode
                
                changePasswordButton.styleButtonText(button: changePasswordButton, buttonName: "Change Password", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                usernameTextField.wantsLayer = true
                usernameTextField.layer?.backgroundColor = NSColor.gray.cgColor
                usernameTextField.layer?.cornerRadius = 4
                
                newPasswordSecureTextField.wantsLayer = true
                newPasswordSecureTextField.layer?.backgroundColor = NSColor.gray.cgColor
                newPasswordSecureTextField.layer?.cornerRadius = 4
                
                confirmPasswordSecureTextField.wantsLayer = true
                confirmPasswordSecureTextField.layer?.backgroundColor = NSColor.gray.cgColor
                confirmPasswordSecureTextField.layer?.cornerRadius = 4
                
                changePasswordButton.wantsLayer = true
                changePasswordButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                changePasswordButton.layer?.cornerRadius = 7
                
                usernameLabel.font = NSFont(name:AppFontBold, size: 13.0)
                usernameLabel.textColor = NSColor.white
                
                newPasswordLabel.font = NSFont(name:AppFontBold, size: 13.0)
                newPasswordLabel.textColor = NSColor.white
                
                confirmPasswordLabel.font = NSFont(name:AppFontBold, size: 13.0)
                confirmPasswordLabel.textColor = NSColor.white
                
                birthdateLabel.font = NSFont(name:AppFontBold, size: 13.0)
                birthdateLabel.textColor = NSColor.white
                
                
                
            } else if (currentStyle.rawValue == "Light") {
                print("Light Style Detected - NewUserViewController")
                
                
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColor.cgColor
                
                changePasswordButton.styleButtonText(button: changePasswordButton, buttonName: "Change Password", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                changePasswordButton.wantsLayer = true
                changePasswordButton.layer?.backgroundColor = NSColor.gray.cgColor
                changePasswordButton.layer?.cornerRadius = 7
                
                usernameLabel.font = NSFont(name:AppFontBold, size: 13.0)
                usernameLabel.textColor = NSColor.white
                
                newPasswordLabel.font = NSFont(name:AppFontBold, size: 13.0)
                newPasswordLabel.textColor = NSColor.white
                
                confirmPasswordLabel.font = NSFont(name:AppFontBold, size: 13.0)
                confirmPasswordLabel.textColor = NSColor.white
                
                birthdateLabel.font = NSFont(name:AppFontBold, size: 13.0)
                birthdateLabel.textColor = NSColor.white
                
            }
        }
        
        CurrentAppearanceForgotUserViewController = "\(currentStyle)"
        
        
    }//end of appearanceCheck()
    
    
    
    
    
    
    //////////////////////////
    
    // Login Table info
    
    func ForgotUserTableData() {
        
        var DBUser: String!
        
        do {
            let UserQuery = try db.executeQuery("SELECT user FROM users", values:nil)
            
            while UserQuery.next() {
                DBUser = UserQuery.string(forColumn: "user")
                
                if (DBUser == "AppSDemo") {
                    
                } else {
                    let userToAdd = ForgotUserList.init(firstName:DBUser)
                    //print("userToAdd")
                    Users.append(userToAdd)
                }
            }
            
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "ForgotUserViewController-\(#function) Query Failed: \"SELECT user FROM users\"")
            DoAlert.DisplayAlert(Class: "ForgotUserViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"SELECT user FROM users\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
        }
        
        forgotUserTableView.reloadData()
        
    }
    
    
    
    func numberOfRows(in tableVieww: NSTableView) -> Int {
        //print("Users.count: \(Users.count)")
        return Users.count
        
    }
    
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        
        guard let cell = tableView.makeView(withIdentifier: tableColumn!.identifier, owner: self) as? NSTableCellView else { return nil }
        
        
        if (tableColumn?.identifier)!.rawValue == "username" {
            
            cell.textField?.stringValue = Users[row].firstName
            
        }
        
        
        
        
        return cell
        
    }
    
    
    
    func tableViewSelectionDidChange(_ notification: Notification) {
        
        let SelectedRow = forgotUserTableView.selectedRow
        //print("SelectedRow: \(SelectedRow)")
        //print("Users: \(Users.count)")
        
        let isIndexValid = Users.indices.contains(SelectedRow)
        
        if isIndexValid {
            let UserString: String = self.Users[SelectedRow].firstName
            //print("String: \(UserString)")
            usernameTextField.stringValue = UserString
            newPasswordSecureTextField.becomeFirstResponder()
        }
        
    }
    
    
   ///////////////////////////////////////////////////////////
    
    func RemoveWhiteSpace(aString: String) -> String {
        let replaced = aString.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        return replaced
    }

    @IBAction func changePasswordButton(_ sender: Any) {
        
        let NewPassword = newPasswordSecureTextField.stringValue
        let NewPasswordBlankCheck = NewPassword.trimmingCharacters(in: .whitespacesAndNewlines)
        
        let ConfirmPassword = confirmPasswordSecureTextField.stringValue
        let ConfirmPasswordBlankCheck = ConfirmPassword.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if !(NewPasswordBlankCheck == ConfirmPasswordBlankCheck) {
            
            print("WARNING: NewPasswordBlankCheck and ConfirmPasswordBlankCheck are not identical")
            loggly(LogType.Info, text: "ForgotUserViewController-\(#function): New Password and Confirm Password Do Not Match")
            DoAlert.DisplayAlert(Class: "ForgotUserViewController", Level: 3, Function: "\(#function)", MessageText: "Warning", InformativeText: "\nNew Password and Confirm Password Do Not Match\nPlease Try Again", DatabaseError: "")
            return
        }
        
        ////////////////////////////////////////////////////
        
        let birthdatePicker = birthdateDatePicker.dateValue
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let dateString = dateFormatter.string(from: birthdatePicker)
        
        var NumCount:Int32 = 0
        
        do {
            let UserCountQuery = try db.executeQuery("SELECT count(*) as count FROM users WHERE user = ? and birthdate = ?", values: ["\(usernameTextField.stringValue)","\(dateString)"])
            
            while UserCountQuery.next() {
               
                NumCount = UserCountQuery.int(forColumn: "count")
                //print("NumCount: \(NumCount)")
                
            }
            
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "ForgotUserViewController-\(#function): Query: SELECT count(*) as cnt FROM users WHERE user = ? and password = ? ")
            DoAlert.DisplayAlert(Class: "ForgotUserViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed", InformativeText: "SELECT count(*) as count FROM users WHERE user = ? and birthdate = ?", DatabaseError: "\(db.lastErrorMessage())")
            return
        }
        
        
        let encryptedPassword = ConfirmPasswordBlankCheck.base64Encoded()!
        
        
        if (NumCount > 0) {
            do {
                try db.executeUpdate("UPDATE users SET password = ? where birthdate = ? and user = ?", values: ["\(encryptedPassword)","\(dateString)","\(usernameTextField.stringValue)"])
            
            } catch {
                print("ERROR: \(db.lastErrorMessage())")
                loggly(LogType.Error, text: "ForgotUserViewController-\(#function): Query: UPDATE users SET password = ? where birthdate = ? and user = ?")
                DoAlert.DisplayAlert(Class: "ForgotUserViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed", InformativeText: "UPDATE users SET password = ? where birthdate = ? and user = ?", DatabaseError: "\(db.lastErrorMessage())")
                return
            }
                
            loggly(LogType.Info, text: "ForgotUserViewController-\(#function): \(usernameTextField.stringValue) successfully updated thier password")
            DoAlert.DisplayAlert(Class: "ForgotUserViewController", Level: 3, Function: "\(#function)", MessageText: "Update Password Successfull", InformativeText: "You can now login with the new credentials", DatabaseError: "")
            
            
        }
        
        CleanUp()
        
        self.view.window?.close()
        

    } // end of changePasswordButton()
    
    func CleanUp() {
        
        usernameTextField.stringValue = ""
        newPasswordSecureTextField.stringValue = ""
        confirmPasswordSecureTextField.stringValue = ""
        birthdateDatePicker.dateValue = NSDate() as Date
        
    }
    
    
    
    
    
}
