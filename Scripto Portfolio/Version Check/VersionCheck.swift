//
//  VersionCheck.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 1/28/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Foundation
import Cocoa
import SemanticVersioning
import SwiftLoggly





class Database: NSObject {
    
//    Given a version number MAJOR.MINOR.PATCH, increment the:
//
//    MAJOR version when you make incompatible API changes,
//    MINOR version when you add functionality in a backwards-compatible manner, and
//    PATCH version when you make backwards-compatible bug fixes.
//    Additional labels for pre-release and build metadata are available as extensions to the MAJOR.MINOR.PATCH format.

    
    
    func VersionChecks() {
        
        //delayWithSeconds(1.0) {
        
            if (DBVersion == "") {
                
                self.databaseVersionCheck()
            }
            

            
            self.appVersionCheck()
            
            //delayWithSeconds(0.10) {
           
                print("Database:\(#function) - AppVersion: \(AppVersion) DBVersion: \(DBVersion)")
                
                loggly(LogType.Info, text: "Scripto Portfolio: AppVersion: \(AppVersion) DBVersion: \(DBVersion)")

                ///////////////////////////
                
                var CurrentAppVersion = String()
                var CurrentDBVersion = String()
                
                do {
                    // let CurrentAppVersion: Version = Version(AppVersion)
                    
                    let CurrentAppVersion1 = try Version(AppVersion)
                    
                    CurrentAppVersion = String(describing: CurrentAppVersion1)
                    
                } catch let error as NSError {
                
                    let ErrorMessage = "VersionCheck:\(#function) - Version Failed  \n\(error)"
                    
                    print(ErrorMessage)
                    
                    loggly(LogType.Error, text: ErrorMessage)
                    
                    DoAlert.DisplayAlert(Class: "VersionCheck", Level: 1, Function: "\(#function)", MessageText: "Warning: CurrentAppVersion\nPlease restart Scripto Portfolio, if it continues please reboot your computer.", InformativeText: ErrorMessage, DatabaseError: "\(error)")
                    
                    return
                    
                }
        
                loggly(LogType.Info, text: "Scripto Portfolio: VersionCheck:\(#function) - CurrentAppVersion Passed")
        
                
                do {
                    // let CurrentDBVersion: Version = Version(DBVersion)
                    
                    let CurrentDBVersion1 = try Version(DBVersion)
                    
                    CurrentDBVersion = String(describing: CurrentDBVersion1)
                    
                    
                } catch let error as NSError {
                    
                    let ErrorMessage = "VersionCheck:\(#function) - Version Failed  \n\(error)"
                    
                    print(ErrorMessage)
                    
                    loggly(LogType.Error, text: ErrorMessage)
                    
                    DoAlert.DisplayAlert(Class: "VersionCheck", Level: 1, Function: "\(#function)", MessageText: "Warning CurrentDBVersion\nPlease restart Scripto Portfolio, if it continues please reboot your computer.", InformativeText: ErrorMessage, DatabaseError: "\(error)")
                    
                    return
                    
                }
        
                loggly(LogType.Info, text: "Scripto Portfolio: VersionCheck:\(#function) - CurrentDBVersion Passed")
                
                
                print("Database:\(#function) CurrentAppVersion: \(CurrentAppVersion) CurrentDBVersion: \(CurrentDBVersion)")
                
                ///////////////////////////
                
                // Older versions from "StoryWriting" that need to do an upgrade
                
                if (DBVersion == "1.0.0.4" || DBVersion == "1.0.1.5") {
                    
                    UpdateDatabase.StoryWritingDatabaseUpgrade()
                }
                
                ///////////////////////////
                
                
                // DB is an LESS THAN the app version
                if (CurrentDBVersion < CurrentAppVersion) {
                    
                    loggly(LogType.Info, text: "Scripto Portfolio: VersionCheck:\(#function) - CurrentDBVersion \(CurrentDBVersion) < CurrentAppVersion \(CurrentAppVersion)")
                    
                    DoneUpdating = false
                    
                    // point to update code
                    print("Updating DB: DB is an LOWER THAN the app version")
                    UpdateDatabase.UpdateDatabase()
                    
                    
                }
                
                // DB is equal to APP version
                if (CurrentDBVersion == CurrentAppVersion) {
                    DoneUpdating = true
                    loggly(LogType.Info, text: "Scripto Portfolio: VersionCheck:\(#function) - CurrentDBVersion \(CurrentDBVersion) == CurrentAppVersion \(CurrentAppVersion) - No Update Required")
                    print("No Update Required")
                    print("Database:\(#function) Same Version: CurrentAppVersion: \(CurrentAppVersion) CurrentDBVersion: \(CurrentDBVersion)")
                    return
                }
                
                // DB is GREATER than APP version
                if (CurrentDBVersion > CurrentAppVersion) {
                
                        loggly(LogType.Info, text: "Scripto Portfolio: VersionCheck:\(#function) - The database version \(CurrentDBVersion) is newer than the application version and will not be opened by Scripto Portfolio \(CurrentAppVersion)")
                    
                        let alert = NSAlert()
                        alert.messageText = "WARNING!!"
                        alert.informativeText = "The database version \(CurrentDBVersion) is newer than the application version and will not be opened by Scripto Portfolio \(CurrentAppVersion)"
                        alert.alertStyle = .warning
                        alert.addButton(withTitle: "OK")
                    
                        if alert.runModal() == .alertFirstButtonReturn {
                            print("Ok Pressed")
                            
                            NSApplication.shared.terminate(self)
                            
                        }
                    

                    
                    
                    
                
                    
                }
            
            //}// end of delay
        
        //}// end of delay
        
    }

    func appVersionCheck() {
        
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        let build = dictionary["CFBundleVersion"] as! String
        print("Version: \(version) build \(build)")
        print("Version: \(version).\(build)")
        //AppVersion = "\(version).\(build)"
        AppVersion = "\(version)" // SemanticVersioning handle only 3 digits EX 2.1.1 NOT EX 2.1.1.1
        
       
        
    }
    
    
    func databaseVersionCheck() {

        do {
            
            db.open()
            
            let DatabaseVersionQuery = try db.executeQuery("SELECT * FROM dbasetable WHERE db = 1", values:nil)
            
            while DatabaseVersionQuery.next() {
                let DBVersionDB = DatabaseVersionQuery.string(forColumn: "dbversion")!
                
                AlwaysSendLogsAppCenter = DatabaseVersionQuery.bool(forColumn: "alwayssendlogs")
                AppCenterSendLogsPrompt = DatabaseVersionQuery.bool(forColumn: "AppCenterSendLogsPrompt")
                
            
                
                let versionArray = DBVersionDB.components(separatedBy: ".")
                
                let Version = String("\(versionArray[0]).\(versionArray[1]).\(versionArray[2])")
                
                DBVersion = Version
                
                
            }

        } catch {
            print("ERROR: \(db.lastErrorMessage())")
        }
        
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}

