//
//  LocationsViewController.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 2/14/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Cocoa
import SwiftLoggly




class LocationsViewController: NSViewController, NSTableViewDataSource, NSTableViewDelegate {
    

    @IBOutlet weak var locationTableView: NSTableView!
    @IBOutlet weak var locationOpenLocationButton: NSButton!
    @IBOutlet weak var locationCloseButton: NSButton!
    @IBOutlet weak var locationRemoveLocationButton: NSButton!
    @IBOutlet weak var locationAddLocationButton: NSButton!

    @IBOutlet weak var LocationSearchTextField: NSSearchField!
    

    var LocationsArray: [LocationList] = []
    
    var SearchBool = false
    
    var CurrentAppearanceLocationsViewController = String()
    
    override func viewDidLoad() {
        
        
        
        
        
        super.viewDidLoad()
        // Do view setup here.
        


        locationTableView.delegate = self
        locationTableView.dataSource = self
        
        
        let DoubleSelect: Selector = #selector(LocationsViewController.locationOpenLocationButton(_:))
        locationTableView.doubleAction = DoubleSelect
        
        //LocationTableData()
        
        
    }
    
    override func viewWillAppear() {
        
        locationTableView.tableColumn(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "ikey"))?.isHidden = true
        
        
        
//        if (LocationInfoWindow.window?.isVisible == false) {
//            print("LocationInfoWindow.window?.isVisible = false NOT LOADING DIRECTORY IMAGES  1 ")
//            return
//        } else {
            LocationTableData()
//        }
        
        
        //LocationTableData()
        
    }
    
    override func viewDidLayout() {
        
        
        
        
        //        enum InterfaceStyle : String {
        //            case Dark, Light
        //
        //            init() {
        //                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
        //                self = InterfaceStyle(rawValue: type)!
        //            }
        //        }
        //
        //        let currentStyle = InterfaceStyle()
        //
        //        //print("currentStyle: \"\(currentStyle)\"  -  CurrentAppearance: \"\(CurrentAppearance)\" ")
        //
        //        if ("\(currentStyle)" != CurrentAppearanceNewUserViewController) {
        //            appearanceCheck()
        //        }
        
        appearanceCheck()
        
        //super.viewDidLayout()
        
        print("NewUserViewController - viewDidLayout finished")
        
        
    }// end of viewDidLayout()
    
    func appearanceCheck() {
        
        enum InterfaceStyle : String {
            case Dark, Light
            
            init() {
                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
                self = InterfaceStyle(rawValue: type)!
            }
        }
        
        let currentStyle = InterfaceStyle()
        
        if ("\(currentStyle)" != CurrentAppearanceLocationsViewController) {
            
            if (currentStyle.rawValue == "Dark") {
                
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColorDarkMode
                
                locationCloseButton.styleButtonText(button: locationCloseButton, buttonName: "Close", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                locationCloseButton.wantsLayer = true
                locationCloseButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                locationCloseButton.layer?.cornerRadius = 7
                
                locationOpenLocationButton.styleButtonText(button: locationOpenLocationButton, buttonName: "Open Location", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                locationOpenLocationButton.wantsLayer = true
                locationOpenLocationButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                locationOpenLocationButton.layer?.cornerRadius = 7
                
                
            } else if (currentStyle.rawValue == "Light") {
                
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColor.cgColor
                
                
                
                locationCloseButton.styleButtonText(button: locationCloseButton, buttonName: "Close", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                locationCloseButton.wantsLayer = true
                locationCloseButton.layer?.backgroundColor = NSColor.gray.cgColor
                locationCloseButton.layer?.cornerRadius = 7
                
                locationOpenLocationButton.styleButtonText(button: locationOpenLocationButton, buttonName: "Open Location", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                locationOpenLocationButton.wantsLayer = true
                locationOpenLocationButton.layer?.backgroundColor = NSColor.gray.cgColor
                locationOpenLocationButton.layer?.cornerRadius = 7
                
            }
        }
        
        CurrentAppearanceLocationsViewController = "\(currentStyle)"
        
        
    }//end of appearanceCheck()
    
    
    ////////////////////////////////////////////////
    
    
    func LocationTableData() {
        
        db.open()
        
        LocationsArray.removeAll()
        
        var Ikey: String!
        var Name: String!
        var Description: String!
        
        var Query = "select * from locationtable where userikeyreference = \"\(CurrentUserIkey!)\""
        
        //print("Query 1: \(Query)")
        
        if (SearchBool == true) {
            Query = "select * from locationtable where userikeyreference = \"\(CurrentUserIkey!)\" and name LIKE '%\(LocationSearchTextField.stringValue)%' OR location LIKE '%\(LocationSearchTextField.stringValue)%' "
            
            //print("Query 2: \(Query)")
            
        }

        do {
            
            let StoryQuery = try db.executeQuery(Query, values:nil)
            
            
            
            while StoryQuery.next() {
                Ikey = StoryQuery.string(forColumn: "ikey")
                Name = StoryQuery.string(forColumn: "name")
                Description = StoryQuery.string(forColumn: "location")
                
                
                print("Ikey: \(Ikey!) - Name: \(Name!) - Description: \(Description!)")
                //print("Name: \(Name!)")
                //print("Description: \(Description!)")
               
                
                //let StoryToAdd = StoryList.init(StoryName: StoryName, LastEdit: LastEdit, CreateDate: CreateDate, Ikey: Ikey)
                let LocationsToAdd = LocationList.init(Ikey: Ikey, Name: Name, Description: Description)
                
              
                LocationsArray.append(LocationsToAdd)
                
            }
            
        } catch {
            
            print("ERROR: LocationsViewController: Failed to get \"\(CurrentUserName!)\"'s Locations\n\(Query)\n\(db.lastErrorMessage())")
            
            let ErrorMessage = "LocationsViewController:\(#function) - Failed to get \"\(CurrentUserName!)\"'s Locations\n\(db.lastErrorMessage())\n\(Query))"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "LocationsViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to Failed to get \"\(CurrentUserName!)\"'s Locations\n\n\(Query)", DatabaseError: "\(db.lastErrorMessage())")
            
        }
        
        print("LocationsArray: \(LocationsArray)")
        print("LocationsArray Description: \(LocationsArray.description)")
        
        locationTableView.reloadData()
        
    }
    
    func numberOfRows(in tableVieww: NSTableView) -> Int {
        print("LocationsArray Count: \(LocationsArray.count)")
        return LocationsArray.count
        
    }
    
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        
        guard let cell = tableView.makeView(withIdentifier: tableColumn!.identifier, owner: self) as? NSTableCellView else
        {
            print("Table Returned Nil")
            return nil
        }
        
        
        if (tableColumn?.identifier)!.rawValue == "ikey" {
            cell.textField?.stringValue = LocationsArray[row].Ikey
        }
        if (tableColumn?.identifier)!.rawValue == "name" {
            cell.textField?.stringValue = LocationsArray[row].Name
        }
        if (tableColumn?.identifier)!.rawValue == "description" {
            cell.textField?.stringValue = LocationsArray[row].Description
        }
        
        
        
        return cell
        
    }
    
    func tableViewSelectionDidChange(_ notification: Notification) {
        
        let SelectedRow = locationTableView.selectedRow

        print("SelectedRow: \(SelectedRow)")
        
        if (SelectedRow == -1) {
            
            locationOpenLocationButton.isEnabled = false
            locationRemoveLocationButton.isEnabled = false
            return
        }
        
        let Name = LocationsArray[SelectedRow].Name
        let Ikey = LocationsArray[SelectedRow].Ikey
        let Description = LocationsArray[SelectedRow].Description

        print("Table Selection - Ikey: \"\(Ikey)\" Name: \"\(Name)\" Description: \"\(Description)\"")
        
        let isIndexValid = LocationsArray.indices.contains(SelectedRow)
        
        if isIndexValid {
            locationOpenLocationButton.isEnabled = true
            locationRemoveLocationButton.isEnabled = true
        } else {
            locationOpenLocationButton.isEnabled = false
            locationRemoveLocationButton.isEnabled = false
        }

        
    }
    

    ////////////////////////////////////////////////
    
    
    @IBAction func locationOpenLocationButton(_ sender: Any) {
        
        print("locationOpenLocationButton Pressed")
        
        
        
        let SelectedRow = locationTableView.selectedRow
        
        if (SelectedRow == -1) {
            return
        }
        
        let Name = LocationsArray[SelectedRow].Name
        let Ikey = LocationsArray[SelectedRow].Ikey
        
        print("LocationsViewController:   locationOpenLocationButton:  Name: \(Name) Ikey: \(Ikey)")
        
        LocationName = Name
        LocationIkey = Ikey
        
        print("LocationName: \(LocationName) LocationIkey: \(LocationIkey)")
        
        //OpenWindows.CurrentOpenWindows()
        
        if (CurrentlyOpenWindows.contains(LocationInfoWindow)) {
            LocationInfoWindow.window?.makeKeyAndOrderFront(self)
            
            
            let alert = NSAlert()
            alert.messageText = "Theres a Location already open"
            alert.informativeText = "Please close the open locatoin before opening another one."
            alert.alertStyle = .warning
            alert.addButton(withTitle: "Ok")
            
            if (CurrentAppearanceLocationsViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.window.titlebarAppearsTransparent = true
            
            alert.runModal()
            
            
            
            return
        }
        
        
        
        
        


        

        self.view.window?.close()
        LocationInfoWindow.showWindow(self)
        
        
    }
    
    @IBAction func locationAddIdeaButton(_ sender: Any) {
        
        print("locationAddIdeaButton Pressed")
        self.view.window?.close()
        
        LocationInfoWindow.showWindow(self)
        
        LocationNewLocation = true
        
    }
    
    @IBAction func locationsCloseButton(_ sender: Any) {
        
        print("locationsCloawButton Pressed")
        if (StoryWindow.window?.isVisible == true) {
            StoryWindow.window?.makeKeyAndOrderFront(nil)
        } else {
            StoryListWindow.showWindow(self)
        }
        
        self.view.window?.close()
//        OpenWindows.CheckForOpenWindows()
    }
    
    @IBAction func locationRemoveIdeaButton(_ sender: Any) {
        
        print("locationRemoveIdeaButton Pressed")
        
        
        let SelectedRow = locationTableView.selectedRow
        
        if (SelectedRow == -1) {
            return
        }
        
        let Name = LocationsArray[SelectedRow].Name
        let Ikey = LocationsArray[SelectedRow].Ikey
        
        let alert = NSAlert()
        alert.messageText = "Are you sure you want to Delete \"\(Name)\""
        alert.informativeText = "This action cannot be undone."
        alert.alertStyle = .warning
        alert.addButton(withTitle: "Canel")
        alert.addButton(withTitle: "Yes Delete")
        
        
        if (CurrentAppearanceLocationsViewController == "Dark") {
            alert.window.backgroundColor = AlertBackgroundColorDarkMode
        } else {
            alert.window.backgroundColor = WindowBackgroundColor
        }
        
        alert.window.titlebarAppearsTransparent = true
        
        let returnCode = alert.runModal()
        print("returnCode: \(returnCode)")
        
        if (returnCode.rawValue == 1000) {
            print("Canel Pressed")
        }
        if (returnCode.rawValue == 1001) {
            
            print("Yes Delete Pressed")

            let DeleteLocationQuery = "DELETE FROM locationtable WHERE ikey = \"\(Ikey)\" and name = \"\(Name)\" "
            
            do {
                
                try db.executeUpdate(DeleteLocationQuery, values:nil)

            } catch {
                
                let ErrorMessage = "LocationsVieController:\(#function) - Failed to Delete \"\(Name)\"'s Locations\n\(DeleteLocationQuery)\n\(db.lastErrorMessage())\n\(DeleteLocationQuery))"
                
                print(ErrorMessage)
                
                loggly(LogType.Error, text: ErrorMessage)
                
                DoAlert.DisplayAlert(Class: "LocationsViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to delete storyname \(EditStoryName)\n\n\(DeleteLocationQuery)", DatabaseError: "\(db.lastErrorMessage())")
                
                return
                
            }
            
            let DeleteImageTableQuery = "DELETE FROM imagetable WHERE locationtableikeyreference = \(Ikey)"
            
            //print("DeleteImageTableQuery: \"\(DeleteImageTableQuery)\" ")
            
            do {
                
                try db.executeUpdate(DeleteImageTableQuery, values:nil)
                
            } catch {
                
                let ErrorMessage = "LocationsVieController:\(#function) - Failed to Delete imagetable references for the Location\n\(DeleteImageTableQuery)\n\(db.lastErrorMessage())\n\(DeleteImageTableQuery))"
                
                print(ErrorMessage)
                
                loggly(LogType.Error, text: ErrorMessage)
                
                DoAlert.DisplayAlert(Class: "LocationsViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to Delete imagetable references for the Location\n\"\(DeleteImageTableQuery)\" ", DatabaseError: "\(db.lastErrorMessage())")
                
            }
            
            
            LocationTableData()
            
            
            
            
            
            //I reuse ikeys so if I delete a locaion then add a new one the new one will have the same ikey as the deleted one.
            
            
            
            
            
            
            
        } // end of 1000
        
        
        
        
        
        
        
    }
    
    @IBAction func locationsSearchField(_ sender: Any) {
        
        if (LocationSearchTextField.stringValue.count < 1 || SearchBool == true) {
            SearchBool = false
            LocationTableData()
        } else {
            SearchBool = true
            LocationTableData()
        }
        
        
        
        
        
    }
    
    
    func delayWithSeconds(_ seconds: Double, completion: @escaping () -> ()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            completion()
        }
    }
    
    
    
    
    
    
    
    
    
    
} // end of LocationsViewController class
