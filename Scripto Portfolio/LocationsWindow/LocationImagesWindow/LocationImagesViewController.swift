//
//  LocationImagesViewController.swift
//  Scripto Portfolio
//
//  Created by Dorian Glanville on 8/8/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Cocoa
import SwiftLoggly


let imageDirectoryLoader = ImageDirectoryLoader()

class LocationImagesViewController: NSViewController {
    
    
    
    
    
    @IBOutlet weak var collectionView: NSCollectionView!
    @IBOutlet weak var ImagesWindowCloseButton: NSButton!
    @IBOutlet weak var ImagesWindowScrollView: NSScrollView!
    @IBOutlet weak var ImagesWindowViewClipView: NSClipView!
    @IBOutlet weak var ImagesWindowAddImageButton: NSButton!
    @IBOutlet weak var ImagesWindowRemoveImageButton: NSButton!
    @IBOutlet weak var ImagesImageCountLabel: NSTextField!
    
    
  
    
    
    
    
    var indexPathsOfItemsBeingDragged: Set<NSIndexPath>!
    
    var CurrentAppearanceLocationImagesViewController = String()

    //  https://www.raywenderlich.com/145978/nscollectionview-tutorial
    
   /////////////////////////////////////////////////////////////////////////
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let initialFolderUrl = URL(fileURLWithPath: "/Library/Desktop Pictures", isDirectory: true)
        imageDirectoryLoader.loadDataForFolderWithUrl(initialFolderUrl)

        configureCollectionView()
        
        
        
        
        
    }
    
    override func viewWillAppear() {
        
       
        
        
//        let initialFolderUrl = URL(fileURLWithPath: "/Library/Desktop Pictures", isDirectory: true)
//        imageDirectoryLoader.loadDataForFolderWithUrl(initialFolderUrl)
//
//        configureCollectionView()
        
        imageDirectoryLoader.setupDataForUrls(nil)
        collectionView.reloadData()
        
        
        
        
        
        let initialFolderUrl = URL(fileURLWithPath: "/Library/Desktop Pictures", isDirectory: true)

        imageDirectoryLoader.loadDataForFolderWithUrl(initialFolderUrl)

        configureCollectionView()
        
        
        
        
        ////////////////////////////////////////////////
        // 1
        //collectionView.registerForDraggedTypes([NSURLPboardType])
        // 2
        collectionView.setDraggingSourceOperationMask(NSDragOperation.every, forLocal: true)
        // 3
        collectionView.setDraggingSourceOperationMask(NSDragOperation.every, forLocal: false)
        
        if (NumberOfImages == 1) {
            
            ImagesImageCountLabel.stringValue = "\(NumberOfImages) Image"
            
        } else {
            
            ImagesImageCountLabel.stringValue = "\(NumberOfImages) Images"
            
        }
        
        
        ImagesImageCountLabel.font = NSFont(name:AppFontBold, size: 13.0)
        ImagesImageCountLabel.textColor = NSColor.white
        
       
    }//end of viewwillappear()
    
    
    override func viewDidLayout() {
        
        //        enum InterfaceStyle : String {
        //            case Dark, Light
        //
        //            init() {
        //                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
        //                self = InterfaceStyle(rawValue: type)!
        //            }
        //        }
        //
        //        let currentStyle = InterfaceStyle()
        //
        //        //print("currentStyle: \"\(currentStyle)\"  -  CurrentAppearance: \"\(CurrentAppearance)\" ")
        //
        //        if ("\(currentStyle)" != CurrentAppearanceNewUserViewController) {
        //            appearanceCheck()
        //        }
        
        appearanceCheck()
        
        super.viewDidLayout()
        
        
        
        
    }// end of viewDidLayout()
    
    
    func appearanceCheck() {
        
        enum InterfaceStyle : String {
            case Dark, Light
            
            init() {
                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
                self = InterfaceStyle(rawValue: type)!
            }
        }
        
        let currentStyle = InterfaceStyle()
        
        if ("\(currentStyle)" != CurrentAppearanceLocationImagesViewController) {
            
            if (currentStyle.rawValue == "Dark") {
                
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColorDarkMode
                
                ImagesWindowCloseButton.styleButtonText(button: ImagesWindowCloseButton, buttonName: "Close", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                ImagesWindowCloseButton.wantsLayer = true
                ImagesWindowCloseButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                ImagesWindowCloseButton.layer?.cornerRadius = 7
                
                
                
            } else if (currentStyle.rawValue == "Light") {
                
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColor.cgColor
                
                ImagesWindowCloseButton.styleButtonText(button: ImagesWindowCloseButton, buttonName: "Close", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                ImagesWindowCloseButton.wantsLayer = true
                ImagesWindowCloseButton.layer?.backgroundColor = NSColor.gray.cgColor
                ImagesWindowCloseButton.layer?.cornerRadius = 7
                
            }
        }
        
        CurrentAppearanceLocationImagesViewController = "\(currentStyle)"
        
        
    }//end of appearanceCheck()
    
    
    
    
    
    
    
    
    
    
    
    func ReloadCollectionView() {
        
        let initialFolderUrl = URL(fileURLWithPath: "/Library/Desktop Pictures", isDirectory: true)
        
        imageDirectoryLoader.loadDataForFolderWithUrl(initialFolderUrl)
        
        configureCollectionView()
        
        collectionView.reloadData()
        
    }
    
//    override func mouseDown(with event: NSEvent) {
//
//        super.mouseDown(with: event)
//
//        print("GO HERE !!!!")
//
//        if event.clickCount > 1 {
//            //do something
//            print("mouseDown(with event: NSEvent) RAN!!!")
//        }
//    }
    

    
    
    

    
    func loadDataForNewFolderWithUrl(_ folderURL: URL) {
        
        
        imageDirectoryLoader.loadDataForFolderWithUrl(folderURL)
        collectionView.reloadData()
    }
    
    
    
    fileprivate func configureCollectionView() {
        // 1
//        let flowLayout = NSCollectionViewFlowLayout()
//        flowLayout.itemSize = NSSize(width: 160.0, height: 140.0)
//        flowLayout.sectionInset = NSEdgeInsets(top: 30.0, left: 20.0, bottom: 30.0, right: 20.0)
//        flowLayout.minimumInteritemSpacing = 20.0
//        flowLayout.minimumLineSpacing = 20.0
        //collectionView.collectionViewLayout = flowLayout
        // 2
        view.wantsLayer = true
        // 3
        //collectionView.layer?.backgroundColor = NSColor.black.cgColor
        
//        if #available(OSX 10.12, *) {
//            flowLayout.sectionHeadersPinToVisibleBounds = true
//        } else {
//            // Fallback on earlier versions
//        }
        
        
    }
    
    //////////////////////////////////////////////////////////////////
    
    
    @IBAction func showHideSections(sender: NSButton) {
        let show = sender.state
        // 1
        imageDirectoryLoader.singleSectionMode = (show == NSControl.StateValue.off)
        // 2
        imageDirectoryLoader.setupDataForUrls(nil)
        // 3
        collectionView.reloadData()
    }
    
    
    @IBAction func ImagesWindowCloseButtonAction(_ sender: Any) {
        
        
        imageDirectoryLoader.setupDataForUrls(nil)
        collectionView.reloadData()
        self.view.window?.close()
        
    }
    
  
    @IBAction func ImagesWindowAddImageButtonAction(_ sender: Any) {
        
        
        print("locationImageButton Pressed")

        ////////////////////
        
//        if (SharedItemLock == false) {
//
//        } else if (SharedItemLock == true) {
//            return
//        }
        
        print("LocationIkey: \"\(LocationIkey)\"")
        
        if (LocationIkey == "" ) {
            
            let alert = NSAlert()
            alert.messageText = "Location Not Saved"
            alert.informativeText = "Please Save your Location"
            alert.alertStyle = .warning
            alert.addButton(withTitle: "OK")
            
            
            if (CurrentAppearanceLocationImagesViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            
           
            alert.window.titlebarAppearsTransparent = true
            
            let returnCode = alert.runModal()
            
            if (returnCode.rawValue == 1000) {
                print("OK Pressed")
                return
            }
            
            return
            
        }
        
        
        
        
        
        let group = DispatchGroup()
        group.enter()
        
        DispatchQueue.main.async {
            //self.SaveButton(self)
            group.leave()
            print("ImagesWindowAddImageButtonAction: group.leave()")
        }
        
        // does not wait. But the code in notify() gets run
        // after enter() and leave() calls are balanced
        
        group.notify(queue: .main) {
            
            
            
            
            
            
            /////////////////////
            
            if let url = NSOpenPanel().selectUrl {
//                self.locationImageButton.image = NSImage(contentsOf: url)
//                print("file selected:", url.path)
                
                let ImagePath = url.path
                
                //////////////////
                
                let SelectLocationImageReferenceQuery = "Select * FROM locationtable WHERE ikey = \(LocationIkey)"
                var LocationImageReferenceDB = String()
                var LocationIkeyDB = String()
                
                do {
                    
                    let SelectLocationImageQueryRun = try db.executeQuery(SelectLocationImageReferenceQuery, values:nil)
                    
                    while SelectLocationImageQueryRun.next() {
                        let LocationImageReferenceDB1 = SelectLocationImageQueryRun.int(forColumn: "imagetableikeyreference")
                        
                        if (LocationImageReferenceDB1 == 0) {
                            LocationImageReferenceDB = ""
                        } else {
                            LocationImageReferenceDB = "\(LocationImageReferenceDB1)"
                        }
                        
                        
                        
                        LocationIkeyDB = SelectLocationImageQueryRun.string(forColumn: "ikey")!
                    }
                    
                    print("LocationImageReferenceDB: \"\(LocationImageReferenceDB)\"")
                    
                } catch {
                    
                    let ErrorMessage = "LocationsInfoViewController:\(#function) - Failed to get Images for \"\(LocationName)\" Location\n\(db.lastErrorMessage())\n\(SelectLocationImageReferenceQuery))"
                    
                    print(ErrorMessage)
                    
                    loggly(LogType.Error, text: ErrorMessage)
                    
                    DoAlert.DisplayAlert(Class: "LocationsInfoViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to get Images for \"\(LocationName)\" Location\n\n\(SelectLocationImageReferenceQuery)", DatabaseError: "\(db.lastErrorMessage())")
                    return
                }
                
                ///////////////////////////////////////////
                
                let ImageName = "\((ImagePath as NSString).lastPathComponent)"
                let FullPathImageCheck = "\(ImagesLocation)/\(ImageName)"
                
                
                let InsertNewImageQuery = "INSERT INTO imagetable (ikey,userikeyreference,characterreference,locationtableikeyreference,pathtoimage) VALUES ((select MAX(ikey) + 1 from imagetable),\"\(CurrentUserIkey!)\",'0',\"\(LocationIkeyDB)\",\"\(FullPathImageCheck)\")"
                
                print("InsertNewImageQuery: \n\(InsertNewImageQuery)")
                
                
                let Success = db.executeStatements(InsertNewImageQuery)
                
                if (!Success) {
                    
                    let ErrorMessage = "Failed to INSERT Image \((ImagePath as NSString).lastPathComponent) for Location: \"\(LocationName)\" Location\n\n\(InsertNewImageQuery)\n\n\(db.lastErrorMessage())"
                    
                    loggly(LogType.Error, text: ErrorMessage)
                    
                    DoAlert.DisplayAlert(Class: "LocationsInfoViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed INSERT Image \((ImagePath as NSString).lastPathComponent) for Location: \"\(LocationName)\" Location\n\n\(InsertNewImageQuery)", DatabaseError: "\(db.lastErrorMessage())")
                    AlertWindow.window?.makeKeyAndOrderFront(self)
                    return
                }
                
                
                
                
                
                
//                if (LocationImageReferenceDB.count < 2) {
//                    // Location Didnt have an image associated with it
//                    // INSERT
//                    
//                    
//                    
//                    let InsertNewImageQuery = "INSERT INTO imagetable (ikey,userikeyreference,characterreference,locationtableikeyreference,pathtoimage) VALUES ((select MAX(ikey) + 1 from imagetable),\"\(CurrentUserIkey!)\",'0',\"\(LocationIkeyDB)\",\"\(FullPathImageCheck)\")"
//                    
//                    print("InsertNewImageQuery: \n\(InsertNewImageQuery)")
//                    
//                    
//                    let Success = db.executeStatements(InsertNewImageQuery)
//                    
//                    if (!Success) {
//                        
//                        let ErrorMessage = "Failed to INSERT Image \((ImagePath as NSString).lastPathComponent) for Location: \"\(LocationName)\" Location\n\n\(InsertNewImageQuery)\n\n\(db.lastErrorMessage())"
//                        
//                        loggly(LogType.Error, text: ErrorMessage)
//                        
//                        DoAlert.DisplayAlert(Class: "LocationsInfoViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed INSERT Image \((ImagePath as NSString).lastPathComponent) for Location: \"\(LocationName)\" Location\n\n\(InsertNewImageQuery)", DatabaseError: "\(db.lastErrorMessage())")
//                        
//                    }
//                    else {
//
//                        var UpdateLocationImageReference = String()
//
//
//                        if (SharedItemOwner != "") {
//                            UpdateLocationImageReference = "UPDATE locationtable SET imagetableikeyreference = (select MAX(ikey) from imagetable) WHERE userikeyreference = \(SharedItemOwner) AND name = '\(LocationName)' "
//                        } else {
//                            UpdateLocationImageReference = "UPDATE locationtable SET imagetableikeyreference = (select MAX(ikey) from imagetable) WHERE userikeyreference = \(CurrentUserIkey!) AND name = '\(LocationName)' "
//                        }
//
//
//
//                        do {
//
//                            try db.executeUpdate(UpdateLocationImageReference, values: nil)
//
//                        } catch {
//
//                            let ErrorMessage = "Failed to INSERT Image reference for Location: \"\(LocationName)\" Location\n\n\(InsertNewImageQuery)\n\n\(db.lastErrorMessage())"
//
//                            loggly(LogType.Error, text: ErrorMessage)
//
//                            DoAlert.DisplayAlert(Class: "LocationsInfoViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed INSERT Image reference for Location: \"\(LocationName)\" Location\nSharedItemOwner = \"\(SharedItemOwner)\"\n\(UpdateLocationImageReference)", DatabaseError: "\(db.lastErrorMessage())")
//
//                        }
//
//
//                    }
//                    
//                    
//                } else {
//                    // Location DID have an image associated with it
//                    // UPDATE
//                    
//                    // UPDATE imagetable SET pathtoimage = \"%@\", locationtableikeyreference = \"%@\" WHERE userikeyreference =  %li AND ikey = %d",ImagePathForDB, userikeyreferenceIkeyDB ,CurrentUserIkey,LocationInImageDB
//                    
//                    var UpdateLocationImageReference = String()
//
//                    if (SharedItemOwner != "") {
//                        UpdateLocationImageReference = "UPDATE locationtable SET imagetableikeyreference = (select MAX(ikey) from imagetable) WHERE userikeyreference = \(SharedItemOwner) AND name = '\(LocationName)' "
//                    } else {
//                        UpdateLocationImageReference = "UPDATE locationtable SET imagetableikeyreference = (select MAX(ikey) from imagetable) WHERE userikeyreference = \(SharedItemOwner) AND name = '\(LocationName)' AND ikey = \"\(LocationIkeyDB)\""
//                    }
//
//
//
//                    do {
//
//                        try db.executeUpdate(UpdateLocationImageReference, values: nil)
//
//                    } catch {
//
//                        let ErrorMessage = "Failed to UPDATE Image reference for Location: \"\(LocationName)\" Location\n\n\(UpdateLocationImageReference)\n\n\(db.lastErrorMessage())"
//
//                        loggly(LogType.Error, text: ErrorMessage)
//
//                        DoAlert.DisplayAlert(Class: "LocationsInfoViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to UPDATE Image reference for Location: \"\(LocationName)\" Location\n\n\(UpdateLocationImageReference)", DatabaseError: "\(db.lastErrorMessage())")
//
//                    }
//
//                    self.collectionView.reloadData()
//                    
//                }
                
                ///////////////////
                // Copy image to Images folder
                
                if FM.fileExists(atPath: FullPathImageCheck) {
                    print("File exists")
                } else {
                    print("File not found")
                    do {
                        try FM.copyItem(atPath: ImagePath, toPath: FullPathImageCheck)
                    }
                    catch let error as NSError {
                        
                        let ErrorMessage = "Failed to copy image \((ImagePath as NSString).lastPathComponent) to Images folder\n\n\(error)"
                        
                        loggly(LogType.Error, text: ErrorMessage)
                        
                        DoAlert.DisplayAlert(Class: "LocationsInfoViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to copy image \((ImagePath as NSString).lastPathComponent) to Images folder \(FullPathImageCheck)", DatabaseError: "\(error)")
                        return
                    }
                }
                
                
                
                
                //////////// end of if
                
            } else {
                print("file selection was canceled")
            }
            
            self.ReloadCollectionView()
            
            
  
            
            if (NumberOfImages == 1) {
                
                self.ImagesImageCountLabel.stringValue = "\(NumberOfImages) Image"
                
            } else {
                
                self.ImagesImageCountLabel.stringValue = "\(NumberOfImages) Images"
                
            }
            
            
            //ImagesImageCountLabel
            
            
            
        }// end of dispatch notify
        
    }
    
    @IBAction func ImagesWindowRemoveImageButton(_ sender: Any) {
        
        //var item = -1
        
        
        
        if (SelectedIndexPath.count == 0 || SelectedIndexPath.first?.item == -1) {
            
            let alert = NSAlert()
            alert.messageText = "No Image Selected to Remove"
            alert.informativeText = "Please Select an Image."
            alert.alertStyle = .warning
            alert.addButton(withTitle: "OK")
            
            if (CurrentAppearanceLocationImagesViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.window.titlebarAppearsTransparent = true
            
            let returnCode = alert.runModal()
            
            if (returnCode.rawValue == 1000) {
                print("OK Pressed")
                return
            }
            
        }
        
        let item = (SelectedIndexPath.first?.item)!
        print("item: \"\(item)\"")
        
        let path:String = urls1[item].path
        print("System Path: \"\(path)\"")
        
        
        // remove ikey from imagetable
        // Check if the path exists in DB
        // if YES then dont delete the image from the image folder
        // if NO then delete the image from the image folder
        
        
        
        /////////////////////////
        
        
        
        let alert = NSAlert()
        alert.messageText = "Do you want to Delete \"\((path as NSString).lastPathComponent)\" from this location?"
        alert.informativeText = "Are you sure ?"
        alert.alertStyle = .warning
        alert.addButton(withTitle: "Cancel")
        alert.addButton(withTitle: "Yes Delete")
        
        if (CurrentAppearanceLocationImagesViewController == "Dark") {
            alert.window.backgroundColor = AlertBackgroundColorDarkMode
        } else {
            alert.window.backgroundColor = WindowBackgroundColor
        }
        
        alert.window.titlebarAppearsTransparent = true
        
        let returnCode = alert.runModal()
        
        if (returnCode.rawValue == 1000) {
            print("Cancel Pressed")
            return
        }
        
        if (returnCode.rawValue == 1001) {
            print("Yes Delete Pressed")
        

        
            /////////////////////////
        
        
            let DeleteImageStatement = "DELETE FROM imagetable WHERE locationtableikeyreference = \(LocationIkey) and pathtoimage =\"\(path)\" "
        
        

            print("DeleteImageStatement: \(DeleteImageStatement)")


            let Success = db.executeStatements(DeleteImageStatement)

            if (!Success) {
                
                let ErrorMessage = "Failed to Delete Images\n\"\(path)\" From DB. \n\n\(db.lastErrorMessage())"
                
                loggly(LogType.Error, text: ErrorMessage)
                
                DoAlert.DisplayAlert(Class: "LocationsInfoViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to Delete Images\n\"\(path)\" From DB. Location\n\n\(DeleteImageStatement)", DatabaseError: "\(db.lastErrorMessage())")
                
                return
                
            } else {
                
                var GetImageCountQuery = ""
                var ImageCount = Int32()
                
                do {
                    
                    GetImageCountQuery = "SELECT Count(PATHTOIMAGE) as cnt from imagetable where pathtoimage = \"\(path)\" "
                    
                    print("GetImageCountQuery: \"\(GetImageCountQuery)\"")
                    
                    let GetImageCountQueryRun = try db.executeQuery(GetImageCountQuery, values:nil)
                    
                    while GetImageCountQueryRun.next() {
                        
                        ImageCount = GetImageCountQueryRun.int(forColumn: "cnt")
                        
                    }
                    
                    print("ImageCount: \"\(ImageCount)\"")
                    
                } catch {
                    print("ERROR: \(db.lastErrorMessage())")
                    loggly(LogType.Error, text: "LocationsInfoViewController-\(#function) Query Failed: \"\(GetImageCountQuery)\"")
                    
                    DoAlert.DisplayAlert(Class: "LocationsInfoViewController", Level: 1, Function: "\(#function)", MessageText: "\"\(GetImageCountQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                    
                    return
                    
                }
                
                
                if (ImageCount == 0) {
                    // if there are no more references to the image in question then remove it from the Images Folder.
                    
                    if FM.fileExists(atPath: path) {
                        print("File Found")
                        
                        do {
                            try FM.removeItem(atPath: path)
                        }
                        catch let error as NSError {
                            
                            let ErrorMessage = "Failed to Delete image \((path as NSString).lastPathComponent) in Images folder\n\n\(error)"
                            
                            loggly(LogType.Error, text: ErrorMessage)
                            
                            DoAlert.DisplayAlert(Class: "LocationsInfoViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to Delete image \((path as NSString).lastPathComponent) in Images folder: \(path)", DatabaseError: "\(error)")
                        }
                        
                        print("File Deleted")
                        
                    } else {
                        print("File not found")
                    }
                    
                } else {
                    
                    print("Image is still being referenced in DB. Was Not Deleted.")
                    
                }
                
                
                
                
            }// end of else
        
        
        
        
        
            ReloadCollectionView()
            
            if (NumberOfImages == 1) {
                
                self.ImagesImageCountLabel.stringValue = "\(NumberOfImages) Image"
                
            } else {
                
                self.ImagesImageCountLabel.stringValue = "\(NumberOfImages) Images"
                
            }
        
        
        }
        
        
    }// end of ImagesWindowRemoveImageButton
    
    
    
    
//    func EnableRemoveButton() {
//
//        if (ImagesWindowRemoveImageButton.isEnabled == false) {
//            ImagesWindowRemoveImageButton.isEnabled = true
//        } else {
//            ImagesWindowRemoveImageButton.isEnabled = false
//        }
//
//
//
//    }
    
    
    
    
    
    
    
    
    
    
    
    
    
}// end of ViewController





extension LocationImagesViewController : NSCollectionViewDataSource {
    
    // 1
    public func numberOfSections(in collectionView: NSCollectionView) -> Int {
        //return ImageDirectoryLoader.numberOfSections
        
        return imageDirectoryLoader.numberOfSections
    }
    
    // 2
    public func collectionView(_ collectionView: NSCollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageDirectoryLoader.numberOfItemsInSection(section)
    }
    
    // 3
    public func collectionView(_ itemForRepresentedObjectAtcollectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
        
        // 4
        let item = collectionView.makeItem(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "CollectionViewItem"), for: indexPath)
        guard let collectionViewItem = item as? CollectionViewItem else {return item}
        
        // 5
        let imageFile = imageDirectoryLoader.imageFileForIndexPath(indexPath)
        collectionViewItem.imageFile = imageFile
        return item
    }
    
    private func collectionView(_ collectionView: NSCollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> NSView {
        // 1                                                    //NSCollectionElementKindSectionHeader
        let view = collectionView.makeSupplementaryView(ofKind: NSCollectionView.SupplementaryElementKind.sectionHeader, withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "HeaderView"), for: indexPath) as! HeaderView
        // 2
        view.sectionTitle.stringValue = "Section \(indexPath.section)"
        let numberOfItemsInSection = imageDirectoryLoader.numberOfItemsInSection(indexPath.section)
        view.imageCount.stringValue = "\(numberOfItemsInSection) image files"
        return view
    }
    
    
    func collectionView(_ collectionView: NSCollectionView, didSelectItemsAt indexPaths: Set<IndexPath>) {
        //print("IndexPath: \"\(indexPaths)\"")
        
        SelectedIndexPath = indexPaths as Set<NSIndexPath>
        
//        let item = SelectedIndexPath.first?.item
//        print("item: \"\(item!)\"")
//
//        let path:String = urls1[item!].path
//        print("System Path: \"\(path)\"")
        
    }
    
    func collectionView(_ collectionView: NSCollectionView, didDeselectItemsAt indexPaths: Set<IndexPath>) {
        print("Deselected IndexPath: \"\(indexPaths))\"")
//        let BlankArray:Array = [-1,-1]
       
        
        let BlankIndexPath = NSIndexPath(forItem: -1, inSection: -1)
        var BlankSet = Set<NSIndexPath>()
        BlankSet.insert(BlankIndexPath as NSIndexPath)
        
        
        SelectedIndexPath = BlankSet
        
        print("Deselected IndexPath: \"\(SelectedIndexPath))\"")
        
        
    }
    
    ///////////////////////////////
    

//    override func mouseDown(with event: NSEvent) {
//
//        super.mouseDown(with: event)
//
//        print("GO HERE !!!!")
//
//        if event.clickCount > 1 {
//            //do something
//            print("mouseDown(with event: NSEvent) RAN!!!")
//        }
//    }
    

    
}// end of extension ViewController : NSCollectionViewDataSource




extension NSViewController : NSCollectionViewDelegateFlowLayout {
    public func collectionView(_ collectionView: NSCollectionView, layout collectionViewLayout: NSCollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> NSSize {
        return imageDirectoryLoader.singleSectionMode ? NSZeroSize : NSSize(width: 1000, height: 40)
}
}//end of extension ViewController : NSCollectionViewDelegateFlowLayout
