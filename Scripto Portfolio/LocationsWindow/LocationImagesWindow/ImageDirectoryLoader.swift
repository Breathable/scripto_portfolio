/*
 * ImageDirectoryLoader.swift
 * SlidesMagic
 *
 * Created by Gabriel Miro on Oct 2016.
 * Copyright (c) 2016 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import Cocoa

import SwiftLoggly


var urls1: [URL] = []
var ArrayOrder = Array<Int>()

class ImageDirectoryLoader: NSObject {
    
    //var urls1: [URL] = []
    
    fileprivate var imageFiles = [ImageFile]()
    fileprivate(set) var numberOfSections = 1
    var singleSectionMode = true
    
    fileprivate struct SectionAttributes {
        var sectionOffset: Int  // the index of the first image of this section in the imageFiles array
        var sectionLength: Int  // number of images in the section
    }
    
    // sectionLengthArray - An array of randomly picked integers just for demo purposes.
    // sectionLengthArray[0] is 7, i.e. put the first 7 images from the imageFiles array into section 0
    // sectionLengthArray[1] is 5, i.e. put the next 5 images from the imageFiles array into section 1
    // and so on...
    fileprivate let sectionLengthArray = [7, 5, 10, 2, 11, 7, 10, 12, 20, 25, 10, 3, 30, 25, 40]
    fileprivate var sectionsAttributesArray = [SectionAttributes]()
    
    func setupDataForUrls(_ urls: [URL]?) {
        
        
        
        
        
        
        if let urls = urls {                    // When new folder
            createImageFilesForUrls(urls)
        }
        
        if sectionsAttributesArray.count > 0 {  // If not first time, clean old sectionsAttributesArray
            sectionsAttributesArray.removeAll()
        }
        
        numberOfSections = 1
        
        if singleSectionMode {
            setupDataForSingleSectionMode()
        } else {
            setupDataForMultiSectionMode()
        }
        
    }
    
    fileprivate func setupDataForSingleSectionMode() {
        let sectionAttributes = SectionAttributes(sectionOffset: 0, sectionLength: imageFiles.count)
        sectionsAttributesArray.append(sectionAttributes) // sets up attributes for first section
    }
    
    fileprivate func setupDataForMultiSectionMode() {
        
        let haveOneSection = singleSectionMode || sectionLengthArray.count < 2 || imageFiles.count <= sectionLengthArray[0]
        var realSectionLength = haveOneSection ? imageFiles.count : sectionLengthArray[0]
        var sectionAttributes = SectionAttributes(sectionOffset: 0, sectionLength: realSectionLength)
        sectionsAttributesArray.append(sectionAttributes) // sets up attributes for first section
        
        guard !haveOneSection else {return}
        
        var offset: Int
        var nextOffset: Int
        let maxNumberOfSections = sectionLengthArray.count
        for i in 1..<maxNumberOfSections {
            numberOfSections += 1
            offset = sectionsAttributesArray[i-1].sectionOffset + sectionsAttributesArray[i-1].sectionLength
            nextOffset = offset + sectionLengthArray[i]
            if imageFiles.count <= nextOffset {
                realSectionLength = imageFiles.count - offset
                nextOffset = -1 // signal this is last section for this collection
            } else {
                realSectionLength = sectionLengthArray[i]
            }
            sectionAttributes = SectionAttributes(sectionOffset: offset, sectionLength: realSectionLength)
            sectionsAttributesArray.append(sectionAttributes)
            if nextOffset < 0 {
                break
            }
        }
    }
    
    fileprivate func createImageFilesForUrls(_ urls: [URL]) {
        if imageFiles.count > 0 {   // When not initial folder
            imageFiles.removeAll()
        }
        for url in urls {
            if let imageFile = ImageFile(url: url) {
                imageFiles.append(imageFile)
            }
        }
        
        
    }
    
    fileprivate func getFilesURLFromFolder(_ folderURL: URL) -> [URL]? {
        
        urls1.removeAll()
        
//        var urls1: [URL] = []
//
//        let fileUrl1 = NSURL(fileURLWithPath: "/Users/dorian/Library/Containers/com.Scripto-Portfolio/Data/Documents/Images/Anime-Wallpaper-Desktop-Background-48.jpg")
//        let fileUrl2 = NSURL(fileURLWithPath: "/Users/dorian/Library/Containers/com.Scripto-Portfolio/Data/Documents/Images/Anime-Wallpaper-Desktop-Background-47.jpg")
//
//
//
//        urls1.append(fileUrl1 as URL)
//        urls1.append(fileUrl2 as URL)
        
        LoadCollectionViewArray()
        
        NumberOfImages = urls1.count
        
       
        
        //////////////////////////////////////////////////////
        
        //    let options: FileManager.DirectoryEnumerationOptions =
        //    [.skipsHiddenFiles, .skipsSubdirectoryDescendants, .skipsPackageDescendants]
        //    let fileManager = FileManager.default
        let resourceValueKeys = [URLResourceKey.isRegularFileKey, URLResourceKey.typeIdentifierKey]
        //
        //    guard let directoryEnumerator = fileManager.enumerator(at: folderURL, includingPropertiesForKeys: resourceValueKeys,
        //      options: options, errorHandler: { url, error in
        //        print("`directoryEnumerator` error: \(error).")
        //        return true
        //    }) else { return nil }
        
        //var urls: [URL] = []  in directoryEnumerator
        
        var urls: [URL] = []
        for case let url in urls1 {
            do {
                let resourceValues = try (url as NSURL).resourceValues(forKeys: resourceValueKeys)
                guard let isRegularFileResourceValue = resourceValues[URLResourceKey.isRegularFileKey] as? NSNumber else { continue }
                guard isRegularFileResourceValue.boolValue else { continue }
                guard let fileType = resourceValues[URLResourceKey.typeIdentifierKey] as? String else { continue }
                guard UTTypeConformsTo(fileType as CFString, "public.image" as CFString) else { continue }
                urls.append(url)
            }
            catch {
                print("Unexpected error occured: \(error).")
            }
        }
        return urls
    }
    
    func numberOfItemsInSection(_ section: Int) -> Int {
        
        
        
        return sectionsAttributesArray[section].sectionLength
        
        
        
        
    }
    
    func imageFileForIndexPath(_ indexPath: IndexPath) -> ImageFile {
        let imageIndexInImageFiles = sectionsAttributesArray[indexPath.section].sectionOffset + indexPath.item
        
        let imageFile = imageFiles[imageIndexInImageFiles]
        return imageFile
    }
    
    func loadDataForFolderWithUrl(_ folderURL: URL) {
        let urls = getFilesURLFromFolder(folderURL)
        if let urls = urls {
            print("\(urls.count) images found in directory \(folderURL.lastPathComponent)")
            for url in urls {
                print("\(url)")
                print("\(url.lastPathComponent)")
                
            }
        }
        setupDataForUrls(urls)
    }
    
    
    
    
    
    
    
    
    
    
    
    func LoadCollectionViewArray() {
    

        if (!(LocationInfoWindow.window?.isVisible)! == true) {
            return
        }

    
            ConnectionTest.CheckConnection()
            db.open()



            let LocationImagesQuery = "SELECT * FROM locationtable WHERE ikey = \(LocationIkey)"


            var LocationImageIkeyReferenceDB = String()



            do {
                let LocationImagesQueryRun = try db.executeQuery(LocationImagesQuery, values:nil)

                while LocationImagesQueryRun.next() {

                    LocationImageIkeyReferenceDB = LocationImagesQueryRun.string(forColumn: "ikey")!

                    //print("LocationLocationDB: \"\(LocationImageIkeyReferenceDB)\" ")

                    //LocationImagesQueryRun.next()

                }

            } catch {
                print("ERROR: \(db.lastErrorMessage())")
                loggly(LogType.Error, text: "ImageDirectoryLoader-\(#function) Query Failed: \"\(LocationImagesQuery)\"")

                DoAlert.DisplayAlert(Class: "ImageDirectoryLoader", Level: 1, Function: "\(#function)", MessageText: "\"\(LocationImagesQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")

                return

            }

            db.close()
            db.open()





            ////////////////

            var GetImageCountQuery = ""
            var ImageCount = Int32()

            do {

            GetImageCountQuery = "SELECT Count(PATHTOIMAGE) as cnt from imagetable where locationtableikeyreference = \(LocationImageIkeyReferenceDB)"

            print("GetImageCountQuery: \"\(GetImageCountQuery)\"")

            let GetImageCountQueryRun = try db.executeQuery(GetImageCountQuery, values:nil)

            while GetImageCountQueryRun.next() {

                ImageCount = GetImageCountQueryRun.int(forColumn: "cnt")

            }

            print("ImageCount: \"\(ImageCount)\"")

            } catch {
                print("ERROR: \(db.lastErrorMessage())")
                loggly(LogType.Error, text: "ImageDirectoryLoader-\(#function) Query Failed: \"\(GetImageCountQuery)\"")

                DoAlert.DisplayAlert(Class: "ImageDirectoryLoader", Level: 1, Function: "\(#function)", MessageText: "\"\(GetImageCountQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")

                return

            }


            db.close()
            db.open()

            ////////////////////////

            var GetImageQuery = ""

            do {

            GetImageQuery = "SELECT * from imagetable where locationtableikeyreference = \(LocationImageIkeyReferenceDB)"

            print("GetImageQuery: \"\(GetImageQuery)\"")

            var GetImagePathDB = String()
            var Count = 1

            let GetImageQueryRun = try db.executeQuery(GetImageQuery, values:nil)

            while GetImageQueryRun.next() {




                GetImagePathDB = GetImageQueryRun.string(forColumn: "PATHTOIMAGE")!

                print("GetImagePathDB: \"\(GetImagePathDB)\"")

                let fileUrl1 = NSURL(fileURLWithPath: GetImagePathDB)

                print("fileUrl1 ADD: \"\(fileUrl1)\" ")

                urls1.append(fileUrl1 as URL)

                Count = Count + 1

                //GetImageQueryRun.next()

            }

                if (Count > ImageCount) {
                    print("GetImageQueryRun.next BREAK")
                } else {
                    GetImageQueryRun.next()
                }

            } catch {
                print("ERROR: \(db.lastErrorMessage())")
                loggly(LogType.Error, text: "ImageDirectoryLoader-\(#function) Query Failed: \"\(LocationImagesQuery)\"")

                DoAlert.DisplayAlert(Class: "ImageDirectoryLoader", Level: 1, Function: "\(#function)", MessageText: "\"\(LocationImagesQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")

                return

            }



            print("urls1.count \"\(urls1.count)\" ")
    
    
    
    
    
    
    }// end of function
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}// end of controller
