//
//  LocationImagesWindowController.swift
//  Scripto Portfolio
//
//  Created by Dorian Glanville on 8/8/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Cocoa

class LocationImagesWindowController: NSWindowController {

    override func windowDidLoad() {
        super.windowDidLoad()
    
        // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
        
        window?.titlebarAppearsTransparent = true
        window?.titleVisibility = .hidden
        
//        if let window = window, let screen = NSScreen.main {
//            let screenRect = screen.visibleFrame
//            window.setFrame(NSRect(x: screenRect.origin.x, y: screenRect.origin.y, width: screenRect.width/2.0, height: screenRect.height), display: true)
//        }
        
    }

}
