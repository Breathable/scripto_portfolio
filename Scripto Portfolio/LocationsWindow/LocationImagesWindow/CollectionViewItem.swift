//
//  CollectionViewItem.swift
//  SlidesMagic
//
//  Created by Dorian Glanville on 8/6/18.
//  Copyright © 2018 razeware. All rights reserved.
//

import Cocoa
class CollectionViewItem: NSCollectionViewItem {
    
    override var isSelected: Bool {
        
        didSet {
            
            view.layer?.borderWidth = isSelected ? 5.0 : 0.0
            
            // This is where you should put your code for what image was selected
            
            // https://stackoverflow.com/questions/27505022/open-another-mac-app
            // double click to open
            
            
            
        }
    }
    
    // 1
    var imageFile: ImageFile? {
        didSet {
            guard isViewLoaded else {
                print("isViewLoaded = FALSE")
                
                
                return
            }
            
            if let imageFile = imageFile {
                imageView?.image = imageFile.thumbnail
                textField?.stringValue = imageFile.fileName
                self.view.toolTip = imageFile.fileName
            } else {
                imageView?.image = nil
                textField?.stringValue = ""
                self.view.toolTip = ""
            }
            
            
        }
    }
    
    // 2
    override func viewDidLoad() {
        
       
        
        super.viewDidLoad()
        view.wantsLayer = true
        view.layer?.backgroundColor = NSColor.lightGray.cgColor
        // 1
        view.layer?.borderColor = NSColor.systemYellow.cgColor
        // 2
        view.layer?.borderWidth = 0.0
    }
    
}
