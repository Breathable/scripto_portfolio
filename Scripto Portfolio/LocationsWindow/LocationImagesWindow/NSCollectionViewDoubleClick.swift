//
//  NSCollectionViewDoubleClick.swift
//  Scripto Portfolio
//
//  Created by Dorian Glanville on 8/12/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Cocoa

class NSCollectionViewDoubleClick: NSCollectionView {

    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)

        //print("DRAW")
        
        self.wantsLayer = true
        //self.layer?.backgroundColor = NSColor.black.cgColor
        
        
        let flowLayout = NSCollectionViewFlowLayout()
        flowLayout.itemSize = NSSize(width: 160.0, height: 140.0)
        flowLayout.sectionInset = NSEdgeInsets(top: 30.0, left: 20.0, bottom: 30.0, right: 20.0)
        flowLayout.minimumInteritemSpacing = 10.0
        flowLayout.minimumLineSpacing = 10.0
        
        self.collectionViewLayout = flowLayout
        
    }
    
    
    
    override func mouseDown(with event: NSEvent) {
        
        super.mouseDown(with: event)
        
        
        
        //rint("GOT HERE !!!!")
        
        if event.clickCount > 1 {
            //do something
            //print("mouseDown(with event: NSEvent) RAN!!!")
            
            let item = SelectedIndexPath.first?.item
            print("item: \"\(item!)\"")
            
            let path:String = urls1[item!].path
            print("System Path: \"\(path)\"")
            
            if (path != "") {

                NSWorkspace.shared.openFile("\(path)", withApplication: "Preview")
                
            }
 
        }
    }// end of mouseDown
    
    
    
}
