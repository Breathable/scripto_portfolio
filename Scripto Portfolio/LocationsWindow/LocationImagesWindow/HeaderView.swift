//
//  HeaderView.swift
//  SlidesMagic
//
//  Created by Dorian Glanville on 8/6/18.
//  Copyright © 2018 razeware. All rights reserved.
//

//import Cocoa
//
//class HeaderView: NSView {
//
////    override func draw(_ dirtyRect: NSRect) {
////        super.draw(dirtyRect)
////
////        // Drawing code here.
////    }
//
//  // 1
//  @IBOutlet weak var sectionTitle: NSTextField!
//  @IBOutlet weak var imageCount: NSTextField!
//
//  // 2
//  override func draw(_ dirtyRect: NSRect) {
//    super.draw(dirtyRect)
//    NSColor(calibratedWhite: 0.8 , alpha: 0.8).set()
//    __NSRectFillUsingOperation(dirtyRect, NSCompositingOperation.sourceOver)
//  }
//
//}
import Cocoa

class HeaderView: NSView {
    
    //    override func draw(_ dirtyRect: NSRect) {
    //        super.draw(dirtyRect)
    //
    //        // Drawing code here.
    //    }
    
    // 1
    @IBOutlet weak var sectionTitle: NSTextField!
    @IBOutlet weak var imageCount: NSTextField!
    
    // 2
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
        NSColor(calibratedWhite: 0.8 , alpha: 0.8).set()
        __NSRectFillUsingOperation(dirtyRect, NSCompositingOperation.sourceOver)
    }
    
}
