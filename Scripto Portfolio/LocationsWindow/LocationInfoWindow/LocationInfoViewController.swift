//
//  LocationInfoViewController.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 2/14/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Cocoa
import SwiftLoggly



class LocationInfoViewController: NSViewController, NSTextViewDelegate, NSTextFieldDelegate {

    //  https://www.raywenderlich.com/145978/nscollectionview-tutorial
    
//    @IBOutlet weak var locationInfoNameTextField: NSTextField!
//    @IBOutlet weak var locationInfoLocationInformationTextField: NSTextField!
//    @IBOutlet var locationInfoInformationTextView: NSTextView!
    
    
    
    @IBOutlet var nameTextField: NSTextField!
    @IBOutlet var locationInfoTextField: NSTextField!
    @IBOutlet var informationTextView: NSTextView!
    

    @IBOutlet weak var locationInfoCloseButton: NSButton!
    @IBOutlet weak var locationInfoSaveButton: NSButton!
    @IBOutlet weak var locationImageButton: NSButton!
    @IBOutlet weak var locationImagesButton: NSButton!
    

    var SaveNeeded = false
    var CleanUpCompleted = false
    
    var CurrentAppearanceLocationsInfoViewController = String()
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        

        //print("LocationInfoViewController viewDidLoad() RAN")
        
        nameTextField.delegate = self
        locationInfoTextField.delegate = self
        informationTextView.delegate = self
        
        locationImageButton.image = NSImage(named: NSImage.Name(rawValue: "ChooseAnImage.png"))
        
      
        
    }
    
    override func viewWillAppear() {
        
        
        
       
        
        print("LocationIkey: \(LocationIkey) LocationName: \(LocationName)")
        
        
        
        if (LocationIkey != "" && LocationName != "") {
            
            OpenLocation()
            
            if (SharedItemLock == false) {
                LocaionUnLock()
            } else if (SharedItemLock == true) {
                LocaionLock()
            }
            
        } else {
            
            CleanUp()
        }
        
    }
    
    
    override func viewDidLayout() {
        
        //        enum InterfaceStyle : String {
        //            case Dark, Light
        //
        //            init() {
        //                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
        //                self = InterfaceStyle(rawValue: type)!
        //            }
        //        }
        //
        //        let currentStyle = InterfaceStyle()
        //
        //        //print("currentStyle: \"\(currentStyle)\"  -  CurrentAppearance: \"\(CurrentAppearance)\" ")
        //
        //        if ("\(currentStyle)" != CurrentAppearanceNewUserViewController) {
        //            appearanceCheck()
        //        }
        
        
      
        
        appearanceCheck()
        
        super.viewDidLayout()
        
        print("NewUserViewController - viewDidLayout finished")
        
        
    }// end of viewDidLayout()
    
    
    func appearanceCheck() {
        
        enum InterfaceStyle : String {
            case Dark, Light
            
            init() {
                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
                self = InterfaceStyle(rawValue: type)!
            }
        }
        
        let currentStyle = InterfaceStyle()
        
        if ("\(currentStyle)" != CurrentAppearanceLocationsInfoViewController) {
            
            if (currentStyle.rawValue == "Dark") {
                
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColorDarkMode
                
//                informationTextView.wantsLayer = true
//                informationTextView.backgroundColor = NSColor.gray
                
                locationInfoCloseButton.styleButtonText(button: locationInfoCloseButton, buttonName: "Close", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                locationInfoCloseButton.wantsLayer = true
                locationInfoCloseButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                locationInfoCloseButton.layer?.cornerRadius = 7
                
                locationInfoSaveButton.styleButtonText(button: locationInfoSaveButton, buttonName: "Save", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                locationInfoSaveButton.wantsLayer = true
                locationInfoSaveButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                locationInfoSaveButton.layer?.cornerRadius = 7
                
                locationImagesButton.styleButtonText(button: locationImagesButton, buttonName: "Images", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                locationImagesButton.wantsLayer = true
                locationImagesButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                locationImagesButton.layer?.cornerRadius = 7
                
                
            } else if (currentStyle.rawValue == "Light") {
                
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColor.cgColor
                
                informationTextView.wantsLayer = true
                informationTextView.backgroundColor = NSColor.white
                
                locationInfoCloseButton.styleButtonText(button: locationInfoCloseButton, buttonName: "Close", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                locationInfoCloseButton.wantsLayer = true
                locationInfoCloseButton.layer?.backgroundColor = NSColor.gray.cgColor
                locationInfoCloseButton.layer?.cornerRadius = 7
                
                locationInfoSaveButton.styleButtonText(button: locationInfoSaveButton, buttonName: "Save", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                locationInfoSaveButton.wantsLayer = true
                locationInfoSaveButton.layer?.backgroundColor = NSColor.gray.cgColor
                locationInfoSaveButton.layer?.cornerRadius = 7
                
                locationImagesButton.styleButtonText(button: locationImagesButton, buttonName: "Images", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                locationImagesButton.wantsLayer = true
                locationImagesButton.layer?.backgroundColor = NSColor.gray.cgColor
                locationImagesButton.layer?.cornerRadius = 7
                
            }
        }
        
        CurrentAppearanceLocationsInfoViewController = "\(currentStyle)"
        
        
    }//end of appearanceCheck()
    
    
    
    

    
    func LocaionLock() {
        
        nameTextField.isEditable = false
        
        locationInfoTextField.isEditable = false
        
        informationTextView.isEditable = false
        
        locationInfoSaveButton.isEnabled = false
        
    }
    
    func LocaionUnLock() {
        
        nameTextField.isEditable = true
        
        locationInfoTextField.isEditable = true
        
        informationTextView.isEditable = true
        
        locationInfoSaveButton.isEnabled = true
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    func OpenLocation() {
        
        var SelectLocationQuery = String()
        
        if (SharedItemOwner != "") {
            SelectLocationQuery = "SELECT * FROM locationtable WHERE ikey = \(LocationIkey) AND name = '\(LocationName)' AND userikeyreference = \(SharedItemOwner)"
        } else {
            SelectLocationQuery = "SELECT * FROM locationtable WHERE ikey = \(LocationIkey) AND name = '\(LocationName)' AND userikeyreference = \(CurrentUserIkey!)"
        }
        

        print("SelectLocationQuery: \n\"\(SelectLocationQuery)\"")
        

        var LocationNameDB = String()
        var LocationLocationDB = String()
        var LocationNotesDB = String()
        //var LocationImageDB = String()
        var LocationImageDB = String()

        do {

            let SelectLocationQueryRun = try db.executeQuery(SelectLocationQuery, values:nil)

            while SelectLocationQueryRun.next() {
                LocationNameDB = SelectLocationQueryRun.string(forColumn: "name")!
                LocationLocationDB = SelectLocationQueryRun.string(forColumn: "location")!
                LocationNotesDB = SelectLocationQueryRun.string(forColumn: "notes")!
                
                let LocationImageDB1 = SelectLocationQueryRun.int(forColumn: "imagetableikeyreference")
                
                if (LocationImageDB1 == 0) {
                    LocationImageDB = ""
                } else {
                    LocationImageDB = "\(LocationImageDB1)"
                }

            }

            print("1  LocationNameDB: \"\(LocationNameDB)\" LocationLocationDB: \"\(LocationLocationDB)\" LocationNotesDB: \"\(LocationNotesDB)\" LocationImageDB: \"\(LocationImageDB)\"")

            nameTextField.stringValue = LocationNameDB

            locationInfoTextField.stringValue = LocationLocationDB

            informationTextView.string = LocationNotesDB
            
            
            loggly(LogType.Info, text: "Location: \"\(LocationNameDB)\" Opened")
            
            ///////////////////
            
            if (LocationImageDB == "") {
                locationImageButton.image = NSImage(named: NSImage.Name(rawValue: "ChooseAnImage.png"))
            } else {
            
                let GetImageQuery = "SELECT PATHTOIMAGE from imagetable where ikey = \(LocationImageDB)"
                
                print("GetImageQuery: \"\(GetImageQuery)\"")
                
                var GetImagePathDB = String()
                
                let GetImageQueryRun = try db.executeQuery(GetImageQuery, values:nil)
                
                while GetImageQueryRun.next() {
                    GetImagePathDB = GetImageQueryRun.string(forColumn: "PATHTOIMAGE")!
                }
                
                print("GetImagePathDB: \"\(GetImagePathDB)\"")

                locationImageButton.image = NSImage(contentsOfFile: NSImage.Name(rawValue: GetImagePathDB).rawValue)
                
                //locationImageButton.image = NSImage(named: NSImage.Name(rawValue: GetImagePathDB))
            }
            
            

        } catch {

            let ErrorMessage = "LocationsInfoViewController:\(#function) - Failed to Open \"\(LocationName)\" Location\n\(db.lastErrorMessage())\n\(SelectLocationQuery))"

            print(ErrorMessage)

            loggly(LogType.Error, text: ErrorMessage)

            DoAlert.DisplayAlert(Class: "LocationsInfoViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to Open \"\(LocationName)\" Location\n\n\"\(SelectLocationQuery)\"\n", DatabaseError: "\(db.lastErrorMessage()))")

        }




    }
    

    

    
    @IBAction func locationImageButton(_ sender: Any) {
        
        print("locationImageButton Pressed")
        
        ////////////////////
        
        if (SharedItemLock == false) {
            
        } else if (SharedItemLock == true) {
            return
        }
        
        
        let group = DispatchGroup()
        group.enter()
        
        DispatchQueue.main.async {
            self.SaveButton(self)
            group.leave()
            
        }
        
        // does not wait. But the code in notify() gets run
        // after enter() and leave() calls are balanced
        
        group.notify(queue: .main) {
            print("LocationIkey: \"\(LocationIkey)\"")
        
        
        
        
        
        /////////////////////
        
        if let url = NSOpenPanel().selectUrl {
            self.locationImageButton.image = NSImage(contentsOf: url)
            print("file selected:", url.path)
            
            let ImagePath = url.path
            
            //////////////////
            
            let SelectLocationImageReferenceQuery = "Select * FROM locationtable WHERE ikey = \(LocationIkey)"
            var LocationImageReferenceDB = String()
            var LocationIkeyDB = String()
            
            do {
                
                let SelectLocationImageQueryRun = try db.executeQuery(SelectLocationImageReferenceQuery, values:nil)
                
                while SelectLocationImageQueryRun.next() {
                    let LocationImageReferenceDB1 = SelectLocationImageQueryRun.int(forColumn: "imagetableikeyreference")
                    
                    if (LocationImageReferenceDB1 == 0) {
                        LocationImageReferenceDB = ""
                    } else {
                        LocationImageReferenceDB = "\(LocationImageReferenceDB1)"
                    }
                    
                    
                    
                    LocationIkeyDB = SelectLocationImageQueryRun.string(forColumn: "ikey")!
                }
                
                print("LocationImageReferenceDB: \"\(LocationImageReferenceDB)\"")

            } catch {
                
                let ErrorMessage = "LocationsInfoViewController:\(#function) - Failed to get Images for \"\(LocationName)\" Location\n\(db.lastErrorMessage())\n\(SelectLocationImageReferenceQuery))"
                
                print(ErrorMessage)
                
                loggly(LogType.Error, text: ErrorMessage)
                
                DoAlert.DisplayAlert(Class: "LocationsInfoViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to get Images for \"\(LocationName)\" Location\n\n\(SelectLocationImageReferenceQuery)", DatabaseError: "\(db.lastErrorMessage())")
                
            }
            
            ///////////////////////////////////////////
            
            let ImageName = "\((ImagePath as NSString).lastPathComponent)"
            let FullPathImageCheck = "\(ImagesLocation)/\(ImageName)"
            
            
            
            if (LocationImageReferenceDB.count < 2) {
                // Location Didnt have an image associated with it
                // INSERT
                
                
                
                let InsertNewImageQuery = "INSERT INTO imagetable (ikey,userikeyreference,characterreference,locationtableikeyreference,pathtoimage) VALUES ((select MAX(ikey) + 1 from imagetable),\"\(CurrentUserIkey!)\",'0',\"\(LocationIkeyDB)\",\"\(FullPathImageCheck)\")"
                
                print("InsertNewImageQuery: \n\(InsertNewImageQuery)")
                
                
                let Success = db.executeStatements(InsertNewImageQuery)
                
                if (!Success) {
                    
                    let ErrorMessage = "Failed to INSERT Image \((ImagePath as NSString).lastPathComponent) for Location: \"\(LocationName)\" Location\n\n\(InsertNewImageQuery)\n\n\(db.lastErrorMessage())"
                    
                    loggly(LogType.Error, text: ErrorMessage)
                    
                    DoAlert.DisplayAlert(Class: "LocationsInfoViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed INSERT Image \((ImagePath as NSString).lastPathComponent) for Location: \"\(LocationName)\" Location\n\n\(InsertNewImageQuery)", DatabaseError: "\(db.lastErrorMessage())")
                    
                } else {
  
                    var UpdateLocationImageReference = String()
                    
                    if (SharedItemOwner != "") {
                        UpdateLocationImageReference = "UPDATE locationtable SET imagetableikeyreference = (select MAX(ikey) from imagetable) WHERE userikeyreference = \(SharedItemOwner) AND name = '\(LocationName)' "
                    } else {
                        UpdateLocationImageReference = "UPDATE locationtable SET imagetableikeyreference = (select MAX(ikey) from imagetable) WHERE userikeyreference = \(CurrentUserIkey!) AND name = '\(LocationName)' "
                    }
                    
                    

                    do {
                        
                        try db.executeUpdate(UpdateLocationImageReference, values: nil)
                        
                    } catch {
                        
                        let ErrorMessage = "Failed to INSERT Image reference for Location: \"\(LocationName)\" Location\n\n\(InsertNewImageQuery)\n\n\(db.lastErrorMessage())"
                        
                        loggly(LogType.Error, text: ErrorMessage)

                        DoAlert.DisplayAlert(Class: "LocationsInfoViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed INSERT Image reference for Location: \"\(LocationName)\" Location\n\n\(UpdateLocationImageReference)", DatabaseError: "\(db.lastErrorMessage())")
                        
                    }

     
                    
                    
                    
                    
                   
                }
                    

            } else {
                // Location DID have an image associated with it
                // UPDATE
                
                // UPDATE imagetable SET pathtoimage = \"%@\", locationtableikeyreference = \"%@\" WHERE userikeyreference =  %li AND ikey = %d",ImagePathForDB, userikeyreferenceIkeyDB ,CurrentUserIkey,LocationInImageDB
                
                var UpdateLocationImageReference = String()
                
                if (SharedItemOwner != "") {
                    UpdateLocationImageReference = "UPDATE locationtable SET imagetableikeyreference = (select MAX(ikey) from imagetable) WHERE userikeyreference = \(SharedItemOwner) AND name = '\(LocationName)' "
                } else {
                    UpdateLocationImageReference = "UPDATE locationtable SET imagetableikeyreference = (select MAX(ikey) from imagetable) WHERE userikeyreference = \(SharedItemOwner) AND name = '\(LocationName)' AND ikey = \"\(LocationIkeyDB)\""
                }
                
                
                
                do {
                    
                    try db.executeUpdate(UpdateLocationImageReference, values: nil)
                    
                } catch {
                    
                    let ErrorMessage = "Failed to UPDATE Image reference for Location: \"\(LocationName)\" Location\n\n\(UpdateLocationImageReference)\n\n\(db.lastErrorMessage())"
                    
                    loggly(LogType.Error, text: ErrorMessage)
                    
                    DoAlert.DisplayAlert(Class: "LocationsInfoViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to UPDATE Image reference for Location: \"\(LocationName)\" Location\n\n\(UpdateLocationImageReference)", DatabaseError: "\(db.lastErrorMessage())")
                    
                }
                
            }
            
            ///////////////////
            // Copy image to Images folder
            
            if FM.fileExists(atPath: FullPathImageCheck) {
                print("File exists")
            } else {
                print("File not found")
                do {
                    try FM.copyItem(atPath: ImagePath, toPath: FullPathImageCheck)
                }
                catch let error as NSError {
                    
                    let ErrorMessage = "Failed to copy image \((ImagePath as NSString).lastPathComponent) to Images folder\n\n\(error)"
                    
                    loggly(LogType.Error, text: ErrorMessage)
                    
                    DoAlert.DisplayAlert(Class: "LocationsInfoViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to copy image \((ImagePath as NSString).lastPathComponent) to Images folder \(FullPathImageCheck)", DatabaseError: "\(error)")
                }
            }
          
            
            
            
           //////////// end of if
            
        } else {
            print("file selection was canceled")
        }
        
        
        
        }// end of dispatch notify
        
        
    } // end of locationImageButton
    
    @IBAction func closeButton(_ sender: Any) {
        
        print("closeButton Pressed")
        
        if (SaveNeeded == true) {
            let alert = NSAlert()
            alert.messageText = "Do you want to Save your Locaiton?"
            alert.informativeText = "Any unsaved information will be lost."
            alert.alertStyle = .warning
            alert.addButton(withTitle: "Save")
            alert.addButton(withTitle: "Don't Save")
            alert.addButton(withTitle: "Cancel")
            
            if (CurrentAppearanceLocationsInfoViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.window.titlebarAppearsTransparent = true
            
            let returnCode = alert.runModal()
            
            if (returnCode.rawValue == 1000) {
                print("Save Pressed")
                SaveLocation()
                
                if (SharedItemOwner != "") {
                    
                } else {
                    if (StoryAssociationsWindow.window?.isVisible)! {
                        
                    } else {
                        LocationsWindow.showWindow(self)
                    }
                }
                
                
                LocVC.LocationTableData()
                
                
                self.view.window?.close()
                
            }
            if (returnCode.rawValue == 1001) {
                print("Don't Save Pressed")
                CleanUp()
                self.view.window?.close()
                
                if (SharedItemOwner != "") {
                    
                } else {
                    if (StoryAssociationsWindow.window?.isVisible)! {
                        
                    } else {
                        LocationsWindow.showWindow(self)
                    }
                }
                
                SaveNeeded = false
            }
            if (returnCode.rawValue == 1002) {
                print("Cancel Pressed")
                return
            }
            
            SharedItemOwner = ""
            SharedItemLock = false
            
        } else {
            
            CleanUp()
    
            if (SharedItemOwner != "") {
                
            } else {
                if (StoryAssociationsWindow.window?.isVisible)! {
                    
                } else {
                    LocationsWindow.showWindow(self)
                }
            }
            
            print("CleanUpCompleted 1 = \(CleanUpCompleted)")
            
            while (CleanUpCompleted == false) {
                
                if (CleanUpCompleted == true) {
                    print("CleanUpCompleted 2 = \(CleanUpCompleted)")
                    print("BREAK")
                    break
                }
            }

            self.view.window?.close()
            
            SharedItemOwner = ""
            SharedItemLock = false
            
        }// end of else
        
        
        OpenWindows.CurrentOpenWindows()
        
        if (CurrentlyOpenWindows.contains(LocationImagesWindow)) {
            LocationImagesWindow.window?.close()
        }
        
        
        
        OpenWindows.CheckForOpenWindows()
        
        
    }
    
    
    @IBAction func SaveButton(_ sender: Any) {
        
        print("SaveButton Pressed")
        
        SaveLocation()
        
        //SaveNeeded = true
        
        locationInfoSaveButton.styleButtonText(button: locationInfoSaveButton, buttonName: "Save", fontColor: .green , alignment: .center, font: AppFont, size: 13.0)
 

    }
    
    
    override func controlTextDidChange(_ obj: Notification) {
        // TextView
        
        
        
        if (self.nameTextField.stringValue.count < 1) {
            
        } else {
            
            if (self.nameTextField.stringValue.count == 0) {
                return
            }
            
            let lastCharacter = String(describing: self.nameTextField.stringValue.last!)

            let SymbolTest = CharacterSet.symbols.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
            
            let punctuationTest = CharacterSet.punctuationCharacters.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
            
            //            let SpaceNewLineTest = CharacterSet.whitespacesAndNewlines.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
            
            let NewLineTest = CharacterSet.newlines.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
            
            if (SymbolTest || punctuationTest || NewLineTest) {
                
                __NSBeep()
                
                self.nameTextField.stringValue = String(self.nameTextField.stringValue.dropLast())
                
                return
            }
            
        }
        
        /////////////////////////
        
        if (self.locationInfoTextField.stringValue.count < 1) {
            
        } else {
            
            if (self.locationInfoTextField.stringValue.count == 0) {
                return
            }
            
            let lastCharacter = String(describing: self.locationInfoTextField.stringValue.last!)
            
            let SymbolTest = CharacterSet.symbols.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
            
            let punctuationTest = CharacterSet.punctuationCharacters.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
            
            //            let SpaceNewLineTest = CharacterSet.whitespacesAndNewlines.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
            
            let NewLineTest = CharacterSet.newlines.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
            
            if (SymbolTest || punctuationTest || NewLineTest) {
                
                __NSBeep()
                
                self.locationInfoTextField.stringValue = String(self.locationInfoTextField.stringValue.dropLast())
                
                return
            }
            
        }
        
        /////////////////////////
        
        locationInfoSaveButton.styleButtonText(button: locationInfoSaveButton, buttonName: "Save", fontColor: .red , alignment: .center, font: AppFont, size: 13.0)
        
        SaveNeeded = true
    }
    
    func textDidChange(_ notification: Notification) {
        // Textfield
        
        /////////////////////////
        
        if (self.informationTextView.string.count < 1) {
            
        } else {
            
            if (self.informationTextView.string.count == 0) {
                return
            }
            
            let lastCharacter = String(describing: self.informationTextView.string.last!)
            
            let SymbolTest = CharacterSet.symbols.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
            
            let punctuationTest = CharacterSet.punctuationCharacters.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
            
            //            let SpaceNewLineTest = CharacterSet.whitespacesAndNewlines.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
            
            let NewLineTest = CharacterSet.newlines.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
            
            if (SymbolTest || punctuationTest || NewLineTest) {
                
                __NSBeep()
                
                self.informationTextView.string = String(self.informationTextView.string.dropLast())
                
                return
            }
            
        }
        
        /////////////////////////
        
        locationInfoSaveButton.styleButtonText(button: locationInfoSaveButton, buttonName: "Save", fontColor: .red , alignment: .center, font: AppFont, size: 13.0)
        
        SaveNeeded = true
    }
    
    
    func CleanUp() {
        
        
        nameTextField.stringValue = ""
        
        locationInfoTextField.stringValue = ""
        
        informationTextView.string = ""
        
        locationImageButton.image = NSImage(named: NSImage.Name(rawValue: "ChooseAnImage.png"))
        
        CleanUpCompleted = true
        
        LocationIkey = ""
        LocationName = ""
        
    }
    
    func SaveLocation() {
        
        print("Location Saved")
        
        ///////////////
        
        
        //  INSERT INTO locationtable (ikey,name,location,notes,associatedstoryikey,userikeyreference) VALUES ((select MAX(ikey)+ 1 from locationtable),\"%@\",\"%@\",\"%@\",\"%@\",%li)",NewLocationNameFiltered,LLocationAddress,LSummaryNotes,@" ",CurrentUserIkey
        
        var LocationNameCheck = String()
        
        if (nameTextField.stringValue == "") {
            
            let alert = NSAlert()
            alert.messageText = "This Location has No Name"
            alert.informativeText = "Please give this Location a Name."
            alert.alertStyle = .warning
            alert.addButton(withTitle: "Ok")

            if (CurrentAppearanceLocationsInfoViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.window.titlebarAppearsTransparent = true
            
            alert.runModal()
            
            return
            
        } else {
            LocationNameCheck = nameTextField.stringValue
        }
        
        
        var SaveORInsertStatement = String()
    
        
        if (LocationNewLocation == true) {
            
            
            
            SaveORInsertStatement = "INSERT INTO locationtable (ikey,name,location,notes,imagetableikeyreference,associatedstoryikey,userikeyreference) VALUES ((select MAX(ikey)+ 1 from locationtable),\"\(LocationNameCheck)\",\"\(locationInfoTextField.stringValue)\",\"\(informationTextView.string)\", '' , '' ,\(CurrentUserIkey!))"
            
            
            
            
            
            ///////////////////
            
//            do {
//                let UserQuery = try db.executeQuery("Select MAX(ikey) as num from locationtable", values:nil)
//                
//                while UserQuery.next() {
//                    let LocationIkeyDB = UserQuery.int(forColumn: "num")
//                    
//                    if (LocationIkeyDB == 0) {
//                        LocationIkey = "1"
//                    } else {
//                        //LocationIkey = "\"\(LocationIkeyDB)\""
//                    }
//                    
//                    
//                }
//                
//            } catch {
//                print("ERROR: \(db.lastErrorMessage())")
//                loggly(LogType.Error, text: "LocationInfoViewController-\(#function) Query Failed: \"Select MAX(ikey) as num from locationtable\"")
//                DoAlert.DisplayAlert(Class: "LocationInfoViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"Select MAX(ikey) as num from locationtable\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
//            }
            
            
            /////////////////////////
            
        } else {
            
            
//            do {
//                let UserQuery = try db.executeQuery("Select MAX(ikey) as num from locationtable", values:nil)
//
//                while UserQuery.next() {
//                    let LocationIkeyDB = UserQuery.int(forColumn: "num")
//
//                    if (LocationIkeyDB == 0) {
//                        LocationIkey = "1"
//                    } else {
//                        LocationIkey = "\"\(LocationIkeyDB)\""
//                    }
//
//
//                }
//
//            } catch {
//                print("ERROR: \(db.lastErrorMessage())")
//                loggly(LogType.Error, text: "LocationInfoViewController-\(#function) Query Failed: \"Select MAX(ikey) as num from locationtable\"")
//                DoAlert.DisplayAlert(Class: "LocationInfoViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"Select MAX(ikey) as num from locationtable\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
//            }
            
            
            /////////////////////////
            
            if (SharedItemOwner != "") {
                SaveORInsertStatement = "UPDATE locationtable set name = \"\(LocationNameCheck)\", location = \"\(locationInfoTextField.stringValue)\", notes = \"\(informationTextView.string)\" where ikey = \(LocationIkey) and userikeyreference = \(SharedItemOwner)"
            } else {
                SaveORInsertStatement = "UPDATE locationtable set name = \"\(LocationNameCheck)\", location = \"\(locationInfoTextField.stringValue)\", notes = \"\(informationTextView.string)\" where ikey = \(LocationIkey) and userikeyreference = \(CurrentUserIkey!)"
            }
            
            
            
        }
        
        print("SaveORInsertStatement: \(SaveORInsertStatement)")
        
        
        let Success = db.executeStatements(SaveORInsertStatement)
        
        if (!Success) {
            
            let ErrorMessage = "Failed to Save \"\(LocationName)\" Location\n\n\(SaveORInsertStatement)\n\n\(db.lastErrorMessage())"
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "LocationsInfoViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to Save \"\(LocationName)\" Location\n\n\(SaveORInsertStatement)", DatabaseError: "\(db.lastErrorMessage())")
            
        } else {
            
            if SaveORInsertStatement.range(of:"INSERT") != nil {
                print("Sucessfully Added New Location")
            }
            if SaveORInsertStatement.range(of:"UPDATE") != nil {
                print("Sucessfully Updated Existing Location")
            }
            
            SaveNeeded = false
            
            

        }
        
        
        //////////////////////////////////////////////////
        
        
        if (LocationIkey == "") {
            
            
            let SelectLocationImageReferenceQuery = "Select * FROM locationtable WHERE name = \"\(LocationNameCheck)\" AND location = \"\(locationInfoTextField.stringValue)\" AND notes = \"\(informationTextView.string)\" AND userikeyreference = \(CurrentUserIkey!)"
            
            print("SelectLocationImageReferenceQuery: \"\(SelectLocationImageReferenceQuery)\"")
            
            var LocationIkeyDB = Int()
            
            do {
                
                let SelectLocationImageQueryRun = try db.executeQuery(SelectLocationImageReferenceQuery, values:nil)
                
                while SelectLocationImageQueryRun.next() {
                    
                    LocationIkeyDB = Int(SelectLocationImageQueryRun.int(forColumn: "ikey"))
                    
                }
                
                print("LocationIkeyDB: \"\(LocationIkeyDB)\"")
                
            } catch {
                
                let ErrorMessage = "LocationsInfoViewController:\(#function) - Failed to get Images for \"\(LocationName)\" Location\n\(db.lastErrorMessage())\n\(SelectLocationImageReferenceQuery))"
                
                print(ErrorMessage)
                
                loggly(LogType.Error, text: ErrorMessage)
                
                DoAlert.DisplayAlert(Class: "LocationsInfoViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to get Images for \"\(LocationName)\" Location\n\n\(SelectLocationImageReferenceQuery)", DatabaseError: "\(db.lastErrorMessage())")
                
            }
            
    
            if (LocationIkeyDB != 0) {
                LocationIkey = String(LocationIkeyDB)
            }
            
            
            
           
            
            
            
        }// end of if (LocationIkey == "") {
        
        
        LocationNewLocation = false
        
        
        
        
        
        ///////////////
        
        //CleanUp()
        //self.view.window?.close()
    }
    
    
    
    func delayWithSeconds(_ seconds: Double, completion: @escaping () -> ()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            completion()
        }
    }
    
    
    
    
    
    
    @IBAction func OpenImagesWindowButton(_ sender: Any) {
        
        SaveButton(self)
        
        //SaveLocation()
        
        if (LocationIkey == "0" || LocationIkey == "") {
           
        } else {
        
//            delayWithSeconds(0.5) {
//                print("Waited and Running LocationImagesWindow.showWindow(nil)")
                LocationImagesWindow.showWindow(nil)
//            }
            
        }
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
