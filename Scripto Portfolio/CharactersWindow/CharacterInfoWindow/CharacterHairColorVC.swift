//
//  CharacterHairColorVC.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 3/6/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Cocoa
import SwiftLoggly

class CharacterHairColorVC: NSViewController,NSTextFieldDelegate {
    
    @IBOutlet weak var HairColorRadioButton: NSButton!
    @IBOutlet weak var HairHexTextField: NSTextField!
    @IBOutlet weak var HairRGBTextField: NSTextField!
    
    @IBOutlet weak var FacialColorRadioButton: NSButton!
    @IBOutlet weak var FacialHexTextField: NSTextField!
    @IBOutlet weak var FacialRGBTextField: NSTextField!
    
    @IBOutlet weak var SkinToneRadioButton: NSButton!
    @IBOutlet weak var SkinToneHexTextField: NSTextField!
    @IBOutlet weak var SkinToneRGBTextField: NSTextField!
    
    @IBOutlet weak var HexTextField: NSTextField!
    
    @IBOutlet weak var RTextField: NSTextField!
    @IBOutlet weak var GTextField: NSTextField!
    @IBOutlet weak var BTextField: NSTextField!
    
    @IBOutlet weak var ColorColorWell: NSColorWell!
    
    

    
    
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        
        self.RTextField.delegate = self
        self.GTextField.delegate = self
        self.BTextField.delegate = self
        
    }
    
    override func viewWillAppear() {
        

        
        NotificationCenter.default.addObserver(self, selector: #selector(SaveColors), name: NSPopover.didCloseNotification, object: nil)
        

    }
    
    override func viewDidAppear() {
        LoadColors()
    }
    
    

    
    
    
    
    
    override func controlTextDidChange(_ obj: Notification) {
        
        print("controlTextDidChange Ran")
        
        
        if (self.RTextField.stringValue.count < 1) {
            
        } else {
            
            print("Else Ran")
            
            let lastCharacter = String(describing: self.RTextField.stringValue.last!)
            
            //print("lastCharacter: \(lastCharacter)")
            
            let SymbolTest = CharacterSet.symbols.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
            
            let punctuationTest = CharacterSet.punctuationCharacters.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
            
            let letterTest = CharacterSet.letters.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
            
            let SpaceNewLineTest = CharacterSet.whitespacesAndNewlines.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
            
            if (SymbolTest || punctuationTest || letterTest || SpaceNewLineTest) {
                
                __NSBeep()
                
                self.RTextField.stringValue = String(self.RTextField.stringValue.dropLast())
                
                return
            }
            
            let Numbers = [0,1,2,3,4,5,6,7,8,9]
            
            if (!Numbers.contains(Int(lastCharacter)!)) {
                __NSBeep()
                
                self.RTextField.stringValue = String(self.RTextField.stringValue.dropLast())
            }
            
            if (self.RTextField.intValue > 255) {
                
                __NSBeep()
                
                self.RTextField.stringValue = String(self.RTextField.stringValue.dropLast())
                
            }

        }
        
        ////////////////////
        
        if (self.GTextField.stringValue.count < 1) {
            
        } else {
            
            print("Else Ran")
            
            let lastCharacter = String(describing: self.GTextField.stringValue.last!)
            
            //print("lastCharacter: \(lastCharacter)")
            
            let SymbolTest = CharacterSet.symbols.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
            
            let punctuationTest = CharacterSet.punctuationCharacters.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
            
            let letterTest = CharacterSet.letters.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
            
            let SpaceNewLineTest = CharacterSet.whitespacesAndNewlines.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
            
            if (SymbolTest || punctuationTest || letterTest || SpaceNewLineTest) {
                
                __NSBeep()
                
                self.GTextField.stringValue = String(self.GTextField.stringValue.dropLast())
                
                return
            }
            
            let Numbers = [0,1,2,3,4,5,6,7,8,9]
            
            if (!Numbers.contains(Int(lastCharacter)!)) {
                __NSBeep()
                
                self.GTextField.stringValue = String(self.GTextField.stringValue.dropLast())
            }
            
            if (self.GTextField.intValue > 255) {
                
                __NSBeep()
                
                self.GTextField.stringValue = String(self.GTextField.stringValue.dropLast())
                
            }
            
        }
        
        ////////////////////
        
        if (self.BTextField.stringValue.count < 1) {
            
        } else {
            
            print("Else Ran")
            
            let lastCharacter = String(describing: self.BTextField.stringValue.last!)
            
            //print("lastCharacter: \(lastCharacter)")
            
            let SymbolTest = CharacterSet.symbols.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
            
            let punctuationTest = CharacterSet.punctuationCharacters.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
            
            let letterTest = CharacterSet.letters.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
            
            let SpaceNewLineTest = CharacterSet.whitespacesAndNewlines.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
            
            if (SymbolTest || punctuationTest || letterTest || SpaceNewLineTest) {
                
                __NSBeep()
                
                self.BTextField.stringValue = String(self.BTextField.stringValue.dropLast())
                
                return
            }
            
            let Numbers = [0,1,2,3,4,5,6,7,8,9]
            
            if (!Numbers.contains(Int(lastCharacter)!)) {
                __NSBeep()
                
                self.BTextField.stringValue = String(self.BTextField.stringValue.dropLast())
            }
            
            if (self.BTextField.intValue > 255) {
                
                __NSBeep()
                
                self.BTextField.stringValue = String(self.BTextField.stringValue.dropLast())
                
            }
            
        }
        
        
    }// end of controltextdidchange
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    func LoadColors() {
        
        
        var HairColorDB = String()
        var FacialColorDB = String()
        var SkinToneColorDB = String()
        
        
        let Query = "select * from charactertable WHERE ikey = \"\(CVCIkey)\" AND userikeyreference = \(CurrentUserIkey!) AND firstname = \"\(CVCName)\""
        
        do {
            
            let CharacterQueryRun = try db.executeQuery(Query, values:nil)
            
            while CharacterQueryRun.next() {
                
                HairColorDB = CharacterQueryRun.string(forColumn: "haircolor")!
                FacialColorDB = CharacterQueryRun.string(forColumn: "facialcolor")!
                SkinToneColorDB = CharacterQueryRun.string(forColumn: "skintone")!
        

            }
            
        } catch {
            
            print("ERROR: CharacterHairColorVC: Failed to get Colors for Character -\"\(CVCName)\" \n\(Query)\n\(db.lastErrorMessage())")
            
            let ErrorMessage = "CharacterHairColorVC:\(#function) - Failed to get Colors for Character -\"\(CVCName)\n\n\(db.lastErrorMessage())\n\(Query))"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "CharacterHairColorVC", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to get Colors for Character -\"\(CVCName)\" \\n\n\(Query)", DatabaseError: "\(db.lastErrorMessage())")
            
        }
        
        
        HairHexTextField.stringValue = HairColorDB
        FacialHexTextField.stringValue = FacialColorDB
        SkinToneHexTextField.stringValue = SkinToneColorDB
        
        
        HairColorRadioButton.state = .on
        HexTextField.stringValue = HairColorDB
        HexTextFieldAction(self)
        HairColorRadioButton.state = .off
        
        
        
        FacialColorRadioButton.state = .on
        HexTextField.stringValue = FacialColorDB
        HexTextFieldAction(self)
        FacialColorRadioButton.state = .off
        
        
        SkinToneRadioButton.state = .on
        HexTextField.stringValue = SkinToneColorDB
        HexTextFieldAction(self)
        SkinToneRadioButton.state = .off
        
        HexTextField.stringValue = ""
        RTextField.stringValue = ""
        GTextField.stringValue = ""
        BTextField.stringValue = ""
        ColorColorWell.color = .blue
        
        
        //CharInfoVC.CharacterHairColorCWColorWell.color = ColorColorWell.color
        
    }
    
    
    
     @objc func SaveColors() {
        
        
        var UpdateChafacterColorSqlquery = String()
        
        do {
            
            
            
            UpdateChafacterColorSqlquery = String(format: "UPDATE charactertable set haircolor = \"\(HairHexTextField.stringValue)\", facialcolor = \"\(FacialHexTextField.stringValue)\", skintone = \"\(SkinToneHexTextField.stringValue)\" where ikey = \(CVCIkey) and userikeyreference = \(CurrentUserIkey!)")
            
            print("UpdateChafacterColorSqlquery: \(UpdateChafacterColorSqlquery)")
            
            
            
            try db.executeUpdate(UpdateChafacterColorSqlquery, values: nil)
            
        } catch let error as NSError {
            
            let ErrorMessage = "\(#function) - Failed to do query \(UpdateChafacterColorSqlquery)\nError: \(error.localizedDescription)"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "CharacterHairColorVC", Level: 3, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to update Character Colors\nPlease quit then try again.\n\n\(UpdateChafacterColorSqlquery)", DatabaseError: "\(db.lastErrorMessage())")
            
//            let alert = NSAlert()
//            alert.messageText = "Warning"
//            alert.informativeText = "Failed to add new user to the Scripto Portfolio database.\nPlease quit then try again."
//            alert.alertStyle = .warning
//            alert.addButton(withTitle: "OK")
//
//            if alert.runModal() == .alertFirstButtonReturn {
//                print("Ok Pressed")
//                return
//            }
//
//            return
            
        }
        
        /////////////////
        
        
        HairColorHVC = "\(HairHexTextField.stringValue)"
        FacialColorHVC = "\(FacialHexTextField.stringValue)"
        SkinColorHVC = "\(SkinToneHexTextField.stringValue)"
        
        //CharInfoVC.CharacterHairColorCWColorWell.color = ColorColorWell.color
        
       //CharInfoVC.UpdateColorOnCIVC(HairColor: "\(HairHexTextField.stringValue)", FacialColor: "\(FacialHexTextField.stringValue)", SkinTone: "\(SkinToneHexTextField.stringValue)")
        
        
        /////////////////
        
        print("Popover Closed")
        NotificationCenter.default.removeObserver(NSPopover.didCloseNotification)
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateColorWells"), object: nil)
        
        //////////////
        
        NSColorPanel.shared.orderOut(nil)
        
        
        
        
        
    }
    
    

    
    

    
    func UpdateColorFields() {
        
        if (HairColorRadioButton.state == .on) {
            HairHexTextField.stringValue = HexTextField.stringValue
            HairRGBTextField.stringValue = "\(RTextField.stringValue)-\(GTextField.stringValue)-\(BTextField.stringValue)"
        } else if (FacialColorRadioButton.state == .on) {
            FacialHexTextField.stringValue = HexTextField.stringValue
            FacialRGBTextField.stringValue = "\(RTextField.stringValue)-\(GTextField.stringValue)-\(BTextField.stringValue)"
        } else if (SkinToneRadioButton.state == .on) {
            SkinToneHexTextField.stringValue = HexTextField.stringValue
            SkinToneRGBTextField.stringValue = "\(RTextField.stringValue)-\(GTextField.stringValue)-\(BTextField.stringValue)"
        }
        
    }
    
    
    

    
    @IBAction func HairRadioButton(_ sender: Any) {
        
        if (HairColorRadioButton.state == .on) {
            
            HexTextField.isEnabled = true
            RTextField.isEnabled = true
            GTextField.isEnabled = true
            BTextField.isEnabled = true
            ColorColorWell.isEnabled = true
   
            
            FacialColorRadioButton.state = .off
            SkinToneRadioButton.state = .off
            
        }
        
    }
    
    
    @IBAction func FacialRadioButton(_ sender: Any) {
        
        if (FacialColorRadioButton.state == .on) {
            
            HexTextField.isEnabled = true
            RTextField.isEnabled = true
            GTextField.isEnabled = true
            BTextField.isEnabled = true
            ColorColorWell.isEnabled = true
            
            
            HairColorRadioButton.state = .off
            SkinToneRadioButton.state = .off
            
        }
        
    }
    
    
    
    @IBAction func SkinTonRadioButton(_ sender: Any) {
        
        if (SkinToneRadioButton.state == .on) {
            
            HexTextField.isEnabled = true
            RTextField.isEnabled = true
            GTextField.isEnabled = true
            BTextField.isEnabled = true
            ColorColorWell.isEnabled = true
            
            
            HairColorRadioButton.state = .off
            FacialColorRadioButton.state = .off
            
        }
        
    }
    
    
    ///////////
    
    @IBAction func HexTextFieldAction(_ sender: Any) {
        
        let HexColor = HexTextField.stringValue
        
        if (HexColor.count < 6) {
            
        } else {
            
            let color = NSColor().HexToColor(hexString: HexColor, alpha: 1.0)
            
            ColorColorWell.color = color
            
            ColorWellAction(self)
            
        }
    }
    
    
    
    
    @IBAction func BTextFieldAction(_ sender: Any) {
        
        if (RTextField.stringValue.count == 0) {
            RTextField.stringValue = "0"
        }
        if (GTextField.stringValue.count == 0) {
            GTextField.stringValue = "0"
        }
        if (BTextField.stringValue.count == 0) {
            BTextField.stringValue = "0"
        }
        
        /////////////////
        
        let RValue = CGFloat(RTextField.floatValue/255)
        let GValue = CGFloat(GTextField.floatValue/255)
        let BValue = CGFloat(BTextField.floatValue/255)
        
        print("RValue: \(RValue) GValue: \(GValue) BValue: \(BValue)")
        
        let RGBColor = NSColor(calibratedRed: RValue, green: GValue, blue: BValue, alpha: 1.0)
        
        ColorColorWell.color = RGBColor
        
        ColorWellAction(self)
        
    }
    
    
    
    @IBAction func ColorWellAction(_ sender: Any) {
        
        let ColorWellColor = ColorColorWell.color
        
        let Hex = getHexFromColor(color: ColorWellColor)
        
        print("Hex: \(Hex)")
        
        HexTextField.stringValue = Hex as String
        
            UpdateColorFields()
    }
    
    

    
    
    
    
    func getHexFromColor(color: NSColor) -> NSString {
        // Get the red, green, and blue components of the color
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        
        var rInt, gInt, bInt: Int
        var rHex, gHex, bHex: NSString
        
        var hexColor: NSString
        
        color.getRed(&r, green: &g, blue: &b, alpha: &a)
        
        print("R: \(r) G: \(g) B:\(b) A:\(a)")
        
        // Convert the components to numbers (unsigned decimal integer) between 0 and 255
        rInt = Int((r * 255.99999))
        gInt = Int((g * 255.99999))
        bInt = Int((b * 255.99999))
        
        RTextField.stringValue = "\(rInt)"
        GTextField.stringValue = "\(gInt)"
        BTextField.stringValue = "\(bInt)"
        
        
        // Convert the numbers to hex strings
        rHex = NSString(format:"%2X", rInt)
        gHex = NSString(format:"%2X", gInt)
        bHex = NSString(format:"%2X", bInt)
        
        if (rHex == " 0") {
            rHex = "00"
        }
        if (gHex == " 0") {
            gHex = "00"
        }
        if (bHex == " 0") {
            bHex = "00"
        }
        
        
        hexColor = "\(rHex)\(gHex)\(bHex)" as NSString
        print("HEX: \"\(rHex)\" \"\(gHex)\" \"\(bHex)\"")
        
        return hexColor
    }
    
    

    

    
    
    
    
    
    
    
    
    
    
    
    
    
    
} // End of File










extension NSColor{
    func HexToColor(hexString: String, alpha:CGFloat? = 1.0) -> NSColor {
        // Convert hex string to an integer
        let hexint = Int(self.intFromHexString(hexStr: hexString))
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        let alpha = alpha!
        // Create color object, specifying alpha as well
        let color = NSColor(red: red, green: green, blue: blue, alpha: alpha)
        return color
    }
    
    func intFromHexString(hexStr: String) -> UInt32 {
        var hexInt: UInt32 = 0
        // Create scanner
        let scanner: Scanner = Scanner(string: hexStr)
        // Tell scanner to skip the # character
        scanner.charactersToBeSkipped = NSCharacterSet(charactersIn: "#") as CharacterSet
        // Scan hex value
        scanner.scanHexInt32(&hexInt)
        return hexInt
    }
}


extension NSColor {
    
    // returns color instance from RGB values (0-255)
    static func fromRGB(red: Double, green: Double, blue: Double, alpha: Double = 100.0) -> NSColor {
        
        let rgbRed = CGFloat(red/255)
        let rgbGreen = CGFloat(green/255)
        let rgbBlue = CGFloat(blue/255)
        let rgbAlpha = CGFloat(alpha/100)
        
        let color = NSColor(red: rgbRed, green: rgbGreen, blue: rgbBlue, alpha: rgbAlpha)
        return color
    }
}
