//
//  CharacterViewController.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 2/14/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Cocoa
import SwiftLoggly
import ProgressKit





// https://crunchybagel.com/working-with-hex-colors-in-swift-3/

class CharacterInfoViewController: NSViewController,NSTextViewDelegate {
    
    @IBOutlet weak var CharacterSaveTopButton: NSButton!
    @IBOutlet weak var CharacterHairColorButton: NSButton!
    @IBOutlet weak var CharacterHairFacialStyleButton: NSButton!
    @IBOutlet weak var CharacterSaveAndCloseButton: NSButton!
    @IBOutlet weak var CharacterCloseButton: NSButton!
    @IBOutlet weak var CharacterOtherCharacterSaveButton: NSButton!
    
    @IBOutlet weak var progressIndicator: MaterialProgress!
    
    
    @IBOutlet var CharacterSummaryTextView: NSTextView!
    
    
    @IBOutlet weak var CharacterPictureButton: NSButton!
    
    @IBOutlet weak var CharacterPictureNameButton: NSButton!
    
    @IBOutlet weak var CharacterPictureTextField: NSTextField!
    @IBOutlet weak var CharacterFirstnameTextField: NSTextField!
    @IBOutlet weak var CharacterMiddleTextField: NSTextField!
    @IBOutlet weak var CharacterLastnameTextField: NSTextField!
    
    @IBOutlet weak var CharacterHeightOneTextField: NSTextField!
    @IBOutlet weak var CharacterHeightOneStepper: NSStepper!
    
    @IBOutlet weak var CharacterHeightTwoTextField: NSTextField!
    @IBOutlet weak var CharacterHeightTwoStepper: NSStepper!
    
    @IBOutlet weak var CharacterWeightTextField: NSTextField!
    @IBOutlet weak var CharacterWeightStepper: NSStepper!
    
    @IBOutlet weak var CharacterGenderPopupBUtton: NSPopUpButton!
    
    @IBOutlet weak var CharacterEthnicityPopUpButton: NSPopUpButton!
    @IBOutlet weak var CharacterEthnicityOtherTextField: NSTextField!
    @IBOutlet weak var CharacterBirthdateDatePicker: NSDatePicker!
    @IBOutlet weak var CharacterBirthPlaceTextField: NSTextField!
    @IBOutlet weak var CharacterOrientationTextField: NSTextField!
    
    @IBOutlet weak var CharacterHairColorCWColorWell: NSColorWell!
    @IBOutlet weak var CharacterFacialColorCWColorWell: NSColorWell!
    @IBOutlet weak var CharacterSkinToneCWColorWell: NSColorWell!
    
    @IBOutlet weak var CharacterHairCWTextField: NSTextField!
    @IBOutlet weak var CharacterFacialCWTextField: NSTextField!
    
    // Other Character Information
    
    @IBOutlet weak var CharacterOtherInformationPopUpButton: NSPopUpButton!
    
    @IBOutlet var CharacterOtherCharacterInfoTexView: NSTextView!
    
    
    var CharacterSummaryText = 0
    
    
    
    var EthnicityArray = [String]()

    var CharacterOtherVariable = String()
    
    var CharacterOtherFirstOpen = true
    
    var ImageName = String()
    
    var CurrentAppearanceCharacterInfoViewController = String()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.

        //print("CharacterInfoViewController: viewDidLoad()")
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(ColorWellsUpdate), name: NSNotification.Name(rawValue: "UpdateColorWells"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(UpdateHairInfo), name: NSNotification.Name(rawValue: "UpdateHairInfo"), object: nil)
        
        self.CharacterSummaryTextView.delegate = self
        self.CharacterOtherCharacterInfoTexView.delegate = self
    
    }
    
    override func viewWillAppear() {
        
        db.open()
        
        //////////////////////////
        
        
        CharacterWindowPrep()
        
        //UnLockCharacter()
        
//        CharacterEthnicityPopUpButton.isEnabled = true
//        CharacterGenderPopupBUtton.isEnabled = true
//
//        CharacterSaveAndCloseButton.isEnabled = true
//        CharacterPictureButton.isEnabled = true
//
//        CharacterSummaryTextView.isEditable = true //
//
//        CharacterPictureTextField.isEnabled = true
//        CharacterFirstnameTextField.isEnabled = true
//        CharacterMiddleTextField.isEnabled = true
//        CharacterLastnameTextField.isEnabled = true
//
//        CharacterHeightOneTextField.isEnabled = true
//        CharacterHeightTwoTextField.isEnabled = true
//        CharacterWeightTextField.isEnabled = true
//
//        CharacterHeightOneStepper.isEnabled = true
//        CharacterHeightTwoStepper.isEnabled = true
//        CharacterWeightStepper.isEnabled = true
//        CharacterBirthdateDatePicker.isEnabled = true
//
//        //CharacterEthnicityOtherTextField.isEditable = true
//
//        CharacterBirthPlaceTextField.isEnabled = true
//        CharacterOrientationTextField.isEnabled = true
//
//        CharacterHairCWTextField.isEnabled = true
//        CharacterFacialCWTextField.isEnabled = true
        
        //////////////////////////
        
        
        

//        print("CVCIkey: \(CVCIkey)")
//
//
//        if (CVCIkey != "") {
//
//            OpenCharacter()
//
//            if (SharedItemOwner != "") {
//
//                LockDownCharacter()
//                print("******* Locked Down Ran *******")
//
//            }
//
//            print("Opened Existing Character")
//
//        } else {
//            //CharacterPictureButton.isEnabled = false
//
//            CreateNewCharacter()
//
//        }
        
        
        
    }// end of view will appear
    
    
    
    override func viewDidAppear() {
        
        
        
        print("viewDidAppear() RAN!!")
        
        UnLockCharacter()
        
        print("CVCIkey: \(CVCIkey)")
        
        
        if (CVCIkey != "") {
            
            OpenCharacter()
            
            if (SharedItemLock == true) {

                LockDownCharacter()
                print("******* Locked Down Ran *******")

            }
            
            print("Opened Existing Character")
            
        } else {
            //CharacterPictureButton.isEnabled = false
            
            CreateNewCharacter()
            
        }
        
    }
    
    
    
    
    
    
    
    override func viewDidLayout() {
        
        //        enum InterfaceStyle : String {
        //            case Dark, Light
        //
        //            init() {
        //                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
        //                self = InterfaceStyle(rawValue: type)!
        //            }
        //        }
        //
        //        let currentStyle = InterfaceStyle()
        //
        //        //print("currentStyle: \"\(currentStyle)\"  -  CurrentAppearance: \"\(CurrentAppearance)\" ")
        //
        //        if ("\(currentStyle)" != CurrentAppearanceNewUserViewController) {
        //            appearanceCheck()
        //        }
        
        appearanceCheck()
        
        super.viewDidLayout()
        
        print("NewUserViewController - viewDidLayout finished")
        
        
    }// end of viewDidLayout()
    
    
    func appearanceCheck() {
        
        enum InterfaceStyle : String {
            case Dark, Light
            
            init() {
                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
                self = InterfaceStyle(rawValue: type)!
            }
        }
        
        let currentStyle = InterfaceStyle()
        
        if ("\(currentStyle)" != CurrentAppearanceCharacterInfoViewController) {
            
            if (currentStyle.rawValue == "Dark") {
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColorDarkMode
                
//                CharacterOtherCharacterInfoTexView.wantsLayer = true
//                CharacterOtherCharacterInfoTexView.backgroundColor = NSColor.gray
//                
//                CharacterSummaryTextView.wantsLayer = true
//                CharacterSummaryTextView.backgroundColor = NSColor.gray
                
                CharacterSaveTopButton.styleButtonText(button: CharacterSaveTopButton, buttonName: "Save", fontColor: .darkGray , alignment: .center, font: AppFont, size: 26.0)
                
                CharacterSaveTopButton.wantsLayer = true
                CharacterSaveTopButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                CharacterSaveTopButton.layer?.cornerRadius = 7
                
                CharacterHairColorButton.styleButtonText(button: CharacterHairColorButton, buttonName: "Hair Colors", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                CharacterHairColorButton.wantsLayer = true
                CharacterHairColorButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                CharacterHairColorButton.layer?.cornerRadius = 7
                
                CharacterHairFacialStyleButton.styleButtonText(button: CharacterHairFacialStyleButton, buttonName: "Hair & Facial Styles", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                CharacterHairFacialStyleButton.wantsLayer = true
                CharacterHairFacialStyleButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                CharacterHairFacialStyleButton.layer?.cornerRadius = 7
                
                CharacterSaveAndCloseButton.styleButtonText(button: CharacterSaveAndCloseButton, buttonName: "Close", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                CharacterSaveAndCloseButton.wantsLayer = true
                CharacterSaveAndCloseButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                CharacterSaveAndCloseButton.layer?.cornerRadius = 7
                
                CharacterCloseButton.styleButtonText(button: CharacterCloseButton, buttonName: "Cancel", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                CharacterCloseButton.wantsLayer = true
                CharacterCloseButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                CharacterCloseButton.layer?.cornerRadius = 7
                
                CharacterOtherCharacterSaveButton.styleButtonText(button: CharacterOtherCharacterSaveButton, buttonName: "Save", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                CharacterOtherCharacterSaveButton.wantsLayer = true
                CharacterOtherCharacterSaveButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                CharacterOtherCharacterSaveButton.layer?.cornerRadius = 7
                
                
                
            } else if (currentStyle.rawValue == "Light") {
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColor.cgColor
                
                CharacterOtherCharacterInfoTexView.wantsLayer = true
                CharacterOtherCharacterInfoTexView.backgroundColor = NSColor.white
                
                CharacterSummaryTextView.wantsLayer = true
                CharacterSummaryTextView.backgroundColor = NSColor.white
                
                CharacterSaveTopButton.styleButtonText(button: CharacterSaveTopButton, buttonName: "Save", fontColor: .white , alignment: .center, font: AppFont, size: 26.0)
                
                CharacterSaveTopButton.wantsLayer = true
                CharacterSaveTopButton.layer?.backgroundColor = NSColor.gray.cgColor
                CharacterSaveTopButton.layer?.cornerRadius = 7
                
                CharacterHairColorButton.styleButtonText(button: CharacterHairColorButton, buttonName: "Hair Colors", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                CharacterHairColorButton.wantsLayer = true
                CharacterHairColorButton.layer?.backgroundColor = NSColor.gray.cgColor
                CharacterHairColorButton.layer?.cornerRadius = 7
                
                CharacterHairFacialStyleButton.styleButtonText(button: CharacterHairFacialStyleButton, buttonName: "Hair & Facial Styles", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                CharacterHairFacialStyleButton.wantsLayer = true
                CharacterHairFacialStyleButton.layer?.backgroundColor = NSColor.gray.cgColor
                CharacterHairFacialStyleButton.layer?.cornerRadius = 7
                
                CharacterSaveAndCloseButton.styleButtonText(button: CharacterSaveAndCloseButton, buttonName: "Close", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                CharacterSaveAndCloseButton.wantsLayer = true
                CharacterSaveAndCloseButton.layer?.backgroundColor = NSColor.gray.cgColor
                CharacterSaveAndCloseButton.layer?.cornerRadius = 7
                
                CharacterCloseButton.styleButtonText(button: CharacterCloseButton, buttonName: "Cancel", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                CharacterCloseButton.wantsLayer = true
                CharacterCloseButton.layer?.backgroundColor = NSColor.gray.cgColor
                CharacterCloseButton.layer?.cornerRadius = 7
                
                CharacterOtherCharacterSaveButton.styleButtonText(button: CharacterOtherCharacterSaveButton, buttonName: "Save", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                CharacterOtherCharacterSaveButton.wantsLayer = true
                CharacterOtherCharacterSaveButton.layer?.backgroundColor = NSColor.gray.cgColor
                CharacterOtherCharacterSaveButton.layer?.cornerRadius = 7
                
            }
        }
        
        CurrentAppearanceCharacterInfoViewController = "\(currentStyle)"
        
        
    }//end of appearanceCheck()
    
    
    
    
    
    
    
    
    
    
    
    
    
    func ProgressIndicator() {
        if (progressIndicator.animate) {
            progressIndicator.animate = false
            progressIndicator.isHidden = true
        } else {
            progressIndicator.animate = true
            progressIndicator.isHidden = false
        }
        
    }
    
    
    
    
    
    func textDidEndEditing(_ notification: Notification) {
        
        ProgressIndicator()
        
        // TextViews

        let FocusedTextfield = notification.object
        let StringText = (FocusedTextfield as! NSTextView).string.prefix(10)

        if (StringText == (CharacterOtherCharacterInfoTexView.string.prefix(10))) {
            print("CharacterOtherCharacterInfoTexView Matched")
            
            CharacterOtherSaveButtonAction(self)
  
        }
        
        
        if (StringText == (CharacterSummaryTextView.string.prefix(10))) {
            
            print("CharacterSummaryTextView Matched")
        
        if (CharacterSummaryTextView.string.count > 0) {

            var UpdateCharacterSummarySqlquery = String()

            do {
                UpdateCharacterSummarySqlquery = String(format: "UPDATE charactertable set charactersummary = \"\(CharacterSummaryTextView.string)\" where ikey = \(CVCIkey) and userikeyreference = \(CurrentUserIkey!)")

                print("UpdateCharacterSummarySqlquery: \(UpdateCharacterSummarySqlquery)")

                try db.executeUpdate(UpdateCharacterSummarySqlquery, values: nil)

            } catch let error as NSError {

                let ErrorMessage = "\(#function) - Failed to do query \(UpdateCharacterSummarySqlquery)\nError: \(error.localizedDescription)"

                print(ErrorMessage)

                loggly(LogType.Error, text: ErrorMessage)

                DoAlert.DisplayAlert(Class: "CharacterInfoViewController", Level: 3, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to update Character Summary\nPlease copy it then reenter it again.\n\n\(UpdateCharacterSummarySqlquery)", DatabaseError: "\(db.lastErrorMessage())")

            }
        }
    
        }
        
        ProgressIndicator()
    }
    
    
    func textDidChange(_ notification: Notification) {
        

        
    }
    
    
    func LockDownCharacter() {

        print("LockDownCharacter() RAN")
        
        CharacterHairColorButton.isEnabled = false
        CharacterHairFacialStyleButton.isEnabled = false
        
        CharacterEthnicityPopUpButton.isEnabled = false
        CharacterGenderPopupBUtton.isEnabled = false
        
        CharacterSaveAndCloseButton.isEnabled = false
        
        //CharacterPictureButton.isEditable = false
        
        CharacterSummaryTextView.isEditable = false
        
        CharacterPictureTextField.isEnabled = false
        CharacterFirstnameTextField.isEnabled = false
        CharacterMiddleTextField.isEnabled = false
        CharacterLastnameTextField.isEnabled = false
        
        CharacterHeightOneTextField.isEnabled = false
        CharacterHeightTwoTextField.isEnabled = false
        CharacterWeightTextField.isEnabled = false
        
        CharacterHeightOneStepper.isEnabled = false
        CharacterHeightTwoStepper.isEnabled = false
        CharacterWeightStepper.isEnabled = false
        CharacterBirthdateDatePicker.isEnabled = false

        //CharacterEthnicityOtherTextField.isEnabled = false
        
        CharacterBirthPlaceTextField.isEnabled = false
        CharacterOrientationTextField.isEnabled = false
        
        CharacterHairCWTextField.isEnabled = false
        CharacterFacialCWTextField.isEnabled = false

    }
    
    func UnLockCharacter() {
        
        print("UnLockCharacter() RAN")
        
        CharacterHairColorButton.isEnabled = true
        CharacterHairFacialStyleButton.isEnabled = true
        
        CharacterEthnicityPopUpButton.isEnabled = true
        CharacterGenderPopupBUtton.isEnabled = true
        
        CharacterSaveAndCloseButton.isEnabled = true
        //CharacterPictureButton.isEnabled = true
        
        CharacterSummaryTextView.isEditable = true
        
        CharacterPictureTextField.isEnabled = true
        CharacterFirstnameTextField.isEnabled = true
        CharacterMiddleTextField.isEnabled = true
        CharacterLastnameTextField.isEnabled = true
        
        CharacterHeightOneTextField.isEnabled = true
        CharacterHeightTwoTextField.isEnabled = true
        CharacterWeightTextField.isEnabled = true
        
        CharacterHeightOneStepper.isEnabled = true
        CharacterHeightTwoStepper.isEnabled = true
        CharacterWeightStepper.isEnabled = true
        CharacterBirthdateDatePicker.isEnabled = true
        
        //CharacterEthnicityOtherTextField.isEnabled = true
        
        CharacterBirthPlaceTextField.isEnabled = true
        CharacterOrientationTextField.isEnabled = true
        
        CharacterHairCWTextField.isEnabled = true
        CharacterFacialCWTextField.isEnabled = true
        
        
        
    }
    
    
    
    
    
    func CharacterWindowPrep() {
        
        UnLockCharacter()
        
        CharacterGenderPopupBUtton.removeAllItems()
        let GenderArray = ["Gender","Male","Female"]
        CharacterGenderPopupBUtton.addItems(withTitles: GenderArray)
        
        CharacterEthnicityPopUpButton.removeAllItems()
        EthnicityArray = ["Ethnicity", "Caucasion/White", "African/Black", "Hispanic/Latino","Asian","Middle Eastern","Pacific Islander","Other"]
        CharacterEthnicityPopUpButton.addItems(withTitles: EthnicityArray)
        
        /////////////////////
        
        self.CharacterHeightOneStepper.maxValue = 99999
        self.CharacterHeightOneStepper.minValue = 0
        self.CharacterHeightOneStepper.increment = 1
        self.CharacterHeightOneStepper.autorepeat = true
        
        
        self.CharacterHeightTwoStepper.maxValue = 99999
        self.CharacterHeightTwoStepper.minValue = 0
        self.CharacterHeightTwoStepper.increment = 1
        self.CharacterHeightTwoStepper.autorepeat = true
        
        self.CharacterWeightStepper.maxValue = 99999
        self.CharacterWeightStepper.minValue = 0
        self.CharacterWeightStepper.increment = 1
        self.CharacterWeightStepper.autorepeat = true
        
//        CharacterPictureTextField.toolTip = CharacterPictureTextField.stringValue
        
        
        let OtherInfoPopUpButtonArray = ["Choose an Option","Aliases/Nicknames","Hopes/Dreams","Fears","Prejudices","Religion","Likes/Dislikes","Background","Traits","Skills","Family/Relatives","Relationships","Work History","Favorite Foods","Allergies","Languages","Fashion","Surgeries","Motivations"]
        
        CharacterOtherInformationPopUpButton.removeAllItems()
        CharacterOtherInformationPopUpButton.addItems(withTitles: OtherInfoPopUpButtonArray)
        
        
        
        
    }
    
    
    func OpenCharacter() {
        
        ProgressIndicator()

        var FNameDB: String!
        var MNameDB: String!
        var LNameDB: String!
        var BirthdateF: Date!
        var HeightFeetDB: String!
        var HeightInchDB: String!
        var WeightDB: String!
        //var GenderDB: String!
        var GenderDB: Int32!
        var EthnicityDB: String!
        var BirthPlaceDB: String!
        var OrientationDB: String!
        var HairColorDB: String!
        var FacialColorDB: String!
        var SkinToneDB: String!
        var CharacterSummaryDB: String!

        
        //////////////////////////
        
        var Query = String()
        
        if (SharedItemOwner != "") {
            print("Open Character - SharedItemOwner = \"\(SharedItemOwner)\"")
            Query = "select * from charactertable WHERE ikey = \"\(CVCIkey)\" AND userikeyreference = \(SharedItemOwner) AND firstname = \"\(CVCName)\""
            
        } else {
            print("Open Character - Normal")
            Query = "select * from charactertable WHERE ikey = \"\(CVCIkey)\" AND userikeyreference = \(CurrentUserIkey!) AND firstname = \"\(CVCName)\""

        }
        
        print("Open Character Query: \(Query)")
        
        

        do {
            
            let CharacterQueryRun = try db.executeQuery(Query, values:nil)
            
            while CharacterQueryRun.next() {
                
                FNameDB = CharacterQueryRun.string(forColumn: "firstname")
                MNameDB = CharacterQueryRun.string(forColumn: "middlename")
                LNameDB = CharacterQueryRun.string(forColumn: "lastname")
                
                self.view.window?.title = "\(FNameDB!) \(LNameDB!)"
                
                // Age Calculation
                let dateFormatterGet = DateFormatter()
                dateFormatterGet.dateFormat = "YYYY-MM-DD HH:MM:SS.SSS"
                var BirthdateFDB = CharacterQueryRun.string(forColumn: "birthdate")

                if (BirthdateFDB == "") {
                    let Today = Date()
                    BirthdateFDB = dateFormatterGet.string(from: Today)
                } else {
                    BirthdateFDB = CharacterQueryRun.string(forColumn: "birthdate")
                }
                
                BirthdateF = dateFormatterGet.date(from: BirthdateFDB!)
                //print("BirthdateF: \"\(BirthdateF!)\"")
                
                if (BirthdateF == nil) {
                    
                    let dateFormatterGet2 = DateFormatter()
                    dateFormatterGet2.dateFormat = "MMMM d yyyy"
                    BirthdateF = dateFormatterGet2.date(from: BirthdateFDB!) as Date?
                    //print("Bdate was NIL: \"\(Bdate)\"")
                    
                }
                
                
                HeightFeetDB = CharacterQueryRun.string(forColumn: "heightfeet")
                HeightInchDB = CharacterQueryRun.string(forColumn: "heightinches")
                WeightDB = CharacterQueryRun.string(forColumn: "weight")
                //GenderDB = CharacterQueryRun.string(forColumn: "gender")
                GenderDB = CharacterQueryRun.int(forColumn: "gender")
                EthnicityDB = CharacterQueryRun.string(forColumn: "ethnicity")
                BirthPlaceDB = CharacterQueryRun.string(forColumn: "birthplace")
                OrientationDB = CharacterQueryRun.string(forColumn: "orientation")
                
                HairColorDB = CharacterQueryRun.string(forColumn: "haircolor")
                FacialColorDB = CharacterQueryRun.string(forColumn: "facialcolor")
                SkinToneDB = CharacterQueryRun.string(forColumn: "skintone")
                
                CharacterSummaryDB = CharacterQueryRun.string(forColumn: "charactersummary")
                //HairCutDB = CharacterQueryRun.string(forColumn: "haircut")
                //HairPartDB = CharacterQueryRun.string(forColumn: "hairpart")
                //HairLengthDB = CharacterQueryRun.string(forColumn: "hairlength")
                //FacialHairDB = CharacterQueryRun.string(forColumn: "facialhair")
                //FacialHairOtherDB = CharacterQueryRun.string(forColumn: "facialhairother")
           
                //////////////////
                
                CharacterFirstnameTextField.stringValue = FNameDB
                CharacterMiddleTextField.stringValue = MNameDB
                CharacterLastnameTextField.stringValue = LNameDB
                
                CharacterBirthdateDatePicker.dateValue = BirthdateF
                
                CharacterHeightOneTextField.stringValue = HeightFeetDB
                CharacterHeightOneStepper.intValue = Int32(HeightFeetDB)!
                CharacterHeightTwoTextField.stringValue = HeightInchDB
                CharacterHeightTwoStepper.intValue = Int32(HeightInchDB)!
                CharacterWeightTextField.stringValue = WeightDB
                CharacterWeightStepper.intValue = Int32(WeightDB)!
                
                var GenderToUI = String()
                
                if (GenderDB == 1) {
                    GenderToUI = "Gender"
                } else if (GenderDB == 2) {
                    GenderToUI = "Male"
                } else if (GenderDB == 3) {
                    GenderToUI = "Female"
                }
                

                
                
                CharacterGenderPopupBUtton.selectItem(withTitle:GenderToUI)
                
                if (EthnicityArray.contains(EthnicityDB)) {
                    CharacterEthnicityPopUpButton.selectItem(withTitle: EthnicityDB)
                } else {
                    CharacterEthnicityPopUpButton.selectItem(withTitle: "Other")
                    CharacterEthnicityOtherTextField.isEnabled = true
                    CharacterEthnicityOtherTextField.isHidden = false
                    CharacterEthnicityOtherTextField.stringValue = EthnicityDB
                }
                
                
                
                CharacterBirthPlaceTextField.stringValue = BirthPlaceDB
                CharacterOrientationTextField.stringValue = OrientationDB
                
                
                let HairColor = NSColor().HexToColorCIVC(hexString: HairColorDB, alpha: 1.0)
                CharacterHairColorCWColorWell.color = HairColor
                
                let FacialColor = NSColor().HexToColorCIVC(hexString: FacialColorDB, alpha: 1.0)
                CharacterFacialColorCWColorWell.color = FacialColor
                
                let SkinToneColor = NSColor().HexToColorCIVC(hexString: SkinToneDB, alpha: 1.0)
                CharacterSkinToneCWColorWell.color = SkinToneColor
                
                
                CharacterSummaryTextView.string = CharacterSummaryDB
                
                CharacterSummaryText = CharacterSummaryTextView.string.count
                
                loggly(LogType.Info, text: "Character: \"\(self.view.window?.title ?? "\(FNameDB ?? "BLANK") \(LNameDB ?? "BLANK")")\" Opened")

                
            }
            
        } catch {
            
            print("ERROR: CharacterInfoViewController: Failed to get Character -\"\(FNameDB ?? "BLANK") \(MNameDB ?? "BLANK") \(LNameDB ?? "BLANK")\" \n\(Query)\n\(db.lastErrorMessage())")
            
            let ErrorMessage = "CharacterInfoViewController:\(#function) - Failed to get Character -\"\(FNameDB ?? "BLANK") \(MNameDB ?? "BLANK") \(LNameDB ?? "BLANK")\n\n\(db.lastErrorMessage())\n\(Query))"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "CharacterInfoViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to get Character -\"\(FNameDB ?? "BLANK") \(MNameDB ?? "BLANK") \(LNameDB ?? "BLANK")\\n\n\(Query)", DatabaseError: "\(db.lastErrorMessage())")
            
        }
        
        
        ///////////////////////////////
        // Load Character Picture
        
        var SelectLocationImageReferenceQuery = String()
        
        if (SharedItemOwner != "") {
            SelectLocationImageReferenceQuery = "Select count(pathtoimage) as cnt FROM imagetable WHERE characterreference = \(CVCIkey) and userikeyreference = \(SharedItemOwner)"
        } else {
            SelectLocationImageReferenceQuery = "Select count(pathtoimage) as cnt FROM imagetable WHERE characterreference = \(CVCIkey) and userikeyreference = \(CurrentUserIkey!)"
        }
        
        print("SelectLocationImageReferenceQuery: \(SelectLocationImageReferenceQuery)")
        

        
        var PathToImageReferenceDB = Int32()
        
        
        do {
            
            let PathToImageQueryRun = try db.executeQuery(SelectLocationImageReferenceQuery, values:nil)
            
            while PathToImageQueryRun.next() {
                PathToImageReferenceDB = PathToImageQueryRun.int(forColumn: "cnt")
            }
            
            
        } catch {
            
            let ErrorMessage = "CharacterInfoViewController:\(#function) - Failed to get Images for \"\(LocationName)\" Location\n\(db.lastErrorMessage())\n\(SelectLocationImageReferenceQuery))"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "CharacterInfoViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to count Images for \(CharacterFirstnameTextField.stringValue) \(CharacterLastnameTextField.stringValue)\n\n\(SelectLocationImageReferenceQuery)", DatabaseError: "\(db.lastErrorMessage())")
            
        }
        
        //////////////////
        
        
        if (PathToImageReferenceDB == 0) {
            
            let BundlePath = Bundle.main.path(forResource: "avatar", ofType: "jpg")
            
            CharacterPictureButton.image = NSImage(contentsOfFile: BundlePath!)
            
            //CharacterPictureTextField.stringValue = "avatar.jpg"
            
            ImageName = "avatar.jpg"
            
            
        } else {
            
            var SelectLocationImageReferenceQuery = String()
            
            if (SharedItemOwner != "") {
                SelectLocationImageReferenceQuery = "Select pathtoimage FROM imagetable WHERE characterreference = \(CVCIkey) and userikeyreference = \(SharedItemOwner)"
            } else {
                SelectLocationImageReferenceQuery = "Select pathtoimage FROM imagetable WHERE characterreference = \(CVCIkey) and userikeyreference = \(CurrentUserIkey!)"
            }
            
            
            
            print("SelectLocationImageReferenceQuery: \(SelectLocationImageReferenceQuery)")
            
            
            
            var PathToImageReferenceDB = String()
            
            
            do {
                
                let PathToImageQueryRun = try db.executeQuery(SelectLocationImageReferenceQuery, values:nil)
                
                while PathToImageQueryRun.next() {
                    PathToImageReferenceDB = PathToImageQueryRun.string(forColumn: "pathtoimage")!
                }
                
                
                
            } catch {
                
                let ErrorMessage = "CharacterInfoViewController:\(#function) - Failed to get Images for \"\(LocationName)\" Location\n\(db.lastErrorMessage())\n\(SelectLocationImageReferenceQuery))"
                
                print(ErrorMessage)
                
                loggly(LogType.Error, text: ErrorMessage)
                
                DoAlert.DisplayAlert(Class: "CharacterInfoViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to get Image path for \(CharacterFirstnameTextField.stringValue) \(CharacterLastnameTextField.stringValue)\n\n\(SelectLocationImageReferenceQuery)", DatabaseError: "\(db.lastErrorMessage())")
                
            }
            
            let ImageNamed = "\((PathToImageReferenceDB as NSString).lastPathComponent)"
            
            print("ImageNamed: \"\(ImageNamed)\"")

            ImageName = ImageNamed

            CharacterPictureButton.image = NSImage(contentsOfFile: NSImage.Name(rawValue: PathToImageReferenceDB).rawValue)

            
        }
        
        
        
        /////////

        
        
        // pulls the Hair and Facial hair from DB to load in CW
        
        UpdateHairInfo()
        
        
        //////////////////
        
        
        
        
        
        
        
        ProgressIndicator()
        
    }
    
    @IBAction func CharacterSaveButton(_ sender: Any) {
        
        let CharacterHairPopover = NSPopover()
        CharacterHairPopover.contentViewController = NSStoryboard(name: NSStoryboard.Name(rawValue: "CharacterInfoWindow"), bundle: nil).instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "SaveButtonPrompt")) as? NSViewController
        CharacterHairPopover.animates = true
        CharacterHairPopover.behavior = NSPopover.Behavior.transient
        CharacterHairPopover.show(relativeTo: CharacterSaveTopButton.visibleRect, of: CharacterSaveTopButton, preferredEdge: NSRectEdge.minY)
        
    }
    
    
    
    
    @IBAction func CharacterHairColorButton(_ sender: Any) {
        
//        let popover = NSPopover()
//        popover.contentViewController = NSStoryboard(name: NSStoryboard.Name(rawValue: ""), bundle: nil).instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "")) as? NSViewController
//        popover.animates = true
//        popover.behavior = NSPopover.Behavior.transient
//        popover.show(relativeTo: , of: , preferredEdge: NSRectEdge.minY)
        
        //print("CharacterHairColorButton: Pressed")
        

        let CharacterHairPopover = NSPopover()
        CharacterHairPopover.contentViewController = NSStoryboard(name: NSStoryboard.Name(rawValue: "CharacterInfoWindow"), bundle: nil).instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "CharacterHairPop")) as? NSViewController
        CharacterHairPopover.animates = true
        CharacterHairPopover.behavior = NSPopover.Behavior.transient
        CharacterHairPopover.show(relativeTo: CharacterHairColorButton.visibleRect, of: CharacterHairColorButton, preferredEdge: NSRectEdge.minY)
        
        
        //print("CharacterHairColorButton: Ran")
        
    }
    
    @IBAction func CharacterHairFacialStyleButton(_ sender: Any) {
        
        
        
        
        
        let CharacterHairStylePopover = NSPopover()
        CharacterHairStylePopover.contentViewController = NSStoryboard(name: NSStoryboard.Name(rawValue: "CharacterInfoWindow"), bundle: nil).instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "CharacterHairStylePop")) as? NSViewController
        CharacterHairStylePopover.animates = true
        CharacterHairStylePopover.behavior = NSPopover.Behavior.transient
        CharacterHairStylePopover.show(relativeTo: CharacterHairFacialStyleButton.visibleRect, of: CharacterHairFacialStyleButton, preferredEdge: NSRectEdge.minY)
        
        
    }
    
//    func CharacterHairStyleSetup() {
//
////        hairstyle = @[@"Bald", @"Military Cut", @"Military Cut - Flat Top",@"Buzz Cut", @"Bowl Cut", @"Crew Cut", @"Mullet", @"Shag", @"Afro", @"Business Cut", @"Ponytail"];
////
////        NSComparator sort = ^(id obj1, id obj2)
////        {
////            return [obj1 caseInsensitiveCompare: obj2];
////        };
////
////        [self.StoryCharacterHairCutComboBox removeAllItems];
////        [self.StoryCharacterHairCutComboBox addItemsWithObjectValues: [hairstyle sortedArrayUsingComparator: sort]];
////        [self.StoryCharacterHairCutComboBox insertItemWithObjectValue:@"Other" atIndex:11];
////        [self.StoryCharacterHairCutComboBox insertItemWithObjectValue:@"Cancel" atIndex:12];
////        [self.StoryCharacterHairLengthComboBox insertItemWithObjectValue:@"Choose One" atIndex:0];
////        [self.StoryCharacterHairCutComboBox setNumberOfVisibleItems:[self.StoryCharacterHairCutComboBox numberOfItems]];
//
//        let hairstyle = ["Bald","Military Cut","Military Cut - Flat Top","Buzz Cut","Bowl Cut","Crew Cut","Mullet","Shag","Afro","Business Cut","Ponytail"]
//
//        // longest to shortest
////        var hairstyleSortedArray = hairstyle.sorted {
////            (obj1, obj2) -> Bool in
////            return obj1.count > obj2.count
////        }
//        //print("hairstyleSortedArray: \(hairstyleSortedArray)")
//
//        let hairstyleSortedArray = hairstyle.sorted {
//            $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending
//        }
//        print("hairstyleSortedArray: \(hairstyleSortedArray)")
//
//
//
//    }
    
    

    
    
    

    @objc func ColorWellsUpdate() {
        
        print("ColorWellsUpdate RAN")
        
        print("HairColorHVC: \"\(HairColorHVC)\" FacialColorHVC: \"\(FacialColorHVC)\" SkinColorHVC: \"\(SkinColorHVC)\"")
        
        let HairColorColor = NSColor().HexToColorCIVC(hexString: HairColorHVC, alpha: 1.0)
        CharacterHairColorCWColorWell.color = HairColorColor
        
        let FacialColorColor = NSColor().HexToColorCIVC(hexString: FacialColorHVC, alpha: 1.0)
        CharacterFacialColorCWColorWell.color = FacialColorColor

        let SkinToneColorColor = NSColor().HexToColorCIVC(hexString: SkinColorHVC, alpha: 1.0)
        CharacterSkinToneCWColorWell.color = SkinToneColorColor
        
        
    }
    
    ///////////////////////////
    // First name

    @IBAction func CharacterFirstNameAction(_ sender: Any) {
        
        ProgressIndicator()
        
        var UpdateCharacterFirstNameSqlquery = String()
        
        do {
            
            
            
            UpdateCharacterFirstNameSqlquery = String(format: "UPDATE charactertable set firstname = \"\(CharacterFirstnameTextField.stringValue)\" where ikey = \(CVCIkey) and userikeyreference = \(CurrentUserIkey!)")
            
            print("UpdateCharacterFirstNameSqlquery: \(UpdateCharacterFirstNameSqlquery)")
            
            
            
            try db.executeUpdate(UpdateCharacterFirstNameSqlquery, values: nil)
            
        } catch let error as NSError {
            
            let ErrorMessage = "\(#function) - Failed to do query \(UpdateCharacterFirstNameSqlquery)\nError: \(error.localizedDescription)"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "CharacterInfoViewController", Level: 3, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to update Character First Name\nPlease copy it then reenter it again.\n\n\(UpdateCharacterFirstNameSqlquery)", DatabaseError: "\(db.lastErrorMessage())")
            
            
        }
        
        ProgressIndicator()
        
    }
    
    ///////////////////////////
    // Middle name
    
    @IBAction func CharacterMiddleNameAction(_ sender: Any) {
        
        ProgressIndicator()
        
        var UpdateCharacterMiddleNameSqlquery = String()
        
        do {
            
            
            
            UpdateCharacterMiddleNameSqlquery = String(format: "UPDATE charactertable set middlename = \"\(CharacterMiddleTextField.stringValue)\" where ikey = \(CVCIkey) and firstname = \"\(CharacterFirstnameTextField.stringValue)\" and userikeyreference = \(CurrentUserIkey!)")
            
            print("UpdateCharacterMiddleNameSqlquery: \(UpdateCharacterMiddleNameSqlquery)")
            
            
            
            try db.executeUpdate(UpdateCharacterMiddleNameSqlquery, values: nil)
            
        } catch let error as NSError {
            
            let ErrorMessage = "\(#function) - Failed to do query \(UpdateCharacterMiddleNameSqlquery)\nError: \(error.localizedDescription)"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "CharacterInfoViewController", Level: 3, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to update Character Middle Name\nPlease copy it then reenter it again.\n\n\(UpdateCharacterMiddleNameSqlquery)", DatabaseError: "\(db.lastErrorMessage())")
            
            
        }
        
        ProgressIndicator()
    }
    
    ///////////////////////////
    // Last name
    
    @IBAction func CharacterLastNameAction(_ sender: Any) {
        ProgressIndicator()
        var UpdateCharacterLastNameSqlquery = String()
        
        do {

            UpdateCharacterLastNameSqlquery = String(format: "UPDATE charactertable set lastname = \"\(CharacterLastnameTextField.stringValue)\" where ikey = \(CVCIkey) and firstname = \"\(CharacterFirstnameTextField.stringValue)\" and userikeyreference = \(CurrentUserIkey!)")
            
            print("UpdateCharacterLastNameSqlquery: \(UpdateCharacterLastNameSqlquery)")
            
            try db.executeUpdate(UpdateCharacterLastNameSqlquery, values: nil)
            
        } catch let error as NSError {
            
            let ErrorMessage = "\(#function) - Failed to do query \(UpdateCharacterLastNameSqlquery)\nError: \(error.localizedDescription)"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "CharacterInfoViewController", Level: 3, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to update Character Last Name\nPlease copy it then reenter it again.\n\n\(UpdateCharacterLastNameSqlquery)", DatabaseError: "\(db.lastErrorMessage())")
            
        }
        
        ProgressIndicator()
    }
    
    
    
    
    @IBAction func CharacterHeightOneStepperAction(_ sender: Any) {
        ProgressIndicator()
        CharacterHeightOneTextField.stringValue = String(CharacterHeightOneStepper.intValue)
        CharacterHeightOneTextFieldAction(self)
        ProgressIndicator()
    }
    
    @IBAction func CharacterHeightOneTextFieldAction(_ sender: Any) {
        ProgressIndicator()
        CharacterHeightOneStepper.intValue = Int32(CharacterHeightOneTextField.intValue)
        SaveHieghtOneInfo()
        ProgressIndicator()
    }
    
    
    @IBAction func CharacterHeightTwoStepperAction(_ sender: Any) {
        ProgressIndicator()
        CharacterHeightTwoTextField.stringValue = String(CharacterHeightTwoStepper.intValue)
        CharacterHeightTwoTextFieldAction(self)
        ProgressIndicator()
    }
    
    @IBAction func CharacterHeightTwoTextFieldAction(_ sender: Any) {
        ProgressIndicator()
        CharacterHeightTwoStepper.intValue = Int32(CharacterHeightTwoTextField.intValue)
        SaveHieghtTwoInfo()
        ProgressIndicator()
    }
    
    func SaveHieghtOneInfo() {
        ProgressIndicator()
        var UpdateCharacterHeightFeetSqlquery = String()
        
        do {
            
            UpdateCharacterHeightFeetSqlquery = String(format: "UPDATE charactertable set heightfeet = \"\(CharacterHeightOneTextField.intValue)\" where ikey = \(CVCIkey) and firstname = \"\(CharacterFirstnameTextField.stringValue)\" and userikeyreference = \(CurrentUserIkey!)")
            
            print("UpdateCharacterHeightFeetSqlquery: \(UpdateCharacterHeightFeetSqlquery)")
            
            try db.executeUpdate(UpdateCharacterHeightFeetSqlquery, values: nil)
            
        } catch let error as NSError {
            
            let ErrorMessage = "\(#function) - Failed to do query \(UpdateCharacterHeightFeetSqlquery)\nError: \(error.localizedDescription)"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "CharacterInfoViewController", Level: 3, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to update Character Height One\nPlease copy it then reenter it again.\n\n\(UpdateCharacterHeightFeetSqlquery)", DatabaseError: "\(db.lastErrorMessage())")
            
        }
        
        ProgressIndicator()
    }
    
    func SaveHieghtTwoInfo() {
        ProgressIndicator()
        var UpdateCharacterHeightInchesSqlquery = String()
        
        do {
            
            UpdateCharacterHeightInchesSqlquery = String(format: "UPDATE charactertable set heightinches = \"\(CharacterHeightTwoTextField.intValue)\" where ikey = \(CVCIkey) and firstname = \"\(CharacterFirstnameTextField.stringValue)\" and userikeyreference = \(CurrentUserIkey!)")
            
            //print("UpdateCharacterHeightInchesSqlquery: \(UpdateCharacterHeightInchesSqlquery)")
            
            try db.executeUpdate(UpdateCharacterHeightInchesSqlquery, values: nil)
            
        } catch let error as NSError {
            
            let ErrorMessage = "\(#function) - Failed to do query \(UpdateCharacterHeightInchesSqlquery)\nError: \(error.localizedDescription)"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "CharacterInfoViewController", Level: 3, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to update Character Height Two\nPlease copy it then reenter it again.\n\n\(UpdateCharacterHeightInchesSqlquery)", DatabaseError: "\(db.lastErrorMessage())")
            
        }
        
        ProgressIndicator()
    }
    
    ///////////////////////////
    // Weight
    
    @IBAction func CharacterWeightTextFieldAction(_ sender: Any) {
        ProgressIndicator()
        CharacterWeightStepper.intValue = Int32(CharacterWeightTextField.stringValue)!
        SaveWeightInfo()
        //print("Weight CharacterWeightTextField: \(CharacterWeightTextField.stringValue)")
        ProgressIndicator()
    }
    
    @IBAction func CharacterWeightStepper(_ sender: Any) {
        ProgressIndicator()
        CharacterWeightTextField.stringValue = String(CharacterWeightStepper.intValue)
        CharacterWeightTextFieldAction(self)
        //print("Weight CharacterWeightStepper: \(CharacterWeightStepper.intValue)")
        ProgressIndicator()
    }
    
    
    func SaveWeightInfo() {
        ProgressIndicator()
        var UpdateCharacterWeightInchesSqlquery = String()
        
        do {
            
            UpdateCharacterWeightInchesSqlquery = String(format: "UPDATE charactertable set weight = \"\(CharacterWeightTextField.intValue)\" where ikey = \(CVCIkey) and firstname = \"\(CharacterFirstnameTextField.stringValue)\" and userikeyreference = \(CurrentUserIkey!)")
            
            print("UpdateCharacterWeightInchesSqlquery: \(UpdateCharacterWeightInchesSqlquery)")
            
            try db.executeUpdate(UpdateCharacterWeightInchesSqlquery, values: nil)
            
        } catch let error as NSError {
            
            let ErrorMessage = "\(#function) - Failed to do query \(UpdateCharacterWeightInchesSqlquery)\nError: \(error.localizedDescription)"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "CharacterInfoViewController", Level: 3, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to update Character Weight Two\nPlease copy it then reenter it again.\n\n\(UpdateCharacterWeightInchesSqlquery)", DatabaseError: "\(db.lastErrorMessage())")
            
        }
        
        ProgressIndicator()
    }
    
    ///////////////////////////
    // Gender Popup
    
    @IBAction func CharacterGenderPopUpButon(_ sender: Any) {
        ProgressIndicator()
        let SelectedGender = CharacterGenderPopupBUtton.titleOfSelectedItem
        
        var GenderToDB = Int()
        
        if (SelectedGender == "Gender") {
            GenderToDB = 1
        } else if (SelectedGender == "Male") {
            GenderToDB = 2
        } else if (SelectedGender == "Female") {
            GenderToDB = 3
        }
        
        
        var UpdateCharacterGenderSqlquery = String()
        
        do {
            //  \"\(SelectedGender!)\"
            
            
            UpdateCharacterGenderSqlquery = String(format: "UPDATE charactertable set gender = \(GenderToDB) where ikey = \(CVCIkey)  and userikeyreference = \(CurrentUserIkey!) and firstname = \"\(CharacterFirstnameTextField.stringValue)\"")
            
            print("UpdateCharacterGenderSqlquery: \(UpdateCharacterGenderSqlquery)")
            
            try db.executeUpdate(UpdateCharacterGenderSqlquery, values: nil)
            
        } catch let error as NSError {
            
            let ErrorMessage = "\(#function) - Failed to do query \(UpdateCharacterGenderSqlquery)\nError: \(error.localizedDescription)"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "CharacterInfoViewController", Level: 3, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to update Character Weight Two\nPlease copy it then reenter it again.\n\n\(UpdateCharacterGenderSqlquery)", DatabaseError: "\(db.lastErrorMessage())")
            
        }
        
        ProgressIndicator()
    }

    ///////////////////////////
    // Ethnicity
    
    @IBAction func CharacterEthnicityPopUpButton(_ sender: Any) {
        ProgressIndicator()
        let SelectedEthnicity = CharacterEthnicityPopUpButton.titleOfSelectedItem
        
        print("SelectedEthnicity: \(SelectedEthnicity!)")
        
        if (SelectedEthnicity! == "Other") {
            
            CharacterEthnicityOtherTextField.isEnabled = true
            CharacterEthnicityOtherTextField.isHidden = false
            CharacterEthnicityOtherTextField.becomeFirstResponder()
            
        } else {
            
            CharacterEthnicityOtherTextField.isEnabled = false
            CharacterEthnicityOtherTextField.isHidden = true
            CharacterEthnicityOtherTextField.stringValue = ""
    
            var UpdateCharacterEthnicitySqlquery = String()
            
            do {
                
                UpdateCharacterEthnicitySqlquery = String(format: "UPDATE charactertable set ethnicity = \"\(SelectedEthnicity!)\" where ikey = \(CVCIkey) and userikeyreference = \(CurrentUserIkey!) and firstname = \"\(CharacterFirstnameTextField.stringValue)\"")
                
                print("UpdateCharacterEthnicitySqlquery: \(UpdateCharacterEthnicitySqlquery)")
                
                try db.executeUpdate(UpdateCharacterEthnicitySqlquery, values: nil)
                
            } catch let error as NSError {
                
                let ErrorMessage = "\(#function) - Failed to do query \(UpdateCharacterEthnicitySqlquery)\nError: \(error.localizedDescription)"
                
                print(ErrorMessage)
                
                loggly(LogType.Error, text: ErrorMessage)
                
                DoAlert.DisplayAlert(Class: "CharacterInfoViewController", Level: 3, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to update Character Ethnicity\nPlease copy it then reenter it again.\n\n\(UpdateCharacterEthnicitySqlquery)", DatabaseError: "\(db.lastErrorMessage())")
                
            }
        }
        
        ProgressIndicator()
    }
    
    ///////////////////////////
    // Birthdate
    
    @IBAction func CharacterBirthdateDatePicker(_ sender: Any) {
        ProgressIndicator()
        let BDate = CharacterBirthdateDatePicker.dateValue

        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "YYYY-MM-DD HH:MM:SS.SSS"

        let BirthdateFDB = dateFormatterGet.string(from: BDate)
        print("BirthdateFDB: \(BirthdateFDB)")

        
        
        var UpdateCharacterBirhtdateSqlquery = String()
        
        do {
            
            UpdateCharacterBirhtdateSqlquery = String(format: "UPDATE charactertable set birthdate = \"\(BirthdateFDB)\" where ikey = \(CVCIkey) and firstname = \"\(CharacterFirstnameTextField.stringValue)\" and userikeyreference = \(CurrentUserIkey!)")
            
            print("UpdateCharacterBirhtdateSqlquery: \(UpdateCharacterBirhtdateSqlquery)")
            
            try db.executeUpdate(UpdateCharacterBirhtdateSqlquery, values: nil)
            
        } catch let error as NSError {
            
            let ErrorMessage = "\(#function) - Failed to do query \(UpdateCharacterBirhtdateSqlquery)\nError: \(error.localizedDescription)"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "CharacterInfoViewController", Level: 3, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to update Character Birthdate\nPlease copy it then reenter it again.\n\n\(UpdateCharacterBirhtdateSqlquery)", DatabaseError: "\(db.lastErrorMessage())")
            
        }
        
        ProgressIndicator()
    }
    
    
    
    @IBAction func CharacterEthnicityOtherTextFieldAction(_ sender: Any) {
        ProgressIndicator()
        let EthnicityOtherText = CharacterEthnicityOtherTextField.stringValue
        
        var UpdateCharacterEthnicitySqlquery = String()
        
        do {
            
            UpdateCharacterEthnicitySqlquery = String(format: "UPDATE charactertable set ethnicity = \"\(EthnicityOtherText)\" where ikey = \(CVCIkey) and userikeyreference = \(CurrentUserIkey!) and firstname = \"\(CharacterFirstnameTextField.stringValue)\"")
            
            print("UpdateCharacterEthnicitySqlquery: \(UpdateCharacterEthnicitySqlquery)")
            
            try db.executeUpdate(UpdateCharacterEthnicitySqlquery, values: nil)
            
        } catch let error as NSError {
            
            let ErrorMessage = "\(#function) - Failed to do query \(UpdateCharacterEthnicitySqlquery)\nError: \(error.localizedDescription)"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "CharacterInfoViewController", Level: 3, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to update Character Ethnicity Other\nPlease copy it then reenter it again.\n\n\(UpdateCharacterEthnicitySqlquery)", DatabaseError: "\(db.lastErrorMessage())")
            
        }
        
        
        
      ProgressIndicator()
    }
    
    
    ///////////////////////////
    // Place of Birth
    
    @IBAction func CharacterPlaceOfBirthTextFieldAction(_ sender: Any) {
        ProgressIndicator()
        let BirthPlaceText = CharacterBirthPlaceTextField.stringValue
        
        var UpdateCharacterBirthPlaceSqlquery = String()
        
        do {
            
            UpdateCharacterBirthPlaceSqlquery = String(format: "UPDATE charactertable set birthplace = \"\(BirthPlaceText)\" where ikey = \(CVCIkey) and userikeyreference = \(CurrentUserIkey!) and firstname = \"\(CharacterFirstnameTextField.stringValue)\"")
            
            print("UpdateCharacterBirthPlaceSqlquery: \(UpdateCharacterBirthPlaceSqlquery)")
            
            try db.executeUpdate(UpdateCharacterBirthPlaceSqlquery, values: nil)
            
        } catch let error as NSError {
            
            let ErrorMessage = "\(#function) - Failed to do query \(UpdateCharacterBirthPlaceSqlquery)\nError: \(error.localizedDescription)"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "CharacterInfoViewController", Level: 3, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to update Character Birthplace\nPlease copy it then reenter it again.\n\n\(UpdateCharacterBirthPlaceSqlquery)", DatabaseError: "\(db.lastErrorMessage())")
            
        }
        
        ProgressIndicator()
    }
    
    ///////////////////////////
    // Orientation
    
    
    @IBAction func CharacterOreientationTextFieldAction(_ sender: Any) {
        ProgressIndicator()
        let OrientationText = CharacterOrientationTextField.stringValue
        
        var UpdateCharacterOrientationSqlquery = String()
        
        do {
            
            
            
            UpdateCharacterOrientationSqlquery = String(format: "UPDATE charactertable set orientation = \"\(OrientationText)\" where ikey = \(CVCIkey) and userikeyreference = \(CurrentUserIkey!) and firstname = \"\(CharacterFirstnameTextField.stringValue)\"")
            
            print("UpdateCharacterOrientationSqlquery: \(UpdateCharacterOrientationSqlquery)")
            
            try db.executeUpdate(UpdateCharacterOrientationSqlquery, values: nil)
            
        } catch let error as NSError {
            
            let ErrorMessage = "\(#function) - Failed to do query \(UpdateCharacterOrientationSqlquery)\nError: \(error.localizedDescription)"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "CharacterInfoViewController", Level: 3, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to update Character Orientation\nPlease copy it then reenter it again.\n\n\(UpdateCharacterOrientationSqlquery)", DatabaseError: "\(db.lastErrorMessage())")
            
        }
        ProgressIndicator()
    }
    
    
    ///////////////////////////
    // Character Picture items
    
    @IBAction func CharacterShowImageNameButtonAction(_ sender: Any) {
        ProgressIndicator()
        if (!CharacterPictureTextField.isHidden) {
            CharacterPictureTextField.isHidden = true
            CharacterPictureTextField.isEnabled = false
        } else {
            CharacterPictureTextField.isHidden = false
            CharacterPictureTextField.isEnabled = true
 
            //print("ImageName: \"\(self.ImageName)\"")
            self.CharacterPictureTextField.stringValue = self.ImageName
        }
        ProgressIndicator()
    }
    
    
    @IBAction func CharacterPictureButton(_ sender: Any) {
        
        if (SharedItemLock == true) {
            return
        }

        ProgressIndicator()
        
        
        if let url = NSOpenPanel().selectUrl {
            CharacterPictureButton.image = NSImage(contentsOf: url)
            print("file selected:", url.path)
            
            let ImagePath = url.path
            
            print("ImagePath: \(ImagePath)")
            
            //////////////////
            
            var SelectLocationImageReferenceQuery = String()
            
            if (SharedItemOwner != "") {
                SelectLocationImageReferenceQuery = "Select COUNT(pathtoimage) as cnt FROM imagetable WHERE characterreference = \(CVCIkey) and userikeyreference = \(SharedItemOwner)"
            } else {
                SelectLocationImageReferenceQuery = "Select COUNT(pathtoimage) as cnt FROM imagetable WHERE characterreference = \(CVCIkey) and userikeyreference = \(CurrentUserIkey!)"
            }
            
            
            //print("SelectLocationImageReferenceQuery: \(SelectLocationImageReferenceQuery)")
            
            
            //i am here ,, need to pull the "pathtoimage" and if its null then check to see if image is in the images folder if no copy it if yes then just add the path into the DB
            
            var PathToImageReferenceDB = Int32()
            
            
            do {
                
                let PathToImageQueryRun = try db.executeQuery(SelectLocationImageReferenceQuery, values:nil)
                
                while PathToImageQueryRun.next() {
                    PathToImageReferenceDB = PathToImageQueryRun.int(forColumn: "cnt")
                }
                
                //print("SelectLocationImageReferenceQuery: \"\(SelectLocationImageReferenceQuery)\"")
                
            } catch {
                
                let ErrorMessage = "LocationsInfoViewController:\(#function) - Failed to get Images for \"\(LocationName)\" Location\n\(db.lastErrorMessage())\n\(SelectLocationImageReferenceQuery))"
                
                print(ErrorMessage)
                
                loggly(LogType.Error, text: ErrorMessage)
                
                DoAlert.DisplayAlert(Class: "LocationsInfoViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to get Images for \"\(LocationName)\" Location\n\n\(SelectLocationImageReferenceQuery)", DatabaseError: "\(db.lastErrorMessage())")
                
            }
            
            //////////////////
            
            
            
            let ImageName = (ImagePath as NSString).lastPathComponent
            let FullPathImageCheck = "\(ImagesLocation)/\(ImageName)"
            
            
            if (PathToImageReferenceDB == 0) {
                
                print("PathToImageReferenceDB = 0")
                // Character has not had a image before
                // INSERT image information into imagetable
                
                //"INSERT INTO charactertable (ikey,userikeyreference,firstname,middlename,lastname,birthdate,heightfeet,heightinches,weight,gender,ethnicity,birthplace,orientation,haircolor,facialcolor,skintone,charactersummary,haircut,hairpart,hairlength,facialhair) VALUES ((select MAX(ikey)+ 1 from charactertable),\"\(CurrentUserIkey!)\",\"FIRSTNAME\",\"\",\"LASTNAME\",\"\",\"0\",\"0\",\"0\",\"1\",\"Ethnicity\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\")"
                
                var NewCharater = String()
                
                if (SharedItemOwner != "") {
                    NewCharater = String(format: "INSERT INTO imagetable (ikey,userikeyreference,characterreference,locationtableikeyreference,pathtoimage) VALUES ((select MAX(ikey) + 1 from imagetable),\(SharedItemOwner),\(CVCIkey),\"\",\"\(FullPathImageCheck)\")")
                } else {
                    NewCharater = String(format: "INSERT INTO imagetable (ikey,userikeyreference,characterreference,locationtableikeyreference,pathtoimage) VALUES ((select MAX(ikey) + 1 from imagetable),\(CurrentUserIkey!),\(CVCIkey),\"\",\"\(FullPathImageCheck)\")")
                }
                
                
    
    //\"\(ImagePath)\"
                
                print("NewCharater: \(NewCharater)")
                
                do {
                    
                    
                    try db.executeUpdate(NewCharater, values: nil)
                    
                } catch let error as NSError {
                    
                    let ErrorMessage = "\(#function) - Failed to UPDATE Character Picture in DB.\nError: \(error.localizedDescription)"
                    
                    print(ErrorMessage)
                    
                    DoAlert.DisplayAlert(Class: "CharacterInfoViewConroller", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to UPDATE Character Picture in DB.\nPlease quit then try again.", DatabaseError: "\(error.localizedDescription)\n\n\(db.lastErrorMessage())")
                }
                
                CharacterPictureTextField.stringValue = (FullPathImageCheck as NSString).lastPathComponent
                
            } else if (PathToImageReferenceDB > 0) {
                
                print("PathToImageReferenceDB GREATER THAN 0")
                // Character has existing image reference
                // UPDATE imagetable
                
                var NewCharater = String()
                
                 if (SharedItemOwner != "") {
                    NewCharater = String(format: "UPDATE imagetable set pathtoimage = \"\(FullPathImageCheck)\" where characterreference = \(CVCIkey) and userikeyreference = \(SharedItemOwner)")
                 } else {
                    NewCharater = String(format: "UPDATE imagetable set pathtoimage = \"\(FullPathImageCheck)\" where characterreference = \(CVCIkey) and userikeyreference = \(CurrentUserIkey!)")
                }
                
                
                
                print("NewCharater: \(NewCharater)")
                
                do {
                    
                    
                    try db.executeUpdate(NewCharater, values: nil)
                    
                    let ImageName = "\((FullPathImageCheck as NSString).lastPathComponent)"
                    CharacterPictureTextField.stringValue = ImageName
                    
                } catch let error as NSError {
                    
                    let ErrorMessage = "\(#function) - Failed to UPDATE Character Picture in DB.\nError: \(error.localizedDescription)"
                    
                    print(ErrorMessage)
                    
                    DoAlert.DisplayAlert(Class: "CharacterInfoViewConroller", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to UPDATE Character Picture in DB.\nPlease quit then try again.", DatabaseError: "\(error.localizedDescription)\n\n\(db.lastErrorMessage())")
                }
                
            }
            
            
            ////////////////////////
            
            // Check for image in ImageFolder
            
            // if Exists your done
            
            // if NOT Exists then copy it there
            
            
           
            
            
            if FM.fileExists(atPath: FullPathImageCheck) {
                print("File exists")
            } else {
                print("File not found")
                do {
                    try FM.copyItem(atPath: ImagePath, toPath: FullPathImageCheck)
                }
                catch let error as NSError {
                    
                    let ErrorMessage = "Failed to copy image \((ImagePath as NSString).lastPathComponent) to Images folder\n\n\(error)"
                    
                    loggly(LogType.Error, text: ErrorMessage)
                    
                    DoAlert.DisplayAlert(Class: "CharacterInfoViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to copy image \((ImagePath as NSString).lastPathComponent) to Images folder \(FullPathImageCheck)", DatabaseError: "\(error)")
                }
            }
            
            print("File Copied Sucessfully")
            
            
        }
        
        ProgressIndicator()
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    func CreateNewCharacter() {
        
        ProgressIndicator()
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "YYYY-MM-DD HH:MM:SS.SSS"
        //let Today = Date()
        let TodaysDate = dateFormatterGet.string(from: Date())
        
        
        let NewCharater = String(format: "INSERT INTO charactertable (ikey,userikeyreference,firstname,middlename,lastname,birthdate,heightfeet,heightinches,weight,gender,ethnicity,birthplace,orientation,haircolor,facialcolor,skintone,charactersummary,haircut,hairpart,hairlength,facialhair,facialhairother) VALUES ((select MAX(ikey)+ 1 from charactertable),\"\(CurrentUserIkey!)\",\"FIRSTNAME\",\"\",\"LASTNAME\",\"\(TodaysDate)\",\"0\",\"0\",\"0\",\"1\",\"Ethnicity\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\")")
        
        
        print("NewCharater: \(NewCharater)")
        
        do {

            
            try db.executeUpdate(NewCharater, values: nil)
            
        } catch let error as NSError {
            
            let ErrorMessage = "\(#function) - Failed to do query \(NewCharater)\nError: \(error.localizedDescription)"
            
            print(ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "CharacterInfoViewConroller", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to Insert New Character into DB.\nPlease quit then try again.", DatabaseError: "\(error.localizedDescription)\n\n\(db.lastErrorMessage())")
        }
        
        ///////////////////////////
        
        CharacterFirstnameTextField.stringValue = "FIRSTNAME"
        CharacterLastnameTextField.stringValue = "LASTNAME"
        
        ///////////////////////////
        
        
        let SelectMaxIkeyQuery = "Select MAX(ikey) as mikey, firstname from charactertable"
        var IkeyDB = String()
        var NameDB = String()
        
        do {
            
            let SelectMaxIkeyQueryRun = try db.executeQuery(SelectMaxIkeyQuery, values:nil)
            
            while SelectMaxIkeyQueryRun.next() {
                IkeyDB = SelectMaxIkeyQueryRun.string(forColumn: "mikey")!
                NameDB = SelectMaxIkeyQueryRun.string(forColumn: "firstname")!
            }

        } catch {
            
            let ErrorMessage = "LocationsInfoViewController: \(#function) - Failed to get Max Ikey\n\n\(db.lastErrorMessage())\n\(SelectMaxIkeyQuery)"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "LocationsInfoViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to get Max Ikey\n\n\(db.lastErrorMessage())\n\(SelectMaxIkeyQuery)", DatabaseError: "\(db.lastErrorMessage())")
            
        }
        
        ///////////////////////////
        
        
        CVCIkey = IkeyDB
        CVCName = NameDB
        
        ///////////////////////////
        
        
        OpenCharacter()
        
        CharacterPictureButton.isEnabled = true
        
        
        
        
        ProgressIndicator()
    }
    
    
    ////////////////
    // OtherCharacterInfo popup button
    
    @IBAction func CharacterOtherInfoPopUpButton(_ sender: Any) {
        
        ProgressIndicator()
        
        //CharacterOtherSaveButtonAction(self)
        
        ////////////////////////
        
        let SelectedOption = CharacterOtherInformationPopUpButton.titleOfSelectedItem
        
        print("SelectedOption: \"\(SelectedOption!)\"")
        
        ////////////////////////
        
        if (SelectedOption == "" || SelectedOption == "Choose an Option") {
            ProgressIndicator()
            CharacterOtherCharacterInfoTexView.string = ""
            return
        } else {
            
            print("CharacterOtherFirstOpen: \(CharacterOtherFirstOpen)")
            
            
            if (CharacterOtherFirstOpen == true){
                //print("CharacterOtherFirstOpen = TRUE")
                
            } else {
                //CharacterOtherSaveButtonAction(self)
                //print("CharacterOtherSaveButtonAction RAN while changing Other Dropdown Option")
                //CharacterOtherFirstOpen = false
                
                if (SharedItemLock == true) {
                    
                    
                    
                } else {
                    
                    CharacterOtherSaveButtonAction(self)
                    
                    delayWithSeconds(0.25) {
                        delayWithSeconds(0.25) {
                            // This is for the PHANTOM issue where saving different other variable patient information back to back to back would stop saving with no error message the update query would be sent but it wouldnt save.
                        }
                    }
                    
                }
                
                
                
                
            }
            
            CharacterOtherFirstOpen = false
            
        }
        
        ////////////////////////
        
        
        
        switch SelectedOption! {
        case "Choose an Option" :
            print("Switch Choose an Option")
            CharacterOtherCharacterInfoTexView.string = ""
            return
        case "Aliases/Nicknames" :
            print("Switch Aliases/Nicknames")
            CharacterOtherVariable = "aliasesnicknames"
        case "Hopes/Dreams" :
            print("Switch Hopes/Dreams")
            CharacterOtherVariable = "hopesdreams"
        case "Fears" :
            print("Switch Fears")
            CharacterOtherVariable = "fears"
        case "Prejudices" :
            print("Switch Prejudices")
            CharacterOtherVariable = "prejudices"
        case "Religion" :
            print("Switch Religion")
            CharacterOtherVariable = "religion"
        case "Likes/Dislikes" :
            print("Switch Likes/Dislikes")
            CharacterOtherVariable = "likesdislikes"
        case "Background" :
            print("Switch Background")
            CharacterOtherVariable = "background"
        case "Traits" :
            print("Switch Traits")
            CharacterOtherVariable = "traits"
        case "Skills" :
            print("Switch Skills")
            CharacterOtherVariable = "skills"
        case "Family/Relatives" :
            print("Switch Family/Relatives")
            CharacterOtherVariable = "familyrelatives"
        case "Relationships" :
            print("Switch Relationships")
            CharacterOtherVariable = "relationships"
        case "Work History" :
            print("Switch Work History")
            CharacterOtherVariable = "workhistory"
        case "Favorite Foods" :
            print("Switch Favorite Foods")
            CharacterOtherVariable = "favoritenotfoods"
        case "Allergies" :
            print("Switch Allergies")
            CharacterOtherVariable = "allergies"
        case "Languages" :
            print("Switch Languages")
            CharacterOtherVariable = "languages"
        case "Fashion" :
            print("Switch Fashion")
            CharacterOtherVariable = "fashion"
        case "Surgeries" :
            print("Switch Surgeries")
            CharacterOtherVariable = "surgeries"
        case "Motivations" :
            print("Switch Motivations")
            CharacterOtherVariable = "motivations"
        default :
            print("Switch Selection Not Recognized: \"\(SelectedOption!)\"")
            return
        }

       /////////////////////////////////////
        
        ConnectionTest.CheckConnection()
        
        var OtherInfoQuery = String()
        
         if (SharedItemOwner != "") {
            OtherInfoQuery = "Select \(CharacterOtherVariable) FROM charactervariableinfotable WHERE characterreference = \(CVCIkey) and userikeyreference = \(SharedItemOwner)"
            
            print("OtherInfoQuery: \(OtherInfoQuery)")
            
         } else {
            OtherInfoQuery = "Select \(CharacterOtherVariable) FROM charactervariableinfotable WHERE characterreference = \(CVCIkey) and userikeyreference = \(CurrentUserIkey!)"
            
            print("OtherInfoQuery: \(OtherInfoQuery)")
            
        }
        
        
        
        
        
        
        var OtherInfoText = String()
        
        
        do {
            
            let OtherInfoQueryRun = try db.executeQuery(OtherInfoQuery, values:nil)
            
            while OtherInfoQueryRun.next() {
                OtherInfoText = OtherInfoQueryRun.string(forColumn: "\(CharacterOtherVariable)")!
            }
            
            
        } catch {
            
            let ErrorMessage = "CharacterInfoViewController:\(#function) - Failed to get Images for \"\(LocationName)\" Location\n\(db.lastErrorMessage())\n\(OtherInfoQuery))"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "CharacterInfoViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to get Other Character Information for \(CharacterFirstnameTextField.stringValue) \(CharacterLastnameTextField.stringValue)\n\n\(OtherInfoQuery)", DatabaseError: "\(db.lastErrorMessage())")
            
        }
        
        print("OtherInfoText: \"\(CharacterOtherVariable)\"\n\"\(OtherInfoText)\"")
        
        CharacterOtherCharacterInfoTexView.string = OtherInfoText
        
        
        
        
        
        ProgressIndicator()
        
    }
    
    
    
    @IBAction func CharacterOtherSaveButtonAction(_ sender: Any) {
        
        ProgressIndicator()
        
        //print("CharacterOtherSaveButtonAction Saved")
        
        let SelectedOption = CharacterOtherInformationPopUpButton.titleOfSelectedItem
        
        print("SelectedOption: \"\(SelectedOption!)\"")
        
        if (SelectedOption == "" || SelectedOption == "Choose an Option") {
            
            //DoAlert.DisplayAlert(Class: "CharacterInfoViewController", Level: 3, Function: "\(#function)", MessageText: "Warning", InformativeText: "CharacterOtherInformationPopUpButton.titleOfSelectedItem = \"\" or \"Choose an Option\" ", DatabaseError: "None")
            ProgressIndicator()
            return
            
        }
        
        
        ////////////////////
        
        db.open()
        
        
        var OtherInfoQuery = String()
        
        if (SharedItemOwner != "") {
            OtherInfoQuery = "Select COUNT(ikey) as CNT FROM charactervariableinfotable WHERE characterreference = \(CVCIkey) and userikeyreference = \(SharedItemOwner)"
        } else {
            OtherInfoQuery = "Select COUNT(ikey) as CNT FROM charactervariableinfotable WHERE characterreference = \(CVCIkey) and userikeyreference = \(CurrentUserIkey!)"
        }
        
       
        
        
        
        var CharacterCount = Int32()
        
        
        do {
            
            let OtherInfoQueryRun = try db.executeQuery(OtherInfoQuery, values:nil)
            
            while OtherInfoQueryRun.next() {
                CharacterCount = OtherInfoQueryRun.int(forColumn: "CNT")
            }
            
            
        } catch {
            
            let ErrorMessage = "CharacterInfoViewController:\(#function) - Failed to get Count for \"\(CharacterFirstnameTextField.stringValue)\" in the \"charactervariableinfotable\"\n\(db.lastErrorMessage())\n\(OtherInfoQuery))"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "CharacterInfoViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to get Count for \"\(CharacterFirstnameTextField.stringValue)\" in the \"charactervariableinfotable\"\n\(db.lastErrorMessage())\n\(OtherInfoQuery))", DatabaseError: "\(db.lastErrorMessage())")
            
        }

        ////////////////////
        
        
        
        if (CharacterCount == 1) {
            
            // if Character has charactervariableinfotable reference
            
            var UpdateCharacterOtherVariableSqlquery = String()
            
            do {
                
                if (SharedItemOwner != "") {
                    UpdateCharacterOtherVariableSqlquery = String(format: "UPDATE charactervariableinfotable SET \(CharacterOtherVariable) = \"\(CharacterOtherCharacterInfoTexView.string)\" WHERE characterreference = \(CVCIkey) and userikeyreference = \(SharedItemOwner) ")
                } else {
                    UpdateCharacterOtherVariableSqlquery = String(format: "UPDATE charactervariableinfotable SET \(CharacterOtherVariable) = \"\(CharacterOtherCharacterInfoTexView.string)\" WHERE characterreference = \(CVCIkey) and userikeyreference = \(CurrentUserIkey!) ")
                }
                
                print("UpdateCharacterOtherVariableSqlquery: \(UpdateCharacterOtherVariableSqlquery)")
                
                try db.executeUpdate(UpdateCharacterOtherVariableSqlquery, values: nil)
                
            } catch let error as NSError {
                
                let ErrorMessage = "\(#function) - Failed to do query \(UpdateCharacterOtherVariableSqlquery)\nError: \(error.localizedDescription)"
                
                print(ErrorMessage)
                
                loggly(LogType.Error, text: ErrorMessage)
                
                DoAlert.DisplayAlert(Class: "CharacterInfoViewController", Level: 3, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to update Other Character Information \"\(CharacterOtherVariable)\" \nPlease copy it then reenter it again.\n\n\(UpdateCharacterOtherVariableSqlquery)", DatabaseError: "\(db.lastErrorMessage())")
                
            }
        
        } else {
            
            // if Character HAS NO charactervariableinfotable reference
            
            let NewCharater = String(format: "INSERT INTO charactervariableinfotable (ikey,userikeyreference,characterreference,aliasesnicknames,hopesdreams,fears,prejudices,religion,likesdislikes,background,traits,skills,familyrelatives,relationships,workhistory,favoritenotfoods,allergies,languages,fashion,surgeries,motivations)VALUES ((select MAX(ikey) + 1 from charactervariableinfotable),\(CurrentUserIkey!),\(CVCIkey),\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\",\"\")")
            
            //\"\(ImagePath)\"
            
            print("NewCharater: \(NewCharater)")
            

            
            do {
                
                
                try db.executeUpdate(NewCharater, values: nil)
                
            } catch let error as NSError {
                
                let ErrorMessage = "\(#function) - Failed to UPDATE Character charactervariableinfotable in DB.\nError: \(error.localizedDescription)"
                
                print(ErrorMessage)
                
                DoAlert.DisplayAlert(Class: "CharacterInfoViewConroller", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to UPDATE Character charactervariableinfotable in DB.\nPlease quit then try again.", DatabaseError: "\(error.localizedDescription)\n\n\(db.lastErrorMessage())")
                
                return
            }
            
            ////////////////
            
            
            var UpdateCharacterOtherVariableSqlquery = String()
            
            do {
                
                if (SharedItemOwner != "") {
                    UpdateCharacterOtherVariableSqlquery = String(format: "UPDATE charactervariableinfotable SET \(CharacterOtherVariable) = \"\(CharacterOtherCharacterInfoTexView.string)\" WHERE characterreference = \(CVCIkey) and userikeyreference = \(SharedItemOwner) ")
                } else {
                    UpdateCharacterOtherVariableSqlquery = String(format: "UPDATE charactervariableinfotable SET \(CharacterOtherVariable) = \"\(CharacterOtherCharacterInfoTexView.string)\" WHERE characterreference = \(CVCIkey) and userikeyreference = \(CurrentUserIkey!) ")
                }
                
                
                
                print("UpdateCharacterOtherVariableSqlquery: \(UpdateCharacterOtherVariableSqlquery)")
                
                try db.executeUpdate(UpdateCharacterOtherVariableSqlquery, values: nil)
                
            } catch let error as NSError {
                
                let ErrorMessage = "\(#function) - Failed to do query \(UpdateCharacterOtherVariableSqlquery)\nError: \(error.localizedDescription)"
                
                print(ErrorMessage)
                
                loggly(LogType.Error, text: ErrorMessage)
                
                DoAlert.DisplayAlert(Class: "CharacterInfoViewController", Level: 3, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to update Other Character Information \"\(CharacterOtherVariable)\" \nPlease copy it then reenter it again.\n\n\(UpdateCharacterOtherVariableSqlquery)", DatabaseError: "\(db.lastErrorMessage())")
                
            }
            
        }
        
        //db.close()
        
        ProgressIndicator()
        
    }// end


    
    @objc func UpdateHairInfo() {
        ProgressIndicator()
        var HairCutDB = String()
        var HairPartDB = String()
        var HairLengthDB = String()
        var FacialHairDB = String()
        var FacialHairOtherDB = String()
        
        
        var CharacterHairInfoQuery = String()
        
        if (SharedItemOwner != "") {
            CharacterHairInfoQuery = "Select haircut,hairpart,hairlength,facialhair,facialhairother FROM charactertable WHERE ikey = \(CVCIkey) and userikeyreference = \(SharedItemOwner)"
        } else {
            CharacterHairInfoQuery = "Select haircut,hairpart,hairlength,facialhair,facialhairother FROM charactertable WHERE ikey = \(CVCIkey) and userikeyreference = \(CurrentUserIkey!)"
        }
        
        
        print("CharacterHairInfoQuery: \(CharacterHairInfoQuery)")
        
        do {
            
            let CharacterHairInfoQueryRun = try db.executeQuery(CharacterHairInfoQuery, values:nil)
            
            while CharacterHairInfoQueryRun.next() {
                HairCutDB = CharacterHairInfoQueryRun.string(forColumn: "haircut")!
                HairPartDB = CharacterHairInfoQueryRun.string(forColumn: "hairpart")!
                HairLengthDB = CharacterHairInfoQueryRun.string(forColumn: "hairlength")!
                FacialHairDB = CharacterHairInfoQueryRun.string(forColumn: "facialhair")!
                FacialHairOtherDB = CharacterHairInfoQueryRun.string(forColumn: "facialhairother")!
            }
            
            
        } catch {
            
            let ErrorMessage = "CharacterInfoViewController:\(#function) - Failed to Get Character Hair Information \n\(db.lastErrorMessage())\n\(CharacterHairInfoQuery))"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "CharacterInfoViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to Update Character Hair Information\n\n\(CharacterHairInfoQuery)", DatabaseError: "\(db.lastErrorMessage())")
            return
        }
        
        
        /////////////////
        
        print("FACIAL HAIR:  HairCutDB: \"\(HairCutDB)\" HairPartDB: \"\(HairPartDB)\" HairLengthDB: \"\(HairLengthDB)\"")
        
        var HairString = String()
       
        
        if (HairCutDB == "Choose One" && HairPartDB == "Choose One") {
            HairString = "\(HairLengthDB)"
            print("1")
        } else if (HairCutDB == "Choose One" && HairLengthDB == "Choose One") {
            HairString = "\(HairLengthDB)"
            print("2")
        } else if (HairPartDB == "Choose One" && HairLengthDB == "Choose One") {
            HairString = "\(HairCutDB)"
            print("3")
        } else if (HairCutDB == "Choose One") {
            HairString = "\(HairPartDB) - \(HairLengthDB)"
            print("4")
        } else if (HairPartDB == "Choose One") {
            HairString = "\(HairCutDB) - \(HairLengthDB)"
            print("5")
        } else if (HairLengthDB == "Choose One") {
            HairString = "\(HairCutDB) - \(HairPartDB)"
            print("6")
        } else  {
            HairString = "\(HairCutDB) - \(HairPartDB) - \(HairLengthDB)"
            print("7")
        }
        
        CharacterHairCWTextField.stringValue = HairString
        
        
        /////////////////
        // Facial Hair
        
        
        if (FacialHairOtherDB == "") {
            FacialHairOtherDB = "Blank"
        }
        if (HairCutDB == "") {
            HairCutDB = "Blank"
        }
        if (HairPartDB == "") {
            HairPartDB = "Blank"
        }
        if (HairLengthDB == "") {
            HairLengthDB = "Blank"
        }
        
        
        
        var FacialHairArray = [String]()
        
        FacialHairArray = FacialHairDB.components(separatedBy: ",")
        
        var FacialString = String()
        
        if (FacialHairArray.contains("&")) {
            FacialString = String(FacialHairArray.joined(separator: "&"))
            FacialString = FacialString.replacingOccurrences(of: "&", with: " & ")
        } else {
            FacialString = String(FacialHairArray.joined(separator: ","))
            FacialString = FacialString.replacingOccurrences(of: ",", with: " & ")
        }
        
        if (FacialHairArray.contains("Other")) {
            FacialString = FacialString.replacingOccurrences(of: "Other", with: "\(FacialHairOtherDB)")
        }
        
        
        
        CharacterFacialCWTextField.stringValue = FacialString
        
        
        
        ProgressIndicator()
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    func CleanUp() {
        
        let BundlePath = Bundle.main.path(forResource: "avatar", ofType: "jpg")
        CharacterPictureButton.image = NSImage(contentsOfFile: BundlePath!)
        
        CharacterFirstnameTextField.stringValue = ""
        CharacterMiddleTextField.stringValue = ""
        CharacterLastnameTextField.stringValue = ""
        

        let Today = Date()
        CharacterBirthdateDatePicker.dateValue = Today
        
        CharacterHeightOneTextField.stringValue = ""
        CharacterHeightOneStepper.intValue = 0
        CharacterHeightTwoTextField.stringValue = ""
        CharacterHeightTwoStepper.intValue = 0
        CharacterWeightTextField.stringValue = ""
        CharacterWeightStepper.intValue = 0
        CharacterGenderPopupBUtton.selectItem(withTitle:"Gender")
        CharacterEthnicityPopUpButton.selectItem(withTitle: "Ethnicity")
        CharacterBirthPlaceTextField.stringValue = ""
        CharacterOrientationTextField.stringValue = ""
        CharacterHairColorCWColorWell.color = NSColor.black
        CharacterFacialColorCWColorWell.color = NSColor.black
        CharacterSkinToneCWColorWell.color = NSColor.black
        CharacterSummaryTextView.string = ""
        CharacterSummaryText = 0
        
        CharacterOtherCharacterInfoTexView.string = ""
        
        CharacterPictureTextField.stringValue = ""
        CharacterPictureTextField.isHidden = true
        CharacterPictureTextField.isEnabled = false

        //CharacterShowImageNameButtonAction(self)
        
        
    }
    
    
    
    @IBAction func CharacterCloseWindowButtonAction(_ sender: Any) {
        
        
        CharacterOtherSaveButtonAction(self)
        CharacterOtherFirstOpen = true
        
        UnLockCharacter()
        
        //////////////////
        
        CVCIkey = ""
        CVCName = ""
        CVCBirthdate = ""
        CleanUp()
        
        SharedItemLock = false
        SharedItemOwner = ""
        
        self.view.window?.close()
 
        //CharVC.CharacterListTableData()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CharacterListTableData"), object: nil)
        
        OpenWindows.CheckForOpenWindows()
        
    }// end of Close Window
    
    
    
    @IBAction func CharacterCancelWindowButtonAction(_ sender: Any) {
        
        UnLockCharacter()
        
        CharacterOtherFirstOpen = true
        
        //////////////////
        
        CVCIkey = ""
        CVCName = ""
        CVCBirthdate = ""
        CleanUp()
        
        SharedItemLock = false
        SharedItemOwner = ""
        
        self.view.window?.close()
        
        //CharVC.CharacterListTableData()
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CharacterListTableData"), object: nil)
        
    }
    
    
    
    
    
    
    
    
    
} // End of CharacterInfoViewController


func delayWithSeconds(_ seconds: Double, completion: @escaping () -> ()) {
    DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
        completion()
    }
}

extension NSColor{
    func HexToColorCIVC(hexString: String, alpha:CGFloat? = 1.0) -> NSColor {
        // Convert hex string to an integer
        let hexint = Int(self.intFromHexString(hexStr: hexString))
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        let alpha = alpha!
        // Create color object, specifying alpha as well
        let color = NSColor(red: red, green: green, blue: blue, alpha: alpha)
        return color
    }
}
