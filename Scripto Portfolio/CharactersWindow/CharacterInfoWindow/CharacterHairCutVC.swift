//
//  CharacterHairCutVC.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 3/27/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Cocoa
import SwiftLoggly

class CharacterHairCutVC: NSViewController {

    
    @IBOutlet weak var CharacterHairCut: NSPopUpButton!
    @IBOutlet weak var CharacterHairCutOtherTextField: NSTextField!
    @IBOutlet weak var CharacterHairPart: NSPopUpButton!
    @IBOutlet weak var CharacterHairPartOtherTextField: NSTextField!
    @IBOutlet weak var CharacterHairLength: NSPopUpButton!
    @IBOutlet weak var CharacterHairLengthOtherTextField: NSTextField!
    
    
    @IBOutlet weak var CharacterHairSideBurnsCheckBox: NSButton!
    @IBOutlet weak var CharacterHairFullBeardCheckBox: NSButton!
    @IBOutlet weak var CharacterHairGoateeCheckBox: NSButton!
    @IBOutlet weak var CharacterHairHorseShoeCheckBox: NSButton!
    @IBOutlet weak var CharacterHairMoustacheCheckBox: NSButton!
    @IBOutlet weak var CharacterHairSoulPatchCheckBox: NSButton!
    @IBOutlet weak var CharacterHairCleanShavenCheckBox: NSButton!
    @IBOutlet weak var CharacterHairOtherCheckBox: NSButton!
    @IBOutlet weak var CharacterHairOtherTextField: NSTextField!
    
    
    var FacialHairStringArray = [String]()
    var FacialHairStringToBD = String()
    var FacialHairOtherStringToBD = String()
    
    
    
    var hairstyle = [String]()
    var hairpart = [String]()
    var hairlength = [String]()
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.

    }
    

    
    override func viewWillAppear() {
        

        
        CharacterHairStyleSetup()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(SaveInfo), name: NSPopover.didCloseNotification, object: nil)
        
        LoadHairInfo()
    }
    
    func LoadHairInfo() {
        
        
        
        var HairCutDB = String()
        var HairPartDB = String()
        var HairLengthDB = String()
        var FacialHairDB = String()
        var FacialHairOtherDB = String()
        
        
        
        let CharacterHairInfoQuery = "Select haircut,hairpart,hairlength,facialhair,facialhairother FROM charactertable WHERE ikey = \(CVCIkey) and userikeyreference = \(CurrentUserIkey!)"
        
        print("CharacterHairInfoQuery: \(CharacterHairInfoQuery)")
        
        do {
            
            let CharacterHairInfoQueryRun = try db.executeQuery(CharacterHairInfoQuery, values:nil)
            
            while CharacterHairInfoQueryRun.next() {
                HairCutDB = CharacterHairInfoQueryRun.string(forColumn: "haircut")!
                HairPartDB = CharacterHairInfoQueryRun.string(forColumn: "hairpart")!
                HairLengthDB = CharacterHairInfoQueryRun.string(forColumn: "hairlength")!
                FacialHairDB = CharacterHairInfoQueryRun.string(forColumn: "facialhair")!
                FacialHairOtherDB = CharacterHairInfoQueryRun.string(forColumn: "facialhairother")!
            }
            
            
        } catch {
            
            let ErrorMessage = "CharacterHairCutVC:\(#function) - Failed to Update Character Hair Information \n\(db.lastErrorMessage())\n\(CharacterHairInfoQuery))"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "CharacterHairCutVC", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to Update Character Hair Information\n\n\(CharacterHairInfoQuery)", DatabaseError: "\(db.lastErrorMessage())")
                return
        }
        
        //////////////////////
        
        // Hair
        
        if (FacialHairOtherDB == "") {
            FacialHairOtherDB = "Blank"
        }
        if (HairCutDB == "") {
            HairCutDB = "Choose One"
        }
        if (HairPartDB == "") {
            HairPartDB = "Choose One"
        }
        if (HairLengthDB == "") {
            HairLengthDB = "Choose One"
        }
        
        
        if (HairCutDB == "Choose One") {
            CharacterHairCut.selectItem(withTitle: "Choose One")
        }else if (hairstyle.contains(HairCutDB)) {
            CharacterHairCut.selectItem(withTitle: HairCutDB)
            CharacterHairCutPopUpAction(self)
        } else {
            CharacterHairCut.selectItem(withTitle: "Other")
            CharacterHairCutOtherTextField.stringValue = HairCutDB
            CharacterHairCutOtherTextField.isEnabled = true
            CharacterHairCutOtherTextField.isHidden = false
        }
        
        if (hairpart.contains(HairPartDB)) {
            CharacterHairPart.selectItem(withTitle: HairPartDB)
            //CharacterPartPopUpActionon(self)
        } else {
            CharacterHairPart.selectItem(withTitle: "Other")
            CharacterHairPartOtherTextField.stringValue = HairPartDB
            CharacterHairPartOtherTextField.isEnabled = true
            CharacterHairPartOtherTextField.isHidden = false
        }
        
        if (hairlength.contains(HairLengthDB)) {
            CharacterHairLength.selectItem(withTitle: HairLengthDB)
            //CharacterLengthPopUpActionon(self)
        } else {
            CharacterHairLength.selectItem(withTitle: "Other")
            CharacterHairLengthOtherTextField.stringValue = HairLengthDB
            CharacterHairLengthOtherTextField.isEnabled = true
            CharacterHairLengthOtherTextField.isHidden = false
        }
        
        
        // Facial Hair
        
        var FacialHairDBArray: Array = [String]()
        
        if (FacialHairDB.contains("&")) {
            FacialHairDBArray = FacialHairDB.components(separatedBy: "&")
        } else {
            FacialHairDBArray = FacialHairDB.components(separatedBy: ",")
        }
        
        

        
        
        if (FacialHairDBArray.contains("SideBurns")) {
            CharacterHairSideBurnsCheckBox.state = .on
        }
        if (FacialHairDBArray.contains("FullBeard")) {
            CharacterHairFullBeardCheckBox.state = .on
            CharacterFullBeardCheckBoxAction(self)
        }
        if (FacialHairDBArray.contains("Goatee")) {
            CharacterHairGoateeCheckBox.state = .on
            CharacterGoateeCheckBoxAction(self)
        }
        if (FacialHairDBArray.contains("HorseShoe")) {
            CharacterHairHorseShoeCheckBox.state = .on
            CharacterHorseShoeCheckBoxAction(self)
        }
        if (FacialHairDBArray.contains("Moustache")) {
            CharacterHairMoustacheCheckBox.state = .on
            CharacterMoustacheCheckBoxAction(self)
        }
        if (FacialHairDBArray.contains("Moustache")) {
            CharacterHairMoustacheCheckBox.state = .on
            CharacterMoustacheCheckBoxAction(self)
        }
        if (FacialHairDBArray.contains("SoulPatch")) {
            CharacterHairSoulPatchCheckBox.state = .on
            CharacterSoulPatchCheckBoxAction(self)
        }
        if (FacialHairDBArray.contains("CleanShaven")) {
            CharacterHairCleanShavenCheckBox.state = .on
            CharacterCleanShavenCheckBoxAction(self)
        }
        if (FacialHairDBArray.contains("Other")) {
            CharacterHairOtherCheckBox.state = .on
            CharacterOtherCheckBoxAction(self)
            CharacterHairOtherTextField.isEnabled = true
            CharacterHairOtherTextField.isHidden = false
            CharacterHairOtherTextField.stringValue = FacialHairOtherDB
        }
        
    
        
        
 
        
        
        
        
        
        
        
        
    } // end of func
    
   
    
    
    
    
    @IBAction func CharacterSideBurnCheckBoxAction(_ sender: Any) {
    }
    @IBAction func CharacterFullBeardCheckBoxAction(_ sender: Any) {
        if (CharacterHairFullBeardCheckBox.state == .on) {
            
            CharacterHairGoateeCheckBox.isEnabled = false
            CharacterHairHorseShoeCheckBox.isEnabled = false
            CharacterHairMoustacheCheckBox.isEnabled = false
            CharacterHairSoulPatchCheckBox.isEnabled = false
            CharacterHairCleanShavenCheckBox.isEnabled = false
            CharacterHairOtherCheckBox.isEnabled = false
            
            CharacterHairOtherTextField.isEnabled = false
            CharacterHairOtherTextField.isHidden = true
        } else {
            CharacterHairGoateeCheckBox.isEnabled = true
            CharacterHairHorseShoeCheckBox.isEnabled = true
            CharacterHairMoustacheCheckBox.isEnabled = true
            CharacterHairSoulPatchCheckBox.isEnabled = true
            CharacterHairCleanShavenCheckBox.isEnabled = true
            CharacterHairOtherCheckBox.isEnabled = true
        }
    }
    @IBAction func CharacterGoateeCheckBoxAction(_ sender: Any) {
        if (CharacterHairGoateeCheckBox.state == .on) {
            
            CharacterHairFullBeardCheckBox.isEnabled = false
            CharacterHairHorseShoeCheckBox.isEnabled = false
            CharacterHairMoustacheCheckBox.isEnabled = false
            CharacterHairSoulPatchCheckBox.isEnabled = false
            CharacterHairCleanShavenCheckBox.isEnabled = false
            CharacterHairOtherCheckBox.isEnabled = false
            
            CharacterHairOtherTextField.isEnabled = false
            CharacterHairOtherTextField.isHidden = true
        } else {
            CharacterHairFullBeardCheckBox.isEnabled = true
            CharacterHairHorseShoeCheckBox.isEnabled = true
            CharacterHairMoustacheCheckBox.isEnabled = true
            CharacterHairSoulPatchCheckBox.isEnabled = true
            CharacterHairCleanShavenCheckBox.isEnabled = true
            CharacterHairOtherCheckBox.isEnabled = true
        }
    }
    @IBAction func CharacterHorseShoeCheckBoxAction(_ sender: Any) {
        if (CharacterHairHorseShoeCheckBox.state == .on) {
            
            CharacterHairFullBeardCheckBox.isEnabled = false
            CharacterHairGoateeCheckBox.isEnabled = false
            CharacterHairMoustacheCheckBox.isEnabled = false
            CharacterHairSoulPatchCheckBox.isEnabled = false
            CharacterHairCleanShavenCheckBox.isEnabled = false
            CharacterHairOtherCheckBox.isEnabled = false
            
            CharacterHairOtherTextField.isEnabled = false
            CharacterHairOtherTextField.isHidden = true
        } else {
            CharacterHairFullBeardCheckBox.isEnabled = true
            CharacterHairGoateeCheckBox.isEnabled = true
            CharacterHairMoustacheCheckBox.isEnabled = true
            CharacterHairSoulPatchCheckBox.isEnabled = true
            CharacterHairCleanShavenCheckBox.isEnabled = true
            CharacterHairOtherCheckBox.isEnabled = true
        }
    }
    @IBAction func CharacterMoustacheCheckBoxAction(_ sender: Any) {
        if (CharacterHairMoustacheCheckBox.state == .on) {
            
            CharacterHairFullBeardCheckBox.isEnabled = false
            CharacterHairGoateeCheckBox.isEnabled = false
            CharacterHairHorseShoeCheckBox.isEnabled = false
            //CharacterHairSoulPatchCheckBox.isEnabled = false
            CharacterHairCleanShavenCheckBox.isEnabled = false
            CharacterHairOtherCheckBox.isEnabled = false
            
            CharacterHairOtherTextField.isEnabled = false
            CharacterHairOtherTextField.isHidden = true
        } else {
            CharacterHairFullBeardCheckBox.isEnabled = true
            CharacterHairGoateeCheckBox.isEnabled = true
            CharacterHairHorseShoeCheckBox.isEnabled = true
            //CharacterHairSoulPatchCheckBox.isEnabled = true
            CharacterHairCleanShavenCheckBox.isEnabled = true
            CharacterHairOtherCheckBox.isEnabled = true
        }
    }
    @IBAction func CharacterSoulPatchCheckBoxAction(_ sender: Any) {
        if (CharacterHairSoulPatchCheckBox.state == .on) {
            
            CharacterHairFullBeardCheckBox.isEnabled = false
            CharacterHairGoateeCheckBox.isEnabled = false
            CharacterHairHorseShoeCheckBox.isEnabled = false
            //CharacterHairMoustacheCheckBox.isEnabled = false
            CharacterHairCleanShavenCheckBox.isEnabled = false
            CharacterHairOtherCheckBox.isEnabled = false
            
            CharacterHairOtherTextField.isEnabled = false
            CharacterHairOtherTextField.isHidden = true
        } else {
            CharacterHairFullBeardCheckBox.isEnabled = true
            CharacterHairGoateeCheckBox.isEnabled = true
            CharacterHairHorseShoeCheckBox.isEnabled = true
            //CharacterHairMoustacheCheckBox.isEnabled = true
            CharacterHairCleanShavenCheckBox.isEnabled = true
            CharacterHairOtherCheckBox.isEnabled = true
        }
    }
    @IBAction func CharacterCleanShavenCheckBoxAction(_ sender: Any) {
        if (CharacterHairCleanShavenCheckBox.state == .on) {
            
            CharacterHairFullBeardCheckBox.isEnabled = false
            CharacterHairGoateeCheckBox.isEnabled = false
            CharacterHairHorseShoeCheckBox.isEnabled = false
            CharacterHairMoustacheCheckBox.isEnabled = false
            CharacterHairSoulPatchCheckBox.isEnabled = false
            CharacterHairOtherCheckBox.isEnabled = false
            
            CharacterHairOtherTextField.isEnabled = false
            CharacterHairOtherTextField.isHidden = true
        } else {
            CharacterHairFullBeardCheckBox.isEnabled = true
            CharacterHairGoateeCheckBox.isEnabled = true
            CharacterHairHorseShoeCheckBox.isEnabled = true
            CharacterHairMoustacheCheckBox.isEnabled = true
            CharacterHairSoulPatchCheckBox.isEnabled = true
            CharacterHairOtherCheckBox.isEnabled = true
        }
    }
    @IBAction func CharacterOtherCheckBoxAction(_ sender: Any) {
        if (CharacterHairOtherCheckBox.state == .on) {
            CharacterHairCleanShavenCheckBox.isEnabled = false
            CharacterHairFullBeardCheckBox.isEnabled = false
            CharacterHairGoateeCheckBox.isEnabled = false
            CharacterHairHorseShoeCheckBox.isEnabled = false
            CharacterHairMoustacheCheckBox.isEnabled = false
            CharacterHairSoulPatchCheckBox.isEnabled = false
            CharacterHairOtherCheckBox.isEnabled = true
            
            CharacterHairOtherTextField.isEnabled = true
            CharacterHairOtherTextField.isHidden = false
        } else {
            CharacterHairCleanShavenCheckBox.isEnabled = true
            CharacterHairFullBeardCheckBox.isEnabled = true
            CharacterHairGoateeCheckBox.isEnabled = true
            CharacterHairHorseShoeCheckBox.isEnabled = true
            CharacterHairMoustacheCheckBox.isEnabled = true
            CharacterHairSoulPatchCheckBox.isEnabled = true
            CharacterHairOtherCheckBox.isEnabled = true
            
            CharacterHairOtherTextField.isEnabled = false
            CharacterHairOtherTextField.isHidden = true
        }
    }
    
    
    
    /////////////////////////////////////////
    
    
    
    func CharacterHairStyleSetup() {
        
        hairstyle = ["Bald","Military Cut","Military Cut - Flat Top","Buzz Cut","Bowl Cut","Crew Cut","Mullet","Shag","Afro","Business Cut","Ponytail"]
        
        // longest to shortest
        //        var hairstyleSortedArray = hairstyle.sorted {
        //            (obj1, obj2) -> Bool in
        //            return obj1.count > obj2.count
        //        }
        //print("hairstyleSortedArray: \(hairstyleSortedArray)")
        
        hairstyle = hairstyle.sorted {
            $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending
        }
        //print("hairstyleSortedArray: \(hairstyleSortedArray)")
        CharacterHairCut.removeAllItems()
        CharacterHairCut.addItems(withTitles: hairstyle)
        CharacterHairCut.insertItem(withTitle: "Other", at: 11)
        CharacterHairCut.insertItem(withTitle: "Cancel", at: 12)
        CharacterHairCut.insertItem(withTitle: "Choose One", at: 0)
        CharacterHairCut.selectItem(at: 0)
        
        /////////////
        
        hairpart = ["Choose One","No Part","Left Side","Middle","Right Side","Other","Cancel"]
        CharacterHairPart.removeAllItems()
        CharacterHairPart.addItems(withTitles: hairpart)
        //        CharacterHairPart.insertItem(withTitle: "Other", at: 3)
        //        CharacterHairPart.insertItem(withTitle: "Cancel", at: 4)
        //        CharacterHairPart.insertItem(withTitle: "Choose One", at: 0)
        CharacterHairPart.selectItem(withTitle: "Choose One")
        
        /////////////
        
        hairlength = ["Choose One","Short","Medium","Long","Other","Canel"]
        CharacterHairLength.removeAllItems()
        CharacterHairLength.addItems(withTitles: hairlength)
        CharacterHairLength.selectItem(at: 0)
        
    }
    
    
    
    @IBAction func CharacterHairCutPopUpAction(_ sender: Any) {
        
        /////////////
        
        let hairpart = ["Choose One","No Part","Left Side","Middle","Right Side","Other","Cancel"]
        CharacterHairPart.removeAllItems()
        CharacterHairPart.addItems(withTitles: hairpart)
        //        CharacterHairPart.insertItem(withTitle: "Other", at: 3)
        //        CharacterHairPart.insertItem(withTitle: "Cancel", at: 4)
        //        CharacterHairPart.insertItem(withTitle: "Choose One", at: 0)
        CharacterHairPart.selectItem(withTitle: "Choose One")
        
        /////////////
        
        let hairlength = ["Choose One","Short","Medium","Long","Other","Cancel"]
        CharacterHairLength.removeAllItems()
        CharacterHairLength.addItems(withTitles: hairlength)
        CharacterHairLength.selectItem(at: 0)
        
        ///////////////////////////////////////
        
        CharacterHairPart.isEnabled = true
        CharacterHairLength.isEnabled = true
        
        if (!CharacterHairPartOtherTextField.isHidden) {
            CharacterHairPartOtherTextField.stringValue = ""
            CharacterHairPartOtherTextField.isEnabled = false
            CharacterHairPartOtherTextField.isHidden = true
        }
        
        if (!CharacterHairLengthOtherTextField.isHidden) {
            CharacterHairLengthOtherTextField.stringValue = ""
            CharacterHairLengthOtherTextField.isEnabled = false
            CharacterHairLengthOtherTextField.isHidden = true
        }
        
        ///////////////////////////////////////
        
        let SelectedOption = CharacterHairCut.titleOfSelectedItem
        
        
        print("SelectedOption: \(SelectedOption!)")
        
        
        
        if (SelectedOption == "Choose One") {
            if (!CharacterHairCutOtherTextField.isHidden) {
                CharacterHairCutOtherTextField.stringValue = ""
                CharacterHairCutOtherTextField.isEnabled = false
                CharacterHairCutOtherTextField.isHidden = true
            }
        } else if (SelectedOption == "Cancel") {
            if (!CharacterHairCutOtherTextField.isHidden) {
                CharacterHairCutOtherTextField.stringValue = ""
                CharacterHairCutOtherTextField.isEnabled = false
                CharacterHairCutOtherTextField.isHidden = true
            }
            CharacterHairCut.selectItem(withTitle: "Choose One")
        } else if (SelectedOption == "Other") {
            if (CharacterHairCutOtherTextField.isHidden) {
                CharacterHairCutOtherTextField.becomeFirstResponder()
                CharacterHairCutOtherTextField.stringValue = ""
                CharacterHairCutOtherTextField.isEnabled = true
                CharacterHairCutOtherTextField.isHidden = false
            }
        } else if (SelectedOption == "Bald") {
            if (!CharacterHairCutOtherTextField.isHidden) {
                CharacterHairCutOtherTextField.stringValue = ""
                CharacterHairCutOtherTextField.isEnabled = false
                CharacterHairCutOtherTextField.isHidden = true
            }
            CharacterHairPart.isEnabled = false
            CharacterHairLength.isEnabled = false
        } else if (SelectedOption == "Military Cut" || SelectedOption == "Military Cut - Flat Top" || SelectedOption == "Buzz Cut" || SelectedOption == "Crew Cut") {
            if (!CharacterHairCutOtherTextField.isHidden) {
                CharacterHairCutOtherTextField.stringValue = ""
                CharacterHairCutOtherTextField.isEnabled = false
                CharacterHairCutOtherTextField.isHidden = true
            }
            CharacterHairPart.isEnabled = false
            CharacterHairLength.isEnabled = false
        } else if (SelectedOption == "Buzz Cut" || SelectedOption == "Crew Cut") {
            if (!CharacterHairCutOtherTextField.isHidden) {
                CharacterHairCutOtherTextField.stringValue = ""
                CharacterHairCutOtherTextField.isEnabled = false
                CharacterHairCutOtherTextField.isHidden = true
            }
        } else if (SelectedOption == "Afro") {
            if (!CharacterHairCutOtherTextField.isHidden) {
                CharacterHairCutOtherTextField.stringValue = ""
                CharacterHairCutOtherTextField.isEnabled = false
                CharacterHairCutOtherTextField.isHidden = true
            }
            CharacterHairLength.removeItem(withTitle: "Short")
            //CharacterHairLength.removeItem(at: 1)
            CharacterHairPart.selectItem(withTitle: "Choose One")
            CharacterHairPart.isEnabled = false
        } else if (SelectedOption == "Ponytail") {
            if (!CharacterHairCutOtherTextField.isHidden) {
                CharacterHairCutOtherTextField.stringValue = ""
                CharacterHairCutOtherTextField.isEnabled = false
                CharacterHairCutOtherTextField.isHidden = true
            }
            CharacterHairPart.isEnabled = true
            CharacterHairLength.removeItem(withTitle: "Short")
        } else if (SelectedOption == "Mullet") {
            if (!CharacterHairCutOtherTextField.isHidden) {
                CharacterHairCutOtherTextField.stringValue = ""
                CharacterHairCutOtherTextField.isEnabled = false
                CharacterHairCutOtherTextField.isHidden = true
            }
            CharacterHairLength.removeItem(withTitle: "Short")
        } else if (SelectedOption == "Shag") {
            if (!CharacterHairCutOtherTextField.isHidden) {
                CharacterHairCutOtherTextField.stringValue = ""
                CharacterHairCutOtherTextField.isEnabled = false
                CharacterHairCutOtherTextField.isHidden = true
            }
            CharacterHairLength.removeItem(withTitle: "Short")
            CharacterHairPart.selectItem(withTitle: "Choose One")
            CharacterHairPart.isEnabled = false
        }

 
    }
    
    @IBAction func CharacterPartPopUpAction(_ sender: Any) {
        
        let SelectedOption = CharacterHairPart.titleOfSelectedItem

        print("SelectedOption: \(SelectedOption!)")

        if (SelectedOption == "Choose One") {
            if (!CharacterHairPartOtherTextField.isHidden) {
                CharacterHairPartOtherTextField.stringValue = ""
                CharacterHairPartOtherTextField.isEnabled = false
                CharacterHairPartOtherTextField.isHidden = true
            }
        } else if (SelectedOption == "Cancel") {
            if (!CharacterHairPartOtherTextField.isHidden) {
                CharacterHairPartOtherTextField.stringValue = ""
                CharacterHairPartOtherTextField.isEnabled = false
                CharacterHairPartOtherTextField.isHidden = true
            }
            CharacterHairPart.selectItem(withTitle: "Choose One")
        } else if (SelectedOption == "Other") {
            if (CharacterHairPartOtherTextField.isHidden) {
                CharacterHairPartOtherTextField.becomeFirstResponder()
                CharacterHairPartOtherTextField.stringValue = ""
                CharacterHairPartOtherTextField.isEnabled = true
                CharacterHairPartOtherTextField.isHidden = false
            }
        } else {
            if (!CharacterHairPartOtherTextField.isHidden) {
                CharacterHairPartOtherTextField.stringValue = ""
                CharacterHairPartOtherTextField.isEnabled = false
                CharacterHairPartOtherTextField.isHidden = true
            }
        }
        
    }// end of CharacterPartPopUpAction
    
    @IBAction func CharacterLengthPopUpAction(_ sender: Any) {
        
        let SelectedOption = CharacterHairLength.titleOfSelectedItem
        
        print("SelectedOption: \(SelectedOption!)")
        
        if (SelectedOption == "Choose One") {
            if (!CharacterHairLengthOtherTextField.isHidden) {
                CharacterHairLengthOtherTextField.stringValue = ""
                CharacterHairLengthOtherTextField.isEnabled = false
                CharacterHairLengthOtherTextField.isHidden = true
            }
        } else if (SelectedOption == "Cancel") {
            if (!CharacterHairLengthOtherTextField.isHidden) {
                CharacterHairLengthOtherTextField.stringValue = ""
                CharacterHairLengthOtherTextField.isEnabled = false
                CharacterHairLengthOtherTextField.isHidden = true
            }
            CharacterHairLength.selectItem(withTitle: "Choose One")
        } else if (SelectedOption == "Other") {
            if (CharacterHairLengthOtherTextField.isHidden) {
                CharacterHairLengthOtherTextField.becomeFirstResponder()
                CharacterHairLengthOtherTextField.stringValue = ""
                CharacterHairLengthOtherTextField.isEnabled = true
                CharacterHairLengthOtherTextField.isHidden = false
            }
        } else {
            if (!CharacterHairLengthOtherTextField.isHidden) {
                CharacterHairLengthOtherTextField.stringValue = ""
                CharacterHairLengthOtherTextField.isEnabled = false
                CharacterHairLengthOtherTextField.isHidden = true
            }
        }
        
    }// end of CharacterLengthPopUpAction
    
    
    
    func FacialHair() {
        
        if (CharacterHairSideBurnsCheckBox.state == .on) {
            FacialHairStringArray.append("SideBurns")
        }
        if (CharacterHairFullBeardCheckBox.state == .on) {
            FacialHairStringArray.append("FullBeard")
        }
        if (CharacterHairGoateeCheckBox.state == .on) {
            FacialHairStringArray.append("Goatee")
        }
        if (CharacterHairMoustacheCheckBox.state == .on) {
            FacialHairStringArray.append("Moustache")
        }
        if (CharacterHairSoulPatchCheckBox.state == .on) {
            FacialHairStringArray.append("SoulPatch")
        }
        if (CharacterHairCleanShavenCheckBox.state == .on) {
            FacialHairStringArray.append("CleanShaven")
        }
        
        if (CharacterHairOtherCheckBox.state == .on) {
            
            FacialHairStringArray.append("Other")
            FacialHairOtherStringToBD = CharacterHairOtherTextField.stringValue
        } else {
            FacialHairOtherStringToBD = ""
        }
        
        /////////////////////////
        
        print("FacialHairStringArray:\n\(FacialHairStringArray)")
        
        FacialHairStringToBD = String(FacialHairStringArray.joined(separator: ","))
        
        print("FacialHairStringToBD:\n\(FacialHairStringToBD)")
        
        
        
        
//        if (FacialHairStringArray.contains("SideBurns")) {
//            FacialHairStringToBD = "SideBurns,"
//        }
        
        
        
    }
    
    
    
    
    @objc func SaveInfo() {
        
        //print("CharacterHairCutVC SavInfo() Ran")
        
        //////////////////
        
        var CHairCut = String()
        var CHairPart = String()
        var CHairLength = String()
        
        
        
        
        print("CharacterHairCut.titleOfSelectedItem: \"\(CharacterHairCut.titleOfSelectedItem ?? "BLANK")\"")
        
        
        if (CharacterHairCut.titleOfSelectedItem == "Other") {
            CHairCut = CharacterHairCutOtherTextField.stringValue
            print("CHairCut: \"\(CHairCut)\"")
        }
//        else if (CharacterHairCut.titleOfSelectedItem == "Choose One") {
//            CHairCut = ""
//        }
        else {
            CHairCut = CharacterHairCut.titleOfSelectedItem!
        }
        
        
        
        if (CharacterHairPart.titleOfSelectedItem == "Other") {
            
            CHairPart = CharacterHairPartOtherTextField.stringValue
        }
        //else if (CharacterHairPart.titleOfSelectedItem == "Choose One") {
//            CHairPart = ""
//        }
            else {
            CHairPart = CharacterHairPart.titleOfSelectedItem!
        }
        
        
        
        if (CharacterHairLength.titleOfSelectedItem == "Other") {
            CHairLength = CharacterHairLengthOtherTextField.stringValue
        }
            //else if (CharacterHairLength.titleOfSelectedItem == "Choose One") {
//            CHairLength = ""
        //}
        else {
            CHairLength = CharacterHairLength.titleOfSelectedItem!
        }
        
        print("CHairCut: \"\(CHairCut)\" CHairPart: \"\(CHairPart)\" CHairLength: \"\(CHairLength)\"")
        
        //////////////////
        
//        var CHairOtherCheckBoxText = ""
//        var CHairOtherCheckBox = ""
//
//        if (CharacterHairOtherCheckBox.state == .on) {
//            CHairOtherCheckBox = "Other"
//            CHairOtherCheckBoxText = CharacterHairOtherTextField.stringValue
//        }
        
        /////////////////////
        
        
        // sets up what checked checkboxes to save to the DB
        FacialHair()
        
        
        /////////////////////
        
        var HairInfoSaveToDB = String()

        do {

            HairInfoSaveToDB = "UPDATE charactertable set haircut = '\(CHairCut)', hairpart = '\(CHairPart)', hairlength = '\(CHairLength)', facialhair = '\(FacialHairStringToBD)', facialhairother = '\(FacialHairOtherStringToBD)' WHERE ikey = \(CVCIkey) and userikeyreference = \(CurrentUserIkey!)"

            print("HairInfoSaveToDB: \(HairInfoSaveToDB)")

            try db.executeUpdate(HairInfoSaveToDB, values: nil)

        } catch let error as NSError {

            let ErrorMessage = "\(#function) - Failed to do query \(HairInfoSaveToDB)\nError: \(error.localizedDescription)"

            print(ErrorMessage)

            loggly(LogType.Error, text: ErrorMessage)

            DoAlert.DisplayAlert(Class: "CharacterHairCutVC", Level: 3, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to update Character Hair Information\nPlease quit then try again.\n\n\(HairInfoSaveToDB)", DatabaseError: "\(db.lastErrorMessage())")

        }
        
        //////////////////
        
        NotificationCenter.default.removeObserver(NSPopover.didCloseNotification)
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateHairInfo"), object: nil)
        print("UpdateHairInfo notification sent")
    }
    
    
    
    
    
    
    
    
    
    
    
}// end of file
