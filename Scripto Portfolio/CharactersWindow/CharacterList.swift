//
//  CharacterList.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 3/3/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Foundation

class CharacterList: NSObject {
    var Ikey: String
    var FName: String
    var Birthdate: String
    
    
    init(Ikey: String, FName: String, Birthdate: String) {
        self.Ikey = Ikey
        self.FName = FName
        self.Birthdate = Birthdate
    }
        
}
