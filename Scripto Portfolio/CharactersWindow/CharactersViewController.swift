//
//  CharactersViewController.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 2/14/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Cocoa
import SwiftLoggly

class CharactersViewController: NSViewController, NSTableViewDataSource, NSTableViewDelegate {
    
    
    
    @IBOutlet weak var CharacterListSearch: NSSearchField!
    @IBOutlet weak var CharacterListTableView: NSTableView!
    @IBOutlet weak var CharacterAddButton: NSButton!
    @IBOutlet weak var CharacterRemoveButton: NSButton!
    @IBOutlet weak var CharacterCloseButton: NSButton!
    @IBOutlet weak var CharcterOpenCharacterButton: NSButton!
    
    var CurrentAppearanceCharacterViewController = String()
    
    var CharacterListArray: [CharacterList] = []
    
    private var BirthdateDB: String!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        

        
        CharacterListTableView.delegate = self
        CharacterListTableView.dataSource = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(CharacterListTableData), name: NSNotification.Name(rawValue: "CharacterListTableData"), object: nil)
    
        
        
    }
    
    override func viewWillAppear() {
        
       
        
        CharacterListTableView.tableColumn(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "ikey"))?.isHidden = true
        CharacterListTableData()
        
        let DoubleSelect: Selector = #selector(CharactersViewController.CharacterOpenCharacterButton(_:))
        CharacterListTableView.doubleAction = DoubleSelect
        
    }
    
    
    override func viewDidLayout() {
        
        //        enum InterfaceStyle : String {
        //            case Dark, Light
        //
        //            init() {
        //                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
        //                self = InterfaceStyle(rawValue: type)!
        //            }
        //        }
        //
        //        let currentStyle = InterfaceStyle()
        //
        //        //print("currentStyle: \"\(currentStyle)\"  -  CurrentAppearance: \"\(CurrentAppearance)\" ")
        //
        //        if ("\(currentStyle)" != CurrentAppearanceNewUserViewController) {
        //            appearanceCheck()
        //        }
        
        appearanceCheck()
        
        super.viewDidLayout()
        
        print("NewUserViewController - viewDidLayout finished")
        
        
    }// end of viewDidLayout()
    
    
    func appearanceCheck() {
        
        enum InterfaceStyle : String {
            case Dark, Light
            
            init() {
                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
                self = InterfaceStyle(rawValue: type)!
            }
        }
        
        let currentStyle = InterfaceStyle()
        
        if ("\(currentStyle)" != CurrentAppearanceCharacterViewController) {
            
            if (currentStyle.rawValue == "Dark") {
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColorDarkMode
                
                CharacterCloseButton.styleButtonText(button: CharacterCloseButton, buttonName: "Close", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                CharacterCloseButton.wantsLayer = true
                CharacterCloseButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                CharacterCloseButton.layer?.cornerRadius = 7
                
                CharcterOpenCharacterButton.styleButtonText(button: CharcterOpenCharacterButton, buttonName: "Open Character", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                CharcterOpenCharacterButton.wantsLayer = true
                CharcterOpenCharacterButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                CharcterOpenCharacterButton.layer?.cornerRadius = 7
                
                
                
            } else if (currentStyle.rawValue == "Light") {
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColor.cgColor
                
                CharacterCloseButton.styleButtonText(button: CharacterCloseButton, buttonName: "Close", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                CharacterCloseButton.wantsLayer = true
                CharacterCloseButton.layer?.backgroundColor = NSColor.gray.cgColor
                CharacterCloseButton.layer?.cornerRadius = 7
                
                CharcterOpenCharacterButton.styleButtonText(button: CharcterOpenCharacterButton, buttonName: "Open Character", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                CharcterOpenCharacterButton.wantsLayer = true
                CharcterOpenCharacterButton.layer?.backgroundColor = NSColor.gray.cgColor
                CharcterOpenCharacterButton.layer?.cornerRadius = 7
                
            }
        }
        
        CurrentAppearanceCharacterViewController = "\(currentStyle)"
        
        
    }//end of appearanceCheck()
    
    
    
    
    
    
    
    ////////////////////////////////////////////////
    
    @objc func CharacterListTableData() {
        
        CharacterListArray.removeAll()
        
        var IkeyDB: String!
        var FNameDB: String!
        var MNameDB: String!
        var LNameDB: String!
        
        db.open()
        
        
        let Query = "select * from charactertable where userikeyreference = \"\(CurrentUserIkey!)\""
        
        //print("Query 1: \(Query)")
        
//        if (SearchBool == true) {
//            Query = "select * from locationtable where userikeyreference = \"\(CurrentUserIkey!)\" and name LIKE '%\(LocationSearchTextField.stringValue)%' OR location LIKE '%\(LocationSearchTextField.stringValue)%' "
//
//            //print("Query 2: \(Query)")
//
//        }
        

        
        do {
            
            let CharacterQueryRun = try db.executeQuery(Query, values:nil)

            while CharacterQueryRun.next() {
                IkeyDB = CharacterQueryRun.string(forColumn: "ikey")
                FNameDB = CharacterQueryRun.string(forColumn: "firstname")
                MNameDB = CharacterQueryRun.string(forColumn: "middlename")
                LNameDB = CharacterQueryRun.string(forColumn: "lastname")
                BirthdateDB = CharacterQueryRun.string(forColumn: "birthdate")
                
                
                print("Ikey: \(IkeyDB!) - Name: \(FNameDB!) \(MNameDB!) \(LNameDB!) - BirthdateDB: \(BirthdateDB!)")
          
                var FullName = String()
                
                if (MNameDB == "") {
                    FullName = "\(FNameDB!) \(LNameDB!)"
                } else {
                    FullName = "\(FNameDB!) \(MNameDB!) \(LNameDB!)"
                }
                
                
                print("FullName \(FullName)")
                
                //////////////////
                // Age Calculation
                
                let dateFormatterGet = DateFormatter()
                dateFormatterGet.dateFormat = "YYYY-MM-DD HH:MM:SS.SSS"
                
                let TodaysDate = NSDate()
                
                var Bdate: NSDate? = dateFormatterGet.date(from: BirthdateDB) as NSDate?
                
                
                
                if (Bdate == nil) {
                    
                    let dateFormatterGet2 = DateFormatter()
                    dateFormatterGet2.dateFormat = "MMMM d yyyy"
                    Bdate = dateFormatterGet2.date(from: BirthdateDB) as NSDate?
                    
                    
                }
                
                
                
                
                
                
                let calendar = Calendar.current
                let components = calendar.dateComponents([.year], from: Bdate! as Date ,to: TodaysDate as Date)
                
                let ageInt = components.year
                
                //print("ageInt: \(ageInt!)")
                
                /////////////////
                
                let CharactersToAdd = CharacterList.init(Ikey: IkeyDB, FName: FullName, Birthdate: "\(ageInt!)")
                
                
                CharacterListArray.append(CharactersToAdd)
                
            }
            
        } catch {
            
            print("ERROR: CharactersViewController: Failed to get \"\(CurrentUserName!)\"'s Characters\n\(Query)\n\(db.lastErrorMessage())")
            
            let ErrorMessage = "CharactersViewController:\(#function) - Failed to get \"\(CurrentUserName!)\"'s Characters\n\(db.lastErrorMessage())\n\(Query))"
            
            print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "CharactersViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to Failed to get \"\(CurrentUserName!)\"'s Characters\n\n\(Query)", DatabaseError: "\(db.lastErrorMessage())")
            
        }
        

        
        CharacterListTableView.reloadData()
        
    }
    
    
    
    func numberOfRows(in tableVieww: NSTableView) -> Int {
        print("CharacterListArray Count: \(CharacterListArray.count)")
        return CharacterListArray.count
        
    }
    
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        
        guard let cell = tableView.makeView(withIdentifier: tableColumn!.identifier, owner: self) as? NSTableCellView else
        {
            print("Table Returned Nil")
            return nil
        }
        
        
        if (tableColumn?.identifier)!.rawValue == "ikey" {
            cell.textField?.stringValue = CharacterListArray[row].Ikey
        }
        if (tableColumn?.identifier)!.rawValue == "name" {
            cell.textField?.stringValue = CharacterListArray[row].FName
        }
        if (tableColumn?.identifier)!.rawValue == "birthdate" {
            cell.textField?.stringValue = CharacterListArray[row].Birthdate
        }
        
        
        
        return cell
        
    }
    
    func tableViewSelectionDidChange(_ notification: Notification) {
        
        let SelectedRow = CharacterListTableView.selectedRow
        
        print("SelectedRow: \(SelectedRow)")
        
        if (SelectedRow == -1) {
            
            CharcterOpenCharacterButton.isEnabled = false
            CharacterRemoveButton.isEnabled = false
            return
        }
        
        let FName = CharacterListArray[SelectedRow].FName
        let Ikey = CharacterListArray[SelectedRow].Ikey
        let Birthdate = CharacterListArray[SelectedRow].Birthdate
        
        print("Table Selection - Ikey: \"\(Ikey)\" Name: \"\(FName)\" Birthdate: \"\(Birthdate)\"")
        
        let isIndexValid = CharacterListArray.indices.contains(SelectedRow)
        
        if isIndexValid {
            CharcterOpenCharacterButton.isEnabled = true
            CharacterRemoveButton.isEnabled = true
        } else {
            CharcterOpenCharacterButton.isEnabled = false
            CharacterRemoveButton.isEnabled = false
        }
        
        
    }
    
    
    
    
    ////////////////////////////////////////////////
    
    
    @IBAction func CharacterOpenCharacterButton(_ sender: Any) {
        print("CharacterOpenCharacterButton Clicked")
        
        OpenWindows.CheckForOpenWindows()

        let SelectedRow = self.CharacterListTableView.selectedRow

        print("SelectedRow: \(SelectedRow)")
        
        if (SelectedRow == -1) {
            return
        }

        let FName = self.CharacterListArray[SelectedRow].FName
        let Ikey = self.CharacterListArray[SelectedRow].Ikey
        //let Birthdate = CharacterListArray[SelectedRow].Birthdate

        //print("Table Selection - Ikey: \"\(Ikey)\" Name: \"\(FName)\" Birthdate: \"\(Birthdate)\"")
        print("Table Selection - Ikey: \"\(Ikey)\" Name: \"\(FName)\" Birthdate: \"\(self.BirthdateDB!)\"")

        /////////////

        let delimiter = " "
        let stringInfo = FName.components(separatedBy: delimiter)

        let FirstName = stringInfo[0]

        /////////////
        CVCIkey = Ikey
        CVCName = FirstName
        CVCBirthdate = BirthdateDB
        
        CharacterInfoWindow.showWindow(self)
        
    }
    
    
    @IBAction func CharacterAddCharacterButton(_ sender: Any) {
        print("CharacterAddCharacterButton Clicked")
        CharacterInfoWindow.showWindow(self)
    }
    
    @IBAction func CharacterRemoveCharacterButton(_ sender: Any) {
        print("CharacterRemoveCharacterButton Clicked")
        
        let SelectedRow = CharacterListTableView.selectedRow
        
        if (SelectedRow == -1) {
            return
        }
        
        
        
        let Name = CharacterListArray[SelectedRow].FName
        let Ikey = CharacterListArray[SelectedRow].Ikey
        
        let alert = NSAlert()
        alert.messageText = "Are you sure you want to Delete \"\(Name)\""
        alert.informativeText = "This action cannot be undone."
        alert.alertStyle = .warning
        alert.addButton(withTitle: "Canel")
        alert.addButton(withTitle: "Yes Delete")
        
        if (CurrentAppearanceCharacterViewController == "Dark") {
            alert.window.backgroundColor = AlertBackgroundColorDarkMode
        } else {
            alert.window.backgroundColor = WindowBackgroundColor
        }
        
        alert.window.titlebarAppearsTransparent = true
        
        let returnCode = alert.runModal()
        print("returnCode: \(returnCode)")
        
        if (returnCode.rawValue == 1000) {
            print("Canel Pressed")
        }
        if (returnCode.rawValue == 1001) {
            
            print("Yes Delete Pressed")
            
            let DeleteCharacterQuery = "DELETE FROM charactertable WHERE ikey = \"\(Ikey)\" and userikeyreference = \"\(CurrentUserIkey!)\" "
            
            do {
                
                try db.executeUpdate(DeleteCharacterQuery, values:nil)
                
            } catch {
                
                let ErrorMessage = "CharacterViewController:\(#function) - Failed to Delete \"\(Name)\"'s Character\n\(DeleteCharacterQuery)\n\(db.lastErrorMessage())\n\(DeleteCharacterQuery))"
                
                print(ErrorMessage)
                
                loggly(LogType.Error, text: ErrorMessage)
                
                DoAlert.DisplayAlert(Class: "CharacterViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to Delete \"\(Name)\"'s Character\n\n\(DeleteCharacterQuery)", DatabaseError: "\(db.lastErrorMessage())")
                
            }
            
            CharacterListTableData()
            
            
        } // end of 1000
        
    }
    
    @IBAction func CharacterCloseButton(_ sender: Any) {
        print("CharacterSelectAStoryButton Clicked")
        self.view.window?.close()
        
//        OpenWindows.CheckForOpenWindows()
        
        if ((!(CurrentlyOpenWindows.contains(CharacterWindow))) && (CurrentlyOpenWindows.contains(CharacterInfoWindow))) {
            
            let alert = NSAlert()
            alert.messageText = "The Character \"\(CharacterInfoWindow.window?.title ?? "UNKNOWN")\" is currently open"
            alert.informativeText = "Do you want to continue closing the Character ?"
            alert.alertStyle = .warning
            alert.window.titlebarAppearsTransparent = true
            
            if (CurrentAppearanceCharacterViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.addButton(withTitle: "Cancel")
            alert.addButton(withTitle: "Close")
            
            let returnCode = alert.runModal()
            print("returnCode: \(returnCode)")
            
            if (returnCode.rawValue == 1000) {
                print("Cancel Pressed")
                CharacterWindow.showWindow(self)
                return
            }
            if (returnCode.rawValue == 1000) {
                print("Close Pressed")
                CharacterInfoWindow.close()
                if (StoryWindow.window?.isVisible == true) {
                    StoryWindow.window?.makeKeyAndOrderFront(nil)
                } else {
                    StoryListWindow.showWindow(self)
                }
                
            }
            
            
        }

        
        

    }
    
    
    func delayWithSeconds(_ seconds: Double, completion: @escaping () -> ()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            completion()
        }
    }
    
    
    
    
    
}


