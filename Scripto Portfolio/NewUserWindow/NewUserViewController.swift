//
//  NewUserViewController.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 1/19/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Cocoa
import AppKit


class NewUserViewController: NSViewController,NSTextFieldDelegate {

    @IBOutlet weak var createUserButton: NSButton!
    @IBOutlet weak var cancelButton: NSButton!
    
    
    
    @IBOutlet weak var userNameLabel: NSTextField!
    @IBOutlet weak var passwordLabel: NSTextField!
    @IBOutlet weak var confirmLabel: NSTextField!
    @IBOutlet weak var fourDigitPinLabel: NSTextField!
    @IBOutlet weak var birthdateLabel: NSTextField!
    @IBOutlet weak var colorPickerLabel: NSTextField!
    
    
    @IBOutlet weak var fourDigitPinInfoIconButton: NSButton!
    @IBOutlet weak var birthdateInfoIconButton: NSButton!
    
    @IBOutlet weak var newUserNameTextField: NSTextField!
    @IBOutlet weak var newUserPasswordTextfield: NSSecureTextField!
    @IBOutlet weak var newUserConfirmPasswordTextField: NSSecureTextField!
    @IBOutlet weak var newUser4DigitPinTextField: NSSecureTextField!
    
    @IBOutlet weak var newUserDatePicker: NSDatePicker!
    
    @IBOutlet weak var newUserColorPickerColor: NSColorWell!
    
    @IBOutlet weak var superUserCheckBox: NSButton!
    
    var CurrentAppearanceNewUserViewController = String()
    
    
    //  https://crunchybagel.com/working-with-hex-colors-in-swift-3/
    
    
    // Versioning 
    //https://github.com/AlexanderNey/SemanticVersioning
    
    
    let date = NSDate()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        

        
        
        
        
        newUser4DigitPinTextField.delegate = self
        
    }
    
    override func viewWillAppear() {
        
        view.wantsLayer = true
        
        
    }
    
    override func viewDidLayout() {
        
//        enum InterfaceStyle : String {
//            case Dark, Light
//
//            init() {
//                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
//                self = InterfaceStyle(rawValue: type)!
//            }
//        }
//
//        let currentStyle = InterfaceStyle()
//
//        //print("currentStyle: \"\(currentStyle)\"  -  CurrentAppearance: \"\(CurrentAppearance)\" ")
//
//        if ("\(currentStyle)" != CurrentAppearanceNewUserViewController) {
//            appearanceCheck()
//        }
        
        appearanceCheck()
        
        super.viewDidLayout()
        
       
        
        
    }// end of viewDidLayout()
    
    
    func appearanceCheck() {

        enum InterfaceStyle : String {
            case Dark, Light
            
            init() {
                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
                self = InterfaceStyle(rawValue: type)!
            }
        }
        
        let currentStyle = InterfaceStyle()
        
        if ("\(currentStyle)" != CurrentAppearanceNewUserViewController) {

            if (currentStyle.rawValue == "Dark") {
                
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColorDarkMode
                
                createUserButton.styleButtonText(button: createUserButton, buttonName: "Create User", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                cancelButton.styleButtonText(button: cancelButton, buttonName: "Cancel", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                createUserButton.wantsLayer = true
                createUserButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                createUserButton.layer?.cornerRadius = 7
                
                cancelButton.wantsLayer = true
                cancelButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                cancelButton.layer?.cornerRadius = 7
                
                userNameLabel.font = NSFont(name:AppFontBold, size: 13.0)
                userNameLabel.textColor = NSColor.white
                
                passwordLabel.font = NSFont(name:AppFontBold, size: 13.0)
                passwordLabel.textColor = NSColor.white
                
                confirmLabel.font = NSFont(name:AppFontBold, size: 13.0)
                confirmLabel.textColor = NSColor.white
                
                fourDigitPinLabel.font = NSFont(name:AppFontBold, size: 13.0)
                fourDigitPinLabel.textColor = NSColor.white
                
                birthdateLabel.font = NSFont(name:AppFontBold, size: 13.0)
                birthdateLabel.textColor = NSColor.white
                
                colorPickerLabel.font = NSFont(name:AppFontBold, size: 13.0)
                colorPickerLabel.textColor = NSColor.white
                
                
                
            } else if (currentStyle.rawValue == "Light") {
               
                
     
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColor.cgColor
                
                createUserButton.styleButtonText(button: createUserButton, buttonName: "Create User", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                cancelButton.styleButtonText(button: cancelButton, buttonName: "Cancel", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                createUserButton.wantsLayer = true
                createUserButton.layer?.backgroundColor = NSColor.gray.cgColor
                createUserButton.layer?.cornerRadius = 7
                
                cancelButton.wantsLayer = true
                cancelButton.layer?.backgroundColor = NSColor.gray.cgColor
                cancelButton.layer?.cornerRadius = 7
                
                userNameLabel.font = NSFont(name:AppFontBold, size: 13.0)
                userNameLabel.textColor = NSColor.white
                
                passwordLabel.font = NSFont(name:AppFontBold, size: 13.0)
                passwordLabel.textColor = NSColor.white
                
                confirmLabel.font = NSFont(name:AppFontBold, size: 13.0)
                confirmLabel.textColor = NSColor.white
                
                fourDigitPinLabel.font = NSFont(name:AppFontBold, size: 13.0)
                fourDigitPinLabel.textColor = NSColor.white
                
                birthdateLabel.font = NSFont(name:AppFontBold, size: 13.0)
                birthdateLabel.textColor = NSColor.white
                
                colorPickerLabel.font = NSFont(name:AppFontBold, size: 13.0)
                colorPickerLabel.textColor = NSColor.white

            }
        }
        
        CurrentAppearanceNewUserViewController = "\(currentStyle)"
        
        
    }//end of appearanceCheck()
    
    
    
    
    
    
    
    
    
    func RemoveWhiteSpace(aString: String) -> String {
        let replaced = aString.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        return replaced
    }
    
    override func controlTextDidChange(_ obj: Notification) {
        
        //print("controlTextDidChange Ran")
        
        if (self.newUser4DigitPinTextField.stringValue.count < 1) {
        
        } else {
        
            let lastCharacter = String(describing: self.newUser4DigitPinTextField.stringValue.last!)
           
            //print("lastCharacter: \(lastCharacter)")
            
            let SymbolTest = CharacterSet.symbols.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
            
            let punctuationTest = CharacterSet.punctuationCharacters.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
            
            let letterTest = CharacterSet.letters.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
            
            let SpaceNewLineTest = CharacterSet.whitespacesAndNewlines.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
            
            if (SymbolTest || punctuationTest || letterTest || SpaceNewLineTest) {
                
//                print("SymbolTest: \(SymbolTest)")
//                print("punctuationTest: \(punctuationTest)")
//                print("letterTest: \(letterTest)")
//                print("SpaceNewLineTest: \(SpaceNewLineTest)")
                
                __NSBeep()
                
                self.newUser4DigitPinTextField.stringValue = String(self.newUser4DigitPinTextField.stringValue.dropLast())
                
                return
            }
            
            
            let Numbers = [0,1,2,3,4,5,6,7,8,9]
     
            if (!Numbers.contains(Int(lastCharacter)!)) {
                __NSBeep()
                
                self.newUser4DigitPinTextField.stringValue = String(self.newUser4DigitPinTextField.stringValue.dropLast())
            }

            if (self.newUser4DigitPinTextField.stringValue.count > 4) {
                __NSBeep()

                self.newUser4DigitPinTextField.stringValue = String(self.newUser4DigitPinTextField.stringValue.dropLast())
                
            }
        }
        
    }
    
    
    
    
    @IBAction func createUserButtonAction(_ sender: Any) {
        
        print("createUserButtonAction Pressed")
        
        let userNameLabelWhiteSpace = RemoveWhiteSpace(aString: newUserNameTextField.stringValue)
    
        let passwordLabelWhiteSpace = RemoveWhiteSpace(aString: newUserPasswordTextfield.stringValue)
        
        let confirmLabelWhiteSpace = RemoveWhiteSpace(aString: newUserConfirmPasswordTextField.stringValue)
        
        let newUser4Ditigs = newUser4DigitPinTextField.intValue
        
        let birthdatePicker = newUserDatePicker.dateValue
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        
        let dateString = dateFormatter.string(from: birthdatePicker)
        
        let newUserColor = newUserColorPickerColor.color.toHexString
        
        let superUser = Float((superUserCheckBox.state).rawValue)
        
        
        ///////////////////////////////
        
        print("userNameLabelWhiteSpace: \"\(userNameLabelWhiteSpace)\"")
        
        print("passwordLabelWhiteSpace: \"\(passwordLabelWhiteSpace)\"")
        
        print("confirmLabelWhiteSpace: \"\(confirmLabelWhiteSpace)\"")
        
        print("newUser4DigitPinTextField: \"\(newUser4Ditigs)\"")
        
        print("DatePicker dateString: \(dateString)")
        
        print("newUserColor: \(newUserColor)")
        
        ///////////////////////////////

        if ((userNameLabelWhiteSpace == "") || (passwordLabelWhiteSpace == "") || (confirmLabelWhiteSpace == "") || (newUser4DigitPinTextField.stringValue == "")) {

            DoAlert.DisplayAlert(Class: "ViewController", Level: 3, Function: "\(#function)", MessageText: "The Username, Password, Confirm or 4 digit pin is blank.", InformativeText: "Please enter the appropriate information", DatabaseError: "")
            
            return


        } else if (passwordLabelWhiteSpace != confirmLabelWhiteSpace){

            DoAlert.DisplayAlert(Class: "ViewController", Level: 3, Function: "\(#function)", MessageText: "Password does not match Confirm password", InformativeText: "Please try again", DatabaseError: "")
            
            return
            


        }
        
        if (dateString <= "01/01/1900") {  //  (dateString == "01/01/1900")
            
            DoAlert.DisplayAlert(Class: "ViewController", Level: 3, Function: "\(#function)", MessageText: "Wow your old!!", InformativeText: "Please enter your birthday.\nRemember this information is only used for password changes, it does not leave the database.", DatabaseError: "")
            
            
            return

            
        }
        
        
        
        //////////////////////////////////////

        ConnectionTest.CheckConnection()

        // Check Database for database to see if username already exists
        
        let sqlString = "SELECT count(user) as count FROM users WHERE user = \"\(userNameLabelWhiteSpace)\""
        print("sqlString: \(sqlString)")

        var NumCount:Int32 = 0
        
        do {
            let result = try db.executeQuery(sqlString, values: nil)
            
            while result.next()
            {
                NumCount = result.int(forColumn: "count")
                print("NumCount: \(NumCount)")
            }
            
            result.close()
            
            if (NumCount > 0) {
                
                DoAlert.DisplayAlert(Class: "ViewController", Level: 2, Function: "\(#function)", MessageText: "Warning", InformativeText: "That Username Already Exists\nPlease Try Again.", DatabaseError: "")
                
                
                return

                
            }
            
        } catch let error as NSError {
            
            let ErrorMessage = "\(#function) - Failed to do query \(sqlString)\nError: \(error.localizedDescription)"
            
            print(ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "ViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to check the Scripto Portfolio database for username.\nPlease quit then try again.", DatabaseError: "\(db.lastErrorMessage())")
            

            
            return
        }
        
        
        let encryptedPassword = passwordLabelWhiteSpace.base64Encoded()
        
        //print("encryptedPassword: \"\(encryptedPassword!)\" ")
        
        //print("decryptedPassword: \"\(encryptedPassword?.base64Decoded()!)\" ")
        
        
        
        
        do {
  
           
            
            let InsertNewUserSqlquery = String(format: "INSERT INTO users (ikey,user,password,birthdate,usercolor,pin,isAdmin) VALUES ((select MAX(ikey)+ 1 from users),\"\(userNameLabelWhiteSpace)\",\"\(encryptedPassword!)\",\"\(dateString)\",\"\(newUserColor)\",\"\(newUser4Ditigs)\",\"\(superUser)\")")
            
            print("InsertNewUserSqlquery: \(InsertNewUserSqlquery)")
            
            
            
            try db.executeUpdate(InsertNewUserSqlquery, values: nil)

        } catch let error as NSError {
            
            let ErrorMessage = "\(#function) - Failed to do query \(sqlString)\nError: \(error.localizedDescription)"
            
            print(ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "ViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to check the Scripto Portfolio database for username.\nPlease quit then try again.", DatabaseError: "\(db.lastErrorMessage())")
            
            let alert = NSAlert()
            alert.messageText = "Warning"
            alert.informativeText = "Failed to add new user to the Scripto Portfolio database.\nPlease quit then try again."
            alert.alertStyle = .warning
            alert.addButton(withTitle: "OK")
            
            if (CurrentAppearanceNewUserViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            
            
            alert.window.titlebarAppearsTransparent = true
            
            if alert.runModal() == .alertFirstButtonReturn {
                print("Ok Pressed")
                return
            }
            
            return
            
        }
        
        
        let alert = NSAlert()
        alert.messageText = "User was added Sucessfully"
        alert.informativeText = "You can now login with the new user \(userNameLabelWhiteSpace)"
        alert.alertStyle = .warning
        alert.addButton(withTitle: "OK")
        
        if (CurrentAppearanceNewUserViewController == "Dark") {
            alert.window.backgroundColor = AlertBackgroundColorDarkMode
        } else {
            alert.window.backgroundColor = WindowBackgroundColor
        }
        
        alert.window.titlebarAppearsTransparent = true
        
        if alert.runModal() == .alertFirstButtonReturn {
            print("Ok Pressed")
            self.view.window?.close()
            LoginWindow.window?.makeKeyAndOrderFront(self)
        }
        
        

    }
    
    @IBAction func fourDigitPinIconInfoButton(_ sender: Any) {

        let popover = NSPopover()
        popover.contentViewController = NewUserWindowFourDigitPopover
        popover.behavior = .transient
        popover.show(relativeTo: fourDigitPinInfoIconButton.bounds, of: fourDigitPinInfoIconButton, preferredEdge: .maxX)
        
    }
    
    @IBAction func birthdateIconInfoButton(_ sender: Any) {

        let popover = NSPopover()
        popover.contentViewController = NewUserWindowBirthdatePopover
        popover.behavior = .transient
        popover.show(relativeTo: birthdateInfoIconButton.bounds, of: birthdateInfoIconButton, preferredEdge: .maxX)
        
    }
    
    @IBAction func cancelButton(_ sender: Any) {
        
        self.view.window?.close()
        LoginWindow.window?.makeKeyAndOrderFront(self)
        
    }
    
    
    
    
    
    
    
    
    
    
    //////////////////////////////
    
    
//    do {
//    let result = try db.executeQuery(sqlString, values: nil)
//
//    while result.next()
//    {
//    let ikey = result.string(forColumn: "ikey")
//    let user = result.string(forColumn: "user")
//    let password = result.string(forColumn: "password")
//    print("ikey = \(ikey!)\nuser = \(user!)\npassword = \(password!)")
//
//    }
//
//    result.close()
//
//
//
//    } catch let error as NSError {
//    print("Failed to do query \(sqlString)\nError: \(error.localizedDescription)")
//    }
    
    
    

    
    
}


extension NSColor {
    var toHexString: String {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        
        self.getRed(&r, green: &g, blue: &b, alpha: &a)
        
        return String(
            format: "%02X%02X%02X",
            Int(r * 0xff),
            Int(g * 0xff),
            Int(b * 0xff)
        )
    }
}

