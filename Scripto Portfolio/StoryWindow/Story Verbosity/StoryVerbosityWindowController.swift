//
//  StoryVerbosityWindowController.swift
//  Scripto Portfolio
//
//  Created by Dorian Glanville on 5/20/19.
//  Copyright © 2019 DTGM. All rights reserved.
//

import Cocoa

class StoryVerbosityWindowController: NSWindowController {

    override func windowDidLoad() {
        super.windowDidLoad()

        
        window?.titlebarAppearsTransparent = true
        window?.titleVisibility = .hidden
    }

}
