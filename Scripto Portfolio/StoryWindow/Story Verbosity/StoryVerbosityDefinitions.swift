//
//  StoryVerbosityDefinitions.swift
//  Scripto Portfolio
//
//  Created by Dorian Glanville on 6/30/19.
//  Copyright © 2019 DTGM. All rights reserved.
//

import Foundation


class StoryVerbosityDefinitions: NSObject {
    
    var PartOfSpeech: String
    var Definition: String
    
    
    init(PartOfSpeech: String, Definition: String ) {
        self.PartOfSpeech = PartOfSpeech
        self.Definition = Definition
        
    }
    
}
