//
//  StoryVerbosityViewController.swift
//  Scripto Portfolio
//
//  Created by Dorian Glanville on 5/20/19.
//  Copyright © 2019 DTGM. All rights reserved.
//

import Cocoa
import Alamofire

class StoryVerbosityViewController: NSViewController, NSTableViewDelegate, NSTableViewDataSource {
    
    
    
    @IBOutlet weak var definitionSearchButton: NSButton!
    @IBOutlet weak var definitionSearchTextField: NSTextField!
    @IBOutlet weak var storyVerbosityTable: NSTableView!
    
    @IBOutlet weak var StoryVerbosityTabView: NSTabView!
    
    @IBOutlet weak var StoryVerbosity_DefintionsView: NSTabViewItem!
    @IBOutlet weak var StoryVerbosity_Synonyms: NSTabViewItem!
    @IBOutlet weak var StoryVerbosity_SpelledLike: NSTabViewItem!
    
    
    
    var StoryDefsArray: [StoryVerbosityDefinitions] = []
    var StoryDefsArrayMain: [StoryVerbosityDefinitions] = []
    var StoryDefsArrayFiltered: [StoryVerbosityDefinitions] = []
    
    var StoryDefsNounArray: [StoryVerbosityDefinitions] = []
    var StoryDefsVerbArray: [StoryVerbosityDefinitions] = []
    var StoryDefsAdjectiveArray: [StoryVerbosityDefinitions] = []
    var StoryDefsAdverbArray: [StoryVerbosityDefinitions] = []
    var StoryDefsUnknownArray: [StoryVerbosityDefinitions] = []
    
    
    
    
    
    var filterOptionsArray = Array<String>()
    
    var SendString = ""
    
    var CurrentAppearanceStoryVerbosityViewController = String()
    
    
    @IBOutlet weak var nounCheckBox: NSButton!
    @IBOutlet weak var verbCheckBox: NSButton!
    @IBOutlet weak var adjectiveCheckBox: NSButton!
    @IBOutlet weak var adverbCheckBox: NSButton!
    @IBOutlet weak var unknownCheckBox: NSButton!
    
    @IBOutlet weak var StoryVerbosityTableSearch: NSSearchField!
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        
        StoryVerbosityTabView.removeTabViewItem(StoryVerbosity_Synonyms)
        StoryVerbosityTabView.removeTabViewItem(StoryVerbosity_SpelledLike)
        
        
    }
    
    override func viewDidLayout() {
        
        appearanceCheck()
        
        
        
        //////////////////////////////
        
        storyVerbosityTable.dataSource = self
        storyVerbosityTable.delegate = self
        
        
    }
    
    override func viewDidAppear() {
        InternetConnectionCheck()
    }
    
    
    
    func InternetConnectionCheck() {
        
        if Connectivity.isConnectedToInternet {
            
            //print("Yes! Internet is available.")
            
        } else {
            
            //print("NO Internet!!")
            
            let alert = NSAlert()
            alert.messageText = "No Internet ?"
            alert.informativeText = "It does not look like your connected to the Internet."
            alert.alertStyle = .warning
            alert.addButton(withTitle: "Ok")
            alert.addButton(withTitle: "Check again")
            
            print("CurrentAppearanceStoryVerbosityViewController")
            print(CurrentAppearanceStoryVerbosityViewController)
            
            if (CurrentAppearanceStoryVerbosityViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.window.titlebarAppearsTransparent = true
            
            let returnCode = alert.runModal()
            //print("returnCode: \(returnCode)")
            
            if (returnCode.rawValue == 1000) {
                print("OK Pressed")
                self.view.window?.close()
                
            }
            if (returnCode.rawValue == 1001) {
                print("Check again Pressed")
                InternetConnectionCheck()
            }
            
        }
        
    }// end of func InternetConnectionCheck()
    
    
    func appearanceCheck() {
        
        enum InterfaceStyle : String {
            case Dark, Light
            
            init() {
                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
                self = InterfaceStyle(rawValue: type)!
            }
        }
        
        let currentStyle = InterfaceStyle()
        
        if ("\(currentStyle)" != CurrentAppearanceStoryVerbosityViewController) {
            
            
            
            if (currentStyle.rawValue == "Dark") {
                
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColorDarkMode
                
                
                
                //                OpenItemButton.styleButtonText(button: OpenItemButton, buttonName: "Open", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                //
                //                OpenItemButton.wantsLayer = true
                //                OpenItemButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                //                OpenItemButton.layer?.cornerRadius = 7
                //
                //                ItemsCloseButton.styleButtonText(button: ItemsCloseButton, buttonName: "Close", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                //
                //                ItemsCloseButton.wantsLayer = true
                //                ItemsCloseButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                //                ItemsCloseButton.layer?.cornerRadius = 7
                //
                //                ItemsEditItemCategories.styleButtonText(button: ItemsEditItemCategories, buttonName: "Edit Item Categories", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                //
                //                ItemsEditItemCategories.wantsLayer = true
                //                ItemsEditItemCategories.layer?.backgroundColor = NSColor.lightGray.cgColor
                //                ItemsEditItemCategories.layer?.cornerRadius = 7
                //
                //                ItemCountLabel.font = NSFont(name:AppFontBold, size: 13.0)
                //                ItemCountLabel.textColor = NSColor.lightGray
                
                
                
            } else if (currentStyle.rawValue == "Light") {
                
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColor.cgColor
                
                //                OpenItemButton.styleButtonText(button: OpenItemButton, buttonName: "Open", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                //
                //                OpenItemButton.wantsLayer = true
                //                OpenItemButton.layer?.backgroundColor = NSColor.gray.cgColor
                //                OpenItemButton.layer?.cornerRadius = 7
                //
                //                ItemsCloseButton.styleButtonText(button: ItemsCloseButton, buttonName: "Close", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                //
                //                ItemsCloseButton.wantsLayer = true
                //                ItemsCloseButton.layer?.backgroundColor = NSColor.gray.cgColor
                //                ItemsCloseButton.layer?.cornerRadius = 7
                //
                //                ItemsEditItemCategories.styleButtonText(button: ItemsEditItemCategories, buttonName: "Edit Item Categories", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                //
                //                ItemsEditItemCategories.wantsLayer = true
                //                ItemsEditItemCategories.layer?.backgroundColor = NSColor.gray.cgColor
                //                ItemsEditItemCategories.layer?.cornerRadius = 7
                //
                //                ItemCountLabel.font = NSFont(name:AppFontBold, size: 13.0)
                //                ItemCountLabel.textColor = NSColor.white
                
            }
        }
        
        CurrentAppearanceStoryVerbosityViewController = "\(currentStyle)"
        
        
    }//end of appearanceCheck()
    
    
    
    
    
    
    
    
    
    func AlamoFireSearch() {
        
        
        
        //Alamofire.request(<#T##url: URLConvertible##URLConvertible#>, method: <#T##HTTPMethod#>, parameters: <#T##Parameters?#>, encoding: <#T##ParameterEncoding#>, headers: <#T##HTTPHeaders?#>)
        
        
        print("SendString: \"\(SendString)\" ")
        
        
        AF.request(SendString, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse) in
            
            ///////////////////////////////////////
            
            let statusCode = response.response?.statusCode
            print("statusCode: '\(statusCode ?? 0000)'")
            
            
            switch response.result {
                
            case let .success(value):
                
                
                ResponseJSONArray = value as! Array
                
                self.StoryDefsArray.removeAll()
                
                //                    print("ResponseJSON: \n\(ResponseJSONArray)")
                print("ResponseJSON COUNT: \(ResponseJSONArray.count)")
                
                var POS = ""
                
                for dictionary in ResponseJSONArray {
                    
                    print("dictionary: \"\(dictionary)\" ")
                    //print("SearchTextField: \"\(self.SearchTextField.stringValue)\" ")
                    
                    // "\(self.definitionSearchTextField.stringValue)"
                    //let dict1 = dictionary.keys.contains("word")
                    
                    if let val = dictionary["word"] {
                        
                        if ("\(val)" == "\(self.definitionSearchTextField.stringValue)") {
                            //                                print("Dictionary contains \"\(self.definitionSearchTextField.stringValue)\" ")
                            
                            if let defs: Array<String> = dictionary["defs"] as? Array<String> {
                                print("defs \"\(defs)\" ")
                                
                                var definitionString = ""
                                
                                //
                                
                                for each in defs {
                                    
                                    print("each: \"\(each)\" ")
                                    
                                    var EachString = ""
                                    
                                    if each.prefix(1) == "n" {
                                        //print("I found \(each.prefix(1))")
                                        //EachString = each.replacingCharacters(in: ...each.startIndex, with: "Noun:")
                                        EachString = String(each.dropFirst(2))
                                        POS = "Noun"
                                        self.nounCheckBox.isEnabled = true
                                        self.nounCheckBox.state = NSControl.StateValue(rawValue: 1)
                                    }
                                    if each.prefix(1) == "v" {
                                        //print("I found \(each.prefix(1))")
                                        //EachString = each.replacingCharacters(in: ...each.startIndex, with: "Verb:")
                                        EachString = String(each.dropFirst(2))
                                        POS = "Verb"
                                        self.verbCheckBox.isEnabled = true
                                        self.verbCheckBox.state = NSControl.StateValue(rawValue: 1)
                                    }
                                    if each.prefix(3) == "adj" {
                                        //print("I found \(each.prefix(3))")
                                        //EachString = each.replacingCharacters(in: ...each.startIndex, with: "Adjective:")
                                        EachString = String(each.dropFirst(4))
                                        //EachString = EachString.trimmingCharacters(in: .ta)// .whitespacesAndNewlines
                                        POS = "Adjective"
                                        self.adjectiveCheckBox.isEnabled = true
                                        self.adjectiveCheckBox.state = NSControl.StateValue(rawValue: 1)
                                    }
                                    if each.prefix(3) == "adv" {
                                        //print("I found \(each.prefix(3))")
                                        //EachString = each.replacingCharacters(in: ...each.startIndex, with: "Adverb:")
                                        EachString = String(each.dropFirst(3))
                                        EachString = EachString.trimmingCharacters(in: .whitespaces)
                                        
                                        POS = "Adverb"
                                        self.adverbCheckBox.isEnabled = true
                                        self.adverbCheckBox.state = NSControl.StateValue(rawValue: 1)
                                        
                                    }
                                    if each.prefix(1) == "u" {
                                        //print("I found \(each.prefix(1))")
                                        //EachString = each.replacingCharacters(in: ...each.startIndex, with: "Cannot Determine:")
                                        EachString = String(each.dropFirst(1))
                                        POS = "Unknown"
                                        self.unknownCheckBox.isHidden = false
                                        self.unknownCheckBox.isEnabled = true
                                        self.unknownCheckBox.state = NSControl.StateValue(rawValue: 1)
                                    }
                                    
                                    EachString = EachString.replacingCharacters(in: EachString.startIndex...EachString.startIndex, with: String(EachString[EachString.startIndex]).uppercased())
                                    
                                    
                                    let NLine = "\(EachString)"
                                    
                                    //print("NLine: \"\(NLine)\" ")
                                    
                                    //definitionString.append(NLine)
                                    //definitions.append(NLine)
                                    
                                    let StoryDefsAdd = StoryVerbosityDefinitions(PartOfSpeech: POS, Definition: NLine)
                                    
                                    ///////////////////
                                    
                                    if (POS == "Noun") {
                                        self.StoryDefsNounArray.append(StoryDefsAdd)
                                    } else if (POS == "Verb") {
                                        self.StoryDefsVerbArray.append(StoryDefsAdd)
                                    } else if (POS == "Adjective") {
                                        self.StoryDefsAdjectiveArray.append(StoryDefsAdd)
                                    } else if (POS == "Adverb") {
                                        self.StoryDefsAdverbArray.append(StoryDefsAdd)
                                    } else if (POS == "Unknown") {
                                        self.StoryDefsUnknownArray.append(StoryDefsAdd)
                                    }
                                    
                                    ///////////////////
                                    
                                    self.StoryDefsArray.append(StoryDefsAdd)
                                    self.StoryDefsArrayMain.append(contentsOf: self.StoryDefsArray)
                                    
                                    //self.StoryDefsArray.append(StoryDefsAdd)
                                }
                                
                                //found = 1
                            }// end of if let defs: Array<String> = dictionary["defs"] as? Array<String>
                            
                        }
                    }// end of  if let val = dictionary["word"]
                    
                    
                }// end of for dictionary in ResponseJSONArray
                
                print("StoryDefsArray.count: \"\(self.StoryDefsArray.count)\" ")
                
                
                if (self.StoryDefsArray.count == 0) {
                    
                    let StoryDefsAdd = StoryVerbosityDefinitions(PartOfSpeech: "NA", Definition: "NONE")
                    self.StoryDefsArray.append(StoryDefsAdd)
                    self.storyVerbosityTable.reloadData()
                }
                
                
                print("StoryDefsArray.count: \"\(self.StoryDefsArray.count)\" ")
                
                self.storyVerbosityTable.reloadData()
                
                
            case let .failure(error):
                
                if let error = error.asAFError {
                    
                    switch error {
                    case .invalidURL(let url):
                        print("Invalid URL: \(url) - \(error.localizedDescription)")
                    case .parameterEncodingFailed(let reason):
                        print("Parameter encoding failed: \(error.localizedDescription)")
                        print("Failure Reason: \(reason)")
                    case .multipartEncodingFailed(let reason):
                        print("Multipart encoding failed: \(error.localizedDescription)")
                        print("Failure Reason: \(reason)")
                    case .responseValidationFailed(let reason):
                        print("Response validation failed: \(error.localizedDescription)")
                        print("Failure Reason: \(reason)")
                        
                        switch reason {
                        case .dataFileNil, .dataFileReadFailed:
                            print("Downloaded file could not be read")
                        case .missingContentType(let acceptableContentTypes):
                            print("Content Type Missing: \(acceptableContentTypes)")
                        case .unacceptableContentType(let acceptableContentTypes, let responseContentType):
                            print("Response content type: \(responseContentType) was unacceptable: \(acceptableContentTypes)")
                        case .unacceptableStatusCode(let code):
                            print("Response status code was unacceptable: \(code)")
                        case .customValidationFailed(error: let error):
                            print("Response customValidationFailed: \(error)")
                        }
                    case .responseSerializationFailed(let reason):
                        print("Response serialization failed: \(error.localizedDescription)")
                        print("Failure Reason: \(reason)")
                    case .createUploadableFailed(error: let error):
                        print("Response createUploadableFailed: \(error.localizedDescription)")
                        print("Failure Reason: \(error)")
                    case .createURLRequestFailed(error: let error):
                        print("Response createURLRequestFailed: \(error.localizedDescription)")
                        print("Failure Reason: \(error)")
                    case .downloadedFileMoveFailed(error: let error, source: let source, destination: let destination):
                        print("Response downloadedFileMoveFailed: \(error.localizedDescription)")
                        print("Failure Reason: \(error)")
                    case .explicitlyCancelled:
                        print("Response explicitlyCancelled: \(error.localizedDescription)")
                        print("Failure Reason: \(error)")
                    case .parameterEncoderFailed(reason: let reason):
                        print("Response parameterEncoderFailed: \(error.localizedDescription)")
                        print("Failure Reason: \(reason)")
                    case .requestAdaptationFailed(error: let error):
                        print("Response requestAdaptationFailed: \(error.localizedDescription)")
                        print("Failure Reason: \(error)")
                    case .requestRetryFailed(retryError: let retryError, originalError: let originalError):
                        print("Response requestRetryFailed: \(error.localizedDescription)")
                        print("Failure Reason: \(originalError)")
                        print("Failure Retry Failure Reason: \(retryError)")
                    case .serverTrustEvaluationFailed(reason: let reason):
                        print("Response serverTrustEvaluationFailed: \(error.localizedDescription)")
                        print("Failure Reason: \(reason)")
                    case .sessionDeinitialized:
                        print("Response sessionDeinitialized: \(error.localizedDescription)")
                        print("Failure Reason: \(error)")
                    case .sessionInvalidated(error: let error):
                        print("Response sessionInvalidated: \(String(describing: error?.localizedDescription))")
                        print("Failure Reason: \(String(describing: error))")
                    case .sessionTaskFailed(error: let error):
                        if let urlError = error as? URLError {
                            print("Response URL ERROR: \(error.localizedDescription)")
                            print("Failure urlError: \(urlError)")
                        } else {
                            print("Response URL ERROR:");
                        }
                    case .urlRequestValidationFailed(reason: let reason):
                        print("Response urlRequestValidationFailed: \(error.localizedDescription)")
                        print("Failure Reason: \(reason)")
                    }
                    
                    print("Underlying error: \(String(describing: error.underlyingError))")
                    
                } else {
                    print("Unknown error: \(error)")
                }
                
                
                
                ///////////////////////////////////////////////
                
                let statusCode = response.response?.statusCode
                
                print("Alamofire statusCode: \"\(statusCode!)\" ") // the status code
                
                if (statusCode == 404) {
                    
                    print("Return StatusCode: \"\(statusCode!)\"")
                    let alert = NSAlert()
                    alert.messageText = "Word Search Error"
                    alert.informativeText = " StatusCode: \"\(statusCode!)\""
                    alert.alertStyle = .warning
                    alert.window.titlebarAppearsTransparent = true
                    alert.addButton(withTitle: "OK")
                    alert.runModal()
                    return
                    
                }
            }
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            //        if let json = response.result == .success {
            //            //print("Subscription JSON: \(json)") // serialized json response
            //
            //            ResponseJSONArray = response.result as! Array
            //
            //            self.StoryDefsArray.removeAll()
            //
            //            //                    print("ResponseJSON: \n\(ResponseJSONArray)")
            //            print("ResponseJSON COUNT: \(ResponseJSONArray.count)")
            //
            //            var POS = ""
            //
            //            for dictionary in ResponseJSONArray {
            //
            //                print("dictionary: \"\(dictionary)\" ")
            //                //print("SearchTextField: \"\(self.SearchTextField.stringValue)\" ")
            //
            //                // "\(self.definitionSearchTextField.stringValue)"
            //                //let dict1 = dictionary.keys.contains("word")
            //
            //                if let val = dictionary["word"] {
            //
            //                    if ("\(val)" == "\(self.definitionSearchTextField.stringValue)") {
            //                        //                                print("Dictionary contains \"\(self.definitionSearchTextField.stringValue)\" ")
            //
            //                        if let defs: Array<String> = dictionary["defs"] as? Array<String> {
            //                            print("defs \"\(defs)\" ")
            //
            //                            var definitionString = ""
            //
            //                            //
            //
            //                            for each in defs {
            //
            //                                print("each: \"\(each)\" ")
            //
            //                                var EachString = ""
            //
            //                                if each.prefix(1) == "n" {
            //                                    //print("I found \(each.prefix(1))")
            //                                    //EachString = each.replacingCharacters(in: ...each.startIndex, with: "Noun:")
            //                                    EachString = String(each.dropFirst(2))
            //                                    POS = "Noun"
            //                                    self.nounCheckBox.isEnabled = true
            //                                    self.nounCheckBox.state = NSControl.StateValue(rawValue: 1)
            //                                }
            //                                if each.prefix(1) == "v" {
            //                                    //print("I found \(each.prefix(1))")
            //                                    //EachString = each.replacingCharacters(in: ...each.startIndex, with: "Verb:")
            //                                    EachString = String(each.dropFirst(2))
            //                                    POS = "Verb"
            //                                    self.verbCheckBox.isEnabled = true
            //                                    self.verbCheckBox.state = NSControl.StateValue(rawValue: 1)
            //                                }
            //                                if each.prefix(3) == "adj" {
            //                                    //print("I found \(each.prefix(3))")
            //                                    //EachString = each.replacingCharacters(in: ...each.startIndex, with: "Adjective:")
            //                                    EachString = String(each.dropFirst(4))
            //                                    //EachString = EachString.trimmingCharacters(in: .ta)// .whitespacesAndNewlines
            //                                    POS = "Adjective"
            //                                    self.adjectiveCheckBox.isEnabled = true
            //                                    self.adjectiveCheckBox.state = NSControl.StateValue(rawValue: 1)
            //                                }
            //                                if each.prefix(3) == "adv" {
            //                                    //print("I found \(each.prefix(3))")
            //                                    //EachString = each.replacingCharacters(in: ...each.startIndex, with: "Adverb:")
            //                                    EachString = String(each.dropFirst(3))
            //                                    EachString = EachString.trimmingCharacters(in: .whitespaces)
            //
            //                                    POS = "Adverb"
            //                                    self.adverbCheckBox.isEnabled = true
            //                                    self.adverbCheckBox.state = NSControl.StateValue(rawValue: 1)
            //
            //                                }
            //                                if each.prefix(1) == "u" {
            //                                    //print("I found \(each.prefix(1))")
            //                                    //EachString = each.replacingCharacters(in: ...each.startIndex, with: "Cannot Determine:")
            //                                    EachString = String(each.dropFirst(1))
            //                                    POS = "Unknown"
            //                                    self.unknownCheckBox.isHidden = false
            //                                    self.unknownCheckBox.isEnabled = true
            //                                    self.unknownCheckBox.state = NSControl.StateValue(rawValue: 1)
            //                                }
            //
            //                                EachString = EachString.replacingCharacters(in: EachString.startIndex...EachString.startIndex, with: String(EachString[EachString.startIndex]).uppercased())
            //
            //
            //                                let NLine = "\(EachString)"
            //
            //                                //print("NLine: \"\(NLine)\" ")
            //
            //                                //definitionString.append(NLine)
            //                                //definitions.append(NLine)
            //
            //                                let StoryDefsAdd = StoryVerbosityDefinitions(PartOfSpeech: POS, Definition: NLine)
            //
            //                                ///////////////////
            //
            //                                if (POS == "Noun") {
            //                                    self.StoryDefsNounArray.append(StoryDefsAdd)
            //                                } else if (POS == "Verb") {
            //                                    self.StoryDefsVerbArray.append(StoryDefsAdd)
            //                                } else if (POS == "Adjective") {
            //                                    self.StoryDefsAdjectiveArray.append(StoryDefsAdd)
            //                                } else if (POS == "Adverb") {
            //                                    self.StoryDefsAdverbArray.append(StoryDefsAdd)
            //                                } else if (POS == "Unknown") {
            //                                    self.StoryDefsUnknownArray.append(StoryDefsAdd)
            //                                }
            //
            //                                ///////////////////
            //
            //                                self.StoryDefsArray.append(StoryDefsAdd)
            //                                self.StoryDefsArrayMain.append(contentsOf: self.StoryDefsArray)
            //
            //                                //self.StoryDefsArray.append(StoryDefsAdd)
            //                            }
            //
            //                            //found = 1
            //                        }// end of if let defs: Array<String> = dictionary["defs"] as? Array<String>
            //
            //                    }
            //                }// end of  if let val = dictionary["word"]
            //
            //
            //            }// end of for dictionary in ResponseJSONArray
            //
            //            print("StoryDefsArray.count: \"\(self.StoryDefsArray.count)\" ")
            //
            //
            //            if (self.StoryDefsArray.count == 0) {
            //
            //                let StoryDefsAdd = StoryVerbosityDefinitions(PartOfSpeech: "NA", Definition: "NONE")
            //                self.StoryDefsArray.append(StoryDefsAdd)
            //                self.storyVerbosityTable.reloadData()
            //            }
            //
            //
            //            print("StoryDefsArray.count: \"\(self.StoryDefsArray.count)\" ")
            //
            //            self.storyVerbosityTable.reloadData()
            //
            //        } else {
            //            print("end of  if let json = response.result.value: Value was not JSON")
            //            return
            //        }// end of  if let json = response.result.value {
            
            
            
            
            
            
            
            
            
            
            
            
            
        }//end of Alamofire.request
        
        
        
        
        
        
        // ALAMOFIRE version 4.0
        
        //        Alamofire.request(SendString, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON
        //            { (response:DataResponse) in
        //
        //                ///////////////////////////////////////
        //
        //                print("Success: \(response.result.isSuccess)")
        //                //print("Response String: \(response.result.value)")
        //
        //                var statusCode = response.response?.statusCode
        //
        //                if let error = response.result.error as? AFError {
        //
        //                    statusCode = error._code // statusCode private
        //
        //                    switch error {
        //
        //                    case .invalidURL(let url):
        //                        print("Invalid URL: \(url) - \(error.localizedDescription)")
        //                    case .parameterEncodingFailed(let reason):
        //                        print("Parameter encoding failed: \(error.localizedDescription)")
        //                        print("Failure Reason: \(reason)")
        //                    case .multipartEncodingFailed(let reason):
        //                        print("Multipart encoding failed: \(error.localizedDescription)")
        //                        print("Failure Reason: \(reason)")
        //                    case .responseValidationFailed(let reason):
        //                        print("Response validation failed: \(error.localizedDescription)")
        //                        print("Failure Reason: \(reason)")
        //
        //                        switch reason {
        //                        case .dataFileNil, .dataFileReadFailed:
        //                            print("Downloaded file could not be read")
        //                        case .missingContentType(let acceptableContentTypes):
        //                            print("Content Type Missing: \(acceptableContentTypes)")
        //                        case .unacceptableContentType(let acceptableContentTypes, let responseContentType):
        //                            print("Response content type: \(responseContentType) was unacceptable: \(acceptableContentTypes)")
        //                        case .unacceptableStatusCode(let code):
        //                            print("Response status code was unacceptable: \(code)")
        //                            statusCode = code
        //                        }
        //                    case .responseSerializationFailed(let reason):
        //                        print("Response serialization failed: \(error.localizedDescription)")
        //                        print("Failure Reason: \(reason)")
        //                        // statusCode = 3840 ???? maybe..
        //                    }
        //
        //                    print("Underlying error: \(error.underlyingError)")
        //
        //                    return
        //
        //                } else if let error = response.result.error as? URLError {
        //                    print("URLError occurred: \(error)")
        //
        //
        //                    if (error.errorCode == -1009) {
        //
        //                        let alert = NSAlert()
        //                        alert.messageText = "The Internet connection appears to be offline. CODE: -1009"
        //                        alert.informativeText = "URLError occurred: \(error)"
        //                        alert.alertStyle = .warning
        //                        alert.window.titlebarAppearsTransparent = true
        //                        alert.addButton(withTitle: "OK")
        //                        alert.runModal()
        //
        //
        //
        //                    }
        //
        //                    return
        //
        //                } else {
        //
        //                    //                    let ErrorMessage = "Unknown error: \(response.result.error ?? "NO ERROR" as! String)"
        //                    //
        //                    //                    if (ErrorMessage.contains("NO ERROR")) {
        //                    //
        //                    //                    } else {
        //                    //                        print("Unknown error: \(response.result.error ?? "NO ERROR" as! Error)")
        //                    //                        return
        //                    //                    }
        //
        //                }
        //
        //                print("Alamofire statusCode: \"\(statusCode!)\" ") // the status code
        //
        //                if (statusCode == 404) {
        //
        //                    print("Return StatusCode: \"\(statusCode!)\"")
        //                    let alert = NSAlert()
        //                    alert.messageText = "Word Search Error"
        //                    alert.informativeText = " StatusCode: \"\(statusCode!)\""
        //                    alert.alertStyle = .warning
        //                    alert.window.titlebarAppearsTransparent = true
        //                    alert.addButton(withTitle: "OK")
        //                    alert.runModal()
        //                    return
        //
        //                }
        //
        //                if let json = response.result.value {
        //                    //print("Subscription JSON: \(json)") // serialized json response
        //
        //                    ResponseJSONArray = response.result.value as! Array
        //
        //                    self.StoryDefsArray.removeAll()
        //
        ////                    print("ResponseJSON: \n\(ResponseJSONArray)")
        //                    print("ResponseJSON COUNT: \(ResponseJSONArray.count)")
        //
        //                    var POS = ""
        //
        //                    for dictionary in ResponseJSONArray {
        //
        //                        print("dictionary: \"\(dictionary)\" ")
        //                        //print("SearchTextField: \"\(self.SearchTextField.stringValue)\" ")
        //
        //                        // "\(self.definitionSearchTextField.stringValue)"
        //                       //let dict1 = dictionary.keys.contains("word")
        //
        //                        if let val = dictionary["word"] {
        //
        //                            if ("\(val)" == "\(self.definitionSearchTextField.stringValue)") {
        ////                                print("Dictionary contains \"\(self.definitionSearchTextField.stringValue)\" ")
        //
        //                                if let defs: Array<String> = dictionary["defs"] as? Array<String> {
        //                                    print("defs \"\(defs)\" ")
        //
        //                                    var definitionString = ""
        //
        ////
        //
        //                                    for each in defs {
        //
        //                                        print("each: \"\(each)\" ")
        //
        //                                        var EachString = ""
        //
        //                                        if each.prefix(1) == "n" {
        //                                            //print("I found \(each.prefix(1))")
        //                                            //EachString = each.replacingCharacters(in: ...each.startIndex, with: "Noun:")
        //                                            EachString = String(each.dropFirst(2))
        //                                            POS = "Noun"
        //                                            self.nounCheckBox.isEnabled = true
        //                                            self.nounCheckBox.state = NSControl.StateValue(rawValue: 1)
        //                                        }
        //                                        if each.prefix(1) == "v" {
        //                                            //print("I found \(each.prefix(1))")
        //                                            //EachString = each.replacingCharacters(in: ...each.startIndex, with: "Verb:")
        //                                            EachString = String(each.dropFirst(2))
        //                                            POS = "Verb"
        //                                            self.verbCheckBox.isEnabled = true
        //                                            self.verbCheckBox.state = NSControl.StateValue(rawValue: 1)
        //                                        }
        //                                        if each.prefix(3) == "adj" {
        //                                            //print("I found \(each.prefix(3))")
        //                                            //EachString = each.replacingCharacters(in: ...each.startIndex, with: "Adjective:")
        //                                            EachString = String(each.dropFirst(4))
        //                                            //EachString = EachString.trimmingCharacters(in: .ta)// .whitespacesAndNewlines
        //                                            POS = "Adjective"
        //                                            self.adjectiveCheckBox.isEnabled = true
        //                                            self.adjectiveCheckBox.state = NSControl.StateValue(rawValue: 1)
        //                                        }
        //                                        if each.prefix(3) == "adv" {
        //                                            //print("I found \(each.prefix(3))")
        //                                            //EachString = each.replacingCharacters(in: ...each.startIndex, with: "Adverb:")
        //                                            EachString = String(each.dropFirst(3))
        //                                            EachString = EachString.trimmingCharacters(in: .whitespaces)
        //
        //                                            POS = "Adverb"
        //                                            self.adverbCheckBox.isEnabled = true
        //                                            self.adverbCheckBox.state = NSControl.StateValue(rawValue: 1)
        //
        //                                        }
        //                                        if each.prefix(1) == "u" {
        //                                            //print("I found \(each.prefix(1))")
        //                                            //EachString = each.replacingCharacters(in: ...each.startIndex, with: "Cannot Determine:")
        //                                            EachString = String(each.dropFirst(1))
        //                                            POS = "Unknown"
        //                                            self.unknownCheckBox.isHidden = false
        //                                            self.unknownCheckBox.isEnabled = true
        //                                            self.unknownCheckBox.state = NSControl.StateValue(rawValue: 1)
        //                                        }
        //
        //                                        EachString = EachString.replacingCharacters(in: EachString.startIndex...EachString.startIndex, with: String(EachString[EachString.startIndex]).uppercased())
        //
        //
        //                                        let NLine = "\(EachString)"
        //
        //                                        //print("NLine: \"\(NLine)\" ")
        //
        //                                        //definitionString.append(NLine)
        //                                        //definitions.append(NLine)
        //
        //                                        let StoryDefsAdd = StoryVerbosityDefinitions(PartOfSpeech: POS, Definition: NLine)
        //
        //                                        ///////////////////
        //
        //                                        if (POS == "Noun") {
        //                                            self.StoryDefsNounArray.append(StoryDefsAdd)
        //                                        } else if (POS == "Verb") {
        //                                            self.StoryDefsVerbArray.append(StoryDefsAdd)
        //                                        } else if (POS == "Adjective") {
        //                                            self.StoryDefsAdjectiveArray.append(StoryDefsAdd)
        //                                        } else if (POS == "Adverb") {
        //                                            self.StoryDefsAdverbArray.append(StoryDefsAdd)
        //                                        } else if (POS == "Unknown") {
        //                                            self.StoryDefsUnknownArray.append(StoryDefsAdd)
        //                                        }
        //
        //                                        ///////////////////
        //
        //                                        self.StoryDefsArray.append(StoryDefsAdd)
        //                                        self.StoryDefsArrayMain.append(contentsOf: self.StoryDefsArray)
        //
        //                                        //self.StoryDefsArray.append(StoryDefsAdd)
        //                                    }
        //
        //                                    //found = 1
        //                                }// end of if let defs: Array<String> = dictionary["defs"] as? Array<String>
        //
        //                            }
        //                        }// end of  if let val = dictionary["word"]
        //
        //
        //                    }// end of for dictionary in ResponseJSONArray
        //
        //                    print("StoryDefsArray.count: \"\(self.StoryDefsArray.count)\" ")
        //
        //
        //                    if (self.StoryDefsArray.count == 0) {
        //
        //                        let StoryDefsAdd = StoryVerbosityDefinitions(PartOfSpeech: "NA", Definition: "NONE")
        //                        self.StoryDefsArray.append(StoryDefsAdd)
        //                        self.storyVerbosityTable.reloadData()
        //                    }
        //
        //
        //                    print("StoryDefsArray.count: \"\(self.StoryDefsArray.count)\" ")
        //
        //                    self.storyVerbosityTable.reloadData()
        //
        //                } else {
        //                    print("end of  if let json = response.result.value: Value was not JSON")
        //                    return
        //                }// end of  if let json = response.result.value {
        //
        //        }//end of Alamofire.request
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
    }//end of AlamoFire
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    @IBAction func definitionSearchButtonAction(_ sender: Any) {
        
        
        StoryDefsArray = []
        StoryDefsArrayMain = []
        StoryDefsArrayFiltered = []
        StoryDefsNounArray = []
        StoryDefsVerbArray = []
        StoryDefsAdjectiveArray = []
        StoryDefsAdverbArray = []
        StoryDefsUnknownArray = []
        
        filterOptionsArray = []
        
        
        ///////////////////
        
        var StoryVerbositySearchFinal = definitionSearchTextField.stringValue.trimmingCharacters(in: .whitespacesAndNewlines)
        print("StoryVerbositySearchFinal: \"\(StoryVerbositySearchFinal)\" ")
        
        if (StoryVerbositySearchFinal.contains(" ")) {
            StoryVerbositySearchFinal = StoryVerbositySearchFinal.replacingOccurrences(of: " ", with: "%20")
            print("StoryVerbositySearchFinal Replacing Spaces with \"%20\": \"\(StoryVerbositySearchFinal)\" ")
        }
        
        print("StoryVerbositySearchFinal: \"\(StoryVerbositySearchFinal)\" ")
        
        
        
        if (StoryVerbositySearchFinal.count == 0) {
            
            let alert = NSAlert()
            alert.messageText = "Unable to Search for blank"
            alert.informativeText = "Please enter a word so Search for"
            alert.alertStyle = .warning
            alert.addButton(withTitle: "Ok")
            
            
            if (CurrentAppearanceStoryVerbosityViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.window.titlebarAppearsTransparent = true
            
            alert.runModal()
            
        } else {
            
            self.nounCheckBox.isEnabled = false
            self.verbCheckBox.isEnabled = false
            self.adjectiveCheckBox.isEnabled = false
            self.adverbCheckBox.isEnabled = false
            
            self.unknownCheckBox.isEnabled = false
            self.unknownCheckBox.isHidden = true
            
            self.nounCheckBox.state = NSControl.StateValue(rawValue: 0)
            self.verbCheckBox.state = NSControl.StateValue(rawValue: 0)
            self.adjectiveCheckBox.state = NSControl.StateValue(rawValue: 0)
            self.adverbCheckBox.state = NSControl.StateValue(rawValue: 0)
            self.unknownCheckBox.state = NSControl.StateValue(rawValue: 0)
            
            //////////////////////////
            
            StoryDefsArray.removeAll()
            StoryDefsArrayMain.removeAll()
            self.storyVerbosityTable.reloadData()
            
            
            delayWithSeconds(0.25) {
                self.SendString = "https://api.datamuse.com/words?sp=\(StoryVerbositySearchFinal)&md=d"
                self.AlamoFireSearch()  //  https://api.datamuse.com/words?sp=lake&md=d
            }
            
            
        }
        
    }// end of definitionSearchButtonAction
    
    
    
    func clearWordArrays() {
        
        StoryDefsArray = []
        StoryDefsArrayMain = []
        StoryDefsArrayFiltered = []
        StoryDefsNounArray = []
        StoryDefsVerbArray = []
        StoryDefsAdjectiveArray = []
        StoryDefsAdverbArray = []
        StoryDefsUnknownArray = []
        
        filterOptionsArray = []
        
        
        
        
        self.nounCheckBox.isEnabled = false
        self.verbCheckBox.isEnabled = false
        self.adjectiveCheckBox.isEnabled = false
        self.adverbCheckBox.isEnabled = false
        
        self.unknownCheckBox.isEnabled = false
        self.unknownCheckBox.isHidden = true
        
        self.nounCheckBox.state = NSControl.StateValue(rawValue: 0)
        self.verbCheckBox.state = NSControl.StateValue(rawValue: 0)
        self.adjectiveCheckBox.state = NSControl.StateValue(rawValue: 0)
        self.adverbCheckBox.state = NSControl.StateValue(rawValue: 0)
        self.unknownCheckBox.state = NSControl.StateValue(rawValue: 0)
        
        //////////////////////////
        
        StoryVerbosityTableSearch.stringValue = ""
        
        StoryDefsArray.removeAll()
        StoryDefsArrayMain.removeAll()
        self.storyVerbosityTable.reloadData()
        
    }
    
    
    
    
    @IBAction func closeStoryVerbosityWindow(_ sender: Any) {
        
        definitionSearchTextField.stringValue = ""
        
        clearWordArrays()
        
        
        
        self.view.window?.close()
        
        
    }
    
    
    
    
    
    
    
    
    func numberOfRows(in tableVieww: NSTableView) -> Int {
        
        return StoryDefsArray.count
        
    }
    
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        
        guard let cell = tableView.makeView(withIdentifier: tableColumn!.identifier, owner: self) as? NSTableCellView else
        {
            return nil
        }
        
        //cell.textField?.font = NSFont(name: AppFontBold, size: 13)
        
        if (tableColumn?.identifier)!.rawValue == "partof" {
            
            cell.textField?.stringValue = StoryDefsArray[row].PartOfSpeech
            
        } else if (tableColumn?.identifier)!.rawValue == "definition" {
            
            cell.textField?.stringValue = StoryDefsArray[row].Definition
            
        }
        
        return cell
        
    }
    
    
    
    //    func tableViewSelectionDidChange(_ notification: Notification) {
    //
    //        let SelectedRow = userTableView.selectedRow
    //        print("SelectedRow: \(SelectedRow)")
    //        print("Users: \(Users.count)")
    //
    //        let isIndexValid = Users.indices.contains(SelectedRow)
    //
    //        if isIndexValid {
    //            let UserString: String = self.Users[SelectedRow].firstName
    //            print("String: \(UserString)")
    //            userTextField.stringValue = UserString
    //            passwordSecureTextField.becomeFirstResponder()
    //        }
    //
    //    }
    
    
    func filterArrayForTable() {
        
        StoryDefsArray.removeAll()
        
        if (nounCheckBox.state == .off) {
            
        } else {
            self.StoryDefsArray.append(contentsOf: self.StoryDefsNounArray)
        }
        if (verbCheckBox.state == .off) {
            
        } else {
            self.StoryDefsArray.append(contentsOf: self.StoryDefsVerbArray)
        }
        if (adjectiveCheckBox.state == .off) {
            
        } else {
            self.StoryDefsArray.append(contentsOf: self.StoryDefsAdjectiveArray)
        }
        if (adverbCheckBox.state == .off) {
            
        } else {
            self.StoryDefsArray.append(contentsOf: self.StoryDefsAdverbArray)
        }
        if (unknownCheckBox.state == .off) {
            
        } else {
            self.StoryDefsArray.append(contentsOf: self.StoryDefsUnknownArray)
        }
        
        print("self.StoryDefsArray.count: \(self.StoryDefsArray.count)")
        
        if (self.StoryDefsArray.count == 0) {
            
            self.StoryDefsArray.append(contentsOf: self.StoryDefsNounArray)
            self.StoryDefsArray.append(contentsOf: self.StoryDefsVerbArray)
            self.StoryDefsArray.append(contentsOf: self.StoryDefsAdjectiveArray)
            self.StoryDefsArray.append(contentsOf: self.StoryDefsAdverbArray)
            self.StoryDefsArray.append(contentsOf: self.StoryDefsUnknownArray)
            
            let checkBoxes = [self.nounCheckBox,self.verbCheckBox,self.adjectiveCheckBox,self.adverbCheckBox,self.unknownCheckBox]
            
            for itemCheckBox in checkBoxes {
                if (itemCheckBox!.isEnabled) {
                    itemCheckBox!.state = .on
                }
            }
            
            
        }
        
        
        self.storyVerbosityTable.reloadData()
        
    }
    
    
    @IBAction func nounFilterCheckBoxAction(_ sender: Any) {
        
        filterArrayForTable()
        
    }// end of  @IBAction func nounFilterCheckBoxAction(_ sender: Any)
    
    
    @IBAction func verbFilterCheckBoxAction(_ sender: Any) {
        filterArrayForTable()
    }
    
    @IBAction func adjectiveFilterCheckBoxAction(_ sender: Any) {
        
        filterArrayForTable()
        
    }
    
    @IBAction func adverbFilterCheckBoxAction(_ sender: Any) {
        
        filterArrayForTable()
        
    }
    
    
    @IBAction func unknowFilterCheckBoxAction(_ sender: Any) {
        
        filterArrayForTable()
        
    }
    
    
    
    @IBAction func StoryVerbositySearchAction(_ sender: Any) {
        
        if (StoryVerbosityTableSearch.stringValue.count == 0) {
            
            filterArrayForTable()
            
            
            //            self.StoryDefsArray.removeAll()
            ////
            ////            self.StoryDefsArray.append(contentsOf: self.StoryDefsNounArray)
            ////            self.StoryDefsArray.append(contentsOf: self.StoryDefsVerbArray)
            ////            self.StoryDefsArray.append(contentsOf: self.StoryDefsAdjectiveArray)
            ////            self.StoryDefsArray.append(contentsOf: self.StoryDefsAdverbArray)
            ////            self.StoryDefsArray.append(contentsOf: self.StoryDefsUnknownArray)
            ////
            ////            let checkBoxes = [self.nounCheckBox,self.verbCheckBox,self.adjectiveCheckBox,self.adverbCheckBox,self.unknownCheckBox]
            ////
            ////            for itemCheckBox in checkBoxes {
            ////                if (itemCheckBox!.isEnabled) {
            ////                    itemCheckBox!.state = .on
            ////                }
            //
            //
            //            for item in StoryDefsArrayMain {
            //                print("StoryDefsArrayMain - item.POS: '\(item.PartOfSpeech)'   Description: '\(item.Definition)'")
            //
            //
            //            }
            //
            //
            //
            ////            self.StoryDefsArray.append(contentsOf: self.StoryDefsArrayMain)
            
            
        } else {
            
            StoryDefsArrayFiltered.removeAll()
            
            for item in StoryDefsArray {
                print("item.POS: '\(item.PartOfSpeech)'   Description: '\(item.Definition)'")
                
                if (item.Definition.contains(StoryVerbosityTableSearch.stringValue)) {
                    
                    StoryDefsArrayFiltered.append(item)
                }
            }
            
            StoryDefsArray.removeAll()
            StoryDefsArray.append(contentsOf: StoryDefsArrayFiltered)
            
            
            print("StoryDefsArray: \(StoryDefsArray.count)")
        }
        
        self.storyVerbosityTable.reloadData()
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}//end of class StoryVerbosityViewController

