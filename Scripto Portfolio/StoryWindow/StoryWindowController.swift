//
//  StoryWindowController.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 2/14/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Cocoa
import SwiftLoggly







class StoryWindowController: NSWindowController,NSWindowDelegate {

    @IBOutlet weak var WCToolbar: NSToolbar!
    @IBOutlet weak var WordCountToolbarItem: NSToolbarItem!
    @IBOutlet weak var ExportToolbarItem: NSToolbarItem!
    @IBOutlet weak var PrintToolbarItem: NSToolbarItem!
    @IBOutlet weak var showHideLineNumber: NSToolbarItem!
    @IBOutlet weak var fullscreenToolbarItem: NSToolbarItem!
    @IBOutlet weak var storyAcheivementsToolbarItem: NSToolbarItem!
    
    
    var showHideLineNumbers = false
    
    
    var originalWindowWidth = CGFloat()
    var originalWindowHeight = CGFloat()
    
    var originalX = CGFloat()
    var originalY = CGFloat()

    
    @IBOutlet weak var TotalWordCountLabel: NSTextField!
    @IBOutlet weak var NewWordCountLabel: NSTextField!
    
    
    
    
    
    
    
    

    override func windowDidLoad() {
        super.windowDidLoad()
        
        self.window?.delegate = self
    
        // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
        
        if (OSSYSTEMVERSION >= 10.13) {  // >=
            window?.titlebarAppearsTransparent = true
            print("StoryWindowController = window?.titlebarAppearsTransparent = true")
            
        }
        
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(StoryPrintToolbarItemAction), name: NSNotification.Name(rawValue: "print"), object: nil)
        
       
        
        
        
        enum InterfaceStyle : String {
            case Dark, Light

            init() {
                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
                self = InterfaceStyle(rawValue: type)!
            }
        }

        let currentStyle = InterfaceStyle()

        if (currentStyle.rawValue == "Dark") {
            window?.backgroundColor = NSColor.darkGray
        } else if (currentStyle.rawValue == "Light") {
            window?.backgroundColor = WindowBackgroundColor
            
        }
        
        
        
        
//        WordCountToolbarItem.action = nil
//        WordCountToolbarItem.target = nil
        

        NotificationCenter.default.addObserver(self, selector: #selector(WordCountUpdate), name: NSNotification.Name(rawValue: "UpdateWordCount"), object: nil)
        
        //NotificationCenter.default.addObserver(self, selector: #selector(DisableToolbarItems), name: NSNotification.Name(rawValue: "DisableToolbarItems"), object: nil)
        
        //NotificationCenter.default.addObserver(self, selector: #selector(EnableToolbarItems), name: NSNotification.Name(rawValue: "EnableToolbarItems"), object: nil)
        
//        NotificationCenter.default.addObserver(self, selector: #selector(OpenWordCountPopover), name: NSNotification.Name(rawValue: "OpenWordCountPopover"), object: nil)
        
        
        self.window?.collectionBehavior = NSWindow.CollectionBehavior.fullScreenPrimary
       

//        NSScreen.mainScreen()!.frame         // {x 0 y 0 w 1,920 h 1,200}
//        NSScreen.mainScreen()!.frame.width   // 1,920.0
//        NSScreen.mainScreen()!.frame.height  // 1,200.0
        
        self.window?.maxFullScreenContentSize.width = NSScreen.main!.frame.width
        self.window?.maxFullScreenContentSize.height = NSScreen.main!.frame.height
        
        self.window?.maxSize.width = NSScreen.main!.frame.width
        self.window?.maxSize.height = NSScreen.main!.frame.height
        
        self.window?.minSize.width = 715
        self.window?.minSize.height = 300
        
        //self.window?.styleMask = [ .resizable, .titled, .closable ]
        self.window?.styleMask = [ .resizable, .titled ]
    }
    
//    override func windowWillLoad() {
//
//        //WordCountToolbarItem.label.
//
//        //window?.backgroundColor = WindowBackgroundColor
//
//    }
    
    
    
    func windowWillEnterFullScreen(_ notification: Notification) {
        print("windowWillEnterFullScreen")
        
        originalWindowWidth = (self.window?.frame.size.width)!
        originalWindowHeight = (self.window?.frame.size.height)!

        originalY = self.window!.frame.origin.y
        originalX = self.window!.frame.origin.x
        

        print("originalWindowWidth: \"\(originalWindowWidth)\" originalWindowHeight: \"\(originalWindowHeight)\" originalY: \"\(originalY)\" originalX: \"\(originalX)\" ")
        
        fullscreenToolbarItem.image = NSImage(named: NSImage.Name(rawValue: "NSExitFullScreenTemplate"))
        
        
        
    }
    
    func windowDidEnterFullScreen(_ notification: Notification) {
        print("windowDidEnterFullScreen")
        
        //self.window?.maxSize = NSMakeRect(0, 0, window!.frame.size.width,window!.frame.size.height)
        self.window?.toggleFullScreen(self)
    }
    
    func windowWillExitFullScreen(_ notification: Notification) {
        
        print("EXITING FULL SCREEN MODE")
        
        fullscreenToolbarItem.image = NSImage(named: NSImage.Name(rawValue: "NSEnterFullScreenTemplate"))
  
        
        
    }// end of windowWillExitFullScreen
    
    func windowDidExitFullScreen(_ notification: Notification) {
        //self.window?.setFrame(NSRect(x:originalX,y:originalY,width:originalWindowWidth,height:originalWindowHeight), display: true)
        
        
        self.window?.setFrame(NSRect(x:originalX,y:originalY,width:originalWindowWidth,height:originalWindowHeight), display: true, animate: true)
        
        WCToolbar.isVisible = true
        
        print("DID EXIT FULL SCREEN MODE")
    }
    
    func windowDidFailToExitFullScreen(_ window: NSWindow) {
        print("windowDidFailToExitFullScreen")
        DoAlert.DisplayAlert(Class: "StoryViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to Exit Full Screen Mode, pleae try again.", DatabaseError: "Not a DB Error")
        
    }
    
    func windowDidFailToEnterFullScreen(_ window: NSWindow) {
        print("windowDidFailToExitFullScreen")
        DoAlert.DisplayAlert(Class: "StoryViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to Enter Full Screen Mode, pleae try again.", DatabaseError: "Not a DB Error")
        
    }
    
    
//    func toggleFullScreen(_ sender: AnyObject?) {
//        print("full screen was pressed")
//
//    }
    
    
   
    
    
    @objc func WordCountUpdate() {
        
        
        if (StartingWordCount == 0) {
            StartingWordCount = StoryVCWordCount
        }
        
        //print("StartingWordCount: \"\(StartingWordCount)\" ")
        
        if (StoryVCWordCount == 0) {
            WordCountToolbarItem.label = "No Words"
            TotalWordCountLabel.stringValue = "0"
            NewWordCountLabel.stringValue = "\(StoryVCWordCount)"
        } else {
            WordCountToolbarItem.label = "\(StoryVCWordCount) Words"
            
            TotalWordCountLabel.stringValue = "\(StoryVCWordCount)"
            TotalWordCountLabel.needsDisplay = true
            
            
            NewWordCountLabel.stringValue = "\(StoryVCWordCount - StartingWordCount)"
        }
        
    }// end of WordCountUpdate()

    
//    @objc func OpenWordCountPopover() {
//
//    let WordPopover = NSStoryboard(name: NSStoryboard.Name(rawValue: "StoryWindow"), bundle: nil).instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "WordPopoverWC")) as! NSWindowController
//        WordPopover.showWindow(self)
//
//        NotificationCenter.default.addObserver(self, selector: #selector(WordCountUpdate), name: NSNotification.Name(rawValue: "UpdateWordCount"), object: nil)
//    }
    
    
    @IBAction func WordCountOpenPopover(_ sender: Any) {
        
        //print("WordCountOpenPopover CLICKED !!!!!!")
       
        
      
        StoryVC.OpenWordCountPopOver()
        
        //NotificationCenter.default.addObserver(self, selector: #selector(WordCountUpdate), name: NSNotification.Name(rawValue: "UpdateWordCount"), object: nil)
        
        
        
    }
    
    
    @IBAction func StoryExportToolbarItemAction(_ sender: Any) {
        
        if (SharedItemLock == true) {
            return
        } else {
        
            
            //  selectUrl
            //  selectUrlSaveLocation
            
            if let path = NSSavePanel().saveToLocation {
                
                //print("Path selected: \"\(path.absoluteString)\"")
                
            do {

                //t exportedFileURL = URL(fileURLWithPath: path)
                let exportedFileURL = path.appendingPathComponent("\(StoryWindow.window?.title ?? "").rtf")

                //print("exportedFileURL: \(exportedFileURL)")


    //            if exportedFileURL != nil
    //            {
    //
                let textDNA: NSAttributedString = StoryAttributedText
                
                //let CharacterCount = String(describing: textDNA).count
                //let CharacterCount = String(textDNA).count

                //let textDNA = NSAttributedString(string: StoryText)

                let range = NSRange(0..<StoryAttributedText.length)

                let textTSave = try textDNA.fileWrapper(from: range, documentAttributes: [NSAttributedString.DocumentAttributeKey.documentType:NSAttributedString.DocumentType.rtf])
                try textTSave.write(to: exportedFileURL, options: FileWrapper.WritingOptions.atomic, originalContentsURL: nil)

    //            }

            } catch {

                loggly(LogType.Error, text: "ViewController:\(#function) - Failed to create story export\n\(StoriesLocation)\n\(error.localizedDescription)")

                DoAlert.DisplayAlert(Class: "ViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to create story export.\nPlease quit then try again.", DatabaseError: "\(db.lastErrorMessage())")

                return
            }
                
            }
            
        }
        
        
    } // end of StoryExportToolbarItemAction
    
    @IBAction func StoryPrintToolbarItemAction(_ sender: Any) {
        
        
        
        if (SharedItemLock == true) {
            return
        } else {
            
            // Create another text view just for printing
            let printTextView = NSTextView(frame: NSMakeRect(0, 0, 528, 688)) // A4
            // Add a modified version of your attributed string to the view
            //printTextView.textStorage!.mutableString.appendString(StoryAttributedText)
            printTextView.textStorage!.setAttributedString(StoryAttributedText)
            
            // Get printing infos
            let sharedInfo = NSPrintInfo.shared
            let sharedDict = sharedInfo.dictionary()
            let printInfoDict = NSMutableDictionary(dictionary: sharedDict)
            printInfoDict.setObject(NSPrintInfo.JobDisposition.spool, forKey: NSPrintInfo.AttributeKey.jobDisposition as NSCopying)
            let printInfo = NSPrintInfo(dictionary: printInfoDict as! [NSPrintInfo.AttributeKey : Any])
            // Set some preferences
            printInfo.isHorizontallyCentered = true
            printInfo.isVerticallyCentered = false
            printInfo.scalingFactor = 0.8
            // Print the custom text view
            let op = NSPrintOperation(view: printTextView, printInfo: printInfo)
            
            op.run()
        }
        
        
        
        
        
        
        
    }// end of StoryPrintToolbarItemAction
    
    
    
    
    @IBAction func showHideLinenumberAction(_ sender: Any) {
        
        if (showHideLineNumbers == false) {
            showHideLineNumbers = true
            showHideLineNumber.label = "Hide Line Numbers"
        } else {
            showHideLineNumbers = false
            showHideLineNumber.label = "Show Line Numbers"
        }

        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showHideLineNumberView"), object: nil)
        //  fullscreen
        
        
        
        
    }
    
    
    
    
    @IBAction func fullscreenbuttonaction(_ sender: Any) {
        
        
//        var frame = self.view.window?.frame
//        frame?.size = NSSize(width: 400, height:200)
//        self.view.window?.setFrame(frame!, display: true)
        
        
        //self.window?.toggleFullScreen(self)
        
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "fullscreen"), object: nil)
        
        WCToolbar.isVisible = false
        
    }// end of fullscreenbuttonaction
    
    
    
    
    @IBAction func storyAcheivementsToolbaritemAction(_ sender: Any) {
        
        
        StoryAchievementsWC.showWindow(self)
    }
    
    
    @IBAction func StoryHistoryToolBarItem(_ sender: Any) {
        
        
        StoryHistoryWC.showWindow(self)
        
        
    }
    
    
    
    @IBAction func StoryVerbosityButton(_ sender: Any) {
        
        
        StoryVerbosityWC.showWindow(self)
        
    }
    
    
    
    
    
    
    
    
    

}// end of file





