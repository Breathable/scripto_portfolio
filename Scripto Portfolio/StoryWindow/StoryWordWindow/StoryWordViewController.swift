//
//  StoryWordViewController.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 4/6/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Cocoa

class StoryWordViewController: NSViewController {

    
    @IBOutlet weak var StoryWordsCountTextField: NSTextField!
    @IBOutlet weak var StoryPagesTextField: NSTextField!
    @IBOutlet weak var StoryTotalPagesTextField: NSTextField!
    
    @IBOutlet weak var StoryWordCloseButton: NSButton!
    
    var CurrentAppearanceStoryWordViewController = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        
        //StoryWordsCountTextField.stringValue = "\(StoryVCWordCount)"
        
        print("StoryWordViewController viewDidLoad RAN ")
        
    }
    
    override func viewWillAppear() {
        
        
        
        StoryWordsCountTextField.stringValue = "\(StoryVCWordCount)"
        print("StoryWordViewController viewWillAppear RAN ")
        
    }
    
    override func viewDidLayout() {
        
        //        enum InterfaceStyle : String {
        //            case Dark, Light
        //
        //            init() {
        //                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
        //                self = InterfaceStyle(rawValue: type)!
        //            }
        //        }
        //
        //        let currentStyle = InterfaceStyle()
        //
        //        //print("currentStyle: \"\(currentStyle)\"  -  CurrentAppearance: \"\(CurrentAppearance)\" ")
        //
        //        if ("\(currentStyle)" != CurrentAppearanceNewUserViewController) {
        //            appearanceCheck()
        //        }
        
        appearanceCheck()
        
        super.viewDidLayout()
        
        print("NewUserViewController - viewDidLayout finished")
        
        
    }// end of viewDidLayout()
    
    
    func appearanceCheck() {
        
        enum InterfaceStyle : String {
            case Dark, Light
            
            init() {
                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
                self = InterfaceStyle(rawValue: type)!
            }
        }
        
        let currentStyle = InterfaceStyle()
        
        if ("\(currentStyle)" != CurrentAppearanceStoryWordViewController) {
            
            if (currentStyle.rawValue == "Dark") {
                print("Dark Style Detected - NewUserViewController")
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColorDarkMode
                
                StoryWordCloseButton.styleButtonText(button: StoryWordCloseButton, buttonName: "Close", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                StoryWordCloseButton.wantsLayer = true
                StoryWordCloseButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                StoryWordCloseButton.layer?.cornerRadius = 7
                
                
                
                
            } else if (currentStyle.rawValue == "Light") {
               
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColor.cgColor
                
                StoryWordCloseButton.styleButtonText(button: StoryWordCloseButton, buttonName: "Close", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                StoryWordCloseButton.wantsLayer = true
                StoryWordCloseButton.layer?.backgroundColor = NSColor.gray.cgColor
                StoryWordCloseButton.layer?.cornerRadius = 7
                
                
            }
        }
        
        CurrentAppearanceStoryWordViewController = "\(currentStyle)"
        
        
    }//end of appearanceCheck()
    
    
    @IBAction func StoryCloseButton(_ sender: Any) {
        
        StoryPagesTextField.stringValue = ""
        StoryTotalPagesTextField.stringValue = ""
        //StoryVCWordCount = 0
        self.view.window?.close()
        
        
    }
    
    @IBAction func numberOfPagesTextFieldAction(_ sender: Any) {
        
        var NumberOfPages = Float()
        
        if (StoryPagesTextField.stringValue == "") {
            StoryPagesTextField.stringValue = "1"
        } else {
            NumberOfPages = StoryPagesTextField.floatValue
        }
        
        
        let NumberOfWords = StoryWordsCountTextField.floatValue
        
        
        if (NumberOfPages == 0) {
            NumberOfPages = 1
        }
        
        
        let PagesTotal = Float(NumberOfWords / NumberOfPages)
        
        print("TOTAL PAGES:  %.2f",PagesTotal)
        
        let decimal = PagesTotal.truncatingRemainder(dividingBy: 1)
        
        print("decimal: ",decimal)
        
        if (decimal == 0.0) {
            print("numberOfPagesTextFieldAction: 1")
            StoryTotalPagesTextField.stringValue = String(format: "%.0f",PagesTotal)
        } else {
            print("numberOfPagesTextFieldAction: 2")
            StoryTotalPagesTextField.stringValue = String(format: "%.2f",PagesTotal)
        }
        
        
        
    }
    
    
    
    
    
}
