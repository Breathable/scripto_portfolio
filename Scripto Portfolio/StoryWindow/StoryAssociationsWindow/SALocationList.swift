//
//  LocationList.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 5/17/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Foundation

class SALocationList: NSObject {
    var Ikey: String
    var Name: String
    var Description: String
    
    
    init(Ikey: String, Name: String, Description: String) {
        self.Ikey = Ikey
        self.Name = Name
        self.Description = Description
    }
    
}
