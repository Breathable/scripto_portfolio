//
//  SAStoryIdeaList.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 5/19/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Foundation

class SAStoryIdeaList: NSObject {
    var Ikey: String
    var Name: String
    var Category: String
    
    
    init(Ikey: String, Name: String, Category: String) {
        self.Ikey = Ikey
        self.Name = Name
        self.Category = Category
    }
    
}
