//
//  StoryAssociationsViewController.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 5/14/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Cocoa
import SwiftLoggly


class StoryAssociationsViewController: NSViewController, NSTableViewDataSource, NSTableViewDelegate {
    
    @IBOutlet weak var SATabView: NSTabView!
    
    
    
    

    @IBOutlet weak var StoryAssociationsCharacterListTableView: NSTableView!
    @IBOutlet weak var StoryAssociationsCharactersAddedListTableView: NSTableView!
    
    @IBOutlet weak var StoryAssociationsStoryIdeaListTableView: NSTableView!
    @IBOutlet weak var StoryAssociationsStoryIdeaAddedListTableView: NSTableView!
    
    @IBOutlet weak var StoryAssociationsLocationListTableView: NSTableView!
    @IBOutlet weak var StoryAssociationsLocationAddedListTableView: NSTableView!
    
    
    @IBOutlet weak var StoryAssociationsCharacterRefreshButton: NSButton!
    @IBOutlet weak var StoryAssociationsStoryIdeasRefreshButton: NSButton!
    @IBOutlet weak var StoryAssociationsLocationsRefreshButton: NSButton!
    
    
    @IBOutlet weak var StoryAssociationsCharacterRemoveButton: NSButton!
    @IBOutlet weak var StoryAssociationsStoryIdeasRemoveButton: NSButton!
    @IBOutlet weak var StoryAssociationsLocationsRemoveButton: NSButton!
    
    
    @IBOutlet weak var StoryAssociationsCharacterOpenButton: NSButton!
    @IBOutlet weak var StoryAssociationsStoryIdeasOpenButton: NSButton!
    @IBOutlet weak var StoryAssociationsLocationsOpenButton: NSButton!
    

    @IBOutlet weak var StoryAssociationsAddCharacterButton: NSButton!
    @IBOutlet weak var StoryAssociationsAddStoryIdeasButton: NSButton!
    @IBOutlet weak var StoryAssociationsAddLocationsButton: NSButton!
    
    
    var LocationsArray: [SALocationList] = []
    var LocationsAddedArray: [SALocationList] = []

    var StoryIdeasArray: [SAStoryIdeaList] = []
    var StoryIdeasAddedArray: [SAStoryIdeaList] = []
    
    var CharactersArray: [SACharacterList] = []
    var CharactersAddedArray: [SACharacterList] = []
    
    var CurrentAppearanceStoryAssociationsViewController = String()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        

        //self.view.window?.backgroundColor = WindowBackgroundColor
        
        
        ////////////////////////
        
        // setting tables Delegate and Data source
        
        StoryAssociationsLocationListTableView.delegate = self
        StoryAssociationsLocationListTableView.dataSource = self
        
        StoryAssociationsLocationAddedListTableView.delegate = self
        StoryAssociationsLocationAddedListTableView.dataSource = self
        
        StoryAssociationsStoryIdeaListTableView.delegate = self
        StoryAssociationsStoryIdeaListTableView.dataSource = self
        
        StoryAssociationsStoryIdeaAddedListTableView.delegate = self
        StoryAssociationsStoryIdeaAddedListTableView.dataSource = self
        
        StoryAssociationsCharacterListTableView.delegate = self
        StoryAssociationsCharacterListTableView.dataSource = self
        
        StoryAssociationsCharactersAddedListTableView.delegate = self
        StoryAssociationsCharactersAddedListTableView.dataSource = self
        
        ///////////////////////

    } // end of viewDidLoad()
    
    
    override func viewWillAppear() {
        
       
        
        ////////////////////////
        

        
        ///////////////////////
        
        
        
        
        
        
        
        ////////////////////////
        
        
        LoadTables()
        
    } // end of viewWillAppear()
    
   
    
    
    override func viewDidLayout() {
        
        //        enum InterfaceStyle : String {
        //            case Dark, Light
        //
        //            init() {
        //                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
        //                self = InterfaceStyle(rawValue: type)!
        //            }
        //        }
        //
        //        let currentStyle = InterfaceStyle()
        //
        //        //print("currentStyle: \"\(currentStyle)\"  -  CurrentAppearance: \"\(CurrentAppearance)\" ")
        //
        //        if ("\(currentStyle)" != CurrentAppearanceNewUserViewController) {
        //            appearanceCheck()
        //        }
        
        appearanceCheck()
        
        super.viewDidLayout()
        
        print("NewUserViewController - viewDidLayout finished")
        
        
    }// end of viewDidLayout()
    
    
    func appearanceCheck() {
        
        enum InterfaceStyle : String {
            case Dark, Light
            
            init() {
                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
                self = InterfaceStyle(rawValue: type)!
            }
        }
        
        let currentStyle = InterfaceStyle()
        
        if ("\(currentStyle)" != CurrentAppearanceStoryAssociationsViewController) {
            
            if (currentStyle.rawValue == "Dark") {
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColorDarkMode
                
                let ButtonBackgroundColor = NSColor.black.cgColor
                
                
                StoryAssociationsCharacterRemoveButton.styleButtonText(button: StoryAssociationsCharacterRemoveButton, buttonName: "Remove", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                StoryAssociationsCharacterRemoveButton.wantsLayer = true
                StoryAssociationsCharacterRemoveButton.layer?.backgroundColor = ButtonBackgroundColor
                StoryAssociationsCharacterRemoveButton.layer?.cornerRadius = 7
                
                StoryAssociationsStoryIdeasRemoveButton.styleButtonText(button: StoryAssociationsStoryIdeasRemoveButton, buttonName: "Remove", fontColor: .white, alignment: .center, font: AppFont, size: 13.0)
                
                StoryAssociationsStoryIdeasRemoveButton.wantsLayer = true
                StoryAssociationsStoryIdeasRemoveButton.layer?.backgroundColor = ButtonBackgroundColor
                StoryAssociationsStoryIdeasRemoveButton.layer?.cornerRadius = 7
                
                StoryAssociationsLocationsRemoveButton.styleButtonText(button: StoryAssociationsLocationsRemoveButton, buttonName: "Remove", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                StoryAssociationsLocationsRemoveButton.wantsLayer = true
                StoryAssociationsLocationsRemoveButton.layer?.backgroundColor = ButtonBackgroundColor
                StoryAssociationsLocationsRemoveButton.layer?.cornerRadius = 7

                StoryAssociationsCharacterOpenButton.styleButtonText(button: StoryAssociationsCharacterOpenButton, buttonName: "Open", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                StoryAssociationsCharacterOpenButton.wantsLayer = true
                StoryAssociationsCharacterOpenButton.layer?.backgroundColor = ButtonBackgroundColor
                StoryAssociationsCharacterOpenButton.layer?.cornerRadius = 7
                
                StoryAssociationsStoryIdeasOpenButton.styleButtonText(button: StoryAssociationsStoryIdeasOpenButton, buttonName: "Open", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                StoryAssociationsStoryIdeasOpenButton.wantsLayer = true
                StoryAssociationsStoryIdeasOpenButton.layer?.backgroundColor = ButtonBackgroundColor
                StoryAssociationsStoryIdeasOpenButton.layer?.cornerRadius = 7
                
                StoryAssociationsLocationsOpenButton.styleButtonText(button: StoryAssociationsLocationsOpenButton, buttonName: "Open", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                StoryAssociationsLocationsOpenButton.wantsLayer = true
                StoryAssociationsLocationsOpenButton.layer?.backgroundColor = ButtonBackgroundColor
                StoryAssociationsLocationsOpenButton.layer?.cornerRadius = 7
                
                StoryAssociationsAddCharacterButton.styleButtonText(button: StoryAssociationsAddCharacterButton, buttonName: "Add Character", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                StoryAssociationsAddCharacterButton.wantsLayer = true
                StoryAssociationsAddCharacterButton.layer?.backgroundColor = ButtonBackgroundColor
                StoryAssociationsAddCharacterButton.layer?.cornerRadius = 7
                
                StoryAssociationsAddStoryIdeasButton.styleButtonText(button: StoryAssociationsAddStoryIdeasButton, buttonName: "Add Story Idea", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                StoryAssociationsAddStoryIdeasButton.wantsLayer = true
                StoryAssociationsAddStoryIdeasButton.layer?.backgroundColor = ButtonBackgroundColor
                StoryAssociationsAddStoryIdeasButton.layer?.cornerRadius = 7
                
                StoryAssociationsAddLocationsButton.styleButtonText(button: StoryAssociationsAddLocationsButton, buttonName: "Add Location", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                StoryAssociationsAddLocationsButton.wantsLayer = true
                StoryAssociationsAddLocationsButton.layer?.backgroundColor = ButtonBackgroundColor
                StoryAssociationsAddLocationsButton.layer?.cornerRadius = 7
                
                
                
            } else if (currentStyle.rawValue == "Light") {
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColor.cgColor
                
                
                StoryAssociationsCharacterRemoveButton.styleButtonText(button: StoryAssociationsCharacterRemoveButton, buttonName: "Remove", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                StoryAssociationsCharacterRemoveButton.wantsLayer = true
                StoryAssociationsCharacterRemoveButton.layer?.backgroundColor = NSColor.gray.cgColor
                StoryAssociationsCharacterRemoveButton.layer?.cornerRadius = 7
                
                StoryAssociationsStoryIdeasRemoveButton.styleButtonText(button: StoryAssociationsStoryIdeasRemoveButton, buttonName: "Remove", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                StoryAssociationsStoryIdeasRemoveButton.wantsLayer = true
                StoryAssociationsStoryIdeasRemoveButton.layer?.backgroundColor = NSColor.gray.cgColor
                StoryAssociationsStoryIdeasRemoveButton.layer?.cornerRadius = 7
                
                StoryAssociationsLocationsRemoveButton.styleButtonText(button: StoryAssociationsLocationsRemoveButton, buttonName: "Remove", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                StoryAssociationsLocationsRemoveButton.wantsLayer = true
                StoryAssociationsLocationsRemoveButton.layer?.backgroundColor = NSColor.gray.cgColor
                StoryAssociationsLocationsRemoveButton.layer?.cornerRadius = 7
                
                
                
                
                
                StoryAssociationsCharacterOpenButton.styleButtonText(button: StoryAssociationsCharacterOpenButton, buttonName: "Open", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                StoryAssociationsCharacterOpenButton.wantsLayer = true
                StoryAssociationsCharacterOpenButton.layer?.backgroundColor = NSColor.gray.cgColor
                StoryAssociationsCharacterOpenButton.layer?.cornerRadius = 7
                
                StoryAssociationsStoryIdeasOpenButton.styleButtonText(button: StoryAssociationsStoryIdeasOpenButton, buttonName: "Open", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                StoryAssociationsStoryIdeasOpenButton.wantsLayer = true
                StoryAssociationsStoryIdeasOpenButton.layer?.backgroundColor = NSColor.gray.cgColor
                StoryAssociationsStoryIdeasOpenButton.layer?.cornerRadius = 7
                
                StoryAssociationsLocationsOpenButton.styleButtonText(button: StoryAssociationsLocationsOpenButton, buttonName: "Open", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                StoryAssociationsLocationsOpenButton.wantsLayer = true
                StoryAssociationsLocationsOpenButton.layer?.backgroundColor = NSColor.gray.cgColor
                StoryAssociationsLocationsOpenButton.layer?.cornerRadius = 7
                
                
                
                StoryAssociationsAddCharacterButton.styleButtonText(button: StoryAssociationsAddCharacterButton, buttonName: "Add Character", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                StoryAssociationsAddCharacterButton.wantsLayer = true
                StoryAssociationsAddCharacterButton.layer?.backgroundColor = NSColor.gray.cgColor
                StoryAssociationsAddCharacterButton.layer?.cornerRadius = 7
                
                StoryAssociationsAddStoryIdeasButton.styleButtonText(button: StoryAssociationsAddStoryIdeasButton, buttonName: "Add Story Idea", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                StoryAssociationsAddStoryIdeasButton.wantsLayer = true
                StoryAssociationsAddStoryIdeasButton.layer?.backgroundColor = NSColor.gray.cgColor
                StoryAssociationsAddStoryIdeasButton.layer?.cornerRadius = 7
                
                StoryAssociationsAddLocationsButton.styleButtonText(button: StoryAssociationsAddLocationsButton, buttonName: "Add Location", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                StoryAssociationsAddLocationsButton.wantsLayer = true
                StoryAssociationsAddLocationsButton.layer?.backgroundColor = NSColor.gray.cgColor
                StoryAssociationsAddLocationsButton.layer?.cornerRadius = 7
                
            }
        }
        
        CurrentAppearanceStoryAssociationsViewController = "\(currentStyle)"
        
        
    }//end of appearanceCheck()
    
    
    
    
    
    
    
    ////////////////////////////////////////////////
    
    
    
    
    func numberOfRows(in tableView: NSTableView) -> Int {
        var RowsCount = Int()



        if (tableView == StoryAssociationsLocationListTableView) {
            //print("numberOfRows = StoryAssociationsLocationListTableView")
            RowsCount = LocationsArray.count
            //print("RowsCount: LocationsArray.count = \"\(LocationsArray.count)\"")
        }
        if (tableView == StoryAssociationsLocationAddedListTableView) {
            //print("numberOfRows = StoryAssociationsLocationAddedListTableView")
            RowsCount = LocationsAddedArray.count
            //print("RowsCount: LocationsAddedArray = \"\(LocationsAddedArray.count)\"")
        }


        if (tableView == StoryAssociationsStoryIdeaListTableView) {
            //print("numberOfRows = StoryAssociationsStoryIdeaListTableView")
            RowsCount = StoryIdeasArray.count
            //print("RowsCount: StoryIdeasArray = \"\(StoryIdeasArray.count)\"")
            
        }
        if (tableView == StoryAssociationsStoryIdeaAddedListTableView) {
            //print("numberOfRows = StoryAssociationsStoryIdeaAddedListTableView")
            RowsCount = StoryIdeasAddedArray.count
            //print("RowsCount: StoryIdeasAddedArray = \"\(StoryIdeasAddedArray.count)\"")
            
        }


        if (tableView == StoryAssociationsCharacterListTableView) {
            print("numberOfRows = StoryAssociationsCharacterListTableView")
            RowsCount = CharactersArray.count
        }
        if (tableView == StoryAssociationsCharactersAddedListTableView) {
            print("numberOfRows = StoryAssociationsCharacterListAddedTableView")
            RowsCount = CharactersAddedArray.count
        }

        print("RowsCount: \"\(RowsCount)\"")

        return RowsCount

    }

    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {

        guard let cell = tableView.makeView(withIdentifier: tableColumn!.identifier, owner: self) as? NSTableCellView else
        {
            print("Table Returned Nil")
            return nil
        }
        
        

        if (tableView == StoryAssociationsLocationListTableView) {

            //print("tableView tableView == StoryAssociationsLocationListTableView")
            
            if (tableColumn?.identifier)!.rawValue == "ikey" {
                cell.textField?.stringValue = LocationsArray[row].Ikey
            }
            if (tableColumn?.identifier)!.rawValue == "name" {
                cell.textField?.stringValue = LocationsArray[row].Name
            }
            if (tableColumn?.identifier)!.rawValue == "description" {
                cell.textField?.stringValue = LocationsArray[row].Description
            }
            
        }
        
        
        if (tableView == StoryAssociationsLocationAddedListTableView) {
            
            //print("tableView tableView == StoryAssociationsLocationAddedListTableView")

            if (tableColumn?.identifier)!.rawValue == "ikey" {
                cell.textField?.stringValue = LocationsAddedArray[row].Ikey
                //print("tableView added Ikey: \"\(LocationsAddedArray[row].Ikey)\"")
            }
            if (tableColumn?.identifier)!.rawValue == "name" {
                cell.textField?.stringValue = LocationsAddedArray[row].Name
                //print("tableView added Name: \"\(LocationsAddedArray[row].Name)\"")
            }
            if (tableColumn?.identifier)!.rawValue == "description" {
                cell.textField?.stringValue = LocationsAddedArray[row].Description
                //print("tableView added Description: \"\(LocationsAddedArray[row].Description)\"")
            }

        }
        

        if (tableView == StoryAssociationsStoryIdeaListTableView) {
            
            //print("tableView tableView == StoryAssociationsStoryIdeaListTableView")

            if (tableColumn?.identifier)!.rawValue == "ikey" {
                cell.textField?.stringValue = StoryIdeasArray[row].Ikey
            }
            if (tableColumn?.identifier)!.rawValue == "name" {
                cell.textField?.stringValue = StoryIdeasArray[row].Name
            }
            if (tableColumn?.identifier)!.rawValue == "category" {
                cell.textField?.stringValue = StoryIdeasArray[row].Category
            }

        }
        
        if (tableView == StoryAssociationsStoryIdeaAddedListTableView) {
            
            //print("tableView tableView == StoryAssociationsStoryIdeaAddedListTableView")
            
            if (tableColumn?.identifier)!.rawValue == "ikey" {
                cell.textField?.stringValue = StoryIdeasAddedArray[row].Ikey
            }
            if (tableColumn?.identifier)!.rawValue == "name" {
                cell.textField?.stringValue = StoryIdeasAddedArray[row].Name
            }
            if (tableColumn?.identifier)!.rawValue == "category" {
                cell.textField?.stringValue = StoryIdeasAddedArray[row].Category
            }
            
        }
        
        //StoryAssociationsCharacterListTableView
        if (tableView == StoryAssociationsCharacterListTableView) {
            
            //print("tableView tableView == StoryAssociationsStoryIdeaAddedListTableView")
            
            if (tableColumn?.identifier)!.rawValue == "ikey" {
                cell.textField?.stringValue = CharactersArray[row].Ikey
            }
            if (tableColumn?.identifier)!.rawValue == "fullname" {
                cell.textField?.stringValue = CharactersArray[row].FullName
            }
            
        }
        
        if (tableView == StoryAssociationsCharactersAddedListTableView) {
            
            //print("tableView tableView == StoryAssociationsStoryIdeaAddedListTableView")
            
            if (tableColumn?.identifier)!.rawValue == "ikey" {
                cell.textField?.stringValue = CharactersAddedArray[row].Ikey
            }
            if (tableColumn?.identifier)!.rawValue == "fullname" {
                cell.textField?.stringValue = CharactersAddedArray[row].FullName
            }
            
        }
        
        
        
        

        return cell

    }
    
    func tableViewSelectionDidChange(_ notification: Notification) {
        
        
        StoryAssociationsLocationsRemoveButton.isEnabled = false
        StoryAssociationsLocationsOpenButton.isEnabled = false
        StoryAssociationsAddLocationsButton.isEnabled = false
        
        StoryAssociationsStoryIdeasRemoveButton.isEnabled = false
        StoryAssociationsStoryIdeasOpenButton.isEnabled = false
        StoryAssociationsAddStoryIdeasButton.isEnabled = false
        
        StoryAssociationsCharacterRemoveButton.isEnabled = false
        StoryAssociationsCharacterOpenButton.isEnabled = false
        StoryAssociationsAddCharacterButton.isEnabled = false


        
        
        /////////////////////////////////////////

        
        let SelectedTable = (notification.object as! NSTableView)
        
        let SelectedRow = SelectedTable.selectedRow
        
        if (SelectedRow == -1) {
            return
        }
        
        
        
        if (SelectedTable == StoryAssociationsLocationListTableView) {

            //print("SelectedRow: \(SelectedRow)")

            if (SelectedRow == -1) {
                StoryAssociationsAddLocationsButton.isEnabled = false
                StoryAssociationsLocationsOpenButton.isEnabled = false
                return
            }

//            let FName = LocationsArray[SelectedRow].Name
//            let Ikey = LocationsArray[SelectedRow].Ikey
//            let Description = LocationsArray[SelectedRow].Description

            //print("Table StoryAssociationsCharacterListTableView - Ikey: \"\(Ikey)\" Name: \"\(FName)\" Description: \"\(Description)\"")
            
            LocationName = LocationsArray[SelectedRow].Name
            LocationIkey = LocationsArray[SelectedRow].Ikey

            let isIndexValid = LocationsArray.indices.contains(SelectedRow)

            if isIndexValid {
                StoryAssociationsAddLocationsButton.isEnabled = true
                StoryAssociationsLocationsOpenButton.isEnabled = true
            } else {
                StoryAssociationsAddLocationsButton.isEnabled = false
                StoryAssociationsLocationsOpenButton.isEnabled = false
            }
            
        }// end of StoryAssociationsLocationListTableView
        
        
        
        if (SelectedTable == StoryAssociationsLocationAddedListTableView) {
            
            //print("SelectedRow: \(SelectedRow)")
            
            if (SelectedRow == -1) {
                StoryAssociationsLocationsRemoveButton.isEnabled = false
                StoryAssociationsLocationsOpenButton.isEnabled = false
                return
            }
            
//            let FName = LocationsAddedArray[SelectedRow].Name
//            let Ikey = LocationsAddedArray[SelectedRow].Ikey
//            let Description = LocationsAddedArray[SelectedRow].Description
//
//            print("Table StoryAssociationsCharacterListTableView - Ikey: \"\(Ikey)\" Name: \"\(FName)\" Description: \"\(Description)\"")
            
            LocationName = LocationsAddedArray[SelectedRow].Name
            LocationIkey = LocationsAddedArray[SelectedRow].Ikey
            
            let isIndexValid = LocationsAddedArray.indices.contains(SelectedRow)
            
            if isIndexValid {
                StoryAssociationsLocationsRemoveButton.isEnabled = true
                StoryAssociationsLocationsOpenButton.isEnabled = true
            } else {
                StoryAssociationsLocationsRemoveButton.isEnabled = false
                StoryAssociationsLocationsOpenButton.isEnabled = false
            }
            
            
        }// end of StoryAssociationsLocationAddedListTableView
        
        // StoryAssociationsStoryIdeaAddedListTableView
        if (SelectedTable == StoryAssociationsStoryIdeaListTableView) {
            
            //print("SelectedRow: \(SelectedRow)")
            
            if (SelectedRow == -1) {
                StoryAssociationsAddStoryIdeasButton.isEnabled = false
                StoryAssociationsStoryIdeasOpenButton.isEnabled = false
                return
            }
            
            //            let FName = LocationsAddedArray[SelectedRow].Name
            //            let Ikey = LocationsAddedArray[SelectedRow].Ikey
            //            let Description = LocationsAddedArray[SelectedRow].Description
            //
            //            print("Table StoryAssociationsCharacterListTableView - Ikey: \"\(Ikey)\" Name: \"\(FName)\" Description: \"\(Description)\"")
            
            StoryIdeaIkey = StoryIdeasArray[SelectedRow].Ikey
            StoryIdeaName = StoryIdeasArray[SelectedRow].Name
            StoryIdeaCategory = StoryIdeasArray[SelectedRow].Category
            
            

            
            let isIndexValid = StoryIdeasArray.indices.contains(SelectedRow)
            
            if isIndexValid {
                StoryAssociationsAddStoryIdeasButton.isEnabled = true
                StoryAssociationsStoryIdeasOpenButton.isEnabled = true
                
                
                
            } else {
                StoryAssociationsAddStoryIdeasButton.isEnabled = false
                StoryAssociationsStoryIdeasOpenButton.isEnabled = false
            }
            
            
        }// end of StoryAssociationsStoryIdeaListTableView
        
        
        if (SelectedTable == StoryAssociationsStoryIdeaAddedListTableView) {
            
            //print("SelectedRow: \(SelectedRow)")
            
            if (SelectedRow == -1) {
                StoryAssociationsStoryIdeasRemoveButton.isEnabled = false
                StoryAssociationsStoryIdeasOpenButton.isEnabled = false
                return
            }
            
            //            let FName = LocationsAddedArray[SelectedRow].Name
            //            let Ikey = LocationsAddedArray[SelectedRow].Ikey
            //            let Description = LocationsAddedArray[SelectedRow].Description
            //
            //            print("Table StoryAssociationsCharacterListTableView - Ikey: \"\(Ikey)\" Name: \"\(FName)\" Description: \"\(Description)\"")
            
            StoryIdeaIkey = StoryIdeasArray[SelectedRow].Ikey
            StoryIdeaName = StoryIdeasArray[SelectedRow].Name
            StoryIdeaCategory = StoryIdeasArray[SelectedRow].Category
            
            let isIndexValid = StoryIdeasAddedArray.indices.contains(SelectedRow)
            
            if isIndexValid {
                StoryAssociationsStoryIdeasRemoveButton.isEnabled = true
                StoryAssociationsStoryIdeasOpenButton.isEnabled = true
            } else {
                StoryAssociationsStoryIdeasRemoveButton.isEnabled = false
                StoryAssociationsStoryIdeasOpenButton.isEnabled = false
            }

        }// end of StoryAssociationsStoryIdeaListTableView
        
        
        if (SelectedTable == StoryAssociationsCharacterListTableView) {
            
            //print("SelectedRow: \(SelectedRow)")
            
            if (SelectedRow == -1) {
                StoryAssociationsAddCharacterButton.isEnabled = false
                StoryAssociationsCharacterOpenButton.isEnabled = false
                return
            }
            
            //            let FName = LocationsAddedArray[SelectedRow].Name
            //            let Ikey = LocationsAddedArray[SelectedRow].Ikey
            //            let Description = LocationsAddedArray[SelectedRow].Description
            //
            //            print("Table StoryAssociationsCharacterListTableView - Ikey: \"\(Ikey)\" Name: \"\(FName)\" Description: \"\(Description)\"")
            
//            StoryIdeaIkey = StoryIdeasArray[SelectedRow].Ikey
//            StoryIdeaName = StoryIdeasArray[SelectedRow].Name
//            StoryIdeaCategory = StoryIdeasArray[SelectedRow].Category
            
            
            for name in CharactersAddedArray {
                print("Name: \"\(name)\" ")
            }
            
            print("CharactersAddedArray: \"\(CharactersAddedArray)\" ")
            
            //
            //var NameArray = CharactersAddedArray[SelectedRow].FullName.components(separatedBy: " ")
            var NameArray = CharactersArray[SelectedRow].FullName.components(separatedBy: " ")
            
            CVCIkey = CharactersArray[SelectedRow].Ikey
            CVCName = NameArray[0]
            
            
            let isIndexValid = CharactersArray.indices.contains(SelectedRow)
            
            if isIndexValid {
                StoryAssociationsAddCharacterButton.isEnabled = true
                StoryAssociationsCharacterOpenButton.isEnabled = true
                
                
                
            } else {
                StoryAssociationsAddCharacterButton.isEnabled = false
                StoryAssociationsCharacterOpenButton.isEnabled = false
            }
            
            
        }// end of StoryAssociationsCharacterListTableView
        
        
        if (SelectedTable == StoryAssociationsCharactersAddedListTableView) {
            
            //print("SelectedRow: \(SelectedRow)")
            
            if (SelectedRow == -1) {
                StoryAssociationsCharacterRemoveButton.isEnabled = false
                StoryAssociationsCharacterOpenButton.isEnabled = false
                return
            }
            
            var NameArray = CharactersAddedArray[SelectedRow].FullName.components(separatedBy: " ")

            CVCIkey = CharactersAddedArray[SelectedRow].Ikey
            CVCName = NameArray[0]
           
            
            let isIndexValid = CharactersAddedArray.indices.contains(SelectedRow)
            
            if isIndexValid {
                StoryAssociationsCharacterRemoveButton.isEnabled = true
                StoryAssociationsCharacterOpenButton.isEnabled = true
            } else {
                StoryAssociationsCharacterRemoveButton.isEnabled = false
                StoryAssociationsCharacterOpenButton.isEnabled = false
            }
            
        }// end of StoryAssociationsCharacterListTableView
        
        
        
       
        
        //        StoryAssociationsCharacterListTableView
        //        StoryAssociationsCharactersAddedListTableView.reloadData()


    }// end of tableViewSelectionDidChange
    
    
    ////////////////////////////////////////////////
    
    func LoadTables() {

        ClearAllArrays()

        LoadLocationTable()
        LoadLocationAddedTable()
        LoadStoryIdeasTable()
        LoadStoryIdeasAddedTable()
        LoadCharacterTable()
        LoadCharacterAddedTable()
        
    }
    
    func ClearAllArrays() {
        
        LocationsArray.removeAll()
        LocationsAddedArray.removeAll()
        
        StoryIdeasArray.removeAll()
        StoryIdeasAddedArray.removeAll()
        
        CharactersArray.removeAll()
        CharactersAddedArray.removeAll()
        
    }
    
    
    
    
    
    
    
    
    func LoadCharacterTable() {
        
        let StoryIkeyQuery = String("SELECT ikey from storytable where storyname = \"\(StoryWindow.window?.title ?? "BLANK STORY NAME")\" and userikeyreference = \"\(CurrentUserIkey!)\" ")
        
        print("StoryIkeyQuery: \"\(StoryIkeyQuery)\" ")
        
        if (StoryWindow.window?.title == "BLANK STORY NAME") {
            print("Load Locations - BLANK STORY NAME")
            return
        }
        
        var StoryIkeyDB = String()
        
        do {
            let StoryIkeyQueryRun = try db.executeQuery(StoryIkeyQuery, values:nil)
            while StoryIkeyQueryRun.next() {
                StoryIkeyDB = StoryIkeyQueryRun.string(forColumn: "ikey")!
            }
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "StoryAssociationsViewController-\(#function) Query Failed:  \"\(StoryIkeyQuery)\" ")
            DoAlert.DisplayAlert(Class: "StoryAssociationsViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"\(StoryIkeyQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
        }
        
        print("StoryIkeyDB: \"\(StoryIkeyDB)\" ")
        
        ////////////////
        
        let CharacterIkeysQuery = String("select * from charactertable where storyassociatedikey not like '%%\(StoryIkeyDB)%%' and userikeyreference = \"\(CurrentUserIkey!)\" ")
        
        print("CharacterIkeysQuery: \"\(CharacterIkeysQuery)\" ")
        
        if (StoryWindow.window?.title == "BLANK STORY NAME") {
            print("Load Locations - BLANK STORY NAME")
            return
        }
        
        
        var IkeyDB = String()
        var FNameDB = String()
        var MNameDB = String()
        var LNameDB = String()
        
        do {
            
            let CharacterIkeysQueryRun = try db.executeQuery(CharacterIkeysQuery, values:nil)
            while CharacterIkeysQueryRun.next() {
                
                IkeyDB = CharacterIkeysQueryRun.string(forColumn: "ikey")!
                FNameDB = CharacterIkeysQueryRun.string(forColumn: "firstname")!
                MNameDB = CharacterIkeysQueryRun.string(forColumn: "middlename")!
                LNameDB = CharacterIkeysQueryRun.string(forColumn: "lastname")!
                
                var FullName = String()
                
                if (MNameDB == "") {
                    FullName = String("\(FNameDB) \(LNameDB)")
                } else {
                    FullName = String("\(FNameDB) \(MNameDB) \(LNameDB)")
                }
                
                
                
                print("IkeyDB: \"\(IkeyDB)\" FNameDB: \"\(FNameDB)\" MNameDB: \"\(MNameDB)\" LNameDB: \"\(LNameDB)\" FullName: \"\(FullName)\" ")
                
            
                
                let CharactersToAdd = SACharacterList.init(Ikey: IkeyDB, FullName: FullName)
                CharactersArray.append(CharactersToAdd)
                
            }
            
            
            
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "StoryAssociationsViewController-\(#function) Query Failed:  \"\(CharacterIkeysQuery)\" ")
            DoAlert.DisplayAlert(Class: "StoryAssociationsViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"\(CharacterIkeysQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
        }
        
        
        print("CharactersArray: \"\(CharactersArray.count)\" ")
        
        
        
        StoryAssociationsCharacterListTableView.reloadData()
        
    }// end of LoadCharacterTable()
    
    func LoadCharacterAddedTable() {
        
        
        let StoryIkeyQuery = String("SELECT ikey from storytable where storyname = \"\(StoryWindow.window?.title ?? "BLANK STORY NAME")\" and userikeyreference = \"\(CurrentUserIkey!)\" ")
        
        print("StoryIkeyQuery: \"\(StoryIkeyQuery)\" ")
        
        if (StoryWindow.window?.title == "BLANK STORY NAME") {
            print("Load Locations - BLANK STORY NAME")
            return
        }
        
        var StoryIkeyDB = String()
        
        do {
            let StoryIkeyQueryRun = try db.executeQuery(StoryIkeyQuery, values:nil)
            while StoryIkeyQueryRun.next() {
                StoryIkeyDB = StoryIkeyQueryRun.string(forColumn: "ikey")!
            }
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "StoryAssociationsViewController-\(#function) Query Failed:  \"\(StoryIkeyQuery)\" ")
            DoAlert.DisplayAlert(Class: "StoryAssociationsViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"\(StoryIkeyQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
        }
        
        print("StoryIkeyDB: \"\(StoryIkeyDB)\" ")
        
        ////////////////
        
        let CharacterIkeysQuery = String("select * from charactertable where storyassociatedikey like '%%\(StoryIkeyDB)%%' and userikeyreference = \"\(CurrentUserIkey!)\" ")
        
        print("CharacterIkeysQuery: \"\(CharacterIkeysQuery)\" ")
        
        if (StoryWindow.window?.title == "BLANK STORY NAME") {
            print("Load Locations - BLANK STORY NAME")
            return
        }
        
        
        var IkeyDB = String()
        var FNameDB = String()
        var MNameDB = String()
        var LNameDB = String()
        
        do {
            
            let CharacterIkeysQueryRun = try db.executeQuery(CharacterIkeysQuery, values:nil)
            while CharacterIkeysQueryRun.next() {
                
                IkeyDB = CharacterIkeysQueryRun.string(forColumn: "ikey")!
                FNameDB = CharacterIkeysQueryRun.string(forColumn: "firstname")!
                MNameDB = CharacterIkeysQueryRun.string(forColumn: "middlename")!
                LNameDB = CharacterIkeysQueryRun.string(forColumn: "lastname")!
                
                var FullName = String()
                
                if (MNameDB == "") {
                    FullName = String("\(FNameDB) \(LNameDB)")
                } else {
                    FullName = String("\(FNameDB) \(MNameDB) \(LNameDB)")
                }
                
                
                
                print("IkeyDB: \"\(IkeyDB)\" FNameDB: \"\(FNameDB)\" MNameDB: \"\(MNameDB)\" LNameDB: \"\(LNameDB)\" FullName: \"\(FullName)\" ")
                
                
                
                let CharactersToAdd = SACharacterList.init(Ikey: IkeyDB, FullName: FullName)
                CharactersAddedArray.append(CharactersToAdd)
                
            }
            
            
            
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "StoryAssociationsViewController-\(#function) Query Failed:  \"\(CharacterIkeysQuery)\" ")
            DoAlert.DisplayAlert(Class: "StoryAssociationsViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"\(CharacterIkeysQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
        }
        
        
        print("CharactersArray: \"\(CharactersAddedArray.count)\" ")
        
        
        
        StoryAssociationsCharactersAddedListTableView.reloadData()
        
        
        
        
    }// end of LoadCharacterAddedTable()
    
    
    func LoadStoryIdeasTable() {
        
        let StoryIkeyQuery = String("SELECT ikey from storytable where storyname = \"\(StoryWindow.window?.title ?? "BLANK STORY NAME")\" and userikeyreference = \"\(CurrentUserIkey!)\" ")
        
        print("StoryIkeyQuery: \"\(StoryIkeyQuery)\" ")
        
        if (StoryWindow.window?.title == "BLANK STORY NAME") {
            print("Load Locations - BLANK STORY NAME")
            return
        }
        
        var StoryIkeyDB = String()
        
        do {
            let StoryIkeyQueryRun = try db.executeQuery(StoryIkeyQuery, values:nil)
            while StoryIkeyQueryRun.next() {
                StoryIkeyDB = StoryIkeyQueryRun.string(forColumn: "ikey")!
            }
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "StoryAssociationsViewController-\(#function) Query Failed:  \"\(StoryIkeyQuery)\" ")
            DoAlert.DisplayAlert(Class: "StoryAssociationsViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"\(StoryIkeyQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
        }
        
        print("StoryIkeyDB: \"\(StoryIkeyDB)\" ")
        
        ////////////////
        
        let StoryIdeaIkeysQuery = String("select * from storyideastable where storyassociatedikey not like '%%\(StoryIkeyDB)%%' and userikeyreference = \"\(CurrentUserIkey!)\" ")
        
        print("StoryIdeaIkeysQuery: \"\(StoryIdeaIkeysQuery)\" ")
        
        if (StoryWindow.window?.title == "BLANK STORY NAME") {
            print("Load Locations - BLANK STORY NAME")
            return
        }
        
       
        var NameDB = String()
        var CategoryDB = String()
        var IkeyDB = String()
        
        do {
            
            let StoryIdeaIkeysQueryRun = try db.executeQuery(StoryIdeaIkeysQuery, values:nil)
            while StoryIdeaIkeysQueryRun.next() {
                NameDB = StoryIdeaIkeysQueryRun.string(forColumn: "name")!
                CategoryDB = StoryIdeaIkeysQueryRun.string(forColumn: "categoryikey")!
                IkeyDB = StoryIdeaIkeysQueryRun.string(forColumn: "ikey")!
                
                
                if (CategoryDB == "1") {
                    CategoryDB = "Arcs"
                } else if (CategoryDB == "2") {
                    CategoryDB = "Starts"
                } else if (CategoryDB == "3") {
                    CategoryDB = "Mids"
                } else if (CategoryDB == "4") {
                    CategoryDB = "Endings"
                } else if (CategoryDB == "5") {
                    CategoryDB = "Scenes"
                } else {
                    CategoryDB = "Unkown"
                }
                
                print("IkeyDB: \"\(IkeyDB)\" NameDB: \"\(NameDB)\" CategoryDB: \"\(CategoryDB)\" ")
                
                
                let StoryIdeasToAdd = SAStoryIdeaList.init(Ikey: IkeyDB, Name: NameDB, Category: CategoryDB)
                StoryIdeasArray.append(StoryIdeasToAdd)
                
            }
            
            
            
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "StoryAssociationsViewController-\(#function) Query Failed:  \"\(StoryIdeaIkeysQuery)\" ")
            DoAlert.DisplayAlert(Class: "StoryAssociationsViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"\(StoryIdeaIkeysQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
        }
        
        print("StoryIkeyDB: \"\(StoryIkeyDB)\" ")
        print("StoryIdeasArray: \"\(StoryIdeasArray.count)\" ")
        
        
        
        StoryAssociationsStoryIdeaListTableView.reloadData()
        
        
        
        
        
    }// end of LoadStoryIdeasTable()
    
    func LoadStoryIdeasAddedTable() {
        
        
        let StoryIkeyQuery = String("SELECT ikey from storytable where storyname = \"\(StoryWindow.window?.title ?? "BLANK STORY NAME")\" and userikeyreference = \"\(CurrentUserIkey!)\" ")
        
        print("StoryIkeyQuery: \"\(StoryIkeyQuery)\" ")
        
        if (StoryWindow.window?.title == "BLANK STORY NAME") {
            print("Load Locations - BLANK STORY NAME")
            return
        }
        
        var StoryIkeyDB = String()
        
        do {
            let StoryIkeyQueryRun = try db.executeQuery(StoryIkeyQuery, values:nil)
            while StoryIkeyQueryRun.next() {
                StoryIkeyDB = StoryIkeyQueryRun.string(forColumn: "ikey")!
            }
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "StoryAssociationsViewController-\(#function) Query Failed:  \"\(StoryIkeyQuery)\" ")
            DoAlert.DisplayAlert(Class: "StoryAssociationsViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"\(StoryIkeyQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
        }
        
        print("StoryIkeyDB: \"\(StoryIkeyDB)\" ")
        
        ////////////////
        
        let StoryIdeaIkeysQuery = String("select * from storyideastable where storyassociatedikey like '%%\(StoryIkeyDB)%%' and userikeyreference = \"\(CurrentUserIkey!)\" ")
        
        print("StoryIdeaIkeysQuery: \"\(StoryIdeaIkeysQuery)\" ")
        
        if (StoryWindow.window?.title == "BLANK STORY NAME") {
            print("Load Locations - BLANK STORY NAME")
            return
        }
        
        
        var NameDB = String()
        var CategoryDB = String()
        var IkeyDB = String()
        
        do {
            
            let StoryIdeaIkeysQueryRun = try db.executeQuery(StoryIdeaIkeysQuery, values:nil)
            while StoryIdeaIkeysQueryRun.next() {
                NameDB = StoryIdeaIkeysQueryRun.string(forColumn: "name")!
                CategoryDB = StoryIdeaIkeysQueryRun.string(forColumn: "categoryikey")!
                IkeyDB = StoryIdeaIkeysQueryRun.string(forColumn: "ikey")!
                
                
                if (CategoryDB == "1") {
                    CategoryDB = "Arcs"
                } else if (CategoryDB == "2") {
                    CategoryDB = "Starts"
                } else if (CategoryDB == "3") {
                    CategoryDB = "Mids"
                } else if (CategoryDB == "4") {
                    CategoryDB = "Endings"
                } else if (CategoryDB == "5") {
                    CategoryDB = "Scenes"
                } else {
                    CategoryDB = "Unkown"
                }
                
                print("IkeyDB: \"\(IkeyDB)\" NameDB: \"\(NameDB)\" CategoryDB: \"\(CategoryDB)\" ")
                
                
                let StoryIdeasToAdd = SAStoryIdeaList.init(Ikey: IkeyDB, Name: NameDB, Category: CategoryDB)
                StoryIdeasAddedArray.append(StoryIdeasToAdd)
                
            }
            
            
            
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "StoryAssociationsViewController-\(#function) Query Failed:  \"\(StoryIdeaIkeysQuery)\" ")
            DoAlert.DisplayAlert(Class: "StoryAssociationsViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"\(StoryIdeaIkeysQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
        }
        
        //print("StoryIkeyDB: \"\(StoryIkeyDB)\" ")
        print("StoryIdeasArray: \"\(StoryIdeasAddedArray.count)\" ")
        
        
        
        StoryAssociationsStoryIdeaAddedListTableView.reloadData()
        
        
        
    }// end of LoadStoryIdeasAddedTable()
    
    
    func LoadLocationTable() {
        
        
        let StoryIkeyQuery = String("SELECT ikey from storytable where storyname = \"\(StoryWindow.window?.title ?? "BLANK STORY NAME")\" and userikeyreference = \"\(CurrentUserIkey!)\" ")
        
        print("StoryIkeyQuery: \"\(StoryIkeyQuery)\" ")
        
        if (StoryWindow.window?.title == "BLANK STORY NAME") {
            
            print("Load Locations - BLANK STORY NAME")
            return
            
        }
        
        
        var StoryIkeyDB = String()
        
        do {
            let StoryIkeyQueryRun = try db.executeQuery(StoryIkeyQuery, values:nil)
            
            while StoryIkeyQueryRun.next() {
                StoryIkeyDB = StoryIkeyQueryRun.string(forColumn: "ikey")!
                
            }
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "StoryAssociationsViewController-\(#function) Query Failed:  \"\(StoryIkeyQuery)\" ")
            DoAlert.DisplayAlert(Class: "StoryAssociationsViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"\(StoryIkeyQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
        }
        
        print("StoryIkeyDB: \"\(StoryIkeyDB)\" ")
        
        ////////////////
        
        let LocationTableQuery = String("select * from locationtable where userikeyreference = \"\(CurrentUserIkey!)\" and associatedstoryikey not like '%%\(StoryIkeyDB)%%' ")
        
        print("LocationTableQuery: \"\(LocationTableQuery)\" ")
        
        var nameDB = String()
        var ikeyDB = String()
        var locationDB = String()
        
        do {
            let LocationTableQueryRun = try db.executeQuery(LocationTableQuery, values:nil)
            
            while LocationTableQueryRun.next() {
                
                nameDB = LocationTableQueryRun.string(forColumn: "name")!
                ikeyDB = LocationTableQueryRun.string(forColumn: "ikey")!
                locationDB = LocationTableQueryRun.string(forColumn: "location")!
                
                print("ADDED - ikeyDB: \"\(ikeyDB)\" nameDB: \"\(nameDB)\"  locationDB: \"\(locationDB)\"")
                
                let locationToAdd = SALocationList.init(Ikey: ikeyDB, Name: nameDB, Description: locationDB)
                LocationsArray.append(locationToAdd)
                
                
            }
            
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "StoryAssociationsViewController-\(#function) Query Failed:  \"\(LocationTableQuery)\" ")
            DoAlert.DisplayAlert(Class: "StoryAssociationsViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"\(LocationTableQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
        }
        
        print("LOAD: LocationsArray: \"\(LocationsArray.count)\"")
        
        
        
        StoryAssociationsLocationListTableView.reloadData()
        
        
        
    }// end of LoadLocationTables()
    
    func LoadLocationAddedTable() {
        
        
        
        let StoryIkeyQuery = String("SELECT ikey from storytable where storyname = \"\(StoryWindow.window?.title ?? "BLANK STORY NAME")\" and userikeyreference = \"\(CurrentUserIkey!)\" ")
        
        print("StoryIkeyQuery: \"\(StoryIkeyQuery)\" ")
        
        if (StoryWindow.window?.title == "BLANK STORY NAME") {
            
            print("Load Locations - BLANK STORY NAME")
            return
            
        }
        
        
        var StoryIkeyDB = String()
        
        do {
            let StoryIkeyQueryRun = try db.executeQuery(StoryIkeyQuery, values:nil)
            
            while StoryIkeyQueryRun.next() {
                StoryIkeyDB = StoryIkeyQueryRun.string(forColumn: "ikey")!
                
            }
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "StoryAssociationsViewController-\(#function) Query Failed:  \"\(StoryIkeyQuery)\" ")
            DoAlert.DisplayAlert(Class: "StoryAssociationsViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"\(StoryIkeyQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
        }
        
        print("StoryIkeyDB: \"\(StoryIkeyDB)\" ")
        
        ////////////////
        
        let LocationTableQuery = String("select * from locationtable where userikeyreference = \"\(CurrentUserIkey!)\" and associatedstoryikey like '%%\(StoryIkeyDB)%%' ")
        
        print("LocationAddedTableQuery: \"\(LocationTableQuery)\" ")
        
        var nameDB = String()
        var ikeyDB = String()
        var locationDB = String()
        
        do {
            let LocationTableQueryRun = try db.executeQuery(LocationTableQuery, values:nil)
            
            while LocationTableQueryRun.next() {
                
                nameDB = LocationTableQueryRun.string(forColumn: "name")!
                ikeyDB = LocationTableQueryRun.string(forColumn: "ikey")!
                locationDB = LocationTableQueryRun.string(forColumn: "location")!
                
                print("ADDED - ikeyDB: \"\(ikeyDB)\" nameDB: \"\(nameDB)\"  locationDB: \"\(locationDB)\"")
                
                let locationToAdd = SALocationList.init(Ikey: ikeyDB, Name: nameDB, Description: locationDB)
                LocationsAddedArray.append(locationToAdd)
                
                
            }
            
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "StoryAssociationsViewController-\(#function) Query Failed:  \"\(LocationTableQuery)\" ")
            DoAlert.DisplayAlert(Class: "StoryAssociationsViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"\(LocationTableQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
        }
        
        print("LocationsAddedArray Count: \"\(LocationsAddedArray.count)\"")
        
        StoryAssociationsLocationAddedListTableView.reloadData()
        
    } // end of LoadLocationAddedTable()
    
    
    
    ////////////////////////////////////////////////
    
    
    
    
    
    
    @IBAction func SALocationsAddButton(_ sender: Any) {
        
        
        let SelectedRow = StoryAssociationsLocationListTableView.selectedRow

        //print("SelectedRow: \(SelectedRow)")
        
        if (SelectedRow == -1) {
            return
        }
        
        let Name = LocationsArray[SelectedRow].Name
        let Ikey = LocationsArray[SelectedRow].Ikey
        let Description = LocationsArray[SelectedRow].Description
        
        print("Table StoryAssociationsCharacterListTableView - Ikey: \"\(Ikey)\" Name: \"\(Name)\" Description: \"\(Description)\"")
        
        //////////////////////////////////////////////
        
        let StoryIkeyQuery = String("SELECT * from storytable where storyname = \"\(StoryWindow.window?.title ?? "BLANK STORY NAME")\" and userikeyreference = \"\(CurrentUserIkey!)\"")
        
        print("StoryIkeyQuery: \"\(StoryIkeyQuery)\" ")

        if (StoryWindow.window?.title == "BLANK STORY NAME") {

            print("Load Locations - BLANK STORY NAME")
            return

        }


        var StoryIkeyDB = String()

        do {
            let StoryIkeyQueryRun = try db.executeQuery(StoryIkeyQuery, values:nil)

            while StoryIkeyQueryRun.next() {
                StoryIkeyDB = StoryIkeyQueryRun.string(forColumn: "ikey")!

            }
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "StoryAssociationsViewController-\(#function) Query Failed:  \"\(StoryIkeyQuery)\" ")
            DoAlert.DisplayAlert(Class: "StoryAssociationsViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"\(StoryIkeyQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
        }

        print("StoryIkeyDB: \"\(StoryIkeyDB)\" ")
        
        /////////////////////////////////////////////
        
        
        
        let GetLocationAssociatedStories = String("SELECT * from locationtable where userikeyreference = \"\(CurrentUserIkey!)\" and name = \"\(Name)\" ")
        
        print("GetLocationAssociatedStories: \"\(GetLocationAssociatedStories)\" ")
    
        var SALocationIkeyDB = String()
        var LocationAssociatedIkeysDB = String()
        
        do {
            let GetLocationAssociatedStoriesRun = try db.executeQuery(GetLocationAssociatedStories, values:nil)
            
            while GetLocationAssociatedStoriesRun.next() {
                LocationAssociatedIkeysDB = GetLocationAssociatedStoriesRun.string(forColumn: "associatedstoryikey")!
                SALocationIkeyDB = GetLocationAssociatedStoriesRun.string(forColumn: "ikey")!
            }
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "StoryAssociationsViewController-\(#function) Query Failed:  \"\(GetLocationAssociatedStories)\" ")
            DoAlert.DisplayAlert(Class: "StoryAssociationsViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"\(GetLocationAssociatedStories)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
        }
        
        print("LocationAssociatedIkeysDB: \"\(LocationAssociatedIkeysDB)\" \"\(SALocationIkeyDB)\"")
        
        //////////////////////////////////////////////
        
        
        var LocationAssociatedIkeysArray = LocationAssociatedIkeysDB.components(separatedBy: ",")
        
        print("LocationAssociatedIkeysArray: \"\(LocationAssociatedIkeysArray)\" ")
        
        LocationAssociatedIkeysArray.append(StoryIkeyDB)
        
        print("LocationAssociatedIkeysArray Added Ikey: \"\(LocationAssociatedIkeysArray)\" ")
        
        
        //////////////////////////////////////////////
        
        let NewStringToDB = LocationAssociatedIkeysArray.joined(separator: ",")
        
        print("NewStringToDB Final: \"\(NewStringToDB)\" ")
        
//        str2.insert(separator: ":", every: 2)
        
        
        
        
        //////////////////////////////////////////////
        
        var UpdateLocationAssociatedStoriesSqlquery = String()

        do {
            UpdateLocationAssociatedStoriesSqlquery = String(format: "Update locationtable SET associatedstoryikey = \"\(NewStringToDB)\" where userikeyreference = \"\(CurrentUserIkey!)\" and name = \"\(Name)\" ")

            print("UpdateLocationAssociatedStoriesSqlquery: \(UpdateLocationAssociatedStoriesSqlquery)")

            try db.executeUpdate(UpdateLocationAssociatedStoriesSqlquery, values: nil)

        } catch let error as NSError {

            print("ERROR: \(error)\n\n\(db.lastErrorMessage())")
            loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(UpdateLocationAssociatedStoriesSqlquery)\"")
            DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(UpdateLocationAssociatedStoriesSqlquery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
            return
        }
        
        //////////////////////////////////////////////
        
        LocationsArray.removeAll()
        LocationsAddedArray.removeAll()
        
        StoryAssociationsLocationListTableView.reloadData()
        StoryAssociationsLocationAddedListTableView.reloadData()
        
        //////////////////////////////////////////////
        
        LoadLocationTable()
        LoadLocationAddedTable()
        
        
        
    } // end of SALocationsAddButton
    
    @IBAction func SALocationsRemoveButton(_ sender: Any) {
        
        let SelectedRow = StoryAssociationsLocationAddedListTableView.selectedRow

        print("SelectedRow: \(SelectedRow)")
        
        if (SelectedRow == -1) {
            return
        }
        
        let Name = LocationsAddedArray[SelectedRow].Name
        let Ikey = LocationsAddedArray[SelectedRow].Ikey
        let Description = LocationsAddedArray[SelectedRow].Description
        
        print("Table StoryAssociationsCharacterListTableView - Ikey: \"\(Ikey)\" Name: \"\(Name)\" Description: \"\(Description)\"")
        
        //////////////////////////////////////////////
        
        let StoryIkeyQuery = String("SELECT * from storytable where storyname = \"\(StoryWindow.window?.title ?? "BLANK STORY NAME")\" and userikeyreference = \"\(CurrentUserIkey!)\"")
        
        print("StoryIkeyQuery: \"\(StoryIkeyQuery)\" ")
        
        if (StoryWindow.window?.title == "BLANK STORY NAME") {
            
            print("Load Locations - BLANK STORY NAME")
            return
            
        }
        
        
        var StoryIkeyDB = String()
        
        do {
            let StoryIkeyQueryRun = try db.executeQuery(StoryIkeyQuery, values:nil)
            
            while StoryIkeyQueryRun.next() {
                StoryIkeyDB = StoryIkeyQueryRun.string(forColumn: "ikey")!
                
            }
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "StoryAssociationsViewController-\(#function) Query Failed:  \"\(StoryIkeyQuery)\" ")
            DoAlert.DisplayAlert(Class: "StoryAssociationsViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"\(StoryIkeyQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
            return
        }
        
        print("StoryIkeyDB: \"\(StoryIkeyDB)\" ")
        
        /////////////////////////////////////////////
        
        
        
        let GetLocationAssociatedStories = String("SELECT * from locationtable where userikeyreference = \"\(CurrentUserIkey!)\" and name = \"\(Name)\" ")
        
        print("GetLocationAssociatedStories: \"\(GetLocationAssociatedStories)\" ")
        
        var SALocationIkeyDB = String()
        var LocationAssociatedIkeysDB = String()
        
        do {
            let GetLocationAssociatedStoriesRun = try db.executeQuery(GetLocationAssociatedStories, values:nil)
            
            while GetLocationAssociatedStoriesRun.next() {
                LocationAssociatedIkeysDB = GetLocationAssociatedStoriesRun.string(forColumn: "associatedstoryikey")!
                SALocationIkeyDB = GetLocationAssociatedStoriesRun.string(forColumn: "ikey")!
            }
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "StoryAssociationsViewController-\(#function) Query Failed:  \"\(GetLocationAssociatedStories)\" ")
            DoAlert.DisplayAlert(Class: "StoryAssociationsViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"\(GetLocationAssociatedStories)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
            return
        }
        
        print("LocationAssociatedIkeysDB: \"\(LocationAssociatedIkeysDB)\" \"\(SALocationIkeyDB)\"")
        
        //////////////////////////////////////////////
        
        
        var LocationAssociatedIkeysArray = LocationAssociatedIkeysDB.components(separatedBy: ",")
        
        print("LocationAssociatedIkeysArray: \"\(LocationAssociatedIkeysArray)\" ")
        
        //LocationAssociatedIkeysArray.append(StoryIkeyDB)
        
        if let indexOfItem = LocationAssociatedIkeysArray.index(of: StoryIkeyDB) {
            print("Found at index \(indexOfItem)")
            LocationAssociatedIkeysArray.remove(at: indexOfItem)
        }
        
        
        print("LocationAssociatedIkeysArray RREMOVED Ikey: \"\(LocationAssociatedIkeysArray)\" ")
        
        
        //////////////////////////////////////////////
        
        let NewStringToDB = LocationAssociatedIkeysArray.joined(separator: ",")
        
        print("NewStringToDB Final: \"\(NewStringToDB)\" ")
        
        //        str2.insert(separator: ":", every: 2)
        
        
        
        
        //////////////////////////////////////////////
        
        var UpdateLocationAssociatedStoriesSqlquery = String()
        
        do {
            UpdateLocationAssociatedStoriesSqlquery = String(format: "Update locationtable SET associatedstoryikey = \"\(NewStringToDB)\" where userikeyreference = \"\(CurrentUserIkey!)\" and ikey = \"\(SALocationIkeyDB)\" ")
            
            print("UpdateLocationAssociatedStoriesSqlquery: \(UpdateLocationAssociatedStoriesSqlquery)")
            
            try db.executeUpdate(UpdateLocationAssociatedStoriesSqlquery, values: nil)
            
        } catch let error as NSError {
            
            print("ERROR: \(error)\n\n\(db.lastErrorMessage())")
            loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(UpdateLocationAssociatedStoriesSqlquery)\"")
            DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(UpdateLocationAssociatedStoriesSqlquery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
            return
        }
        
        //////////////////////////////////////////////
        
        LocationsArray.removeAll()
        LocationsAddedArray.removeAll()
        
        StoryAssociationsLocationListTableView.reloadData()
        StoryAssociationsLocationAddedListTableView.reloadData()
        
        //////////////////////////////////////////////
        
        LoadLocationTable()
        LoadLocationAddedTable()
 
    }// end of SALocationsRemoveButton
    
    @IBAction func SALocationsOpenButton(_ sender: Any) {
   
        
        print("LocationName: \(LocationName) LocationIkey: \(LocationIkey)")
        
        let NILName = defined(str: LocationName)
        let NILIkey = defined(str: LocationIkey)
        
        print("NILName: \"\(NILName)\"  NILIkey: \"\(NILIkey)\" ")
        
        // true mean its nil
        if (NILName == true || NILIkey == true ) {
            
            let alert = NSAlert()
            alert.messageText = "No Location was selected"
            alert.informativeText = "Try selecting a Locagion again."
            alert.alertStyle = .warning
            alert.addButton(withTitle: "Ok")

            
            
            if (CurrentAppearanceStoryAssociationsViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.window.titlebarAppearsTransparent = true
            
            alert.runModal()
            
//            let returnCode = alert.runModal()
//            if (returnCode.rawValue == 1000) {
//            }
            StoryAssociationsLocationListTableView.deselectAll(self)
            StoryAssociationsLocationAddedListTableView.deselectAll(self)
            
            return
            
        } else {
                LocationInfoWindow.showWindow(self)
        }

    }// end of SALocationsOpenButton
    
    @IBAction func SALocationsRefreshButton(_ sender: Any) {
        
        LocationsArray.removeAll()
        LocationsAddedArray.removeAll()
        
        StoryAssociationsLocationListTableView.reloadData()
        StoryAssociationsLocationAddedListTableView.reloadData()
        
        LoadLocationTable()
        LoadLocationAddedTable()
        
    }
    
    
    
    
    // Story Idea Tab Buttons
    ////////////////////////////////////////////////
    
    
    
    @IBAction func SAStoryIdeaAddStoryIdeaButton(_ sender: Any) {
        
        // StoryAssociationsStoryIdeaAddedListTableView
        let SelectedRow = StoryAssociationsStoryIdeaListTableView.selectedRow
        
        //print("SelectedRow: \(SelectedRow)")
        
        if (SelectedRow == -1) {
            return
        }
        
        // StoryIdeasAddedArray
        let Name = StoryIdeasArray[SelectedRow].Name
        let Ikey = StoryIdeasArray[SelectedRow].Ikey
        let Description = StoryIdeasArray[SelectedRow].Category
        
        print("Table StoryAssociationsStoryIdeaListTableView - Ikey: \"\(Ikey)\" Name: \"\(Name)\" Description: \"\(Description)\"")
        
        //////////////////////////////////////////////
        
        let StoryIkeyQuery = String("SELECT * from storytable where storyname = \"\(StoryWindow.window?.title ?? "BLANK STORY NAME")\" and userikeyreference = \"\(CurrentUserIkey!)\"")
        
        print("StoryIkeyQuery: \"\(StoryIkeyQuery)\" ")
        
        if (StoryWindow.window?.title == "BLANK STORY NAME") {
            
            print("Load Locations - BLANK STORY NAME")
            return
            
        }
        
        
        var StoryIkeyDB = String()
        
        do {
            let StoryIkeyQueryRun = try db.executeQuery(StoryIkeyQuery, values:nil)
            
            while StoryIkeyQueryRun.next() {
                StoryIkeyDB = StoryIkeyQueryRun.string(forColumn: "ikey")!
                
            }
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "StoryAssociationsViewController-\(#function) Query Failed:  \"\(StoryIkeyQuery)\" ")
            DoAlert.DisplayAlert(Class: "StoryAssociationsViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"\(StoryIkeyQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
        }
        
        print("StoryIkeyDB: \"\(StoryIkeyDB)\" ")
        
        /////////////////////////////////////////////
        

        
        // select * from storyideastable where userikeyreference = \"\(CurrentUserIkey!)\" and storyassociatedikey not like '%%\(StoryIkeyDB)%%' order by categoryikey
        
        let GetStoryIdeaAssociatedStories = String("select * from storyideastable where ikey = \"\(Ikey)\" and userikeyreference = \"\(CurrentUserIkey!)\" and storyassociatedikey not like '%%\(StoryIkeyDB)%%' ")
        
        print("GetStoryIdeaAssociatedStories: \"\(GetStoryIdeaAssociatedStories)\" ")
        
        //var StoryIdeaIkeyDB = String()
        var StoryIdeaNameDB = String()
        var StoryIdeaCategoryIkeyDB = String()
        
        do {
            let GetLocationAssociatedStoriesRun = try db.executeQuery(GetStoryIdeaAssociatedStories, values:nil)
            
            while GetLocationAssociatedStoriesRun.next() {
                StoryIdeaNameDB = GetLocationAssociatedStoriesRun.string(forColumn: "name")!
                StoryIdeaCategoryIkeyDB = GetLocationAssociatedStoriesRun.string(forColumn: "storyassociatedikey")!
                //StoryIdeaIkeyDB = GetLocationAssociatedStoriesRun.string(forColumn: "ikey")!
            }
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "StoryAssociationsViewController-\(#function) Query Failed:  \"\(GetStoryIdeaAssociatedStories)\" ")
            DoAlert.DisplayAlert(Class: "StoryAssociationsViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"\(GetStoryIdeaAssociatedStories)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
        }
        
        //print("LocationAssociatedIkeysDB: \"\(LocationAssociatedIkeysDB)\" \"\(SALocationIkeyDB)\"")
        
        //////////////////////////////////////////////
        
        var StoryIdeaAssociatedIkeysArray = StoryIdeaCategoryIkeyDB.components(separatedBy: ",")
        
        print("StoryIdeaAssociatedIkeysArray: \"\(StoryIdeaAssociatedIkeysArray)\" ")
        
        StoryIdeaAssociatedIkeysArray.append(StoryIkeyDB)
        
        print("StoryIdeaAssociatedIkeysArray Added Ikey: \"\(StoryIdeaAssociatedIkeysArray)\" ")
        
        //////////////////////////////////////////////
        
        let NewStringToDB = StoryIdeaAssociatedIkeysArray.joined(separator: ",")
        
        print("NewStringToDB Final: \"\(NewStringToDB)\" ")

        //////////////////////////////////////////////
        
        var UpdateStoryIdeaAssociatedStoriesSqlquery = String()
        
        do {
            UpdateStoryIdeaAssociatedStoriesSqlquery = String(format: "UPDATE storyideastable set storyassociatedikey = \"\(NewStringToDB)\" where userikeyreference = \"\(CurrentUserIkey!)\" and name = \"\(StoryIdeaNameDB)\"")
            
            print("UpdateStoryIdeaAssociatedStoriesSqlquery: \(UpdateStoryIdeaAssociatedStoriesSqlquery)")
            
            try db.executeUpdate(UpdateStoryIdeaAssociatedStoriesSqlquery, values: nil)
            
        } catch let error as NSError {
            
            print("ERROR: \(error)\n\n\(db.lastErrorMessage())")
            loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(UpdateStoryIdeaAssociatedStoriesSqlquery)\"")
            DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(UpdateStoryIdeaAssociatedStoriesSqlquery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
            return
        }
        
        //////////////////////////////////////////////
        
        StoryIdeasArray.removeAll()
        StoryIdeasAddedArray.removeAll()
        
        StoryAssociationsStoryIdeaListTableView.reloadData()
        StoryAssociationsStoryIdeaAddedListTableView.reloadData()
        
        //////////////////////////////////////////////
        
        LoadStoryIdeasTable()
        LoadStoryIdeasAddedTable()
        
        
    }
    
    @IBAction func SAStoryIdeaRemoveStoryIdeaButton(_ sender: Any) {
        
        
        // StoryAssociationsStoryIdeaAddedListTableView
        let SelectedRow = StoryAssociationsStoryIdeaAddedListTableView.selectedRow
        
        //print("SelectedRow: \(SelectedRow)")
        
        if (SelectedRow == -1) {
            return
        }
        
        // StoryIdeasAddedArray
        let Name = StoryIdeasAddedArray[SelectedRow].Name
        let Ikey = StoryIdeasAddedArray[SelectedRow].Ikey
        let Description = StoryIdeasAddedArray[SelectedRow].Category
        
        print("Table StoryAssociationsStoryIdeaListTableView - Ikey: \"\(Ikey)\" Name: \"\(Name)\" Description: \"\(Description)\"")
        
        //////////////////////////////////////////////
        
        let StoryIkeyQuery = String("SELECT * from storytable where storyname = \"\(StoryWindow.window?.title ?? "BLANK STORY NAME")\" and userikeyreference = \"\(CurrentUserIkey!)\"")
        
        print("StoryIkeyQuery: \"\(StoryIkeyQuery)\" ")
        
        if (StoryWindow.window?.title == "BLANK STORY NAME") {
            
            print("Load Locations - BLANK STORY NAME")
            return
            
        }
        
        
        var StoryIkeyDB = String()
        
        do {
            let StoryIkeyQueryRun = try db.executeQuery(StoryIkeyQuery, values:nil)
            
            while StoryIkeyQueryRun.next() {
                StoryIkeyDB = StoryIkeyQueryRun.string(forColumn: "ikey")!
                
            }
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "StoryAssociationsViewController-\(#function) Query Failed:  \"\(StoryIkeyQuery)\" ")
            DoAlert.DisplayAlert(Class: "StoryAssociationsViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"\(StoryIkeyQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
        }
        
        print("StoryIkeyDB: \"\(StoryIkeyDB)\" ")
        
        /////////////////////////////////////////////
        
        
        
        // select * from storyideastable where userikeyreference = \"\(CurrentUserIkey!)\" and storyassociatedikey not like '%%\(StoryIkeyDB)%%' order by categoryikey
        
        let GetStoryIdeaAssociatedStories = String("select * from storyideastable where ikey = \"\(Ikey)\" and userikeyreference = \"\(CurrentUserIkey!)\" and storyassociatedikey not like '%%\(StoryIkeyDB)%%' ")
        
        print("GetStoryIdeaAssociatedStories: \"\(GetStoryIdeaAssociatedStories)\" ")
        
        //var StoryIdeaIkeyDB = String()
        //var StoryIdeaNameDB = String()
        var StoryIdeaCategoryIkeyDB = String()
        
        do {
            let GetLocationAssociatedStoriesRun = try db.executeQuery(GetStoryIdeaAssociatedStories, values:nil)
            
            while GetLocationAssociatedStoriesRun.next() {
                //StoryIdeaNameDB = GetLocationAssociatedStoriesRun.string(forColumn: "name")!
                StoryIdeaCategoryIkeyDB = GetLocationAssociatedStoriesRun.string(forColumn: "storyassociatedikey")!
                //StoryIdeaIkeyDB = GetLocationAssociatedStoriesRun.string(forColumn: "ikey")!
            }
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "StoryAssociationsViewController-\(#function) Query Failed:  \"\(GetStoryIdeaAssociatedStories)\" ")
            DoAlert.DisplayAlert(Class: "StoryAssociationsViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"\(GetStoryIdeaAssociatedStories)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
        }
        
        //print("LocationAssociatedIkeysDB: \"\(LocationAssociatedIkeysDB)\" \"\(SALocationIkeyDB)\"")
        
        //////////////////////////////////////////////
        
        var StoryIdeaAssociatedIkeysArray = StoryIdeaCategoryIkeyDB.components(separatedBy: ",")
        
        print("StoryIdeaAssociatedIkeysArray: \"\(StoryIdeaAssociatedIkeysArray)\" ")
        
        //StoryIdeaAssociatedIkeysArray.append(StoryIkeyDB)
        if let indexOfItem = StoryIdeaAssociatedIkeysArray.index(of: StoryIkeyDB) {
            print("Found at index \(indexOfItem)")
            StoryIdeaAssociatedIkeysArray.remove(at: indexOfItem)
        }
        
        print("StoryIdeaAssociatedIkeysArray Added Ikey: \"\(StoryIdeaAssociatedIkeysArray)\" ")
        
        //////////////////////////////////////////////
        
        let NewStringToDB = StoryIdeaAssociatedIkeysArray.joined(separator: ",")
        
        print("NewStringToDB Final: \"\(NewStringToDB)\" ")
        
        //////////////////////////////////////////////
        
        var UpdateStoryIdeaAssociatedStoriesSqlquery = String()
        
        do {
            UpdateStoryIdeaAssociatedStoriesSqlquery = String(format: "UPDATE storyideastable set storyassociatedikey = \"\(NewStringToDB)\" where userikeyreference = \"\(CurrentUserIkey!)\" and name = \"\(Name)\"")
            
            print("UpdateStoryIdeaAssociatedStoriesSqlquery: \(UpdateStoryIdeaAssociatedStoriesSqlquery)")
            
            try db.executeUpdate(UpdateStoryIdeaAssociatedStoriesSqlquery, values: nil)
            
        } catch let error as NSError {
            
            print("ERROR: \(error)\n\n\(db.lastErrorMessage())")
            loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(UpdateStoryIdeaAssociatedStoriesSqlquery)\"")
            DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(UpdateStoryIdeaAssociatedStoriesSqlquery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
            return
        }
        
        //////////////////////////////////////////////
        
        StoryIdeasArray.removeAll()
        StoryIdeasAddedArray.removeAll()
        
        StoryAssociationsStoryIdeaListTableView.reloadData()
        StoryAssociationsStoryIdeaAddedListTableView.reloadData()
        
        //////////////////////////////////////////////
        
        LoadStoryIdeasTable()
        LoadStoryIdeasAddedTable()
        
        
    }
    
    @IBAction func SAStoryIdeaOpenStoryIdeaButton(_ sender: Any) {
        
        
       
        
        ///////////////////////
        
      
        SharedItemLock = false
        SharedItemOwner = "\(CurrentUserIkey!)"
        SAStoryIdeaBool = true
        
        ////////////////////
        
 
        
        StoryIdeasWindow.showWindow(self)
        
        
        
    }
    
    @IBAction func SAStoryIdeaRefreshButton(_ sender: Any) {
        
        StoryIdeasArray.removeAll()
        StoryIdeasAddedArray.removeAll()
        
        StoryAssociationsStoryIdeaListTableView.reloadData()
        StoryAssociationsStoryIdeaAddedListTableView.reloadData()
        
        //////////////////////////////////////////////
        
        LoadStoryIdeasTable()
        LoadStoryIdeasAddedTable()
        
    }
    
    
    
    @IBAction func SACharacterRefreshButton(_ sender: Any) {
        
        CharactersArray.removeAll()
        CharactersAddedArray.removeAll()
        
        StoryAssociationsCharacterListTableView.reloadData()
        StoryAssociationsCharactersAddedListTableView.reloadData()
        
        //////////////////////////////////////////////
        
        LoadCharacterTable()
        LoadCharacterAddedTable()
        
    }
    
    @IBAction func SACharacterAddCharacterButton(_ sender: Any) {
        
        
        
        // StoryAssociationsCharacterListTableView
        let SelectedRow = StoryAssociationsCharacterListTableView.selectedRow
        
        //print("SelectedRow: \(SelectedRow)")
        
        if (SelectedRow == -1) {
            return
        }
        
        // StoryIdeasAddedArray
        let Ikey = CharactersArray[SelectedRow].Ikey
        let FullName = CharactersArray[SelectedRow].FullName
        
        
        print("Table StoryAssociationsCharacterListTableView - Ikey: \"\(Ikey)\" FullName: \"\(FullName)\" ")
        
        //////////////////////////////////////////////
        
        let StoryIkeyQuery = String("SELECT * from storytable where storyname = \"\(StoryWindow.window?.title ?? "BLANK STORY NAME")\" and userikeyreference = \"\(CurrentUserIkey!)\"")
        
        print("StoryIkeyQuery: \"\(StoryIkeyQuery)\" ")
        
        if (StoryWindow.window?.title == "BLANK STORY NAME") {
            
            print("Load Locations - BLANK STORY NAME")
            return
            
        }
        
        
        var StoryIkeyDB = String()
        
        do {
            let StoryIkeyQueryRun = try db.executeQuery(StoryIkeyQuery, values:nil)
            
            while StoryIkeyQueryRun.next() {
                StoryIkeyDB = StoryIkeyQueryRun.string(forColumn: "ikey")!
                
            }
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "StoryAssociationsViewController-\(#function) Query Failed:  \"\(StoryIkeyQuery)\" ")
            DoAlert.DisplayAlert(Class: "StoryAssociationsViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"\(StoryIkeyQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
        }
        
        print("StoryIkeyDB: \"\(StoryIkeyDB)\" ")
        
        /////////////////////////////////////////////
        
        
        
        // select * from storyideastable where userikeyreference = \"\(CurrentUserIkey!)\" and storyassociatedikey not like '%%\(StoryIkeyDB)%%' order by categoryikey
        
        let GetCharacterAssociatedStories = String("select * from charactertable where ikey = \"\(Ikey)\" and userikeyreference = \"\(CurrentUserIkey!)\" and storyassociatedikey not like '%%\(StoryIkeyDB)%%' ")
        
        print("GetCharacterAssociatedStories: \"\(GetCharacterAssociatedStories)\" ")
        
        
        
        var CharacterCategoryIkeyDB = String()
        var CharacterNameDB = String()
        
        do {
            let GetCharacterAssociatedStoriesRun = try db.executeQuery(GetCharacterAssociatedStories, values:nil)
            
            while GetCharacterAssociatedStoriesRun.next() {
                CharacterCategoryIkeyDB = GetCharacterAssociatedStoriesRun.string(forColumn: "storyassociatedikey")!
                CharacterNameDB = GetCharacterAssociatedStoriesRun.string(forColumn: "firstname")!
            }
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "StoryAssociationsViewController-\(#function) Query Failed:  \"\(GetCharacterAssociatedStories)\" ")
            DoAlert.DisplayAlert(Class: "StoryAssociationsViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"\(GetCharacterAssociatedStories)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
        }
        
        //print("LocationAssociatedIkeysDB: \"\(LocationAssociatedIkeysDB)\" \"\(SALocationIkeyDB)\"")
        
        //////////////////////////////////////////////
        
        var CharacterAssociatedIkeysArray = CharacterCategoryIkeyDB.components(separatedBy: ",")
        
        print("CharacterAssociatedIkeysArray: \"\(CharacterAssociatedIkeysArray)\" ")
        
        CharacterAssociatedIkeysArray.append(StoryIkeyDB)
        
        print("CharacterAssociatedIkeysArray Added Ikey: \"\(CharacterAssociatedIkeysArray)\" ")
        
        //////////////////////////////////////////////
        
        let NewStringToDB = CharacterAssociatedIkeysArray.joined(separator: ",")
        
        print("NewStringToDB Final: \"\(NewStringToDB)\" ")
        
        //////////////////////////////////////////////
        
        var UpdateCharacterAssociatedStoriesSqlquery = String()
        
        do {
            UpdateCharacterAssociatedStoriesSqlquery = String(format: "UPDATE charactertable set storyassociatedikey = \"\(NewStringToDB)\" where ikey = \"\(Ikey)\" and userikeyreference = \"\(CurrentUserIkey!)\" and firstname = \"\(CharacterNameDB)\"")
            
            print("UpdateCharacterAssociatedStoriesSqlquery: \(UpdateCharacterAssociatedStoriesSqlquery)")
            
            try db.executeUpdate(UpdateCharacterAssociatedStoriesSqlquery, values: nil)
            
        } catch let error as NSError {
            
            print("ERROR: \(error)\n\n\(db.lastErrorMessage())")
            loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(UpdateCharacterAssociatedStoriesSqlquery)\"")
            DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(UpdateCharacterAssociatedStoriesSqlquery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
            return
        }
        
        //////////////////////////////////////////////
        
        CharactersArray.removeAll()
        CharactersAddedArray.removeAll()
        
        StoryAssociationsCharacterListTableView.reloadData()
        StoryAssociationsCharactersAddedListTableView.reloadData()
        
        //////////////////////////////////////////////
        
        LoadCharacterTable()
        LoadCharacterAddedTable()
        
        
        
        
    }
    
    @IBAction func SACharacterRemoveButton(_ sender: Any) {
        
        
        // StoryAssociationsCharacterListTableView
        let SelectedRow = StoryAssociationsCharactersAddedListTableView.selectedRow
        
        //print("SelectedRow: \(SelectedRow)")
        
        if (SelectedRow == -1) {
            return
        }
        
        // StoryIdeasAddedArray
        let Ikey = CharactersAddedArray[SelectedRow].Ikey
        let FullName = CharactersAddedArray[SelectedRow].FullName
        
        
        print("Table StoryAssociationsCharacterListTableView - Ikey: \"\(Ikey)\" FullName: \"\(FullName)\" ")
        
        //////////////////////////////////////////////
        
        let StoryIkeyQuery = String("SELECT * from storytable where storyname = \"\(StoryWindow.window?.title ?? "BLANK STORY NAME")\" and userikeyreference = \"\(CurrentUserIkey!)\"")
        
        print("StoryIkeyQuery: \"\(StoryIkeyQuery)\" ")
        
        if (StoryWindow.window?.title == "BLANK STORY NAME") {
            
            print("Load Locations - BLANK STORY NAME")
            return
            
        }
        
        
        var StoryIkeyDB = String()
        
        do {
            let StoryIkeyQueryRun = try db.executeQuery(StoryIkeyQuery, values:nil)
            
            while StoryIkeyQueryRun.next() {
                StoryIkeyDB = StoryIkeyQueryRun.string(forColumn: "ikey")!
                
            }
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "StoryAssociationsViewController-\(#function) Query Failed:  \"\(StoryIkeyQuery)\" ")
            DoAlert.DisplayAlert(Class: "StoryAssociationsViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"\(StoryIkeyQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
        }
        
        print("StoryIkeyDB: \"\(StoryIkeyDB)\" ")
        
        /////////////////////////////////////////////
        
        
        
        // select * from storyideastable where userikeyreference = \"\(CurrentUserIkey!)\" and storyassociatedikey not like '%%\(StoryIkeyDB)%%' order by categoryikey
        
        let GetCharacterAssociatedStories = String("select * from charactertable where ikey = \"\(Ikey)\" and userikeyreference = \"\(CurrentUserIkey!)\" and storyassociatedikey like '%%\(StoryIkeyDB)%%' ")
        
        print("GetCharacterAssociatedStories: \"\(GetCharacterAssociatedStories)\" ")
        
        
        
        var CharacterCategoryIkeyDB = String()
        var CharacterNameDB = String()
        
        do {
            let GetCharacterAssociatedStoriesRun = try db.executeQuery(GetCharacterAssociatedStories, values:nil)
            
            while GetCharacterAssociatedStoriesRun.next() {
                CharacterCategoryIkeyDB = GetCharacterAssociatedStoriesRun.string(forColumn: "storyassociatedikey")!
                CharacterNameDB = GetCharacterAssociatedStoriesRun.string(forColumn: "firstname")!
            }
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "StoryAssociationsViewController-\(#function) Query Failed:  \"\(GetCharacterAssociatedStories)\" ")
            DoAlert.DisplayAlert(Class: "StoryAssociationsViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"\(GetCharacterAssociatedStories)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
        }
        
        //print("LocationAssociatedIkeysDB: \"\(LocationAssociatedIkeysDB)\" \"\(SALocationIkeyDB)\"")
        
        //////////////////////////////////////////////
        
        var CharacterAssociatedIkeysArray = CharacterCategoryIkeyDB.components(separatedBy: ",")
        
        print("CharacterAssociatedIkeysArray: \"\(CharacterAssociatedIkeysArray)\" ")
        
        //CharacterAssociatedIkeysArray.append(StoryIkeyDB)
        
        if let indexOfItem = CharacterAssociatedIkeysArray.index(of: StoryIkeyDB) {
            print("Found at index \(indexOfItem)")
            CharacterAssociatedIkeysArray.remove(at: indexOfItem)
        }
        
        
        print("CharacterAssociatedIkeysArray Added Ikey: \"\(CharacterAssociatedIkeysArray)\" ")
        
        //////////////////////////////////////////////
        
        let NewStringToDB = CharacterAssociatedIkeysArray.joined(separator: ",")
        
        print("NewStringToDB Final: \"\(NewStringToDB)\" ")
        
        //////////////////////////////////////////////
        
        var UpdateCharacterAssociatedStoriesSqlquery = String()
        
        do {
            UpdateCharacterAssociatedStoriesSqlquery = String(format: "UPDATE charactertable set storyassociatedikey = \"\(NewStringToDB)\" where ikey = \"\(Ikey)\" and userikeyreference = \"\(CurrentUserIkey!)\" and firstname = \"\(CharacterNameDB)\"")
            
            print("UpdateCharacterAssociatedStoriesSqlquery: \(UpdateCharacterAssociatedStoriesSqlquery)")
            
            try db.executeUpdate(UpdateCharacterAssociatedStoriesSqlquery, values: nil)
            
        } catch let error as NSError {
            
            print("ERROR: \(error)\n\n\(db.lastErrorMessage())")
            loggly(LogType.Error, text: "SharingViewController-\(#function) Query Failed:\n \"\(UpdateCharacterAssociatedStoriesSqlquery)\"")
            DoAlert.DisplayAlert(Class: "SharingViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed:\n \"\(UpdateCharacterAssociatedStoriesSqlquery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
            return
        }
        
        //////////////////////////////////////////////
        
        CharactersArray.removeAll()
        CharactersAddedArray.removeAll()
        
        StoryAssociationsCharacterListTableView.reloadData()
        StoryAssociationsCharactersAddedListTableView.reloadData()
        
        //////////////////////////////////////////////
        
        LoadCharacterTable()
        LoadCharacterAddedTable()
        
        
    }
    
    @IBAction func SACharacterOpenButton(_ sender: Any) {
        
        
        SharedItemLock = false
        
        CharacterInfoWindow.showWindow(self)
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    func defined(str : String?) -> Bool {
        //return str != nil
        print("str: \"\(str!)\"")
        if (str == nil || str == "") {
            return true
        } else {
            return false
        }
    }
    
    
    
    
    
} /// end of StoryAssociationsViewController


//  https://stackoverflow.com/questions/34454532/how-add-separator-to-string-at-every-n-characters-in-swift
extension String {
    var pairs: [String] {
        var result: [String] = []
        let characters = Array(self)
        stride(from: 0, to: count, by: 2).forEach {
            result.append(String(characters[$0..<min($0+2, count)]))
        }
        return result
    }
    mutating func insert(separator: String, every n: Int) {
        self = inserting(separator: separator, every: n)
    }
    func inserting(separator: String, every n: Int) -> String {
        var result: String = ""
        let characters = Array(self)
        stride(from: 0, to: count, by: n).forEach {
            result += String(characters[$0..<min($0+n, count)])
            if $0+n < count {
                result += separator
            }
        }
        return result
    }
}








