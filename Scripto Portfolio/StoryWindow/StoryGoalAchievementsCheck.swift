//
//  StoryGoalAchievementsCheck.swift
//  Scripto Portfolio
//
//  Created by Dorian Glanville on 3/22/19.
//  Copyright © 2019 DTGM. All rights reserved.
//

import Foundation
import SwiftLoggly
import FMDB


class StoryGoalAchievementsCheck: NSObject {
    
    //Variable that I need
    
    
    // StoryVCWordCount - current word count
    // StoryIkeyOpen - Ikey of the story needed.
    // -1 is the storygoaltable storyreferenceikey for no associated story

    // Example of the query
    
    // select * from storygoaltable where userikeyreference = 2 and storyikeyreference = 1 or storyikeyreference = 0 and status = "t"
    // "select * from storygoaltable where userikeyreference = \"\(userikeyreference)\" and storyikeyreference = \"\(storyikeyreference)\" or storyikeyreference = 0 and status = \"t\"
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    
    func CheckStoryAchievements(userikeyreference: String, storyikeyreference: String) {
        
        db.open()
        
        var count: Int = 0
        
        let StoryAchievementsQuery = "select count(ikey) as cnt from storygoaltable where userikeyreference = \"\(userikeyreference)\" and storyikeyreference = \"\(storyikeyreference)\" or storyikeyreference = 0 and status = \"t\" "
        
        do {
            let StoryAchievementsQueryRun = try db.executeQuery(StoryAchievementsQuery, values:nil)
            
            while StoryAchievementsQueryRun.next() {
                
                count = Int(StoryAchievementsQueryRun.int(forColumn: "cnt"))
                
            }
            
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "StoryGoalAchievementsCheck-\(#function) Query Failed: \"\(StoryAchievementsQuery)\" ")
            DoAlert.DisplayAlert(Class: "StoryGoalAchievementsCheck", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"\(StoryAchievementsQuery)\" ", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
            
            return
            
        }
        
        
        
        if (count < 1) {
            
            return
            
        } else {
            

            // Total word count
            
            var totalwordcountCount: Int = 0
            
            // select * from storygoaltable where userikeyreference = 2 and status = "t" and dailywordcount = "0" and totalwordcount != 0 and storyikeyreference = 1 or storyikeyreference = 0
            
            let StoryAchievementsTotalWordCountQuery = "select * from storygoaltable where userikeyreference = \"\(userikeyreference)\"  and status = \"t\" and dailywordcount = 0 and totalwordcount != 0 and storyikeyreference = \"\(storyikeyreference)\" or storyikeyreference = 0 "
            
            do {
                let StoryAchievementsTotalWordCountQueryRun = try db.executeQuery(StoryAchievementsTotalWordCountQuery, values:nil)
                
                while StoryAchievementsTotalWordCountQueryRun.next() {
                    
                    totalwordcountCount = Int(StoryAchievementsTotalWordCountQueryRun.int(forColumn: "totalwordcount"))
                    
                    if (StoryVCWordCount > totalwordcountCount) {
                        
                        print("MORE - Story word count \"\(StoryVCWordCount)\" has MORE words than \"\(totalwordcountCount)\" ")
                        
                        if #available(OSX 10.15, *) {
                            
                            //I am here
                            
                        } else {
                            // Fallback on earlier versions
                            
                            
                            
                            
                            
                            
                            
                            
                            let InsertDEMOwUserSqlquery = String(format: "INSERT INTO storygoalcompletiontable (ikey,userikeyreference,storyikeyreference,storygoalreference,goalcompletiondate) VALUES ((select MAX(ikey)+ 1 from storygoalcompletiontable),\"\(CurrentUserIkey!)\",\"\(storyikeyreference)\",\"\(CurrentUserIkey!)\",\"\(CurrentUserIkey!)\") ")
                            
                            print("InsertDEMOwUserSqlquery: \(InsertDEMOwUserSqlquery)")
                            
                            dbQueue.inTransaction { _, rollback in
                                do {
                                    
                                    try db.executeUpdate(InsertDEMOwUserSqlquery, values: nil)
                                    
                                } catch {
                                    
                                    rollback.pointee = true
                                    let Message = "SQL Create Demo User Query Failed: \(InsertDEMOwUserSqlquery)\n\n\(db.lastExtendedErrorCode())"
                                    loggly(LogType.Error, text: Message)
                                    DoAlert.DisplayAlert(Class: "StoryGoalAchievemtsCheck", Level: 1, Function: "\(#function)", MessageText: "An Error has occurred", InformativeText: "Database changes were rolled back.\nPlease Quit and try again. If this continues please restart your computer.", DatabaseError: "SQL Create Demo User Query Failed: \(InsertDEMOwUserSqlquery)\n\n\(db.lastExtendedErrorCode())")
                                    
                                }
                                loggly(LogType.Info, text: "Demo User Added Successfully")
                            }
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            //////////////////////////////////////////////////////////////////////////////////////////
                            
                            
//                            let notification = NSUserNotification()
//                            NSUserNotificationCenter.default.delegate = self as? NSUserNotificationCenterDelegate
//
//                            notification.title = "Test."
//                            notification.subtitle = "Sub Test."
//                            notification.soundName = NSUserNotificationDefaultSoundName
//                            NSUserNotificationCenter.default.deliver(notification)
                            

                            
                        }
                        
                        
                    } else {
                        
                        print("LESS - Story word count \"\(StoryVCWordCount)\" has LESS words than \"\(totalwordcountCount)\" ")
                        
                        
                    }
                    
                }
                
            } catch {
                print("ERROR: \(db.lastErrorMessage())")
                loggly(LogType.Error, text: "StoryGoalAchievementsCheck-\(#function) Query Failed: \"\(StoryAchievementsTotalWordCountQuery)\" ")
                DoAlert.DisplayAlert(Class: "StoryGoalAchievementsCheck", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"\(StoryAchievementsTotalWordCountQuery)\" ", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                
                return
                
            }
            
            
            
            
            
            
        }
        
        
        
    }//end of func CheckStoryAchievements()
    
    
    // for 10.14 and below
    func userNotificationCenter(_ center: NSUserNotificationCenter,
                                shouldPresent notification: NSUserNotification) -> Bool {
        return true
    }
    
    
    
    
    
    
    
    
    


}// end of class StoryGoalAchievementsCheck:
