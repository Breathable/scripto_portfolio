//
//  StoryViewController.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 2/14/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Cocoa
import SwiftLoggly



class StoryViewController: NSViewController,NSTextViewDelegate,NSUserNotificationCenterDelegate {

    
    @IBOutlet var MainView: NSView!
    
    @IBOutlet var StoryTextView: NSTextView!
    
    @IBOutlet weak var SVOpenCharactersButton: NSButton!
    @IBOutlet weak var SVOpenStoryIdeasButton: NSButton!
    @IBOutlet weak var SVOpenLocationsButton: NSButton!
    @IBOutlet weak var SVOpenStoryAssociationsButton: NSButton!
    @IBOutlet weak var SVLogOutButton: NSButton!
    @IBOutlet weak var SVCloseStoryButton: NSButton!
    @IBOutlet weak var SVSaveStoryButton: NSButton!
    @IBOutlet weak var SVOpenItemsButton: NSButton!
    
    var NewWordListCount = 0
    var newWordString = String()
    
    var WordCountAchCheck = Int()

    var FontColor = NSColor()
    
    var CurrentAppearanceStoryViewController = String()
    
    var StoryStartDate = ""
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        
        //print("StoryViewController OPENED!!")
        
        
        

        StoryTextView.lnv_setUpLineNumberView()
        
        NotificationCenter.default.addObserver(self, selector: #selector(showHideLineNumberView), name: NSNotification.Name(rawValue: "showHideLineNumberView"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(fullscreenWindow), name: NSNotification.Name(rawValue: "fullscreen"), object: nil)
        
        
    }
    
    override func viewWillAppear() {
        
    StoryTextView.delegate = self
        
    NSUserNotificationCenter.default.delegate = self
        
        
        
        
        LoadStory()
        
        
        
        
        
        
        if (SharedItemOwner != "") {
            
            // get current user font color
            
            var ColorDB: String!
            
            do {
                let UserQuery = try db.executeQuery("SELECT * FROM users WHERE user = \"\(CurrentUserName!)\" AND ikey = \(CurrentUserIkey!)", values:nil)
                
                while UserQuery.next() {
                    
                    ColorDB = UserQuery.string(forColumn: "usercolor")
                    
                }
                
            } catch {
                print("ERROR: \(db.lastErrorMessage())")
                loggly(LogType.Error, text: "UserPreferencesViewController-\(#function) Query Failed: \"SELECT * FROM users WHERE user = \"\(CurrentUserName!)\" AND ikey = \(CurrentUserIkey!)\"")
                DoAlert.DisplayAlert(Class: "UserPreferencesViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"SELECT * FROM users WHERE user = \"\(CurrentUserName!)\" AND ikey = \(CurrentUserIkey!)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
            }
            
            //////////////////////////////////
            
            //let color = NSColor().HexToColor(hexString: ColorDB, alpha: 1.0)
            
            FontColor = NSColor().HexToColor(hexString: ColorDB, alpha: 1.0)
            
            
          
            let attributes = [NSAttributedStringKey.foregroundColor: FontColor]
            StoryTextView.typingAttributes = attributes
    
    
            
            StoryTextView.usesInspectorBar = false
            
            
            
            //////////////////

            if (SharedItemLock == true) {
                LockDownWindow()
                
                // Change Font color to BLACK
                
            } else {
                UnLockWindow()
                
                // Change Font color to the user font color

            }
            
        } else {
            
            //StoryTextView.usesInspectorBar = false
            
            let attributes = [NSAttributedStringKey.foregroundColor: NSColor.init(calibratedWhite: 0, alpha: 1)]
            StoryTextView.typingAttributes = attributes
    
            FontColor = NSColor.black
    
        } // end of (SharedItemOwner != "")
        
        
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(SaveStory), name: NSNotification.Name(rawValue: "SaveStory"), object: nil)
        
        StorySaved = false
        
    }
    
    override func viewDidAppear() {
        
        //delayWithSeconds(1.0) {
            //StoryWC.ToolbarWordCountToolbaritemCount()
            self.WordCount()
        //}

        
        
        // This is for an issue where the line numbers are off and to force it to reload its view
        StoryTextView.hideLnView()
        delayWithSeconds(0.10) {
            self.StoryTextView.hideLnView()
        }
        

        
        
        
    }
    
    override func viewDidLayout() {
        
        //        enum InterfaceStyle : String {
        //            case Dark, Light
        //
        //            init() {
        //                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
        //                self = InterfaceStyle(rawValue: type)!
        //            }
        //        }
        //
        //        let currentStyle = InterfaceStyle()
        //
        //        //print("currentStyle: \"\(currentStyle)\"  -  CurrentAppearance: \"\(CurrentAppearance)\" ")
        //
        //        if ("\(currentStyle)" != CurrentAppearanceNewUserViewController) {
        //            appearanceCheck()
        //        }
        
        appearanceCheck()
        
        super.viewDidLayout()
        
        
        
        
    }// end of viewDidLayout()
    
    func appearanceCheck() {
        
        enum InterfaceStyle : String {
            case Dark, Light
            
            init() {
                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
                self = InterfaceStyle(rawValue: type)!
            }
        }
        
        let currentStyle = InterfaceStyle()
        
        if ("\(currentStyle)" != CurrentAppearanceStoryViewController) {
            
            
            
            if (currentStyle.rawValue == "Dark") {
                
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColorDarkMode

                //self.view.window?.backgroundColor = WindowBackgroundColor
                
                StoryTextView.wantsLayer = true
                StoryTextView.backgroundColor = NSColor.lightGray
                
                
                
                SVOpenCharactersButton.styleButtonText(button: SVOpenCharactersButton, buttonName: "Characters", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                SVOpenCharactersButton.wantsLayer = true
                SVOpenCharactersButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                SVOpenCharactersButton.layer?.cornerRadius = 7
                
                SVOpenStoryIdeasButton.styleButtonText(button: SVOpenStoryIdeasButton, buttonName: "Story Ideas", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                SVOpenStoryIdeasButton.wantsLayer = true
                SVOpenStoryIdeasButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                SVOpenStoryIdeasButton.layer?.cornerRadius = 7
                
                SVOpenLocationsButton.styleButtonText(button: SVOpenLocationsButton, buttonName: "Locations", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                SVOpenLocationsButton.wantsLayer = true
                SVOpenLocationsButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                SVOpenLocationsButton.layer?.cornerRadius = 7
                
                SVOpenStoryAssociationsButton.styleButtonText(button: SVOpenStoryAssociationsButton, buttonName: "Associations", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                SVOpenStoryAssociationsButton.wantsLayer = true
                SVOpenStoryAssociationsButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                SVOpenStoryAssociationsButton.layer?.cornerRadius = 7
                
                SVOpenItemsButton.styleButtonText(button: SVOpenItemsButton, buttonName: "Items", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                SVOpenItemsButton.wantsLayer = true
                SVOpenItemsButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                SVOpenItemsButton.layer?.cornerRadius = 7
                
            
                SVLogOutButton.styleButtonText(button: SVLogOutButton, buttonName: "Logout", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                SVLogOutButton.wantsLayer = true
                SVLogOutButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                SVLogOutButton.layer?.cornerRadius = 7
                
                SVCloseStoryButton.styleButtonText(button: SVCloseStoryButton, buttonName: "Close", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                SVCloseStoryButton.wantsLayer = true
                SVCloseStoryButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                SVCloseStoryButton.layer?.cornerRadius = 7
                
                SVSaveStoryButton.styleButtonText(button: SVSaveStoryButton, buttonName: "Save Story", fontColor: .darkGray , alignment: .center, font: AppFontBold, size: 22.0)
                
                SVSaveStoryButton.wantsLayer = true
                SVSaveStoryButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                SVSaveStoryButton.layer?.cornerRadius = 7
                
                
                view.window?.backgroundColor = NSColor.darkGray
                
                
            } else if (currentStyle.rawValue == "Light") {

                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColor.cgColor
            
                //self.view.window?.backgroundColor = WindowBackgroundColor
                
                StoryTextView.wantsLayer = true
                StoryTextView.backgroundColor = NSColor.white
                
                SVOpenCharactersButton.styleButtonText(button: SVOpenCharactersButton, buttonName: "Characters", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                SVOpenCharactersButton.wantsLayer = true
                SVOpenCharactersButton.layer?.backgroundColor = NSColor.gray.cgColor
                SVOpenCharactersButton.layer?.cornerRadius = 7
                
                SVOpenStoryIdeasButton.styleButtonText(button: SVOpenStoryIdeasButton, buttonName: "Story Ideas", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                SVOpenStoryIdeasButton.wantsLayer = true
                SVOpenStoryIdeasButton.layer?.backgroundColor = NSColor.gray.cgColor
                SVOpenStoryIdeasButton.layer?.cornerRadius = 7
                
                SVOpenLocationsButton.styleButtonText(button: SVOpenLocationsButton, buttonName: "Locations", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                SVOpenLocationsButton.wantsLayer = true
                SVOpenLocationsButton.layer?.backgroundColor = NSColor.gray.cgColor
                SVOpenLocationsButton.layer?.cornerRadius = 7
                
                SVOpenStoryAssociationsButton.styleButtonText(button: SVOpenStoryAssociationsButton, buttonName: "Associations", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                SVOpenStoryAssociationsButton.wantsLayer = true
                SVOpenStoryAssociationsButton.layer?.backgroundColor = NSColor.gray.cgColor
                SVOpenStoryAssociationsButton.layer?.cornerRadius = 7
                
                SVOpenItemsButton.styleButtonText(button: SVOpenItemsButton, buttonName: "Items", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                SVOpenItemsButton.wantsLayer = true
                SVOpenItemsButton.layer?.backgroundColor = NSColor.gray.cgColor
                SVOpenItemsButton.layer?.cornerRadius = 7
                
                SVLogOutButton.styleButtonText(button: SVLogOutButton, buttonName: "Logout", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                SVLogOutButton.wantsLayer = true
                SVLogOutButton.layer?.backgroundColor = NSColor.gray.cgColor
                SVLogOutButton.layer?.cornerRadius = 7
                
                SVCloseStoryButton.styleButtonText(button: SVCloseStoryButton, buttonName: "Close", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                SVCloseStoryButton.wantsLayer = true
                SVCloseStoryButton.layer?.backgroundColor = NSColor.gray.cgColor
                SVCloseStoryButton.layer?.cornerRadius = 7
                
                SVSaveStoryButton.styleButtonText(button: SVSaveStoryButton, buttonName: "Save Story", fontColor: .white , alignment: .center, font: AppFontBold, size: 22.0)
                
                SVSaveStoryButton.wantsLayer = true
                SVSaveStoryButton.layer?.backgroundColor = NSColor.gray.cgColor
                SVSaveStoryButton.layer?.cornerRadius = 7
                
                
                view.window?.backgroundColor = WindowBackgroundColor
                

                
            }
        }
        
        CurrentAppearanceStoryViewController = "\(currentStyle)"
        
        
    }//end of appearanceCheck()
    
    override func viewDidDisappear() {
        
        
        
        
    }
    
    @objc func fullscreenWindow() {

        
        
        
        self.StoryTextView.window?.toggleFullScreen(self)
        
        
    }
    
    func LockDownWindow() {
        
        StoryTextView.isEditable = false
        StoryTextView.isSelectable = false
        
        SVOpenCharactersButton.isEnabled = false
        SVOpenStoryIdeasButton.isEnabled = false
        SVOpenLocationsButton.isEnabled = false
//        SVOpenStoryAssociationsButton
//        SVLogOutButton
//        SVCloseStoryButton
        SVSaveStoryButton.isEnabled = false
        
        
        
    }
    
    func UnLockWindow() {
        
        StoryTextView.isEditable = true
        StoryTextView.isSelectable = true
        
        SVOpenCharactersButton.isEnabled = true
        SVOpenStoryIdeasButton.isEnabled = true
        SVOpenLocationsButton.isEnabled = true
        //        SVOpenStoryAssociationsButton
        //        SVLogOutButton
        //        SVCloseStoryButton
        SVSaveStoryButton.isEnabled = true
        
        
        
    }
    

    ////////////////////////////////////////////////////////
    
    func LoadStory() {
        
//        need to create lockdown functions
//        need to change fontcolor to the user font color
        
        
        
        
        MainView.window?.title = StoryNameOpen
        
        let storyPath = "\(StoriesLocation)/\(StoryNameOpen).rtf"
        print("storyPath: \(storyPath)")

        loggly(LogType.Info, text: "Story: \"\(StoryNameOpen)\" Opened")
        
            do {
                
                let StoryPathURL = NSURL(fileURLWithPath: storyPath)
  
                let attributedStringWithRtf = try NSAttributedString(url: StoryPathURL as URL, options: [.documentType:NSAttributedString.DocumentType.rtf], documentAttributes: nil)
           
                StoryTextView.textStorage?.append(attributedStringWithRtf)
                StoryAttributedText = attributedStringWithRtf
                
            }
            catch {
                
                /* error handling here */
                print(error.localizedDescription)
                
                loggly(LogType.Error, text: "StoryViewConroller-\(#function): Unable to load Story \"\(StoryNameOpen)\"n\(error.localizedDescription)")
                
                DoAlert.DisplayAlert(Class: "ViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Unable to load Story \"\(StoryNameOpen)\"\nPlease quit and try agian.", DatabaseError: "\(error.localizedDescription)")
                DoAlert.window.orderFront(self)
            }
        
//        let Fsize = StoryTextView?.font?.pointSize
//        print("Fsize: \(Fsize)")

        let attributes = [NSAttributedStringKey.foregroundColor: FontColor,  NSAttributedString.Key.font: NSFont.systemFont(ofSize: (StoryTextView?.font?.pointSize)!)]
        StoryTextView.typingAttributes = attributes
        
        
        delayWithSeconds(1.0) {
            self.WordCount()
            //toryGoalAchievementsCheckClass.CheckStoryAchievements(userikeyreference: "\(CurrentUserIkey!)", storyikeyreference: "\(StoryIkeyOpen)")
            self.CheckStoryAchievements(userikeyreference: "\(CurrentUserIkey!)", storyikeyreference: "\(StoryIkeyOpen)")
            
        }
        
        
        //////////////////////////////////////
        
        
        let dateFormatter = DateFormatter()
        //dateFormatter.dateFormat = "MMMM d, yyyy"
        //dateFormatter.dateFormat = "MMMM d, yyyy H:mm a"
        dateFormatter.dateFormat = "MMMM d, yyyy hh:mm a"
        //dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        
        StoryStartDate = dateFormatter.string(from:Date())
        
        
        
        
        
    } // end of LoadStory()
    
    
    @objc func SaveStory() {
        
        let storyPath = "\(StoriesLocation)/\(StoryNameOpen).rtf"
        print("storyPath: \(storyPath)")
        let StoryPathURL = NSURL(fileURLWithPath: storyPath)
        
        do {
            
            //let textDNA = NSAttributedString(string: StoryTextView)
            
            let textDNA = NSAttributedString(attributedString: StoryTextView.attributedString())
            
            
            //print("textDNA: \(textDNA)")
            
            
            let range = NSRange(0..<StoryTextView.string.count)
            
            let textTSave = try textDNA.fileWrapper(from: range, documentAttributes: [NSAttributedString.DocumentAttributeKey.documentType:NSAttributedString.DocumentType.rtf])
            try textTSave.write(to: StoryPathURL as URL, options: FileWrapper.WritingOptions.atomic, originalContentsURL: nil)
            
            
        } catch {
      
            print(error.localizedDescription)
            
            loggly(LogType.Error, text: "StoryViewConroller-\(#function): Unable to load Story \"\(StoryNameOpen)\"n\(error.localizedDescription)")
            
            DoAlert.DisplayAlert(Class: "ViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Unable to load Story \"\(StoryNameOpen)\"\nPlease quit and try agian.", DatabaseError: "\(error.localizedDescription)")
            DoAlert.window.orderFront(self)
        }
        
        
        // Udpate last edit field in DB for current Story
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "MM/dd/yyyy hh:mm a"
        
        let Today = Date()
        let TodayDateTime = dateFormatterGet.string(from: Today)
        
        let CurrentStory = self.view.window?.title
        
        var UpdateStoryEditDateReference = String()
        
        db.open()
        
        if (SharedItemOwner != "") {
//            UpdateStoryEditDateReference = "UPDATE storytable SET dateoflastedit = \"\(TodayDateTime)\" WHERE userikeyreference = (select ikey from users where user = \"\(SharedItemOwner)\") AND storyname = \"\(CurrentStory!)\""
            UpdateStoryEditDateReference = "UPDATE storytable SET dateoflastedit = \"\(TodayDateTime)\" WHERE userikeyreference = \"\(SharedItemOwner)\" AND storyname = \"\(CurrentStory!)\""
            print("UpdateLocationImageReference 1\n\"\(UpdateStoryEditDateReference)\"")
        } else {
//            UpdateStoryEditDateReference = "UPDATE storytable SET dateoflastedit = \"\(TodayDateTime)\" WHERE userikeyreference = (select ikey from users where user = \"\(CurrentUserName!)\") AND storyname = \"\(CurrentStory!)\""
            UpdateStoryEditDateReference = "UPDATE storytable SET dateoflastedit = \"\(TodayDateTime)\" WHERE userikeyreference = \"\(CurrentUserIkey!)\" AND storyname = \"\(CurrentStory!)\""
            print("UpdateLocationImageReference 2\n\"\(UpdateStoryEditDateReference)\"")
        }
        
        print("UpdateStoryEditDateReference: \"\(UpdateStoryEditDateReference)\"")
        
        do {
            
            try db.executeUpdate(UpdateStoryEditDateReference, values: nil)
            
        } catch {
            
            let ErrorMessage = "StoryViewController: \(#function) - Failed to INSERT Updated Time for Story: \"\(CurrentStory!)\" \n\n\(UpdateStoryEditDateReference)\n\n\(db.lastErrorMessage())"
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "StoryViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to INSERT Updated Time for Story: \"\(CurrentStory!)\"\n\n\(UpdateStoryEditDateReference)", DatabaseError: "\(db.lastErrorMessage())")
            
        }
        
        if (RefreshStoryTable == false) {
            StorySaved = true
            
        } else {
            //StoryListVC.StoryTableData()
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "StoryTableDataReload"), object: nil)
        }
        
        
        
        
    } // end of savestory()


    @IBAction func StorySaveButton(_ sender: Any) {
        
        
        SaveStory()
        
        StorySaved = true
        
        
    }
    
    
    func WordCount() {
    
        let wordList = StoryTextView.string.components(separatedBy: NSCharacterSet.punctuationCharacters).joined(separator: "").components(separatedBy: " ").filter {
            $0 != ""
            
        }
        
        //print("wordList count: \(wordList.count)")
        
        StoryVCWordCount = wordList.count
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateWordCount"), object: nil)

        print("\(StoryVCWordCount)/\(WordCountAchCheck)")
        
        if (WordCountAchCheck == 0) {
            
            WordCountAchCheck = StoryVCWordCount + 10
            
        } else if (WordCountAchCheck == StoryVCWordCount) {
            
            StoryGoalAchievementsCheckClass.CheckStoryAchievements(userikeyreference: "\(CurrentUserIkey!)", storyikeyreference: "\(StoryIkeyOpen)")
            WordCountAchCheck = StoryVCWordCount + 10
        }
        
        
        
        
       
        
        
        
        
        
        
        
        
        
        
        
        
        
        //Variable that I need
        
        //StoryIkeyOpen - Ikey of the story needed.
        // -1 is the storygoaltable storyreferenceikey for no associated story
        
        
        
        
        
        // Example of the query
        
        ///  select * from storygoaltable where userikeyreference = 2 and storyikeyreference = 1 or storyikeyreference = 0 and status = "t"
        
        
        
        
    }
    

    
    
    
    
    
    
//    func textDidBeginEditing(_ notification: Notification) {
//
//        print("textDidBeginEditing")
//
//        let attributes = [NSAttributedStringKey.foregroundColor: FontColor]
//        StoryTextView.typingAttributes = attributes
//
//    }
    
//    func textViewDidChangeSelection(_ notification: Notification) {
//        
//        print("textViewDidChangeSelection")
//        
//        let attributes = [NSAttributedStringKey.foregroundColor: FontColor]
//        StoryTextView.typingAttributes = attributes
//        
//    }
    
    
    func textDidChange(_ notification: Notification) {
        
    
        
        
        //let myAttribute = [ NSAttributedString.Key.font: NSFont(name: "Chalkduster", size: 18.0)! ]
        
        
        
        
        let attributes = [NSAttributedStringKey.foregroundColor: FontColor,  NSAttributedString.Key.font: NSFont.systemFont(ofSize: (StoryTextView?.font?.pointSize)!)]
        StoryTextView.typingAttributes = attributes
        
        //print("textDidBeginEditing")
        
        WordCount()
        
        StoryAttributedText = StoryTextView.attributedString()
        
        StorySaved = false
    }
    

    
    
    func OpenWordCountPopOver() {
        
        StoryCharacterWordPopover.showWindow(self)
    }
    
    
    @IBAction func StoryViewCloseWIndow(_ sender: Any) {
        
        if (SharedItemLock == true) {
            
        } else {
            SaveStory()
        }
        
        //print("self.view.window!.styleMask.rawValue = \"\(self.view.window!.styleMask.rawValue)\" ")
        
        
        
        //  https://stackoverflow.com/questions/6815917/how-to-know-if-a-nswindow-is-fullscreen-in-mac-os-x-lion
        //  let isWindowFullscreen = window.styleMask.contains(.fullScreen)
        //  if (self.view.window!.styleMask.rawValue != 0) {
        
        
        
        

        
        
        
        WordCount()
        
        //print("StoryVCWordCount: \(StoryVCWordCount)")
        
        let dateFormatter = DateFormatter()
        //dateFormatter.dateFormat = "MMMM d, yyyy"
        dateFormatter.dateFormat = "MMMM d, yyyy H:mm a"
        //dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        
        let StoryEndDate = dateFormatter.string(from:Date())
        
        ///////////////////////////////////
        
        //  StoryIkeyOpen
        
        
        var OwnerIkeyDB: Int = 0
        
        dbQueue.inTransaction { _, rollback in
            do {
        
                
                
                let StoryOwnerQuery = "select userikeyreference from storytable where ikey =  \(StoryIkeyOpen) "
                
                do {
                    let StoryAchievementsQueryRun = try db.executeQuery(StoryOwnerQuery, values:nil)
                    
                    while StoryAchievementsQueryRun.next() {
                        
                        OwnerIkeyDB = Int(StoryAchievementsQueryRun.int(forColumn: "userikeyreference"))
                        
                    }
                    
                } catch {
                    print("ERROR: \(db.lastErrorMessage())")
                    loggly(LogType.Error, text: "StoryViewController-\(#function) Query Failed: \"\(StoryOwnerQuery)\" ")
                    DoAlert.DisplayAlert(Class: "StoryViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"\(StoryOwnerQuery)\" ", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                    
                    return
                    
                }
            }
            
        }
        
        //print("StoryVCWordCount: \(StoryVCWordCount)")
        
        ///////////////////////////////////
        
    
        
        dbQueue.inTransaction { _, rollback in
            do {
              
                
                let InsertStorygoalcompletiontableSqlquery = String(format: "INSERT INTO storyhistorytable (ikey,storyikeyreference,startwordcount,endwordcount,startdate,enddate,updatedbyuserikey,ownedbyuserikey) VALUES ((select MAX(ikey)+ 1 from storyhistorytable),\"\(StoryIkeyOpen)\",\"\(StartingWordCount)\",\"\(StoryVCWordCount)\",\"\(StoryStartDate)\",\"\(StoryEndDate)\",\"\(CurrentUserIkey!)\",\"\(OwnerIkeyDB)\")")
                
                print("InsertStorygoalcompletiontableSqlquery: \(InsertStorygoalcompletiontableSqlquery)")
                
                
                do {
                    
                    try db.executeUpdate(InsertStorygoalcompletiontableSqlquery, values: nil)
                    
                } catch {
                    
                    
                    let Message = "StoryViewController-\(#function):  SQL Query Failed: \(InsertStorygoalcompletiontableSqlquery)\n\n\(db.lastError())"
                    loggly(LogType.Error, text: Message)
                    DoAlert.DisplayAlert(Class: "StoryViewController", Level: 1, Function: "\(#function)", MessageText: "An Error has occurred", InformativeText: "Database changes were rolled back.\nPlease Quit and try again. If this continues please restart your computer.", DatabaseError: "SQL Query Failed: \(InsertStorygoalcompletiontableSqlquery)\n\n\(db.lastErrorMessage())\n\n\(db.lastError())")
                    
                }
                
            }
            
        }
        
        
        
        StartingWordCount = 0
        
        ////////////////////////////////////////////////////////////////////
        
        if let window = NSApp.mainWindow {
            
            let isWindowFullscreen = window.styleMask.contains(.fullScreen)
            
            if (isWindowFullscreen) {
                
                print("FULLSCREEN MODE CALLED TO CLOSE FULLSCREEN - StoryViewCloseWIndow()")
                
                fullscreenWindow()
                
                delayWithSeconds(1.0) {
                    
                    if (StoryAssociationsWindow.isWindowLoaded) {
                        StoryAssociationsWindow.close()
                    }
                    if (StoryCharacterWordPopover.isWindowLoaded) {
                        StoryCharacterWordPopover.close()
                    }
                    
                    SharedItemLock = false
                    SharedItemOwner = ""
                    
                    StorySaved = true
                    //OpenWindows.CheckForOpenWindows()
                    
                    self.StoryTextView.string = ""
                    
                    self.view.window?.close()
                    
                }
                
                
                
            } else {
                
                self.StoryTextView.string = ""
                
                
                if (StoryAssociationsWindow.isWindowLoaded) {
                    StoryAssociationsWindow.close()
                }
                if (StoryCharacterWordPopover.isWindowLoaded) {
                    StoryCharacterWordPopover.close()
                }
                
                SharedItemLock = false
                SharedItemOwner = ""
                
                StorySaved = true
                
                self.view.window?.close()
                
            }
            
        }
        
        
        
        
        if (StoryHistoryWC.window!.isVisible) {
            StoryHistoryWC.window?.close()
        }
        
        
        
        
        
        
        
        
    }// end of StoryViewCloseWIndow
    
    
    @IBAction func StoryViewStoryAssocationsButton(_ sender: Any) {
        print("AssocationsButton Pressed")
        
        StoryAssociationsWindow.showWindow(self)
    }
    
    @IBAction func StoryViewStoryLocationsButton(_ sender: Any) {
        print("LocationsButton Pressed")
        LocationsWindow.showWindow(self)
    }
    
    @IBAction func StoryViewStoryLogoutButton(_ sender: Any) {
        print("LogoutButton Pressed")
        
        SaveStory()
        StoryTextView.string = ""
        
        self.view.window!.close()
        
        //OpenWindows.CheckForOpenWindows()
        
        OpenWindows.CloseAllWindows()
        
        
//        if (self.view.window!.styleMask.rawValue != 0) {
//            print("FULLSCREEN MODE CALLED")
//            fullscreenWindow()
//        }
        
        if let window = NSApp.mainWindow {
            
            let isWindowFullscreen = window.styleMask.contains(.fullScreen)
            
            if (isWindowFullscreen) {
                
                print("FULLSCREEN MODE CALLED TO CLOSE FULLSCREEN - StoryViewStoryLogoutButton()")
                
                fullscreenWindow()
                
            }
            
        }
        
        
        
        delayWithSeconds(1.0) {
            
            
            CurrentUserIkey = 0
            CurrentUserName = ""
            
            SharedItemLock = false
            SharedItemOwner = ""
            
            ///////////////////
            
            NSApp.mainMenu?.item(at: 0)?.submenu?.removeItem(at: 3) // Wanted Features
            NSApp.mainMenu?.removeItem(at: 5) // UserMenu
            
            //OpenWindows.CheckForOpenWindows()
            
            LoginWindow.showWindow(self)
            
        }
        
    }
    
    @IBAction func StoryViewStoryCharactersButton(_ sender: Any) {
        print("CharactersButton Pressed")
        CharacterWindow.showWindow(self)
    }
    
    @IBAction func StoryViewStoryStoryIdeasButton(_ sender: Any) {
        print("StoryIdeasButton Pressed")
        StoryIdeasWindow.showWindow(self)
    }
    
    @IBAction func StoryViewItemsButton(_ sender: Any) {
        print("ItemsButton Pressed")
        ItemsWindow.showWindow(self)
    }
    

    
    @objc func showHideLineNumberView() {
        
        StoryTextView.hideLnView()
  
    }
    
    
    
    
    
 
    
    /////////////////////////////////////////////////////////////////
    
    
    
    
    
    
    
    
    func CheckStoryAchievements(userikeyreference: String, storyikeyreference: String) {
        
        
        
        db.open()
        
        var count: Int = 0
        
        let StoryAchievementsQuery = "select count(ikey) as cnt from storygoaltable where userikeyreference = \"\(userikeyreference)\" and storyikeyreference = \"\(storyikeyreference)\" or storyikeyreference = 0 and status = \"t\" "
        
        do {
            let StoryAchievementsQueryRun = try db.executeQuery(StoryAchievementsQuery, values:nil)
            
            while StoryAchievementsQueryRun.next() {
                
                count = Int(StoryAchievementsQueryRun.int(forColumn: "cnt"))
                
            }
            
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "StoryViewController-\(#function) Query Failed: \"\(StoryAchievementsQuery)\" ")
            DoAlert.DisplayAlert(Class: "StoryViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"\(StoryAchievementsQuery)\" ", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
            
            return
            
        }
        
        
        
        if (count < 1) {
            
            return
            
        } else {
            
            
            // Total word count
            
            var totalwordcountCount: Int = 0
            var ikeyDB: Int = 0
    
            
            let StoryAchievementsQuery = "select * from storygoaltable where userikeyreference = \"\(userikeyreference)\"  and status = \"t\" and dailywordcount = 0 and totalwordcount != 0 and storyikeyreference = \"\(storyikeyreference)\" or storyikeyreference = 0 "
            
            do {
                let StoryAchievementsQueryQueryRun = try db.executeQuery(StoryAchievementsQuery, values:nil)
                
                while StoryAchievementsQueryQueryRun.next() {
                    
                    totalwordcountCount = Int(StoryAchievementsQueryQueryRun.int(forColumn: "totalwordcount"))
                    ikeyDB = Int(StoryAchievementsQueryQueryRun.int(forColumn: "ikey"))
                    
                    if (StoryVCWordCount > totalwordcountCount) {
                        
                        print("MORE - Story word count \"\(StoryVCWordCount)\" has MORE words than \"\(totalwordcountCount)\" ")
                        
                        /////////////////////////////////
                        
                        // storygoalcompletiontable check
                        
                        
                        var countStorygoalreference: Int = 0
                        
                        let StoryAchievementsCheckQuery = "select count(ikey) as cnt from storygoalcompletiontable where userikeyreference = \"\(userikeyreference)\" and storygoalreference = \(ikeyDB) and storyikeyreference = \"\(storyikeyreference)\" or storyikeyreference = 0 "
                        
                        print("StoryAchievementsCheckQuery: \"\(StoryAchievementsCheckQuery)\" ")
                        
                        
                        
                        do {
                            let StoryAchievementsCheckQueryRun = try db.executeQuery(StoryAchievementsCheckQuery, values:nil)
                            
                            while StoryAchievementsCheckQueryRun.next() {
                                
                                countStorygoalreference = Int(StoryAchievementsCheckQueryRun.int(forColumn: "cnt"))
                                print("countStorygoalreference: \"\(countStorygoalreference)\" ")
                            }
                            
                        } catch {
                            print("ERROR: \(db.lastErrorMessage())")
                            loggly(LogType.Error, text: "StoryViewController-\(#function) Query Failed: \"\(StoryAchievementsCheckQuery)\" ")
                            DoAlert.DisplayAlert(Class: "StoryGoalAchievStoryViewControllerementsCheck", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"\(StoryAchievementsCheckQuery)\" ", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                            
                            return
                            
                        }
                        

                        /////////////////////////////////
                        
                        // Insert into storygoalcompletiontable
                        
                        if (countStorygoalreference == 0) {
                            
                            let dateFormatterGet = DateFormatter()
                            dateFormatterGet.dateFormat = "MM/dd/yyyy"
                            
                            let Today = Date()
                            let TodayDate = dateFormatterGet.string(from: Today)
                        
                            let InsertStorygoalcompletiontableSqlquery = String(format: "INSERT INTO storygoalcompletiontable (ikey,userikeyreference,storyikeyreference,storygoalreference,goalcompletiondate) VALUES ((select MAX(ikey)+ 1 from storygoalcompletiontable),\"\(CurrentUserIkey!)\",\"\(storyikeyreference)\",\"\(ikeyDB)\",\"\(TodayDate)\") ")
                            
                            print("InsertStorygoalcompletiontableSqlquery: \(InsertStorygoalcompletiontableSqlquery)")
                            
                            dbQueue.inTransaction { _, rollback in
                                do {
                                    
                                    try db.executeUpdate(InsertStorygoalcompletiontableSqlquery, values: nil)
                                    
                                    ///////////////////////////
                                    
                                    //  1 = true
                                    //  2 = false
                                    
                                    let UpdateStoryGoaltableSqlquery = String(format: "UPDATE storygoaltable set completed = 1 where ikey = \(ikeyDB)")
                                    
                                    print("UpdateStoryGoaltableSqlquery: \(UpdateStoryGoaltableSqlquery)")
                                    
                                    
                                        do {
                                            
                                            try db.executeUpdate(UpdateStoryGoaltableSqlquery, values: nil)
                                            
                                        } catch {
                                            
                                           
                                            let Message = "StoryViewController-\(#function):  SQL Query Failed: \(UpdateStoryGoaltableSqlquery)\n\n\(db.lastErrorMessage())"
                                            loggly(LogType.Error, text: Message)
                                            DoAlert.DisplayAlert(Class: "StoryViewController", Level: 1, Function: "\(#function)", MessageText: "An Error has occurred", InformativeText: "Database changes were rolled back.\nPlease Quit and try again. If this continues please restart your computer.", DatabaseError: "SQL Query Failed: \(UpdateStoryGoaltableSqlquery)\n\n\(db.lastErrorMessage())")
                                            
                                        }
                                        
                                    
                                    
                                    ///////////////////////////
                                    
                                } catch {
                                    
                                    rollback.pointee = true
                                    let Message = "StoryViewController-\(#function):  SQL Query Failed: \(InsertStorygoalcompletiontableSqlquery)\n\n\(db.lastErrorMessage())"
                                    loggly(LogType.Error, text: Message)
                                    DoAlert.DisplayAlert(Class: "StoryViewController", Level: 1, Function: "\(#function)", MessageText: "An Error has occurred", InformativeText: "Database changes were rolled back.\nPlease Quit and try again. If this continues please restart your computer.", DatabaseError: "SQL Query Failed: \(InsertStorygoalcompletiontableSqlquery)\n\n\(db.lastErrorMessage())")
                                    
                                }
                                loggly(LogType.Info, text: "Story Goal Achievement Added Successfully")
                            }
                            
                        }  else {
                            print("Story Acheivement Count was \"\(countStorygoalreference)\" so none were added")
                        }//end of if (countStorygoalreference == 0)
                        
                    } else {
                        
                        print("LESS - Story word count \"\(StoryVCWordCount)\" has LESS words than \"\(totalwordcountCount)\" ")
                        
                    }
                    
                }
                
            } catch {
                print("ERROR: \(db.lastErrorMessage())")
                loggly(LogType.Error, text: "StoryViewController-\(#function) Query Failed: \"\(StoryAchievementsQuery)\" ")
                DoAlert.DisplayAlert(Class: "StoryViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"\(StoryAchievementsQuery)\" ", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
                
                return
                
            }
            
            
            // This is where I need the popups for the Story Goal Achievements
            
            if #available(OSX 10.15, *) {
                
                // if 10.15 do something else due to teh
                
            } else {
                // Fallback on earlier versions
                
                
//                let notification = NSUserNotification()
//                notification.title = "Test."
//                notification.subtitle = "Sub Test."
//                notification.soundName = NSUserNotificationDefaultSoundName
//                NSUserNotificationCenter.default.delegate = self
//                NSUserNotificationCenter.default.deliver(notification)
                
                
                
                
                
                //print("NSUserNotificationCenter.default.deliver DELIVERED!!  1")
                
                
                //showNotification1014(title: "HI", subtitle: "By", informativeText: "\(Date())")
                
            }
            
        }
        
        
        
    }//end of func CheckStoryAchievements()
    
    
    // for 10.14 and below
//    func userNotificationCenter(_ center: NSUserNotificationCenter, shouldPresent notification: NSUserNotification) -> Bool {
//
//
//        return true
//    }
    

    func userNotificationCenter(_ center: NSUserNotificationCenter, shouldPresent notification: NSUserNotification) -> Bool {
        print("DTG shouldPresent");
        
        return true
    }
    
//    private func userNotificationCenter(_ center: NSUserNotificationCenter, shouldPresent notification: NSUserNotification) -> Bool {
//        print("DTG shouldPresent");
//        return true;
//    }
    
    
    private func userNotificationCenter(_ center: NSUserNotificationCenter, didActivate notification: NSUserNotification) -> Bool {
        print("DTG didActivate");
        
        return true;
    }
    
    func userNotificationCenter(_ center: NSUserNotificationCenter, didDeliver notification: NSUserNotification) {
        print("DTG didDeliver")
        
       
    }
    
    
    
    //func showNotification1014(title: String, subtitle: String, informativeText: String, contentImage: NSImage) -> Void {
    func showNotification1014(title: String, subtitle: String, informativeText: String) -> Void {
        let notification = NSUserNotification()
        
        notification.title = title
        notification.subtitle = subtitle
        notification.informativeText = informativeText
        //notification.contentImage = contentImage
        notification.soundName = NSUserNotificationDefaultSoundName
        
        let NotificationDelivery = NSUserNotificationCenter.default
        NotificationDelivery.delegate = self
        //NotificationDelivery.deliver(notification)
        
        notification.deliveryDate = Date(timeIntervalSinceNow: 2)
        NotificationDelivery.scheduleNotification(notification)
        
        print("NSUserNotificationCenter.default.deliver DELIVERED!!  2")
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
}// end of file





