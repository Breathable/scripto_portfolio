//
//  Alerts.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 1/29/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Cocoa

class Alerts: NSAlert {
    
    //Level
    
    // 1 Critical            - EX DB Error , couldnt write to DB    loggly(LogType.Error, text: "here!!")
    // 2 Program Error               - DB Connecgion Failed         loggly(LogType.Warnings, text: "Matter")
    // 3 Missing Information                                        loggly(LogType.Verbose, text: "Fun")
    
    func DisplayAlert(Class: String, Level: Int, Function: String, MessageText: String, InformativeText: String, DatabaseError: String) {
        
        // EXAMPLE
        //  DoAlert.DisplayAlert(Class: "ViewController", Level: 1, Function: "\(#function)", MessageText: "An Error has occurred", InformativeText: "My Informative Text which is my Error in detail", DatabaseError: "\(db.lastErrorMessage())")

            AlertLevel = "\(Level)"
            AlertMessage = MessageText
            AlertInformative = InformativeText
            AlertClassAndFunction = "\(Class) - \(Function)"
            AlertDatabaseError = DatabaseError
        
            AlertWindow.showWindow(self)
    }
    
    
    
    
    
    // create a custom alert window that has a ViewController so the ViewController when DarkMode is turned on will change the background color appropriately 
    
    
    
    
    
    
    
    
}// end of Alerts
