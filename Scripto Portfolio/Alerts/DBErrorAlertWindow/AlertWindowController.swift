//
//  AlertWindowController.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 1/31/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Cocoa

class AlertWindowController: NSWindowController {

    override func windowDidLoad() {
        super.windowDidLoad()
    
        // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
        
        //window?.backgroundColor = NSColor.red  //LoginBackgroundColorBlue
        window?.titlebarAppearsTransparent = true
        window?.titleVisibility = .hidden
        
        if (AlertLevel == "1") {
            window?.backgroundColor = NSColor.red
        } else if (AlertLevel == "2") {
            window?.backgroundColor = NSColor.yellow
        } else if (AlertLevel == "3") {
            window?.backgroundColor = NSColor.green
        }
    }
    
    
    
    
    
    
    
    

}
