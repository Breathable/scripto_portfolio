//
//  AlertViewController.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 1/31/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Cocoa
import ProgressKit


class AlertViewController: NSViewController {

    
    @IBOutlet weak var levelLabelLabel: NSTextField!
    @IBOutlet weak var levelLabel: NSTextField!
    
    @IBOutlet weak var classAndFunctionLabel: NSTextField!
    
    @IBOutlet weak var lastDBErrorLabelLabel: NSTextField!
    @IBOutlet var lastDBErrorTextView: NSTextView!
    
    @IBOutlet weak var informativeInfoLabelLabel: NSTextField!
    @IBOutlet var informativeInfoTextView: NSTextView!
    
    
    @IBOutlet weak var AlertEmailButton: NSButton!
    @IBOutlet weak var AlertCopyInformationButton: NSButton!
    @IBOutlet weak var AlertCloseButton: NSButton!
    
    
    @IBOutlet weak var prgoressSpinner: Spinner!

    var NOWDATE = String()
    
    var EmailBody = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        
        view.wantsLayer = true
        
        
        
        
    }
    
    override func viewWillAppear() {
        
        print("AlertLevel: \(AlertLevel)")
        
        
        
        
        
        
       
        
        //let NowDate = Date()
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "MMM d, h:mm:ss a"
        NOWDATE = dateFormatterGet.string(from: Date())
        //let Bdate: NSDate? = dateFormatterGet.date(from: BirthdateDB) as NSDate?
        
        levelLabel.stringValue = AlertLevel
        informativeInfoTextView.string = "\(NOWDATE)\n\n\(AlertMessage)\n\(AlertInformative)"
        classAndFunctionLabel.stringValue = AlertClassAndFunction
        lastDBErrorTextView.string = "\(AlertDatabaseError)"
        
        
        
        ////////////////////////////////////////////////
        
//        AlertEmailButton
//        AlertCopyInformationButton
//        AlertCloseButton
        
        AlertEmailButton.styleButtonText(button: AlertEmailButton, buttonName: "Email", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
        
        AlertEmailButton.wantsLayer = true
        AlertEmailButton.layer?.backgroundColor = NSColor.gray.cgColor
        AlertEmailButton.layer?.cornerRadius = 7
        
        AlertCopyInformationButton.styleButtonText(button: AlertCopyInformationButton, buttonName: "Copy Information", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
        
        AlertCopyInformationButton.wantsLayer = true
        AlertCopyInformationButton.layer?.backgroundColor = NSColor.gray.cgColor
        AlertCopyInformationButton.layer?.cornerRadius = 7
        
        AlertCloseButton.styleButtonText(button: AlertCloseButton, buttonName: "Close", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
        
        AlertCloseButton.wantsLayer = true
        AlertCloseButton.layer?.backgroundColor = NSColor.gray.cgColor
        AlertCloseButton.layer?.cornerRadius = 7
        
        
        
        EmailBody = "\(NOWDATE)\n\nError Level: \(levelLabel.stringValue) - \(classAndFunctionLabel.stringValue)\n\nDatabase Version: \"\(DBVersion)\"\nApp Version: \"\(AppVersion)\"\nmacOS Version: \"\(OSSYSTEMVERSION)\"\n\(informativeInfoTextView.string)\n\nError Reason: \"\(lastDBErrorTextView.string)\" "
        
    }
    
    override func viewDidAppear() {
        
        //self.view.window?.makeKeyAndOrderFront(self)
        //self.view.window?.makeMain()
        self.view.window?.orderFront(self)
        
        
    }
    
    
    override func viewDidLayout() {
        
        //        enum InterfaceStyle : String {
        //            case Dark, Light
        //
        //            init() {
        //                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
        //                self = InterfaceStyle(rawValue: type)!
        //            }
        //        }
        //
        //        let currentStyle = InterfaceStyle()
        //
        //        //print("currentStyle: \"\(currentStyle)\"  -  CurrentAppearance: \"\(CurrentAppearance)\" ")
        //
        //        if ("\(currentStyle)" != CurrentAppearanceNewUserViewController) {
        //            appearanceCheck()
        //        }
        
        appearanceCheck()
        
        super.viewDidLayout()
        
        
        
        
    }// end of viewDidLayout()
    
    
    func appearanceCheck() {
        
 
        enum InterfaceStyle : String {
            case Dark, Light

            init() {
                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
                self = InterfaceStyle(rawValue: type)!
            }
        }

        let currentStyle = InterfaceStyle()
        
        print("currentStyle: \"\(currentStyle)\" ")
        
        
        levelLabel.font = NSFont(name:AppFontBold, size: 13.0)
        levelLabelLabel.font = NSFont(name:AppFontBold, size: 13.0)
        lastDBErrorLabelLabel.font = NSFont(name:AppFontBold, size: 13.0)
        classAndFunctionLabel.font = NSFont(name:AppFontBold, size: 13.0)
        informativeInfoLabelLabel.font = NSFont(name:AppFontBold, size: 13.0)
        
        



            if (currentStyle.rawValue == "Dark") {

                //lastDBErrorTextView.backgroundColor = NSColor.gray
                //informativeInfoTextView.backgroundColor = NSColor.gray

                

                if (AlertLevel == "1") {
                    view.layer?.backgroundColor = NSColor.init(srgbRed: 153/256, green: 0, blue: 0, alpha: 1).cgColor
                    levelLabelLabel.textColor = NSColor.gray
                    levelLabel.textColor = NSColor.gray
                    classAndFunctionLabel.textColor = NSColor.gray
                    lastDBErrorLabelLabel.textColor = NSColor.gray
                    informativeInfoLabelLabel.textColor = NSColor.gray
                } else if (AlertLevel == "2") {
                    view.layer?.backgroundColor = NSColor.init(deviceRed: 153/256, green: 153/256, blue: 0, alpha: 1).cgColor
                    levelLabelLabel.textColor = NSColor.black
                    levelLabel.textColor = NSColor.black
                    classAndFunctionLabel.textColor = NSColor.black
                    lastDBErrorLabelLabel.textColor = NSColor.black
                    informativeInfoLabelLabel.textColor = NSColor.black
                } else if (AlertLevel == "3") {
                    view.layer?.backgroundColor = NSColor.init(deviceRed: 0, green: 153/256, blue: 51/256, alpha: 1).cgColor
                    levelLabelLabel.textColor = NSColor.black
                    levelLabel.textColor = NSColor.black
                    classAndFunctionLabel.textColor = NSColor.black
                    lastDBErrorLabelLabel.textColor = NSColor.black
                    informativeInfoLabelLabel.textColor = NSColor.black
                }
                

            } else if (currentStyle.rawValue == "Light") {

                //lastDBErrorTextView.backgroundColor = NSColor.white
                //informativeInfoTextView.backgroundColor = NSColor.white
                
//                levelLabelLabel.textColor = NSColor.white
//                levelLabel.textColor = NSColor.white
//                classAndFunctionLabel.textColor = NSColor.white
//                lastDBErrorLabelLabel.textColor = NSColor.white
//                informativeInfoLabelLabel.textColor = NSColor.white
                
                
                if (AlertLevel == "1") {
                    view.layer?.backgroundColor = NSColor.red.cgColor
                    levelLabelLabel.textColor = NSColor.black
                    levelLabel.textColor = NSColor.black
                    classAndFunctionLabel.textColor = NSColor.black
                    lastDBErrorLabelLabel.textColor = NSColor.black
                    informativeInfoLabelLabel.textColor = NSColor.black
                } else if (AlertLevel == "2") {
                    view.layer?.backgroundColor = NSColor.yellow.cgColor
                    levelLabelLabel.textColor = NSColor.black
                    levelLabel.textColor = NSColor.black
                    classAndFunctionLabel.textColor = NSColor.black
                    lastDBErrorLabelLabel.textColor = NSColor.black
                    informativeInfoLabelLabel.textColor = NSColor.black
                } else if (AlertLevel == "3") {
                    view.layer?.backgroundColor = NSColor.green.cgColor
                    levelLabelLabel.textColor = NSColor.black
                    levelLabel.textColor = NSColor.black
                    classAndFunctionLabel.textColor = NSColor.black
                    lastDBErrorLabelLabel.textColor = NSColor.black
                    informativeInfoLabelLabel.textColor = NSColor.black
                }
                
            }
//        }

        //CurrentAppearanceThemesViewController = "\(currentStyle)"

 
    }//end of appearanceCheck()
    
    
    
    
    
    
    
    @IBAction func closeAlertWindowButton(_ sender: Any) {
        AlertWindow.close()
        //OpenWindows.CheckForOpenWindows()
    }
    
    @IBAction func emailAlertInformationButton(_ sender: Any) {
        
        ProgressIndicator()
        
        AlertEmailButton.styleButtonText(button: AlertEmailButton, buttonName: "Email", fontColor: .green , alignment: .center, font: AppFont, size: 13.0)
        
        // https://stackoverflow.com/questions/39370881/how-to-open-mail-panel-in-swift-mac-os/50517403#50517403
        
        let service = NSSharingService(named: NSSharingService.Name.composeEmail)
        service?.recipients = ["Support@dangthatsgoodmedia.biz"]
        service?.subject = "Level: \(levelLabel.stringValue) - \(classAndFunctionLabel.stringValue) - \(AppVersion)"
        service?.perform(withItems: ["\(EmailBody)"])
        
        delayWithSeconds(2.0) {
            self.ProgressIndicator()
            self.AlertEmailButton.styleButtonText(button: self.AlertEmailButton, buttonName: "Email", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
        }
        
        
    } // end of emailAlertInformationButton
    
    
    @IBAction func copyInformationButton(_ sender: Any) {
        
       
        AlertCopyInformationButton.styleButtonText(button: AlertCopyInformationButton, buttonName: "Copy Information", fontColor: .green , alignment: .center, font: AppFont, size: 13.0)
        

        
        let pb = NSPasteboard.init(name: NSPasteboard.Name.generalPboard)
        pb.string(forType: NSPasteboard.PasteboardType.string)
        //pb.changeCount
        pb.declareTypes([NSPasteboard.PasteboardType.string], owner: nil)
        pb.setString(EmailBody, forType: NSPasteboard.PasteboardType.string)
        
        
        
        delayWithSeconds(1.0) {
            self.AlertCopyInformationButton.styleButtonText(button: self.AlertCopyInformationButton, buttonName: "Copy Information", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
        }
        
        
        
    }
    
    
    func ProgressIndicator() {
        if (prgoressSpinner.animate) {
            prgoressSpinner.animate = false
            prgoressSpinner.isHidden = true
        } else {
            prgoressSpinner.animate = true
            prgoressSpinner.isHidden = false
        }
        
    }
    
    
    
    
    
    
    
    
    
    
    
} // end of alert controller




//  http://swiftrien.blogspot.com/2015/03/swift-and-nspasteboard.html
//        let pasteboard = NSPasteboard.general
//
//        if let nofElements = pasteboard.pasteboardItems?.count {
//
//            if nofElements > 0 {
//
//                let EmailBody = "Error Level: \(levelLabel.stringValue) - \(classAndFunctionLabel.stringValue)\n\nDatabase Version: \"\(DBVersion)\"\nApp Version: \"\(AppVersion)\"\nmacOS Version: \"\(OSSYSTEMVERSION)\"\n\(informativeInfoTextView.string)\n\n\(lastDBErrorTextView.string)"
//
//
//                // Assume they are strings
//
//                var strArr: Array<String> = []
//                for element in pasteboard.pasteboardItems! {
//                    if let str = element.string(forType: NSPasteboard.PasteboardType(rawValue: "public.utf8-plain-text")) {
//                        strArr.append(str)
//                    }
//                }
//
//
//                // Exit if no string was read
//
////                if strArr.count == 0 {
////                    return
////                }
//
//
//
//                // Perform the paste operation
//
//                //dataSource.cmdPaste(strArr)
//            }
//        }




