//
//  Logging.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 2/2/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Foundation
import SwiftLoggly

class Log: NSObject {

    
    func CreateLoggingLogFile() {
        
//        loggly(LogType.Info, text: "Welcome to Swift Loggly")
//        loggly(LogType.Verbose, text: "Fun")
//        loggly(LogType.Debug, text: "is")
//        loggly(LogType.Warnings, text: "Matter")
//        loggly(LogType.Error, text: "here!!")
        
        
        
        // Enable Emojis
        Loggly.logger.enableEmojis = true
        
        //Set the log save format type
        //Loggly.logger.logFormateType = LogFormateType.JSON  //default is "Normal"  .JSON
        
        // Set the log encoding format
        Loggly.logger.logEncodingType = String.Encoding.utf8;  //default is "utf8"
        
        //Set the name of the log files
        Loggly.logger.name = "ScriptoPortfolio" //default is "logglyfile"
        
        //Set the max size of each log file. Value is in KB
        Loggly.logger.maxFileSize = 2048 //default is 1024
        
        //Set the max number of logs files that will be kept
        Loggly.logger.maxFileCount = 8 //default is 4
        
        Loggly.logger.directory = NSSearchPathForDirectoriesInDomains(.documentDirectory , .userDomainMask, true)[0].appending("/ScriptoPortfolioLogs")
        
        
        
    }
    
    
    
    
    
}

