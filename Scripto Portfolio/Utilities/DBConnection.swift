//
//  DBConnection.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 1/26/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Cocoa

class DBConnection: NSObject {

    
    func OpenDB() {
        
        if (!db.open()) {
        
            DoAlert.DisplayAlert(Class: "DBConnection", Level: 1, Function: "\(#function)", MessageText: "An Error has occurred", InformativeText: "Scripto Portfolio is not able to access/open the database.\nIf this continues please restart your computer.", DatabaseError: "\(db.lastExtendedErrorCode())")
            
        }
        
        return
        
    }
    

    
    func CheckConnection() {
        
        let connection = db.goodConnection
        
        if (!connection) {
            
            DoAlert.DisplayAlert(Class: "DBConnection", Level: 1, Function: "\(#function)", MessageText: "The connection to the database was interupted", InformativeText: "Scripto Portfolio is not able to access/open the database.\nIf this continues please restart your computer.", DatabaseError: "\(db.lastExtendedErrorCode())")
            
            
        } else {
            
            print("DBConnection : CheckConnection - Connection Good")
            
        }
        
    }
    
    
    
    
    
    
    
    
    
}
