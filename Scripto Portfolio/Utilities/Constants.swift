//
//  Constants.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 1/19/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Foundation
import Cocoa
import FMDB
import SemanticVersioning
import SwiftLoggly

//////////////////////////////////////////////////////////////////

//  Versioning
//  https://github.com/AlexanderNey/SemanticVersioning

//  FMDB
//  https://github.com/ccgus/fmdb

//  swift FMDB examples
//  https://github.com/ccgus/fmdb/issues/291

//  SwiftlyDefaults
//  https://github.com/radex/SwiftyUserDefaults

//  SwiftLoggly
//  https://github.com/vigneshuvi/SwiftLoggly

//  loggly(LogType.Info, text: "Welcome to Swift Loggly")           Information
//[💙 Info -  Jan 31, 2017, 1:52:38 PM]: Welcome to Swift Loggly

//  loggly(LogType.Verbose, text: "Fun")                            WARNING
//[💜 Warn -  Jan 31, 2017, 1:52:38 PM]: Fun

//  loggly(LogType.Debug, text: "is")                               Error Type
//[💚 Error -  Jan 31, 2017, 1:52:38 PM]: is

//  loggly(LogType.Warnings, text: "Matter")                        Error Type
//[💛 Error -  Jan 31, 2017, 1:52:38 PM]: Matter

//  loggly(LogType.Error, text: "here!!")                           Error Type
//[❤️ Error -  Jan 31, 2017, 1:52:38 PM]: here!!



// ProgressKit
//  https://github.com/kaunteya/ProgressKit




//////////////////////////////////////////////////////////////////

// Menu Control


    


// colors

//var WindowBackgroundColor = NSColor()
var WindowBackgroundColor = NSColor(calibratedRed: 0.00, green: 0.46, blue: 0.80, alpha: 1.00)

var AlertBackgroundColorDarkMode = NSColor.darkGray

//if (CurrentAppearanceNewUserViewController == "Dark") {
//    alert.window.backgroundColor = AlertBackgroundColorDarkMode
//} else {
//    alert.window.backgroundColor = WindowBackgroundColor
//}


var WindowBackgroundColorDarkMode = NSColor.darkGray.cgColor
var TableBackgroundColorDarkMode = NSColor.controlBackgroundColor


// DB , Imaging, Story Locations                          .documentDirectory
//let StoriesLocation = NSSearchPathForDirectoriesInDomains(.libraryDirectory , .userDomainMask, true)[0].appending("/ScriptoPortfolio/Stories")
//let ImagesLocation = NSSearchPathForDirectoriesInDomains(.libraryDirectory , .userDomainMask, true)[0].appending("/ScriptoPortfolio/Images")
//let DatabaseLocation = NSSearchPathForDirectoriesInDomains(.libraryDirectory , .userDomainMask, true)[0].appending("ScriptoPortfolio/ScriptoPortfolio.db")
//let BackupsLocation = NSSearchPathForDirectoriesInDomains(.libraryDirectory , .userDomainMask, true)[0].appending("/ScriptoPortfolio/Backups")
//let LogsLocation = NSSearchPathForDirectoriesInDomains(.libraryDirectory , .userDomainMask, true)[0].appending("/ScriptoPortfolio/ScriptoPortfolioLogs")

let StoriesLocation = NSSearchPathForDirectoriesInDomains(.documentDirectory , .userDomainMask, true)[0].appending("/Stories")
let ImagesLocation = NSSearchPathForDirectoriesInDomains(.documentDirectory , .userDomainMask, true)[0].appending("/Images")
let DatabaseLocation = NSSearchPathForDirectoriesInDomains(.documentDirectory , .userDomainMask, true)[0].appending("/ScriptoPortfolio.db")
let BackupsLocation = NSSearchPathForDirectoriesInDomains(.documentDirectory , .userDomainMask, true)[0].appending("/Backups")
let LogsLocation = NSSearchPathForDirectoriesInDomains(.documentDirectory , .userDomainMask, true)[0].appending("/ScriptoPortfolioLogs")
let UserPrefPlist = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0].appending("/UserPreferences.plist")




// Appcenter
var AlwaysSendLogsAppCenter = Bool()
var NeverSendLogsAppCenter = Bool()
var AppCenterSendLogsPrompt = Bool()








var OSSYSTEMVERSION = Double()

//var db = FMDatabase(path: DatabaseLocation)
var db = FMDatabase()
var dbQueue = FMDatabaseQueue()

let DatabaseFunction = Database()

let FM = FileManager.default

let UD = UserDefaults.standard

let ConnectionTest = DBConnection()

let UpdateDatabase = DatabaseChanges()
var DBChangesTimer = Timer()
var ISUPDATING = false
var DoneUpdating = false




let DoAlert = Alerts()

let userDefaults = UserDefaults.standard

// VersionCheck


var AppVersion = String()
var DBVersion = String()

// Dark Mode variable

//var isDarkMode = false



// Logging
let WriteToLog = Log()





// Fonts

var AppFont = String()
var AppFontBold = String()

// ALERT WINDOW VARIABLES

var AlertInformative = String()
var AlertMessage = String()
var AlertLevel = String()
var AlertClassAndFunction = String()
var AlertDatabaseError = String()


/// Current Logged in user info

let LoginVC = LoginViewController()

var CurrentUserIkey: Int32?
var CurrentUserName: String?

var NewMenu = NSMenu()

// Story List window reference

let StoryListVC = StoryListViewController()


/// Current open windows Array
// Array of all windows

var CurrentlyOpenWindows = [NSWindowController]()

//let AllWindows = [NSWindowController]()

let OpenWindows = OpenWindowsChecker()

//  Edit Story Name Bool

var EditStoryIkey = String()
var EditStoryName = String()

// LocationView and LocationInfoView controllers

let LocInfoVC = LocationInfoViewController()
let LocVC = LocationsViewController()
var SelectedIndexPath: Set<NSIndexPath> = []

// LocationImageiew

let LocImageView = LocationImagesViewController()
var NumberOfImages = Int()

//////////////

let CharInfoVC = CharacterInfoViewController()

var HairColorHVC = String()
var FacialColorHVC = String()
var SkinColorHVC = String()

let CharVC = CharactersViewController()

var CVCIkey = String()
var CVCName = String()
var CVCBirthdate = String()

/////


var LocationName = String()
var LocationIkey = String()

var LocationNewLocation = false


////  Open Story Info

var StoryNameOpen = String()
var StoryIkeyOpen = String()

// StoryView window


let StoryVC = StoryViewController()

let StoryWC = StoryWindowController()
var StoryVCWordCount = Int()
var StoryAttributedText = NSAttributedString()
var SAStoryIdeaBool = false
var StorySaved = true
var RefreshStoryTable = true

var StartingWordCount = 0

/// Shared Window variables

var SharedItemIkey = String()
var SharedItemName = String()
var SharedItemCategory = String()
var SharedItemOwner = String()
var SharedItemLock = Bool()

// Story Idea Window


let StoryIdeaVC = StoryIdeasViewController()

var StoryIdeaIkey = String()
var StoryIdeaName = String()
var StoryIdeaCategory = String()


// Mobile View Controller

let MVC = MobileViewController()

// Itemviewcontroller

let IVC = ItemViewController()

var itemIkeyToLoad = Int()
var itemBool = Bool()


// ItemListView Controller

let ItemListVC = ItemsViewController()

// User Preferences

let UP = UserPreferences()


// Story Achievements

let StoryGoalAchievementsCheckClass = StoryGoalAchievementsCheck()






// Notification Prompts

//let notificationB1014 = NSUserNotification()






///  Story Verbosity

var ResponseJSONArray = Array<Dictionary<String,Any>>()









// View Controlers

let LoginWindow = NSStoryboard(name: NSStoryboard.Name(rawValue: "Main"), bundle: nil).instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "LoginWC")) as! NSWindowController

let NewUserWindow = NSStoryboard(name: NSStoryboard.Name(rawValue: "NewUserWindow"), bundle: nil).instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "NewUserWC")) as! NSWindowController


// Popover
let NewUserWindowFourDigitPopover = NSStoryboard(name: NSStoryboard.Name(rawValue: "NewUserWindow"), bundle: nil).instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "FourDigitPinPopover")) as! NSViewController

// Popover
let NewUserWindowBirthdatePopover = NSStoryboard(name: NSStoryboard.Name(rawValue: "NewUserWindow"), bundle: nil).instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "BirthdatePopover")) as! NSViewController

// Popover
let MobileInstructionsPopover = NSStoryboard(name: NSStoryboard.Name(rawValue: "MobileWindow"), bundle: nil).instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "MobileInstructionsPopOver")) as! NSViewController



let ForgotUserWindow = NSStoryboard(name: NSStoryboard.Name(rawValue: "ForgotUser"), bundle: nil).instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "ForgotWC")) as! NSWindowController

let StoryListWindow = NSStoryboard(name: NSStoryboard.Name(rawValue: "StoryListWindow"), bundle: nil).instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "StoryListWC")) as! NSWindowController

let StoryCharacterWordPopover = NSStoryboard(name: NSStoryboard.Name(rawValue: "StoryWindow"), bundle: nil).instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "WordPopoverWC")) as! NSWindowController

let AlertWindow = NSStoryboard(name: NSStoryboard.Name(rawValue: "Alert"), bundle: nil).instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "AlertWC")) as! NSWindowController

let StoryWindow = NSStoryboard(name: NSStoryboard.Name(rawValue: "StoryWindow"), bundle: nil).instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "StoryWC")) as! NSWindowController

let AddStoryWindow = NSStoryboard(name: NSStoryboard.Name(rawValue: "AddStoryWindow"), bundle: nil).instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "AddStoryWC")) as! NSWindowController



let RenameStoryWindow = NSStoryboard(name: NSStoryboard.Name(rawValue: "RenameStoryWindow"), bundle: nil).instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "RenameStoryWC")) as! NSWindowController

let WantedFeatureWindow = NSStoryboard(name: NSStoryboard.Name(rawValue: "WantedFeaturesWindow"), bundle: nil).instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "WantedFeaturesWC")) as! NSWindowController

let LockWindow = NSStoryboard(name: NSStoryboard.Name(rawValue: "LockWindow"), bundle: nil).instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "LockWC")) as! NSWindowController

let AppPreferencesWindow = NSStoryboard(name: NSStoryboard.Name(rawValue: "PreferencesWindow"), bundle: nil).instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "PreferencesWC")) as! NSWindowController

let UserPreferencesWindow = NSStoryboard(name: NSStoryboard.Name(rawValue: "UserPreferencesWindow"), bundle: nil).instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "UserPreferencesWC")) as! NSWindowController



let SharedWindow = NSStoryboard(name: NSStoryboard.Name(rawValue: "SharedWindow"), bundle: nil).instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "SharedWC")) as! NSWindowController

let SharingWindow = NSStoryboard(name: NSStoryboard.Name(rawValue: "SharingWindow"), bundle: nil).instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "SharingWC")) as! NSWindowController

let AboutWindow = NSStoryboard(name: NSStoryboard.Name(rawValue: "AboutWindow"), bundle: nil).instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "AboutWC")) as! NSWindowController

let StoryIdeasWindow = NSStoryboard(name: NSStoryboard.Name(rawValue: "StoryIdeasWindow"), bundle: nil).instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "StoryIdeasWC")) as! NSWindowController

let LocationsWindow = NSStoryboard(name: NSStoryboard.Name(rawValue: "LocationsWindow"), bundle: nil).instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "LocationsWC")) as! NSWindowController



let LocationInfoWindow = NSStoryboard(name: NSStoryboard.Name(rawValue: "LocationInfoWindow"), bundle: nil).instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "LocationInfoWC")) as! NSWindowController

let CharacterWindow = NSStoryboard(name: NSStoryboard.Name(rawValue: "CharactersWindow"), bundle: nil).instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "CharactersWC")) as! NSWindowController

let CharacterInfoWindow = NSStoryboard(name: NSStoryboard.Name(rawValue: "CharacterInfoWindow"), bundle: nil).instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "CharacterInfoWC")) as! NSWindowController

let AppThemesWindow = NSStoryboard(name: NSStoryboard.Name(rawValue: "ThemesWindow"), bundle: nil).instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "AppThemeWC")) as! NSWindowController

let StoryAssociationsWindow = NSStoryboard(name: NSStoryboard.Name(rawValue: "StoryAssociations"), bundle: nil).instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "StoryAssociationsWC")) as! NSWindowController

let MobileWindow = NSStoryboard(name: NSStoryboard.Name(rawValue: "MobileWindow"), bundle: nil).instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "MobileWC")) as! NSWindowController

let LocationImagesWindow = NSStoryboard(name: NSStoryboard.Name(rawValue: "LocationImagesWindow"), bundle: nil).instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "LocationIWC")) as! NSWindowController

let ItemsWindow = NSStoryboard(name: NSStoryboard.Name(rawValue: "ItemsList"), bundle: nil).instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "ItemsWindowController")) as! NSWindowController

let ItemInfoWindow = NSStoryboard(name: NSStoryboard.Name(rawValue: "ItemInformationWindow"), bundle: nil).instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "ItemInfoWindowController")) as! NSWindowController

let ItemCSCWindow = NSStoryboard(name: NSStoryboard.Name(rawValue: "ItemCategoriesSubcategories"), bundle: nil).instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "ItemCSCWindowController")) as! NSWindowController

let StoryGoalsWindow = NSStoryboard(name: NSStoryboard.Name(rawValue: "StoryGoalsStoryBoard"), bundle: nil).instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "StoryGoalsWC")) as! NSWindowController

let StoryAchievementsWC = NSStoryboard(name: NSStoryboard.Name(rawValue: "StoryAchievementsWindow"), bundle: nil).instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "StoryAchievementsWC")) as! NSWindowController

let StoryHistoryWC = NSStoryboard(name: NSStoryboard.Name(rawValue: "StoryHistoryStoryboard"), bundle: nil).instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "storyHistoryWC")) as! NSWindowController

let StoryVerbosityWC = NSStoryboard(name: NSStoryboard.Name(rawValue: "StoryVerbosity"), bundle: nil).instantiateController(withIdentifier: NSStoryboard.SceneIdentifier(rawValue: "storyverbositywc")) as! NSWindowController



///////////////////////////////////////////////


// print function
//func print(items: Any..., separator: String = " ", terminator: String = "\n") {
//
//    #if DEBUG
//
//        var idx = items.startIndex
//        let endIdx = items.endIndex
//
//        repeat {
//            Swift.print(items[idx], separator: separator, terminator: idx == (endIdx - 1) ? terminator : separator)
//            idx += 1
//        }
//            while idx < endIdx
//
//    #endif
//}



// Bonjour

let Bonjour = BonjourServer()

// Mobile

var dicFromData = Dictionary<String,String>()

var HandleBodyInt = 0


///////////////////////




extension String {
    
    func base64Encoded() -> String? {
        return data(using: .utf8)?.base64EncodedString()
    }
    
    func base64Decoded() -> String? {
        guard let data = Data(base64Encoded: self) else { return nil }
        return String(data: data, encoding: .utf8)
    }
    
}















//func LoadCollectionViewArray() {
    
    
    //        let fileUrl1 = NSURL(fileURLWithPath: "/Users/dorian/Library/Containers/com.Scripto-Portfolio/Data/Documents/Images/Anime-Wallpaper-Desktop-Background-47.jpg")
    //        let fileUrl2 = NSURL(fileURLWithPath: "/Users/dorian/Library/Containers/com.Scripto-Portfolio/Data/Documents/Images/maxresdefault1.jpg")
    //
    //
    //        urls1.append(fileUrl1 as URL)
    //        urls1.append(fileUrl2 as URL)
    
    
    
    
    //        ConnectionTest.CheckConnection()
    //        db.open()
    //
    ////        var LocationImagesQuery = String()
    ////
    ////        if (SharedItemOwner != "") {
    ////            //LocationImagesQuery = "SELECT * FROM locationtable WHERE ikey = \(LocationIkey) AND name = '\(LocationName)' AND userikeyreference = \(SharedItemOwner)"
    ////            LocationImagesQuery = "SELECT * FROM locationtable WHERE ikey = \(LocationIkey)"
    ////            print("LocationImagesQuery: SharedItemOwner \"\(LocationImagesQuery)\"")
    ////        } else {
    ////            //LocationImagesQuery = "SELECT * FROM locationtable WHERE ikey = \(LocationIkey) AND name = '\(LocationName)' AND userikeyreference = \(CurrentUserIkey!)"
    ////            LocationImagesQuery = "SELECT * FROM locationtable WHERE locationtableikeyreference = \(LocationIkey)"
    ////            print("LocationImagesQuery: \"\(LocationImagesQuery)\"")
    ////        }
    //
    //        let LocationImagesQuery = "SELECT * FROM locationtable WHERE ikey = \(LocationIkey)"
    //
    //
    //        // GetImagePathDB: "/Users/dorian/Library/Containers/com.Scripto-Portfolio/Data/Documents/Images/Anime-Wallpaper-Desktop-Background-48.jpg"
    //       // LocationImagesQuery: "SELECT * FROM locationtable WHERE name = 'Dorian Test 2' AND userikeyreference = 2"
    //        // LocationImagesQuery: "SELECT * FROM locationtable WHERE name = 'Dorians Work' AND userikeyreference = 2"
    //
    //        var LocationImageIkeyReferenceDB = String()
    //
    //
    //
    //        do {
    //            let LocationImagesQueryRun = try db.executeQuery(LocationImagesQuery, values:nil)
    //
    //            while LocationImagesQueryRun.next() {
    //
    //                LocationImageIkeyReferenceDB = LocationImagesQueryRun.string(forColumn: "ikey")!
    //
    //                //print("LocationLocationDB: \"\(LocationImageIkeyReferenceDB)\" ")
    //
    //                //LocationImagesQueryRun.next()
    //
    //            }
    //
    //        } catch {
    //            print("ERROR: \(db.lastErrorMessage())")
    //            loggly(LogType.Error, text: "ImageDirectoryLoader-\(#function) Query Failed: \"\(LocationImagesQuery)\"")
    //
    //            DoAlert.DisplayAlert(Class: "ImageDirectoryLoader", Level: 1, Function: "\(#function)", MessageText: "\"\(LocationImagesQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
    //
    //            return
    //
    //        }
    //
    //        db.close()
    //        db.open()
    //
    //
    //
    //
    //
    //        ////////////////
    //
    //        var GetImageCountQuery = ""
    //        var ImageCount = Int32()
    //
    //        do {
    //
    //        GetImageCountQuery = "SELECT Count(PATHTOIMAGE) as cnt from imagetable where locationtableikeyreference = \(LocationImageIkeyReferenceDB)"
    //
    //        print("GetImageCountQuery: \"\(GetImageCountQuery)\"")
    //
    //        let GetImageCountQueryRun = try db.executeQuery(GetImageCountQuery, values:nil)
    //
    //        while GetImageCountQueryRun.next() {
    //
    //            ImageCount = GetImageCountQueryRun.int(forColumn: "cnt")
    //
    //        }
    //
    //        print("ImageCount: \"\(ImageCount)\"")
    //
    //        } catch {
    //            print("ERROR: \(db.lastErrorMessage())")
    //            loggly(LogType.Error, text: "ImageDirectoryLoader-\(#function) Query Failed: \"\(GetImageCountQuery)\"")
    //
    //            DoAlert.DisplayAlert(Class: "ImageDirectoryLoader", Level: 1, Function: "\(#function)", MessageText: "\"\(GetImageCountQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
    //
    //            return
    //
    //        }
    //
    //
    //        db.close()
    //        db.open()
    //
    //        ////////////////////////
    //
    //        var GetImageQuery = ""
    //
    //        do {
    //
    //        GetImageQuery = "SELECT * from imagetable where locationtableikeyreference = \(LocationImageIkeyReferenceDB)"
    //
    //        print("GetImageQuery: \"\(GetImageQuery)\"")
    //
    //        var GetImagePathDB = String()
    //        var Count = 1
    //
    //        let GetImageQueryRun = try db.executeQuery(GetImageQuery, values:nil)
    //
    //        while GetImageQueryRun.next() {
    //
    //
    //
    //
    //            GetImagePathDB = GetImageQueryRun.string(forColumn: "PATHTOIMAGE")!
    //
    //            print("GetImagePathDB: \"\(GetImagePathDB)\"")
    //
    //            let fileUrl1 = NSURL(fileURLWithPath: GetImagePathDB)
    //
    //            print("fileUrl1 ADD: \"\(fileUrl1)\" ")
    //
    //            urls1.append(fileUrl1 as URL)
    //
    //            Count = Count + 1
    //
    //            //GetImageQueryRun.next()
    //
    //        }
    //
    //            if (Count > ImageCount) {
    //                print("GetImageQueryRun.next BREAK")
    //            } else {
    //                GetImageQueryRun.next()
    //            }
    //
    //        } catch {
    //            print("ERROR: \(db.lastErrorMessage())")
    //            loggly(LogType.Error, text: "ImageDirectoryLoader-\(#function) Query Failed: \"\(LocationImagesQuery)\"")
    //
    //            DoAlert.DisplayAlert(Class: "ImageDirectoryLoader", Level: 1, Function: "\(#function)", MessageText: "\"\(LocationImagesQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
    //
    //            return
    //
    //        }
    //
    //
    //
    //        print("urls1.count \"\(urls1.count)\" ")
    
    
    
    
    
    
//}// end of function













// Dark Mode detection

//let currentStyle = InterfaceStyle()
//
//if (currentStyle.rawValue == "Dark") {
//    print("Dark Style Detected")
//
//    //let nsCOLOR = NSColor(cgColor: CGColor(red: 0.69, green: 0.85, blue: 0.99, alpha: 0.5))
//
//
//    self.highTemp.layer?.backgroundColor = CGColor(red: 0.69, green: 0.85, blue: 0.99, alpha: 0.5)
//    self.lowTemp.layer?.backgroundColor = CGColor(red: 0.69, green: 0.85, blue: 0.99, alpha: 0.5)
//
//
//    //lighTemp.backgroundColor = NSColor.black
//    // highTemp.backgroundColor = nsCOLOR
//    //lowTemp.backgroundColor = nsCOLOR
//    //cellDate.backgroundColor = nsCOLOR
//
//} else if (currentStyle.rawValue == "Light") {
//
//}






