//
//  InternetCheck.swift
//  Scripto Portfolio
//
//  Created by Dorian Glanville on 5/20/19.
//  Copyright © 2019 DTGM. All rights reserved.
//


import Foundation
import Alamofire

class Connectivity {
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }
}
