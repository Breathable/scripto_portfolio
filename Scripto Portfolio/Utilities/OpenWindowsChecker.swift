//
//  File.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 1/21/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Foundation
import Cocoa

class OpenWindowsChecker: NSObject {
    

    let AllWindows = [LockWindow,LoginWindow,NewUserWindow,ForgotUserWindow,StoryListWindow,StoryWindow,AddStoryWindow,RenameStoryWindow,AlertWindow,WantedFeatureWindow,SharedWindow,SharingWindow,AboutWindow, LocationsWindow,LocationInfoWindow,LocationImagesWindow, CharacterWindow,CharacterInfoWindow,StoryIdeasWindow,AppThemesWindow,StoryAssociationsWindow,StoryCharacterWordPopover,AppPreferencesWindow,MobileWindow,UserPreferencesWindow,ItemsWindow,ItemInfoWindow,ItemCSCWindow,StoryGoalsWindow]
    
    
    //NewUserWindowFourDigitPopover,NewUserWindowBirthdatePopover
    

    let AllWindowsWithNames = ["LockWindow","LoginWindow","NewUserWindow","ForgotUserWindow","StoryListWindow","StoryWindow","AddStoryWindow","RenameStoryWindow","AlertWindow","WantedFeatureWindow","SharedWindow","SharingWindow","AboutWindow","LocationsWindow","LocationInfoWindow","LocationImagesWindow","CharacterWindow","CharacterInfoWindow","StoryIdeasWindow","AppThemesWindow","StoryAssociationsWindow","NewUserWindowFourDigitPopover","NewUserWindowBirthdatePopover","StoryCharacterWordPopover","AppPreferencesWindow","MobileWindow","UserPreferencesWindow","ItemsWindow","ItemInfoWindow","ItemCSCWindow","StoryGoalsWindow"]
    
    //,"NewUserWindowFourDigitPopover","NewUserWindowBirthdatePopover"
    
    var CurrentlyOpenWindowNames = [String]()
    
    
    func CurrentOpenWindows() {
        
        CurrentlyOpenWindows.removeAll()
        
        var count = 0
        
        for window in AllWindows {
            
            if (window.window?.isVisible)! {
                
                print("window: \(AllWindowsWithNames[count])")
                
                CurrentlyOpenWindows.append(window)
                
                count = count + 1
            }
            
        }
        
    }
    

    func CheckForOpenWindows() {
        
        enum InterfaceStyle : String {
            case Dark, Light
            
            init() {
                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
                self = InterfaceStyle(rawValue: type)!
            }
        }
        
        let currentStyle = InterfaceStyle()
        
        CurrentlyOpenWindows.removeAll()
        
        var count = 0
        
        for window in AllWindows {
            
            if (window.window?.isVisible)! {
                
                print("window: \(AllWindowsWithNames[count])")
                
                CurrentlyOpenWindows.append(window)
                
                count = count + 1
            }
            
        }
        
        ////////////////
        
        let OpenWindowCount = CurrentlyOpenWindows.count
        
        // if no windows are open
        
        if (OpenWindowCount == 0) {
            
            NSApp.mainMenu?.item(at: 0)?.submenu?.insertItem(withTitle: "Login Window", action: #selector(OpenLoginWindow), keyEquivalent: "", at: 3)
            NSApp.mainMenu?.item(at: 0)?.submenu?.item(at: 3)?.target = self
            NSApp.mainMenu?.item(at: 0)?.submenu?.item(at: 3)?.isEnabled = true
            
        } else {
            let Exists = NSApp.mainMenu?.item(at: 0)?.submenu?.item(withTitle: "Login Window")
            if ((Exists) != nil) {
                NSApp.mainMenu?.item(at: 0)?.submenu?.removeItem(at: 3)
            }
        }
        
        
        // Lockwindow is open but so are other windows so show the LoginWindow
        
        if (CurrentlyOpenWindows.contains(LockWindow) && (OpenWindowCount < 1)) {
            
            for window in AllWindows {
                
                window.close()
                
            }
            
            LoginWindow.showWindow(self)
            
        }
        
        if (OpenWindowCount < 1) {
            LoginWindow.showWindow(self)
        }
        
        // if the StoryWindow is already open
        
        if (CurrentlyOpenWindows.contains(StoryWindow)) {
            //print("Story Window is already open")
            
            let Storyname = StoryWindow.window?.title
            
            let alert = NSAlert()
            alert.messageText = "The story \"\(Storyname!)\" is currently open"
            alert.informativeText = "Please close it before trying open another stroy."
            alert.alertStyle = .warning
            alert.window.titlebarAppearsTransparent = true
            
            if (currentStyle.rawValue == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            }
            
            if (currentStyle.rawValue == "Light") {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.addButton(withTitle: "OK")
            
            let returnCode = alert.runModal()
            print("returnCode: \(returnCode)")
            
            if (returnCode.rawValue == 1000) {
                
                print("OK Pressed")
                
            }
            
            return
        }
        
        if ((!(CurrentlyOpenWindows.contains(LocationsWindow))) && (CurrentlyOpenWindows.contains(LocationInfoWindow))) {
            LocationInfoWindow.close()
        }
        
        if (CurrentlyOpenWindows.contains(CharacterInfoWindow)) {
            
            let alert = NSAlert()
            alert.messageText = "The Character \"\(CharacterInfoWindow.window?.title ?? "UNKNOWN")\" is currently open"
            alert.informativeText = "Please close the Character before trying to open another."
            alert.alertStyle = .warning
            alert.window.titlebarAppearsTransparent = true
            if (currentStyle.rawValue == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            }
            
            if (currentStyle.rawValue == "Light") {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            alert.addButton(withTitle: "OK")
            
            let returnCode = alert.runModal()
            print("returnCode: \(returnCode)")
            
            if (returnCode.rawValue == 1000) {
                print("OK Pressed")
                return
            }
            
        }
        
        if (CurrentlyOpenWindows.contains(LoginWindow) && (OpenWindowCount > 1)) {
            print("LoginWindow is open when other windows are open")
            LoginWindow.close()
        }
        
    } // end of CheckF
    
    
    @objc func OpenLoginWindow() {
        LoginWindow.showWindow(self)
    }
    
    
    func CloseAllWindows() {
        
        CurrentlyOpenWindows.removeAll()
        
        var count = 0
        
        for window in AllWindows {
            
            if (window.window?.isVisible)! {
                
                print("window: \(AllWindowsWithNames[count])")
                
                CurrentlyOpenWindows.append(window)
                
                count = count + 1
            }
            
        }
        
        
        
        for window in AllWindows {
            
            if (window.window?.isVisible)! {
                
                window.close()
                
            }
            
        }
        
    } // end of Close all windows
    
    
    func LockOpenWindows() {
        
        for window in AllWindows {
            
            if (window.window?.isVisible == true) {
                
                print("window: \(window)")
                
                let Index = AllWindows.index(of: window)
                let IndexValue = AllWindowsWithNames[Index!]
                
                //print("Window Added: \"\(IndexValue)\" ")
                
                CurrentlyOpenWindowNames.append(IndexValue)
                CurrentlyOpenWindows.append(window)
                
            }
            
        }
        
        //print("CurrentlyOpenWindowNames: \"\(CurrentlyOpenWindowNames)\"")
        
        for window in CurrentlyOpenWindows {
            
            if (window.window?.isVisible == false) {
                
                window.showWindow(self)
                
            }
            
        }
        
        print("LockOpenWindows RAN")
        
        CurrentlyOpenWindows.removeAll()
        CurrentlyOpenWindowNames.removeAll()
        
    }
    
    
    
}

