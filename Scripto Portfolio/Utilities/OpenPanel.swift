//
//  OpenPanel.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 3/3/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Foundation
import AppKit

//  https://stackoverflow.com/questions/28008262/detailed-instruction-on-use-of-nsopenpanel


extension NSOpenPanel {
    var selectUrl: URL? {
        title = "Select Image"
        allowsMultipleSelection = false
        canChooseDirectories = false
        canChooseFiles = true
        canCreateDirectories = false
        allowedFileTypes = ["jpg","png","pdf","pct", "bmp", "tiff","jpeg"]  // to allow only images, just comment out this line to allow any file type to be selected
        return runModal() == .OK ? urls.first : nil
    }
    var selectUrls: [URL]? {
        title = "Select Images"
        allowsMultipleSelection = true
        canChooseDirectories = false
        canChooseFiles = true
        canCreateDirectories = false
        allowedFileTypes = ["jpg","png","pdf","pct", "bmp", "tiff","jpeg"]  // to allow only images, just comment out this line to allow any file type to be selected
        return runModal() == .OK ? urls : nil
    }
    var selectUrlSaveLocation: [URL]? {
        title = "Select Location to Save Export"
        allowsMultipleSelection = false
        canChooseDirectories = true
        canChooseFiles = true
        canCreateDirectories = true
        //allowedFileTypes = ["jpg","png","pdf","pct", "bmp", "tiff","jpeg"]  // to allow only images, just comment out this line to allow any file type to be selected
        return runModal() == .OK ? urls : nil
    }
}
