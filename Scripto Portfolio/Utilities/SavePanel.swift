//
//  SavePanel.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 4/12/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Foundation
import AppKit


extension NSSavePanel {

    var saveToLocation: URL? {
        title = "Select Location"
        prompt = "Save"
        message = "Please choose a location for your export."
        canCreateDirectories = true
        nameFieldStringValue = "\(StoryWindow.window?.title ?? "").rtf"
        
        
        return runModal() == .OK ? directoryURL?.absoluteURL : nil
    }

}
