//
//  DatabaseChanges.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 1/29/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Foundation
import FMDB
import SwiftLoggly

//  /Users/xcodeprojects/Library/Containers/DTGM.Scripto-Portfolio/Data/Documents/ScriptoPortfolio.db

//  /Users/xcodeprojects/Library/Containers/com.StoryWriting/Data/Documents/StoryWriting.log


class DatabaseChanges: NSObject {

    
    func StoryWritingDatabaseUpgrade() {
        
        //print("dbpath: \(db.databasePath!)")
        
        //var ERROR = 0
        
        if (DBVersion == "1.0.0.4") {
        
            //print("Updating to 1.0.1.5")
            
            ConnectionTest.OpenDB()
            
            dbQueue.inTransaction { _, rollback in
                
                
                ConnectionTest.OpenDB()
                
               
                    
                    let AllStatementsUpdate = "ALTER TABLE locationtable RENAME TO TMP;" +
                    "CREATE TABLE `locationtable` (`ikey` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,`name` TEXT NOT NULL,`location` TEXT,`notes` TEXT,`imagetableikeyreference` TEXT, `associatedstoryikey` TEXT, `userikeyreference`    INTEGER NOT NULL);" +
                    "INSERT INTO locationtable(ikey, name, location, notes, imagetableikeyreference, associatedstoryikey, userikeyreference) SELECT ikey, name, location, notes, imagetableikeyreference, associatedstoryikey, userikeyreference FROM tmp;" +
                    "DROP TABLE TMP;" +
                "UPDATE dbasetable SET dbversion = '1.0.1.5'"
                    
                    
                    let Success = db.executeStatements(AllStatementsUpdate)
                    
                    if (!Success) {
                        
                        rollback.pointee = true
                        DoAlert.DisplayAlert(Class: "DatabaseChanges", Level: 1, Function: "\(#function)", MessageText: "An Error has occurred", InformativeText: "Database changes were rolled back.\nPlease Quit and try again.\nIf this continues please restart your computer.", DatabaseError: "\nSQL Query Failed:\nlastErrorMessage: \"\(db.lastErrorMessage())\"")
                        
                    } else {
                        print("Sucessfully updated DB to 1.0.1.5")
                        loggly(LogType.Info, text: "Success in updating \"\(DBVersion)\" to \"1.0.1.5\"")
                    }
                
                
            
                }

            
            
            //DatabaseFunction.VersionChecks()
            
        }// end of  (DBVersion == "1.0.0.4")
        
        
        //print("--DBVersion: \(DBVersion)")
        
        
         if (DBVersion == "1.0.1.5") {
            
            //print("Updating from \"1.0.1.5\" to \"\(AppVersion)\"")
            
            ConnectionTest.OpenDB()
            
            dbQueue.inTransaction { _, rollback in
                
                let AllStatementsUpdate = "ALTER TABLE users ADD COLUMN usercolor TEXT DEFAULT '0000FF';" + "CREATE TABLE `storysharingtable` (`ikey` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, `userikeyreference` INTEGER, `sharewithuserikeyreference` INTEGER, `storyikey` INTEGER, `storyshare` TEXT DEFAULT No, `storyedit` TEXT DEFAULT No, `storyadd` TEXT DEFAULT No );" + "CREATE TABLE `storyideasharingtable` (`ikey` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,`userikeyreference` INTEGER NOT NULL,`sharewithuserikeyreference` INTEGER NOT NULL,`storyideaikey` INTEGER NOT NULL,`storyideashare` TEXT DEFAULT No,`storyideaedit` TEXT DEFAULT No);" + "CREATE TABLE `locationsharingtable` ( `ikey` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, `userikeyreference` INTEGER NOT NULL, `sharewithuserikeyreference` INTEGER NOT NULL, `locationikey` INTEGER NOT NULL, `locationshare` TEXT DEFAULT No, `locationedit` TEXT DEFAULT No);" + "UPDATE dbasetable SET dbversion = '1.0.2.6';"
                
               // print("AllStatementsUpdate: \(AllStatementsUpdate)")
                
                let Success = db.executeStatements(AllStatementsUpdate)
                
                if (!Success) {
                    
                    print("ERROR: \"\(db.lastErrorMessage())\"")
                    
                    rollback.pointee = true
                    loggly(LogType.Error, text: "DatabaseChanges: \(#function) An Error has occurred SQL Query Failed:\nlastErrorMessage: \"\(db.lastErrorMessage())\"")
                    DoAlert.DisplayAlert(Class: "DatabaseChanges", Level: 1, Function: "\(#function)", MessageText: "An Error has occurred", InformativeText: "Database changes were rolled back.\nPlease Quit and try again.\nIf this continues please restart your computer.", DatabaseError: "\nSQL Query Failed:\nlastErrorMessage: \"\(db.lastErrorMessage())\"")
                    
                } else {
                    print("Success in Updating 1.0.1.6 to \"\(AppVersion)\"")
                    loggly(LogType.Info, text: "Success in updating \"\(DBVersion)\" to \"1.0.1.6\"")
                }
                
                
                
                
                
                
                
            } // end of inTransaction
            
            

            
            //print("Done with updating DB from 1.0.1.5 to \(AppVersion)")
            
            //DatabaseFunction.VersionChecks()
            
            
            
        }// end of  (DBVersion == "1.0.1.5")
        
        
        
        
        
        DatabaseFunction.VersionChecks()
        
    }// end of DatabaseUpgrade
    

    func CreateDatabase() {
        
        ConnectionTest.OpenDB()
        
        DatabaseFunction.appVersionCheck()
       
        DBVersion = AppVersion
        
        let sqlCreateDatabaseStatements = String(format:"CREATE TABLE `dbasetable` (`ikey`    INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,`dbversion`    TEXT NOT NULL DEFAULT '',`db`    INTEGER DEFAULT 1,`alwayssendlogs`    NUMERIC DEFAULT 1,`appcentersendlogsprompt`    NUMERIC DEFAULT 0);" +
            
            "CREATE TABLE `storytable` (`ikey`    INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,`userikeyreference`INTEGER NOT NULL,`storyname`TEXT NOT NULL,`dateoflastedit`REAL NOT NULL,`createdate`    REAL NOT NULL,`pathtostory`    TEXT NOT NULL);" +
            
            "CREATE TABLE `users` (`ikey` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,`user` TEXT NOT NULL,`password` TEXT NOT NULL,`birthdate` TEXT NOT NULL, usercolor TEXT DEFAULT '0000FF',`pin` INTEGER,`isAdmin` INTEGER);" +
            "CREATE TABLE `backupTable` (`ikey`    INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,`path` TEXT NOT NULL,`createdate`    TEXT NOT NULL );" +
            
            "CREATE TABLE `storyideastable` (`ikey`    INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,`userikeyreference`    INTEGER NOT NULL,`categoryikey`    INTEGER NOT NULL,`name`    TEXT NOT NULL,`storyvalue` TEXT,`storyassociatedikey` TEXT DEFAULT '');" +
            
            "CREATE TABLE `charactertable` (`ikey`    INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,`userikeyreference`    INTEGER NOT NULL,`firstname`    TEXT,`middlename`    TEXT,`lastname`    TEXT,`birthdate`    TEXT,`heightfeet`    INTEGER,`heightinches`    INTEGER,`weight`    INTEGER,`gender`    INTEGER,`ethnicity`    TEXT,`birthplace`    TEXT,`orientation`    TEXT,`haircolor`    TEXT,`facialcolor`    TEXT,`skintone`    TEXT,`charactersummary`    TEXT,`haircut`    TEXT,`hairpart`    TEXT,`hairlength`    TEXT,`facialhair`    TEXT ,`facialhairother`    TEXT,`storyassociatedikey`    TEXT default '');" +
            
            "CREATE TABLE `imagetable` (`ikey` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,`userikeyreference` INTEGER NOT NULL,`locationtableikeyreference`    INTEGER,`characterreference` INTEGER,`itemreference` INTEGER,`pathtoimage` INTEGER NOT NULL);" +
            
            "CREATE TABLE `charactervariableinfotable` (`ikey`    INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,`userikeyreference` INTEGER NOT NULL,`characterreference`    INTEGER NOT NULL,`aliasesnicknames`    TEXT DEFAULT '',`hopesdreams` TEXT DEFAULT '',`fears`    TEXT DEFAULT '',`prejudices`    TEXT DEFAULT '',`religion`    TEXT DEFAULT '',`likesdislikes` TEXT DEFAULT '', `background`    TEXT DEFAULT '',`traits`    TEXT DEFAULT '',`skills`    TEXT DEFAULT '',`familyrelatives` TEXT DEFAULT '',`relationships`    TEXT DEFAULT '',`workhistory`    TEXT DEFAULT '',`favoritenotfoods`    TEXT DEFAULT '', `allergies`    TEXT DEFAULT '',`languages`    TEXT DEFAULT '',`fashion`    TEXT DEFAULT '',`surgeries`    TEXT DEFAULT '',`motivations`    TEXT DEFAULT '');" +
            
            "CREATE TABLE `locationtable` (`ikey` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,`name` TEXT NOT NULL,`location` TEXT,`notes` TEXT,`imagetableikeyreference` TEXT, `associatedstoryikey` TEXT, `userikeyreference`    INTEGER NOT NULL);" +
            
            "CREATE TABLE `storysharingtable` (`ikey` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, `userikeyreference` INTEGER, `sharewithuserikeyreference` INTEGER, `storyikey` INTEGER, `storyshare` TEXT DEFAULT No, `storyedit` TEXT DEFAULT No, `storyadd` TEXT DEFAULT No );" +
            
            "CREATE TABLE `charactersharingtable` (`ikey` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,`userikeyreference` INTEGER NOT NULL,`sharewithuserikeyreference` INTEGER NOT NULL,`characterikey` INTEGER NOT NULL,`charactershare` TEXT DEFAULT No,`characteredit` TEXT DEFAULT No);" +

            "CREATE TABLE `storyideasharingtable` (`ikey` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,`userikeyreference` INTEGER NOT NULL,`sharewithuserikeyreference` INTEGER NOT NULL,`storyideaikey` INTEGER NOT NULL,`storyideashare` TEXT DEFAULT No,`storyideaedit` TEXT DEFAULT No);" +
            
            "CREATE TABLE `locationsharingtable` ( `ikey` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, `userikeyreference` INTEGER NOT NULL, `sharewithuserikeyreference` INTEGER NOT NULL, `locationikey` INTEGER NOT NULL, `locationshare` TEXT DEFAULT No, `locationedit` TEXT DEFAULT No );" +
            
            "CREATE TABLE `mobiletable` (`ikey` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,`userikeyreference` TEXT NOT NULL,`type` TEXT, `characterfirstname` TEXT,`charactermiddlename` TEXT,`characterlastname` TEXT,`characterbirthdate` TEXT,`charactersummary` TEXT,`storyname` TEXT,`storytext` TEXT,`locationname` TEXT,`locationaddress` TEXT,`locationtext` TEXT,`storyideaname` TEXT,`storyideatype` TEXT,`storyideatext` TEXT,`itemname`    TEXT,`itemcomp`    TEXT,`itemhistory`    TEXT);" +
            
            "CREATE TABLE `itemtable` (`ikey` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,`name` TEXT NOT NULL DEFAULT 'Item Name',`createdate` TEXT,`composition` TEXT,`createdby` TEXT,`usedfor` TEXT,`history` TEXT,`itemcategory` INTEGER,`itemsubcategory` INTEGER,`userikeyreference` INTEGER);" +
            
            "CREATE TABLE `itemcategorytable` (`ikey` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,`categoryname` TEXT NOT NULL,`categorynote` TEXT);" +
            
            "CREATE TABLE `itemsubcategorytable` (`ikey` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,`itemcategoryreferenceikey` INTEGER NOT NULL,`subcategoryname` TEXT NOT NULL,`subcategorynote` TEXT);" +
            
             "CREATE TABLE `storygoaltable` (`ikey`    INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,`userikeyreference`    INTEGER NOT NULL,`storyikeyreference`    INTEGER,`dailywordcount`    INTEGER,`totalwordcount`    INTEGER,`startgoaldate`    TEXT DEFAULT '',`endgoaldate`    TEXT DEFAULT '',`createdate`    TEXT,`status`   TEXT DEFAULT '',`completed`    INTEGER DEFAULT 0);" +
            
            "CREATE TABLE `storygoalcompletiontable` (`ikey`    INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,`userikeyreference`    INTEGER NOT NULL,`storyikeyreference`    INTEGER,`goalcompletiondate`    TEXT NOT NULL,`storygoalreference`    INTEGER);" +
            
            "CREATE TABLE `storyhistorytable` (`ikey`    INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,`storyikeyreference`    INTEGER,`startwordcount`    INTEGER,`endwordcount`    INTEGER,`startdate`    TEXT,`enddate`    TEXT,`updatedbyuserikey`    INTEGER,`ownedbyuserikey`    INTEGER);"
            
            
        )
        
        //  `category` INTEGER, `subcategory` INTEGER,`categoryarray` TEXT,`subcategoryarray` TEXT);"
        
        //print("\nsqlCreateDatabaseStatements:\n\n\(sqlCreateDatabaseStatements)\n\n")
        
        
        dbQueue.inTransaction() {(_, rollback) in
            
            if !db.executeStatements(sqlCreateDatabaseStatements) {
                //print("Create Database Failure: \(db.lastErrorMessage())")
                
                loggly(LogType.Error, text: "DatabaseChanges: \"\(#function) SQL Create Database Query Failed: \(sqlCreateDatabaseStatements)\n\n\(db.lastErrorMessage())")
                
                DoAlert.DisplayAlert(Class: "DatabaseChanges", Level: 1, Function: "\(#function)", MessageText: "An Error has occurred", InformativeText: "Database changes were rolled back.\nPlease Quit and try again. If this continues please restart your computer.", DatabaseError: "SQL Create Database Query Failed: \(sqlCreateDatabaseStatements)\n\n\(db.lastErrorMessage())")
                
                rollback.pointee = true
                
            }
            
            if !db.executeStatements(sqlCreateDatabaseStatements) {
                return
            }
            
         loggly(LogType.Info, text: "Database Creation Successful")
            
        }
        
        
        /////////////////////
        
        
        
        let InsertDEMOwUserSqlquery = String(format: "INSERT INTO users (ikey,user,password,birthdate,usercolor,pin,isAdmin) VALUES ((select MAX(ikey)+ 1 from users),\"AppSDemo\",\"QXBwU0RlbW8=\",\"01/02/1918\",\"0E0EFF\",\"0000\",\"1\")")
        
        //print("UpdateDBVersionToCurrent: \(UpdateDBVersionToCurrent)")
        
        dbQueue.inTransaction { _, rollback in
            do {
                
                try db.executeUpdate(InsertDEMOwUserSqlquery, values: nil)
                
            } catch {
                
                rollback.pointee = true
                let Message = "SQL Create App S Demo Query Failed: \(InsertDEMOwUserSqlquery)\n\n\(db.lastExtendedErrorCode())"
                loggly(LogType.Error, text: Message)
                DoAlert.DisplayAlert(Class: "DatabaseChanges", Level: 1, Function: "\(#function)", MessageText: "An Error has occurred", InformativeText: "Database changes were rolled back.\nPlease Quit and try again. If this continues please restart your computer.", DatabaseError: "SQL Created App S Demo Query Failed: \(InsertDEMOwUserSqlquery)\n\n\(db.lastExtendedErrorCode())")
                
            }
        }
        
       
        
        //////////////////////
        
        ConnectionTest.OpenDB()
        
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "MM/dd/yyyy"
        
        
        let UpdateDBVersionToCurrent = "INSERT INTO dbasetable (ikey,dbversion,db) VALUES ((select MAX(ikey)+ 1 from dbasetable),\"\(AppVersion)\",\"1\")"
        
        //print("UpdateDBVersionToCurrent: \(UpdateDBVersionToCurrent)")
        
        dbQueue.inTransaction { _, rollback in
            do {

                try db.executeUpdate(UpdateDBVersionToCurrent, values: nil)
                
            } catch {
                
                rollback.pointee = true
                let Message2 = "\(UpdateDBVersionToCurrent)\n\n\(db.lastExtendedErrorCode())"
                loggly(LogType.Error, text: Message2)
                DoAlert.DisplayAlert(Class: "DatabaseChanges", Level: 1, Function: "\(#function)", MessageText: "An Error has occurred", InformativeText: "Database changes were rolled back.\nPlease Quit and try again. If this continues please restart your computer.", DatabaseError: "SQL Create Database Query Failed: \(UpdateDBVersionToCurrent)\n\n\(db.lastExtendedErrorCode())")

            }
            
            //print("Updated Database with AppVersion")
            loggly(LogType.Info, text: "Success Inserting Demo User")
            
        }
        
        dbQueue.inTransaction { _, rollback in
            
            let AllStatementsUpdate =
                String(format:
                        "INSERT INTO storygoaltable(ikey, userikeyreference, storyikeyreference, dailywordcount, totalwordcount, createdate, status) VALUES ((select MAX(ikey) + 1 from storygoaltable),0,0,0,500,'','t');" +
                        
                        "INSERT INTO storygoaltable(ikey, userikeyreference, storyikeyreference, dailywordcount, totalwordcount, createdate, status) VALUES ((select MAX(ikey) + 1 from storygoaltable),0,0,0,5000,'','t');" +
                        
                        "INSERT INTO storygoaltable(ikey, userikeyreference, storyikeyreference, dailywordcount, totalwordcount, createdate, status) VALUES ((select MAX(ikey) + 1 from storygoaltable),0,0,0,25000,'','t');" +
                        
                        "INSERT INTO storygoaltable(ikey, userikeyreference, storyikeyreference, dailywordcount, totalwordcount, createdate, status) VALUES ((select MAX(ikey) + 1 from storygoaltable),0,0,0,50000,'','t');" +
                        
                        "INSERT INTO storygoaltable(ikey, userikeyreference, storyikeyreference, dailywordcount, totalwordcount, createdate, status) VALUES ((select MAX(ikey) + 1 from storygoaltable),0,0,0,75000,'','t');" +
                        
                    "INSERT INTO storygoaltable(ikey, userikeyreference, storyikeyreference, dailywordcount, totalwordcount, createdate, status) VALUES ((select MAX(ikey) + 1 from storygoaltable),0,0,0,1000000,'','t',`completed`    INTEGER);" +
                            
                        
                    "UPDATE dbasetable SET dbversion = '4.0.0';")
            
            let Success = db.executeStatements(AllStatementsUpdate)
            
            if (!Success) {
                
                rollback.pointee = true
                loggly(LogType.Error, text: "DatabaseChanges-\(#function) Query Failed: \"\(AllStatementsUpdate)\"")
                DoAlert.DisplayAlert(Class: "DatabaseChanges", Level: 1, Function: "\(#function)", MessageText: "An Error has occurred", InformativeText: "Database changes were rolled back.\nPlease Quit and try again.\nIf this continues please restart your computer.", DatabaseError: "\nSQL Query Failed:\nlastErrorMessage: \"\(db.lastErrorMessage())\"")
                
                return
                
                
            }
            
        } // end of inTransaction

        
        DatabaseFunction.VersionChecks()
        
        
        
        
        loggly(LogType.Info, text: "Successfully Created Database")
        
        
        
    }// End of CreateDatabase()
    
    
    
    @objc func UpdateDatabase() {
        
        
        
        //DBChangesTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(UpdateDatabase),userInfo: nil, repeats: true)
        DatabaseFunction.databaseVersionCheck()
        
        
        
        print("DBVersion: \"\(DBVersion)\" ")
        
        /////////        2.1.0.1
        if (DBVersion > "2.1.0.1" && DBVersion < "2.1.0" && ISUPDATING == false) {
            ISUPDATING = true
            DB2101()
            DatabaseFunction.databaseVersionCheck()
        }

        if (DBVersion == "2.1.0" && DBVersion < "2.1.1" && ISUPDATING == false) {
            ISUPDATING = true
            DB211()
            DatabaseFunction.databaseVersionCheck()
        }
        
        if (DBVersion == "2.1.1" && DBVersion < "2.1.2" && ISUPDATING == false) {
            ISUPDATING = true
            DB212()
            DatabaseFunction.databaseVersionCheck()
        }
        
        if (DBVersion == "2.1.2" && DBVersion < "3.4.6" && ISUPDATING == false) {
            ISUPDATING = true
            DB322()
            DatabaseFunction.databaseVersionCheck()
        }
        
        if (DBVersion == "3.4.6" && DBVersion < "3.5.0" && ISUPDATING == false) {
            ISUPDATING = true
            DB350()
            DatabaseFunction.databaseVersionCheck()
        }
        
        if (DBVersion == "3.5.0" && DBVersion < "3.6.0" && ISUPDATING == false) {
            ISUPDATING = true
            DB360()
            DatabaseFunction.databaseVersionCheck()
        }
        
        if (DBVersion == "3.6.0" && DBVersion < "4.0.0" && ISUPDATING == false) {
            ISUPDATING = true
            DB40()
            DatabaseFunction.databaseVersionCheck()
        }
        
        if (DBVersion == "4.0.0" && DBVersion < "4.0.1" && ISUPDATING == false) {
            ISUPDATING = true
            DB401()
            DatabaseFunction.databaseVersionCheck()
        }
        
        
        
        
        
        
        
        
        
        
        
        AddAppStoreUser()
        
        
        
        
        
      
        DoneUpdating = true
        
        
        
    }// end of UpdateDatabase
    
    
    
    func AddAppStoreUser() {
        
        ConnectionTest.OpenDB()
        
        var DBUser: Int!
        
        let DBUserQuery = "SELECT count(user) as cnt FROM users where user = 'AppSDemo' "
        
        do {
            let UserQuery = try db.executeQuery(DBUserQuery, values:nil)
            
            while UserQuery.next() {
                DBUser = Int(UserQuery.int(forColumn: "cnt"))
            }
            
        } catch {
            //print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "DatabaseChanges-\(#function) Query Failed: \"\(DBUserQuery)\"")
            DoAlert.DisplayAlert(Class: "DatabaseChanges", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"\(DBUserQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
            
            return
        }
        
        
        if (DBUser == 0) {
            
            let InsertDEMOwUserSqlquery = String(format: "INSERT INTO users (ikey,user,password,birthdate,usercolor,pin,isAdmin) VALUES ((select MAX(ikey)+ 1 from users),\"AppSDemo\",\"QXBwU0RlbW8=\",\"01/02/1918\",\"0E0EFF\",\"0000\",\"1\")")
            
            //print("UpdateDBVersionToCurrent: \(UpdateDBVersionToCurrent)")
            
            dbQueue.inTransaction { _, rollback in
                do {
                    
                    try db.executeUpdate(InsertDEMOwUserSqlquery, values: nil)
                    
                } catch {
                    
                    rollback.pointee = true
                    let Message = "SQL Create Demo User Query Failed: \(InsertDEMOwUserSqlquery)\n\n\(db.lastErrorMessage())"
                    loggly(LogType.Error, text: Message)
                    DoAlert.DisplayAlert(Class: "DatabaseChanges", Level: 1, Function: "\(#function)", MessageText: "An Error has occurred", InformativeText: "Database changes were rolled back.\nPlease Quit and try again. If this continues please restart your computer.", DatabaseError: "SQL Create Demo User Query Failed: \(InsertDEMOwUserSqlquery)\n\n\(db.lastErrorMessage())")
                    
                }
                loggly(LogType.Info, text: "Demo User Added Successfully")
            }
            
        }
        
        
        
        
        
    }
    
    
    
    
    func DB2101() {
        //print("Updating from \"\(DBVersion)\" to \"\(AppVersion)\"")
        
        // update
        // dbasetable: add a updateddate column - text
        // users: add a pin column - text
        
        ConnectionTest.OpenDB()
        
        dbQueue.inTransaction { _, rollback in
            
            let AllStatementsUpdate = "ALTER TABLE users RENAME TO TMP;" +
                "CREATE TABLE `users` (`ikey` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,`user` TEXT NOT NULL,`password` TEXT NOT NULL,`birthdate` TEXT NOT NULL, usercolor TEXT DEFAULT '000FF',`pin` INTEGER,`isAdmin` INTEGER);" +
                "INSERT INTO users(ikey, user, password, birthdate, usercolor) SELECT ikey, user, password, birthdate, usercolor from TMP;" +
                "DROP TABLE TMP;" +
            "UPDATE dbasetable SET dbversion = '2.1.0.1';"
            
            let Success = db.executeStatements(AllStatementsUpdate)
            
            if (!Success) {
                
                rollback.pointee = true
                loggly(LogType.Error, text: "DatabaseChanges-\(#function) Query Failed: \"\(AllStatementsUpdate)\"")
                DoAlert.DisplayAlert(Class: "DatabaseChanges", Level: 1, Function: "\(#function)", MessageText: "An Error has occurred", InformativeText: "Database changes were rolled back.\nPlease Quit and try again.\nIf this continues please restart your computer.", DatabaseError: "\nSQL Query Failed:\nlastErrorMessage: \"\(db.lastErrorMessage())\"")
                
                return
                
                
            } else {
                //print("Success in updating \"\(DBVersion)\" to \"2.1.0.1\"")
                //DBVersion = "2.1.0.1"
                loggly(LogType.Info, text: "Success in updating \"\(DBVersion)\" to \"2.1.0.1\"")
            }
            
        } // end of inTransaction
        
        ISUPDATING = false
        DatabaseFunction.VersionChecks()
    }// end of DB2101
    
    func DB211() {
        
        //print("Updating from \"\(DBVersion)\" to \"\(AppVersion)\"")
        
        ////////////////////////
        
        ConnectionTest.OpenDB()
        
        dbQueue.inTransaction { _, rollback in
            
            let NullCheckQueries =
                "UPDATE charactervariableinfotable SET aliasesnicknames = '' WHERE aliasesnicknames IS NULL;" +
                "UPDATE charactervariableinfotable SET hopesdreams = '' WHERE hopesdreams IS NULL;" +
                "UPDATE charactervariableinfotable SET fears = '' WHERE fears IS NULL;" +
                "UPDATE charactervariableinfotable SET prejudices = '' WHERE prejudices IS NULL;" +
                "UPDATE charactervariableinfotable SET religion = '' WHERE religion IS NULL;" +
                "UPDATE charactervariableinfotable SET likesdislikes = '' WHERE likesdislikes IS NULL;" +
                "UPDATE charactervariableinfotable SET background = '' WHERE background IS NULL;" +
                "UPDATE charactervariableinfotable SET traits = '' WHERE traits IS NULL;" +
                "UPDATE charactervariableinfotable SET skills = '' WHERE skills IS NULL;" +
                "UPDATE charactervariableinfotable SET familyrelatives = '' WHERE familyrelatives IS NULL;" +
                "UPDATE charactervariableinfotable SET relationships = '' WHERE relationships IS NULL;" +
                "UPDATE charactervariableinfotable SET workhistory = '' WHERE workhistory IS NULL;" +
                "UPDATE charactervariableinfotable SET favoritenotfoods = '' WHERE favoritenotfoods IS NULL;" +
                "UPDATE charactervariableinfotable SET allergies = '' WHERE allergies IS NULL;" +
                "UPDATE charactervariableinfotable SET languages = '' WHERE languages IS NULL;" +
                "UPDATE charactervariableinfotable SET fashion = '' WHERE fashion IS NULL;" +
                "UPDATE charactervariableinfotable SET surgeries = '' WHERE surgeries IS NULL;" +
                "UPDATE charactervariableinfotable SET motivations = '' WHERE motivations IS NULL;" +
            "UPDATE dbasetable SET dbversion = '2.1.1';"
            
            
            let Success = db.executeStatements(NullCheckQueries)
            
            if (!Success) {
                
                rollback.pointee = true
                loggly(LogType.Error, text: "DatabaseChanges-\(#function) Query Failed: \"\(NullCheckQueries)\"\n\nLastErrorMessage: \"\(db.lastErrorMessage())\"")
                DoAlert.DisplayAlert(Class: "DatabaseChanges", Level: 1, Function: "\(#function)", MessageText: "An Error has occurred", InformativeText: "Database changes were rolled back from updating to \"2.1.1.1\".\nPlease Quit and try again.\nIf this continues please restart your computer.", DatabaseError: "\nSQL Query Failed:\n\n\(NullCheckQueries)\n\nLastErrorMessage: \"\(db.lastErrorMessage())\"")
                
                return
                
            } else {
                //print("Success in writing \"\(DBVersion)\" to \"2.1.1\" ")
                loggly(LogType.Info, text: "Success in updating \"\(DBVersion)\" to \"2.1.1\"")
                
            }
            
        } // end of transaction
        
        
        dbQueue.inTransaction { _, rollback in
            
            let AlterQueries = "ALTER table charactertable add column storyassociatedikey default ''"
            
            var Success = db.executeStatements(AlterQueries)
            
            if ((db.lastErrorMessage().range(of: "duplicate column name: storyassociatedikey")) != nil) {
                //print("ERROR: duplicate column name: storyassociatedikey NOT AN ERROR")
                Success = true
            }
            
            if (!Success) {
                
                rollback.pointee = true
                loggly(LogType.Error, text: "DatabaseChanges-\(#function) Query Failed: \"\(AlterQueries)\"\n\nLastErrorMessage: \"\(db.lastErrorMessage())\"")
                DoAlert.DisplayAlert(Class: "DatabaseChanges", Level: 1, Function: "\(#function)", MessageText: "An Error has occurred", InformativeText: "Database changes were rolled back from updating to \"2.1.1.1\".\nPlease Quit and try again.\nIf this continues please restart your computer.", DatabaseError: "\nSQL Query Failed:\n\n\(AlterQueries)\n\nLastErrorMessage: \"\(db.lastErrorMessage())\"")
                
                return
                
                
            } else {
                
                //print("Success in 2.1.1 update")
                
                //DBVersion = "2.1.1"
                ISUPDATING = false
            }
            
        } // end of transaction
        
        
        
        
        
        ////////////////////////
        
        DatabaseFunction.VersionChecks()
        
    }// end of DB211
    
    func DB212() {
        
        loggly(LogType.Info, text: "DatabaseChanges-\(#function): Database Update Started: Updating from \"\(DBVersion)\" to \"\(AppVersion)\"")
        
        ConnectionTest.OpenDB()
        
        dbQueue.inTransaction { _, rollback in
            
            let MobileTableCreateStatement = String(format:"CREATE TABLE `mobiletable` (`ikey` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,`userikeyreference` TEXT NOT NULL,`type` TEXT,`characterfirstname` TEXT,`charactermiddlename` TEXT,`characterlastname` TEXT,`characterbirthdate` TEXT,`charactersummary` TEXT,`storyname` TEXT,`storytext` TEXT,`locationname` TEXT,`locationaddress` TEXT,`locationtext` TEXT,`storyideaname` TEXT,`storyideatype` TEXT,`storyideatext` TEXT,`itemname`    TEXT,`itemcomp`    TEXT,`itemhistory`    TEXT);"  +
                "UPDATE dbasetable SET dbversion = '2.1.2'"
            )
            
            let Success = db.executeStatements(MobileTableCreateStatement)

            
            
            if (!Success) {
                
                rollback.pointee = true
                
                loggly(LogType.Warnings, text: "DatabaseChanges-\(#function): Unable to create  \"mobiletable\"\n\(db.lastErrorMessage())")
                
                
                DoAlert.DisplayAlert(Class: "DatabaseChanges", Level: 1, Function: "\(#function)", MessageText: "An Error has occurred", InformativeText: "Database changes were rolled back from updating to \"2.1.2\".\nPlease Quit and try again.\nIf this continues please restart your computer.", DatabaseError: "\nSQL Query Failed:\n\n\(MobileTableCreateStatement)\n\nLastErrorMessage: \"\(db.lastErrorMessage())\"")
                
                return

            } else {
                //print("Success in writing \"\(DBVersion)\" to \"2.1.2\"")
                //DBVersion = "2.1.2"
                ISUPDATING = false
                
                
            }
            
        } // end of transaction
        
        
        
    }// end of DB212
    
    func DB322() {
        
        ConnectionTest.OpenDB()
        
        db.open()
        
        
        
        //dbQueue.inDatabase { _, rollback in
        dbQueue.inTransaction { _, rollback in
            
            let ItemTableCreateStatement = String(format:"CREATE TABLE `itemtable` (`ikey` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,`name` TEXT NOT NULL DEFAULT 'Item Name',`createdate` TEXT,`composition` TEXT,`createdby` TEXT,`usedfor` TEXT,`history` TEXT,`userikeyreference` INTEGER);"  +
                "CREATE TABLE `itemcategorytable` (`ikey` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,`categoryname` TEXT NOT NULL,`categorynote` TEXT,`itemcategory` INTEGER,`itemsubcategory` INTEGER);" +
                "CREATE TABLE `itemsubcategorytable` (`ikey` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,`itemcategoryreferenceikey` INTEGER NOT NULL,`subcategoryname` TEXT NOT NULL,`subcategorynote` TEXT);" +
                "ALTER table imagetable add column `itemreference` INTEGER;"  +
                "UPDATE dbasetable SET dbversion = '3.4.6'"
            )
            
            let Success = db.executeStatements(ItemTableCreateStatement)
            
            
            
            
            
            
            
            if (!Success) {
                
                if ("\(db.lastErrorMessage())".range(of:"`itemtable` already exists") != nil) {
                    
                    var countItemTable = Int()
                    
                    dbQueue.inTransaction { _, rollback in
                        
                        let ItemTableCheckQuery = String(format:"Select count(*) from itemtable")
                        
                        do {
                            
                            let SuccessItemTableCheckQuery = try db.executeQuery(ItemTableCheckQuery, values:nil)
                            
                            while SuccessItemTableCheckQuery.next() {
                                countItemTable = Int(SuccessItemTableCheckQuery.longLongInt(forColumn: "count"))
                            }
                            
                        } catch {
                            
                            //print("ERROR: DatabaseChanges: Failed to get itemtable Count\n\(ItemTableCheckQuery)\n\(db.lastErrorMessage())")
                            
                            let ErrorMessage = "DatabaseChanges:\(#function) - Failed to get itemtable Count\n\(db.lastErrorMessage())\n\(ItemTableCheckQuery))"
                            
                            //print(ErrorMessage)
                            
                            loggly(LogType.Error, text: ErrorMessage)
                            
                            DoAlert.DisplayAlert(Class: "DatabaseChanges", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to Failed to get \"\(CurrentUserName!)\"`s Locations\n\n\(ItemTableCheckQuery)", DatabaseError: "\(db.lastErrorMessage())")
                            
                            
                            return
                            
                            
                        }
                    }// end of dbQueue.inTransaction
                    
                    if (countItemTable == 0) {
                        
                        dbQueue.inTransaction { _, rollback in
                            
                            let ItemTableDropQuery = String(format:"DROP TABLE IF EXISTS itemtable")
                            
                            
                            let SuccessItemTableDropQuery =  db.executeUpdate(ItemTableDropQuery, withArgumentsIn: [])
                            
                            
                            if (!SuccessItemTableDropQuery) {
                                
                                
                                //print("ERROR: DatabaseChanges: Failed to DROP itemtable\n\(ItemTableDropQuery)\n\(db.lastErrorMessage())")
                                
                                let ErrorMessage = "DatabaseChanges:\(#function) - Failed to DROP itemtable \n\(db.lastErrorMessage())\n\(ItemTableDropQuery))"
                                
                                //print(ErrorMessage)
                                
                                loggly(LogType.Error, text: ErrorMessage)
                                
                                DoAlert.DisplayAlert(Class: "DatabaseChanges", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to Failed to DROP itemtable\n\(ItemTableDropQuery)\n\n\(ItemTableDropQuery)", DatabaseError: "\(db.lastErrorMessage())")
                                return
                            }
 
                        }// end of dbQueue.inTransaction
                        
                    }
                    
                } else {
                    
                    rollback.pointee = true
                    
                    loggly(LogType.Warnings, text: "DatabaseChanges-\(#function): Unable to create  \"itemtable\"\n\(db.lastErrorMessage())")
                    
                    DoAlert.DisplayAlert(Class: "DatabaseChanges", Level: 1, Function: "\(#function)", MessageText: "An Error has occurred", InformativeText: "Database changes were rolled back from updating to \"\(AppVersion)\".\nPlease Quit and try again.\nIf this continues please restart your computer.", DatabaseError: "\nSQL Query Failed:\n\n\(ItemTableCreateStatement)\n\nLastErrorMessage: \"\(db.lastErrorMessage())\"")
                }
                
            } else {
                //print("Success in writing \"\(DBVersion)\" to \"3.4.6\"")
                loggly(LogType.Info, text: "Success in updating \"\(DBVersion)\" to \"3.4.6\"")
                
                ISUPDATING = false

                
                
            }// end of if (!Success)
            
        }// end of dbQueue.inTransaction { _, rollback in
        
    }// end of DB322
    
    func DB350() {
    
        
        
        ConnectionTest.OpenDB()
        
        
        AddAppStoreUser()
        
        
        
        dbQueue.inTransaction { _, rollback in
            
            var DBPassword = String()
            var DBIkey = String()
            
            let Query = "select password,ikey from users"
            
            //print("Query 1: \(Query)")
            
            do {
                
                let PasswordQueryRun = try db.executeQuery(Query, values:nil)
                
                while PasswordQueryRun.next() {
                    
                    DBPassword = PasswordQueryRun.string(forColumn: "password")!
                    
                    DBIkey = PasswordQueryRun.string(forColumn: "ikey")!
                    
                    let encryptedPassword = DBPassword.base64Encoded()!
                    
                    let PWUpdateQuery = "update users set password = \"\(encryptedPassword)\" WHERE ikey = \(DBIkey)"
                    
                    //print("PWUpdateQuery: \(PWUpdateQuery)")
                    
                    
                    
                    let EncodePasswordQueryRun = db.executeUpdate(PWUpdateQuery, withArgumentsIn: [])
                    
                    if (!EncodePasswordQueryRun) {
                        
                        rollback.pointee = true
                        
                        let ErrorMessage = "\(#function) - Failed to encode password"
                        
                        //print(ErrorMessage)
                        
                        loggly(LogType.Error, text: ErrorMessage)
                        
                        DoAlert.DisplayAlert(Class: "DatabaseChanges", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to encode password\nPlease try it again.\n\n", DatabaseError: "\(db.lastErrorMessage())")
                        
                        
                        return
                        
                    }
                    
                    
                    
                    
                    
                }
                
            } catch {
                
                rollback.pointee = true
                
                let ErrorMessage = "\(#function) - Failed to get user credentials from Database\n"
                
                //print(ErrorMessage)
                
                loggly(LogType.Error, text: ErrorMessage)
                
                DoAlert.DisplayAlert(Class: "DatabaseChanges", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to get user credentials from Database\nPlease try it again.\n\n", DatabaseError: "\(db.lastErrorMessage())")
                
                return
                
            }
        }
        
        
        dbQueue.inTransaction { _, rollback in
            
            //                "ALTER TABLE dbasetable ADD COLUMN appcentersendlogsprompt NUMERIC DEFAULT false" +
            //                "ALTER TABLE dbasetable ADD COLUMN alwayssendlogs NUMERIC DEFAULT true" +
            //                "UPDATE dbasetable SET dbversion = '3.5.0';"
            
            //   ALTER TABLE employees ADD status VARCHAR;
            
            
            //                2019-01-29 17:09:28.461678-0700 Scripto Portfolio[11151:2607406] [logging] near "TABLE": syntax error
            //                2019-01-29 17:09:28.461736-0700 Scripto Portfolio[11151:2607406] Error inserting batch: near "TABLE": syntax error
            
            let UpdateDBStatement = String(format:"ALTER TABLE dbasetable ADD COLUMN appcentersendlogsprompt NUMERIC DEFAULT false;" + "ALTER TABLE dbasetable ADD COLUMN alwayssendlogs NUMERIC DEFAULT true;" + "UPDATE dbasetable SET dbversion = '3.5.0';"

            )
            
            
            
            
            let Success = db.executeStatements(UpdateDBStatement)
            
            if (!Success) {
                
                rollback.pointee = true
                
                loggly(LogType.Warnings, text: "DatabaseChanges-\(#function): Unable to create to update DB to \"\(AppVersion)\"\n\(UpdateDBStatement)\n\(db.lastErrorMessage())")
                
                DoAlert.DisplayAlert(Class: "DatabaseChanges", Level: 1, Function: "\(#function)", MessageText: "An Error has occurred", InformativeText: "Database changes were rolled back from updating.\nPlease Quit and try again.\nIf this continues please restart your computer.", DatabaseError: "\nSQL Query Failed:\n\n\(UpdateDBStatement)\n\nLastErrorMessage: \"\(db.lastErrorMessage())\"")
                
                
                return
                
                
            } else {
                loggly(LogType.Info, text: "Success in updating \"\(DBVersion)\" to \"3.5.0\"")
                ISUPDATING = false
            }
            
            //DBVersion = "3.5.0"
            
        }// end of dbQueue.inTransaction {
        
        
        
    
    }// end of DB350
    
    func DB360() {
        
        
            
            ConnectionTest.OpenDB()
            
            // pragma table_info(mobiletable)
            
            
            
            let TableQuery = "pragma table_info(mobiletable)"
            
            //print("TableQuery: \"\(TableQuery)\"")
            
            
            // var CategoryListArray = ["Select a Category"]
            var TableQueryDB:Array<String> = []
            
            do {
                
                let TableQueryRun = try db.executeQuery(TableQuery, values:nil)
                
                
                while TableQueryRun.next() {
                    let Item = TableQueryRun.string(forColumn: "name")
                    
                    TableQueryDB.append(Item!)
                    
                    
                }
                
//                for item in TableQueryDB {
//                    print("item: \(item)")
//                }
                
            } catch {
                
                let ErrorMessage = "DatabaseChanges:\(#function) - Failed to get column names in mobiletable\n\n\(db.lastErrorMessage())\n\(TableQuery))"
 
                loggly(LogType.Error, text: ErrorMessage)
                
                DoAlert.DisplayAlert(Class: "DatabaseChanges", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to get column names in mobiletable\n\n\(TableQueryDB)", DatabaseError: "\(db.lastErrorMessage())")
                
                
                return
                
                
            }
            
            
            
            var Hasitemname = false
            var Hasitemcomp = false
            var Hasitemhistory = false
            
            
            
            if (!TableQueryDB.contains("itemname")) {
                
                dbQueue.inTransaction { _, rollback in
                    
                    let UpdateDBStatement = String(format:
                        "ALTER TABLE mobiletable ADD COLUMN `itemname`    TEXT;"
                    )
                    
                    let Success = db.executeStatements(UpdateDBStatement)
                    
                    if (!Success) {
                        
                        rollback.pointee = true
                        
                        loggly(LogType.Warnings, text: "DatabaseChanges-\(#function): Unable to add itemname column to mobiletable\n\(UpdateDBStatement)\n\(db.lastErrorMessage())")
                        
                        DoAlert.DisplayAlert(Class: "DatabaseChanges", Level: 1, Function: "\(#function)", MessageText: "Unable to add itemname column to mobiletable", InformativeText: "Database changes were rolled back from updating.\nPlease Quit and try again.\nIf this continues please restart your computer.", DatabaseError: "\nSQL Query Failed:\n\n\(UpdateDBStatement)\n\nLastErrorMessage: \"\(db.lastErrorMessage())\"")
                        
                        return
                        
                    }
                    
                    Hasitemname = true
                    
                }// end of dbQueue.inTransaction {
                
            } else {
                Hasitemname = true
            }
            
            if (!TableQueryDB.contains("itemcomp")) {
                
                dbQueue.inTransaction { _, rollback in
                    
                    let UpdateDBStatement = String(format:
                        "ALTER TABLE mobiletable ADD COLUMN `itemcomp`    TEXT;"
                    )
                    
                    let Success = db.executeStatements(UpdateDBStatement)
                    
                    if (!Success) {
                        
                        rollback.pointee = true
                        
                        loggly(LogType.Warnings, text: "DatabaseChanges-\(#function): Unable to add itemcomp column to mobiletable\n\(UpdateDBStatement)\n\(db.lastErrorMessage())")
                        
                        DoAlert.DisplayAlert(Class: "DatabaseChanges", Level: 1, Function: "\(#function)", MessageText: "Unable to add itemcomp column to mobiletable", InformativeText: "Database changes were rolled back from updating.\nPlease Quit and try again.\nIf this continues please restart your computer.", DatabaseError: "\nSQL Query Failed:\n\n\(UpdateDBStatement)\n\nLastErrorMessage: \"\(db.lastErrorMessage())\"")
                        
                        return
                        
                    }
                    
                    Hasitemcomp = true
                    
                }// end of dbQueue.inTransaction {
                
            } else {
                Hasitemcomp = true
            }
            
            if (!TableQueryDB.contains("itemhistory")) {
                
                dbQueue.inTransaction { _, rollback in
                    
                    let UpdateDBStatement = String(format:
                        "ALTER TABLE mobiletable ADD COLUMN `itemhistory`    TEXT;"
                    )
                    
                    let Success = db.executeStatements(UpdateDBStatement)
                    
                    if (!Success) {
                        
                        rollback.pointee = true
                        
                        loggly(LogType.Warnings, text: "DatabaseChanges-\(#function): Unable to add itemhistory column to mobiletable\n\(UpdateDBStatement)\n\(db.lastErrorMessage())")
                        
                        DoAlert.DisplayAlert(Class: "DatabaseChanges", Level: 1, Function: "\(#function)", MessageText: "Unable to add itemhistory column to mobiletable", InformativeText: "Database changes were rolled back from updating.\nPlease Quit and try again.\nIf this continues please restart your computer.", DatabaseError: "\nSQL Query Failed:\n\n\(UpdateDBStatement)\n\nLastErrorMessage: \"\(db.lastErrorMessage())\"")
                        
                        return
                        
                    }
                    
                    Hasitemhistory = true
                    
                }// end of dbQueue.inTransaction {
                
            } else {
                Hasitemhistory = true
            }
            
            
            if (Hasitemname == true && Hasitemcomp == true && Hasitemhistory == true) {
                
                dbQueue.inTransaction { _, rollback in
                    
                    let UpdateDBStatement = String(format:
                        "UPDATE dbasetable SET dbversion = '3.6.0';"
                    )
                    
                    let Success = db.executeStatements(UpdateDBStatement)
                    
                    if (!Success) {
                        
                        rollback.pointee = true
                        
                        loggly(LogType.Warnings, text: "DatabaseChanges-\(#function): Unable to set dbversion in mobiletable\n\(UpdateDBStatement)\n\(db.lastErrorMessage())")
                        
                        DoAlert.DisplayAlert(Class: "DatabaseChanges", Level: 1, Function: "\(#function)", MessageText: "Unable to set dbversion in mobiletable", InformativeText: "Database changes were rolled back from updating.\nPlease Quit and try again.\nIf this continues please restart your computer.", DatabaseError: "\nSQL Query Failed:\n\n\(UpdateDBStatement)\n\nLastErrorMessage: \"\(db.lastErrorMessage())\"")
                        
                        return
                        
                    } else {
                        Hasitemhistory = true
                        ISUPDATING = false
                        loggly(LogType.Info, text: "Success in updating \"\(DBVersion)\" to \"3.6.0\"")
                    }
                    
                    
                    
                }// end of dbQueue.inTransaction {
                
                
                
                
            }
        
            
        
        
    }// end of DB360
        
    func DB40() {
        
        
        
        ConnectionTest.OpenDB()
        
        dbQueue.inTransaction { _, rollback in
            
            let storyGoalTableCreate = "CREATE TABLE `storygoaltable` (`ikey`    INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,`userikeyreference`    INTEGER NOT NULL,`storyikeyreference`    INTEGER,`dailywordcount`    INTEGER,`totalwordcount`    INTEGER,`startgoaldate`    TEXT DEFAULT '',`endgoaldate`    TEXT DEFAULT '',`createdate`    TEXT,`status`   TEXT DEFAULT '',`completed`    INTEGER DEFAULT 0);"
            
            // print("AllStatementsUpdate: \(AllStatementsUpdate)")
            
            let storyGoalTableCreateSuccess = db.executeStatements(storyGoalTableCreate)
            
            if (!storyGoalTableCreateSuccess) {
                
                print("ERROR: \"\(db.lastErrorMessage())\"")
                
                rollback.pointee = true
                loggly(LogType.Error, text: "DatabaseChanges: \(#function) An Error has occurred SQL Query Failed:\nlastErrorMessage: \"\(db.lastErrorMessage())\"")
                DoAlert.DisplayAlert(Class: "DatabaseChanges", Level: 1, Function: "\(#function)", MessageText: "An Error has occurred", InformativeText: "Database changes were rolled back.\nPlease Quit and try again.\nIf this continues please restart your computer.", DatabaseError: "\nSQL Query Failed: QUERY\n\"\(storyGoalTableCreate)\"\nlastErrorMessage: \"\(db.lastErrorMessage())\"")
                
            } else {
                print("Success in Updating 3.6 to \"\(AppVersion)\"")
                loggly(LogType.Info, text: "Success in updating \"\(DBVersion)\" to \"4.0\"")
            }
   
            
        } // end of inTransaction
        
        
        
        
        
        
        dbQueue.inTransaction { _, rollback in
            
            
//            let AllStatementsUpdate =
//                String(format:"INSERT INTO storygoaltable(ikey, userikeyreference, storyikeyreference, dailywordcount, totalwordcount, goaldate, createdate) VALUES ((select MAX(ikey) + 1 from storygoaltable),0,0,0,500,'','');" +
//                    "INSERT INTO storygoaltable(ikey, userikeyreference, storyikeyreference, dailywordcount, totalwordcount, goaldate, createdate) VALUES ((select MAX(ikey) + 1 from storygoaltable),0,0,0,5000,'','');" +
//                    "INSERT INTO storygoaltable(ikey, userikeyreference, storyikeyreference, dailywordcount, totalwordcount, goaldate, createdate) VALUES ((select MAX(ikey) + 1 from storygoaltable),0,0,0,25000,'','');" +
//                    "INSERT INTO storygoaltable(ikey, userikeyreference, storyikeyreference, dailywordcount, totalwordcount, goaldate, createdate) VALUES ((select MAX(ikey) + 1 from storygoaltable),0,0,0,50000,'','');" +
//                    "INSERT INTO storygoaltable(ikey, userikeyreference, storyikeyreference, dailywordcount, totalwordcount, goaldate, createdate) VALUES ((select MAX(ikey) + 1 from storygoaltable),0,0,0,75000,'','');" +
//                    "INSERT INTO storygoaltable(ikey, userikeyreference, storyikeyreference, dailywordcount, totalwordcount, goaldate, createdate) VALUES ((select MAX(ikey) + 1 from storygoaltable),0,0,0,1000000,'','');" +
//                    "UPDATE dbasetable SET dbversion = '4.0.0';")
            
            
            
            let AllStatementsUpdate =
                String(format:
                    "INSERT INTO storygoaltable(ikey, userikeyreference, storyikeyreference, dailywordcount, totalwordcount, createdate, status) VALUES ((select MAX(ikey) + 1 from storygoaltable),0,0,0,500,'','t');" +
                    "INSERT INTO storygoaltable(ikey, userikeyreference, storyikeyreference, dailywordcount, totalwordcount, createdate, status) VALUES ((select MAX(ikey) + 1 from storygoaltable),0,0,0,5000,'','t');" +
                    "INSERT INTO storygoaltable(ikey, userikeyreference, storyikeyreference, dailywordcount, totalwordcount, createdate, status) VALUES ((select MAX(ikey) + 1 from storygoaltable),0,0,0,25000,'','t');" +
                    "INSERT INTO storygoaltable(ikey, userikeyreference, storyikeyreference, dailywordcount, totalwordcount, createdate, status) VALUES ((select MAX(ikey) + 1 from storygoaltable),0,0,0,50000,'','t');" +
                    "INSERT INTO storygoaltable(ikey, userikeyreference, storyikeyreference, dailywordcount, totalwordcount, createdate, status) VALUES ((select MAX(ikey) + 1 from storygoaltable),0,0,0,75000,'','t');" +
                    "INSERT INTO storygoaltable(ikey, userikeyreference, storyikeyreference, dailywordcount, totalwordcount, createdate, status) VALUES ((select MAX(ikey) + 1 from storygoaltable),0,0,0,1000000,'','t');" +
                    
                    "UPDATE dbasetable SET dbversion = '4.0.0';")
            
            let Success = db.executeStatements(AllStatementsUpdate)
            
            if (!Success) {
                
                rollback.pointee = true
                loggly(LogType.Error, text: "DatabaseChanges-\(#function) Query Failed: \"\(AllStatementsUpdate)\"")
                DoAlert.DisplayAlert(Class: "DatabaseChanges", Level: 1, Function: "\(#function)", MessageText: "An Error has occurred", InformativeText: "Database changes were rolled back.\nPlease Quit and try again.\nIf this continues please restart your computer.", DatabaseError: "\nSQL Query Failed:\nlastErrorMessage: \"\(db.lastErrorMessage())\"")
                
                return
                
                
            } else {
                print("Success in updating \"\(DBVersion)\" to \"4.0\"")
                //DBVersion = "2.1.0.1"
                ISUPDATING = false
                loggly(LogType.Info, text: "Success in updating \"\(DBVersion)\" to \"4.0\"")
            }
            
        } // end of inTransaction
        
        
        
        
        
    }// end of DB40
    
    func DB401() {
        
        

        
        ConnectionTest.OpenDB()
        
        dbQueue.inTransaction { _, rollback in
            
            let storyGoalCompletionTableCreate = String(format: "CREATE TABLE `storygoalcompletiontable` (`ikey`    INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,`userikeyreference`    INTEGER NOT NULL,`storyikeyreference`    INTEGER,`goalcompletiondate`    TEXT NOT NULL,`storygoalreference`    INTEGER);" +
                
                "CREATE TABLE `storyhistorytable` (`ikey`    INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,`storyikeyreference`    INTEGER,`startwordcount`    INTEGER,`endwordcount`    INTEGER,`startdate`    TEXT,`enddate`    TEXT,`updatedbyuserikey`    INTEGER,`ownedbyuserikey`    INTEGER);" +
                
                "UPDATE dbasetable SET dbversion = '4.0.1';")
            
            // print("AllStatementsUpdate: \(AllStatementsUpdate)")
            
            let storyGoalCompletionTableCreateRun = db.executeStatements(storyGoalCompletionTableCreate)
            
            if (!storyGoalCompletionTableCreateRun) {
                
                print("ERROR: \"\(db.lastErrorMessage())\"")
                
                rollback.pointee = true
                loggly(LogType.Error, text: "DatabaseChanges: \(#function) An Error has occurred SQL Query Failed:\nlastErrorMessage: \"\(db.lastErrorMessage())\"")
                DoAlert.DisplayAlert(Class: "DatabaseChanges", Level: 1, Function: "\(#function)", MessageText: "An Error has occurred", InformativeText: "Database changes were rolled back.\nPlease Quit and try again.\nIf this continues please restart your computer.", DatabaseError: "\nSQL Query Failed: QUERY\n\"\(storyGoalCompletionTableCreate)\"\nlastErrorMessage: \"\(db.lastErrorMessage())\"")
                
            } else {
                print("Success in Updating 4.0.1 to \"\(AppVersion)\"")
                ISUPDATING = false
                loggly(LogType.Info, text: "Success in updating \"\(DBVersion)\" to \"4.0.1\"")
            }
            
            
        } // end of inTransaction
        
        
        
        
        
        
    }// end of DB401
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    // NOT USED AS OF March 30 2018
    // Will need to create a new DBtable to include a column for the Vacuum date
    func DBVacuum() {
        
        var VacuumDBQuery = String()
        
        do {
            
            VacuumDBQuery = "VACUUM;"
            
            //print("VacuumDBQuery: \(VacuumDBQuery)")
            
            try db.executeUpdate(VacuumDBQuery, values: nil)
            
        } catch let error as NSError {
            
            let ErrorMessage = "\(#function) - Failed to do query \"\(VacuumDBQuery)\" \nError: \(error.localizedDescription)\n\(db.lastErrorMessage())"
            
            //print(ErrorMessage)
            
            loggly(LogType.Error, text: ErrorMessage)
            
            DoAlert.DisplayAlert(Class: "DatabaseChanges", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to run Query on DB\nPlease quit then try again.\n\n\(VacuumDBQuery)", DatabaseError: "\(db.lastErrorMessage())")
            
        }
        
        
    }
    
    
    
    
    
    
    
    
} // end of file
