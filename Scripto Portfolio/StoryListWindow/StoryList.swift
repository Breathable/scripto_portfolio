//
//  StoryList.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 11/17/17.
//  Copyright © 2017 DTGM. All rights reserved.
//

import Foundation

class StoryList: NSObject {
    var StoryName: String
    var LastEdit: String
    var CreateDate: String
    var Ikey: String
    
    init(StoryName: String, LastEdit: String, CreateDate: String, Ikey: String) {
        self.StoryName = StoryName
        self.LastEdit = LastEdit
        self.CreateDate = CreateDate
        self.Ikey = Ikey
    }

    
}
