//
//  RenameStoryViewController.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 2/10/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Cocoa
import SwiftLoggly

class RenameStoryViewController: NSViewController,NSTextFieldDelegate {

    @IBOutlet weak var storyNameLabel: NSTextField!
    @IBOutlet weak var newStoryNameTextField: NSTextField!
    @IBOutlet weak var nameCheckLabel: NSTextField!
    @IBOutlet weak var addStoryButton: NSButton!
    @IBOutlet weak var cancelButton: NSButton!
    
    var CurrentAppearanceRenameStoryViewController = String()
    


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.

        
        
        newStoryNameTextField.delegate = self
        
        storyNameLabel.font = NSFont(name:AppFontBold, size: 13.0)
        storyNameLabel.textColor = NSColor.white
    }
    
    override func viewWillAppear() {
        
       
        
    }
    
    
    
    
    override func viewDidLayout() {
        
        //        enum InterfaceStyle : String {
        //            case Dark, Light
        //
        //            init() {
        //                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
        //                self = InterfaceStyle(rawValue: type)!
        //            }
        //        }
        //
        //        let currentStyle = InterfaceStyle()
        //
        //        //print("currentStyle: \"\(currentStyle)\"  -  CurrentAppearance: \"\(CurrentAppearance)\" ")
        //
        //        if ("\(currentStyle)" != CurrentAppearanceNewUserViewController) {
        //            appearanceCheck()
        //        }
        
        appearanceCheck()
        
        super.viewDidLayout()
        
        print("RenameStoryViewController - viewDidLayout finished")
        

        //forgotUserTableView.deselectAll(self)
        
    }// end of viewDidLayout()
    
    
    func appearanceCheck() {
        
        enum InterfaceStyle : String {
            case Dark, Light
            
            init() {
                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
                self = InterfaceStyle(rawValue: type)!
            }
        }
        
        let currentStyle = InterfaceStyle()
        
        if ("\(currentStyle)" != CurrentAppearanceRenameStoryViewController) {
            
            if (currentStyle.rawValue == "Dark") {
                print("Dark Style Detected - RenameStoryViewController")
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColorDarkMode
                
                addStoryButton.styleButtonText(button: addStoryButton, buttonName: "Edit Name", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                addStoryButton.wantsLayer = true
                addStoryButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                addStoryButton.layer?.cornerRadius = 7
                
                cancelButton.styleButtonText(button: cancelButton, buttonName: "Cancel", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                cancelButton.wantsLayer = true
                cancelButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                cancelButton.layer?.cornerRadius = 7

                newStoryNameTextField.wantsLayer = true
                newStoryNameTextField.layer?.backgroundColor = NSColor.gray.cgColor
                newStoryNameTextField.layer?.cornerRadius = 4
                
            } else if (currentStyle.rawValue == "Light") {
                print("Light Style Detected - RenameStoryViewController")
                
                
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColor.cgColor
                
                addStoryButton.styleButtonText(button: addStoryButton, buttonName: "Edit Name", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                addStoryButton.wantsLayer = true
                addStoryButton.layer?.backgroundColor = NSColor.gray.cgColor
                addStoryButton.layer?.cornerRadius = 7
                
                cancelButton.styleButtonText(button: cancelButton, buttonName: "Cancel", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                cancelButton.wantsLayer = true
                cancelButton.layer?.backgroundColor = NSColor.gray.cgColor
                cancelButton.layer?.cornerRadius = 7
                
            }
        }
        
        CurrentAppearanceRenameStoryViewController = "\(currentStyle)"
        
        
        
        
        
        
        
        
    }//end of appearanceCheck()
    
    
    
    
    
    
    
    
    
    override func controlTextDidChange(_ obj: Notification) {
        
        if (newStoryNameTextField.stringValue.count < 1) {
            
        } else {
            
            let lastCharacter = String(describing: self.newStoryNameTextField.stringValue.last!)
            
            let SymbolTest = CharacterSet.symbols.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
            
            let punctuationTest = CharacterSet.punctuationCharacters.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
            
            let NewLineTest = CharacterSet.newlines.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
            
            if (SymbolTest) {
                //print("SymbolTest: \(SymbolTest)")
                __NSBeep()
                //print("storyNameLabel.stringValue: \(newStoryNameTextField.stringValue)")
                self.newStoryNameTextField.stringValue = String(self.newStoryNameTextField.stringValue.dropLast())
                //print("storyNameLabel.stringValue: \(newStoryNameTextField.stringValue)")
                return
            }
            if (punctuationTest) {
                //print("punctuationTest: \(punctuationTest)")
                __NSBeep()
                //print("storyNameLabel.stringValue: \(newStoryNameTextField.stringValue)")
                self.newStoryNameTextField.stringValue = String(self.newStoryNameTextField.stringValue.dropLast())
                //print("storyNameLabel.stringValue: \(newStoryNameTextField.stringValue)")
                return
            }
            if (NewLineTest) {
                //print("NewLineTest: \(NewLineTest)")
                __NSBeep()
                //print("storyNameLabel.stringValue: \(newStoryNameTextField.stringValue)")
                self.newStoryNameTextField.stringValue = String(self.newStoryNameTextField.stringValue.dropLast())
                //print("storyNameLabel.stringValue: \(newStoryNameTextField.stringValue)")
                return
            }
            
            
        }
        
        
    }
    
    func RemoveWhiteSpace(aString: String) -> String {
        let replaced = aString.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        return replaced
    }
    
    func delayWithSeconds(_ seconds: Double, completion: @escaping () -> ()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            completion()
        }
    }
    
    
    @IBAction func editStoryNameButton(_ sender: Any) {
        
        let NewStoryName = newStoryNameTextField.stringValue.trimmingCharacters(in: .whitespacesAndNewlines)
        
        //print("NewStoryName: \"\(NewStoryName)\"")
        
        newStoryNameTextField.stringValue = NewStoryName
        
        if (NewStoryName.count < 1) {
            //nameCheckLabel.textColor = NSColor.red
            nameCheckLabel.stringValue = "Please Enter in a Story Name"
            delayWithSeconds(0.25) {
                //Do something
                self.nameCheckLabel.isHidden = true
                
                self.delayWithSeconds(0.25) {
                    //Do something
                    self.nameCheckLabel.isHidden = false
                    //self.nameCheckLabel.textColor = NSColor.black
                }
            }
            return
        }
        
        
        
        ////////////////////////////
        
        ConnectionTest.CheckConnection()
        
        ConnectionTest.OpenDB()
        
        
        let DupStoryCheck = "SELECT count(ikey) as count FROM storytable WHERE storyname = \"\(NewStoryName)\" and userikeyreference in (select ikey from users where user = \"\(CurrentUserName!)\")"
        
        print("DupStoryCheck: \(DupStoryCheck)")
        
        var NumCount:Int32 = 0
        
        do {
            let result = try db.executeQuery(DupStoryCheck, values: nil)
            
            while result.next()
            {
                NumCount = result.int(forColumn: "count")
                print("NumCount: \(NumCount)")
            }
            
        } catch let error as NSError {
            
            let ErrorMessage = "\(#function) - Failed to do query \(DupStoryCheck)\nError: \(error.localizedDescription)"
            
            print(ErrorMessage)
            
            
            
            DoAlert.DisplayAlert(Class: "ViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to check the Scripto Portfolio database for username.\nPlease quit then try again.", DatabaseError: "\(DupStoryCheck)\n\n\(db.lastErrorMessage())")
        }
        
        
        if (NumCount > 0) {
            
            let alert = NSAlert()
            alert.messageText = "Warning: Another of your Stories has that name"
            alert.informativeText = "Do you want to name this the same ?"
            alert.alertStyle = .warning
            alert.addButton(withTitle: "Cancel")
            alert.addButton(withTitle: "Yes")
            
            if (CurrentAppearanceRenameStoryViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }

            
            let resultCode: Float = Float(alert.runModal().rawValue)
            
            if (resultCode == 1000) {
                //print("Cancel Pressed")
                return
            }
            
            if (resultCode == 1001) {
                //print("Yes Pressed")
                
            }
            
        }
        
        //////////////////////////////////
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        
        
        
        //////////////
        
        // StoriesLocation
        let PathToStory = "\(StoriesLocation)/\(NewStoryName).rtf"
        
        ////////////////
        
        
        let UpdateStoryQuery = "UPDATE storytable SET storyname = \"\(NewStoryName)\" WHERE ikey = \"\(EditStoryIkey)\" AND storyname = \"\(EditStoryName)\" "
        
        print("UpdateStoryQuery: \(UpdateStoryQuery)")
        
        
        //////////////////
        
        var Success = Bool()
        
        ConnectionTest.OpenDB()
        
        dbQueue.inTransaction { _, rollback in
            
        Success = db.executeStatements(UpdateStoryQuery)
            
            
            
            if (!Success) {
                
                rollback.pointee = true
                
                let ErrorMessage = "ViewController:\(#function) - Failed to update storyname\n \(db.lastErrorMessage())"
                
                print(ErrorMessage)
                
                loggly(LogType.Error, text: ErrorMessage)
                
                DoAlert.DisplayAlert(Class: "ViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to update storyname \(NewStoryName)\n\n\(UpdateStoryQuery)", DatabaseError: "\(db.lastErrorMessage())")
                
            }
            
        } // end of intransaction
        
        
        ///////////////////
        
        
        if (!Success) {
            
        } else {
            
                print("PathToStory: \(PathToStory)")
                
                let exportedFileURL = URL(fileURLWithPath:"\(PathToStory)")
            
                //let exportedFileURL = URL.init(string: "file://\(PathToStory)")
                
                print("exportedFileURL: \(exportedFileURL)")
            
            
            // exportedFileURL != nil
            if "\(exportedFileURL)".count > 0
                {
                    
                    print("PathToStory: \(PathToStory)")
                    let NewStoryName = "\(StoriesLocation)/\(EditStoryName).rtf"
                    print("NewStoryName: \(NewStoryName)")
                    
                    //  https://iswift.org/cookbook/rename-a-file
                    do {
                        try FM.moveItem(atPath: NewStoryName, toPath: PathToStory)
                    }
                    catch let error as NSError {
                        //print("Ooops! Something went wrong: \(error)")
                        
                        loggly(LogType.Error, text: "ViewController:\(#function) - Failed to rename \(NewStoryName).rtf at\n\(StoriesLocation)\n\(error.localizedDescription)")
                        
                        DoAlert.DisplayAlert(Class: "ViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to rename \(NewStoryName).rtf.\n", DatabaseError: "ERROR: \(error)")
                        
                        }
                    
                }
            
        }

        StoryListWindow.showWindow(self)
        StoryListWindow.window?.orderBack(self)
        
        let alert = NSAlert()
        alert.messageText = "Yea!"
        
        alert.informativeText = "\"\(NewStoryName)\" was updated Sucessfully\n\nYou can now open it to start writing!"

        alert.alertStyle = .warning
        alert.addButton(withTitle: "OK")
        
        if (CurrentAppearanceRenameStoryViewController == "Dark") {
            alert.window.backgroundColor = AlertBackgroundColorDarkMode
        } else {
            alert.window.backgroundColor = WindowBackgroundColor
        }
        alert.window.titlebarAppearsTransparent = true
        
        let resultCode: Float = Float(alert.runModal().rawValue)
        
        if (resultCode == 1000) {
            //print("Cancel Pressed")
            
            self.view.window?.close()
            
            newStoryNameTextField.stringValue = ""
        
            return
        }
        
        EditStoryName = ""
        EditStoryIkey = ""
 
    }
    
    @IBAction func cancelStoryEditButton(_ sender: Any) {
        
        EditStoryName = ""
        EditStoryIkey = ""
        
        
        self.view.window?.close()
        StoryListWindow.showWindow(self)
        
        newStoryNameTextField.stringValue = ""
        
    }
    
    
    
    
    
}// end of RenameStoryViewController

