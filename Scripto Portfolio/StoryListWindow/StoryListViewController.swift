//
//  StoryListViewController.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 1/19/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Cocoa
import SwiftLoggly

class StoryListViewController: NSViewController, NSTableViewDelegate, NSTableViewDataSource {
    
    
    @IBOutlet weak var welcomeUserLabel: NSTextField!
    
    @IBOutlet weak var storyIdeasButton: NSButton!
    @IBOutlet weak var locationsButton: NSButton!
    @IBOutlet weak var charactersButton: NSButton!
    @IBOutlet weak var openStoryButton: NSButton!
    @IBOutlet weak var logoutButton: NSButton!
    @IBOutlet weak var editStoryNameButton: NSButton!
    @IBOutlet weak var removeStoryButton: NSButton!
    @IBOutlet weak var addStoryButton: NSButton!
    @IBOutlet weak var storyListTableView: NSTableView!
    @IBOutlet weak var itemsButton: NSButton!
    
    
    
    
    @IBOutlet weak var storySearchField: NSSearchField!
    var UsingSearch = false
    
    

    
    var Stories: [StoryList] = []
    
    var CurrentAppearanceStoryListViewController = String()

    
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        

        
        storyListTableView.dataSource = self
        storyListTableView.delegate = self
        
        let DoubleSelect: Selector = #selector(openStoryButton(_:))
        storyListTableView.doubleAction = DoubleSelect
        
        NotificationCenter.default.addObserver(self, selector: #selector(StoryTableData), name: NSNotification.Name(rawValue: "StoryTableDataReload"), object: nil)
        
        print("StoryListViewController - viewDidLoad finished")
        
       
    }
    
    
    override func viewWillAppear() {
        
       
        
        storyListTableView.tableColumn(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "ikey"))?.isHidden = true
        
        
        
        StoryTableData()
        
        editStoryNameButton.isEnabled = false
        removeStoryButton.isEnabled = false
        
        //appearanceCheck()
        
        print("StoryListViewController - viewWillAppear finished")
        
    }
    
    override func viewDidAppear() {
        
 
        //https://developer.apple.com/documentation/appkit/nsviewcontroller
        
        
        
        print("StoryListViewController - viewDidAppear finished")
        
    }
    
    
    
    override func viewDidLayout() {
    
        enum InterfaceStyle : String {
            case Dark, Light

            init() {
                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
                self = InterfaceStyle(rawValue: type)!
            }
        }

        let currentStyle = InterfaceStyle()

        //print("currentStyle: \"\(currentStyle)\"  -  CurrentAppearance: \"\(CurrentAppearance)\" ")

        if ("\(currentStyle)" != CurrentAppearanceStoryListViewController) {
            appearanceCheck()
        }

        
        
        super.viewDidLayout()
        
        print("StoryListViewController - viewDidLayout finished")
        
        
    }// end of viewDidLayout()
    
    
    func appearanceCheck() {
        
        enum InterfaceStyle : String {
            case Dark, Light
            
            init() {
                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
                self = InterfaceStyle(rawValue: type)!
            }
        }
        
        let currentStyle = InterfaceStyle()
    
        welcomeUserLabel.stringValue = "Welcome \(CurrentUserName!)"
        welcomeUserLabel.sizeToFit()
        
        if (currentStyle.rawValue == "Dark") {
            print("Dark Style Detected - STORYLISTVIEWCONTROLLER")
            
            welcomeUserLabel.textColor = .lightGray
            
            storyListTableView.backgroundColor = TableBackgroundColorDarkMode
            
            view.wantsLayer = true
            view.layer?.backgroundColor = WindowBackgroundColorDarkMode
            
            storyIdeasButton.styleButtonText(button: storyIdeasButton, buttonName: "Story Ideas", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
            locationsButton.styleButtonText(button: locationsButton, buttonName: "Locations", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
            charactersButton.styleButtonText(button: charactersButton, buttonName: "Characters", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
            openStoryButton.styleButtonText(button: openStoryButton, buttonName: "Open", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
            logoutButton.styleButtonText(button: logoutButton, buttonName: "Logout", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
            itemsButton.styleButtonText(button: itemsButton, buttonName: "Items", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
            
            
            itemsButton.wantsLayer = true
            itemsButton.layer?.backgroundColor = NSColor.lightGray.cgColor
            itemsButton.layer?.cornerRadius = 7
            
            storyIdeasButton.wantsLayer = true
            storyIdeasButton.layer?.backgroundColor = NSColor.lightGray.cgColor
            storyIdeasButton.layer?.cornerRadius = 7
            
            locationsButton.wantsLayer = true
            locationsButton.layer?.backgroundColor = NSColor.lightGray.cgColor
            locationsButton.layer?.cornerRadius = 7
            
            charactersButton.wantsLayer = true
            charactersButton.layer?.backgroundColor = NSColor.lightGray.cgColor
            charactersButton.layer?.cornerRadius = 7
            
            openStoryButton.wantsLayer = true
            openStoryButton.layer?.backgroundColor = NSColor.lightGray.cgColor
            openStoryButton.layer?.cornerRadius = 7
            
            logoutButton.wantsLayer = true
            logoutButton.layer?.backgroundColor = NSColor.lightGray.cgColor
            logoutButton.layer?.cornerRadius = 7
            
            
            
        } else if (currentStyle.rawValue == "Light") {
            print("Light Style Detected - STORYLISTVIEWCONTROLLER")
            
            welcomeUserLabel.textColor = .white
            
            view.wantsLayer = true
            view.layer?.backgroundColor = WindowBackgroundColor.cgColor

            storyIdeasButton.styleButtonText(button: storyIdeasButton, buttonName: "Story Ideas", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
            locationsButton.styleButtonText(button: locationsButton, buttonName: "Locations", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
            charactersButton.styleButtonText(button: charactersButton, buttonName: "Characters", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
            openStoryButton.styleButtonText(button: openStoryButton, buttonName: "Open", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
            logoutButton.styleButtonText(button: logoutButton, buttonName: "Logout", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
            itemsButton.styleButtonText(button: itemsButton, buttonName: "Items", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
            
            itemsButton.wantsLayer = true
            itemsButton.layer?.backgroundColor = NSColor.gray.cgColor
            itemsButton.layer?.cornerRadius = 7
            
            storyIdeasButton.wantsLayer = true
            storyIdeasButton.layer?.backgroundColor = NSColor.gray.cgColor
            storyIdeasButton.layer?.cornerRadius = 7
            
            locationsButton.wantsLayer = true
            locationsButton.layer?.backgroundColor = NSColor.gray.cgColor
            locationsButton.layer?.cornerRadius = 7
            
            charactersButton.wantsLayer = true
            charactersButton.layer?.backgroundColor = NSColor.gray.cgColor
            charactersButton.layer?.cornerRadius = 7
            
            openStoryButton.wantsLayer = true
            openStoryButton.layer?.backgroundColor = NSColor.gray.cgColor
            openStoryButton.layer?.cornerRadius = 7
            
            logoutButton.wantsLayer = true
            logoutButton.layer?.backgroundColor = NSColor.gray.cgColor
            logoutButton.layer?.cornerRadius = 7

        }
            
        
        
        CurrentAppearanceStoryListViewController = "\(currentStyle)"
        
       
        
 
    }//end of appearanceCheck()
    
    
    
    @IBAction func storySearch(_ sender: Any) {
        
        if (storySearchField.stringValue.count < 1 || UsingSearch == true) {
            UsingSearch = false
            StoryTableData()
        } else {
            
            UsingSearch = true
            StoryTableData()
        }
        
        
        
    }// end of storySearch
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    func tableViewSelectionDidChange(_ notification: Notification) {

        print("Row Clicked")
        let SelectedRow = storyListTableView.selectedRow
        print("SelectedRow: \(SelectedRow)")
        
        
        let isIndexValid = Stories.indices.contains(SelectedRow)
        
        if isIndexValid {
            let StoryString: String = self.Stories[SelectedRow].StoryName
            let StoryIkey: String = self.Stories[SelectedRow].Ikey
            print("Selected Story: \"\(StoryIkey)\" \"\(StoryString)\"")
            
            removeStoryButton.isEnabled = true
            editStoryNameButton.isEnabled = true
            
            EditStoryName = StoryString
            EditStoryIkey = StoryIkey
            
            
        } else {
            removeStoryButton.isEnabled = false
            editStoryNameButton.isEnabled = false
        }

    }
    
    
    
    
    
    
    
    
    

    
    ////////////////////////////////////////////////
    
    @objc func StoryTableData() {
        
        Stories.removeAll()
        
        
        var StoryName: String!
        var LastEdit: String!
        var CreateDate: String!
        var Ikey: String!
        
        var Query = String()
        
        db.open()
        
        do {
            
            
            
            
            if (UsingSearch == false) {
                Query = "select * from storytable where userikeyreference in (select ikey from users where user = \"\(CurrentUserName!)\")"
            } else {
                Query = "select * from storytable where userikeyreference in (select ikey from users where user = \"\(CurrentUserName!)\" and storyname LIKE '%\(storySearchField.stringValue)%')"
            }
            
            print("Query: \"\(Query)\"")
            
            let StoryQuery = try db.executeQuery(Query, values:nil)
            
            
            while StoryQuery.next() {
                StoryName = StoryQuery.string(forColumn: "storyname")
                LastEdit = StoryQuery.string(forColumn: "dateoflastedit")
                CreateDate = StoryQuery.string(forColumn: "createdate")
                Ikey = StoryQuery.string(forColumn: "ikey")
                
//                print("StoryName: \(StoryName)")
//                print("LastEdit: \(LastEdit)")
//                print("CreateDate: \(CreateDate)")
//                print("Ikey: \(Ikey)")
                
                let StoryToAdd = StoryList.init(StoryName: StoryName, LastEdit: LastEdit, CreateDate: CreateDate, Ikey: Ikey)
                
                Stories.append(StoryToAdd)
                
            }
            
        } catch {
            
            let ErrorString = "ERROR: StoryListViewController Adding to Stories Array \(db.lastErrorMessage())"
            
            print("\(ErrorString)")
            
            loggly(LogType.Error, text: "ERROR: StoryListViewController Adding to Stories Array \(db.lastErrorMessage())")
            
            DoAlert.DisplayAlert(Class: "StoryListViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to Add to Stories Array\n\n\(Query)", DatabaseError: "\(db.lastErrorMessage())")
        }
        
        //print("Stories: \(Stories)")
        
        storyListTableView.reloadData()
        
    }
    
    
    
    func numberOfRows(in tableVieww: NSTableView) -> Int {
        print("Stories.count: \(Stories.count)")
        return Stories.count
        
    }
    
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        
        guard let cell = tableView.makeView(withIdentifier: tableColumn!.identifier, owner: self) as? NSTableCellView else
        {
            print("Table Returned Nil")
            return nil
        }
        
        if (tableColumn?.identifier)!.rawValue == "ikey" {
            cell.textField?.stringValue = Stories[row].Ikey
        }
        if (tableColumn?.identifier)!.rawValue == "storyname" {
            cell.textField?.stringValue = Stories[row].StoryName
        }
        if (tableColumn?.identifier)!.rawValue == "lastedit" {
            cell.textField?.stringValue = Stories[row].LastEdit
        }
        if (tableColumn?.identifier)!.rawValue == "created" {
            cell.textField?.stringValue = Stories[row].CreateDate
        }

        return cell
        
    }//end of tableView
    
    
    
    
    ////////////////////////////////////////////////
    
    
    
    
    @IBAction func storyIdeasButton(_ sender: Any) {
        print("storyIdeasButton Pressed")
        StoryIdeasWindow.showWindow(self)
    }
    @IBAction func locationsButton(_ sender: Any) {
        print("locationsButton Pressed")
        LocationsWindow.showWindow(self)
    }
    @IBAction func charactersButton(_ sender: Any) {
        print("charactersButton Pressed")
        CharacterWindow.showWindow(self)
    }
    @IBAction func openStoryButton(_ sender: Any) {
        //print("openStoryButton Pressed")
        
        let SelectedRow = storyListTableView.selectedRow
        
        if (SelectedRow == -1) {
            return
        }
        
        let StoryString: String = self.Stories[SelectedRow].StoryName
        let StoryIkey: String = self.Stories[SelectedRow].Ikey
        print("Selected Story: \"\(StoryIkey)\" \"\(StoryString)\"")

        StoryNameOpen = StoryString
        StoryIkeyOpen = StoryIkey
        
        
        //OpenWindows.CheckForOpenWindows()
    
        //delayWithSeconds(1.0) {
            StoryWindow.showWindow(self)
        //}
        

    }
    @IBAction func logoutButton(_ sender: Any) {
        //print("logoutButton Pressed")
        
        //  ToLoginSegue
        self.view.window!.close()
        
        OpenWindows.CloseAllWindows()
        
        delayWithSeconds(0.5) {
            
            NSApp.mainMenu?.item(at: 0)?.submenu?.removeItem(at: 4) // App Themes
            
            NSApp.mainMenu?.item(at: 0)?.submenu?.removeItem(at: 3) // Wanted Features

            NSApp.mainMenu?.item(at: 0)?.submenu?.removeItem(at: 2) // Preferences

            CurrentUserIkey = 0
            CurrentUserName = ""
            
            ///////////////////
            
            
            NSApp.mainMenu?.removeItem(at: 5) // UserMenu
            
            

            

            
            
            LoginWindow.showWindow(self)
            
            
            OpenWindows.CheckForOpenWindows()
            
            
            
        }
        
    }
    
    @IBAction func editStoryNameButton(_ sender: Any) {
        //print("editStoryNameButton Pressed")

        let SelectedRow = storyListTableView.selectedRow
        print("SelectedRow: \(SelectedRow)")
        
        if (SelectedRow == -1) {
            return
        }
        
        
        let StoryString: String = self.Stories[SelectedRow].StoryName
        let StoryIkey: String = self.Stories[SelectedRow].Ikey
        print("Selected Story: \"\(StoryIkey)\" \"\(StoryString)\"")
        
        EditStoryName = StoryString
        EditStoryIkey = StoryIkey
        
        self.view.window?.close()
        
        RenameStoryWindow.showWindow(self)
    
        
    }
    @IBAction func removeStoryButton(_ sender: Any) {
        //print("removeStoryButton Pressed")
        
        
        let alert = NSAlert()
        alert.messageText = "Are you sure you want to delete \"\(EditStoryName)\""
        //alert.informativeText = "Please Enter a Story Name"
        alert.alertStyle = .warning
        alert.window.titlebarAppearsTransparent = true
        
        
        if (CurrentAppearanceStoryListViewController == "Dark") {
            alert.window.backgroundColor = AlertBackgroundColorDarkMode
        } else {
            alert.window.backgroundColor = WindowBackgroundColor
        }
        
        
        alert.addButton(withTitle: "Cancel")
        alert.addButton(withTitle: "Yes Delete")
        
        
        let returnCode = alert.runModal()
        print("returnCode: \(returnCode)")
        
        if (returnCode.rawValue == 1000) {
            
            print("Cancel Pressed")
            
            
        }

        if (returnCode.rawValue == 1001) {
           print("Yes Delete Pressed")
            
            
            let DeleteStoryQuery = "DELETE FROM storytable WHERE ikey = \"\(EditStoryIkey)\" AND storyname = \"\(EditStoryName)\" "
            
            print("DeleteStoryQuery: \(DeleteStoryQuery)")
            
            
            //////////////////
            
            var Success = Bool()
            
            ConnectionTest.OpenDB()
            
            dbQueue.inTransaction { _, rollback in
                
                Success = db.executeStatements(DeleteStoryQuery)
                
                if (!Success) {
                    
                    rollback.pointee = true
                    
                    let ErrorMessage = "StoryListViewController:\(#function) - Failed to delete storyname\n\(db.lastErrorMessage())\n\(DeleteStoryQuery))"
                    
                    print(ErrorMessage)
                    
                    loggly(LogType.Error, text: ErrorMessage)
                    
                    DoAlert.DisplayAlert(Class: "StoryListViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to delete storyname \(EditStoryName)\n\n\(DeleteStoryQuery)", DatabaseError: "\(db.lastErrorMessage())")
                    
                }
                
            } // end of intransaction
            
            if (Success) {
                
                let StoryToRemove = "\(StoriesLocation)/\(EditStoryName).rtf"
                print("StoryToRemove: \(StoryToRemove)")
                
                do {

                    // Check if file exists
                    if FM.fileExists(atPath: StoryToRemove) {
                        // Delete file
                        try FM.removeItem(atPath: StoryToRemove)
                    } else {
                        let ErrorMessage = "StoryListViewController:\(#function) -  \"\(StoryToRemove)\" does not exist\n\n\(DeleteStoryQuery))"
                        
                        print(ErrorMessage)
                        
                        loggly(LogType.Error, text: ErrorMessage)
                        
                        DoAlert.DisplayAlert(Class: "StoryListViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "\"\(EditStoryName)\" does not exist\n\n\(DeleteStoryQuery)", DatabaseError: "")
                    }
                    
                }
                catch let error as NSError {
                    print("An error took place: \(error)")
                    print("File does not exist")
                    
                    let ErrorMessage = "StoryListViewController:\(#function) - Failed to delete \(StoryToRemove)\n\n\(DeleteStoryQuery)"
                    
                    print(ErrorMessage)
                    
                    loggly(LogType.Error, text: ErrorMessage)
                    
                    DoAlert.DisplayAlert(Class: "StoryListViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to delete Story: \(EditStoryName)\n\n\(DeleteStoryQuery)", DatabaseError: "\(error)")
                    
                }
                
                
                StoryTableData()
                
            }
            
            
            
            
            
        }// end of returncode 1001
        
        
        
        
    }
    @IBAction func addStoryButton(_ sender: Any) {
        print("addStoryButton Pressed")
        
        
       
        
        self.view.window?.close()
        
        
        AddStoryWindow.showWindow(self)


        
        
        
    }
    
    
    func RemoveWhiteSpace(aString: String) -> String {
        let replaced = aString.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        return replaced
    }
    
    func delayWithSeconds(_ seconds: Double, completion: @escaping () -> ()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            completion()
        }
    }
    
  
    @IBAction func itemsButtonAction(_ sender: Any) {
        ItemsWindow.showWindow(self)
        
        //LocationImagesWindow.showWindow(nil)
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
