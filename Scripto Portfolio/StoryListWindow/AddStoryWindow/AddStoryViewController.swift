//
//  AddStoryViewController.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 2/8/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Cocoa
import SwiftLoggly

class AddStoryViewController: NSViewController, NSTextFieldDelegate {
    
    
    
    @IBOutlet weak var storyNameLabel: NSTextField!
    @IBOutlet weak var newStoryNameTextField: NSTextField!
    @IBOutlet weak var nameCheckLabel: NSTextField!
    @IBOutlet weak var addStoryButton: NSButton!
    @IBOutlet weak var cancelButton: NSButton!
    
    
    
    var CurrentAppearanceAddStoryViewController = String()
    
   

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        
        //AddStoryWindow.window?.isReleasedWhenClosed = true
        //self.view.window?.isReleasedWhenClosed = true
        
        

        
        
        newStoryNameTextField.delegate = self
        
        storyNameLabel.font = NSFont(name:AppFontBold, size: 13.0)
        storyNameLabel.textColor = NSColor.white
        
        //////////////////////
        
       
    }
    
    override func viewWillAppear() {
        
        
        
    }
    

    override func viewDidLayout() {
        
        //        enum InterfaceStyle : String {
        //            case Dark, Light
        //
        //            init() {
        //                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
        //                self = InterfaceStyle(rawValue: type)!
        //            }
        //        }
        //
        //        let currentStyle = InterfaceStyle()
        //
        //        //print("currentStyle: \"\(currentStyle)\"  -  CurrentAppearance: \"\(CurrentAppearance)\" ")
        //
        //        if ("\(currentStyle)" != CurrentAppearanceNewUserViewController) {
        //            appearanceCheck()
        //        }
        
        appearanceCheck()
        
        super.viewDidLayout()
        
        print("AddStoryViewController - viewDidLayout finished")
        
        
      
        
    }// end of viewDidLayout()
    
    
    func appearanceCheck() {
        
        enum InterfaceStyle : String {
            case Dark, Light
            
            init() {
                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
                self = InterfaceStyle(rawValue: type)!
            }
        }
        
        let currentStyle = InterfaceStyle()
        
        if ("\(currentStyle)" != CurrentAppearanceAddStoryViewController) {
            
            if (currentStyle.rawValue == "Dark") {
                print("Dark Style Detected - AddStoryViewController")
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColorDarkMode
                
                addStoryButton.styleButtonText(button: addStoryButton, buttonName: "Add Story", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                addStoryButton.wantsLayer = true
                addStoryButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                addStoryButton.layer?.cornerRadius = 7
                
                cancelButton.styleButtonText(button: cancelButton, buttonName: "Cancel", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                cancelButton.wantsLayer = true
                cancelButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                cancelButton.layer?.cornerRadius = 7

                newStoryNameTextField.wantsLayer = true
                newStoryNameTextField.layer?.backgroundColor = NSColor.gray.cgColor
                newStoryNameTextField.layer?.cornerRadius = 4
                
                
                
                
                
            } else if (currentStyle.rawValue == "Light") {
                print("Light Style Detected - AddStoryViewController")
                
                
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColor.cgColor
                
                addStoryButton.styleButtonText(button: addStoryButton, buttonName: "Add Story", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                addStoryButton.wantsLayer = true
                addStoryButton.layer?.backgroundColor = NSColor.gray.cgColor
                addStoryButton.layer?.cornerRadius = 7
                
                cancelButton.styleButtonText(button: cancelButton, buttonName: "Cancel", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                cancelButton.wantsLayer = true
                cancelButton.layer?.backgroundColor = NSColor.gray.cgColor
                cancelButton.layer?.cornerRadius = 7
                
            }
        }
        
        CurrentAppearanceAddStoryViewController = "\(currentStyle)"
       

        
    }//end of appearanceCheck()
    
    
    
    
    
    
    
    
    //newStoryNameTextField
    
    override func controlTextDidChange(_ obj: Notification) {

        if (newStoryNameTextField.stringValue.count < 1) {

        } else {

            let lastCharacter = String(describing: self.newStoryNameTextField.stringValue.last!)

            let SymbolTest = CharacterSet.symbols.isSuperset(of: CharacterSet(charactersIn: lastCharacter))

            let punctuationTest = CharacterSet.punctuationCharacters.isSuperset(of: CharacterSet(charactersIn: lastCharacter))

            let NewLineTest = CharacterSet.newlines.isSuperset(of: CharacterSet(charactersIn: lastCharacter))

            if (SymbolTest) {
                //print("SymbolTest: \(SymbolTest)")
                __NSBeep()
                //print("storyNameLabel.stringValue: \(newStoryNameTextField.stringValue)")
                self.newStoryNameTextField.stringValue = String(self.newStoryNameTextField.stringValue.dropLast())
                //print("storyNameLabel.stringValue: \(newStoryNameTextField.stringValue)")
                return
            }
            if (punctuationTest) {
                //print("punctuationTest: \(punctuationTest)")
                __NSBeep()
                //print("storyNameLabel.stringValue: \(newStoryNameTextField.stringValue)")
                self.newStoryNameTextField.stringValue = String(self.newStoryNameTextField.stringValue.dropLast())
                //print("storyNameLabel.stringValue: \(newStoryNameTextField.stringValue)")
                return
            }
            if (NewLineTest) {
                //print("NewLineTest: \(NewLineTest)")
                __NSBeep()
                //print("storyNameLabel.stringValue: \(newStoryNameTextField.stringValue)")
                self.newStoryNameTextField.stringValue = String(self.newStoryNameTextField.stringValue.dropLast())
                //print("storyNameLabel.stringValue: \(newStoryNameTextField.stringValue)")
                return
            }


        }


    }
    
    func RemoveWhiteSpace(aString: String) -> String {
        let replaced = aString.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        return replaced
    }
    
    func delayWithSeconds(_ seconds: Double, completion: @escaping () -> ()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            completion()
        }
    }
    
    
    @IBAction func addStoryButton(_ sender: Any) {
        
        //print("storyNameLabel.stringValue: \"\(newStoryNameTextField.stringValue)\"")
        
        let NewStoryName = newStoryNameTextField.stringValue.trimmingCharacters(in: .whitespacesAndNewlines)
        
        //print("NewStoryName: \"\(NewStoryName)\"")
        
        newStoryNameTextField.stringValue = NewStoryName
        
        if (NewStoryName.count < 1) {
            //nameCheckLabel.textColor = NSColor.red
            nameCheckLabel.stringValue = "Please Enter in a Story Name"
            delayWithSeconds(0.25) {
                //Do something
                self.nameCheckLabel.isHidden = true
                
                self.delayWithSeconds(0.25) {
                    //Do something
                    self.nameCheckLabel.isHidden = false
                    //self.nameCheckLabel.textColor = NSColor.black
                }
            }
            return
        }
        
        
        
        ////////////////////////////
        
        ConnectionTest.CheckConnection()
        
        ConnectionTest.OpenDB()
        
        
        let DupStoryCheck = "SELECT count(ikey) as count FROM storytable WHERE storyname = \"\(NewStoryName)\" and userikeyreference in (select ikey from users where user = \"\(CurrentUserName!)\")"
        
        print("DupStoryCheck: \(DupStoryCheck)")
        
        var NumCount:Int32 = 0
        
        do {
            let result = try db.executeQuery(DupStoryCheck, values: nil)
            
            while result.next()
            {
                NumCount = result.int(forColumn: "count")
                print("NumCount: \(NumCount)")
            }
            
        } catch let error as NSError {
            
            let ErrorMessage = "\(#function) - Failed to do query \(DupStoryCheck)\nError: \(error.localizedDescription)"
            
            print(ErrorMessage)
            
            
            
            DoAlert.DisplayAlert(Class: "ViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to check the Scripto Portfolio database for username.\nPlease quit then try again.", DatabaseError: "\(DupStoryCheck)\n\n\(db.lastErrorMessage())")
        }
        
        
        if (NumCount > 0) {
            
            let alert = NSAlert()
            alert.messageText = "Warning: Another of your Stories has that name"
            alert.informativeText = "Do you want to name this the same ?"
            alert.alertStyle = .warning
            alert.addButton(withTitle: "Cancel")
            alert.addButton(withTitle: "Yes")
            
            if (CurrentAppearanceAddStoryViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            let resultCode: Float = Float(alert.runModal().rawValue)

            if (resultCode == 1000) {
                //print("Cancel Pressed")
                return
            }
            
            if (resultCode == 1001) {
                //print("Yes Pressed")
                
            }
            
        }
        
        //////////////////////////////////
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        
        let dateString = dateFormatter.string(from: Date())
        
        //////////////
        
        // StoriesLocation
        let PathToStory = "\(StoriesLocation)/\(NewStoryName).rtf"
        
        ////////////////
        
        let CreateStoryQuery = "INSERT INTO storytable (ikey,userikeyreference,storyname,dateoflastedit,createdate,pathtostory) VALUES ((select MAX(ikey)+ 1 from storytable),\(CurrentUserIkey!),\"\(NewStoryName)\",\"\(dateString)\",\"\(dateString)\",\"\(PathToStory)\")"
        
        print("CreateStoryQuery: \(CreateStoryQuery)")
        
        let UpdateStoryQuery = "UPDATE storytable SET storyname = \"\(NewStoryName)\" WHERE ikey = \"\(EditStoryIkey)\" AND storyname = \"\(EditStoryName)\" "
        
        print("UpdateStoryQuery: \(UpdateStoryQuery)")
        
        
        //////////////////
        
        var Success = Bool()
        
        ConnectionTest.OpenDB()
        
        dbQueue.inTransaction { _, rollback in

            print("EditStoryNameBool = false")
            Success = db.executeStatements(CreateStoryQuery)

            if (!Success) {
            
                rollback.pointee = true
            
                let ErrorMessage = "ViewController:\(#function) - Failed to create \(NewStoryName).rtf file\n \(db.lastErrorMessage())"
                
                print(ErrorMessage)
                
                loggly(LogType.Error, text: ErrorMessage)
                
                DoAlert.DisplayAlert(Class: "ViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to create \(NewStoryName).rtf file\n\n\(CreateStoryQuery)", DatabaseError: "\(db.lastErrorMessage())")
                    
                   
                    
                }

        } // end of intransaction
        

        ///////////////////
        
        
        if (!Success) {
            
        } else {
           
            do {
                
                print("PathToStory: \(PathToStory)")
            
                let exportedFileURL = URL(fileURLWithPath:"\(PathToStory)")
                
                
                //let exportedFileURL = URL.init(string: "file://\(PathToStory)")
                
                print("exportedFileURL: \(exportedFileURL)")
                

                if exportedFileURL != nil
                {
                    
                    
                    let textDNA = NSAttributedString(string: " ")
                    
                    let range = NSRange(0..<1)
                    
                    let textTSave = try textDNA.fileWrapper(from: range, documentAttributes: [NSAttributedString.DocumentAttributeKey.documentType:NSAttributedString.DocumentType.rtf])
                    try textTSave.write(to: exportedFileURL as URL, options: FileWrapper.WritingOptions.atomic, originalContentsURL: nil)

                }
                
            } catch {
                    
                    loggly(LogType.Error, text: "ViewController:\(#function) - Failed to create \(NewStoryName).rtf at\n\(StoriesLocation)\n\(error.localizedDescription)")
                    
                    DoAlert.DisplayAlert(Class: "ViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to create \(NewStoryName).rtf.\nPlease quit then try again.", DatabaseError: "\(DupStoryCheck)\n\n\(db.lastErrorMessage())")
                    
                    return
            }
            
        }

        let alert = NSAlert()
        
        StoryListWindow.showWindow(self)
        StoryListWindow.window?.orderBack(self)
        
        
        alert.messageText = "Yea!"
    
        alert.informativeText = "\"\(NewStoryName)\" was added Sucessfully\n\nYou can now open it to start writing!"
        
        alert.alertStyle = .warning
        alert.addButton(withTitle: "OK")
        
        if (CurrentAppearanceAddStoryViewController == "Dark") {
            alert.window.backgroundColor = AlertBackgroundColorDarkMode
        } else {
            alert.window.backgroundColor = WindowBackgroundColor
        }
        alert.window.titlebarAppearsTransparent = true
        
        
        let resultCode: Float = Float(alert.runModal().rawValue)
        
        if (resultCode == 1000) {
            //print("Cancel Pressed")
            
            self.view.window?.close()
            
            newStoryNameTextField.stringValue = ""
            
            AddStoryWindow.dismissController(self)
            
            return
        }
        
       
        EditStoryName = ""
        EditStoryIkey = ""
       
        
        
    } // end of addStoryButton
    
    
    @IBAction func cancelStoryButton(_ sender: Any) {
        EditStoryName = ""
        EditStoryIkey = ""
        
        
        self.view.window?.close()
        StoryListWindow.showWindow(self)
        
        newStoryNameTextField.stringValue = ""
        
      
    }
    
    
  
    
    
    
    
}
