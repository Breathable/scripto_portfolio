//
//  AppDelegate.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 1/19/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Cocoa
import FMDB
import SwiftLoggly
import SwiftyUserDefaults

import AppCenter
import AppCenterAnalytics
import AppCenterCrashes



@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    //var QuitCalled = 0
    
    var appearance = String()
    
    
    

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
        

        appearanceCheck()
        
        
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application

    }
    
    
    func appearanceCheck() {
        
        enum InterfaceStyle : String {
            case Dark, Light
            
            init() {
                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
                self = InterfaceStyle(rawValue: type)!
            }
        }
        
        
        let currentStyle = InterfaceStyle()
        
        if (currentStyle.rawValue == "Dark") {
            print("Dark Style Detected - ViewController")

            appearance = "Dark"
            
        } else if (currentStyle.rawValue == "Light") {
            appearance = "Light"
        }
        
        
    }
    
    
    
    
    
    

    
    @IBAction func quitAppMenuItem(_ sender: Any) {
        
      
        
        
    if (StorySaved == false) {

        let Storyname = StoryWindow.window?.title

        appearanceCheck()
        
        let alert = NSAlert()
        alert.messageText = "The story \"\(Storyname!)\" is currently open and not Saved"
        alert.informativeText = "Do you want to Save before close?"
        alert.alertStyle = .warning
        alert.window.titlebarAppearsTransparent = true
        
        if (appearance == "Dark") {
            alert.window.backgroundColor = AlertBackgroundColorDarkMode
        } else {
            alert.window.backgroundColor = WindowBackgroundColor
        }
        
        alert.addButton(withTitle: "Save")
        alert.addButton(withTitle: "Close")
        alert.addButton(withTitle: "Cancel")

        let returnCode = alert.runModal()
        print("returnCode: \(returnCode)")

        if (returnCode.rawValue == 1000) {
            print("OK Pressed")
            StoryWC.window?.makeKeyAndOrderFront(self)
            
            delayWithSeconds(1.0) {
                
                RefreshStoryTable = false
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SaveStory"), object: nil)

                if (StorySaved == true) {
                    print("StorySaved == true")
                    
                    
                    //Defaults["Running App"] = false
                    loggly(LogType.Info, text: "Scripto Portfolio Quit")
                    NSApplication.shared.terminate(self)
                } else {
                    
                    while (StorySaved == false) {
                        
                        if (StorySaved == true) {
                            print("StorySaved == true")
                            
                            //Defaults["Running App"] = false
                            loggly(LogType.Info, text: "Scripto Portfolio Quit")
                            NSApplication.shared.terminate(self)
                        }
                        
                    }
                    
                }
                
            }
        }
        
        if (returnCode.rawValue == 1001) {
            print("Close Pressed")
            //Defaults["Running App"] = false
            loggly(LogType.Info, text: "Scripto Portfolio Quit")
            NSApplication.shared.terminate(self)
        }
        
        if (returnCode.rawValue == 1002) {
            print("Cancel Pressed")
            
            loggly(LogType.Info, text: "Canceled Quit")
        }

    
        
    } else {
        
        QuitApp()
        
    }
        

       
        
        

    }/// end of quitAppMenuItem
    
    
    func QuitApp() {
        
        appearanceCheck()
        
        let alert = NSAlert()
        alert.messageText = "Do you want to Quit"
        //alert.informativeText = "Please quit and try again"
        alert.alertStyle = .warning
        alert.addButton(withTitle: "Cancel")
        alert.addButton(withTitle: "Quit")
        if (appearance == "Dark") {
            alert.window.backgroundColor = AlertBackgroundColorDarkMode
        } else {
            alert.window.backgroundColor = WindowBackgroundColor
        }
        alert.window.titlebarAppearsTransparent = true
        
        let response: NSApplication.ModalResponse = alert.runModal()
        
        if (response == NSApplication.ModalResponse.alertFirstButtonReturn) {
            //print("Cancel button")
            loggly(LogType.Info, text: "Quit Canceled")
        }
        if (response == NSApplication.ModalResponse.alertSecondButtonReturn) {
            //print("Quit button")
            //Defaults["Running App"] = false
            loggly(LogType.Info, text: "Scripto Portfolio Quit")
            NSApplication.shared.terminate(self)
        }
        
    }
    
    
    @IBAction func AboutAppMenuItem(_ sender: Any) {
        
        AboutWindow.showWindow(self)
        
    }
    
    
    
    @IBAction func PrintMenuButton(_ sender: NSMenuItem) {
        
        
        
        if (StoryWindow.window!.isVisible) {
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "print"), object: nil)
            
        } else {
            
            let alert = NSAlert()
            alert.messageText = "No Story is open to Print"
            alert.informativeText = "Please open the story you want to Print"
            alert.alertStyle = .warning
            alert.window.titlebarAppearsTransparent = true
            
            if (appearance == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.addButton(withTitle: "Ok")
            alert.runModal()
            
        }
        
        
        
        
    }
    
    
    

}// end of file





