//
//  StoryIdeasViewController.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 2/14/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Cocoa
import SwiftLoggly




class StoryIdeasViewController: NSViewController, NSTableViewDelegate, NSTableViewDataSource, NSTextViewDelegate, NSTextFieldDelegate {
    
    //  , NSTableViewDelegate, NSTableViewDataSource
    
    
    @IBOutlet weak var storyIdeaCategoryLabel: NSTextField!
    @IBOutlet weak var storyIdeaCategoryPopupButton: NSPopUpButton!
    @IBOutlet weak var storyIdeaCategoryNewIdeaTextfield: NSTextField!
    @IBOutlet weak var storyIdeaAddItemLabel: NSTextField!
    
    @IBOutlet weak var storyIdeaCategoryAddButton: NSButton!
    @IBOutlet weak var storyIdeaCategoryCategoryTable: NSTableView!
    @IBOutlet weak var storyIdeaTableIkeyColumn: NSTableColumn!
    @IBOutlet weak var storyIdeaCategoryLoadButton: NSButton!
    @IBOutlet var storyIdeaCategoryTextView: NSTextView!
    @IBOutlet weak var storyIdeaCategorySaveButton: NSButton!
    @IBOutlet weak var storyIdeaCategoryExportButton: NSButton!
    @IBOutlet weak var storyIdeaCategoryCloseButton: NSButton!
    @IBOutlet weak var storyIdeaDeleteIdeaButton: NSButton!
    
    
    
    
    
    var CategoryListArray: [CategoryList] = []

    var CategoryPopupButtonSelectedItem = String()
    
    
    var TextViewSaved = true
    
    var LoadedStoryIdea = String()
    
    var CurrentAppearanceStoryIdeasViewController = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
        
        
        
        storyIdeaCategoryCategoryTable.delegate = self
        storyIdeaCategoryCategoryTable.dataSource = self
        
        storyIdeaCategoryTextView.delegate = self
        
        storyIdeaCategoryNewIdeaTextfield.delegate = self
        
        
        

    }
    
    
    override func viewWillAppear() {
        
        LoadCategoryItems()
        
        
        
        
        CategoryPopupButtonSelectedItem = storyIdeaCategoryPopupButton.titleOfSelectedItem!
        
        
        
        
        
        
        
        
    } // end of viewWillAppear
    
    override func viewDidAppear() {
        
        
        if (SharedItemOwner != "") {
           
            
            storyIdeaCategoryPopupButton.setTitle(StoryIdeaCategory)
            self.categoryPopUpButton(self)
            
            var int = 0
            
            CategoryListArray.first { element in
                
                if (element.itemName == StoryIdeaName) {
                    //Index = element.ikey
                    return true
                }
                int = int + 1
                return false
                
            }
            
            //print("Index: \"\(StoryIdeaName)\" = \"\(int)\"")
            
            let indexset: IndexSet = [int]
            storyIdeaCategoryCategoryTable.selectRowIndexes(indexset, byExtendingSelection: false)
            
            storyIdeaLoadButton(self)
            
            if (SharedItemLock == true) {
                LockDownItems()
            } else {
                UnLockItems()
            }
            
            if (SAStoryIdeaBool == true) {
                ResetLockItems()
            }
            
        } else {
            
            ResetLockItems()
        }
        
        
    } // end of viewDidAppear
    
    
    
    
    override func viewDidLayout() {
        
        //        enum InterfaceStyle : String {
        //            case Dark, Light
        //
        //            init() {
        //                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
        //                self = InterfaceStyle(rawValue: type)!
        //            }
        //        }
        //
        //        let currentStyle = InterfaceStyle()
        //
        //        //print("currentStyle: \"\(currentStyle)\"  -  CurrentAppearance: \"\(CurrentAppearance)\" ")
        //
        //        if ("\(currentStyle)" != CurrentAppearanceNewUserViewController) {
        //            appearanceCheck()
        //        }
        
        appearanceCheck()
        
        super.viewDidLayout()
        
        
        
        
    }// end of viewDidLayout()
    
    
    func appearanceCheck() {
        
        enum InterfaceStyle : String {
            case Dark, Light
            
            init() {
                let type = UserDefaults.standard.string(forKey: "AppleInterfaceStyle") ?? "Light"
                self = InterfaceStyle(rawValue: type)!
            }
        }
        
        let currentStyle = InterfaceStyle()
        
        if ("\(currentStyle)" != CurrentAppearanceStoryIdeasViewController) {
            
            if (currentStyle.rawValue == "Dark") {
                
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColorDarkMode
                
//                storyIdeaCategoryCategoryTable.wantsLayer = true
//                storyIdeaCategoryCategoryTable.backgroundColor = NSColor.lightGray
//
//                storyIdeaCategoryTextView.wantsLayer = true
//                storyIdeaCategoryTextView.backgroundColor = NSColor.lightGray
                
                
                // Buttons
                
                storyIdeaCategoryAddButton.styleButtonText(button: storyIdeaCategoryAddButton, buttonName: "Add", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                storyIdeaCategoryAddButton.wantsLayer = true
                storyIdeaCategoryAddButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                storyIdeaCategoryAddButton.layer?.cornerRadius = 7
                
                storyIdeaCategoryLoadButton.styleButtonText(button: storyIdeaCategoryLoadButton, buttonName: "Load", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                storyIdeaCategoryLoadButton.wantsLayer = true
                storyIdeaCategoryLoadButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                storyIdeaCategoryLoadButton.layer?.cornerRadius = 7
                
                storyIdeaCategorySaveButton.styleButtonText(button: storyIdeaCategorySaveButton, buttonName: "Save", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                storyIdeaCategorySaveButton.wantsLayer = true
                storyIdeaCategorySaveButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                storyIdeaCategorySaveButton.layer?.cornerRadius = 7
                
                storyIdeaCategoryExportButton.styleButtonText(button: storyIdeaCategoryExportButton, buttonName: "Export", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                storyIdeaCategoryExportButton.wantsLayer = true
                storyIdeaCategoryExportButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                storyIdeaCategoryExportButton.layer?.cornerRadius = 7
                
                storyIdeaCategoryCloseButton.styleButtonText(button: storyIdeaCategoryCloseButton, buttonName: "Close", fontColor: .darkGray , alignment: .center, font: AppFont, size: 13.0)
                
                storyIdeaCategoryCloseButton.wantsLayer = true
                storyIdeaCategoryCloseButton.layer?.backgroundColor = NSColor.lightGray.cgColor
                storyIdeaCategoryCloseButton.layer?.cornerRadius = 7
                
                
                //  Labels
                
                storyIdeaCategoryLabel.font = NSFont(name:AppFontBold, size: 13.0)
                storyIdeaCategoryLabel.textColor = NSColor.white
                
                storyIdeaAddItemLabel.font = NSFont(name:AppFontBold, size: 13.0)
                storyIdeaAddItemLabel.textColor = NSColor.white
                
                
                
            } else if (currentStyle.rawValue == "Light") {
                
                
                view.wantsLayer = true
                view.layer?.backgroundColor = WindowBackgroundColor.cgColor
                
                storyIdeaCategoryCategoryTable.wantsLayer = true
                storyIdeaCategoryCategoryTable.backgroundColor = NSColor.white
                
                storyIdeaCategoryTextView.wantsLayer = true
                storyIdeaCategoryTextView.backgroundColor = NSColor.white
                
                // Buttons
                
                storyIdeaCategoryAddButton.styleButtonText(button: storyIdeaCategoryAddButton, buttonName: "Add", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                storyIdeaCategoryAddButton.wantsLayer = true
                storyIdeaCategoryAddButton.layer?.backgroundColor = NSColor.gray.cgColor
                storyIdeaCategoryAddButton.layer?.cornerRadius = 7
                
                storyIdeaCategoryLoadButton.styleButtonText(button: storyIdeaCategoryLoadButton, buttonName: "Load", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                storyIdeaCategoryLoadButton.wantsLayer = true
                storyIdeaCategoryLoadButton.layer?.backgroundColor = NSColor.gray.cgColor
                storyIdeaCategoryLoadButton.layer?.cornerRadius = 7
                
                storyIdeaCategorySaveButton.styleButtonText(button: storyIdeaCategorySaveButton, buttonName: "Save", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                storyIdeaCategorySaveButton.wantsLayer = true
                storyIdeaCategorySaveButton.layer?.backgroundColor = NSColor.gray.cgColor
                storyIdeaCategorySaveButton.layer?.cornerRadius = 7
                
                storyIdeaCategoryExportButton.styleButtonText(button: storyIdeaCategoryExportButton, buttonName: "Export", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                storyIdeaCategoryExportButton.wantsLayer = true
                storyIdeaCategoryExportButton.layer?.backgroundColor = NSColor.gray.cgColor
                storyIdeaCategoryExportButton.layer?.cornerRadius = 7
                
                storyIdeaCategoryCloseButton.styleButtonText(button: storyIdeaCategoryCloseButton, buttonName: "Close", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                
                storyIdeaCategoryCloseButton.wantsLayer = true
                storyIdeaCategoryCloseButton.layer?.backgroundColor = NSColor.gray.cgColor
                storyIdeaCategoryCloseButton.layer?.cornerRadius = 7
                
                
                //  Labels
                
                storyIdeaCategoryLabel.font = NSFont(name:AppFontBold, size: 13.0)
                storyIdeaCategoryLabel.textColor = NSColor.white
                
                storyIdeaAddItemLabel.font = NSFont(name:AppFontBold, size: 13.0)
                storyIdeaAddItemLabel.textColor = NSColor.white
                
            }
        }
        
        CurrentAppearanceStoryIdeasViewController = "\(currentStyle)"
        
        
    }//end of appearanceCheck()
    
    
    
    
    
    
    
    
    
    
    func LockDownItems() {
      
        storyIdeaCategoryPopupButton.isEnabled = false
        storyIdeaCategoryNewIdeaTextfield.isEditable = false
        storyIdeaCategoryAddButton.isEnabled = false
        storyIdeaCategoryCategoryTable.isEnabled = false
        
        storyIdeaCategoryLoadButton.isEnabled = false
        storyIdeaCategorySaveButton.isEnabled = false
        storyIdeaCategoryExportButton.isEnabled = false

    }
    
    func UnLockItems() {
        
        storyIdeaCategoryPopupButton.isEnabled = false
        storyIdeaCategoryNewIdeaTextfield.isEditable = false
        storyIdeaCategoryAddButton.isEnabled = false
        storyIdeaCategoryCategoryTable.isEnabled = false
        
        storyIdeaCategoryLoadButton.isEnabled = true
        storyIdeaCategorySaveButton.isEnabled = true
        storyIdeaCategoryExportButton.isEnabled = true
        
    }
    
    func ResetLockItems() {
        
        storyIdeaCategoryPopupButton.isEnabled = true
        storyIdeaCategoryNewIdeaTextfield.isEditable = true
        storyIdeaCategoryAddButton.isEnabled = true
        storyIdeaCategoryCategoryTable.isEnabled = true
        
        storyIdeaCategoryLoadButton.isEnabled = true
        storyIdeaCategorySaveButton.isEnabled = true
        storyIdeaCategoryExportButton.isEnabled = true
        
    }
    
    
    
    func LoadCategoryItems() {
        
        storyIdeaCategoryPopupButton.removeAllItems()
        
        storyIdeaCategoryCategoryTable.tableColumns.first?.isHidden = true
        
        let categoryList = ["Select a Category","Arcs","Starts","Mids","Endings","Scenes"]
        
        // Choose an Option = 0
        // Arcs             = 1
        // Starts           = 2
        // Mids             = 3
        // Endings          = 4
        // Scenes           = 5
        
        storyIdeaCategoryPopupButton.addItems(withTitles: categoryList)
     
        storyIdeaCategoryPopupButton.selectItem(at: 0)
        
    }
    
    @IBAction func categoryPopUpButton(_ sender: Any) {
        
        
        if (!TextViewSaved) {
            
            let alert = NSAlert()
            alert.messageText = "Any unsaved StoryIdeas will be lost!"
            alert.informativeText = "Do you want to Save ?"
            alert.alertStyle = .warning
            alert.addButton(withTitle: "Save")
            alert.addButton(withTitle: "Canel")
            alert.addButton(withTitle: "Dont Save")
            
            alert.window.titlebarAppearsTransparent = true
            
            if (CurrentAppearanceStoryIdeasViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            let returnCode = alert.runModal()
            print("returnCode: \(returnCode)")
            
            if (returnCode.rawValue == 1000) {
                print("Save Pressed")
                StoryIdeaSaveButton(self)
                PopupButtonLoad()
            }
            
            if (returnCode.rawValue == 1001) {
                print("Canel Pressed")
            }
            
            if (returnCode.rawValue == 1002) {
                print("Close without Saving Pressed")
                PopupButtonLoad()
            }
            
        } else {
            
            PopupButtonLoad()

            storyIdeaCategorySaveButton.isEnabled = false
            storyIdeaCategoryExportButton.isEnabled = false
        }
        
        

    }
    
    func PopupButtonLoad() {
        
        storyIdeaCategoryLoadButton.isEnabled = false
        storyIdeaDeleteIdeaButton.isEnabled = false
        
        storyIdeaCategoryExportButton.isEnabled = false
        
        storyIdeaCategoryTextView.string = ""
        
        let selectedItem = storyIdeaCategoryPopupButton.titleOfSelectedItem
        
        if (selectedItem == "Select a Category") {
            
            CategoryListArray = []
            storyIdeaCategoryCategoryTable.reloadData()
            storyIdeaCategoryCategoryTable.tableColumns.last?.title = ""
            
        } else {
            storyIdeaAddItemLabel.stringValue = String("New \(storyIdeaCategoryPopupButton.titleOfSelectedItem!):")
            storyIdeaCategoryCategoryTable.tableColumns.last?.title = "\(storyIdeaCategoryPopupButton.titleOfSelectedItem!)"
            
            StoryIdeaTableData()
        }
        
        
        storyIdeaCategorySaveButton.styleButtonText(button: storyIdeaCategorySaveButton, buttonName: "Save", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
        
        
    }
    
    
    
   
    @IBAction func categoryTableView(_ sender: Any) {
        
//        let selectedItemIkey = storyIdeaCategoryCategoryTable.column(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "ikey"))
//        let selectedItemCategory = storyIdeaCategoryCategoryTable.column(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "category"))
//
//        print("Table Selection - Ikey: \(selectedItemIkey) Category: \(selectedItemCategory)")
//
//        if (selectedItemIkey != 0) {
//            storyIdeaCategoryLoadButton.isEnabled = true
//        } else {
//            storyIdeaCategoryLoadButton.isEnabled = false
//        }
        
        
    }
    
    
    override func controlTextDidChange(_ obj: Notification) {
        
        //print("controlTextDidChange Ran")
        
        
        
        
        if (self.storyIdeaCategoryNewIdeaTextfield.stringValue.count < 1) {
            
        } else {
            
            let lastCharacter = String(describing: self.storyIdeaCategoryNewIdeaTextfield.stringValue.last!)
            
            //print("lastCharacter: \(lastCharacter)")
            
            let SymbolTest = CharacterSet.symbols.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
            
            let punctuationTest = CharacterSet.punctuationCharacters.isSuperset(of: CharacterSet(charactersIn: lastCharacter))

//            let SpaceNewLineTest = CharacterSet.whitespacesAndNewlines.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
            
            let NewLineTest = CharacterSet.newlines.isSuperset(of: CharacterSet(charactersIn: lastCharacter))
            
            if (SymbolTest || punctuationTest || NewLineTest) {
                
                __NSBeep()
                
                self.storyIdeaCategoryNewIdeaTextfield.stringValue = String(self.storyIdeaCategoryNewIdeaTextfield.stringValue.dropLast())
                
                return
            }
            
        }

        
    }
    

    
    
    ////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////
    
    
    
    //////////////////////////
    
    // Login Table info
    
    func StoryIdeaTableData() {
        
        db.close()
        
        CategoryListArray.removeAll()
        
        let SelectedCategoryIndex = storyIdeaCategoryPopupButton.indexOfSelectedItem
        
        print("SelectedCategoryIndex: \(SelectedCategoryIndex)")
        
        var countQuery = String()
        
        if (SharedItemOwner != "") {
            countQuery = "COUNT QUERY: select count(name) as cnt from storyideastable where userikeyreference = \(SharedItemOwner) and categoryikey = \(SelectedCategoryIndex)"
        } else {
            countQuery = "COUNT QUERY: select count(name) as cnt from storyideastable where userikeyreference = \(CurrentUserIkey!) and categoryikey = \(SelectedCategoryIndex)"
        }
        
        
        print("QUERY: \(countQuery)")
        
        
        
        var count: Int32!
        
        
        db.open()
        
        
        do {
            
            if (SharedItemOwner != "") {
                let CategoryQuery = try db.executeQuery("select count(name) as cnt from storyideastable where userikeyreference = \(SharedItemOwner) and categoryikey = \(SelectedCategoryIndex)", values:nil)
                
                while CategoryQuery.next() {
                    count = CategoryQuery.int(forColumn: "cnt")
                    
                }
            } else {
                let CategoryQuery = try db.executeQuery("select count(name) as cnt from storyideastable where userikeyreference = \(CurrentUserIkey!) and categoryikey = \(SelectedCategoryIndex)", values:nil)
                
                while CategoryQuery.next() {
                    count = CategoryQuery.int(forColumn: "cnt")
                    
                }
            }
            
            
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "StoryIdeasViewController-\(#function) Query Failed: \"\(countQuery)\"")
            DoAlert.DisplayAlert(Class: "StoryIdeasViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"\(countQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
        }
        
        print("Count: \(count)")
        
        
        ////////////////////////////
        
        
        var Item: String!
        var Ikey: String!
        
        var CategoryQueryPrint = String()
        
        if (SharedItemOwner != "") {
            CategoryQueryPrint = "select name,ikey from storyideastable where userikeyreference = \(SharedItemOwner) and categoryikey = \(SelectedCategoryIndex)"
        } else {
            CategoryQueryPrint = "select name,ikey from storyideastable where userikeyreference = \(CurrentUserIkey!) and categoryikey = \(SelectedCategoryIndex)"
        }
        
        print("QUERY: \(CategoryQueryPrint)")
        
        
        do {
            if (SharedItemOwner != "") {
            
                let CategoryQuery = try db.executeQuery("select name,ikey from storyideastable where userikeyreference = \(SharedItemOwner) and categoryikey = \(SelectedCategoryIndex)", values:nil)
                
                while CategoryQuery.next() {
                    Item = CategoryQuery.string(forColumn: "name")
                    Ikey = CategoryQuery.string(forColumn: "ikey")
                    
                    let ItemToAdd = CategoryList.init(itemName: Item, ikey: Ikey)
                    
                    print("ItemToAdd: Item:\(Item) Ikey:\(Ikey)")
                    
                    CategoryListArray.append(ItemToAdd)
                }
                
            } else {
                
                let CategoryQuery = try db.executeQuery("select name,ikey from storyideastable where userikeyreference = \(CurrentUserIkey!) and categoryikey = \(SelectedCategoryIndex)", values:nil)
                
                while CategoryQuery.next() {
                    Item = CategoryQuery.string(forColumn: "name")
                    Ikey = CategoryQuery.string(forColumn: "ikey")
                    
                    let ItemToAdd = CategoryList.init(itemName: Item, ikey: Ikey)
                    
                    //print("ItemToAdd: Item:\(Item) Ikey:\(Ikey)")
                    
                    CategoryListArray.append(ItemToAdd)
                }
                
            }
            
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "StoryIdeasViewController-\(#function) Query Failed: \"\(CategoryQueryPrint)\"")
            DoAlert.DisplayAlert(Class: "StoryIdeasViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"\(CategoryQueryPrint)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
        }
        
        storyIdeaCategoryCategoryTable.reloadData()
        
        
        
    }
    
    
    
    
    
    


    func numberOfRows(in tableVieww: NSTableView) -> Int {

        return CategoryListArray.count

    }

    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {

        guard let cell = tableView.makeView(withIdentifier: tableColumn!.identifier, owner: self) as? NSTableCellView else { return nil }


        if (tableColumn?.identifier)!.rawValue == "ikey" {

            cell.textField?.stringValue = CategoryListArray[row].ikey

        }
        
        if (tableColumn?.identifier)!.rawValue == "ideaname" {
            
            cell.textField?.stringValue = CategoryListArray[row].itemName
            
        }



        return cell

    }



    func tableViewSelectionDidChange(_ notification: Notification) {
        
        let selectedItemIkey = storyIdeaCategoryCategoryTable.column(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "ikey"))
        let selectedItemCategory = storyIdeaCategoryCategoryTable.column(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "ideaname"))

        print("Table Selection - Ikey: \(selectedItemIkey) Category: \(selectedItemCategory)")
        
        let SelectedRow = storyIdeaCategoryCategoryTable.selectedRow
//
//        print("SelectedRow: \(SelectedRow)")
        

        let isIndexValid = CategoryListArray.indices.contains(SelectedRow)

        if isIndexValid {
            storyIdeaCategoryLoadButton.isEnabled = true
            storyIdeaDeleteIdeaButton.isEnabled = true
        } else {
            storyIdeaCategoryLoadButton.isEnabled = false
            storyIdeaDeleteIdeaButton.isEnabled = false
        }
        
        
        
        

    }
    
    func textDidChange(_ notification: Notification) {
    
        //print("Text Was Changed")
        
        //let lastCharacter = String(describing: self.storyIdeaCategoryNewIdeaTextfield.stringValue.last!)
        
        let CharacterCount = self.storyIdeaCategoryTextView.string.count
        
        if (CharacterCount == 0) {
            return
        }
        
        let lastCharacter = String(describing: self.storyIdeaCategoryTextView.string.last!)
        
        if (lastCharacter == "\"") {
            
            __NSBeep()
            
            self.storyIdeaCategoryTextView.string = String(self.storyIdeaCategoryTextView.string.dropLast())
            
            return
        }
        
        
        storyIdeaCategorySaveButton.styleButtonText(button: storyIdeaCategorySaveButton, buttonName: "Save", fontColor: .red , alignment: .center, font: AppFont, size: 13.0)
    
        TextViewSaved = false
        
        storyIdeaCategorySaveButton.isEnabled = true
        
    }
    

    
    
    
    
    
    func RemoveWhiteSpace(aString: String) -> String {
        let replaced = aString.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        return replaced
    }
    
    
    
    
    
    //////////////////////////
    //////////////////////////
    //////////////////////////
    //////////////////////////
    
    
    @IBAction func storyIdeaLoadButton(_ sender: Any) {
        
        
        
        
        
        
        if (!TextViewSaved) {
            
            let alert = NSAlert()
            alert.messageText = "Any unsaved StoryIdeas will be lost!"
            alert.informativeText = "Do you want to Save ?"
            alert.alertStyle = .warning
            alert.addButton(withTitle: "Save")
            alert.addButton(withTitle: "Canel")
            alert.addButton(withTitle: "Dont Save")
            
            alert.window.titlebarAppearsTransparent = true
            
            if (CurrentAppearanceStoryIdeasViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            let returnCode = alert.runModal()
            print("returnCode: \(returnCode)")
            
            if (returnCode.rawValue == 1000) {
                print("Save Pressed")
                StoryIdeaSaveButton(self)
            }
            
            if (returnCode.rawValue == 1001) {
                print("Canel Pressed")
                return
            }
            
            if (returnCode.rawValue == 1002) {
                print("Dont Save Pressed")
            }
            
        }
        
        
        
        
        ////////////////////////////

        var query = String()
        
        if (SharedItemOwner != "") {
            
            query = "select storyvalue from storyideastable where name = \"\(StoryIdeaName)\" and userikeyreference = \(SharedItemOwner) and ikey = \(StoryIdeaIkey)"
            
            print("1 query: \(query)")
            
        } else {
            
        
            storyIdeaCategoryTextView.string = ""
            
            
            let SelectedRow = storyIdeaCategoryCategoryTable.selectedRow

            print("SelectedRow: \(SelectedRow)")
            
            let Name = CategoryListArray[SelectedRow].itemName
            let ikey = CategoryListArray[SelectedRow].ikey
            
            LoadedStoryIdea = Name
            
            print("Name: \(Name)")
            print("ikey: \(ikey)")
            
            query = "select storyvalue from storyideastable where name = \"\(Name)\" and userikeyreference = \(CurrentUserIkey!) and ikey = \(ikey)"
            
            print("2 query: \(query)")
            
            
        }

        var StoryIdeaValueDB = String()
        
        do {

            let StoryIdeaQuery = try db.executeQuery(query, values:nil)
            
            
            while StoryIdeaQuery.next() {
                StoryIdeaValueDB = StoryIdeaQuery.string(forColumn: "storyvalue")!

                print("StoryIdeaValueDB: \(StoryIdeaValueDB)")
                
            }
            
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "StoryIdeasViewController-\(#function) Query Failed: \"\(query)\"")
            DoAlert.DisplayAlert(Class: "StoryIdeasViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"\(query)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
        }
        
        loggly(LogType.Info, text: "StoryIdea: \"\(LoadedStoryIdea)\" Opened")
        
        storyIdeaCategoryTextView.string = StoryIdeaValueDB
        
        storyIdeaCategorySaveButton.isEnabled = true
        
        storyIdeaCategoryExportButton.isEnabled = true
        
    }
    
    
    
    @IBAction func StoryIdeaSaveButton(_ sender: Any) {
        
        
        if (LoadedStoryIdea == "" && SharedItemOwner == "") {
            storyIdeaCategorySaveButton.isEnabled = false
        } else {
        
            
            let StoryIdeaValue = storyIdeaCategoryTextView.string
            
            let SelectedRow = storyIdeaCategoryCategoryTable.selectedRow
            
            print("SelectedRow: \(SelectedRow)")
            
            //let Name = CategoryListArray[SelectedRow].itemName
            
            var UpdateQuery = String()
            
            if (SharedItemOwner != "") {
                UpdateQuery = "UPDATE storyideastable set storyvalue = \"\(StoryIdeaValue)\" where name = \"\(StoryIdeaName)\" and userikeyreference = \(SharedItemOwner)"
            } else {
                UpdateQuery = "UPDATE storyideastable set storyvalue = \"\(StoryIdeaValue)\" where name = \"\(LoadedStoryIdea)\" and userikeyreference = \(CurrentUserIkey!)"
            }
            
            
            print("UpdateQuery: \(UpdateQuery)")
            
            do {
                
                try db.executeUpdate(UpdateQuery, values:nil)
        
                // let StoryIdeaUpdateQuery =
                
    //            if (!StoryIdeaUpdateQuery) {
    //
    //                print("FAILED")
    //
    //            }
                
                
            } catch {
                print("ERROR: \(db.lastErrorMessage())")
                loggly(LogType.Error, text: "StoryIdeasViewController-\(#function) Query Failed: \"\(UpdateQuery)\"")
                DoAlert.DisplayAlert(Class: "StoryIdeasViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"\(UpdateQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
            }
            
            
            storyIdeaCategorySaveButton.styleButtonText(button: storyIdeaCategorySaveButton, buttonName: "Saved", fontColor: .green , alignment: .center, font: AppFontBold, size: 13.0)
            
            TextViewSaved = true
        }
        
    }
    
    
    
    @IBAction func StoryIdeaExportButton(_ sender: Any) {
        
        
        if (storyIdeaCategoryTextView.string.count < 1) {
            
        } else {
            
            StoryIdeaSaveButton(self)
            
            
            storyIdeaCategoryExportButton.isEnabled = false
            
            let textToExport = storyIdeaCategoryTextView.textStorage?.string
            
            if textToExport != "" {
                let mySave = NSSavePanel()
                
                mySave.message = "Choose where to Save your Story Idea"
                
                //mySave.message = "Choose where to Save \"\(LoadedStoryIdea).rtf\"."
                mySave.nameFieldStringValue = "\(LoadedStoryIdea).rtf"
                
                mySave.begin { (result) -> Void in
                    
                    if result.rawValue == NSFileHandlingPanelOKButton {
                        let filename = mySave.url
                        
                        print("filename: \(filename!)")
                        
                        do {
                            //try textToExport?.write(to: filename!, atomically: true, encoding: String.Encoding.utf8)


                            let textDNA = NSAttributedString(string: (self.storyIdeaCategoryTextView.textStorage?.string)!)

                            let range = NSRange(0..<self.storyIdeaCategoryTextView.string.count)

                            let textTSave = try textDNA.fileWrapper(from: range, documentAttributes: [NSAttributedString.DocumentAttributeKey.documentType:NSAttributedString.DocumentType.rtf])
                            try textTSave.write(to: filename!, options: FileWrapper.WritingOptions.atomic, originalContentsURL: nil)


                            self.storyIdeaCategoryExportButton.isEnabled = true

                        } catch {
                            // failed to write file (bad permissions, bad filename etc.)
                            
                            loggly(LogType.Error, text: "StoryIdeasViewController-\(#function) Saving \(self.LoadedStoryIdea).rtf Failed - ERROR: \(error)")
                            DoAlert.DisplayAlert(Class: "StoryIdeasViewController", Level: 1, Function: "\(#function)", MessageText: "Saving \(self.LoadedStoryIdea).rtf Failed", InformativeText: "Please quit and try again", DatabaseError: "\(error)")
                        }
                        
                    } else {
                        //__NSBeep()
                    }
                }
            }
            
            
            ///////////////
            
            
            
        }
        
        
        
        
        
        
    }
    
    
    
    @IBAction func StoryIdeasCloseButton(_ sender: Any) {
        
        if (!TextViewSaved) {
            
            let alert = NSAlert()
            alert.messageText = "Any unsaved StoryIdeas will be lost!"
            alert.informativeText = "Do you want to Save ?"
            alert.alertStyle = .warning
            alert.addButton(withTitle: "Save & Close")
            alert.addButton(withTitle: "Canel")
            alert.addButton(withTitle: "Close without Saving")
            
            if (CurrentAppearanceStoryIdeasViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.window.titlebarAppearsTransparent = true
            
            let returnCode = alert.runModal()
            print("returnCode: \(returnCode)")
            
            if (returnCode.rawValue == 1000) {
                print("Save & Close Pressed")
                StoryIdeaSaveButton(self)
                StoryIdeaWindowCloseCleanup()
                TextViewSaved = true
                self.view.window?.close()
            }
            
            if (returnCode.rawValue == 1001) {
                print("Canel Pressed")
            }
            
            if (returnCode.rawValue == 1002) {
                print("Close without Saving Pressed")
                StoryIdeaWindowCloseCleanup()
                TextViewSaved = true
                self.view.window?.close()
            }

        } else {
            StoryIdeaWindowCloseCleanup()
            TextViewSaved = true
            self.view.window?.close()
            
        }
        
        ResetLockItems()
        SharedItemOwner = ""
        SharedItemLock = false
//        OpenWindows.CheckForOpenWindows()
        if (StoryWindow.window?.isVisible == true) {
            StoryWindow.window?.makeKeyAndOrderFront(nil)
        } else {
            StoryListWindow.showWindow(self)
        }
        
    }
    
    func StoryIdeaWindowCloseCleanup() {
        
        storyIdeaCategoryPopupButton.setTitle("Select a Category")
        CategoryListArray = []
        storyIdeaCategoryCategoryTable.reloadData()
        storyIdeaCategoryCategoryTable.tableColumns.last?.title = ""
        storyIdeaCategoryTextView.string = ""
        storyIdeaCategoryLoadButton.isEnabled = false
        storyIdeaAddItemLabel.stringValue = "New:"
        LoadedStoryIdea = ""
        storyIdeaCategorySaveButton.isEnabled = false
        storyIdeaCategoryNewIdeaTextfield.stringValue = ""
        
    }
    
    @IBAction func storyIdeaAddButton(_ sender: Any) {
        
        let selectedItem = storyIdeaCategoryPopupButton.titleOfSelectedItem
        
        let Name =  RemoveWhiteSpace(aString: "\(storyIdeaCategoryNewIdeaTextfield.stringValue)")
        
        if (selectedItem == "Select a Category") {
            return
        } else if (Name == "") {
            return
        }
        
        
        
        ConnectionTest.CheckConnection()
        
        //////////////////////////////////
        
        
        
        

        let AddCheckQuery = "SELECT count(name) as cnt FROM storyideastable where name = \"\(Name)\" and userikeyreference = \"\(CurrentUserIkey!)\""
        
        print("AddCheckQuery: \(AddCheckQuery)")
        
        var AddCount = Int32()
        
        do {
            
            let AddCheckQueryRun = try db.executeQuery(AddCheckQuery, values:nil)
            
            while AddCheckQueryRun.next() {
                AddCount = AddCheckQueryRun.int(forColumn: "cnt")
            }
            
            
        } catch {
            print("ERROR: \(db.lastErrorMessage())")
            loggly(LogType.Error, text: "StoryIdeasViewController-\(#function) Query Failed: \"\(AddCheckQuery)\"")
            DoAlert.DisplayAlert(Class: "StoryIdeasViewController", Level: 1, Function: "\(#function)", MessageText: "Query Failed: \"\(AddCheckQuery)\"", InformativeText: "Please quit and try again", DatabaseError: "\(db.lastErrorMessage())")
        }

        
        print("AddCount: \(AddCount)")
        
        if (AddCount == 0) {
            
            let SelectedCategoryTitle = storyIdeaCategoryPopupButton.titleOfSelectedItem
            let SelectedCategoryIndex = storyIdeaCategoryPopupButton.indexOfSelectedItem
            
            let StoryValueInitalText = "New \(SelectedCategoryTitle!) named:  \(Name)"
            
            let InsertNewStoryIdeaQuery = String(format: "INSERT INTO storyideastable (ikey,userikeyreference,categoryikey,name,storyvalue) VALUES ((select MAX(ikey)+ 1 from storyideastable),'\(CurrentUserIkey!)','\(SelectedCategoryIndex)','\(Name)\','\(StoryValueInitalText)')")
            
            print("InsertNewStoryIdeaQuery: \(InsertNewStoryIdeaQuery)")
            
            do {

                try db.executeUpdate(InsertNewStoryIdeaQuery, values: nil)
                
            } catch let error as NSError {
                
                let ErrorMessage = "\(#function) - Failed to do query \(InsertNewStoryIdeaQuery)\nError: \(error.localizedDescription)"
                
                print(ErrorMessage)
                
                DoAlert.DisplayAlert(Class: "StoryIdeasViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to Add New Story Idea.\nPlease quit then try again.", DatabaseError: "Query: \(InsertNewStoryIdeaQuery)\n\n\(db.lastErrorMessage())")
                
                
                return
                
            }
            
            StoryIdeaTableData()
            storyIdeaCategoryNewIdeaTextfield.stringValue = ""
            
        } else {
            
            let alert = NSAlert()
            alert.messageText = "You have a Story Idea with that name already!"
            alert.informativeText = "Please choose a different name even ifs only slightly different."
            alert.alertStyle = .warning
            alert.addButton(withTitle: "OK")

            alert.window.titlebarAppearsTransparent = true
            
            if (CurrentAppearanceStoryIdeasViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.runModal()
            
//            let returnCode = alert.runModal()
//            print("returnCode: \(returnCode)")
//            if (returnCode.rawValue == 1000) {
//            }
            
            return
            
        }
        
        storyIdeaCategorySaveButton.styleButtonText(button: storyIdeaCategorySaveButton, buttonName: "Save", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
        
        
    }
    
    
    
    @IBAction func storyIdeaDeleteIdeaButtonAction(_ sender: Any) {
        
        let selectedItemIkey = storyIdeaCategoryCategoryTable.column(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "ikey"))
        let selectedItemCategory = storyIdeaCategoryCategoryTable.column(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "ideaname"))

        let SelectedRow = storyIdeaCategoryCategoryTable.selectedRow

        let SelectedString: String = CategoryListArray[SelectedRow].itemName
        let SelectedIkey: String = CategoryListArray[SelectedRow].ikey
        
        print("Table Selection - Ikey: \(SelectedIkey) Category: \(selectedItemCategory) Item Name: \(SelectedString) ")
        
        
        let isIndexValid = CategoryListArray.indices.contains(SelectedRow)
        
        if isIndexValid {
            // DELETE
            
            let alert = NSAlert()
            alert.messageText = "Deleting cannot be undone"
            alert.informativeText = "Do you want to delete \"\(SelectedString)\" ?"
            alert.alertStyle = .warning
            alert.addButton(withTitle: "Yes Delete")
            alert.addButton(withTitle: "Canel")
            
            
            alert.window.titlebarAppearsTransparent = true
            
            if (CurrentAppearanceStoryIdeasViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            let returnCode = alert.runModal()
            print("returnCode: \(returnCode)")
            
            if (returnCode.rawValue == 1000) {
                print("Delete Pressed")
                
                let DeleteStoryIdeaQuery = "DELETE FROM storyideastable WHERE ikey = \"\(SelectedIkey)\" AND name = \"\(SelectedString)\" "
                
                print("DeleteStoryIdeaQuery: \(DeleteStoryIdeaQuery)")
                
                
                //////////////////
                
                var Success = Bool()
                
                ConnectionTest.OpenDB()
                
                dbQueue.inTransaction { _, rollback in
                    
                    Success = db.executeStatements(DeleteStoryIdeaQuery)
                    
                    if (!Success) {
                        
                        rollback.pointee = true
                        
                        let ErrorMessage = "StoryIdeasViewController:\(#function) - Failed to delete Storyidea\n\(db.lastErrorMessage())\n\(DeleteStoryIdeaQuery))"
                        
                        print(ErrorMessage)
                        
                        loggly(LogType.Error, text: ErrorMessage)
                        
                        DoAlert.DisplayAlert(Class: "StoryIdeasViewController", Level: 1, Function: "\(#function)", MessageText: "Warning", InformativeText: "Failed to delete Storyidea \(SelectedString)\n\n\(DeleteStoryIdeaQuery)", DatabaseError: "\(db.lastErrorMessage())")
                        
                    }
                    
                    loggly(LogType.Info, text: "Story Idea: \"\(SelectedString)\" was Deleted\n\"\(storyIdeaCategoryTextView.string)\"")
                    
                    StoryIdeaTableData()
                    storyIdeaCategorySaveButton.isEnabled = false
                    storyIdeaCategoryLoadButton.isEnabled = false
                    storyIdeaCategoryExportButton.isEnabled = false
                    storyIdeaDeleteIdeaButton.isEnabled = false
                    storyIdeaCategoryTextView.string = ""
                    storyIdeaCategorySaveButton.styleButtonText(button: storyIdeaCategorySaveButton, buttonName: "Save", fontColor: .white , alignment: .center, font: AppFont, size: 13.0)
                    
                    
                } // end of intransaction

            }
            
            if (returnCode.rawValue == 1001) {
                print("Canel Pressed")
            }
            

        } else {
            // DONT DELETE - CANCELED
            let alert = NSAlert()
            alert.messageText = "Invalid Selection"
            alert.informativeText = "Please select an item and try again"
            alert.alertStyle = .warning
            alert.addButton(withTitle: "OK")
            
            
            
            alert.window.titlebarAppearsTransparent = true
            
            if (CurrentAppearanceStoryIdeasViewController == "Dark") {
                alert.window.backgroundColor = AlertBackgroundColorDarkMode
            } else {
                alert.window.backgroundColor = WindowBackgroundColor
            }
            
            alert.runModal()
        
        }
        
        
        
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
} // end of StoryIDeasViewConroller
