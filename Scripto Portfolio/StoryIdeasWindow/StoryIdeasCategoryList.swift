//
//  StoryIdeasCategoryList.swift
//  Scripto Portfolio
//
//  Created by Xcode Projects on 2/21/18.
//  Copyright © 2018 DTGM. All rights reserved.
//

import Foundation

class CategoryList: NSObject {
    let itemName: String
    let ikey: String
    
    init(itemName: String, ikey: String) {
        self.itemName = itemName
        self.ikey = ikey
    }
 
    
}
